import 'package:json_annotation/json_annotation.dart';
import 'Country.dart';

import 'Language.dart';
import 'base.dart';
part 'CountryDescription.g.dart';
@JsonSerializable(explicitToJson: true)
class CountryDescription extends _CountryDescription {


  factory CountryDescription.fromJson(Map<String, dynamic> js)  => _$CountryDescriptionFromJson(js);

  Map<String, dynamic> toJson() => _$CountryDescriptionToJson(this);

  CountryDescription();
// Language getLanguage() {
  //   return language;
  // }

  // void setLanguage(Language language) {
  //   this.language = language;
  // }
  //
  // Country getCountry() {
  //   return country;
  // }
  //
  // void setCountry(Country country) {
  //   this.country = country;
  // }
  //
  // String getName() {
  //   return name;
  // }
  //
  // void setName(String name) {
  //   this.name = name;
  // }
  //
  // String getTitle() {
  //   return title;
  // }
  //
  // void setTitle(String title) {
  //   this.title = title;
  // }
  //
  // String getDescription() {
  //   return description;
  // }
  //
  // void setDescription(String description) {
  //   this.description = description;
  // }
}

class _CountryDescription extends Base {
  Language language;
  Country country;
  String name;
  String title;
  String description;

}
