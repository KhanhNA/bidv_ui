import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'OdooResultDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class OdooResultDto extends _OdooResultDto{

  factory OdooResultDto.fromJson(Map<String, dynamic> js)  => _$OdooResultDtoFromJson(js);

  Map<String, dynamic> toJson() => _$OdooResultDtoToJson(this);

  OdooResultDto();
}
class _OdooResultDto extends Base{
  int length;
  int total_record;
  List<dynamic> records;
}