// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillPackageDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillPackageDto _$BillPackageDtoFromJson(Map<String, dynamic> json) {
  return BillPackageDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..key_map = json['key_map'] as String
    ..warehouse_name = json['warehouse_name'] as String
    ..address = json['address'] as String
    ..phone = json['phone'] as String
    ..from_bill_package_id = json['from_bill_package_id'] as int
    ..from_change_bill_package_id = json['from_change_bill_package_id'] as int
    ..bill_package_id = json['bill_package_id'] as int
    ..bill_lading_detail_id = json['bill_lading_detail_id'] as int
    ..item_name = json['item_name'] as String
    ..net_weight = (json['net_weight'] as num)?.toDouble()
    ..weight = (json['weight'] as num)?.toDouble()
    ..quantity_package = json['quantity_package'] as int
    ..length = (json['length'] as num)?.toDouble()
    ..width = (json['width'] as num)?.toDouble()
    ..height = (json['height'] as num)?.toDouble()
    ..totalVol = (json['totalVol'] as num)?.toDouble()
    ..capacity = (json['capacity'] as num)?.toDouble()
    ..total_weight = (json['total_weight'] as num)?.toDouble()
    ..quantity_import = json['quantity_import'] as int
    ..quantity_export = json['quantity_export'] as int
    ..product_type_name = json['product_type_name'] as String
    ..status = json['status'] as String
    ..productType = json['productType'] == null
        ? null
        : ProductTypeDto.fromJson(json['productType'] as Map<String, dynamic>)
    ..qr_char = json['qr_char'] as String
    ..insurance_name = json['insurance_name'] as String
    ..subscribe_name = json['subscribe_name'] as String
    ..rest = json['rest'] as int
    ..isSelect = json['isSelect'] as bool
    ..splitQuantity = json['splitQuantity'] as int;
}

Map<String, dynamic> _$BillPackageDtoToJson(BillPackageDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'key_map': instance.key_map,
      'warehouse_name': instance.warehouse_name,
      'address': instance.address,
      'phone': instance.phone,
      'from_bill_package_id': instance.from_bill_package_id,
      'from_change_bill_package_id': instance.from_change_bill_package_id,
      'bill_package_id': instance.bill_package_id,
      'bill_lading_detail_id': instance.bill_lading_detail_id,
      'item_name': instance.item_name,
      'net_weight': instance.net_weight,
      'weight': instance.weight,
      'quantity_package': instance.quantity_package,
      'length': instance.length,
      'width': instance.width,
      'height': instance.height,
      'totalVol': instance.totalVol,
      'capacity': instance.capacity,
      'total_weight': instance.total_weight,
      'quantity_import': instance.quantity_import,
      'quantity_export': instance.quantity_export,
      'product_type_name': instance.product_type_name,
      'status': instance.status,
      'productType': instance.productType?.toJson(),
      'qr_char': instance.qr_char,
      'insurance_name': instance.insurance_name,
      'subscribe_name': instance.subscribe_name,
      'rest': instance.rest,
      'isSelect': instance.isSelect,
      'splitQuantity': instance.splitQuantity,
    };
