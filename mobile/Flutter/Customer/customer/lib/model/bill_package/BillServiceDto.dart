
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
part 'BillServiceDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillServiceDto extends _BillServiceDto{

  factory BillServiceDto.fromJson(Map<String, dynamic> js) => _$BillServiceDtoFromJson(js);

  Map<String, dynamic> toJson() => _$BillServiceDtoToJson(this);

  BillServiceDto();
}
class _BillServiceDto extends Base{
   int id;
   String name;
   String display_name;
   String service_code;
   String type;
   double price;
   String description;
   String status;
   String partner_name;

}