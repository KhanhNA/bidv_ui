import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'OdooSessionDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class OdooSessionDto extends _OdooSessionDto{





  factory OdooSessionDto.fromJson(Map<String, dynamic> js)  => _$OdooSessionDtoFromJson(js);

  Map<String, dynamic> toJson() => _$OdooSessionDtoToJson(this);

  OdooSessionDto();
}
class _OdooSessionDto extends Base{

  String username;



  String db;
  int uid;
  bool is_system;
  bool is_admin;
  String name;
  String partner_display_name;
  int company_id;
  int partner_id;
  String show_effect;
  bool display_switch_company_menu;
  String session_id;
  String access_token;
  String distance_notification_message;
  String time_mobile_notification_key;
  String company_type;
  String save_log_duration;
  String duration_request;
  String distance_check_point;
  String sharevan_sos;
  String biding_time_confirm;
  String currency;
  String google_api_key_geocode;
  String weight_constant_order;
  String rating_customer_duration_key;
  String weight_unit;
  String volume_unit;
  String length_unit;
  String parcel_unit;


}
