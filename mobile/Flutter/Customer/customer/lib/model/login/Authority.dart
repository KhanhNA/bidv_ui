import 'package:json_annotation/json_annotation.dart';
import 'package:customer/model/base.dart';
part 'Authority.g.dart';
@JsonSerializable(explicitToJson: true)
class Authority extends _Authority {

    String error;
    Authority.withError(this.error);
    Authority();

    factory Authority.fromJson(Map<String, dynamic> js) =>Base().fromJs<Authority>(js, (js) => _$AuthorityFromJson(js));

    Map<String, dynamic> toJson() => _$AuthorityToJson(this);
}

class _Authority extends Base{
    String authority;

    String getMethod() {
        return authority.split("/")[0];
    }

    String getUrl() {
        return authority.substring(this.getMethod().length);
    }

    String getAuthority() {
        return authority.toLowerCase();
    }

    void setAuthority(String authority) {
        this.authority = authority;
    }
}
