// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Rule.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rule _$RuleFromJson(Map<String, dynamic> json) {
  return Rule()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..userAuthentication = json['userAuthentication'] == null
        ? null
        : UserAuthentication.fromJson(
            json['userAuthentication'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RuleToJson(Rule instance) => <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'userAuthentication': instance.userAuthentication?.toJson(),
    };
