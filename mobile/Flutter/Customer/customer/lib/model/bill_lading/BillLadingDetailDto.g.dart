// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillLadingDetailDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillLadingDetailDto _$BillLadingDetailDtoFromJson(Map<String, dynamic> json) {
  return BillLadingDetailDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..area_id = json['area_id'] as int
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..approved_type = json['approved_type'] as String
    ..status = json['status'] as String
    ..name = json['name'] as String
    ..id = json['id'] as int
    ..name_seq = json['name_seq'] as String
    ..max_price = (json['max_price'] as num)?.toDouble()
    ..min_price = (json['min_price'] as num)?.toDouble()
    ..total_weight = (json['total_weight'] as num)?.toDouble()
    ..warehouse = json['warehouse'] == null
        ? null
        : WarehouseDto.fromJson(json['warehouse'] as Map<String, dynamic>)
    ..address = json['address'] as String
    ..warehouse_name = json['warehouse_name'] as String
    ..phone = json['phone'] as String
    ..warehouse_type = json['warehouse_type'] as String
    ..description = json['description'] as String
    ..expected_from_time = json['expected_from_time'] as String
    ..expected_to_time = json['expected_to_time'] as String
    ..status_order = json['status_order'] as String
    ..order_type = json['order_type'] as String
    ..billService = (json['billService'] as List)
        ?.map((e) => e == null
            ? null
            : BillServiceDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..bill_lading_id = json['bill_lading_id'] as int
    ..billPackages = (json['billPackages'] as List)
        ?.map((e) => e == null
            ? null
            : BillPackageDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..wjson_address = json['wjson_address'] as String
    ..zone_area_id = json['zone_area_id'] as int
    ..areaDistances = (json['areaDistances'] as List)
        ?.map((e) => e == null
            ? null
            : AreaDistanceDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..express_distance = (json['express_distance'] as num)?.toDouble()
    ..price = (json['price'] as num)?.toDouble()
    ..total_parcel = json['total_parcel'] as int
    ..total_volume = (json['total_volume'] as num)?.toDouble();
}

Map<String, dynamic> _$BillLadingDetailDtoToJson(
        BillLadingDetailDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'latitude': instance.latitude,
      'area_id': instance.area_id,
      'longitude': instance.longitude,
      'approved_type': instance.approved_type,
      'status': instance.status,
      'name': instance.name,
      'id': instance.id,
      'name_seq': instance.name_seq,
      'max_price': instance.max_price,
      'min_price': instance.min_price,
      'total_weight': instance.total_weight,
      'warehouse': instance.warehouse?.toJson(),
      'address': instance.address,
      'warehouse_name': instance.warehouse_name,
      'phone': instance.phone,
      'warehouse_type': instance.warehouse_type,
      'description': instance.description,
      'expected_from_time': instance.expected_from_time,
      'expected_to_time': instance.expected_to_time,
      'status_order': instance.status_order,
      'order_type': instance.order_type,
      'billService': instance.billService?.map((e) => e?.toJson())?.toList(),
      'bill_lading_id': instance.bill_lading_id,
      'billPackages': instance.billPackages?.map((e) => e?.toJson())?.toList(),
      'wjson_address': instance.wjson_address,
      'zone_area_id': instance.zone_area_id,
      'areaDistances':
          instance.areaDistances?.map((e) => e?.toJson())?.toList(),
      'express_distance': instance.express_distance,
      'price': instance.price,
      'total_parcel': instance.total_parcel,
      'total_volume': instance.total_volume,
    };
