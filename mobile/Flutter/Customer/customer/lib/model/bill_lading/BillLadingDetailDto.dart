import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';

import '../AreaDistanceDto.dart';
import '../base.dart';

import 'package:json_annotation/json_annotation.dart';
part 'BillLadingDetailDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillLadingDetailDto extends _BillLadingDetailDto{

   factory BillLadingDetailDto.fromJson(Map<String, dynamic> js) => _$BillLadingDetailDtoFromJson(js);

   Map<String, dynamic> toJson() => _$BillLadingDetailDtoToJson(this);

   BillLadingDetailDto();
}
class _BillLadingDetailDto extends Base{
   double latitude;
   int area_id;
   double longitude;
   String approved_type;
   String status;
   String name;
   int id;
   String name_seq = "New";
   double max_price;
   double min_price;
   double total_weight;
   WarehouseDto warehouse;
   String address;
   String warehouse_name;
   String phone;
   String warehouse_type;//0. nhan, 1. tra
   // OdooRelType from_bill_lading_detail_id;
   String description;
   // OdooRelType service_id;
   String expected_from_time;
   String expected_to_time;
   String status_order = "running";
   String order_type;//0: trong zone, 1: ngoài zone  - so với kho xuất hàng.
   List<BillServiceDto> billService=[];
   int bill_lading_id;
   List<BillPackageDto> billPackages=[];
   String wjson_address;
   int zone_area_id;
   List<AreaDistanceDto> areaDistances=[];
   double express_distance;
   double price;
   int total_parcel;
   double total_volume;
}
