import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
import 'BillLadingDetailDto.dart';
part 'BillLadingDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillLadingDto extends _BillLadingDto{

   factory BillLadingDto.fromJson(Map<String, dynamic> js) => _$BillLadingDtoFromJson(js);

   Map<String, dynamic> toJson() => _$BillLadingDtoToJson(this);

   BillLadingDto();
}
class _BillLadingDto extends Base{
   int id;
   int from_bill_lading_id;
   int origin_bill_lading_detail_id;
   String insurance_name;
   String subscribe_name;
   int company_id;
//     OdooRelType insurance_id = new OdooRelType();
   double total_weight;
   int total_parcel;
   double total_amount;
   double total_volume;
   double vat;
   List<BillLadingDetailDto> arrBillLadingDetail=[];
   String name_seq = "New";
   String name;
   String code;
   String bill_state;
   // String status = "running";
   BillLadingStatus status;

   String status_routing;
   String create_date;
   bool web=true;
   DateTime start_date;
   DateTime end_date;

   String startDateStr;

   List<String> image_urls;

   InsuranceDto insurance;
//     RecurrentModel recurrent;

   int frequency;// 1 daily, 2 weekly, 3 monthly
   int day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
   int day_of_month;// if frequency = 3.
   SubscribeDto subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...
   OrderPackageDto order_package;//Loại đơn hàng: đơn express, ...
   double price_not_discount;
   double insurance_price;
   double service_price;
   setEndDate(SubscribeDto subscribe){
      if(start_date==null){
         start_date = DateTime.now();
      }
      if(subscribe == null){
         end_date = start_date;
         return;
      }
      Duration duration =Duration(days : subscribe.value);
      end_date = start_date.add(duration);
   }
}
extension BillLadingStatusExt on BillLadingStatus{
   String get value{
      switch(this){

         case BillLadingStatus.RUNNING:
            return 'running';
         case BillLadingStatus.FINISHED:
            return 'finished';
         case BillLadingStatus.DELETED:
            return 'deleted';
        default: {
           return 'running';
        }
      }
   }
}
enum BillLadingStatus{
@JsonValue('running') RUNNING,@JsonValue('finished') FINISHED,@JsonValue('deleted') DELETED
}