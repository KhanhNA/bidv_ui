// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RecurrentModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecurrentModel _$RecurrentModelFromJson(Map<String, dynamic> json) {
  return RecurrentModel()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..frequency = json['frequency'] as int
    ..day_of_week = json['day_of_week'] as int
    ..day_of_month = json['day_of_month'] as int
    ..subscribe = json['subscribe'] == null
        ? null
        : SubscribeDto.fromJson(json['subscribe'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RecurrentModelToJson(RecurrentModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'frequency': instance.frequency,
      'day_of_week': instance.day_of_week,
      'day_of_month': instance.day_of_month,
      'subscribe': instance.subscribe?.toJson(),
    };
