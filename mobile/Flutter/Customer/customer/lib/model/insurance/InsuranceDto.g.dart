// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'InsuranceDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InsuranceDto _$InsuranceDtoFromJson(Map<String, dynamic> json) {
  return InsuranceDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..code = json['code'] as String
    ..name = json['name'] as String
    ..vendor = json['vendor'] == null
        ? null
        : VendorDto.fromJson(json['vendor'] as Map<String, dynamic>)
    ..status = json['status'] as String
    ..amount = (json['amount'] as num)?.toDouble()
    ..display_name = json['display_name'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$InsuranceDtoToJson(InsuranceDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'vendor': instance.vendor?.toJson(),
      'status': instance.status,
      'amount': instance.amount,
      'display_name': instance.display_name,
      'description': instance.description,
    };
