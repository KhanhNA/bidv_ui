import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
import 'BillRoutingDetailDto.dart';
part 'BillRoutingDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillRoutingDto extends _BillRoutingDto{

  factory BillRoutingDto.fromJson(Map<String, dynamic> js) => _$BillRoutingDtoFromJson(js);

  Map<String, dynamic> toJson() => _$BillRoutingDtoToJson(this);

  BillRoutingDto();
}

class _BillRoutingDto extends Base {
   int id;
   int from_bill_lading_id;
   int origin_bill_lading_detail_id;
   String insurance_name;
   String insurance_code;
   String subscribe_name;
   bool routing_scan;
   int company_id;
//     OdooRelType insurance_id = new OdooRelType();
   double total_weight;
   int total_parcel;
   double total_amount;
   double total_volume;
   double vat;
   List<BillRoutingDetailDto> arrBillLadingDetail;
   String name_seq = "New";
   String name;
   String code;
   String status;
   String status_routing;
   String trouble_type;
   String create_date;
   int change_bill_lading_detail_id;
   String start_date;
   String end_date;

   String startDateStr;
   String description;//lý do hủy đơn

   InsuranceDto insurance;
//     RecurrentModel recurrent;

   int frequency;// 1 daily, 2 weekly, 3 monthly
   int day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
   int day_of_month;// if frequency = 3.
   SubscribeDto subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...
   String order_package_name;
}