// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillRoutingDetailDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillRoutingDetailDto _$BillRoutingDetailDtoFromJson(Map<String, dynamic> json) {
  return BillRoutingDetailDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..area_id = json['area_id'] as int
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..approved_type = json['approved_type'] as String
    ..status = json['status'] as String
    ..name = json['name'] as String
    ..id = json['id'] as int
    ..max_price = (json['max_price'] as num)?.toDouble()
    ..min_price = (json['min_price'] as num)?.toDouble()
    ..total_weight = (json['total_weight'] as num)?.toDouble()
    ..warehouse = json['warehouse'] == null
        ? null
        : WarehouseDto.fromJson(json['warehouse'] as Map<String, dynamic>)
    ..address = json['address'] as String
    ..warehouse_name = json['warehouse_name'] as String
    ..phone = json['phone'] as String
    ..warehouse_type = json['warehouse_type'] as String
    ..description = json['description'] as String
    ..image_urls =
        (json['image_urls'] as List)?.map((e) => e as String)?.toList()
    ..order_type = json['order_type'] as String
    ..billService = (json['billService'] as List)
        ?.map((e) => e == null
            ? null
            : BillServiceDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..bill_lading_id = json['bill_lading_id'] as int
    ..billPackages = (json['billPackages'] as List)
        ?.map((e) => e == null
            ? null
            : BillPackageDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..routing = (json['routing'] as List)
        ?.map((e) =>
            e == null ? null : RoutingDto.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$BillRoutingDetailDtoToJson(
        BillRoutingDetailDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'latitude': instance.latitude,
      'area_id': instance.area_id,
      'longitude': instance.longitude,
      'approved_type': instance.approved_type,
      'status': instance.status,
      'name': instance.name,
      'id': instance.id,
      'max_price': instance.max_price,
      'min_price': instance.min_price,
      'total_weight': instance.total_weight,
      'warehouse': instance.warehouse?.toJson(),
      'address': instance.address,
      'warehouse_name': instance.warehouse_name,
      'phone': instance.phone,
      'warehouse_type': instance.warehouse_type,
      'description': instance.description,
      'image_urls': instance.image_urls,
      'order_type': instance.order_type,
      'billService': instance.billService?.map((e) => e?.toJson())?.toList(),
      'bill_lading_id': instance.bill_lading_id,
      'billPackages': instance.billPackages?.map((e) => e?.toJson())?.toList(),
      'routing': instance.routing?.map((e) => e?.toJson())?.toList(),
    };
