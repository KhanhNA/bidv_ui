// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillRoutingDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillRoutingDto _$BillRoutingDtoFromJson(Map<String, dynamic> json) {
  return BillRoutingDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..from_bill_lading_id = json['from_bill_lading_id'] as int
    ..origin_bill_lading_detail_id = json['origin_bill_lading_detail_id'] as int
    ..insurance_name = json['insurance_name'] as String
    ..insurance_code = json['insurance_code'] as String
    ..subscribe_name = json['subscribe_name'] as String
    ..routing_scan = json['routing_scan'] as bool
    ..company_id = json['company_id'] as int
    ..total_weight = (json['total_weight'] as num)?.toDouble()
    ..total_parcel = json['total_parcel'] as int
    ..total_amount = (json['total_amount'] as num)?.toDouble()
    ..total_volume = (json['total_volume'] as num)?.toDouble()
    ..vat = (json['vat'] as num)?.toDouble()
    ..arrBillLadingDetail = (json['arrBillLadingDetail'] as List)
        ?.map((e) => e == null
            ? null
            : BillRoutingDetailDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..name_seq = json['name_seq'] as String
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..status = json['status'] as String
    ..status_routing = json['status_routing'] as String
    ..trouble_type = json['trouble_type'] as String
    ..create_date = json['create_date'] as String
    ..change_bill_lading_detail_id = json['change_bill_lading_detail_id'] as int
    ..start_date = json['start_date'] as String
    ..end_date = json['end_date'] as String
    ..startDateStr = json['startDateStr'] as String
    ..description = json['description'] as String
    ..insurance = json['insurance'] == null
        ? null
        : InsuranceDto.fromJson(json['insurance'] as Map<String, dynamic>)
    ..frequency = json['frequency'] as int
    ..day_of_week = json['day_of_week'] as int
    ..day_of_month = json['day_of_month'] as int
    ..subscribe = json['subscribe'] == null
        ? null
        : SubscribeDto.fromJson(json['subscribe'] as Map<String, dynamic>)
    ..order_package_name = json['order_package_name'] as String;
}

Map<String, dynamic> _$BillRoutingDtoToJson(BillRoutingDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'from_bill_lading_id': instance.from_bill_lading_id,
      'origin_bill_lading_detail_id': instance.origin_bill_lading_detail_id,
      'insurance_name': instance.insurance_name,
      'insurance_code': instance.insurance_code,
      'subscribe_name': instance.subscribe_name,
      'routing_scan': instance.routing_scan,
      'company_id': instance.company_id,
      'total_weight': instance.total_weight,
      'total_parcel': instance.total_parcel,
      'total_amount': instance.total_amount,
      'total_volume': instance.total_volume,
      'vat': instance.vat,
      'arrBillLadingDetail':
          instance.arrBillLadingDetail?.map((e) => e?.toJson())?.toList(),
      'name_seq': instance.name_seq,
      'name': instance.name,
      'code': instance.code,
      'status': instance.status,
      'status_routing': instance.status_routing,
      'trouble_type': instance.trouble_type,
      'create_date': instance.create_date,
      'change_bill_lading_detail_id': instance.change_bill_lading_detail_id,
      'start_date': instance.start_date,
      'end_date': instance.end_date,
      'startDateStr': instance.startDateStr,
      'description': instance.description,
      'insurance': instance.insurance?.toJson(),
      'frequency': instance.frequency,
      'day_of_week': instance.day_of_week,
      'day_of_month': instance.day_of_month,
      'subscribe': instance.subscribe?.toJson(),
      'order_package_name': instance.order_package_name,
    };
