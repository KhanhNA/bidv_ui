// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AreaDistanceDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AreaDistanceDto _$AreaDistanceDtoFromJson(Map<String, dynamic> json) {
  return AreaDistanceDto()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..from_name_seq = json['from_name_seq'] as String
    ..to_name_seq = json['to_name_seq'] as String
    ..from_warehouse_name = json['from_warehouse_name'] as String
    ..to_warehouse_name = json['to_warehouse_name'] as String
    ..type = json['type'] as int
    ..distance = (json['distance'] as num)?.toDouble()
    ..duration = (json['duration'] as num)?.toDouble()
    ..price = (json['price'] as num)?.toDouble();
}

Map<String, dynamic> _$AreaDistanceDtoToJson(AreaDistanceDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'from_name_seq': instance.from_name_seq,
      'to_name_seq': instance.to_name_seq,
      'from_warehouse_name': instance.from_warehouse_name,
      'to_warehouse_name': instance.to_warehouse_name,
      'type': instance.type,
      'distance': instance.distance,
      'duration': instance.duration,
      'price': instance.price,
    };
