
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
import 'HubDto.dart';
import 'ZoneDto.dart';
part 'AreaDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class AreaDto extends _AreaDto{

  factory AreaDto.fromJson(Map<String, dynamic> js) => _$AreaDtoFromJson(js);

  Map<String, dynamic> toJson() => _$AreaDtoToJson(this);

  AreaDto();
}
class _AreaDto extends Base{
   int id;
   // OdooRelType country_id;
   String name;
   String code;
   String province_name;
   // OdooRelType zone_area_id;
   HubDto hubInfo;
   ZoneDto zoneInfo;
}