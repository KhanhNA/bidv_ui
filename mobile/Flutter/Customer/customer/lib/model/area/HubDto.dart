

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
part 'HubDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class HubDto extends _HubDto{

  factory HubDto.fromJson(Map<String, dynamic> js) => _$HubDtoFromJson(js);

  Map<String, dynamic> toJson() => _$HubDtoToJson(this);

  HubDto();
}
class _HubDto extends Base{
   int id;
   double latitude;
   double longitude;
   String name;
   String address;
   String status;

   String name_seq;
}