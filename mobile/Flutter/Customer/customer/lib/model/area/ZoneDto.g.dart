// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ZoneDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ZoneDto _$ZoneDtoFromJson(Map<String, dynamic> json) {
  return ZoneDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..name_seq = json['name_seq'] as String
    ..depotInfo = json['depotInfo'] == null
        ? null
        : HubDto.fromJson(json['depotInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ZoneDtoToJson(ZoneDto instance) => <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'name_seq': instance.name_seq,
      'depotInfo': instance.depotInfo?.toJson(),
    };
