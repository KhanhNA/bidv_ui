// import 'package:json_annotation/json_annotation.dart';
//
// part 'base_model.g.dart';
//
// @JsonSerializable(explicitToJson: true)
import 'package:json_annotation/json_annotation.dart';

class BaseModel {

  @JsonKey(ignore: true)
  bool checked=false;
  @JsonKey(ignore: true)
  int index;
  @JsonKey(ignore: true)
  String transactionErrCode;
  // List<BaseModel> child;
  // @JsonKey(ignore: true)
  @JsonKey(ignore: true)
  String error;
  void bindingAction() {

  }
  BaseModel();
  factory BaseModel.fromJson(Map<String, dynamic> json) => null;

  Map<String, dynamic> toJson() => null;
  BaseModel.withError(this.error);
}