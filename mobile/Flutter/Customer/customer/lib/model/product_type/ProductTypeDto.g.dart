// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProductTypeDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductTypeDto _$ProductTypeDtoFromJson(Map<String, dynamic> json) {
  return ProductTypeDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name_seq = json['name_seq'] as String
    ..name = json['name'] as String
    ..net_weight = (json['net_weight'] as num)?.toDouble()
    ..status = json['status'] as String
    ..extra_price = (json['extra_price'] as num)?.toDouble();
}

Map<String, dynamic> _$ProductTypeDtoToJson(ProductTypeDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name_seq': instance.name_seq,
      'name': instance.name,
      'net_weight': instance.net_weight,
      'status': instance.status,
      'extra_price': instance.extra_price,
    };
