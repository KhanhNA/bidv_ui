import '../base.dart';
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'RatingBadgesDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RatingBadgesDto extends _RatingBadgesDto{

  factory RatingBadgesDto.fromJson(Map<String, dynamic> js) => _$RatingBadgesDtoFromJson(js);

  Map<String, dynamic> toJson() => _$RatingBadgesDtoToJson(this);

  RatingBadgesDto();
}
class _RatingBadgesDto extends Base {
   int id;
   String rating_level;// Badges ứng với số sao rating
   String name;
   String description;
   String image;
   bool check=false;
}