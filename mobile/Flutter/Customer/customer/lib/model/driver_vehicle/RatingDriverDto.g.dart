// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RatingDriverDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingDriverDto _$RatingDriverDtoFromJson(Map<String, dynamic> json) {
  return RatingDriverDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..num_rating = json['num_rating'] as int
    ..note = json['note'] as String
    ..badges_routings = (json['badges_routings'] as List)
        ?.map((e) => e == null
            ? null
            : RatingBadgesDto.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RatingDriverDtoToJson(RatingDriverDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'num_rating': instance.num_rating,
      'note': instance.note,
      'badges_routings':
          instance.badges_routings?.map((e) => e?.toJson())?.toList(),
    };
