// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DriverDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DriverDto _$DriverDtoFromJson(Map<String, dynamic> json) {
  return DriverDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..driver_code = json['driver_code'] as String
    ..name = json['name'] as String
    ..phone = json['phone'] as String
    ..image_1920 = json['image_1920'] as String;
}

Map<String, dynamic> _$DriverDtoToJson(DriverDto instance) => <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'driver_code': instance.driver_code,
      'name': instance.name,
      'phone': instance.phone,
      'image_1920': instance.image_1920,
    };
