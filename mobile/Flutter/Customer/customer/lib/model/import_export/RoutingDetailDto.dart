import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/driver_vehicle/VehicleDto.dart';

import '../base.dart';

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'RoutingDetailDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RoutingDetailDto extends _RoutingDetailDto{

  factory RoutingDetailDto.fromJson(Map<String, dynamic> js) => _$RoutingDetailDtoFromJson(js);

  Map<String, dynamic> toJson() => _$RoutingDetailDtoToJson(this);

  RoutingDetailDto();
}

class _RoutingDetailDto extends Base{
   int id;
   String type;// 0 nhận, 1 trả.(Đối với kho. Với vai trò là driver thì ngược lại)
   String order_type; // 0 cố định, 1 phát sinh.
   String insurance_name;
   List<BillServiceDto> service_name;
   String routing_plan_detail_code;
   int status;


   String date_plan;
   String expected_from_time;
   String expected_to_time;
   VehicleDto vehicle ;
   double latitude;
   double longitude;
   String address;
   String phone;
   String warehouse_name;
   List<BillPackageDto> list_bill_package_import;
   List<BillPackageDto> list_bill_package_export;

   String routing_plan_day_code;
}