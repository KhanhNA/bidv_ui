
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/driver_vehicle/DriverDto.dart';
import 'package:customer/model/driver_vehicle/RatingDriverDto.dart';

import '../base.dart';

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import 'RoutingDetailDto.dart';

part 'RoutingDayDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RoutingDayDto extends _RoutingDayDto{

  factory RoutingDayDto.fromJson(Map<String, dynamic> js) => _$RoutingDayDtoFromJson(js);

  Map<String, dynamic> toJson() => _$RoutingDayDtoToJson(this);

  RoutingDayDto();
}

class _RoutingDayDto extends Base{
  int id;
  int routing_plan_day_id;
  int bill_routing_id;
  String bill_routing_name;
  String routing_plan_day_code;
  String bill_lading_detail_code;
  String actual_time;
  String expected_from_time;
  String expected_to_time;
  String name;
  String license_plate;
  String type; // 0 kho xuất, 1 kho nhập
  String status;
  String trouble_type;
  String date_plan;
  DriverDto driver;
   int change_bill_lading_detail_id;
   List<BillServiceDto> services;
   RoutingDayDto vehicle;
   double latitude;
   double longitude;
   bool arrived_check;
   String address;
   String phone;
   String booked_employee;
   String insurance_name;
   String warehouse_name;
   List<String> image_urls;
   List<RoutingDetailDto> routing_plan_day_detail;
   List<BillPackageDto> list_bill_package;
   bool sos_ids;
   RatingDriverDto rating_drivers;

   String driver_name;
   String driver_phone;
   String accept_time;
   String confirm_time;
   bool so_type;
}