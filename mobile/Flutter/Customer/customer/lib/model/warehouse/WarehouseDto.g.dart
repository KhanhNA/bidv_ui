// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'WarehouseDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WarehouseDto _$WarehouseDtoFromJson(Map<String, dynamic> json) {
  return WarehouseDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..warehouse_code = json['warehouse_code'] as String
    ..name = json['name'] as String
    ..state_id = json['state_id'] as int
    ..area_id = json['area_id'] as int
    ..district = json['district'] as int
    ..ward = json['ward'] as int
    ..country_id = json['country_id'] as int
    ..province_name = json['province_name'] as String
    ..district_name = json['district_name'] as String
    ..ward_name = json['ward_name'] as String
    ..address = json['address'] as String
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..phone = json['phone'] as String
    ..areaInfo = json['areaInfo'] == null
        ? null
        : AreaDto.fromJson(json['areaInfo'] as Map<String, dynamic>)
    ..name_seq = json['name_seq'] as String
    ..fromWareHouseCode = json['fromWareHouseCode'] as String
    ..wjson_address = json['wjson_address'] as String
    ..placeId = json['placeId'] as String;
}

Map<String, dynamic> _$WarehouseDtoToJson(WarehouseDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'warehouse_code': instance.warehouse_code,
      'name': instance.name,
      'state_id': instance.state_id,
      'area_id': instance.area_id,
      'district': instance.district,
      'ward': instance.ward,
      'country_id': instance.country_id,
      'province_name': instance.province_name,
      'district_name': instance.district_name,
      'ward_name': instance.ward_name,
      'address': instance.address,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'phone': instance.phone,
      'areaInfo': instance.areaInfo?.toJson(),
      'name_seq': instance.name_seq,
      'fromWareHouseCode': instance.fromWareHouseCode,
      'wjson_address': instance.wjson_address,
      'placeId': instance.placeId,
    };
