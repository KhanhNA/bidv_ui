import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'AdministrativeDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class AdministrativeDto extends _AdministrativeDto {
  factory AdministrativeDto.fromJson(Map<String, dynamic> js) =>
      _$AdministrativeDtoFromJson(js);

  Map<String, dynamic> toJson() => _$AdministrativeDtoToJson(this);

  AdministrativeDto();
}
class _AdministrativeDto extends Base{
   int id;
   int parentId;
   String name;
   String code;
   int country_id;
}