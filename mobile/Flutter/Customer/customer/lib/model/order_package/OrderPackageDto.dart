import 'package:customer/model/base.dart';
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
part 'OrderPackageDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class OrderPackageDto extends _OrderPackageDto{

   factory OrderPackageDto.fromJson(Map<String, dynamic> js) => _$OrderPackageDtoFromJson(js);

   Map<String, dynamic> toJson() => _$OrderPackageDtoToJson(this);

   OrderPackageDto();
}

class _OrderPackageDto extends Base{
   int id;
   String name;
   String type;
   String description;
}