import 'dart:io';

import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/util/DateUtils.dart';
import 'package:customer/model/area/AddressDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/create_account/create_account_vm.dart';
import 'package:customer/base/ui/common_ui.dart';
import 'package:dio/dio.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:get/get.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class CreateAccountController extends BaseController<CreateAccountVM> {
  CreateAccountController(CreateAccountVM vm) : super(vm) {
    viewModel.provinces = ListController(loadFunc: this.getAreas);
    viewModel.districts = ListController(loadFunc: this.getAreas);
    viewModel.careers = ListController(loadFunc: this.getCareers);
    viewModel.dayOfBirth.value = DateTimeUtils.convertDateToString(
        DateTime.now(), DateTimeUtils.DATE_SHOW_FORMAT);
  }

  Future<PagedListData<AddressDto>> getCareers(int page) async {
    Map<String, dynamic> params = {'location_type': 'province', 'parent_id': 0};
    final data = await viewModel.repository.getCareer(params);
    return data;
  }

  Future<PagedListData<AddressDto>> getAreas(int page) async {
    int parent_id = 0;

    if (viewModel.areaType == 'district') {
      parent_id = viewModel.province.value.id;
    }
    Map<String, dynamic> params = {
      'location_type': viewModel.areaType,
      'parent_id': parent_id
    };
    // params.removeNullOrBlankValue();
    final data = await viewModel.repository.getArea(params);
    if (viewModel.areaType == 'province') {
      viewModel?.province?.value = data.content[0];
    }
    return data;
  }

  Future<ResponseBody> createAccount() async{
    Map<String, dynamic> params = {'location_type': 'province', 'parent_id': 0};
    final data = await viewModel.repository.createAccount(params,viewModel.listImage);
    return data;
  }

  List<Asset> _getAssets() {
    List<Asset> lst = [];
    viewModel.listImage.forEach((element) {
      if (element != null) lst.add(element);
    });
    return lst;
  }

  Future<void> goSelectImage() async {
    final value = await CommonUI.loadAssets(3, _getAssets());
    if (value == null) {
      return;
    }
    viewModel.listImage.clear();
    viewModel.listImage.addAll(value);
    viewModel.quantityImage.value = viewModel.listImage.length;
  }
}
