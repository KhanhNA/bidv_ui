import 'dart:io';

import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/get/my_text_form_field.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/pages/create_account/create_account_controller.dart';
import 'package:customer/pages/create_account/create_account_vm.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:get/get.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

import 'package:flutter/widgets.dart';

class CreateAccountSecondStepTap
    extends BaseView<CreateAccountVM, CreateAccountController> {
  @override
  Widget body(BuildContext context) {
    return Container(
        child: Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 40,
            child: MyTextFormField(
              obscureText: true,
              decoration:
                  InputDecoration(labelText: 'Create_account.password'.tr),
              onChanged: (text) => viewModel?.password?.value = text,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 40,
            child: MyTextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  labelText: 'Create_account.confirm_password'.tr),
              onChanged: (text) => viewModel?.passwordConfirm?.value = text,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 40,
            child: MyTextFormField(
              obscureText: true,
              decoration: InputDecoration(labelText: 'Create_account.email'.tr),
              onChanged: (text) => viewModel?.email?.value = text,
            ),
          ),
        ),
        Row(
          children: <Widget>[
            Text('Add image'),
            IconButton(
              icon: Icon(Icons.add_a_photo_outlined),
              onPressed: () {
                // Go to info
                controller.goSelectImage();
              },
            )
          ],
        ),
        Container(
          child: Obx(() => viewModel.quantityImage.value ==0
              ? Container(child: Text('NO IMAGE')) :
              Expanded(
                  child: GridView.count(
                  crossAxisCount: 3,
                  children: List.generate(viewModel.listImage.length, (index) {
                    Asset asset = viewModel.listImage[index];
                    return AssetThumb(
                      asset: asset,
                      width: 100,
                      height: 100,
                    );
                  }),
                ))),
        ),
        ElevatedButton(
          child: Text(
            'Create_account.next_step'.tr,
          ),
          style: ButtonStyle(
            minimumSize: MaterialStateProperty.all(Size(20, 40)),
          ),
          onPressed: () async {
            _confirm();
          },
        ),
      ],
    ));
  }

  _confirm() {
    // validate
    controller.createAccount();
  }

  @override
  CreateAccountController putController() {
    return CreateAccountController(CreateAccountVM());
  }
}
