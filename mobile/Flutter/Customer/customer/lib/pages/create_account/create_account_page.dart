import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/pages/create_account/create_account_controller.dart';
import 'package:customer/pages/create_account/create_account_vm.dart';
import 'package:customer/pages/create_account/step_1/create_account_step_1_page.dart';
import 'package:customer/pages/create_account/step_2/create_account_step_2_page.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class CreateAccountPage extends BaseView<CreateAccountVM,CreateAccountController>{
  final List<Widget> listTab = <Widget>[];
  BuildContext currentContext;
  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(
      title: Text('Create account'),
    );
  }

  CreateAccountPage(){
    listTab.addAll([
      CreateAccountTapView(),
      CreateAccountSecondStepTap(),
    ]);
  }

  @override
  Widget body(BuildContext context) {
    currentContext= context;
    return DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: AppTheme.white,

                child: TabBar(
                  tabs: viewModel.notificationStatus.map((e) =>
                      Tab(text: ('Create_account.'+e).tr)).toList(),
                ),
              ),
            ),
            Expanded(
              child: TabBarView(children: listTab),
            )
          ],
        ));
  }

  @override
  CreateAccountController putController() {
   return CreateAccountController(CreateAccountVM());
  }

}