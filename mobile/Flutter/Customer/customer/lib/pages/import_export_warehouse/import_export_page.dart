import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'import_export_controller.dart';
import 'import_export_vm.dart';

class ImportExportPage
    extends BaseView<ImportExportVM, ImportExportController> {

  ImportExportPage(String routing_plan_day_code){
    viewModel.routingDayCode=routing_plan_day_code;
    controller.initData();
  }

  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(
        title: Container(
            child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Detail routing day',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.normal, fontSize: 14),
        ),
        ElevatedButton(
            onPressed: () => {print('Confirm routing')},
            child: Text('Confirm routing'))
      ],
    )));
  }

  @override
  Widget body(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: <Widget>[
                context.titleContainer('Routing day info', Icon(Icons.list)),
                Obx(()=>context.listItemViewV2('Routing code', viewModel?.routingDayObs?.value?.address??'No address'))
              ],
            ),
          ),
        ),
      ],
    ));
  }

  @override
  ImportExportController putController() {
    return ImportExportController(ImportExportVM());
  }
}
