import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/import_export/RoutingDayDto.dart';

import 'import_export_vm.dart';

class ImportExportController extends BaseController<ImportExportVM> {
  ImportExportController(ImportExportVM vm) : super(vm);
  initData() async{
    viewModel.routingDayObs.value= await getRoutingDayDetailDto();
  }

  Future<RoutingDayDto> getRoutingDayDetailDto() async {
    Map<String, dynamic> params = {'routing_plan_day_code': viewModel.routingDayCode};
    final response = await viewModel.repository.getRoutingDayDetailDto(params);
    return response;
  }
}
