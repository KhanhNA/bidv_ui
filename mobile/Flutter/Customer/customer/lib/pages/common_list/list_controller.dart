import 'package:loading_more_list/loading_more_list.dart';
import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/model/list/PagedListData.dart';

class ListController<T> extends LoadingMoreBase<T> {
  final Future<PagedListData<T>> Function(int page) loadFunc;

  ListController({
    this.loadFunc,
    this.maxLength = 300,
  }) ;
  String keyword = "";
  int totalPage = 0;
  int currentPage = 1;

  bool forceRefresh = false;

  @override
  bool get hasMore => (totalPage > 0 && currentPage <= totalPage) || forceRefresh;
  final int maxLength;


  @override
  Future<bool> refresh([bool notifyStateChanged = false]) async {
    // _hasMore = true;
    print("call refresh 1");
    currentPage = 1;
    //force to refresh list when you don't want clear list before request
    //for the case, if your list already has 20 items.
    forceRefresh = !notifyStateChanged;
    final bool result = await super.refresh(notifyStateChanged);
    forceRefresh = false;
    return true;
  }

  @override
  Future<bool> loadData([bool isloadMoreAction = false]) async {
    print("call loadData1");
    if (currentPage == 1) {
      clear();
    }
    return run(() async {
      try {
        final data = await loadFunc(currentPage);
        if (data != null && data.content.length > 0) {
          addAll(data.content);
          totalPage = (data.totalPages);
          currentPage++;
        }

      } finally {
        return true;
      }
    });

  }
}
