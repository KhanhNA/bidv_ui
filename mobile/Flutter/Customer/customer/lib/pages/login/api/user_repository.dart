
import 'package:customer/base/auth/token.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:customer/model/login/UserAuthentication.dart';
import 'package:customer/model/login/login_body.dart';
import 'package:customer/model/session/OdooSessionDto.dart';

import 'user_provider.dart';

class UserRepository {
  final UserProvider _api = UserProvider();

  Future<OdooSessionDto> login(LoginBody loginBody) => _api.loginUser(loginBody);
  Future<UserAuthentication> getUserMe() => _api.getUserMe();
  Future<PartnerDto> getInfoCustomer() => _api.getInfoCustomer();


}