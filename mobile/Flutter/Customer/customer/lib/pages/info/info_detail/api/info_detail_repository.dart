import 'package:customer/model/login/PartnerDto.dart';
import 'package:customer/pages/info/info_detail/api/info_detail_provider.dart';

class InfoDetailRepository{
  final InfoDetailProvider _api= InfoDetailProvider();
  Future<PartnerDto> updateInfoCustomer(Map<String,dynamic> params) async => _api.updateInfoCustomer(params);
}