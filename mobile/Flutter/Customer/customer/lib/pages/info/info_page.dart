import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/pages/Dialog/dialog_confirm_view.dart';
import 'package:customer/pages/info/info_controller.dart';
import 'package:customer/pages/info/info_detail/info_detail_page.dart';
import 'package:customer/pages/list_order/list_order_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:customer/base/ui/text/text_ui.dart';

import 'infoVM.dart';

class InfoPage extends BaseView<InfoVM, InfoController> {
  final _formOldPassKey = GlobalKey<FormState>();
  final _formNewPassKey = GlobalKey<FormState>();
  final _formConfirmPassKey = GlobalKey<FormState>();

  @override
  Widget body(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: Container(
          child: Column(
            children: <Widget>[
              Obx(() =>
                  Row(
                    children: <Widget>[
                      Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child:
                        Image.network(
                          viewModel?.partnerDto?.value?.uri_path ?? '',
                          fit: BoxFit.fill,
                          // height: 100,
                          // width: 100,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                viewModel?.partnerDto?.value?.name ?? 'No name',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              Text(
                                viewModel?.partnerDto?.value?.street ?? 'No Home',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                child: context.divider(),
              ),
              context.textOnTap(
                'Info',
                onTap: () {
                  _gotoPage(context, 0);
                },
              ),
              context.textOnTap(
                'Info_event.Promotion_point'.tr,
                onTap: () {
                  _gotoPage(context, 1);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.List_order'.tr,
                onTap: () {
                  _gotoPage(context, 2);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.Setting'.tr,
                onTap: () {
                  _gotoPage(context, 3);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.Wallet'.tr,
                onTap: () {
                  _gotoPage(context, 4);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.Change_password'.tr,
                onTap: () {
                  _gotoPage(context, 5);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.Support_center'.tr,
                onTap: () {
                  _gotoPage(context, 6);
                },
              ),
              context.divider(),
              context.textOnTap(
                'Info_event.Logout'.tr,
                onTap: () {
                  _gotoPage(context, 7);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _gotoPage(BuildContext context, int index) {
    switch (index) {
      case 0:
        {
          final result = Get.to(() => InfoDetailPage());
        }
        break;
      case 1:
        {
          // Get.to()
        }
        break;
      case 2:
        {
          Get.to(() => ListOrderPage());
        }
        break;
      case 3:
        {}
        break;
      case 4:
        {

        }
        break;
      case 5:
        {
          _showMaterialDialog(context);
        }
        break;
      case 6:
        {

        }
        break;
      case 7:
        {
          DialogConfirm dialogConfirm = DialogConfirm('Info_event.Logout'.tr, 'Do you want logout',true,()=>controller.logout());
          dialogConfirm.showMaterialDialog(context);
        }
        break;
    }
  }

  @override
  InfoController putController() {
    return InfoController(InfoVM());
  }

  _showMaterialDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) =>
        new AlertDialog(
          title: Container(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Change password',style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.normal),),
                IconButton(icon: Icon(Icons.clear), onPressed: ()=>{Navigator.of(context).pop()})
              ],
            )
          ),
          content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.3,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 0.6,
                      height: 40,
                      child: Form(
                          key: _formOldPassKey,
                          child: TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                                labelText: 'Old password'),
                            onChanged: (text) => {},
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Not empty';
                              }
                              return null;
                            },
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 0.6,
                      height: 40,
                      child: Form(
                          key: _formNewPassKey,
                          child: TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                                labelText: 'New password'),
                            onChanged: (text) => {},
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Not empty';
                              }
                              return null;
                            },
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 0.6,
                      height: 40,
                      child: Form(
                          key: _formConfirmPassKey,
                          child: TextFormField(
                            obscureText: true,
                            decoration:
                            InputDecoration(labelText: 'Confirm new password'),
                            onChanged: (text) => {},
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Not empty';
                              }
                              return null;
                            },
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: () => {Navigator.of(context).pop()},
                          child: Container(
                            child: Center(
                                child: Text(
                                  'Update',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal),
                                )),
                            width: MediaQuery
                                .of(context)
                                .size
                                .width * 0.25,
                            height: 40,
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                        //   child: ElevatedButton(
                        //     onPressed: () => {Navigator.of(context).pop()},
                        //     child: Container(
                        //       child: Center(
                        //           child: Text(
                        //             'Yes',
                        //             style: TextStyle(
                        //                 color: Colors.black,
                        //                 fontSize: 14,
                        //                 fontWeight: FontWeight.normal),
                        //           )),
                        //       width:
                        //       MediaQuery.of(context).size.width * 0.25,
                        //       height: 40,
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  )
                ],
              )),
        ));
  }
}
