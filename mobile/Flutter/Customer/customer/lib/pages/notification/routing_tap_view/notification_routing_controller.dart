import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/enum/notification_type.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/notification/notification_detail_page.dart';
import 'package:get/get.dart';

import '../notification_vm.dart';


class NotificationRoutingController extends BaseController<NotificationVM>{
  NotificationRoutingController(NotificationVM viewModel) : super(viewModel){
    viewModel.listNotificationRoutingController =
        ListController(loadFunc: this.getNotifications);
  }
  final inDex = 0.obs;

  details(notificationDto) {
    //goto page
  }

  seenNotification(int id) async{
    Map<String, dynamic> param = {
      "id": id,
    };
  }
  Future<PagedListData<NotificationDto>> getNotifications(int page) async{
    Map<String, dynamic> params = {
      "type": NotificationType.ROUTING,
      "offset": (page-1)*10,
    };
    final data=await viewModel.repository.getNotifications(params);
    return data;
  }

}