import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/pages/import_export_warehouse/import_export_page.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_page.dart';
import 'package:customer/pages/routing_day/routing_day_detail/routing_day_detail_vm.dart';
import 'package:get/get.dart';

class RoutingDayDetailController extends BaseController<RoutingDayDetailVM> {
  RoutingDayDetailController(RoutingDayDetailVM vm) : super(vm);

  Future<BillRoutingDto> getBillRoutingDetail() async {
    Map<String, dynamic> param = {
      "bill_routing_id": viewModel.billRoutingId,
    };
    final response = await viewModel.repository.getBillRoutingDetailDto(param);
    return response;
  }

  gotoPage(billRoutingDto) {
      Get.to(() => BillLadingDetailPage(billRoutingDto.from_bill_lading_id));
  }
}
