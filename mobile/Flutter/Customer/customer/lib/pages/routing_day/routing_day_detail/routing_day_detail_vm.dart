import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/pages/routing_day/api/routing_day_repository.dart';
import 'package:get/get.dart';

class RoutingDayDetailVM extends BaseVM{
      int billRoutingId=0;
      final repository= RoutingDayRepository();
      Rx<BillRoutingDto> billRoutingDto= Rx(BillRoutingDto());
      RxBool isLoading= RxBool(false);
}