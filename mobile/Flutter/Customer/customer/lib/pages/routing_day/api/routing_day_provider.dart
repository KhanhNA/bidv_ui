import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/bill_routing/BillRoutingDetailDto.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:flutter/cupertino.dart';

class RoutingDayProvider {
  final String GET_ROUTING_DAY_DETAIL ='share_van_order/get_bill_routing_detail';
  final String GET_ROUTING_DAY ='share_van_order/get_bill_routing_by_day';
  ApiAuthProvider api = ApiAuthProvider();

  Future<PagedListData<BillRoutingDto>> getBillRoutingDto(Map<String, dynamic> params) async {
    try {
      final response = await api.getListJsonParams<BillRoutingDto>(
          GET_ROUTING_DAY,
          params,
          (js) => BillRoutingDto.fromJson(js));
      // when
      return response;
    } catch (error, stacktrace) {
      _printError(error, stacktrace);
      return PagedListData.withError('$error');
    }
  }
  Future<BillRoutingDto> getBillRoutingDetailDto(Map<String, dynamic> params) async {
      final response = await api.getOne(GET_ROUTING_DAY_DETAIL, params, (js) =>
          BillRoutingDto.fromJson(js));
      return response;
  }

  void _printError(error, StackTrace stacktrace) {
    debugPrint('error: $error & stacktrace: $stacktrace');
  }
}
