import 'package:customer/model/bill_routing/BillRoutingDetailDto.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/model/list/PagedListData.dart';

import 'routing_day_provider.dart';

class RoutingDayRepository{
  final RoutingDayProvider _api = RoutingDayProvider();

  Future<PagedListData<BillRoutingDto>> getBillRoutingDto(Map<String, dynamic> params) =>
      _api.getBillRoutingDto(params);
  Future<BillRoutingDto> getBillRoutingDetailDto(Map<String, dynamic> params) =>
      _api.getBillRoutingDetailDto(params);
}