import 'package:customer/model/bill_routing/BillRoutingDetailDto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'routing_day_map_controller.dart';
import 'routing_day_map_vm.dart';

class RoutingDayMapPage extends GetView<RoutingDayMapController> {
  final viewModel = RoutingDayMapVM();
  BillRoutingDetailDto billRoutingDetailDto;
  RoutingDayMapPage(this.billRoutingDetailDto);

  int id=0;
  GoogleMapController mapController;

  // addMarker(){
  _add() {
    final MarkerId markerId = MarkerId((++id).toString());
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(21.012146,105.779425+id/10000),
      infoWindow: InfoWindow(title: 'DXT', snippet: '*'),
      onTap: () {
        print('Click Marker');
      },
    );
    viewModel.listMarker.value[markerId] = marker;
    viewModel.listMarker.refresh();
  }
  _updatePosition(LatLng currentPosition) {
    Marker marker = viewModel.listMarker.value["1"];
    marker.copyWith(positionParam: LatLng(currentPosition.latitude, currentPosition.longitude));
    viewModel.listMarker.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      constraints: BoxConstraints.expand(),
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Obx(() => GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(20.9740415,105.0921716),
                  zoom: 40.0
                ),
                myLocationEnabled: true,
                onMapCreated: (GoogleMapController mapController) {
                  this.mapController = mapController;
                },
                markers: Set<Marker>.of(viewModel.listMarker.value.values),
              )),
          Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: Column(
                children: <Widget>[
                  AppBar(
                    title: Text(
                      'Routing day map',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.create_new_folder),
                      onPressed: () => _add())
                ],
              )),
        ],
      ),
    ));
  }
}
