import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';
import 'package:customer/base/controller/BaseZone.dart';

import 'routing_day_detail/routing_day_detail_page.dart';
import 'routing_day_vm.dart';


class RoutingDayController extends BaseController<RoutingDayVM> {

  RoutingDayController(RoutingDayVM vm) : super(vm) {
    vm.initData();
    viewModel.listController =
        ListController(loadFunc: this.loadDataRoutingDay);
  }

  Future<PagedListData<BillRoutingDto>> loadDataRoutingDay(int page) async {
    Map<String, dynamic> param = {
      "date": '2021-04-05',
      // "date": viewModel.date,
      "offset": (page-1)*10,
      "limit": 10
    };
    final data = await viewModel.repository.getBillRoutingDto(param);
    return data;
  }
  details(billRoutingDto) {
    viewModel.billRoutingDto = billRoutingDto;
    final result = Get.to(()=>RoutingDayDetailPage(viewModel.billRoutingDto));
  }
}
