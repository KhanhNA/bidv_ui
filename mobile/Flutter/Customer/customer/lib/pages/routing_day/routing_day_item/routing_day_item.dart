import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:get/get.dart';

import '../routing_day_controller.dart';
import 'routing_day_item_controller.dart';




class RoutingDayItem extends GetView<RoutingDayItemController> {
  // final ImportStatement importStatement;
  final BuildContext context;
  final BillRoutingDto billRoutingDto;
  final int index;
  final RoutingDayController parentController;
  RoutingDayItemController controller;
  RoutingDayItem(this.context, this.billRoutingDto, this.index, this.parentController){
    controller = RoutingDayItemController();
    controller.listController= ListController(
        loadFunc: (page) async => PagedListData.withContent(
            billRoutingDto.arrBillLadingDetail));
  }



  @override
  Widget build(BuildContext context) {

    List<Widget> listWidgets =  billRoutingDto.arrBillLadingDetail.map((e)
    {
      return Container(
        width: MediaQuery.of(context).size.width*0.9,
        child: Column(
          children: [
            context.listItemViewV2('address', e?.address??'Abc'),
          ],
        ),
      );
    }).toList();

    return CommonUI.card(
      child: Column(
        children: [
          context.listItemHeader(
            (billRoutingDto?.code ?? ''),
            context.textDrawableStart(
                "viewDetail".tr,
                CustomIcons.fromName('FontAwesome.0xf105'),
                Theme.of(context).primaryColor,
                isStart: false),
            onTap: () {
              parentController.details(billRoutingDto);
            },
          ),
          context.divider(),
          Container(
            width: double.infinity,
            child: Wrap(
              children: listWidgets,
              )
          )
        ],
      ),
    );
  }
}
