import 'dart:math';

import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/AreaDistanceDto.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:customer/model/order_package/order_package_type.dart';
import 'package:customer/model/product_type/ProductTypeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';
import 'package:customer/base/ui/common_ui.dart';

import 'create_order_vm.dart';

class CreateOrderController extends BaseController<CreateOrderVM> {
  CreateOrderController(CreateOrderVM viewModel) : super(viewModel) {
    viewModel.warehouseListController = ListController(loadFunc: getWarehouse);
    viewModel.serviceListController = ListController(loadFunc: getBillService);
    viewModel.subscribeListController = ListController(loadFunc: getSubscribe);
    viewModel.insuranceListController = ListController(loadFunc: getInsurance);
    viewModel.orderPackageListController =
        ListController(loadFunc: getOrderPackage);
    getProductType();
    viewModel.warehouseListController.refresh();
    viewModel.serviceListController.refresh();
    viewModel.insuranceListController.refresh();
    viewModel.subscribeListController.refresh();
    for(int i=1;i<32;i++){
      viewModel.listDate.value.add(i);
    }
  }

  Future<PagedListData<WarehouseDto>> getWarehouse(int page) async {
    Map<String, dynamic> params = {
      'companyId': AppUtils.partnerDto.company_id,
    };
    final data = await viewModel.repository.getWarehouse(params);
    return data;
  }

  Future<PagedListData<BillServiceDto>> getBillService(int page) async {
    Map<String, dynamic> params = {
      'status': 'running',
    };
    final data = await viewModel.repository.getBillService(params);
    return data;
  }

  // Handle Step 1
  getProductType() async {
    Map<String, dynamic> params = {
      'status': 'running',
    };
    final data = await viewModel.repository.getProductType(params);
    viewModel.productTypeList.value.addAll(data.content);
    for (int i = 1; i < viewModel.productTypeList.length; i++) {
      viewModel.productTypeList.value[i].checked = false;
    }
    viewModel.productTypeList.value[0].checked = true;
  }


  Future<bool> addPackage() async {
    if (await checkProductType()) {
      viewModel.billPackageDto.value.productType =
      viewModel.productTypeList.value[viewModel.indexSelectedTemp];

      // convert cm to m
      viewModel.billPackageDto.value.length =
          viewModel.billPackageDto.value.length / 100.0;
      viewModel.billPackageDto.value.width =
          viewModel.billPackageDto.value.width / 100.0;
      viewModel.billPackageDto.value.height =
          viewModel.billPackageDto.value.height / 100.0;
      // calculate net weight
      viewModel.billPackageDto.value.net_weight = max(
          viewModel.billPackageDto.value.calculateNetWeight(),
          viewModel.billPackageDto.value.weight);
      viewModel.billPackageDto.value.createOrderId = viewModel.createOrderId;
      viewModel.exportBillLadingDetail.value.billPackages.add(
          viewModel.billPackageDto.value);
      //refresh data
      viewModel.createOrderId++;
      viewModel.exportBillLadingDetail.refresh();
      viewModel.indexSelectedTemp = 0;
      viewModel.billPackageDto.value = BillPackageDto();
      return true;
    }
    return false;
  }

  Future<bool> addBillLadingDetail() async {
    if (await validateStep2()) {
      viewModel.listBillPackage.value.forEach((e) {
        if (e.checked && e.quantity_package > 0) {
          if (e.quantity_package ==
              viewModel.hashMapQuantity[e.createOrderId]) {
            viewModel.hashMapQuantity.remove(e.createOrderId);
          } else {
            viewModel.hashMapQuantity[e.createOrderId] =
                viewModel.hashMapQuantity[e.createOrderId] - e.quantity_package;
          }
          viewModel.importBillLadingDetail.value.billPackages.add(e);
        }
      });
      viewModel.importBillLadingDetail.value.warehouse_type = '1';
      await getAreaDistances(2);
      setCapacity(
          viewModel.importBillLadingDetail.value, false);
      viewModel.billLadingDto.value.arrBillLadingDetail
          .add(viewModel.importBillLadingDetail.value);
      viewModel.importBillLadingDetail.value = BillLadingDetailDto();
      return true;
    }
    return false;
  }

  Future<bool> checkProductType() async {
    if (viewModel.exportBillLadingDetail.value.billPackages.length == 0) {
      return true;
    }
    Map<String, dynamic> params = {
      'currentProductType':
      viewModel.exportBillLadingDetail.value.billPackages[0].productType
          .name_seq,
      'newProductType':
      viewModel.productTypeList.value[viewModel.indexSelectedTemp].name_seq,
    };
    final data = await viewModel.repository.checkProductType(params);
    return data;
  }

  Future<bool> validateStep2() async {
    return true;
  }

  Future<bool> validateStep1() async {
    if (viewModel.exportBillLadingDetail.value.billPackages.length == 0) {
      return false;
    }
    viewModel.exportBillLadingDetail.value.warehouse_type = '0';
    await getAreaDistances(1);
    setCapacity(
        viewModel.exportBillLadingDetail.value, true);
    // render data step 2
    viewModel.quantity_package= 0.0.obs;
    viewModel.billLadingDto.value.total_weight = 0;
    viewModel.billLadingDto.value.total_volume = 0;
    viewModel.exportBillLadingDetail.value.billPackages
        .forEach((billPackageDto) {

      viewModel.quantity_package+= billPackageDto.quantity_package;
      viewModel.billLadingDto.value.total_weight+= billPackageDto.net_weight;
      viewModel.billLadingDto.value.total_volume+= billPackageDto.getTotalVol();
      viewModel.hashMapBillPackage[billPackageDto.createOrderId] =
          billPackageDto;
      viewModel.hashMapQuantity[billPackageDto.createOrderId] =
          billPackageDto.quantity_package;
    });
    viewModel.listBillPackage.clear();
    viewModel.listBillPackage.value
        .addAll(viewModel.exportBillLadingDetail.value.billPackages);


    if (viewModel.billLadingDto.value.arrBillLadingDetail.length == 0) {
      viewModel.billLadingDto.value.arrBillLadingDetail
          .add(viewModel.exportBillLadingDetail.value);
    } else {
      viewModel.billLadingDto.value.arrBillLadingDetail[0] =
          viewModel.exportBillLadingDetail.value;
    }
    return true;
  }

  setCapacity(BillLadingDetailDto billLadingDetail, bool isCapacityBill) {
    double total_weight = 0;
    int totalParcel = 0;
    double totalVol = 0;
    billLadingDetail.billPackages.forEach((value) {
      total_weight += value.net_weight * value.quantity_package;
      totalParcel += value.quantity_package;
      totalVol += value.getTotalVol();
    });
    if (isCapacityBill) {
      viewModel.exportBillLadingDetail.value.total_weight = total_weight;
      viewModel.exportBillLadingDetail.value.total_parcel = totalParcel;
      viewModel.exportBillLadingDetail.value.area_id =
          billLadingDetail.warehouse.areaInfo.id;
      viewModel.exportBillLadingDetail.value.total_volume = totalVol;
      viewModel.billLadingDto.value.total_weight = total_weight;
      viewModel.billLadingDto.value.total_volume = totalVol;
      viewModel.billLadingDto.value.total_parcel = totalParcel;
    } else {
      viewModel.importBillLadingDetail.value.total_weight = total_weight;
      viewModel.importBillLadingDetail.value.total_parcel = totalParcel;
      viewModel.importBillLadingDetail.value.area_id =
          billLadingDetail.warehouse.areaInfo.id;
      viewModel.importBillLadingDetail.value.total_volume = totalVol;
    }
  }

  getAreaDistances(int step) async {
    WarehouseDto fromWarehouse;
    WarehouseDto toWarehouse;
    fromWarehouse = viewModel.exportBillLadingDetail.value.warehouse;
    if (step != 1) {
      toWarehouse = viewModel.importBillLadingDetail.value.warehouse;
    }

    Map<String, dynamic> params = {
      'from_warehouse': fromWarehouse.toJson(),
      'to_warehouse': toWarehouse?.toJson() ?? null,
    };
    final data = await viewModel.repository.getAreaDistances(params);
    if (step == 1) {
      viewModel.exportBillLadingDetail.value.areaDistances.clear();
      viewModel.exportBillLadingDetail.value.areaDistances.addAll(data.content);
    } else {
      viewModel.importBillLadingDetail.value.areaDistances.addAll(data.content);
    }
  }

  Future<PagedListData<SubscribeDto>> getSubscribe(int step) async {
    Map<String, dynamic> params = {
      'status': 'running',
    };
    final data = await viewModel.repository.getSubscribe(params);
    return data;
  }
  Future<PagedListData<InsuranceDto>> getInsurance(int step) async {
    Map<String, dynamic> params = {
      'status': 'running',
    };
    final data = await viewModel.repository.getInsurance(params);
    return data;
  }

  Future<PagedListData<OrderPackageDto>> getOrderPackage(int step) async {
    Map<String, dynamic> params = {};
    final data = await viewModel.repository.getOrderPackage(params);
    await handleStep3();
    return data;
  }

  Future<BillLadingDto> calculatorPrice() async {
    final data = await viewModel.repository.calculatorPrice(
        viewModel.billLadingDto.value);
    viewModel.billLadingDto.value.total_amount =data.total_amount;
    viewModel.billLadingDto.value.price_not_discount = data.price_not_discount;
    viewModel.billLadingDto.value.service_price = data.service_price;
    viewModel.billLadingDto.value.insurance_price = data.insurance_price;
    return data;
  }

  handleStep3() async {
    await viewModel.orderPackageListController.toList().forEach((e) {
      if (OrderPackageType.NORMAL ==e.type) {
        viewModel.billLadingDto.value.order_package =e;
      }
    });
  }

  createOrder() async{
    viewModel.billLadingDto.value.company_id =  AppUtils.partnerDto.company_id;
    final data =await viewModel.repository.createOrder(viewModel.billLadingDto.value);

  }

}
