import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/create_order/create_order_controller.dart';
import 'package:customer/pages/create_order/create_order_vm.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class CreateOrderStep1 extends BaseView<CreateOrderVM, CreateOrderController> {
  final _formPhoneKey = GlobalKey<FormState>();
  final _formPackageNameKey = GlobalKey<FormState>();
  final _formPackageLengthKey = GlobalKey<FormState>();
  final _formPackageHeightKey = GlobalKey<FormState>();
  final _formPackageWidthKey = GlobalKey<FormState>();
  final _formPackageWeightKey = GlobalKey<FormState>();
  final _formPackageQuantityKey = GlobalKey<FormState>();

  @override
  Widget body(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () => _selectWarehouse(context),
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black12),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Obx(() => context.normalText(
                          viewModel?.exportBillLadingDetail?.value?.warehouse
                                  ?.name ??
                              'Choose warehouse',
                        )),
                    Icon(
                      Icons.keyboard_arrow_down_outlined,
                      size: 24,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
            child: Container(
              height: 40,
              child: Form(
                key: _formPhoneKey,
                child: Obx(() => TextFormField(
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 13),
                      controller: TextEditingController(
                          text: viewModel?.exportBillLadingDetail?.value
                                  ?.warehouse?.phone ??
                              ''),
                      decoration: InputDecoration(labelText: 'Phone Number'),
                      onChanged: (text) => {},
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
            )),
        Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Container(
              child: Row(
            children: <Widget>[
              context.normalText('accompanying service'),
              IconButton(
                  onPressed: () => _selectService(context),
                  icon: Icon(
                    Icons.add_business_outlined,
                    color: Colors.green,
                  )),
            ],
          )),
        ),
        Obx(
          () => viewModel.exportBillLadingDetail.value.billService.length == 0
              ? Container()
              : listServiceSelected(context),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: context.divider(),
        ),
        Expanded(
          child: Container(
              child: Obx(
            () =>
                viewModel.exportBillLadingDetail.value.billPackages.length == 0
                    ? _notProduct(context)
                    : listPackageCreated(context),
          )),
        ),
        ElevatedButton(
            onPressed: () async {
              if (await controller.validateStep1()) {
                // controller.renderDataStep2();
                DefaultTabController.of(context).animateTo(1);
              }
            },
            child: Container(child: Center(child: Text('Next step'))))
      ],
    ));
  }

  Widget listPackageCreated(BuildContext context) {
    List<Widget> listWidgets =
        viewModel.exportBillLadingDetail.value.billPackages.toList().map((e) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Column(children: <Widget>[
                context.listItemViewV2(
                    'Package name', e.item_name ?? 'No name'),
                context.listItemViewV2(
                    'Package type', e.productType?.name ?? 'Normal'),
                context.listItemViewV2(
                    'Quantity', e.quantity_package.toString() ?? '0'),
                context.listItemViewV2(
                    'Size',
                    (e.length.toString() + ' cm' ?? '0 cm') + ' - ' +( e.width.toString() +' cm'??
                        '0 cm') + ' - ' + (e.height.toString()+' cm' ??
                        '0 cm')),
                context.listItemViewV2(
                    'Weight', e.net_weight.toString()+' kg' ?? '0 +kg'),
              ]),
            )),
      );
    }).toList();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        InkWell(
          onTap: () => addPackage(context),
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Container(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.4,
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(8)),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'Add new package',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 13),
                        ),
                        Icon(
                          Icons.add_chart,
                          size: 24,
                          color: Colors.white,
                        ),
                      ]),
                )),
          ),
        ),
        Wrap(
          children: listWidgets,
        ),
      ],
    );
  }

  Widget listServiceSelected(BuildContext context) {
    List<Widget> listWidgets =
        viewModel.exportBillLadingDetail.value.billService.toList().map((e) {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Center(child: context.normalTextOneLine(e.name)),
            )),
      );
    }).toList();
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
      child: Container(
        height: 25,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: listWidgets,
        ),
      ),
    );
  }

  Widget _notProduct(BuildContext context) {
    return CommonUI.card(
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/truck.png',
                height: 60, width: 60, fit: BoxFit.fill),
            ElevatedButton(
              onPressed: () => addPackage(context),
              child: Center(child: context.normalText('Add new package')),
            )
          ],
        ),
      ),
    );
  }

  addPackage(BuildContext context) {
    showModalBottomSheet<void>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      context.titleText('Add new package'),
                      IconButton(
                          onPressed: () => Navigator.pop(context),
                          icon: Icon(Icons.clear))
                    ],
                  )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 40,
                      child: Form(
                        key: _formPackageNameKey,
                        child: Obx(() => context.formInput(
                                viewModel.billPackageDto?.value?.item_name ??
                                    '',
                                'item name', (value) {
                              viewModel.billPackageDto?.value?.item_name =
                                  value;
                            })),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: context.normalText('Select product type'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
                    child: Container(
                      height: 30,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: viewModel.productTypeList.length,
                        itemBuilder: (_, i) {
                          return Padding(
                            padding:
                                const EdgeInsets.only(left: 4.0, right: 4.0),
                            child: InkWell(
                                onTap: () => {
                                      viewModel
                                          .productTypeList
                                          .value[viewModel.indexSelectedTemp]
                                          .checked = false,
                                      viewModel.indexSelectedTemp = i,
                                      viewModel.productTypeList.value[i]
                                          .checked = true,
                                      viewModel.productTypeList.refresh()
                                    },
                                child: Obx(
                                  () => Container(
                                    height: 30,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: viewModel.productTypeList
                                                .value[i].checked
                                            ? Colors.green
                                            : Colors.white,
                                        border:
                                            Border.all(color: Colors.black12),
                                        borderRadius: BorderRadius.circular(6)),
                                    child: Center(
                                        child: context.normalText(viewModel
                                            .productTypeList.value[i].name)),
                                  ),
                                )),
                          );
                        },
                        // scrollDirection: Axis.horizontal,
                        // children: listWidgets,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
                    child: Container(
                      height: 40,
                      child: Form(
                        key: _formPackageQuantityKey,
                        child: Obx(() => context.formInput(
                                viewModel
                                        .billPackageDto?.value?.quantity_package
                                        .toString() ??
                                    '',
                                'Quantity', (value) {
                              viewModel.billPackageDto?.value
                                  ?.quantity_package = int.parse(value);
                            }, inputType: TextInputType.number)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 4),
                            child: Container(
                              height: 40,
                              child: Form(
                                key: _formPackageLengthKey,
                                child: Obx(() => context.formInput(
                                    viewModel.billPackageDto?.value?.length
                                            .toString() ??
                                        '',
                                    'Length',
                                    (value) => {
                                          viewModel.billPackageDto?.value
                                              ?.length = double.parse(value),
                                        },
                                    suffixWidget: context.normalText('cm'),
                                    inputType: TextInputType.number)),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 4, right: 8),
                            child: Container(
                              height: 40,
                              child: Form(
                                key: _formPackageHeightKey,
                                child: Obx(() => context.formInput(
                                    viewModel.billPackageDto?.value?.height
                                            .toString() ??
                                        '',
                                    'Height',
                                    (value) => {
                                          viewModel.billPackageDto?.value
                                              ?.height = double.parse(value),
                                        },
                                    suffixWidget: context.normalText('cm'),
                                    inputType: TextInputType.number)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 4),
                            child: Container(
                              height: 40,
                              child: Form(
                                key: _formPackageWidthKey,
                                child: Obx(() => context.formInput(
                                    viewModel.billPackageDto?.value?.width
                                            .toString() ??
                                        '',
                                    'Width',
                                    (value) => {
                                          viewModel.billPackageDto?.value
                                              ?.width = double.parse(value),
                                        },
                                    suffixWidget: context.normalText('cm'),
                                    inputType: TextInputType.number)),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 4.0, right: 8),
                            child: Container(
                              height: 40,
                              child: Form(
                                key: _formPackageWeightKey,
                                child: Obx(() => context.formInput(
                                    viewModel.billPackageDto?.value?.weight
                                            .toString() ??
                                        '',
                                    'Weight',
                                    (value) => {
                                          viewModel.billPackageDto?.value
                                              ?.weight = double.parse(value),
                                        },
                                    suffixWidget: context.normalText('kg'),
                                    inputType: TextInputType.number)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        if (await validateForm() &&
                            await controller.addPackage()) {
                          Navigator.pop(context);
                        } else {
                          Fluttertoast.showToast(
                              msg: "This product type conflict",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              // timeInSecForIos: 1
                              backgroundColor: Colors.black12,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: 40,
                          child:
                              Center(child: context.normalText('add package'))))
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<bool> validateForm() async {
    bool check = true;
    if (!_formPackageHeightKey.currentState.validate()) {
      check = false;
    }
    if (!_formPackageLengthKey.currentState.validate()) {
      check = false;
    }
    if (!_formPackageNameKey.currentState.validate()) {
      check = false;
    }
    if (!_formPackageQuantityKey.currentState.validate()) {
      check = false;
    }
    if (!_formPackageWeightKey.currentState.validate()) {
      check = false;
    }
    if (!_formPackageWidthKey.currentState.validate()) {
      check = false;
    }
    return check;
  }

  @override
  CreateOrderController putController() {
    return CreateOrderController(CreateOrderVM());
  }

  _selectWarehouse(BuildContext context) {
    context.showBottomSheet<WarehouseDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Select Warehouse').tr),
        ), (context, item, index) {
      return InkWell(
        onTap: () => {
          viewModel.exportBillLadingDetail.value.warehouse = item,
          viewModel.exportBillLadingDetail.value.latitude = item.latitude,
          viewModel.exportBillLadingDetail.value.longitude = item.longitude,
          viewModel.exportBillLadingDetail.value.wjson_address =
              item.wjson_address,
          viewModel.exportBillLadingDetail.value.order_type = '0',
          viewModel.exportBillLadingDetail.value.address = item.address,
          viewModel.exportBillLadingDetail.value.zone_area_id =
              item.areaInfo.zoneInfo.id,
          viewModel.exportBillLadingDetail.refresh(),
          Navigator.pop(context)
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/warehouse.png',
                      height: 23,
                      width: 23,
                      fit: BoxFit.fill,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          context.titleText(item.name),
                          context.normalText(item.phone),
                          context.normalText(item.address),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: context.divider(),
                )
              ],
            ),
          ),
        ),
      );
    }, viewModel.warehouseListController);
  }

  _selectService(BuildContext context) {
    context.showBottomSheet<BillServiceDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Select Service').tr),
        ), (context, item, index) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            child: InkWell(
                onTap: () => {
                      viewModel.exportBillLadingDetail.value.billService
                          .add(item),
                      viewModel.exportBillLadingDetail.refresh(),
                      Navigator.pop(context)
                    },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Column(
                    children: [
                      Container(
                        height: 40,
                        width: double.infinity,
                        child: Container(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              context.normalText(item.name),
                              context.normalText(item.price.toString(),
                                  color: Colors.green),
                            ])),
                      ),
                      context.divider()
                    ],
                  ),
                ))),
      );
    }, viewModel.serviceListController);
  }
}
