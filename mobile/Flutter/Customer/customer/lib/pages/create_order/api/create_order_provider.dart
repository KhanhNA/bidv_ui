import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/AreaDistanceDto.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:customer/model/product_type/ProductTypeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';

class CreateOrderProvider{
  final String GET_WAREHOUSE_URI = 'share_van_order/get_warehouse';
  final String CHECK_PRODUCT_TYPE_URI = 'sharevan/check_product_type';
  final String GET_BILL_SERVICE_URI = 'share_van_order/service/list_active';
  final String GET_PRODUCT_TYPE_URI = 'share_van_order/product_type/list_active/';
  final String GET_AREA_DISTANCES = 'share_van_order/get_area_distance';
  final String GET_SUBSCRIBE = 'share_van_order/subscribe/list';
  final String GET_INSURANCE = 'share_van_order/insurance/list_active';
  final String GET_ORDER_PACKAGE = 'share_van_order/get_bill_order_package';
  final String GET_PRICE_BILL = 'app/v1/price/calc/bill';
  final String CREATE_ORDER = 'share_van_order/create_order/';
  ApiAuthProvider api = ApiAuthProvider();
  Future<PagedListData<WarehouseDto>> getWarehouse(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_WAREHOUSE_URI, params, (js) => WarehouseDto.fromJson(js));
    return data;
  }
  Future<PagedListData<BillServiceDto>> getBillService(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_BILL_SERVICE_URI, params, (js) => BillServiceDto.fromJson(js));
    return data;
  }
  Future<PagedListData<ProductTypeDto>> getProductType(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_PRODUCT_TYPE_URI, params, (js) => ProductTypeDto.fromJson(js));
    return data;
  }
  Future<PagedListData<AreaDistanceDto>> getAreaDistances(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_AREA_DISTANCES, params, (js) => AreaDistanceDto.fromJson(js));
    return data;
  }
  Future<PagedListData<SubscribeDto>> getSubscribe(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_SUBSCRIBE, params, (js) => SubscribeDto.fromJson(js));
    return data;
  }
  Future<PagedListData<InsuranceDto>> getInsurance(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_INSURANCE, params, (js) => InsuranceDto.fromJson(js));
    return data;
  }
  Future<PagedListData<OrderPackageDto>> getOrderPackage(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_ORDER_PACKAGE, params, (js) => OrderPackageDto.fromJson(js));
    return data;
  }
  Future<bool> checkProductType(Map<String,dynamic> params) async {
    final data = await api.jsonCheck(CHECK_PRODUCT_TYPE_URI, params);
    return data;
  }
  Future<BillLadingDto> calculatorPrice(BillLadingDto billLadingDto) async {
    final data = await api.calculatorPrice(billLadingDto);
    return data;
  }
  Future<double> createOrder(BillLadingDto billLadingDto) async {
    final data = await api.createOrder(CREATE_ORDER,billLadingDto);
    return data;
  }
}