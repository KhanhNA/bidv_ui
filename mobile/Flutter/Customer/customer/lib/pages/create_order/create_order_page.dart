import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'create_order_controller.dart';
import 'step1/create_order_step1.dart';
import 'step2/create_order_step2.dart';
import 'step3/create_order_step3.dart';
import 'create_order_vm.dart';

class CreateOrderPage extends BaseView<CreateOrderVM, CreateOrderController> {
  final List<Widget> listTab = <Widget>[];

  CreateOrderPage() {
    listTab.addAll([
      CreateOrderStep1(),
      CreateOrderStep2(),
      CreateOrderStep3(),
    ]);
  }

  @override
  Widget body(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: AppTheme.white,
                child: TabBar(
                  tabs: viewModel.createOrderStep
                      .map((e) => Tab(text: (e).tr))
                      .toList(),
                ),
              ),
            ),
            Expanded(
              child: TabBarView(children: listTab),
            )
          ],
        ));
  }

  @override
  putController() {
    return CreateOrderController(CreateOrderVM());
  }
}
