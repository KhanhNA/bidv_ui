import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/create_order/create_order_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../create_order_vm.dart';

class SelectImportWarehouse
    extends BaseView<CreateOrderVM, CreateOrderController> {
  final _formPhoneKey = GlobalKey<FormState>();
  final _forListBillPackageKey = GlobalKey<FormState>();

  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(
      title: Text('Select warehouse import'),
    );
  }

  @override
  Widget body(BuildContext context) {
    return Form(
      key: _forListBillPackageKey,
      autovalidateMode: AutovalidateMode.always,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () => _selectWarehouse(context),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.black12),
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Obx(() => context.normalText(
                            viewModel?.importBillLadingDetail?.value?.warehouse
                                    ?.name ??
                                'Choose warehouse',
                          )),
                      Icon(
                        Icons.keyboard_arrow_down_outlined,
                        size: 24,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
              child: Container(
                height: 40,
                child: Form(
                  key: _formPhoneKey,
                  child: Obx(() => TextFormField(
                        // initialValue: '0987654321',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 13),
                        controller: TextEditingController(
                            text: viewModel?.importBillLadingDetail?.value
                                    ?.warehouse?.phone ??
                                ''),
                        decoration: InputDecoration(labelText: 'Phone Number'),
                        onChanged: (text) => {},
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Not empty';
                          }
                          return null;
                        },
                      )),
                ),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Container(
                child: Row(
              children: <Widget>[
                context.normalText('accompanying service'),
                IconButton(
                    onPressed: () => _selectService(context),
                    icon: Icon(
                      Icons.add_business_outlined,
                      color: Colors.green,
                    )),
              ],
            )),
          ),
          Obx(
                () => viewModel.exportBillLadingDetail.value.billService.length == 0
                ? Container()
                : listServiceSelected(context),
          ),
          context.divider(),
          Expanded(
              child: ListView.builder(
                  itemCount: viewModel.listBillPackage.value.length,
                  itemBuilder: (_, index) {
                    return Obx(() => Padding(
                          padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black12),
                                  borderRadius: BorderRadius.circular(8)),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Column(children: <Widget>[
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(4, 2, 4, 2),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              'Package name',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize: 12),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: Text(
                                                viewModel
                                                    .listBillPackage.value[index]
                                                        .item_name ??
                                                    'No name',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    // fontWeight: FontWeight.normal,
                                                    fontSize: 13),
                                              ),
                                            )
                                          ],
                                        ),
                                        Checkbox(
                                            value: viewModel.listBillPackage.value[index].checked,
                                            onChanged: (value) => {
                                                  viewModel
                                                      .listBillPackage.value[index]
                                                      .checked = value,
                                                  viewModel.listBillPackage.refresh(),
                                                })
                                      ],
                                    ),
                                  ),
                                  context.listItemViewV2(
                                      'Package type',
                                      viewModel.listBillPackage.value[index]
                                              .productType?.name ??
                                          'Normal'),
                                  context.listItemViewV2(
                                      'Size',
                                      viewModel.listBillPackage.value[index]
                                              .length
                                              .toString()+' cm' +
                                          ' - ' +
                                          viewModel.listBillPackage.value[index]
                                              .width
                                              .toString()+' cm' +
                                          ' - ' +
                                          viewModel.listBillPackage.value[index]
                                              .height
                                              .toString()+' cm'),
                                  context.listItemViewV2(
                                      'Weight',
                                      viewModel.listBillPackage.value[index]
                                          .net_weight
                                          .toString()+' kg'),
                                  viewModel.listBillPackage.value[index].checked
                                      ? Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            height: 40,
                                            child: context.formInput(
                                                viewModel
                                                    .listBillPackage.value[index]
                                                    .quantity_package
                                                    .toString(),
                                                'Quantity', (value) {
                                              viewModel
                                                  .listBillPackage.value[index]
                                                      .quantity_package =
                                                  int.parse(value);
                                            },
                                                inputType: TextInputType.number,
                                                suffixWidget:
                                                    context.normalText(viewModel
                                                        .hashMapQuantity[
                                                            viewModel
                                                                .listBillPackage.value[index]
                                                                .createOrderId]
                                                        .toString())),
                                          ),
                                        )
                                      : Container(),
                                ]),
                              )),
                        ));
                  })),
          ElevatedButton(
              onPressed: () async {
                if (await controller.addBillLadingDetail()) {
                  viewModel.billLadingDto.refresh();
                  Navigator.pop(context);
                }
              },
              child: Center(child: Text('Add bill lading detail')))
        ],
      ),
    );
  }

  @override
  CreateOrderController putController() {
    return CreateOrderController(CreateOrderVM());
  }

  _selectWarehouse(BuildContext context) {
    context.showBottomSheet<WarehouseDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Select Warehouse').tr),
        ), (context, item, index) {
      return InkWell(
        onTap: () => {
          viewModel.importBillLadingDetail.value.warehouse = item,
          viewModel.importBillLadingDetail.value.latitude = item.latitude,
          viewModel.importBillLadingDetail.value.longitude = item.longitude,
          viewModel.importBillLadingDetail.value.wjson_address =
              item.wjson_address,
          if(viewModel.exportBillLadingDetail.value.warehouse.areaInfo.zoneInfo.id==item.areaInfo.zoneInfo.id){
            viewModel.importBillLadingDetail.value.order_type = '0',
          }else{
            viewModel.importBillLadingDetail.value.order_type = '1',
          },
          viewModel.importBillLadingDetail.value.address = item.address,
          viewModel.importBillLadingDetail.value.zone_area_id =
              item.areaInfo.zoneInfo.id,

          viewModel.importBillLadingDetail.refresh(),
          Navigator.pop(context)
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/warehouse.png',
                      height: 23,
                      width: 23,
                      fit: BoxFit.fill,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          context.titleText(item.name),
                          context.normalText(item.phone),
                          context.normalText(item.address),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: context.divider(),
                )
              ],
            ),
          ),
        ),
      );
    }, viewModel.warehouseListController);
  }

  _selectService(BuildContext context) {
    context.showBottomSheet<BillServiceDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Select Service').tr),
        ), (context, item, index) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            child: InkWell(
                onTap: () => {
                  viewModel.importBillLadingDetail.value.billService
                      .add(item),
                  viewModel.exportBillLadingDetail.refresh(),
                  Navigator.pop(context)
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Column(
                    children: [
                      Container(
                        height: 40,
                        width: double.infinity,
                        child: Container(
                            child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  context.normalText(item.name),
                                  context.normalText(item.price.toString(),
                                      color: Colors.green),
                                ])),
                      ),
                      context.divider()
                    ],
                  ),
                ))),
      );
    }, viewModel.serviceListController);
  }
  Widget listServiceSelected(BuildContext context) {
    List<Widget> listWidgets =
    viewModel.importBillLadingDetail.value.billService.toList().map((e) {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Container(
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Center(child: context.normalTextOneLine(e.name)),
            )),
      );
    }).toList();
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
      child: Container(
        height: 25,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: listWidgets,
        ),
      ),
    );
  }
}
