import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/list_order/bill_lading_detail/api/bill_lading_detail_provider.dart';

class BillLadingDetailRepository{
  final BillLadingDetailProvider _api= BillLadingDetailProvider();
  Future<BillLadingDto> getBillLadings(Map<String,dynamic> params)=> _api.getBillLadings(params);
}