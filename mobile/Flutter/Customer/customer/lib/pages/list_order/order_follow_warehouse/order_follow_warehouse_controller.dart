import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/list_order/order_follow_warehouse/order_follow_warehouse_vm.dart';

class OrderFollowWarehouseController extends BaseController<OrderFollowWarehouseVM>{
  OrderFollowWarehouseController(OrderFollowWarehouseVM vm) : super(vm) {
    viewModel.listController = ListController(
        loadFunc: (page) async => PagedListData.withContent(
            viewModel.billLadingDetailDto?.value?.billPackages));
  }

}