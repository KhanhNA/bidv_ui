import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

class OrderFollowWarehouseVM extends BaseVM {
  Rx<BillLadingDetailDto> billLadingDetailDto= BillLadingDetailDto().obs;
  ListController<BillPackageDto> listController;
}
