import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/pages/list_order/order_follow_warehouse/order_follow_warehouse_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderFollowWarehouseItem{
  OrderFollowWarehouseController controller1;
  OrderFollowWarehouseItem(this.controller1);

  Widget buildItem(
      BuildContext context, BillPackageDto billPackageDto, int index) {
    return CommonUI.card(
      child: Column(
        children: [
          context.listItemView(
              'Bill_lading.order_code'.tr, billPackageDto?.item_name),
          context.listItemView('Bill_lading.total_weight'.tr,
              billPackageDto.product_type_name),
          context.listItemView('Bill_lading.total_weight'.tr,
              billPackageDto.total_weight.toString()),
          context.listItemView('Bill_lading.total_weight'.tr,
              billPackageDto.quantity_package.toString()),
          context.listItemView('Bill_lading.total_weight'.tr,
              billPackageDto.length.toString()+billPackageDto.width.toString()+billPackageDto.height.toString()),
        ],
      ),
    );
  }
}