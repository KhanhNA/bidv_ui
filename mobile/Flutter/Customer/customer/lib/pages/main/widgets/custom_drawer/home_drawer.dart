// import 'package:customer/base/ui/list/list_ui.dart';
// import 'package:customer/base/util/AppUtils.dart';
// import 'package:customer/icon/custom_icons.dart';
// import 'package:customer/model/login/Menu.dart';
// import 'package:customer/model/login/Role.dart';
// import 'package:customer/pages/main/routes/app_pages.dart';
// import 'package:customer/theme/app_theme.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// class HomeDrawer extends StatefulWidget {
//   const HomeDrawer(
//       {Key key,
//       this.screenRoute,
//       this.iconAnimationController,
//       this.callBackIndex})
//       : super(key: key);
//
//   final AnimationController iconAnimationController;
//   final String screenRoute;
//   final Function(String) callBackIndex;
//
//   @override
//   _HomeDrawerState createState() => _HomeDrawerState();
// }
//
// class _HomeDrawerState extends State<HomeDrawer> {
//   List<DrawerList> drawerList;
//
//   @override
//   void initState() {
//     setDrawerListArray();
//     super.initState();
//   }
//
//   void setDrawerListArray() {
//     // Map<String, int> menus = {};
//     // for(Role role in AppUtils.me.principal.roles){
//     //   if(GetUtils.isNullOrBlank(role.menus)){
//     //     continue;
//     //   }
//     //   for(Menu menu in role.getMobileMenus()){
//     //     menus.putIfAbsent(menu.code, ()=>1);
//     //   }
//     // }
//     // drawerList = drawerLists.where((element) => menus.containsKey(element.code)).toList();
//     drawerList = getDrawerList();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//       // backgroundColor: Theme.of(context).backgroundColor,
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: <Widget>[
//           Container(
//             color: Theme.of(context).primaryColor,
//             width: double.infinity,
//             padding: EdgeInsets.only(top: 30.0, left: 15, right: 15),
//             child: Row(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 // IconButton(
//                 //   icon: Icon(CustomIcons.fromName(0xe80e)),
//                 //   onPressed: () {},
//                 //   color: Colors.white,
//                 // ),
//                 Text("Menu",
//                     style: Theme.of(context).textTheme.headline6.apply(
//                           color: Colors.white,
//                         )),
//                 // CustomSelectLanguageWidget()
//               ],
//             ),
//           ),
//           Expanded(
//             child: ListView.separated(
//               separatorBuilder: (context, index) {
//                 return Divider(
//                   color: Theme.of(context).dividerColor,
//                   height: 0,
//                   indent: 45,
//                   endIndent: 15,
//                 );
//               },
//               physics: const BouncingScrollPhysics(),
//               padding: const EdgeInsets.all(0.0),
//               itemCount: drawerList.length,
//               itemBuilder: (BuildContext context, int index) {
//                 return drawItem(drawerList[index]);
//               },
//             ),
//           ),
//
//           context.divider(),
//           // Divider(
//           //   color: Theme.of(context).dividerColor,
//           //   height: 0,
//           // ),
//           Column(
//             children: <Widget>[
//               ListTile(
//                 title: Text(
//                   'Sign Out',
//                   style: TextStyle(
//                     fontFamily: AppTheme.fontName,
//                     fontWeight: FontWeight.w600,
//                     fontSize: 16,
//                     color: AppTheme.darkText,
//                   ),
//                   textAlign: TextAlign.left,
//                 ),
//                 trailing: Icon(
//                   Icons.power_settings_new,
//                   color: Colors.red,
//                 ),
//                 onTap: () => Get.offAllNamed(Routes.LOGIN),
//               ),
//               SizedBox(
//                 height: MediaQuery.of(context).padding.bottom,
//               )
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget drawItem(DrawerList listData) {
//     if (listData.children == null || listData.children.length == 0) {
//       return inkwell(listData);
//     }
//     List<Widget> lst = [];
//     for (var i = 0; i < listData.children.length; i++) {
//       lst.add(drawItem(listData.children[i]));
//     }
//
//     return ExpansionTile(
//       tilePadding: EdgeInsets.only(top: 0, right: 12, bottom: 0, left: 12),
//       backgroundColor: Colors.transparent,
//       title: Row(
//         children: [
//           listData.isAssetsImage
//               ? Container(
//                   width: 24,
//                   height: 24,
//                   child: Image.asset(listData.imageName,
//                       color: widget.screenRoute == listData.route
//                           ? Theme.of(context).primaryColor
//                           : Theme.of(context).iconTheme.color),
//                 )
//               : SizedBox(
//                   width: 24,
//                   child: listData.icon==null?null:Icon(listData.icon.icon,
//                       color: widget.screenRoute == listData.route
//                           ? Theme.of(context).primaryColor
//                           : Theme.of(context).iconTheme.color),
//                 ),
//           Expanded(
//               child: Padding(
//             padding: EdgeInsets.only(left: 10),
//             child: Text(
//               listData.labelName.tr,
//               style: widget.screenRoute == listData.route
//                   ? Theme.of(context)
//                       .textTheme
//                       .headline6
//                       .apply(fontWeightDelta: -2)
//                   : Theme.of(context).textTheme.headline6,
//               textAlign: TextAlign.left,
//             ),
//           ))
//         ],
//       ),
//       children: lst,
//     );
//   }
//
//   Widget inkwell(DrawerList listData) {
//     return Material(
//       color: Colors.transparent,
//       child: InkWell(
//         splashColor: Colors.grey.withOpacity(0.1),
//         highlightColor: Colors.transparent,
//         onTap: () {
//           navigationtoScreen(listData.route);
//         },
//         child: Stack(
//           children: <Widget>[
//             Container(
//               padding: EdgeInsets.only(top: 10, right: 12, bottom: 10, left: 12),
//               child: Row(
//                 children: <Widget>[
//                   listData.isAssetsImage
//                       ? Container(
//                           width: 24,
//                           height: 24,
//                           child: Image.asset(listData.imageName,
//                               color: widget.screenRoute == listData.route
//                                   ? Theme.of(context).primaryColor
//                                   : Theme.of(context).accentColor),
//                         )
//                       : SizedBox(
//                           width: 24,
//                           child: listData.icon==null?null:Icon(listData.icon.icon,
//                               color: widget.screenRoute == listData.route
//                                   ? Theme.of(context).primaryColor
//                                   : Theme.of(context).iconTheme.color),
//                         ),
//                   Expanded(
//                       child: Padding(
//                     padding: EdgeInsets.only(left: 10, bottom: 10),
//                     child: Text(
//                       listData.labelName.tr,
//                       textAlign: TextAlign.left,
//                       style: widget.screenRoute == listData.route
//                           ? Theme.of(context).textTheme.bodyText2
//                           : Theme.of(context)
//                               .textTheme
//                               .bodyText2
//                               .apply(fontWeightDelta: -2),
//                     ),
//                   ))
//                 ],
//               ),
//             ),
//             widget.screenRoute == listData.route
//                 ? AnimatedBuilder(
//                     animation: widget.iconAnimationController,
//                     builder: (BuildContext context, Widget child) {
//                       return Transform(
//                         transform: Matrix4.translationValues(
//                             (MediaQuery.of(context).size.width - 64) *
//                                 (1.0 -
//                                     widget.iconAnimationController.value -
//                                     1.0),
//                             0.0,
//                             0.0),
//                         child: Padding(
//                           padding: EdgeInsets.all(0),
//                           child: Container(
//                             width: MediaQuery.of(context).size.width - 64,
//                             height: 40,
//                             decoration: BoxDecoration(
//                               color: AppTheme.mainColor.withOpacity(0.2),
//                               borderRadius: new BorderRadius.only(
//                                 topLeft: Radius.circular(0),
//                                 topRight: Radius.circular(28),
//                                 bottomLeft: Radius.circular(0),
//                                 bottomRight: Radius.circular(28),
//                               ),
//                             ),
//                           ),
//                         ),
//                       );
//                     },
//                   )
//                 : const SizedBox()
//           ],
//         ),
//       ),
//     );
//   }
//
//   Future<void> navigationtoScreen(String routeScreen) async {
//     widget.callBackIndex(routeScreen);
//   }
//
//   List<DrawerList> getDrawerList() {
//     Map<String, DrawerList> ret = {};
//     for (Role role in AppUtils.me.principal.roles) {
//       if (GetUtils.isNullOrBlank(role.menus)) {
//         continue;
//       }
//
//       for (Menu menu in role.menus) {
//         if ('MOBILE' != menu.appType || menu.code == 'LOGOUT') {
//           continue;
//         }
//         if (menu.parentMenu == null) {
//           ret.putIfAbsent(menu.code, () => DrawerList.fromMenu(menu));
//           // parents.putIfAbsent(menu.id, () => []);
//         } else {
//           DrawerList parent = ret[menu.parentMenu.code];
//           if (parent == null) {
//             ret[menu.parentMenu.code] = DrawerList.fromMenu(menu.parentMenu);
//           }
//           List<DrawerList> children = parent.children;
//
//           children.add(DrawerList.fromMenu(menu));
//         }
//       }
//     }
//     return ret.values.toList();
//   }
//
//   // final drawerLists = <DrawerList>[
//   //   DrawerList(
//   //     route: Routes.HOME,
//   //     labelName: 'Home',
//   //     icon: Icon(Icons.home),
//   //   ),
//   //   DrawerList(route: '', code: 'CATEGORY', labelName: 'CATEGORY'.tr, icon: Icon(Icons.home), children: [
//   //     DrawerList(
//   //       route: Routes.INVENTORY,
//   //       code: 'INVENTORY',
//   //       labelName: 'INVENTORY'.tr,
//   //       icon: Icon(Icons.home),
//   //     ),
//   //   ]),
//   //   DrawerList(
//   //     route: '',
//   //     code:'IMPORT_STATEMENTs',
//   //     labelName: 'IMPORT_STATEMENTs'.tr,
//   //     icon: Icon(Icons.import_export_sharp),
//   //     children: [
//   //       DrawerList(
//   //         route: Routes.ImportStock,
//   //         code:'IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT',
//   //         labelName: 'IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT'.tr,
//   //         icon: Icon(Icons.import_export_sharp),
//   //       ),
//   //       DrawerList(
//   //         route: Routes.claim,
//   //         code:'IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES',
//   //         labelName: 'IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES'.tr,
//   //         icon: Icon(Icons.import_export_sharp),
//   //       ),
//   //
//   //     ]
//   //   ),
//   //   DrawerList(
//   //     route: '',
//   //     code: 'EXPORT_STATEMENTs',
//   //     labelName: 'EXPORT_STATEMENTs'.tr,
//   //     icon: Icon(Icons.login_outlined),
//   //     children: [
//   //       DrawerList(
//   //         route: Routes.EXPORT_STATEMENT,
//   //         code:'EXPORT_STATEMENTs_EXPORT_STATEMENT',
//   //         labelName: 'EXPORT_STATEMENTs_EXPORT_STATEMENT'.tr,
//   //         icon: Icon(Icons.login_outlined),
//   //       ),
//   //       DrawerList(
//   //         route: Routes.ExportSO,
//   //         code:'EXPORT_STATEMENTs_RELEASES',
//   //         labelName: 'EXPORT_STATEMENTs_RELEASES'.tr,
//   //         icon: Icon(Icons.login_outlined),
//   //       ),
//   //     ]
//   //   ),
//   //   DrawerList(
//   //     route: '',
//   //     code:'MANAGE_FC',
//   //     labelName: 'MANAGE_FC'.tr,
//   //     icon: Icon(Icons.dashboard),
//   //     children: [
//   //       DrawerList(
//   //         route: Routes.IMPORT_STOCK_REPACKING,
//   //         code:'MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING',
//   //         labelName: 'MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //       DrawerList(
//   //         route: Routes.IMPORT_STOCK_REPACKING,
//   //         code:'MANAGE_FC_UPDATE_PALLET',
//   //         labelName: 'MANAGE_FC_UPDATE_PALLET'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //     ]
//   //   ),
//   //   DrawerList(
//   //     route: '',
//   //     code:'PUCHASEORDER',
//   //     labelName: 'PUCHASEORDER'.tr,
//   //     icon: Icon(Icons.dashboard),
//   //     children: [
//   //       DrawerList(
//   //         route: Routes.claim,
//   //         code:'PUCHASEORDER_LIST',
//   //         labelName: 'PUCHASEORDER_LIST'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //     ]
//   //   ),
//   //   DrawerList(
//   //     route: '',
//   //     code:'CLAIM',
//   //     labelName: 'CLAIM'.tr,
//   //     icon: Icon(Icons.dashboard),
//   //     children: [
//   //       DrawerList(
//   //         route: Routes.claim,
//   //         code:'CLAIM_FIND',
//   //         labelName: 'CLAIM_FIND'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //       DrawerList(
//   //         route: Routes.createClaimPalletStep1,
//   //         code:'CLAIM_PALLET_STEP_ONE',
//   //         labelName: 'CLAIM_PALLET_STEP_ONE'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //       DrawerList(
//   //         route: Routes.createClaimPalletStep2,
//   //         code:'CLAIM_PALLET_STEP_TWO',
//   //         labelName: 'CLAIM_PALLET_STEP_TWO'.tr,
//   //         icon: Icon(Icons.dashboard),
//   //       ),
//   //     ]
//   //   ),
//   //
//   // ];
// }
//
// // enum DrawerIndex {
// //   HOME,
// //   FeedBack,
// //   ImportStock,
// //   claim,
// //   Share,
// //   About,
// //   Invite,
// //   Testing,
// // }
//
// class DrawerList {
//   DrawerList(
//       {this.isAssetsImage = false,
//       this.labelName = '',
//       this.icon,
//       this.code,
//       this.route,
//       this.imageName = '',
//       this.children});
//
//   factory DrawerList.fromMenu(Menu menu) {
//     return DrawerList(
//         labelName: 'menu.' + menu.code,
//         code: menu.code,
//         icon: GetUtils.isNullOrBlank(menu.url)?null: Icon(CustomIcons.fromName(menu.url)),
//         route: '/' + menu.code,
//         children: []);
//   }
//
//   String labelName;
//   Icon icon;
//   bool isAssetsImage;
//   String imageName;
//
//   // DrawerIndex index;
//   String route;
//   String code;
//   List<DrawerList> children;
// }
