part of './app_pages.dart';
abstract class Routes{

  static const INITIAL = '/';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const WAREHOUSE_MANAGER = '/warehouse/manager';
  static const ROUTING = '/Routing';
  static const INFO = '/Info';
  static const LIST_ORDER = '/list_order';
  static const NOTIFICATION = '/notification';
}