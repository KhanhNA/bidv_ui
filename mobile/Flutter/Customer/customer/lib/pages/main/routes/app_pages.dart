import 'package:customer/pages/login/login_page.dart';
import 'package:customer/pages/main/splash/splash_page.dart';
import 'package:customer/pages/warehouse_manager/warehouse_page.dart';
import 'package:get/get.dart';

import '../navigation_page.dart';

part './app_routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.INITIAL, page:()=> SplashPage(),),
    GetPage(name: Routes.HOME, page:()=> NavigationPage(),),
    GetPage(name: Routes.LOGIN, page:()=> LoginPage(),),
    GetPage(name: Routes.WAREHOUSE_MANAGER, page:()=> WarehousePage(),),
  ];

}