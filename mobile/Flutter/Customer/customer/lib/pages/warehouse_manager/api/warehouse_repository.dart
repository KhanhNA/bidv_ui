import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/warehouse/AdministrativeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/create_order/api/create_order_provider.dart';
import 'package:customer/pages/warehouse_manager/api/warehouse_provider.dart';

class WarehouseRepository{
  WarehouseProvider api= WarehouseProvider();
  Future<PagedListData<WarehouseDto>> getWarehouse(Map<String, dynamic> params) => api.getWarehouse(params);
  Future<PagedListData<AdministrativeDto>> getAddressArea(Map<String, dynamic> params) => api.getAddressArea(params);
  Future<bool> createWarehouse(WarehouseDto warehouse) => api.createWarehouse(warehouse);
}