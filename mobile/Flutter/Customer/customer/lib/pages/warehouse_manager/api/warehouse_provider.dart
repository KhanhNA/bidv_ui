import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/warehouse/AdministrativeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';

class WarehouseProvider{
  final String GET_WAREHOUSE_URI = 'share_van_order/get_warehouse';
  final String GET_ALLOW_AREA = 'customer/get_allow_area';
  final String CREATE_UPDATE_WAREHOUSE = 'customer/create_update_warehouse';
  ApiAuthProvider api = ApiAuthProvider();
  Future<PagedListData<WarehouseDto>> getWarehouse(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_WAREHOUSE_URI, params, (js) => WarehouseDto.fromJson(js));
    return data;
  }
  Future<PagedListData<AdministrativeDto>> getAddressArea(Map<String,dynamic> params) async {
    final data = await api.getListJsonParams(GET_ALLOW_AREA, params, (js) => AdministrativeDto.fromJson(js));
    return data;
  }
  Future<bool> createWarehouse(WarehouseDto warehouseDto) async {
    final data = await api.httpJson(CREATE_UPDATE_WAREHOUSE, warehouseDto, (js) => WarehouseDto.fromJson(js));

    return data!=null;
  }
}