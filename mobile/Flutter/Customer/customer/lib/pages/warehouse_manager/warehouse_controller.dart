import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/warehouse/AdministrativeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

import 'create_or_update_warehouse.dart';
import 'warehouse_vm.dart';

class WarehouseController extends BaseController<WarehouseVM>{
  WarehouseController(WarehouseVM viewModel) : super(viewModel){
    viewModel.listController =ListController(loadFunc: getWarehouse);
    viewModel.listAddressAreaController =ListController(loadFunc: getAddressArea);
    viewModel.listAddressAreaController.refresh();
  }
  Future<PagedListData<WarehouseDto>> getWarehouse(int page) async {
    Map<String, dynamic> params = {
      'companyId': AppUtils.partnerDto.company_id,
    };
    final data = await viewModel.repository.getWarehouse(params);
    return data;
  }
  Future<PagedListData<AdministrativeDto>> getAddressArea(int page) async {
    Map<String, dynamic> params = {
      'parent_id': viewModel.area_parent_id,
    };
    final data = await viewModel.repository.getAddressArea(params);
    return data;
  }
  Future<bool> createWarehouse() async {
    final data = await viewModel.repository.createWarehouse(viewModel.warehouseDto.value);
    return data;
  }
  gotoUpdateWarehouse(waehouseDto){
    viewModel.warehouseDto.value = waehouseDto;
    BaseZone.run(() =>Get.to(CreateOrUpdateWarehouse()));
  }
}