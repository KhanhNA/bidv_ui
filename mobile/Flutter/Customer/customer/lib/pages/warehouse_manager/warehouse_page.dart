import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/home/search_float_bar.dart';
import 'package:customer/pages/warehouse_manager/warehouse_controller.dart';
import 'package:customer/pages/warehouse_manager/warehouse_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'create_or_update_warehouse.dart';
import 'warehouse_vm.dart';

class WarehousePage extends BaseView<WarehouseVM, WarehouseController> {
  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(
      title: Text('Warehouse manager'),
    );
  }
  @override
  Widget body(BuildContext context) {
    return SliverView(
      toolbarHeight: 100,
      floatBar: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            SearchBar(controller),
            InkWell(
              onTap: ()=>{
                viewModel.warehouseDto.value=WarehouseDto(),
                Get.to(CreateOrUpdateWarehouse())},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 30,
                  width: MediaQuery.of(context).size.width*0.4,
                  decoration: BoxDecoration(color: Colors.green,borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        context.normalText('Create warehouse',color: Colors.white),
                        Icon(Icons.add_business_outlined,size: 24,),
                      ],
                    ),
                ),
              ),
            )
          ],
        ),
      ),
      listController: viewModel.listController,
      itemBuilder: WarehouseItemBuilder(controller).itemBuilder,
    );
  }

  @override
  WarehouseController putController() {
    return WarehouseController(WarehouseVM());
  }
}
