import 'dart:async';

import 'dart:ui';



class DelayHandle {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  DelayHandle({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}