
import 'package:customer/base/util/Utils.dart';

abstract class AbstractNotify extends Error{
  String title;
  String message;
  void showNotify();
}

class NotifyError extends AbstractNotify{
  String title;
  String message;


  NotifyError(this.title, this.message);

  @override
  void showNotify() {
    Utils.snackBarError(title, message);
  }
}

class NotifyWarning extends AbstractNotify{
  String title;
  String message;


  NotifyWarning(this.title, this.message);

  @override
  void showNotify() {
    Utils.snackBarWarning(title, message);
  }
}
class NotifyInfor extends AbstractNotify{
  String title;
  String message;


  NotifyInfor(this.title, this.message);

  @override
  void showNotify() {
    Utils.snackBarInfo(title, message);
  }
}
class NotifySuccess extends AbstractNotify{
  String title;
  String message;


  NotifySuccess(this.title, this.message);

  @override
  void showNotify() {
    Utils.snackBarSuccess(title, message);
  }
}