import 'package:customer/pages/common_list/SliverListIndicatorBuilder.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/theme/app_theme.dart';

import '../common_ui.dart';


abstract class BaseView<VM extends BaseVM, T extends BaseController<VM>> extends GetView<T> {

  VM get viewModel => this.controller.viewModel;
  // T controller;

  T putController();


  BaseView(){
    if(!Get.isRegistered<T>()) {
      Get.put(putController());
      // final f = super.controller;
    }
    // this.controller = super.controller;


  }
  // BaseView(){
  //   registerController();
  //   initVM();
  //   controller.onInit();
  // }



  // T get baseController;

  // set baseController(T value) {
  //   _baseController = value;
  // }

  AppBar appBar(BuildContext context) {
    return null;
  }

  // Widget bottomItem(BuildContext context) {
  //   final item = this.bottomNavigationBar(context);
  //   if (item == null) return SizedBox.shrink();
  //   return Visibility(visible: baseController.bottomVisible.value, child: CommonUI.footer(children: item));
  // }

  List<Widget> bottomNavigationBar(BuildContext context) {
    return null;
  }

  Future<void> refreshAll() async {
    return this.controller.update();
  }

  Widget body(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: appBar(context),
      body: RefreshIndicator(child: body(context), onRefresh: () => refreshAll()),
      bottomNavigationBar: Obx(() => Visibility(visible: this.controller.bottomVisible.value, child: (CommonUI.footer(children: this.bottomNavigationBar(context) ?? [Container()])))),
    );
  }
}


abstract class BaseView1<VM extends BaseVM, T extends BaseController1<VM>> extends GetView<T> {
  // final VM viewModel;
  T controller;
  void registerController(VM vm);

  BaseView(VM vm){
    registerController(vm);
    this.controller = super.controller;
    // controller.viewModel = viewModel;
    this.controller.init(vm);
    BaseZone.run(() => this.controller.reload());
  }
  // BaseView(){
  //   registerController();
  //   initVM();
  //   controller.onInit();
  // }

  VM get viewModel => this.controller.viewModel;

  // T get baseController;

  // set baseController(T value) {
  //   _baseController = value;
  // }

  AppBar appBar(BuildContext context) {
    return null;
  }

  // Widget bottomItem(BuildContext context) {
  //   final item = this.bottomNavigationBar(context);
  //   if (item == null) return SizedBox.shrink();
  //   return Visibility(visible: baseController.bottomVisible.value, child: CommonUI.footer(children: item));
  // }

  List<Widget> bottomNavigationBar(BuildContext context) {
    return null;
  }

  Future<void> refreshAll() async {
    return this.controller.update();
  }

  Widget body(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: appBar(context),
      body: RefreshIndicator(child: body(context), onRefresh: () => refreshAll()),
      bottomNavigationBar: Obx(() => Visibility(visible: this.controller.bottomVisible.value, child: (CommonUI.footer(children: this.bottomNavigationBar(context) ?? [Container()])))),
    );
  }
}


abstract class BaseGetView<T extends BaseGetController> extends GetView<T> {
  // final bottomVisible = true.obs;
  // T _baseController;


  // T get baseController;

  // set baseController(T value) {
  //   _baseController = value;
  // }

  AppBar appBar(BuildContext context) {
    return null;
  }

  // Widget bottomItem(BuildContext context) {
  //   final item = this.bottomNavigationBar(context);
  //   if (item == null) return SizedBox.shrink();
  //   return Visibility(visible: baseController.bottomVisible.value, child: CommonUI.footer(children: item));
  // }

  List<Widget> bottomNavigationBar(BuildContext context) {
    return null;
  }

  Future<void> refreshAll() async {
    return this.controller.update();
  }

  Widget body(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: appBar(context),
      body: RefreshIndicator(child: body(context), onRefresh: () => refreshAll()),
      bottomNavigationBar: Obx(()=>Visibility(visible: this.controller.bottomVisible.value, child: (CommonUI.footer(children: this.bottomNavigationBar(context)??[Container()])))),
    );
  }
}

class SliverView<VM> extends GetView {
  final RxDouble toolbarHeight = 500.0.obs;

  // final Widget Function() floatBar;
  final Widget floatBar;
  final Widget expandableBar;
  final Widget Function(BuildContext context, VM item, int index) itemBuilder;
  final List<Widget> widgetBefore;
  final List<Widget> widgetAfter;
  final ListController<VM> listController;
  final SliverGridDelegate gridDelegate;

  SliverView({double toolbarHeight = 60.0, this.floatBar, this.itemBuilder, this.widgetBefore,
    this.widgetAfter, this.listController, this.expandableBar,this.gridDelegate}) {
    this.toolbarHeight.value = toolbarHeight;
  }

  Future refresh() {
    return Future(() => this.controller.update());
  }

  SliverAppBar appBarWidget(){
    return this.floatBar == null?null:SliverAppBar(
      toolbarHeight: this.toolbarHeight.value,
      backgroundColor: AppTheme.backgroundApp,
      automaticallyImplyLeading: false,
      title: floatBar,
      floating: true,
    );
  }
  // Widget itemBuilder(BuildContext context, VM item, int index);
  // List<Widget> widgetBefore(){
  //   return [];
  // }
  // List<Widget> widgetAfter(){
  //   return [];
  // }
  // LoadingMoreBase getListController();
  LoadingMoreSliverList<VM> loadMoreList() {
    return LoadingMoreSliverList(SliverListConfig<VM>(
      itemBuilder: itemBuilder,
      sourceList: listController,
      indicatorBuilder: (context, status) => SliverListIndicatorBuilder().build(context, status),
        gridDelegate:gridDelegate
      //isLastOne: false
      //autoRefresh: false,
    ));
  }

  @override
  Widget build(BuildContext context) {
    List lstWidget = (widgetBefore ?? []).adds([loadMoreList()]).adds(widgetAfter ?? []);
    if(expandableBar != null){
      lstWidget.insert(0, expandableBar);
    }
    if (this.floatBar != null) {
      lstWidget.insert(0, appBarWidget());
    }
    return LoadingMoreCustomScrollView(showGlowLeading: false, rebuildCustomScrollView: true, slivers: lstWidget);
  }
}

class MyExpandableController extends ExpandableController {
  void Function(bool) onToggle;

  MyExpandableController(this.onToggle); //
  // MyExpandableController({void Function(bool) onToggle}){
  //  this.onToggle = onToggle;
  // }

  @override
  void toggle() {
    expanded = !expanded;
    onToggle(expanded);
  }
}

class FloatSliverAppBar extends GetView {
  final toolbarHeight = 100.0.obs;
  final Widget collapse;
  final Widget expand;
  final double collapseHeight;
  final double expandHeight;

  FloatSliverAppBar({this.collapse, this.expand, this.collapseHeight = 100.0, this.expandHeight = 500.0}) {
    toolbarHeight.value = collapseHeight;
  }

  @override
  Widget build(BuildContext context) {
    ExpandableController expController = MyExpandableController((b) {
      toolbarHeight.value = b ? this.expandHeight : this.collapseHeight;
    });
    return Obx(() => SliverAppBar(
          toolbarHeight: this.toolbarHeight.value,
          backgroundColor: AppTheme.white,
          automaticallyImplyLeading: false,
          floating: true,
          elevation: 0,
          title: Column(children: [
            // context.textSearch(onchange: onChange, hintText: 'repack.hintTextSearch'.tr),
            collapse,
            SizedBox(
              height: 10,
            ),
            ExpandableNotifier(
                // <-- Provides ExpandableController to its children
                controller: expController,
                child: IntrinsicHeight(
                    child: Expandable(
                        // <-- Driven by ExpandableController from ExpandableNotifier
                        collapsed: ExpandableButton(child: context.textDrawableStart('ExpandSearch'.tr, Icons.keyboard_arrow_down_sharp, AppTheme.mainColor)),
                        expanded: expand)) //expandLayout(context))),
                )
          ]),
        ));
  }

// expandLayout(BuildContext context) {
//   // Size size = MediaQuery.of(context).size;
//   // tollbarHeight.value = 500.0;
//   return Column(
//     // crossAxisAlignment: CrossAxisAlignment.start,
//     children: [
//       // context.listItemView("lbl_status".tr, null),
//       // context.wrapListChipGroup<RepackingPlanningStatus>(RepackingPlanningStatus.values,
//       //     obs: controller.repackingPlanningStatusObs, display: (status) => status.displayValue),
//       context.listItemViewWidgetFixWidth('storeId'.tr, StoreWidget(onChanged: (store) => onChange)),
//       context.listItemViewWidgetFixWidth('distributor'.tr, DistributorWidget(onChanged: (distributor) => onChange)),
//       context.listItemViewWidgetFixWidth('po.CURRENT_STATUS'.tr, MySelectionFormField<PoStatus>(options: PoStatus.values, onChanged: (store) => onChange,
//         titleBuilder: (type, selected) => Text(('po.type.' + type?.value).tr, style: selected?AppTheme.btnTextPrimaryOutline: AppTheme.normal,),
//         // initialValues: PoStatus.All,
//       )),
//       context.listItemViewWidgetFixWidth('po.ORDER_STATUS'.tr, MySelectionFormField<PoStatus>(options: PoStatus.values, onChanged: (store) => onChange,
//           titleBuilder: (type, selected) => Text(('po.type.' + type?.value).tr, style: selected?AppTheme.btnTextPrimaryOutline: AppTheme.normal,)),
//       ),
//
//       Row(children: [Expanded(child: MyDatePicker()), Expanded(child: MyDatePicker())]),
//       // searchTime(size, context),
//       SizedBox(
//         height: 10,
//       ),
//       ExpandableButton(child: TextUI.textDrawableStart('CollapseSearch'.tr, Icons.keyboard_arrow_up_sharp, AppTheme.mainColor))
//     ],
//   );
// }
}

abstract class ScaffoldViewNoAppBar<T extends GetxController> extends GetView<T> {
  Future refresh() {
    return Future(() => this.controller.update());
  }

  Widget body(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: RefreshIndicator(child: body(context), onRefresh: () => refresh()),
    );
  }
}


abstract class SliverView1<T extends BaseGetController, VM> extends BaseGetView<T> {
  final RxDouble toolbarHeight = 200.0.obs;

  SliverView1({toolbarHeight=100.0}){
    this.toolbarHeight.value = toolbarHeight;
  }

  Future refresh() {
    return Future(() => controller.update());
  }

  @override
  Widget body(BuildContext context){
    return null;
  }

  Widget floatBar();

  Obx appBarWidget(){
    return Obx(()=>SliverAppBar(
      toolbarHeight: this.toolbarHeight.value,
      backgroundColor: AppTheme.white,
      automaticallyImplyLeading: false,
      title: floatBar(),
      floating: true,
    ));
  }
  Widget itemBuilder(BuildContext context, VM item, int index);
  List<Widget> widgetBefore(){
    return [];
  }
  List<Widget> widgetAfter(){
    return [];
  }
  LoadingMoreBase getListController();
  LoadingMoreSliverList<VM> loadMoreList(){
    return LoadingMoreSliverList(SliverListConfig<VM>(
      itemBuilder: itemBuilder,
      sourceList: getListController(),
      indicatorBuilder: (context, status) => SliverListIndicatorBuilder().build(context, status),
      //isLastOne: false
      //autoRefresh: false,
    ));
  }
  @override
  Widget build(BuildContext context) {
    List lstWidget = widgetBefore().adds([loadMoreList()]).adds(widgetAfter());
    lstWidget.insert(0, appBarWidget());
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: appBar(context),
      body: RefreshIndicator(child: LoadingMoreCustomScrollView(
        showGlowLeading: false,
        rebuildCustomScrollView: true,
        slivers: lstWidget
      ), onRefresh: () => refreshAll()),
      // bottomNavigationBar: bottomItem(context),
    );
  }
}


