


import 'dart:typed_data';

import 'package:flutter/widgets.dart';
import 'package:customer/base/util/Utils.dart';

class Base64Image extends StatelessWidget {
  Uint8List decoded;

  Base64Image(String encoded) {
    if (encoded==null || encoded.isEmpty) {
      decoded = null;
    } else {
      decoded = Utils.decodeBase64(encoded);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (decoded==null) {
      return Image.asset("assets/icons/no_image.png");
    } else {
      return Image.memory(decoded);
    }
  }

}