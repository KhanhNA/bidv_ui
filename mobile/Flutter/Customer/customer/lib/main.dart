import 'dart:async';

import 'package:customer/pages/info/info_page.dart';
import 'package:customer/pages/list_order/list_order_page.dart';
import 'package:customer/pages/login/login_page.dart';
import 'package:customer/pages/main/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

import 'pages/notification/notification_page.dart';
import 'pages/routing_day/routing_day_page.dart';
import 'pages/warehouse_manager/warehouse_page.dart';
import 'theme/app_theme.dart';
import 'base/LocalizationService.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle (
      SystemUiOverlayStyle (
        statusBarColor: Colors.transparent,
      )
  );
  await GetStorage.init();

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoginPage(),
    themeMode: ThemeMode.light,

    locale: Locale(LocalizationService.locale, ''),
    // fallbackLocale: LocalizationService.fallbackLocale,
    translations: LocalizationService(),
    theme: appThemeData,

    defaultTransition: Transition.rightToLeftWithFade,
    routes: {
      Routes.ROUTING : (context) => RoutingDayPage(),
      Routes.WAREHOUSE_MANAGER : (context) => WarehousePage(),
      Routes.LIST_ORDER: (context) => ListOrderPage(),
      Routes.NOTIFICATION: (context) => NotificationPage(),
      Routes.INFO: (context) => InfoPage(),
    },
  ));
  // Get.rootController.setTheme(appThemeData);

}
