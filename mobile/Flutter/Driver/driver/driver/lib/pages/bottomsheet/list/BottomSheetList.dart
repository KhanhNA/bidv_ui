import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:driver/pages/common_list/SliverListIndicatorBuilder.dart';

class BottomSheetList<T> extends GetWidget {
  final Widget title;
  final Function(BuildContext context, T item, int index) itemBuilder;
  final LoadingMoreBase<T> sourceList;

  BottomSheetList(this.title, this.itemBuilder, this.sourceList);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      title,
      Expanded(
          child: LoadingMoreCustomScrollView(
              showGlowLeading: false,
              rebuildCustomScrollView: true,
              slivers: <Widget>[
                LoadingMoreSliverList<T>(SliverListConfig<T>(
                  itemBuilder: itemBuilder,
                  sourceList: sourceList,
                  indicatorBuilder: (context, status) => SliverListIndicatorBuilder().build(context, status),
                ))
              ])
      )
    ]);
  }

}