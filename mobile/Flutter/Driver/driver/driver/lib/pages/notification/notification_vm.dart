import 'package:driver/base/controller/BaseViewModel.dart';
import 'package:driver/model/notification/NotificationDto.dart';
import 'package:driver/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

import 'api/notification_repository.dart';

class NotificationVM extends BaseVM {
  RxInt totalElementsNotificationRouting = 0.obs;
  RxInt totalElementsNotificationSystem= 0.obs;
  final notificationStatus = ['Routing', 'System'];
  final notificationDto = NotificationDto();
  final repository = NotificationRepository();
  String notificationType;

  NotificationVM();

  ListController<NotificationDto> listNotificationSystemController;
  ListController<NotificationDto> listNotificationRoutingController;
}
