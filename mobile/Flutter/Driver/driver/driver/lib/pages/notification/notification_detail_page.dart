import 'package:driver/base/ui/get/base_get_view.dart';
import 'package:driver/base/ui/list/list_ui.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/model/notification/NotificationDto.dart';
import 'package:driver/pages/notification/notification_vm.dart';
import 'package:driver/pages/notification/system_tap_view/notification_system_controller.dart';
import 'package:driver/theme/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class NotificationSystemDetail extends BaseView<NotificationVM,NotificationSystemController>{
  NotificationDto notificationDto;
  NotificationSystemDetail(this.notificationDto);
  @override
  Widget body(BuildContext context) {
    return Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.adb,
                  color: AppTheme.grey,
                ),
                Text(
                  notificationDto.create_date,
                  style: TextStyle(color: Colors.black, fontSize: 10),
                )
              ],
            ),
            context.divider(),
            Container(
              child : (notificationDto?.image_256 !=null )? Image.network(
                notificationDto.image_256,
                width: double.infinity,
                height: 150,
              ) : Image.asset("assets/icons/no_image.png"),
            ),
            context.divider(),
            context.itemTitleView(notificationDto.title),
            context.itemView(notificationDto.content),
            context.divider(),
          ],
        )
    );
  }

  @override
  NotificationSystemController putController() {
    return NotificationSystemController(NotificationVM());
  }


}