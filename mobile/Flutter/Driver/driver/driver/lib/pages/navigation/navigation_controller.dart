import 'package:driver/base/ui/get/base_get_controller.dart';
import 'package:driver/pages/main/routes/app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'navigation_vm.dart';


class NavigationController extends BaseController<NavigationVM> {
  Rx<Widget> screenView = Rx();
  String route = Routes.HOME;
  final navInd = 0.obs;

  NavigationController(viewModel) : super(viewModel);


  changeIndex(String route){
    // Get.toNamed(route);
    this.route = route;
    screenView.value = AppPages.pages.where((element) => element.name == route).first.page();
    // print('aaaa');
  }


}
