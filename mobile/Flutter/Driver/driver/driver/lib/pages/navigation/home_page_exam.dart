import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:driver/base/ui/common_ui.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/model/dashboard.dart';

// import 'package:driver/pages/pallet/updatePallet/UpdatePalletPage.dart';
// import 'package:driver/pages/repack/RepackPage.dart';

import 'package:driver/theme/app_theme.dart';

import 'navigation_controller.dart';
import 'navigation_vm.dart';

class NavigationPageTemp extends GetView<NavigationController> {
  final SIZE_PROCESS_ORDER = 200.0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final NavigationController controller1 = Get.put(NavigationController(NavigationVM()));

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    Get.rootController.setTheme(appThemeData);
    final double heightProcessTitle = SIZE_PROCESS_ORDER;
    final double heightProcessContent = 160;
    return Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                // color: Theme.of(context).primaryColor,
                height: heightProcessTitle + heightProcessContent / 2,
                child: Stack(
                  children: [
                    Container(
                        alignment: Alignment.topLeft,
                        // padding: AppTheme.contentPadding,
                        height: heightProcessTitle,
                        color: Theme.of(context).primaryColor,
                        child: Column(children: [
                          AppBar(
                            title: Center(
                                child: Text(
                              'dashboard.title'.tr,
                              style: Theme.of(context)
                                  .appBarTheme
                                  .textTheme
                                  .headline6,
                            )),
                            leading:
                                Icon(Icons.menu, color: Colors.transparent),
                            bottomOpacity: 0.0,
                            elevation: 0.0,
                            actions: <Widget>[
                              IconButton(
                                icon: Icon(Icons.refresh),
                                onPressed: () {
                                  // _dashboardUserBloc.add(BaseEvent());
                                },
                              ),
                              // IconButton(
                              //   icon: Icon(FontAwesomeIcons.signOutAlt),
                              //   onPressed: () {
                              //     locator<SharedPreferencesManager>().clearAll();
                              //     Navigator.pushNamedAndRemoveUntil(
                              //         context, '/login_screen', (r) => false);
                              //   },
                              // ),
                            ],
                          ),
                          Container(
                            child: Text(
                              "dashboard.processingOrder".tr,
                              style:
                                  Theme.of(context).primaryTextTheme.headline6,
                            ),
                            alignment: Alignment.topLeft,
                            padding: AppTheme.contentPadding,
                          ),
                        ])),
                    Container(
                        alignment: Alignment.bottomLeft,
                        padding: AppTheme.edgeLeftRight,
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.08),
                            spreadRadius: 0,
                            blurRadius: 16,
                            offset: Offset(0, 8),
                          )
                        ]),
                        child: _sumInfo(context, heightProcessContent))
                    // ),
                  ],
                ),
              ),
              Container(margin: AppTheme.contentPadding, child: Text(' ')),
              Container(
                  padding: AppTheme.contentPadding,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: AppTheme.contentMargin,
                        child: Text(
                          'dashboard.expireWarning.title'.tr,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Padding(
                        padding: AppTheme.contentMargin,
                        child: Obx(() => InkWell(
                            child: context.textView(context,
                                'dashboard.expireWarning.warning'.tr, 'xyz',
                                color: AppTheme.warningColor.withOpacity(0.3),
                                colorText: AppTheme.colorTextWarning),
                            onTap: () {})),
                      ),
                      Padding(
                        padding: AppTheme.contentMargin,
                        child: Obx(() => InkWell(
                              child: context.textView(context,
                                  'dashboard.expireWarning.expire'.tr, 'dxt',
                                  color: AppTheme.colorView,
                                  colorText: AppTheme.txtHintColor),
                              onTap: () {},
                            )),
                      )
                    ],
                  )),
            ],
          ),
        ));
  }

  Widget _sumInfo(BuildContext context, double height) {
    return Container(
        height: height,
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
        ),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
                child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                   Expanded(
                          child: _poBoxSum(
                              context, 0,
                              borderBottom: true,
                              colorDot: AppTheme
                                  .dashboardOptionStatus['soWaitForExport'],
                              borderRight: true,
                              text: 'soWaitForExport'.tr)),
                  Expanded(
                      child: _poBoxSum(
                          context, 1,
                          borderBottom: true,
                          text: "stoWaitForImport".tr,
                          colorDot: AppTheme
                              .dashboardOptionStatus['stoWaitForImport']))
                ])),
            Expanded(
                child: Row(children: [
              Expanded(
                  child: _poBoxSum(context,2,
                      borderBottom: false,
                      borderRight: true,
                      colorDot: AppTheme.dashboardOptionStatus['stoDelivery'],
                      text: 'stoDelivery'.tr)),
              Expanded(
                  child: _poBoxSum(context, 3,
                      colorDot:
                          AppTheme.dashboardOptionStatus['stoWaitForExport'],
                      text: "stoWaitForExport".tr))
            ]))
          ],
        ));
  }

  Widget _poBoxSum(BuildContext context, int value,
      {borderBottom: false,
      borderRight: false,
      colorDot: AppTheme.mainColor,
      text: ""}) {
    return Container(
        height: 82,
        decoration: new BoxDecoration(
            border: Border(
                bottom: borderBottom
                    ? BorderSide(color: AppTheme.divider)
                    : BorderSide.none,
                right: borderRight
                    ? BorderSide(color: AppTheme.divider)
                    : BorderSide.none)
            //   color: Colors.grey,
            // )
            // )
            ),
        child: Row(
          children: [
            Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.only(top: 20),
                child: context.dotCircle(colorDot)),
            Padding(
                padding: EdgeInsets.only(top: 16, left: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      text,
                      textAlign: TextAlign.left,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Text(
                        CommonUI.formatNumber(value),
                        style: TextStyle(
                            color: AppTheme.darkerText,
                            fontWeight: FontWeight.bold,
                            fontSize: 22),
                      ),
                    )
                  ],
                ))
          ],
        ));
  }
}
