
import 'package:driver/base/auth/token.dart';
import 'package:driver/model/login/PartnerDto.dart';
import 'package:driver/model/login/UserAuthentication.dart';
import 'package:driver/model/login/login_body.dart';
import 'package:driver/model/session/OdooSessionDto.dart';

import 'user_provider.dart';

class UserRepository {
  final UserProvider _api = UserProvider();

  Future<OdooSessionDto> login(LoginBody loginBody) => _api.loginUser(loginBody);
  Future<UserAuthentication> getUserMe() => _api.getUserMe();
  Future<PartnerDto> getInfoCustomer() => _api.getInfoCustomer();


}