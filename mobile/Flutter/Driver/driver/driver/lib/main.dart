import 'dart:async';

import 'package:driver/pages/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

import 'pages/notification/notification_page.dart';
import 'theme/app_theme.dart';
import 'base/LocalizationService.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle (
      SystemUiOverlayStyle (
        statusBarColor: Colors.transparent,
      )
  );
  await GetStorage.init();

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoginPage(),
    themeMode: ThemeMode.light,

    locale: Locale(LocalizationService.locale, ''),
    // fallbackLocale: LocalizationService.fallbackLocale,
    translations: LocalizationService(),
    theme: appThemeData,

    defaultTransition: Transition.rightToLeftWithFade,
    // routes: {
    //   'Routing': (context) => RoutingDayPage(),
    //   'List_order': (context) => ListOrderPage(),
    //   'Notification': (context) => NotificationPage(),
    //   'Info': (context) => InfoPage(),
    // },
  ));
  // Get.rootController.setTheme(appThemeData);

}
