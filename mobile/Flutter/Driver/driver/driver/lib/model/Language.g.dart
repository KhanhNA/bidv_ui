// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Language.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Language _$LanguageFromJson(Map<String, dynamic> json) {
  return Language()
    ..checked = json['checked'] as bool
    ..index = json['index'] as int
    ..transactionErrCode = json['transactionErrCode'] as String
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..sortOrder = json['sortOrder'] as int
    ..code = json['code'] as String
    ..error = json['error'] as String;
}

Map<String, dynamic> _$LanguageToJson(Language instance) => <String, dynamic>{
      'checked': instance.checked,
      'index': instance.index,
      'transactionErrCode': instance.transactionErrCode,
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'sortOrder': instance.sortOrder,
      'code': instance.code,
      'error': instance.error,
    };
