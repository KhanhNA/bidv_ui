import 'UserAuthentication.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:driver/model/base.dart';
part 'Rule.g.dart';
@JsonSerializable(explicitToJson: true)
class Rule extends _Rule {

    String error;
    Rule.withError(this.error);
    Rule();

    factory Rule.fromJson(Map<String, dynamic> js) =>Base().fromJs<Rule>(js, (js) => _$RuleFromJson(js));

    Map<String, dynamic> toJson() => _$RuleToJson(this);
}


enum RuleType {
    TKFC
}
class _Rule extends Base {

    

    UserAuthentication userAuthentication;


}
