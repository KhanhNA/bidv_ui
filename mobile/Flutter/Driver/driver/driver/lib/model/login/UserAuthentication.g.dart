// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserAuthentication.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserAuthentication _$UserAuthenticationFromJson(Map<String, dynamic> json) {
  return UserAuthentication()
    ..checked = json['checked'] as bool
    ..index = json['index'] as int
    ..transactionErrCode = json['transactionErrCode'] as String
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..principal = json['principal'] == null
        ? null
        : Principal.fromJson(json['principal'] as Map<String, dynamic>)
    ..authorities = (json['authorities'] as List)
        ?.map((e) =>
            e == null ? null : Authority.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..error = json['error'] as String;
}

Map<String, dynamic> _$UserAuthenticationToJson(UserAuthentication instance) =>
    <String, dynamic>{
      'checked': instance.checked,
      'index': instance.index,
      'transactionErrCode': instance.transactionErrCode,
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'principal': instance.principal?.toJson(),
      'authorities': instance.authorities?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
