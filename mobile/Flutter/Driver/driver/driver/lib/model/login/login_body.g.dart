// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginBody _$LoginBodyFromJson(Map<String, dynamic> json) {
  return LoginBody(
    json['login'] as String,
    json['password'] as String,
    json['db'] as String,
  )
    ..checked = json['checked'] as bool
    ..index = json['index'] as int
    ..transactionErrCode = json['transactionErrCode'] as String
    ..error = json['error'] as String;
}

Map<String, dynamic> _$LoginBodyToJson(LoginBody instance) => <String, dynamic>{
      'checked': instance.checked,
      'index': instance.index,
      'transactionErrCode': instance.transactionErrCode,
      'error': instance.error,
      'login': instance.login,
      'password': instance.password,
      'db': instance.db,
    };
