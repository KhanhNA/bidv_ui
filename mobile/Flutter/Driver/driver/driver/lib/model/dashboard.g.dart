// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Dashboard _$DashboardFromJson(Map<String, dynamic> json) {
  return Dashboard()
    ..poWaitForImport = json['poWaitForImport'] as int
    ..soWaitForExport = json['soWaitForExport'] as int
    ..stoWaitForExport = json['stoWaitForExport'] as int
    ..stoDelivery = json['stoDelivery'] as int
    ..stoWaitForImport = json['stoWaitForImport'] as int
    ..inventoryWarningOutOfDate = json['inventoryWarningOutOfDate'] as int
    ..inventoryOutOfDate = json['inventoryOutOfDate'] as int
    ..storeId = json['storeId'] as int;
}

Map<String, dynamic> _$DashboardToJson(Dashboard instance) => <String, dynamic>{
      'poWaitForImport': instance.poWaitForImport,
      'soWaitForExport': instance.soWaitForExport,
      'stoWaitForExport': instance.stoWaitForExport,
      'stoDelivery': instance.stoDelivery,
      'stoWaitForImport': instance.stoWaitForImport,
      'inventoryWarningOutOfDate': instance.inventoryWarningOutOfDate,
      'inventoryOutOfDate': instance.inventoryOutOfDate,
      'storeId': instance.storeId,
    };
