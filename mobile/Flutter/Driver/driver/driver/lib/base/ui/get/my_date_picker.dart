
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:driver/theme/app_theme.dart';





class MyDatePicker extends GetView {
  final Function(DateTime) onValue;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: InputDatePickerFormField(
        initialDate: obs?.value??initialDate,
        firstDate: firstDate ?? DateTime(2000),
        lastDate: lastDate ?? DateTime.now(),
        fieldLabelText: fieldLabelText,
      )),
      FittedBox(
          fit: BoxFit.contain,
          child: InkWell(
              child: Icon(
                Icons.calendar_today_outlined,
                color: AppTheme.grey,
              ),
              onTap: () => showDateDialog(context)))
    ]);
  }

  showDateDialog(BuildContext context) {
    showDatePicker(context: context, initialDate: obs?.value ?? DateTime.now(), firstDate: firstDate ?? DateTime(2020), lastDate: lastDate ?? DateTime.now()).then((value) {
      obs?.value = value;
      if (onValue != null) onValue(value);
    });
  }

  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final Rx<DateTime> obs;
  final String fieldLabelText;

  MyDatePicker({this.initialDate, this.firstDate, this.lastDate, this.obs, this.onValue, this.fieldLabelText});
}
