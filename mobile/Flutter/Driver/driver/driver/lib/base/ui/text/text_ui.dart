
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:driver/base/ui/get/my_text_form_field.dart';
import 'package:driver/theme/app_theme.dart';


extension TextUI on BuildContext{
  InputDecoration textDecoration(){
    return InputDecoration(
      border: new OutlineInputBorder(
          borderSide:
          new BorderSide(color: AppTheme.textFieldNomalBorder)),
      focusedBorder: new OutlineInputBorder(
          borderSide: new BorderSide(color: AppTheme.mainColor)),
    );
  }
  InputDecoration textDecorationNone(){
    return InputDecoration(
      border: new OutlineInputBorder(
          borderSide: BorderSide.none
          ),
      focusedBorder: new OutlineInputBorder(
          borderSide: BorderSide.none),
    );
  }
   Widget rxTextField(TextEditingController ctl, ){
    TextFormField tf= TextFormField(
      controller: ctl, //TextEditingController(text: obs?.value),
      //onChanged: (val)=>obs.value = val,
      decoration: textDecoration(),
      // readOnly: true,


    );
    return tf;
  }


   Widget password(TextEditingController controller) {
    bool _showPassword = true;
    return new Container(
        child: new Column(
      children: <Widget>[
        new TextFormField(
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock),
            border: new OutlineInputBorder(
                borderSide:
                    new BorderSide(color: AppTheme.textFieldNomalBorder)),
            focusedBorder: new OutlineInputBorder(
                borderSide: new BorderSide(color: AppTheme.mainColor)),
            suffixIcon: GestureDetector(
              onTap: () {
                // _togglevisibility();
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.red,
              ),
            ),
          ),
          validator: (val) => val.length < 6 ? 'Password too short.' : null,
          controller: controller,

          // onSaved: (val) => _password = val,
          // obscureText: _obscureText,
        ),
      ],
    ));
  }

   Widget textField(TextEditingController controller,
      {hintText,
      preIcon,
      type: TextInputType.text,

      id,
      onChange}) {
    return TextField(
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        hintText: hintText,
        filled: true,
        prefixIcon: preIcon,
        fillColor: AppTheme.backgroundTextFile,
        isDense: true,
        contentPadding: EdgeInsets.all(2),
        border: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.textFieldNomalBorder)),
        focusedBorder: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.mainColor)),
      ),
      controller: controller,
      onChanged: (val) {

        onChange(val);
      },
      keyboardType: type,
    );
  }





   Widget textFormField(TextEditingController controller,
      {name,
      id,
      hintText,
      preIcon,
      type: TextInputType.text,
      validators,
      binding,
      text: '',
        maxLines = 1,
      onChange}) {
    return new TextFormField(
      decoration: InputDecoration(
        isDense: true,
        border: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.textFieldNomalBorder)),
        focusedBorder: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.mainColor)),
        contentPadding: new EdgeInsets.symmetric(vertical: (maxLines??5)*10.0, horizontal: 0),
      ),
      validator: (val) => validators != null ? validators(val) : null,
      controller: controller,
      // onSaved: (val) => binding(val),
      keyboardType: type,
      initialValue: text,
      onChanged: (val) {
        if(onChange != null) {
          onChange(val);
        }
      },
    );
  }

   Widget myTextFormField(
      {RxString obs,arrId,
        id,
      hintText,
      preIcon,
      type: TextInputType.text,
      validators,
      binding,
      text: '',
        maxLines = 1,
      onChange}) {
    return MyTextFormField(
      decoration: InputDecoration(
        prefixIcon: preIcon,
        isDense: true,
        border: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.textFieldNomalBorder)),
        focusedBorder: new OutlineInputBorder(
            borderSide: new BorderSide(color: AppTheme.mainColor)),
        contentPadding: new EdgeInsets.symmetric(vertical: (maxLines??5)*10.0, horizontal: 0),
      ),
      validator: (val) => validators != null ? validators(val) : null,
      obs: obs,
      arrId: arrId,
      // onSaved: (val) => binding(val),
      keyboardType: type,
      initialValue: obs?.value??text,
      maxLines: maxLines,
      onChanged: (val) {

        if(onChange != null)
          onChange(val);
      },
    );
  }

   Widget dotCircle(Color color, {EdgeInsets margin = AppTheme.edgeLeft, double size = 10.0}) {
    return Container(
        width: size,
        height: size,
        margin: margin,
        decoration: new BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ));
  }

   Widget text(String text, {
     trans: false,
     dot: false,
     dotColor: AppTheme.mainColor,
     style: AppTheme.normal,
     paddingStyle: AppTheme.cardFieldPadding,
     alignment :Alignment.centerLeft,
   }) {
    text = trans ? (text) : text;
    Container container = Container(
        padding: paddingStyle,
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: style,
          textAlign: TextAlign.left,
        ));
    if (!dot) {
      return container;
    } else {
      return Row(children: [this.dotCircle(dotColor), container]);
    }
  }

   Widget textView(BuildContext context, String lbl, String text,
      {color: Colors.white,colorText: AppTheme.darkerText}) {
    return Container(
      // margin: AppTheme.edgeHomeDash1,
      decoration: new BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(8.0),
      ),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Padding(

                  padding: AppTheme.edgeHomeDash,
                  child: Text(
                    (lbl),
                    style: TextStyle(color:AppTheme.txtNormalColor,fontSize: 14,fontWeight: FontWeight.normal ),
                    textAlign: TextAlign.left,
                  ))),
          Container(
              child: Row(
                children: [
                  Padding(
                      padding: AppTheme.edgeHomeDash,
                      child: Text(
                        text,
                        style: TextStyle(color:colorText,fontSize: 22,fontWeight: FontWeight.bold ),
                        textAlign: TextAlign.right,
                      )),
                Padding(
                  padding: const EdgeInsets.only(right: 12),
                  child: Icon(Icons.arrow_forward_ios,size: 20,color: Color(0xFF919395),),
                )],
              ))
        ],
      ),
    );
  }

   Widget textViewPink(BuildContext context, String lbl, String text,
      {color: Colors.white,colorText: AppTheme.darkerText}) {
    return Container(
      // margin: AppTheme.edgeHomeDash1,
      width: MediaQuery.of(context).size.width,
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Padding(

                  padding: AppTheme.edgeHomeDash,
                  child: Text(
                    (lbl),
                    style: TextStyle(color:AppTheme.txtNormalColor,fontSize: 16,fontWeight: FontWeight.bold ),
                    textAlign: TextAlign.left,
                  ))),
          Container(
              child: Row(
                children: [
                  Padding(
                      padding: AppTheme.edgeHomeDash,
                      child: Text(
                        text,
                        style: TextStyle(color:colorText,fontSize: 22,fontWeight: FontWeight.bold ),
                        textAlign: TextAlign.right,
                      )),
                  Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: Icon(Icons.arrow_forward_ios,size: 20,color: AppTheme.mainColor,),
                  )],
              ))
        ],
      ),
    );
  }

   Widget textDrawableStart(String text, IconData icons, Color colorText,
      {colorDrawable, fontSize: 14, drawableSize: 16, isStart: true}) {
    return Text.rich(
      TextSpan(

        style: TextStyle(
          color: colorText,
          fontSize:
              fontSize == null ? double.parse('14') : double.parse('$fontSize'),
        ),
        children: [
          isStart?  WidgetSpan(
            child: Container(
              width: 10,
              margin: EdgeInsets.only(right: 5),
              child: Icon(icons,
                  color: colorDrawable == null ? Theme.of(this).primaryColor : colorDrawable,
                  size: fontSize == null
                      ? double.parse('16')
                      : double.parse('$drawableSize')),
            )
          ):TextSpan(
            text: '',
          ),
          TextSpan(
            text: '\u0020\u0020\u0020',
          ),
          TextSpan(
            text: text,
          ),!isStart?  WidgetSpan(
            child: Container(
                width: 10,
                margin: EdgeInsets.only(left: 5),
                child: Icon(icons,
                color: colorDrawable == null ? Theme.of(this).primaryColor : colorDrawable,
                size: fontSize == null
                    ? double.parse('16')
                    : double.parse('$drawableSize'))),
          ):TextSpan(
            text: '',
          )
        ],
      ),
    );
  }
}
