import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MySelectionFormField<T> extends FormField<T> {
  MySelectionFormField(
      {
        this.obs,
        Key key,
      @required T initialValues,
      @required List<T> options,
      @required Widget Function(T, bool) titleBuilder,
      Widget Function(T) subtitleBuilder,
      Widget hint,
      this.decoration = const InputDecoration(enabledBorder: OutlineInputBorder(gapPadding: 0)),
      this.onChanged,
      FormFieldSetter<T> onSaved,
      FormFieldValidator<T> validator,
      bool autovalidate = false,
      Widget disabledHint,
      int elevation = 0,
      TextStyle style,
      Widget underline,
      Widget icon,
      Color iconDisabledColor,
      Color iconEnabledColor,
      Color activeColor,
      Color checkColor,
      double iconSize = 24.0,
      bool isDense = true,
      bool isExpanded = false,
      double itemHeight,
      bool autofocus = false,
      FocusNode focusNode,
      Color focusColor,
      bool isItemdense,
      bool isItemThreeLine = false,
      String deleteButtonTooltipMessage,
      this.name})
      : assert(
          options == null ||
              initialValues == null ||
              ((value) =>
                      options.where((T option) {
                        return option == value;
                      }).length ==
                      1) !=
                  null,
          'There should be exactly one item with [DropdownButton]\'s value: '
          '$initialValues. \n'
          'Either zero or 2 or more [DropdownMenuItem]s were detected '
          'with the same value',
        ),
        assert(decoration != null),
        assert(elevation != null),
        assert(iconSize != null),
        assert(isDense != null),
        assert(isExpanded != null),
        assert(itemHeight == null || itemHeight > 0),
        assert(autofocus != null),
        assert(isItemThreeLine != null),
        super(
          key: key,
          onSaved: onSaved,
          initialValue: initialValues,
          validator: validator,
          // autovalidate: autovalidate,
          builder: (FormFieldState<T> field) {
            final InputDecoration effectiveDecoration = decoration.applyDefaults(
              Theme.of(field.context).inputDecorationTheme,
            );

            return InputDecorator(
              decoration: effectiveDecoration.copyWith(errorText: field.errorText),
              isEmpty: field.value == null,
              isFocused: false, //focusNode?.hasFocus,
              child: MySelectionField<T>(
                value: field.value??(options.length > 0?options[0]:null),
                options: options,
                titleBuilder: titleBuilder,
                subtitleBuilder: subtitleBuilder,
                hint: field.value != null ? hint : null,
                onChanged: field.didChange,
                disabledHint: disabledHint,
                elevation: elevation,
                style: style,
                underline: underline,
                icon: icon,
                iconDisabledColor: iconDisabledColor,
                iconEnabledColor: iconEnabledColor,
                activeColor: activeColor,
                checkColor: checkColor,
                iconSize: iconSize,
                isDense: isDense,
                isExpanded: isExpanded,
                itemHeight: itemHeight,
                focusNode: focusNode,
                focusColor: focusColor,
                autofocus: autofocus,
                isItemdense: isItemdense,
                isItemThreeLine: isItemThreeLine,
                deleteButtonTooltipMessage: deleteButtonTooltipMessage,
              ),
            );
          },
        );

  final ValueChanged<T> onChanged;
  final Rx<T> obs;
  final InputDecoration decoration;
  final name;

  @override
  FormFieldState<T> createState() => _MySelectionFormFieldState<T>(this.name);
}

class _MySelectionFormFieldState<T> extends FormFieldState<T> {



  @override
  MySelectionFormField<T> get widget => super.widget;

  final String name;

  _MySelectionFormFieldState(this.name);

  @override
  void didChange(T value) {
    super.didChange(value);
    // (Form.of(context) as MyFormState).values[name] = value;
    // this.obs.value = value;
    if(widget.obs != null) {
      widget.obs.value = value;
    }
    if (this.hasError) {
      this.validate();
    }
    if (widget.onChanged != null) {
      widget.onChanged(value);
    }
  }


}

// fields/myMultiselectionField.dart ************************

class MySelectionField<T> extends StatelessWidget {
  MySelectionField({
    Key key,
    this.value,
    @required this.options,
    this.titleBuilder,
    this.subtitleBuilder,
    this.hint,
    @required this.onChanged,
    this.disabledHint,
    this.elevation = 0,
    this.style,
    this.underline,
    this.icon,
    this.iconDisabledColor,
    this.iconEnabledColor,
    this.activeColor,
    this.checkColor,
    this.iconSize = 24.0,
    this.isDense = false,
    this.isExpanded = false,
    this.itemHeight,
    this.autofocus = false,
    this.focusNode,
    this.focusColor,
    this.isItemdense,
    this.isItemThreeLine = false,
    this.deleteButtonTooltipMessage,
  })  : //assert(options == null || options.isEmpty || value == null
        // values.every(
        //         (value) => options.where((T option) {
        //       return option == value;
        //     }).length == 1
        // )
        //),

        // assert(onChanged != null),
        assert(iconSize != null),
        assert(isDense != null),
        assert(isExpanded != null),
        assert(autofocus != null),
        assert(isItemThreeLine != null),
        super(key: key);

  final ValueChanged<T> onChanged;
  T value;
  final List<T> options;
  final Widget hint;
  final Widget disabledHint;
  final Widget Function(T, bool) titleBuilder;
  final Widget Function(T) subtitleBuilder;

  final int elevation;
  final TextStyle style;

  final Widget underline;
  final Widget icon;
  final Color iconDisabledColor;
  final Color iconEnabledColor;
  final Color activeColor;
  final Color checkColor;
  final double iconSize;
  final bool isDense;
  final bool isExpanded;
  final double itemHeight;
  final Color focusColor;
  final FocusNode focusNode;
  final bool autofocus;
  final bool isItemThreeLine;
  final bool isItemdense;
  final String deleteButtonTooltipMessage;

  @override
  Widget build(BuildContext context) {
    return Stack(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      // crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        DropdownButtonHideUnderline(
          child: DropdownButton<T>(
            isExpanded: true,
            value: value,

            items: options
                .map<DropdownMenuItem<T>>(
                  (T option) => DropdownMenuItem<T>(value: option, child: titleBuilder(option, value == option)),
                )
                .toList(),
            // selectedItemBuilder: (BuildContext context) {
            //   return options.map<Widget>((T option) {
            //     return Text('');
            //   }).toList();
            // },
            hint: hint,
            onChanged: (T value) {
              this.value = value;
              if(onChanged != null)  onChanged(value);
            },
            disabledHint: disabledHint,
            elevation: elevation,
            style: style,
            underline: underline,
            icon: icon,
            iconDisabledColor: iconDisabledColor,
            iconEnabledColor: iconEnabledColor,
            iconSize: iconSize,
            isDense: isDense,
            // isExpanded: isExpanded,
            itemHeight: itemHeight,
            focusNode: focusNode,
            focusColor: focusColor,
            autofocus: autofocus,
          ),
        ),
      ],
    );
  }
}
