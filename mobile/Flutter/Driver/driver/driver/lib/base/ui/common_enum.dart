
class CommonEnum {
  static bool _isEnumItem(enumItem, List parse) {
    // final split_enum = enumItem.toString().split('.');
    return parse.length > 1 &&
        parse[0] == enumItem.runtimeType.toString();
  }
  static List parse(enumItem) {
    if (enumItem == null) return null;


    final _tmp = enumItem.toString().split('.');
    assert(_isEnumItem(enumItem, _tmp),
    '$enumItem of type ${enumItem.runtimeType.toString()} is not an enum item');
    return _tmp;
  }

  static String name(enumItem){
    if(enumItem== null)return '';
    List parseItem = CommonEnum.parse(enumItem);
    return parseItem[1];
  }
  static dynamic fromString(List enums, String name){
    return enums.singleWhere((element) => CommonEnum.name(element) == name);
  }
}
