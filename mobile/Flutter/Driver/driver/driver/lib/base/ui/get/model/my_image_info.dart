import 'package:multi_image_picker/multi_image_picker.dart';

class MyImageInfo {
  int id;
  Asset asset;
  String url;
  String description;
  bool isDelete = false;
  MyImageInfo(this.id, this.asset, this.url, this.description, {this.isDelete=false});
}