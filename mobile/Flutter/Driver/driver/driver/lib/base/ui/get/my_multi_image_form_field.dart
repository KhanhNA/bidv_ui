import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'model/my_image_info.dart';
import 'my_multi_image_field.dart';

class MyMultiImageFormField extends FormField {
  RxList<MyImageInfo> images;
  final String name;
  final int arrId;
  final ValueChanged<List<MyImageInfo>> onChanged;
  final bool readOnly;

  MyMultiImageFormField({
    Key key,
    this.name,
    this.arrId,
    this.images,
    this.onChanged,
    this.readOnly = false,
    // List<MyImageInfo> initialValues,
    FormFieldSetter<List<MyImageInfo>> onSaved,
    FormFieldValidator<List<MyImageInfo>> validator,
  }) : super(
            key: key,
            onSaved: onSaved,
            initialValue: images.value,
            validator: validator,
            builder: (FormFieldState field) {

              return MyMultiImageField(
                  images: images, onChanged: field.didChange, readOnly: readOnly,);
            });

  @override
  _MyMultiImageFormFieldState createState() =>
      new _MyMultiImageFormFieldState();
}

class _MyMultiImageFormFieldState extends FormFieldState {
  final double imgWidth = 300;
  final double imgHeight = 300;
  // final String name;
  // List<MyImageInfo> images;
  // String _error = 'No Error Dectected';

  _MyMultiImageFormFieldState();

  @override
  MyMultiImageFormField get widget => super.widget;

  @override
  void save() {
    super.save();
    // (Form.of(context) as MyFormState)?.updateChange(widget.name, widget.arrId,value);
  }

  @override
  void didChange(dynamic value) {
    super.didChange(value);
    // (Form.of(context) as MyFormState)?.updateChange(widget.name, widget.arrId,value);
    if (this.hasError) {
      this.validate();
    }
    if (widget.onChanged != null) {
      widget.onChanged(value);
    }
  }

  @override
  void initState() {
    super.initState();
  }


}
