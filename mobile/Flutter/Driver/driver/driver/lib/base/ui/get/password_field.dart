
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:driver/theme/app_theme.dart';

import 'my_text_form_field.dart';


class PasswordField extends StatefulWidget {
  PasswordField({Key key, this.obs}) : super(key: key);

  RxString obs;
  @override
  State<StatefulWidget> createState() => _PasswordField();
}
class _PasswordField extends State<PasswordField>{

  bool _showPassword = false;

  @override
  PasswordField get widget => super.widget;

  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
          child: new Column(
            children: <Widget>[
              new MyTextFormField(
                obs: widget.obs,

                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: new EdgeInsets.symmetric(vertical: 10.0, horizontal: 0),
                  prefixIcon: Icon(Icons.lock),
                  border: new OutlineInputBorder(
                      borderSide:
                      new BorderSide(color: AppTheme.textFieldNomalBorder)),
                  focusedBorder: new OutlineInputBorder(
                      borderSide: new BorderSide(color: AppTheme.mainColor)),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      _togglevisibility();
                    },
                    child: Icon(
                      _showPassword ? Icons.visibility : Icons.visibility_off,
                      color: Colors.red,
                    ),
                  ),
                ),
                validator: (val) => val.length < 6 ? 'Password too short.' : null,

                // onSaved: (val) => _password = val,
                obscureText: !_showPassword,
              ),
            ],
          ))
    );
  }

}