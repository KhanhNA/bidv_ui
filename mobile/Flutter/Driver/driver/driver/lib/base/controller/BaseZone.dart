import 'dart:async';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:driver/base/util/Utils.dart';
import 'package:driver/base/notify/notifies.dart';
extension GetViewExt on GetView{


  // R run<R>(body, {showError=true, String message}){
  //   bool isOK = true;
  //   R r= runZonedGuarded(body, (error, stack){
  //     isOK = false;
  //     BaseZone.printError(error, stack, showError, message);
  //   });
  //   return r;
  // }
  Future<R> run<R>(body, {showError=true, String message}){
    // bool isOK = true;
    // return runZonedGuarded(body, (error, stack){
    //
    //   BaseZone.printError(error, stack, showError, message);
    // });
    return BaseZone.run(() => body());
  }
}
class BaseZone{

  // static final specification = new ZoneSpecification(
  //     registerCallback: <R>(Zone self, ZoneDelegate parent, Zone zone, R Function() f) {
  //       // var stackTrace = currentStackTrace;
  //       return parent.registerCallback<R>(zone, () {
  //         // lastStackTrace = stackTrace;
  //
  //         R r= f();
  //         Get.snackbar('title', 'speci ok');
  //         return r;
  //       });
  //     },
  //     registerUnaryCallback: <R,T>(self, parent, zone, R Function(T arg) f) {
  //       // var stackTrace = currentStackTrace;
  //       return parent.registerUnaryCallback<R,T>(zone, (arg) {
  //         // lastStackTrace = stackTrace;
  //         return f(arg);
  //       });
  //     },
  //     registerBinaryCallback: <R, T1, T2>(self, parent, zone,  R Function(T1 arg1, T2 arg2) f) {
  //       // var stackTrace = currentStackTrace;
  //       return parent.registerBinaryCallback<R, T1, T2>(zone,  (T1 arg1, T2 arg2) {
  //         // lastStackTrace = stackTrace;
  //         return f(arg1, arg2);
  //       });
  //     },
  //     handleUncaughtError: (self, parent, zone, error, stackTrace) {
  //       // if (lastStackTrace != null) print("last stack: $lastStackTrace");
  //       return parent.handleUncaughtError(zone, error, stackTrace);
  //     });



  static printError(error, stack, showError, String title, String message){
    print("Exception $error");
    print("StackTrace $stack");
    if(!showError){
      return;
    }
    if(error is AbstractNotify){
      // Utils.snackBarSuccess(error.title??title, error.message??message);
      error.showNotify();
      return;
    }
    if(message != null){
      // Get.snackbar('title', message);
      Utils.snackBarSuccess(title, message);
      return;
    }


    if(error is DioError && error.response != null && error.response.data != null ){
      String strErr = error.response.data['localizedMessage']??error.response.data['message']??error.response.data['error']??'unknownError';
      // Get.snackbar('title', strErr);
      Utils.snackBarError(title, strErr);
    }else{
      Utils.snackBarError(title, error.toString());
      // Get.snackbar('title', error.toString());
    }

  }
  static Future<R> run<R>(Future<R> Function() body, {showError=true,
    String errorTitle, String errorMessage, showSuccess=false,
    String successMessage='successMessage', void Function(dynamic, dynamic) onError}) async {

    if(body == null){
      return null;
    }
    try {
      final result = await body();
      if(showSuccess){
        Get.snackbar('titleSuccess'.tr, successMessage.tr);
      }
      return result;
    }catch(error, stack){

      BaseZone.printError(error, stack, showError,errorTitle??'errorTitle'.tr, errorMessage);
      if(onError != null) {
        onError(error, stack);
      }
      return null;
    }

  }
}

extension GetxControllerExt on GetxController{

  // bool run<R>(body, {showError=true, String message}){
  //   bool isOK = true;
  //    runZonedGuarded(body, (error, stack){
  //     isOK = false;
  //    BaseZone.printError(error, stack, showError, message);
  //   });
  //   return isOK;
  // }
  Future<R> run<R>(body, {showError=true, String message}){
    // bool isOK = true;
    // return runZonedGuarded(body, (error, stack){
    //
    //   BaseZone.printError(error, stack, showError, message);
    // });
    return BaseZone.run(() => body());
  }
}
extension LoadingMoreBaseExt on LoadingMoreBase{
  Future<R> run<R>(body, {showError=true, String message}){
    // bool isOK = true;
    // return runZonedGuarded(body, (error, stack){
    //
    //   BaseZone.printError(error, stack, showError, message);
    // });
    return BaseZone.run(() => body());
  }
}
