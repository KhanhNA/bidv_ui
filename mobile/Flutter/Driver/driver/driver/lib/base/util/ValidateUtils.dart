
class ValidateUtils {
    ValidateUtils();

    static bool isNullOrEmptyList(List list) {
        return list == null || list.isEmpty;
    }

    static bool isNullOrEmptyObj(Object obj) {
        return obj == null;
    }

    static bool isNullOrEmptyInt(int obj) {
        return obj == null || obj == 0;
    }

    static bool isNullOrEmptyStr(String str) {
        return str == null || str.trim() == '';
    }

}
