import 'package:intl/intl.dart';

extension StringUtils on String{
  DateTime toDate(String type){
    return DateFormat(type).parse(this);
  }
  int toInt(){
    return int.parse(this);
  }
}