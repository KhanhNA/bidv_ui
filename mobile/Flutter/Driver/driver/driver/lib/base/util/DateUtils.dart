
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import 'ValidateUtils.dart';
extension DatetimeUtils on DateTime{
    plus({int year = 0, int month = 0}){
        DateTime tmp = DateTime(this.year + year, this.month + month + 1, 0);
        int day = this.day > tmp.day? tmp.day: this.day;

        return DateTime(tmp.year, tmp.month, this.day, day, this.hour, this.second, this.millisecond, this.microsecond);
    }
    String formatToDate(){
        return this.format('dd/MM/yyyy');
    }
    String formatToMinute(){
        return this.format('dd/MM/yyyy HH:mm');
    }
    String formatToSecond(){
        return this.format('dd/MM/yyyy HH:mm:ss');
    }
    String format(String type){
        return DateFormat(type).format(this);
    }
}
class DateTimeUtils {
    static final String DATE_SHOW_FORMAT = "dd/MM/yyyy";
    static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
    static final String DATE_JSON = "yyyy-MM-dd";


    static String convertDateToString(DateTime date, String type) {
        if (ValidateUtils.isNullOrEmptyObj(date)) {
            return "";
        }
        String result;

        result = DateFormat(type).format(date);
        return result;
    }

    // static String clearTimeAndConvertDateToString(Date date, String type) {
    //     String result;
    //     Calendar calendar = Calendar.getInstance();
    //     calendar.setTime(date);
    //     date = convertDayMonthYearToDate(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    //     SimpleDateFormat formatter = new SimpleDateFormat(type);
    //     result = formatter.format(date);
    //     return result;
    // }

    static DateTime convertStringToDate(String date, String type) {

        try {
            return date == null? null:DateFormat(type).parse(date);
        } catch (error, stacktrace){
            debugPrint('error: $error & stacktrace: $stacktrace');
        }
        return null;
    }
    static String convertFormatDateV1 (DateTime time){
        final DateFormat formatter = DateFormat(DATE_FORMAT);
        final String formatted = formatter.format(time);
        return formatted;
    }

    static String dateToDisplayString(DateTime time){
        final DateFormat formatter = DateFormat(DATE_SHOW_FORMAT);
        final String formatted = formatter.format(time);
        return formatted;
    }

    static String dateStringToDisplayString(String date){
        if (date==null || date.isEmpty) return "";
        try {
            DateTime datetime = DateFormat(DATE_FORMAT).parse(date);
            final DateFormat formatter = DateFormat(DATE_SHOW_FORMAT);
            final String formatted = formatter.format(datetime);
            return formatted;
        } catch (error, stacktrace){
            debugPrint('error: $error & stacktrace: $stacktrace');
        }
        return "";
    }

    // static Date convertDayMonthYearToDate(int day, int month, int year) {
    //     Calendar calendar = new GregorianCalendar();
    //     calendar.set(year, month, day);
    //     calendar.set(Calendar.HOUR_OF_DAY, 0);
    //     calendar.set(Calendar.MINUTE, 0);
    //     calendar.set(Calendar.SECOND, 0);
    //     calendar.set(Calendar.MILLISECOND, 0);
    //     return calendar.getTime();
    // }
    //
    // static String convertDayMonthYearToString(int day, int month, int year, String type) {
    //     Date date = convertDayMonthYearToDate(day, month, year);
    //     return convertDateToString(date, type);
    // }
    //
    static String convertStringDateToDifferentType(String date, String currentFormat, String displayFormat) {
        if (ValidateUtils.isNullOrEmptyStr(date)) {
            return "";
        }
        return DateTimeUtils.convertDateToString(DateTimeUtils.convertStringToDate(date, currentFormat), displayFormat);
    }
    //
    // static Date getCurrentDate() {
    //     Calendar calendar = Calendar.getInstance();
    //     int day = calendar.get(Calendar.DAY_OF_MONTH);
    //     int month = calendar.get(Calendar.MONTH);
    //     int year = calendar.get(Calendar.YEAR);
    //     return DateUtils.convertDayMonthYearToDate(day, month, year);
    //
    // }
    //
    // static Date reduceOneMonth(Date date) {
    //     Calendar cal = Calendar.getInstance();
    //     cal.setTime(date);
    //     cal.add(Calendar.MONTH, -1);
    //     return cal.getTime();
    // }

}
