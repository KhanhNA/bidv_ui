


import 'api_auth_provider.dart';
import 'token.dart';
import 'refresh_token_body.dart';
class ApiAuthRepository {
  final ApiAuthProvider _apiAuthProvider = ApiAuthProvider();


  Future<Token> postRefreshAuth(RefreshTokenBody refreshTokenBody) => _apiAuthProvider.refreshAuth(refreshTokenBody);


}