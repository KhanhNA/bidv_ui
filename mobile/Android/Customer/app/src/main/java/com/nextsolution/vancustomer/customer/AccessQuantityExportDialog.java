package com.nextsolution.vancustomer.customer;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.AccessQuantityExportDialogVM;
import com.nextsolution.vancustomer.databinding.AccessQuatityExportDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;

public class AccessQuantityExportDialog extends DialogFragment {
    AccessQuantityExportDialogVM dialogVM;
    AccessQuatityExportDialogBinding mBinding;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        dialogVM.getQuantityExport().set(bundle.getInt("QUANTITY_EXPORT"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.access_quatity_export_dialog, container, false);
        dialogVM = ViewModelProviders.of(this).get(AccessQuantityExportDialogVM.class);
        mBinding.setViewModel(dialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mBinding.getRoot();
    }
    public void onClick(View v, ViewModel vm){
        if(v.getId()==R.id.btnAccess){
            if(dialogVM.getQuantityExport() !=null && dialogVM.getQuantityExport().get()!=0){
                backFragment();
            }
            else{
                mBinding.llTitle.setText("");
                mBinding.llTitle.setHint(getResources().getString(R.string.pass_empty));
            }
        }
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
    public void backFragment(){
        Intent intent = new Intent();
        intent.putExtra(Constants.DATA_RESULT, dialogVM.getQuantityExport());
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
        this.dismiss();
    }
}
