package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.nextsolution.db.dto.Subscribe;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.RecurrentViewModel;
import com.nextsolution.vancustomer.databinding.SubscribeDialogBinding;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class DialogSubscribe extends DialogFragment {
    private OnConfirm onConfirm;
    private XBaseAdapter adapter;
    private SubscribeDialogBinding binding;

    RecurrentViewModel recurrentViewModel;
    public DialogSubscribe(DialogSubscribe.OnConfirm onConfirm){
        this.onConfirm = onConfirm;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.subscribe_dialog, container, false);
        recurrentViewModel = ViewModelProviders.of(this).get(RecurrentViewModel.class);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        adapter = new XBaseAdapter(R.layout.item_subs, recurrentViewModel.getListSubs(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                onConfirm.onConfirm(((Subscribe)o));
                dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        binding.rcSubs.setAdapter(adapter);
        binding.spinKit.setVisibility(View.VISIBLE);
        recurrentViewModel.getSubs(this::abc);
        return binding.getRoot();
    }


    private void abc(Object[] objects) {
        String action = (String) objects[0];
        if(action.equals("getSubs")){
            adapter.notifyDataSetChanged();
            binding.spinKit.setVisibility(View.GONE);
        }
    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        return new Dialog(requireContext(), R.style.WideDialog);
//    }

    public interface OnConfirm{
        void onConfirm(Subscribe selectedSubs);
    }
}
