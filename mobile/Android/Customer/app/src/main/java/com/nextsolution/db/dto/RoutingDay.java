package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.model.Vehicle;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingDay extends BaseModel {

    Integer id;
    Integer routing_plan_day_id;
    Integer bill_routing_id;
    String bill_routing_name;
    String routing_plan_day_code;
    String bill_lading_detail_code;
    OdooDateTime actual_time;
    OdooDateTime expected_from_time;
    OdooDateTime expected_to_time;
    String name;
    String license_plate;
    String type; // 0 kho xuất, 1 kho nhập
    String status;
    String trouble_type;
    OdooDateTime date_plan;
    Driver driver;
    private Integer change_bill_lading_detail_id;
    private List<BillService> services;
    private Vehicle vehicle;
    private Double latitude;
    private Double longitude;
    private Boolean arrived_check;
    private String address;
    private String phone;
    private String booked_employee;
    private String insurance_name;
    private String warehouse_name;
    private List<String> image_urls;
    private List<RoutingDetail> routing_plan_day_detail = new ArrayList<>();
    private List<BillPackage> list_bill_package;
    private Boolean sos_ids;
    private RatingDriverDTO rating_drivers;
    transient Integer quantityBillPackage;

    private String driver_name;
    private String driver_phone;
    private OdooDateTime accept_time;
    private OdooDateTime confirm_time;
    private Boolean so_type;

    public String getAccept_time_str(){
        if(this.accept_time == null){
            return "";
        }
        return AppController.formatDateTime.format(accept_time);
    }

    public String getExpectedFromTimeStr(){
        if(this.expected_from_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_from_time);
    }

    public String getExpectedToTimeStr(){
        if(this.expected_to_time == null){
            return "";
        }
        return AppController.formatTime.format(expected_to_time);
    }

}
