package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.WarehouseApi;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChooseWarehouseDialogVM extends BaseViewModel {
    Integer position = 0;
    ObservableField<Boolean> isReload= new ObservableField<>();
    List<Warehouse> warehouses;

    public ChooseWarehouseDialogVM(@NonNull Application application) {
        super(application);
        warehouses = new ArrayList<>();
        isReload.set(true);
    }

    public void getWareHouse(RunUi runUi) {
        if(StaticData.getPartner().getCompany_id() == null){
            runUi.run("companyNull");
            return;
        }
        WarehouseApi.getCustWarehouse(StaticData.getPartner().getCompany_id().getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        try{
            OdooResultDto<Warehouse> resultDto = (OdooResultDto<Warehouse>) o;
            Warehouse warehouse= new Warehouse();
            warehouse.setWarehouse_code(AppController.getInstance().getResources().getString(R.string.ALL));
            warehouse.checked=true;
            warehouses.add(warehouse);
            if (resultDto != null && resultDto.getRecords() != null) {
                warehouses.addAll(resultDto.getRecords());
                runUi.run(Constants.GET_DATA_SUCCESS);
            }
            else{
                runUi.run(Constants.GET_DATA_FAIL);
            }
            isReload.set(false);
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
