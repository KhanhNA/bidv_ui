package com.nextsolution.db.dto;

public interface IModel {
    String getModelName();
}
