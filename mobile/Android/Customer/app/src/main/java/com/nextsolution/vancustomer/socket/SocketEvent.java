package com.nextsolution.vancustomer.socket;

public interface SocketEvent {
    void handleMessage(Object msg);
}
