package com.nextsolution.db.dto;


import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillCycle extends BaseModel {
    private Integer id;
    private Integer type;
    private String name;
    private Date startDate;
    private Date endDate;

}
