package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.model.BiddingVehicleOrder;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingVehicle extends BaseModel {
    private Integer id;

    private Integer vehicle_id;

    private Object code;

    private String lisence_plate;

    private String driver_phone_number;

    private String driver_name;

    private OdooDateTime expiry_time;

    private Integer company_id;

    private String status;

    private String description;

    private String id_card;

    private Integer res_partner_id;

    private Integer tonnage;

    private Integer vehicle_type;

    private Integer weight_unit;

    private String bidding_vehicle_seq;
    //list cargo ứng với xe
    private List<CargoTypes> cargo_types;

    private BiddingVehicleOrder bidding_order_receive;
    private BiddingVehicleOrder bidding_order_return;

    /**
     *
     * @return trạng thái của xe với đơn hàng:
     *       0: chưa nhận hàng.
     *       1: Đã nhận hàng (đang vận chuyển).
     *       2: Đã trả hàng.
     *      -1: đã hủy
     */
    public int getVehicleOrderStatus(){
        if ("2".equals(bidding_order_return.getStatus())
        ) {
            // trả hàng.
            return 2;
        }
        if ("-1".equals(bidding_order_return.getStatus())
                || "-1".equals(bidding_order_receive.getStatus())) {
            return -1;
        }
        if ("0".equals(bidding_order_receive.getStatus())) {
            //chưa vận chuyển.
            return 0;
        }
        if ("1".equals(bidding_order_receive.getStatus())) {
            // đang vận chuyển.
            return 1;
        }
        //chưa vận chuyển.
        return 0;
    }

}