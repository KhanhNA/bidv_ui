package com.nextsolution.vancustomer.ui.fragment;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.databinding.FragmentIncentivePackingBinding;
import com.nextsolution.vancustomer.viewmodel.ListViewModel;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListDialogFragment extends BottomSheetDialogFragment {
    private ListViewModel listVM;
    private int itemLayout;
    private AdapterListener listener;
    private int title;
    private String description;
    private XBaseAdapter adapter;
    private GetList getList;
    private List<BaseModel> lstData;
    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, GetList getList, AdapterListener adapterListener) {
        this.itemLayout = itemLayout;
        this.getList = getList;
        this.listener = adapterListener;
        this.title = title;
    }

    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, String description, List<BaseModel> lstData, AdapterListener adapterListener) {
        this.lstData = lstData;
        this.itemLayout = itemLayout;
        this.listener = adapterListener;
        this.title = title;
        this.description = description;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentIncentivePackingBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_incentive_packing, container, false);
        binding.txtTitle.setText(getString(title));
        listVM = ViewModelProviders.of(this).get(ListViewModel.class);
        if (lstData != null) {
            this.listVM.getListData().addAll(lstData);
        }
        if (description != null) {
            binding.txtMsg.setText(description);
        } else {
            binding.txtMsg.setVisibility(View.GONE);
        }

        adapter = new XBaseAdapter(itemLayout, listVM.getListData(), listener);
        RecyclerView rcIncentive = binding.rcIncentiveProduct;
        rcIncentive.setAdapter(adapter);
        rcIncentive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinimumHeight(height / 2);
        if(getList != null) {
            getList.getList(listVM, this::RunUi);
        }


        return binding.getRoot();
    }

    public void RunUi(Object[] objects) {
        adapter.notifyDataSetChanged();
    }

    public interface GetList{
        void getList(ListViewModel listVM, RunUi runUi);
    }
}