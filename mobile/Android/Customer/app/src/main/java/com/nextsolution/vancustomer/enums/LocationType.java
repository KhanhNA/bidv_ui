package com.nextsolution.vancustomer.enums;

public class LocationType {
    public static final String DISTRICT="district";
    public static final String PROVINCE="province";
}
