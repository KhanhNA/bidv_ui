package com.nextsolution.db.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Company {
    private String name;
    private String partner_id;
    private String currency_id;
    private String user_ids;

    private String country_id;
    private String paperformat_id;
    private String base_onboarding_company_state;
    private String favicon;
    private String font;
//    List<Warehouse> warehouse_ids;
    private Long id;
    private String display_name;


}
