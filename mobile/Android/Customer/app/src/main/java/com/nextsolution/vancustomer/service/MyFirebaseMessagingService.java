package com.nextsolution.vancustomer.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolution.db.api.BaseApi;
import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.ObjectStatus;
import com.nextsolution.vancustomer.service.service_dto.NotificationService;
import com.nextsolution.vancustomer.ui.LoginActivityV2;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import org.greenrobot.eventbus.EventBus;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "NOTIFICATION_FIREBASE_FROM";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // phương thức được gọi khi đang  mở appppppppppppppppppppppppppppppppppp
        Log.d(TAG, remoteMessage.getFrom());

        try {
            final NotificationService notificationService = NotificationService.of(remoteMessage.getData());//mapper.convertValue(remoteMessage.getData(), NotificationService.class);
            EventBus.getDefault().post(notificationService);
            if (notificationService != null) {
                sendNotification(notificationService);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(NotificationService notification) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        NotificationDTO notificationDTO = gson.fromJson(notification.getMess_object(), NotificationDTO.class);

        //logout when edit phone number driver
        if (ObjectStatus.THREE.equals(notificationDTO.getObject_status())) {
            logout();
            return;
        }

        Intent notificationIntent = new Intent(notification.getClick_action());

        if (notification.getItem_id() != null) {
            notificationIntent.putExtra(Constants.ITEM_ID, "" + notification.getItem_id());
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "share_vans_notification";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription(notification.getBody());
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
//                .setSound(Uri.parse("android.resource://"
//                        + getPackageName() + "/" + R.raw.notification_bell))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_vans)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle(notification.getTitle())
                .setContentIntent(pendingIntent)
                .setContentText(notification.getBody());
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    private void logout() {
        // Delete token firebase
        BaseApi.logout();
        // Delete user & pass
        AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
        // Delete cache
        StaticData.setOdooSessionDto(null);
        StaticData.setPartner(null);
        StaticData.sessionCookie = "";

        // Intent loginActivity
        Intent intent = new Intent(this, LoginActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }
}
