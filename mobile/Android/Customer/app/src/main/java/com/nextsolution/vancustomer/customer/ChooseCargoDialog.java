package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.ChooseCargoDialogVM;
import com.nextsolution.vancustomer.databinding.ChooseCargoDialogFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.listener.AdapterListener;

public class ChooseCargoDialog extends DialogFragment {

    ChooseCargoDialogVM chooseCargoDialogVM;
    ChooseCargoDialogFragmentBinding mBinding;
    XBaseAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.choose_cargo_dialog_fragment, container, false);
        chooseCargoDialogVM = ViewModelProviders.of(this).get(ChooseCargoDialogVM.class);
        mBinding.setViewModel(chooseCargoDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setUpRecycleView();
        chooseCargoDialogVM.getCargoTypes(this::runUI);
        return mBinding.getRoot();
    }
    public void runUI(Object[] objects) {
        String action = (String) objects[0];
        if(action.equals(Constants.GET_DATA_FAIL)){
            ToastUtils.showToast(getActivity(),getResources().getString(R.string.NOT_FOUND_DATA),getResources().getDrawable(R.drawable.ic_fail));
        }else if(action.equals(Constants.GET_DATA_SUCCESS)){
            getData();
            adapter.notifyDataSetChanged();
        }
    }
    public void onClick(View v, ViewModel vm){
        if(v.getId()==R.id.btnClose){
            dismiss();
        }
    }

    public void setUpRecycleView() {
        adapter= new XBaseAdapter(R.layout.choose_cargo_dialog_item, chooseCargoDialogVM.getListData(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                CargoTypes cargoTypes= (CargoTypes) o;
                getResult(cargoTypes,((CargoTypes) o).index-1);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcCargoItem.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mBinding.rcCargoItem.setLayoutManager(layoutManager);
    }
    public void getData(){
        Bundle bundle= getArguments();
        if(bundle!=null){
            int position= bundle.getInt(Constants.DATA,-1);
            chooseCargoDialogVM.getListData().get(0).checked=false;
            chooseCargoDialogVM.getListData().get(position).checked=true;
            adapter.notifyItemChanged(0);
            adapter.notifyItemChanged(position);
        }

    }

    public void getResult(CargoTypes cargoTypes,Integer position) {
        Intent intent = new Intent();
        Bundle bundle= new Bundle();
        bundle.putSerializable(Constants.DATA, cargoTypes);
        bundle.putInt(Constants.POSITION_SELECTED,position);
        intent.putExtras(bundle);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
        this.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
