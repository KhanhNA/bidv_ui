package com.nextsolution.vancustomer.confirm_change;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.ExportDetailPackageAdapter;
import com.nextsolution.vancustomer.adapter.ImageBaseAdapter;
import com.nextsolution.vancustomer.customer.BillLadingInfoActivity;
import com.nextsolution.vancustomer.databinding.DetailExportChangeFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DetailExportChangeFragment extends BaseFragment {
    ConfirmChangeVM confirmChangeVM;
    DetailExportChangeFragmentBinding mBinding;
    ExportDetailPackageAdapter adapter;
    ImageBaseAdapter imageAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(ConfirmChangeVM.class);
        confirmChangeVM = (ConfirmChangeVM) viewModel;
        binding.setVariable(BR.viewModel, viewModel);
        mBinding = (DetailExportChangeFragmentBinding) binding;

        initView();

        return v;

    }

    private void initView() {
        adapter = new ExportDetailPackageAdapter(getContext());
        mBinding.rcBillPackage.setAdapter(adapter);
        mBinding.btnNextStep.setOnClickListener(v -> nextStep());

        imageAdapter = new ImageBaseAdapter(getActivity(), R.layout.item_image_100dp, new ArrayList<>());
        mBinding.rcImages.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mBinding.rcImages.setAdapter(imageAdapter);

        mBinding.txtRoutingCode.setOnClickListener(v -> goToOriginBillLading());

    }

    private void goToOriginBillLading() {
        Intent intent = new Intent(getActivity(), BillLadingInfoActivity.class);
        intent.putExtra(Constants.ITEM_ID, confirmChangeVM.getModelE().getFrom_bill_lading_id());
        startActivity(intent);
    }

    public void nextStep() {
        EventBus.getDefault().post(1);
    }

    public void notifyBillPackageAdapter() {
        adapter.update(confirmChangeVM.getModelE().getArrBillLadingDetail().get(0).getBillPackages());
    }

    public void notifyImageAdapter(){
        imageAdapter.notifyDataSetChange(confirmChangeVM.getModelE().getImage_urls());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.detail_export_change_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmChangeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
