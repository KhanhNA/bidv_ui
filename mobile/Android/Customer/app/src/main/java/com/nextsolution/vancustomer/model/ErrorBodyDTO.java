package com.nextsolution.vancustomer.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorBodyDTO {
    private String message;
    private String status;
    private Integer id;
}
