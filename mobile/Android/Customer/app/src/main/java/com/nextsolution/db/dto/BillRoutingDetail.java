package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BillRoutingDetail extends BaseModel {

    private Double latitude;
    private Integer area_id;
    private Double longitude;
    private String approved_type;
    private String status;
    private String name;
    private Integer id;
    private Double max_price;
    private Double min_price;
    private Double total_weight;

    private Warehouse warehouse;
    private String address;
    private String warehouse_name;
    private String phone;
    private String warehouse_type;//0. nhan, 1. tra
    private String description;
    private List<String> image_urls;

    private String order_type;//0: trong zone, 1: ngoài zone  - so với kho xuất hàng.
    private List<BillService> billService;
    private Integer bill_lading_id;

    private List<BillPackage> billPackages;

    private List<Routing> routing;

}
