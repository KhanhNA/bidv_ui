package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vendor extends BaseModel {
    private Integer id;
    private String name;
    private String phone;
    private String email;
    private String country_id;
    private String city_name;
    private String district;
}
