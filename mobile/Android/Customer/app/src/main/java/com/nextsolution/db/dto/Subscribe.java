package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Subscribe extends BaseModel {
    private Long id;
    private String name;
    private String description;
    private String subscribe_code;

    //tính theo ngày
    private Integer value;
}
