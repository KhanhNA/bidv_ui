package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nextsolution.db.api.IResponse;
import com.nextsolution.db.api.InsuranceApi;
import com.nextsolution.db.api.ServiceApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillService;
import com.nextsolution.db.dto.Insurance;
import com.nextsolution.vancustomer.base.RunUi;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListViewModel extends BaseViewModel {
    private List<BaseModel> listData;

    public ListViewModel(@NonNull Application application) {
        super(application);
        listData = new ArrayList<>();
    }

    public void getInsurance(RunUi runUi) {
        InsuranceApi.getInsurance(true, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                listData.clear();
                listData.addAll(((OdooResultDto<Insurance>)o).getRecords());
                runUi.run();
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getService(RunUi runUi) {
        ServiceApi.getService(true, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                listData.clear();
                listData.addAll(((OdooResultDto<BillService>)o).getRecords());
                runUi.run();
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

}
