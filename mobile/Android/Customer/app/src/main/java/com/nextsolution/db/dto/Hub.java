package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hub extends BaseModel {
    private Integer id;
    private Double latitude;
    private Double longitude;
    private String name;
    private String address;
    private String status;

    private String name_seq;
}
