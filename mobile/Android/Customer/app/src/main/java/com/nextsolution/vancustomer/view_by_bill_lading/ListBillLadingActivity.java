package com.nextsolution.vancustomer.view_by_bill_lading;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.confirm_change.ConfirmEditBillLadingActivity;
import com.nextsolution.vancustomer.customer.OrderFragment;
import com.nextsolution.vancustomer.databinding.BillLadingActivityBinding;
import com.nextsolution.vancustomer.enums.BillRoutingStatus;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.TroubleType;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.Date;

public class ListBillLadingActivity extends BaseActivity<BillLadingActivityBinding> implements CalendarView.OnCalendarSelectListener,
        CalendarView.OnYearChangeListener {
    private BillLadingVM billLadingVM;
    private BaseAdapterV3 billAdapter;
    String date;
    private int mYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billLadingVM = (BillLadingVM) viewModel;
        initToolbar();
        initView();
        initLoadMore();

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, c.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
        Date toDate = c.getTime();
        c.set(java.util.Calendar.MONTH, c.get(java.util.Calendar.MONTH) - 1);
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        Date fromDate = c.getTime();

        date = binding.calendar.getCurYear() + "-" + binding.calendar.getCurMonth() + "-" + binding.calendar.getCurDay();
        billLadingVM.getListDate(fromDate, toDate, this::runUi);
        billLadingVM.getBillLadingByDate(date, false, this::runUi);

    }

    private void initLoadMore() {
        billAdapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            billLadingVM.loadMore(date, this::runUi);
        });
        billAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        billAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getBillLadingSuccess":
                billAdapter.notifyDataSetChanged();
                billAdapter.getLoadMoreModule().loadMoreComplete();
                billAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                billAdapter.getLoadMoreModule().loadMoreEnd();
                break;
            case "getListDate":
                binding.calendar.update();
                break;
        }
    }

    private void initView() {
        billAdapter = new BaseAdapterV3(R.layout.item_bill_lading, billLadingVM.getBillLadingList(), this);
        binding.rcBillLading.setAdapter(billAdapter);
        binding.swRefresh.setOnRefreshListener(() -> {
            billLadingVM.onRefresh(date, this::runUi);
        });
        binding.calendar.setOnCalendarSelectListener(this);
        binding.calendar.setOnYearChangeListener(this);
        binding.calendar.setSchemeDate(billLadingVM.getMScheme());
        binding.calendarLayout.setModeOnlyWeekView();
        binding.calendar.setSelectDefaultMode();
        binding.calendar.setOnMonthChangeListener((year, month) -> getListDateByMonth(month));

    }

    private void getListDateByMonth(int month) {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        c.set(java.util.Calendar.MONTH, month - 2);
        Date fromDate = c.getTime();
        c.set(java.util.Calendar.DAY_OF_MONTH, 10);
        c.set(java.util.Calendar.MONTH, month);
        Date toDate = c.getTime();
        billLadingVM.getListDate(fromDate, toDate, ListBillLadingActivity.this::runUi);
    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemBillLading) {
            BillRouting billRouting = (BillRouting) o;
            Intent intent;
            if (billRouting.getStatus_routing().equals(BillRoutingStatus.In_claim) && billRouting.getTrouble_type().equals(TroubleType.NORMAL)) {
                intent = new Intent(getBaseActivity(), ConfirmEditBillLadingActivity.class);
                intent.putExtra(Constants.ITEM_ID, billRouting.getChange_bill_lading_detail_id() + "");
            } else {
                intent = new Intent(this, BillRoutingDetailActivity.class);
                intent.putExtra(Constants.ITEM_ID, ((BillRouting) o).getId()+"");
            }
            startActivity(intent);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initToolbar() {
        binding.btnBack.setOnClickListener(v -> {
            if (binding.calendar.isYearSelectLayoutVisible()) {
                binding.calendar.closeYearSelectLayout();
            } else {
                finish();
            }
        });
        binding.tvMonthDay.setOnClickListener(v -> {
            binding.calendar.showYearSelectLayout(mYear);
            binding.tvYear.setVisibility(View.GONE);
            binding.tvMonthDay.setText(String.valueOf(mYear));
        });
        binding.flCurrent.setOnClickListener(v -> binding.calendar.scrollToCurrent());
        binding.tvYear.setText(String.valueOf(binding.calendar.getCurYear()));
        mYear = binding.calendar.getCurYear();
        String monthStr = binding.calendar.getCurMonth() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        String dayStr = binding.calendar.getCurDay() + "";
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        binding.tvMonthDay.setText(dayStr + " / " + monthStr);
        binding.tvCurrentDay.setText(String.valueOf(binding.calendar.getCurDay()));
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        binding.tvYear.setVisibility(View.VISIBLE);
        String monthStr = calendar.getMonth() + "";
        String dayStr = calendar.getDay() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        binding.tvMonthDay.setText(dayStr + " / " + monthStr);
        binding.tvYear.setText(String.valueOf(calendar.getYear()));
        mYear = calendar.getYear();
        if (isClick) {
            date = calendar.getYear() + "-" + calendar.getMonth() + "-" + calendar.getDay();
            billLadingVM.getBillLadingByDate(date, false, this::runUi);
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.calendar.isYearSelectLayoutVisible()) {
            binding.calendar.closeYearSelectLayout();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onYearChange(int year) {
        binding.tvMonthDay.setText(String.valueOf(year));

    }

    public void scrollToCurrent() {
        binding.calendar.scrollToCurrent();
    }


    @Override
    public int getLayoutRes() {
        return R.layout.bill_lading_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillLadingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
