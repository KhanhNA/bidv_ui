package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Depot extends BaseModel {
    //type =0 nhận hàng, 1trả hàng
    transient int type=0;
    private Integer id;
    private String name;
    private String phone;
    private String depot_code;
    private Double latitude;
    private Double longitude;
    private String address;
    private String street;
    private String street2;
    private String cityName;

}