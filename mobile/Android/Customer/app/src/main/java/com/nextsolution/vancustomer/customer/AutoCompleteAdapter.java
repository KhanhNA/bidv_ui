package com.nextsolution.vancustomer.customer;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter<T> extends ArrayAdapter implements Filterable {
    private final String MY_DEBUG_TAG = "CustomerAdapter";

    private ArrayList<T> itemsAll;
    private ArrayList<T> suggestions;
    private int viewResourceId;

    public AutoCompleteAdapter(Context context, int viewResourceId, ArrayList<T> items) {
        super(context, viewResourceId, items);
        if(items == null){
            this.itemsAll = new ArrayList<>();
        }else {
            this.itemsAll = items;
        }
        this.suggestions = new ArrayList<T>();
        this.viewResourceId = viewResourceId;
    }
    public void setList(List list){
        this.itemsAll.clear();
        this.itemsAll.addAll(list);
    }
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View v = convertView;
//        if (v == null) {
//            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            v = vi.inflate(viewResourceId, null);
//        }
//        T t = items.get(position);
//        if (t != null) {
//            TextView customerNameLabel = (TextView) v.findViewById(R.id.customerNameLabel);
//            if (customerNameLabel != null) {
//
//                customerNameLabel.setText(t.getName());
//            }
//        }
//        return v;
//    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((T)(resultValue)).toString();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            if(constraint == null){
                suggestions.addAll(itemsAll);
            }else{
                for (T customer : itemsAll) {
                    if(customer.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }

            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = suggestions;
            filterResults.count = suggestions.size();
            return filterResults;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<T> filteredList = (ArrayList<T>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (T c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}
