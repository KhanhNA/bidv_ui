package com.nextsolution.vancustomer.enums;

public class TroubleType {
    public static String NORMAL = "0";
    public static String SOS = "1";
    public static String RETRY = "2";
    public static String RETURN = "3";
    public static String PICKUP_FAIL = "4";
    public static String WAITING_CONFIRM = "5";
}
