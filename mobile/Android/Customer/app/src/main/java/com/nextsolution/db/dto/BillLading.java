package com.nextsolution.db.dto;

import android.content.res.Resources;
import android.widget.TextView;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.enums.StatusType;
import com.nextsolution.vancustomer.model.OrderPackage;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;

import java.util.Calendar;
import java.util.List;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillLading extends BaseModel implements IModel {
    private Integer id;
    private Integer from_bill_lading_id;
    private Integer origin_bill_lading_detail_id;
    private String insurance_name;
    private String subscribe_name;
    private Long company_id;
//    private OdooRelType insurance_id = new OdooRelType();
    private Double total_weight;
    private Integer total_parcel;
    private Double total_amount;
    private Double total_volume;
    private Double vat;
    private List<BillLadingDetail> arrBillLadingDetail;
    private String name_seq = "New";
    private String name;
    private String code;
    private String bill_state;
    private String status = StatusType.RUNNING;
    private String status_routing;
    private OdooDate create_date;

    private OdooDate start_date;
    private OdooDate end_date;

    private String startDateStr;

    private List<String> image_urls;

    private Insurance insurance;
//    private RecurrentModel recurrent;

    private Integer frequency;// 1 daily, 2 weekly, 3 monthly
    private Integer day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
    private Integer day_of_month;// if frequency = 3.
    private Subscribe subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...
    private OrderPackage order_package;//Loại đơn hàng: đơn express, ...
    private Double price_not_discount;
    private Double insurance_price;
    private Double service_price;

    public void setStartDateStr(OdooDate start_date){
        if(start_date == null){
            return ;
        }
        startDateStr =  AppController.formatDate.format(start_date);
    }

    public void setStart_date(OdooDate start_date) {
        this.start_date = start_date;
        setStartDateStr(start_date);
    }
    public String getTotal_amountStr(){
        return AppController.getInstance().formatCurrency(total_amount);
    }

    public String getTotal_weight_str(){
        if(total_weight == null) return "0 kg";
        return AppController.getInstance().formatNumber(total_weight) + " kg";
    }

    public String getTotal_volume_str(){
        if(total_volume == null) return "";
        return AppController.getInstance().formatNumber(total_volume);
    }

    @BindingAdapter("billStatus")
    public static void billStatus(TextView textView, List<BillLadingDetail> arrBillLadingDetail) {
        boolean validReceiptWarehouse = true;
        Resources res = textView.getContext().getResources();
        if (arrBillLadingDetail != null && arrBillLadingDetail.size() > 1) {
            for (BillLadingDetail billRoutingDetail : arrBillLadingDetail) {
                boolean isEmpty = true;
                for (BillPackage billPackage : billRoutingDetail.getBillPackages()) {
                    if (billPackage.getQuantity_package() > 0) {
                        isEmpty = false;
                        break;
                    }
                }
                if (isEmpty){
                    textView.setCompoundDrawablesWithIntrinsicBounds(res.getDrawable(R.drawable.ic_danger), null,
                            null, null);
                    textView.setText(res.getString(R.string.some_warehouse_is_empty));
                    textView.setTextColor(res.getColor(R.color.color_price));
                    return;
                }
            }

            List<BillPackage> listPickup = arrBillLadingDetail.get(0).getBillPackages();
            for (int i = 0; i < listPickup.size(); i++) {
                int total = 0;
                for (int j = 1; j < arrBillLadingDetail.size(); j++) {
                    total += arrBillLadingDetail.get(j).getBillPackages().get(i).getQuantity_package();
                }
                if(total < listPickup.get(i).getQuantity_package() || total > listPickup.get(i).getQuantity_package()){
                    validReceiptWarehouse =  false;
                    break;
                }
            }
        }else {
            validReceiptWarehouse =  false;
        }
        if (validReceiptWarehouse) {
            textView.setCompoundDrawablesWithIntrinsicBounds(res.getDrawable(R.drawable.ic_complete), null,
                    null, null);
            textView.setText(res.getString(R.string.click_next_step_to_continue));
            textView.setTextColor(res.getColor(R.color.primaryColor));
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(res.getDrawable(R.drawable.ic_warning), null,
                    null, null);
            textView.setText(res.getString(R.string.some_item_is_invalid));
            textView.setTextColor(res.getColor(R.color.color_pending));
        }
    }

    public String getStart_date_str(){
        if(start_date != null){
            return AppController.formatDate.format(start_date);
        }
        return  "";
    }

    public String getEnd_date_str(){
        if(end_date != null){
            return AppController.formatDate.format(end_date);
        }
        return  "";
    }

    public void setEndDate(Subscribe subscribe){
        if(subscribe == null || start_date == null){
            end_date = null;
            return;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(start_date);
        c.add(Calendar.DATE, subscribe.getValue()-1);
        end_date = new OdooDate(c.getTime().getTime());
    }

    @Override
    public String getModelName() {
        return "sharevan.billlading";
    }
}
