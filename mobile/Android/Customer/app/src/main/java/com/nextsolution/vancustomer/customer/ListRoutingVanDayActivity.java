package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.GridImageAdapter;
import com.nextsolution.vancustomer.adapter.ImageBaseAdapter;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.ListRoutingVanDayVM;
import com.nextsolution.vancustomer.databinding.ComfirmRoutingVanDayFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.nextsolution.vancustomer.util.RoutingDayStatus;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.view_by_bill_lading.BillRoutingDetailActivity;
import com.nextsolution.vancustomer.widget.FullyGridLayoutManager;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.nextsolution.vancustomer.widget.OnSingleClickListener;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class ListRoutingVanDayActivity extends BaseActivity<ComfirmRoutingVanDayFragmentBinding> implements NetworkManager.NetworkHandler {

    private final int REQUEST_PERMISSION_CAMERA = 1001;
    //    private int preCate = 0;
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    ListRoutingVanDayVM listRoutingVanDayVM;
    XBaseAdapter adapter;
    ImageBaseAdapter adapterImage;
    private GridImageAdapter imageSelectAdapter;
    private ProgressDialog dialog;
    private String objectStatus = "1";
    private String routing_plan_day_code;
    private String FROM_ACTIVITY;
    private String LIST_ROUTING_VAN_DAY_ACTIVITY = "LIST_ROUTING_VAN_DAY_ACTIVITY";
    boolean isOnline;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listRoutingVanDayVM = (ListRoutingVanDayVM) viewModel;
        dialog = new ProgressDialog(this);
        binding.txtTitle.setText(R.string.CONFIRM_ROUTING_VAN_DAY);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.btnBack.setOnClickListener(v -> onBackPressed());
        setUpRecycleView();
        setUpImagesRecycleView();
        initAdapterImage(savedInstanceState);

        binding.txtRoutingCode.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Customer_DLP", listRoutingVanDayVM.getRoutingDay().get().getRouting_plan_day_code());
                clipboard.setPrimaryClip(clip);
                showToast();
            }
        });

        binding.txtBillLadingCode.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Customer_DLP", listRoutingVanDayVM.getRoutingDay().get().getBill_lading_detail_code());
                clipboard.setPrimaryClip(clip);
                showToast();
            }
        });

        getData();
    }
    public void showToast(){
        Toast.makeText(this, getResources().getString(R.string.COPIED_TO_CLIPBOARD), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }
    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void initView() {
        try {
            if (listRoutingVanDayVM.getIsConfirm().get()) {
                if (listRoutingVanDayVM.getRoutingDay().get().getStatus().equals(RoutingDayStatus.THE_DRIVER_HAS_ARRIVED) ||
                        listRoutingVanDayVM.getRoutingDay().get().getStatus().equals(RoutingDayStatus.SHIPPING)) {
                    binding.btnMap.setVisibility(View.VISIBLE);
                } else {
                    binding.btnMap.setVisibility(View.GONE);
                }
            } else {
                binding.btnMap.setVisibility(View.GONE);
            }

            if (objectStatus.equals("2")) {
                binding.txtTitle.setText(R.string.SOS_WARNING_TITLE);
            } else if (!listRoutingVanDayVM.getRoutingDay().get().getStatus().equals(RoutingDayStatus.THE_DRIVER_HAS_ARRIVED)) {
                binding.txtTitle.setText(R.string.DETAIL_ROUTING_VAN_DAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        hideImages();
        binding.lbBillLadingCode.setOnClickListener(v->gotoBillRoutingDetailActivity());
    }
    private void gotoBillRoutingDetailActivity(){
        Intent intent= new Intent(this, BillRoutingDetailActivity.class);
        intent.putExtra(Constants.ITEM_ID,listRoutingVanDayVM.getRoutingDay().get().getBill_routing_id()+"");
        startActivity(intent);
    }

    @SuppressLint("NonConstantResourceId")
    public void showMoreInfo(View v) {
        switch (v.getId()) {
            case R.id.warehouse:
                listRoutingVanDayVM.getShowMoreInfoWarehouse().set(!listRoutingVanDayVM.getShowMoreInfoWarehouse().get());
                break;
            case R.id.warehouseExport:
                listRoutingVanDayVM.getShowMoreInfoWarehouseExport().set(!listRoutingVanDayVM.getShowMoreInfoWarehouseExport().get());
                break;
            case R.id.driver:
                listRoutingVanDayVM.getShowMoreInfoDriver().set(!listRoutingVanDayVM.getShowMoreInfoDriver().get());
                break;
        }
    }

    public void gotoMapsFragment() {
        Intent intent = new Intent(this, CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ROUTING_CODE, routing_plan_day_code);
        bundle.putSerializable(Constants.FRAGMENT, MapsFragment.class);
        bundle.putSerializable(Constants.FROM_FRAGMENT, "ORDER_FRAGMENT");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                if (listRoutingVanDayVM.getListBillPackages().size() > 0) {
                    adapter.notifyDataSetChanged();
                    adapterImage.notifyDataSetChange();
                }
                if (listRoutingVanDayVM.getRoutingDay().get().getImage_urls() != null) {
                    binding.txtNumberImage.setText(getResources().getString(R.string.number_image) + ": " + listRoutingVanDayVM.getRoutingDay().get().getImage_urls().size());
                } else {
                    binding.txtNumberImage.setText(Constants.EMPTY_STRING);
                }
                initView();
                listRoutingVanDayVM.getIsLoading().set(false);
                break;
            case "confirmSuccess":
                showToast(getResources().getString(R.string.CONFIRMED));
                updateView();
                setResultData(true);
                EventBus.getDefault().post("confirm routing success");
                try {
                    Thread.sleep(2000);
                    gotoRatingDriver();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case "confirmFail":
                showToast(getResources().getString(R.string.CONFIRM_FAIL));
                break;
            case Constants.GET_DATA_FAIL:
                if ("MAIN_ACTIVITY".equals(FROM_ACTIVITY)) {
                    setResultData(false);
                    onBackPressed();
                }
                ToastUtils.showToast(this, getString(R.string.can_not_get_routing_day), getResources().getDrawable(R.drawable.ic_information));
                break;
        }

    }

    // Ẩn nút confirm sau khi confirm
    private void updateView() {
        binding.btnConfirm.setVisibility(View.GONE);
        binding.rcAddImage.setVisibility(View.GONE);
    }

    private void setResultData(Boolean isSuccess) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ROUTING_CODE, routing_plan_day_code);
        intent.putExtras(bundle);
        if (isSuccess)
            setResult(Constants.RESULT_OK, intent);
        else
            setResult(Constants.RESULT_FAIL, intent);
    }

    private void gotoRatingDriver() {
        binding.btnMap.setVisibility(View.GONE);
        binding.btnConfirm.setEnabled(false);
        Intent intent = new Intent(this, CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.MODEL, listRoutingVanDayVM.getRoutingDay().get().getDriver());
        bundle.putSerializable("routing_plan_day_id", listRoutingVanDayVM.getRoutingDay().get().getId());
        bundle.putSerializable(Constants.FRAGMENT, DrivingRatingFragment.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_FRAGMENT_RESULT);
    }


    public void hideImages() {
        try {
            if (listRoutingVanDayVM.getListImage().size() == 0 || listRoutingVanDayVM.getRoutingDay().get().getStatus().equals(RoutingDayStatus.SHIPPING)) {
                binding.ctImages.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            binding.ctImages.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }


    public void showServiceArising() {
        try {
            FragmentManager fm = this.getSupportFragmentManager();
            ShowDetailServiceArisingDialog dialogFragment = new ShowDetailServiceArisingDialog();
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.DATA, listRoutingVanDayVM.getRoutingDay().get().getId());
            bundle.putSerializable(Constants.MODEL, listRoutingVanDayVM.getRoutingDay().get().getDriver());
            bundle.putSerializable("vehicle", listRoutingVanDayVM.getRoutingDay().get().getVehicle());
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "abc");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gotoProblemsArising() {
        Intent intent = new Intent(this, CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.ID, listRoutingVanDayVM.getRoutingDay().get().getId());
        bundle.putSerializable(Constants.MODEL, listRoutingVanDayVM.getRoutingDay().get().getDriver());
        bundle.putSerializable("vehicle", listRoutingVanDayVM.getRoutingDay().get().getVehicle());
        bundle.putSerializable(Constants.DATA, listRoutingVanDayVM.getRoutingDay().get());
        bundle.putSerializable(Constants.FRAGMENT, DetailProblemsArisingFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void gotoDetailDriver() {
        if(listRoutingVanDayVM.getRoutingDay().get().getRating_drivers().getNum_rating()!=null){
            Intent intent = new Intent(this, CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.MODEL, listRoutingVanDayVM.getRoutingDay().get().getDriver());
            bundle.putSerializable("routing_plan_day_id", listRoutingVanDayVM.getRoutingDay().get().getId());
            bundle.putSerializable("Rating_driver", listRoutingVanDayVM.getRoutingDay().get().getRating_drivers());
            bundle.putBoolean("Read_only", true);
            bundle.putSerializable(Constants.FRAGMENT, DrivingRatingFragment.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }else{
            gotoRatingDriver();
        }
    }

    public void showToast(String text_toast) {
        if (text_toast.equals(getResources().getString(R.string.CONFIRMED))) {
            ToastUtils.showToast(this, text_toast, getResources().getDrawable(R.drawable.ic_check));
        } else {
            ToastUtils.showToast(this, text_toast, getResources().getDrawable(R.drawable.ic_fail));
        }

    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcImages1.setLayoutManager(manager);

        binding.rcImages1.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }


    private void getData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(Constants.ITEM_ID)) {
                routing_plan_day_code = intent.getStringExtra(Constants.ITEM_ID);
                listRoutingVanDayVM.getData(intent.getStringExtra(Constants.ITEM_ID), this::runUi);
            }
            if (intent.hasExtra(Constants.FROM_ACTIVITY)) {
                this.FROM_ACTIVITY = intent.getStringExtra(Constants.FROM_ACTIVITY);
            }
            if (intent.hasExtra(Constants.OBJECT_STATUS)) {
                objectStatus = intent.getStringExtra(Constants.OBJECT_STATUS);
            }
        }
    }


    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new ListRoutingVanDayActivity.MyResultCallback(imageSelectAdapter, listRoutingVanDayVM));
    }

    private void initAdapterImage(Bundle savedInstanceState) {
        initLayoutManager();
        imageSelectAdapter = new GridImageAdapter(this, this::pickImage);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setOnClear(() -> listRoutingVanDayVM.getIsNotEmptyImage().set(false));
        binding.rcImages1.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> openOptionImage(position));
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.comfirm_routing_van_day_item, listRoutingVanDayVM.getListBillPackages(), this) {
            @Override
            public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
                super.onBindViewHolder(viewHolder, position);
                viewHolder.bindWithVM(listRoutingVanDayVM);
            }
        };
        binding.rcBillLading.setAdapter(adapter);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        binding.rcBillLading.setLayoutManager(layoutManager);

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {

        switch (view.getId()) {
            case R.id.btnConfirm:
                if(isOnline){

                    if (listRoutingVanDayVM.checkConfirm()) {
                        listRoutingVanDayVM.confirmRouting(binding.txtNote.getText().toString(), this::runUi);
                    } else {
                        ToastUtils.showToast(this, getResources().getString(R.string.Checked_fail), getResources().getDrawable(R.drawable.ic_fail));
                    }
                }else{
                    ToastUtils.showToast(getResources().getString(R.string.network_error));
                }
                break;
            case R.id.qrCode:
                String[] perms = {Manifest.permission.CAMERA};
                if (EasyPermissions.hasPermissions(this, perms)) {
                    gotoQrScanActivity();
                } else {
                    // Do not have permissions, request them now
                    EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                            REQUEST_PERMISSION_CAMERA, perms);
                }
                break;
        }

    }

    private void gotoQrScanActivity() {
        Intent intent = new Intent(this, QrScannerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.FROM_FRAGMENT, LIST_ROUTING_VAN_DAY_ACTIVITY);
        bundle.putBoolean(Constants.SCAN_MULTI_QR_CODE, true);
        bundle.putSerializable(Constants.DATA, listRoutingVanDayVM.getRoutingDay().get());
        intent.putExtra(Constants.QR_DATA, listRoutingVanDayVM.getQrDataChecked());
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_CODE_SCAN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isScanCodeAndResultOk(requestCode, resultCode)) {
            listRoutingVanDayVM.setSuccess(data.getBooleanExtra(Constants.SUCCESS, false));
            listRoutingVanDayVM.getRoutingDay().set((RoutingDay) data.getSerializableExtra(Constants.DATA));
            listRoutingVanDayVM.setQrDataChecked((HashMap<Integer, HashMap<String, Boolean>>) data.getSerializableExtra(Constants.QR_DATA));
            listRoutingVanDayVM.getListBillPackages().clear();
            listRoutingVanDayVM.getListBillPackages().addAll(listRoutingVanDayVM.getRoutingDay().get().getList_bill_package());
            adapter.notifyDataSetChanged();
            if (listRoutingVanDayVM.isSuccess())
                ToastUtils.showToast(this, getResources().getString(R.string.SUCCESS), getResources().getDrawable(R.drawable.ic_access));
        } else if (requestCode == Constants.REQUEST_FRAGMENT_RESULT) {
            onBackPressed();
        }
        closeProcess();
    }


    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Constants.RESULT_OK;
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_PERMISSION_CAMERA) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            gotoQrScanActivity();
        }
    }

    public void setUpImagesRecycleView() {
        adapterImage = new ImageBaseAdapter(this, R.layout.item_image_100dp, listRoutingVanDayVM.getListImage());
        binding.rcImages.setAdapter(adapterImage);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        binding.rcImages.setLayoutManager(layoutManager);

    }


    @Override
    public int getLayoutRes() {
        return R.layout.comfirm_routing_van_day_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListRoutingVanDayVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcBillLading;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            binding.tvNetwork.setVisibility(View.GONE);
            binding.progressLayout.setVisibility(View.GONE);
        }
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private ListRoutingVanDayVM billDetailVM;

        public MyResultCallback(GridImageAdapter adapter, ListRoutingVanDayVM billDetailVM) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.billDetailVM = billDetailVM;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            billDetailVM.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }
}