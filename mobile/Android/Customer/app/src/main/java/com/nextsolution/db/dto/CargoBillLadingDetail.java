package com.nextsolution.db.dto;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoBillLadingDetail extends BaseModel {
    Integer id;
    String name_seq;
    Integer bill_lading_id;
    Double total_weight;
    Integer warehouse_id;
    String warehouse_type;
    Integer from_bill_lading_detail_id;
    String description;
    OdooDateTime expected_from_time;
    OdooDateTime expected_to_time;
    String name;
    String status;
    String approved_type;
    Integer from_warehouse_id;
    Double latitude;
    Double longitude;
    Integer area_id;
    Integer zone_area_id;
    String status_order;
    String address;
    String warehouse_name;
    Double distance;
    String from_seq;
    Double max_price;
    Double min_price;
    String to_seq;
    String type;
    Integer trans_id;
    Integer depot_id;
    String phone;
    Integer hub_id;
    List<CargoBillPackage> list_bill_package;
}
