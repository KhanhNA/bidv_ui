package com.nextsolution.db.dto;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.net.URL;

@lombok.Getter
@lombok.Setter
public class Van implements ShipmentStandStill {
    private String __typename;
    private Integer app_param_value_id;
    private Double available_capacity;
    private String avatar_url;
    private Double axles;
    private Double body_length;
    private Double body_width;
    private Double capacity;
    private Double cost;
    private Double cost_center;
    private java.util.Date create_date;
    private String create_user;
    private String description;
    private String driver;
    private Double engine_size;
    private Integer fleet_id;
    private String fuel_type_id;
    private Double gross_weight;
    private Integer has_attachment;
    private Integer has_image;
    private Double height;
    private Integer id;
    private java.util.Date inspection_due_date;
    private Double latitude;
    private String licence_plate;
    private String location;
    private Double longitude;
    private Integer maintenance_template_id;
    private String name;
    private String note;
    private Integer parking_point_id;
    private Integer status_available;
    private Integer status_car;
    private Double tire_front_pressure;
    private Double tire_front_size;
    private Double tire_rear_pressure;
    private Double tire_rear_size;
    private java.util.Date update_date;
    private String update_user;
    private String van_inspection;
    private Integer van_type_Id;
    private String vehicle_registration;
    private Double vehicle_tonnage;
    private java.util.Date warranty_date1;
    private java.util.Date warranty_date2;
    private Double warranty_meter1;
    private Double warranty_meter2;
    private String warranty_name1;
    private String warranty_name2;
    private Double wheelbase;
    private Integer year;
    RoutingVanDay firstRoute;

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);

    }

  @Override
  public String getAvatarStr() {
    return null;
  }

  @Override
  public URL getAvatarUrl() throws Exception {
    return null;
  }

  @Override
  public void setMarker(Marker marker1) {

  }

  @Override
  public Marker getMarker() {
    return null;
  }

  @Override
  public String getServiceTime() {
    return null;
  }

  @Override
  public String getArriveTime() {
    return null;
  }

}

