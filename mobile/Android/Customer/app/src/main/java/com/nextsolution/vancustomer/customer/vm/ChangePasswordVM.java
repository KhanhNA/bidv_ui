package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.model.ChangingPassword;
import com.tsolution.base.BaseViewModel;

import java.lang.ref.WeakReference;

import lombok.Getter;
import lombok.Setter;

import static com.nextsolution.vancustomer.util.StringUtils.isNullOrEmpty;

@Getter
@Setter
public class ChangePasswordVM extends BaseViewModel {

    private ObservableField<ChangingPassword> changingPassword = new ObservableField<>();
    ObservableBoolean isLoading = new ObservableBoolean(false);

    public ChangePasswordVM(@NonNull Application application) {
        super(application);
        changingPassword.set(new ChangingPassword());

    }

    private WeakReference<Context> context;

    public void setContext(Context context) {
        this.context = new WeakReference<>(context);
    }


    public void changePassword(RunUi runUi) {
        isLoading.set(true);
        ChangingPassword changeRequest = changingPassword.get();
        CustomerApi.changePassword(changeRequest.getOldPassword(), changeRequest.getNewPassword(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getSuccess(Object o, RunUi runUi) {
        isLoading.set(false);
        try {
            Boolean result = (Boolean) o;
            if (result) {
                runUi.run(Constants.SUCCESS);
            }else{
                runUi.run(Constants.FAIL);
            }
        } catch (Exception e) {
            runUi.run(Constants.FAIL);
            e.printStackTrace();
        }
    }


    private boolean isTheSamePassword(String pass1, String pass2) {
        return !isNullOrEmpty(pass1) && !isNullOrEmpty(pass2) && pass1.equals(pass2);
    }
}
