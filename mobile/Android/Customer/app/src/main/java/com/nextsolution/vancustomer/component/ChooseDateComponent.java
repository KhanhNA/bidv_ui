package com.nextsolution.vancustomer.component;

import com.nextsolution.vancustomer.util.MyDateUtils;

import java.util.Calendar;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChooseDateComponent {
    public Date fromDate = MyDateUtils.reduceOneMonth(Calendar.getInstance().getTime());
    public Date toDate = Calendar.getInstance().getTime();

    public void resetData() {
        fromDate.setTime(MyDateUtils.reduceOneMonth(Calendar.getInstance().getTime()).getTime());
        toDate.setTime((Calendar.getInstance().getTime()).getTime());
//        fromDate.notify();
//        toDate.notify();
    }


}
