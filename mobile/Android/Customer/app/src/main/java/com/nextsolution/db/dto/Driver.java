package com.nextsolution.db.dto;


import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver extends BaseModel {
    private Long id;
    private String driver_code;
    private String name;

    private String phone;
    private String image_1920;
}