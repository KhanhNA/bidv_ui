package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessQuantityExportDialogVM extends BaseViewModel {
    ObservableField<Integer> quantityExport = new ObservableField<>();
    ObservableField<Integer> maxQuantityExport= new ObservableField<>();
    public AccessQuantityExportDialogVM(@NonNull Application application) {
        super(application);
        quantityExport.set(0);
        maxQuantityExport.set(0);
    }
}
