package com.nextsolution.db.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Zone implements Cloneable, Serializable {
    private Integer id;
    private String name;
    private String code;
    private String name_seq;
    private Hub depotInfo;

}
