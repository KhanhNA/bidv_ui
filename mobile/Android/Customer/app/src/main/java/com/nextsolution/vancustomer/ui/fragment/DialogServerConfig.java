package com.nextsolution.vancustomer.ui.fragment;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.databinding.DialogChangeConfigBinding;
import com.nextsolution.vancustomer.util.StringUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

@SuppressLint("ValidFragment")
public class DialogServerConfig extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogChangeConfigBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_config, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        SharedPreferences.Editor editor = AppController.getInstance().getEditor();

        String urlLogin = sharedPreferences.getString("U_LOGIN", AppController.SERVER_URL);
        String urlBilling = sharedPreferences.getString("U_BILLING", AppController.BILLING_URL);
        String urlSocket = sharedPreferences.getString("U_SOCKET", AppController.TRACKING_SOCKET);
        String urlLogistic = sharedPreferences.getString("U_DATABASE", AppController.DATABASE);

        binding.edLogin.setText(urlLogin);
        binding.edSocket.setText(urlSocket);
        binding.edBilling.setText(urlBilling);
        binding.edDatabase.setText(urlLogistic);
        binding.txtMessage.setText("Nếu thay đổi link server thì bấm confirm -> thoát app rồi vào lại." +
                "\n Nếu thay đổi database thì ko cần thoát app..");

        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(binding.edLogin.getText().toString())) {
                editor.putString("U_LOGIN", binding.edLogin.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edBilling.getText().toString())) {
                editor.putString("U_BILLING", binding.edBilling.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edSocket.getText().toString())) {
                editor.putString("U_SOCKET", binding.edSocket.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edDatabase.getText().toString())) {
                editor.putString("U_DATABASE", binding.edDatabase.getText().toString()).apply();
            }
            dismiss();
        });
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}
