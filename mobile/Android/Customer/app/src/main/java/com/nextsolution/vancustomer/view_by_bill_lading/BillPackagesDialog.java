package com.nextsolution.vancustomer.view_by_bill_lading;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.db.dto.BillRoutingDetail;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.databinding.DialogBillPackageBinding;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class BillPackagesDialog extends BottomSheetDialogFragment implements AdapterListener {
    private DialogBillPackageBinding binding;
    public BillRoutingDetail billLadingDetail;

    BillPackagesDialog(BillRoutingDetail billLadingDetail) {
        this.billLadingDetail = billLadingDetail;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_bill_package, container, false);
        binding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return binding.getRoot();
    }

    private void initView() {
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        XBaseAdapter goodsAdapter = new XBaseAdapter(R.layout.item_bill_package, billLadingDetail.getBillPackages(), this);
        binding.rcGoods.setAdapter(goodsAdapter);
        binding.rcGoods.setLayoutManager(new LinearLayoutManager(getContext()));

    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }


    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onItemClick(View view, Object o) {
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }
}
