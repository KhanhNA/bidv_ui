package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.ListVehicleDialogVM;
import com.nextsolution.vancustomer.databinding.DialogChooseBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

public class ListVehicleDialog extends DialogFragment {
    ListVehicleDialogVM listVehicleDialogVM;
    DialogChooseBinding mBinding;
    AdapterListener onClickListener;
    List<BiddingVehicle> biddingVehicles= new ArrayList<>();
    XBaseAdapter adapter;
    public ListVehicleDialog(List<BiddingVehicle> biddingVehicles,AdapterListener onClickListener) {
        this.onClickListener = onClickListener;
        this.biddingVehicles.addAll(biddingVehicles);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_choose, container, false);
        listVehicleDialogVM = ViewModelProviders.of(this).get(ListVehicleDialogVM.class);
        mBinding.setViewModel(listVehicleDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        mBinding.btnClose.setOnClickListener(v->{
            dismiss();
        });
        setUpRecycleView();
//        getData();
        return mBinding.getRoot();
    }
    public void getData(){
        try{
            Bundle bundle= getArguments();
            listVehicleDialogVM.getBiddingVehicles().addAll(bundle.getParcelableArrayList(Constants.DATA));
            adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void setUpRecycleView(){
        adapter = new XBaseAdapter(R.layout.list_vehicle_item,this.biddingVehicles,onClickListener);
        mBinding.rcVehicle.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        mBinding.rcVehicle.setLayoutManager(linearLayoutManager);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
