package com.nextsolution.db.api;

import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.db.dto.BiddingPackage;
import com.nextsolution.db.dto.CargoBillLadingDetail;
import com.nextsolution.db.dto.CargoDTO;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.TsUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class DepotAPI extends BaseApi {
    public static String GET_CARGO_TYPE = "/depot/get_all_cargo_size_standard";
    public static String GET_CARGO_ALL_BY_DEPOT_ID = "/depot/get_all_cargo_by_depot_id";
    public static String GET_CARGO_DETAIL = "/stock_man/get_bill_lading_detail_by_cargo_id";
    public static String GOM_CARGO = "/stock_man/confirm_quantity_package_bill_package";
    public static String GET_CARGO_BIDDING = "/bidding/get_bidding_order_bidded_stockman";
    public static String GET_BIDDING_CARGO_DETAIL = "/bidding/get_bidding_order_detail";
    public static String CONFIRM_BIDDING_CARGO_VEHICLE = "/stock_man/confirm_bidding_order_success";
    public static String GET_LOCATION_VEHICLES = "/stock_man/location_vehicle";

    public static void getLocationVehicles(List<Integer> vehicleId, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bidding_vehicle_ids", new JSONArray(vehicleId));
            mOdoo.callRoute(GET_LOCATION_VEHICLES, params, new SharingOdooResponse<OdooResultDto<Vehicle>>() {

                @Override
                public void onSuccess(OdooResultDto<Vehicle> resultDto) {
                    if (resultDto != null && TsUtils.isNotNull(resultDto.getRecords())) {
                        result.onSuccess(resultDto.getRecords());
                    } else {
                        result.onSuccess(null);

                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }

    }
    public static void getBiddingCargoDetail(int biddingOrderId, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bidding_order_id", biddingOrderId);
            mOdoo.callRoute(GET_BIDDING_CARGO_DETAIL, params, new SharingOdooResponse<OdooResultDto<BiddingPackage>>() {

                @Override
                public void onSuccess(OdooResultDto<BiddingPackage> resultDto) {
                    if (resultDto != null && TsUtils.isNotNull(resultDto.getRecords())) {
                        result.onSuccess(resultDto.getRecords().get(0));
                    } else {
                        result.onSuccess(null);

                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }

    }

    public static void confirmBiddingCargoVehicle(int biddingOrderId, int biddingVehicleId, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bidding_order_id", biddingOrderId);
            params.put("bidding_vehicle_id", biddingVehicleId);
            mOdoo.callRoute(CONFIRM_BIDDING_CARGO_VEHICLE, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }

    }

    public static void getCargoTypes(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            mOdoo.callRoute(GET_CARGO_TYPE, params, new SharingOdooResponse<OdooResultDto<CargoTypes>>() {
                @Override
                public void onSuccess(OdooResultDto<CargoTypes> cargoTypesOdooResultDto) {
                    result.onSuccess(cargoTypesOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    /**
     * @param type          0: xuất hàng - depotId = fromDepotId, 1: nhập hàng - depotId = toDepotId
     * @param status        trạng thái của đơn
     * @param offset        phân trang
     * @param orderByNewest Lấy mới nhất hay cũ nhất
     * @param search_code   tìm kiếm
     * @param result        callback
     */
    public static void getCargoBidding(Integer type, List<Integer> status, boolean orderByNewest, Integer offset, String search_code, IResponse result) {
        JSONObject params;

        final String depot_id = type == 0 ? "from_depot_id" : "to_depot_id";

        try {
            params = new JSONObject();
            params.put("type", type + "");
            params.put(depot_id, StaticData.getPartner().getDepot_id());
            params.put("offset", offset);
            params.put("limit", 10);
            if (search_code == null || search_code.isEmpty()) {
                search_code = "";
            }
            params.put("search_code", search_code);
            params.put("status", new JSONArray(mGson.toJson(status)));
            params.put("order", orderByNewest ? "5" : "6");
            mOdoo.callRoute(GET_CARGO_BIDDING, params, new SharingOdooResponse<OdooResultDto<BiddingOrder>>() {
                @Override
                public void onSuccess(OdooResultDto<BiddingOrder> biddingInformationOdooResultDto) {
                    if (biddingInformationOdooResultDto != null && biddingInformationOdooResultDto.getRecords() != null) {
                        result.onSuccess(biddingInformationOdooResultDto.getRecords());
                    } else {
                        result.onSuccess(null);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void gomCargo(Integer cargo_id, List<Integer> list_bill_package_id, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("cargo_id", cargo_id);
            params.put("list_bill_package_id", new JSONArray(mGson.toJson(list_bill_package_id)));
            mOdoo.callRoute(GOM_CARGO, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getCargosByDepotId(Integer size_id, String code, Integer offset, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("depot_id", StaticData.getPartner().getDepot_id());
            if (code == null) {
                code = Constants.EMPTY_STRING;
            }
            params.put("size_id", size_id);
            params.put("code", code);
            params.put("offset", offset);
            mOdoo.callRoute(GET_CARGO_ALL_BY_DEPOT_ID, params, new SharingOdooResponse<OdooResultDto<CargoDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<CargoDTO> cargoDTOOdooResultDto) {
                    result.onSuccess(cargoDTOOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }


            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getCargoDetail(Integer cargo_id, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("cargo_id", cargo_id);
            mOdoo.callRoute(GET_CARGO_DETAIL, params, new SharingOdooResponse<OdooResultDto<CargoBillLadingDetail>>() {
                @Override
                public void onSuccess(OdooResultDto<CargoBillLadingDetail> billLadingDetailOdooResultDto) {
                    result.onSuccess(billLadingDetailOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
