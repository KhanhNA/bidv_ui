package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.dto.BillLading;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerOrdersChildVM extends BaseViewModel {
    private ObservableField<Boolean> emptyData=new ObservableField<>();
    private ObservableField<String> orderCode = new ObservableField<>();
    private List<BillLading> listBillByDay ;
    private ObservableField<String> fromDate;
    private ObservableField<String> toDate;
    private String status;

    public void init(RunUi runUi) {
        getData(runUi,status);
    }

    public CustomerOrdersChildVM(@NonNull Application application) {
        super(application);
        Date fDate = MyDateUtils.reduceOneMonth(Calendar.getInstance().getTime());
        Date tDate = Calendar.getInstance().getTime();
        toDate = new ObservableField<>(MyDateUtils.convertDateToString(tDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        fromDate = new ObservableField<>(MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        listBillByDay =  new ArrayList<>();
        String[] array = AppController.getInstance().getResources().getStringArray(R.array.order_status);
    }


    public void getData(RunUi runUi,String status){
//        OrderApi.getListOrderHistory(fromDate.get(), toDate.get(), orderCode.get(), status, new SharingOdooResponse() {
//            @Override
//            public void onSuccess(Object o) {
//                getListData(o,runUi);
//            }
//        });
    }

    private void getListData(Object response, RunUi runUi) {
        try{
            if(response!=null){
                this.listBillByDay.clear();
                OdooResultDto<BillLading> resultDto = (OdooResultDto<BillLading>)response;
                this.listBillByDay.addAll((List<BillLading>)resultDto.getRecords());
                runUi.run(Constants.GET_DATA_SUCCESS);
            }else{
                runUi.run(Constants.GET_DATA_FAIL);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
