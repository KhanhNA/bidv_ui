package com.nextsolution.vancustomer.service.service_dto;


import android.content.SharedPreferences;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.enums.Constants;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationService {
    private Long id;
    private String title;
    private Integer android_channel_id;
    private String body;
    private String token;
    private String topic;
    private String senderName;
    private String customData;
    private Long senderId;
    private String item_id;
    private String sound;
    private String appId;
    private String click_action;
    private String mess_object;
    private String badge;


    public static NotificationService of(Map<String, String> data) throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final NotificationService notificationService = mapper.convertValue(data, NotificationService.class);
        try {
                SharedPreferences.Editor editor = AppController.getInstance().getEditor();
                Integer totalNotification=AppController.getInstance().getSharePre().getInt(Constants.IS_NOTIFICATION,-1);
                if (totalNotification>=0){
                    editor.putInt(Constants.IS_NOTIFICATION,totalNotification+1);
                }
                editor.commit();
        }catch (Exception e){
            e.printStackTrace();
        }

        return notificationService;
    }
}
