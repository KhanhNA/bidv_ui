package com.nextsolution.db.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CargoBillPackageRoutingPlan extends BaseModel implements Parcelable {
    String item_name;
    Integer bill_lading_detail_id;
    Double length;
    Double height;
    Integer capacity;
    String description;
    Integer product_type_id;
    String status;
    Integer from_bill_package_id;
    Integer quantity;
    Double width;
    Double total_weight;
    Integer product_type_id_moved0;
    Integer product_package_type_id;
    Integer bill_package_id;
    String note;
    String insurance_name;
    String service_name;
    Integer from_warehouse_id;
    Integer to_warehouse_id;
    String qr_char;
    Integer routing_plan_day_id;
    String name;

    protected CargoBillPackageRoutingPlan(Parcel in) {
        item_name = in.readString();
        if (in.readByte() == 0) {
            bill_lading_detail_id = null;
        } else {
            bill_lading_detail_id = in.readInt();
        }
        if (in.readByte() == 0) {
            length = null;
        } else {
            length = in.readDouble();
        }
        if (in.readByte() == 0) {
            height = null;
        } else {
            height = in.readDouble();
        }
        if (in.readByte() == 0) {
            capacity = null;
        } else {
            capacity = in.readInt();
        }
        description = in.readString();
        if (in.readByte() == 0) {
            product_type_id = null;
        } else {
            product_type_id = in.readInt();
        }
        status = in.readString();
        if (in.readByte() == 0) {
            from_bill_package_id = null;
        } else {
            from_bill_package_id = in.readInt();
        }
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }
        if (in.readByte() == 0) {
            width = null;
        } else {
            width = in.readDouble();
        }
        if (in.readByte() == 0) {
            total_weight = null;
        } else {
            total_weight = in.readDouble();
        }
        if (in.readByte() == 0) {
            product_type_id_moved0 = null;
        } else {
            product_type_id_moved0 = in.readInt();
        }
        if (in.readByte() == 0) {
            product_package_type_id = null;
        } else {
            product_package_type_id = in.readInt();
        }
        if (in.readByte() == 0) {
            bill_package_id = null;
        } else {
            bill_package_id = in.readInt();
        }
        note = in.readString();
        insurance_name = in.readString();
        service_name = in.readString();
        if (in.readByte() == 0) {
            from_warehouse_id = null;
        } else {
            from_warehouse_id = in.readInt();
        }
        if (in.readByte() == 0) {
            to_warehouse_id = null;
        } else {
            to_warehouse_id = in.readInt();
        }
        qr_char = in.readString();
        if (in.readByte() == 0) {
            routing_plan_day_id = null;
        } else {
            routing_plan_day_id = in.readInt();
        }
        name = in.readString();
    }

    public static final Creator<CargoBillPackageRoutingPlan> CREATOR = new Creator<CargoBillPackageRoutingPlan>() {
        @Override
        public CargoBillPackageRoutingPlan createFromParcel(Parcel in) {
            return new CargoBillPackageRoutingPlan(in);
        }

        @Override
        public CargoBillPackageRoutingPlan[] newArray(int size) {
            return new CargoBillPackageRoutingPlan[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(item_name);
        if (bill_lading_detail_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bill_lading_detail_id);
        }
        if (length == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(length);
        }
        if (height == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(height);
        }
        if (capacity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(capacity);
        }
        dest.writeString(description);
        if (product_type_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(product_type_id);
        }
        dest.writeString(status);
        if (from_bill_package_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(from_bill_package_id);
        }
        if (quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity);
        }
        if (width == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(width);
        }
        if (total_weight == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(total_weight);
        }
        if (product_type_id_moved0 == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(product_type_id_moved0);
        }
        if (product_package_type_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(product_package_type_id);
        }
        if (bill_package_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bill_package_id);
        }
        dest.writeString(note);
        dest.writeString(insurance_name);
        dest.writeString(service_name);
        if (from_warehouse_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(from_warehouse_id);
        }
        if (to_warehouse_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(to_warehouse_id);
        }
        dest.writeString(qr_char);
        if (routing_plan_day_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(routing_plan_day_id);
        }
        dest.writeString(name);
    }
}
