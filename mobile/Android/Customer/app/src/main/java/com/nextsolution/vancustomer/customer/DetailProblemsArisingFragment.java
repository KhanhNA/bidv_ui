package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.DetailProblemsArisingVM;
import com.nextsolution.vancustomer.databinding.DetailProblemsArisingFragmentBinding;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class DetailProblemsArisingFragment extends BaseFragment {
    DetailProblemsArisingVM detailProblemsArisingVM;
    DetailProblemsArisingFragmentBinding mBinding;
    XBaseAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (DetailProblemsArisingFragmentBinding) binding;
        detailProblemsArisingVM = (DetailProblemsArisingVM) viewModel;
        init();
        setUpRecycleView();
        getData();
        return view;
    }

    private void runUi(Object[] params) {
        String action = (String) params[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                adapter.notifyDataSetChanged();
                break;
        }

    }

    private void init() {
        mBinding.toolbar.setTitle(R.string.Problems_arise);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void getData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(Constants.MODEL)) {
            detailProblemsArisingVM.getDriver().set((Driver) intent.getSerializableExtra(Constants.MODEL));
        }
        if (intent.hasExtra("vehicle")) {
            detailProblemsArisingVM.getVehicle().set((Vehicle) intent.getSerializableExtra("vehicle"));
        }
        if (intent.hasExtra(Constants.DATA)) {
            detailProblemsArisingVM.getRoutingDay().set((RoutingDay) intent.getSerializableExtra(Constants.DATA));
        }
        detailProblemsArisingVM.getServiceArising(intent.getIntExtra(Constants.ID, -1), this::runUi);
    }

    public void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.show_detail_service_arising_item, detailProblemsArisingVM.getSosTypes(), this);
        mBinding.rcServiceArising.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mBinding.rcServiceArising.setLayoutManager(layoutManager);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.detail_problems_arising_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailProblemsArisingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
