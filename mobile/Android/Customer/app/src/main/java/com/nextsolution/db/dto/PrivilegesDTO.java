package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PrivilegesDTO extends BaseModel {
    Long id;
    String uri_path;
    String name;
}
