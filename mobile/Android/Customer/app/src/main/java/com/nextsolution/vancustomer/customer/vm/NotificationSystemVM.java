package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.NotificationAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.NotificationDTO;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationSystemVM extends BaseViewModel {
    ObservableField<Boolean> isLoading= new ObservableField<>();
    ObservableField<NotificationDTO> notificationDTO;
    public NotificationSystemVM(@NonNull Application application) {
        super(application);
        notificationDTO= new ObservableField<>();
    }
    public void getNotificationById(Integer id){
        isLoading.set(true);
        NotificationAPI.getNotificationById(id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o){
        OdooResultDto<NotificationDTO> resultDto = (OdooResultDto<NotificationDTO>) o;
            if(resultDto!=null && resultDto.getRecords() !=null && resultDto.getRecords().size()>0){
                notificationDTO.set(resultDto.getRecords().get(0));
                isLoading.set(false);
            }
    }
}
