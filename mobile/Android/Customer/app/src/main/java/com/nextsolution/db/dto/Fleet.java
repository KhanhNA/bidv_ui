package com.nextsolution.db.dto;

@lombok.Getter
@lombok.Setter
public class Fleet {
  private String __typename;
  private String address1;
  private String address2;
  private String address_registration;
  private String business_registration;
  private String city;
  private String contact_name;
  private java.util.Date create_date;
  private String create_user;
  private String email;
  private String fax;
  private String fleet_code;
  private String fleet_name;
  private Integer id;
  private Integer parent_id;
  private String phone1;
  private String phone2;
  private String postal_code;
  private Integer status;
  private String tax_code;
  private java.util.Date update_date;
  private String update_user;
  private String website;

}

