package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDTO extends BaseModel {
    Integer id;
    String name;
    String code;
    Integer parent_id;
}
