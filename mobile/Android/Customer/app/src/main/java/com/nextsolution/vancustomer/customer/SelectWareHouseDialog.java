package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.db.dto.Area;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.BillService;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.OrderVMV2;
import com.nextsolution.vancustomer.customer.vm.SelectWareHouseVM;
import com.nextsolution.vancustomer.databinding.DialogSelectWarehouseBinding;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.ListDialogFragment;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class SelectWareHouseDialog extends BottomSheetDialogFragment implements AdapterListener {
    private DialogSelectWarehouseBinding binding;
    private SelectWareHouseVM selectWareHouseVM;
    private final OrderVMV2 orderVM;
    private List<BillPackage> listGoods;
    private final OnConfirm onConfirm;
    private BillLadingDetail billLadingDetail;
    private XBaseAdapter goodsAdapter;

    private List<Integer> itemQuantities;// lưu lại split quantity của từng item.
    private List<Boolean> itemsChecked;// lưu lại trạng thái của từng item.
    private Warehouse warehouse;//lưu lại warehouse
    private List<BillService> billServices;//lưu lại danh sách dịch vụ theo kho

    private boolean isConfirm;
    private ListDialogFragment dialogFragment;
    DialogConfirm dialogConfirm;


    SelectWareHouseDialog(OrderVMV2 orderVM, OnConfirm onConfirm) {
        this.orderVM = orderVM;
        this.listGoods = new ArrayList<>();
        List<BillPackage> temps = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages();
        for (BillPackage temp : temps) {
            BillPackage clone;
            clone = (BillPackage) temp.clone();
            this.listGoods.add(clone);
        }
        this.onConfirm = onConfirm;
        calculateRest();
    }

    SelectWareHouseDialog(OrderVMV2 orderVM, BillLadingDetail billLadingDetail, OnConfirm onConfirm) {
        this.orderVM = orderVM;
        this.onConfirm = onConfirm;
        this.itemQuantities = new ArrayList<>();
        this.itemsChecked = new ArrayList<>();
        this.billLadingDetail = billLadingDetail;
        calculateRest(billLadingDetail);
    }

    private void calculateRest(BillLadingDetail current) {
        List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();
        List<BillPackage> billPackages = billLadingDetails.get(0).getBillPackages();

        this.warehouse = (Warehouse) current.getWarehouse().clone();
        this.billServices = new ArrayList<>();
        this.billServices.addAll(current.getBillService());

        for (int i = 0; i < billPackages.size(); i++) {
            BillPackage billPackage = billPackages.get(i);
            // Bằng số lượng của billdetail hiện tại * -1.
            int split = current.getBillPackages().get(i).getQuantity_package() * -1;

            for (int j = 1; j < billLadingDetails.size(); j++) {
                split += billLadingDetails.get(j).getBillPackages().get(i).getQuantity_package();
            }
            itemQuantities.add(current.getBillPackages().get(i).getQuantity_package());
            itemsChecked.add(current.getBillPackages().get(i).isSelect());
            if (current.getBillPackages().get(i).getQuantity_package() == 0) {
                current.getBillPackages().get(i).setQuantity_package(billPackage.getQuantity_package() - split);
            }
            current.getBillPackages().get(i).setRest(billPackage.getQuantity_package() - split);
        }

    }

    private void calculateRest() {
        List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();
        List<BillPackage> billPackages = billLadingDetails.get(0).getBillPackages();

        for (int i = 0; i < billPackages.size(); i++) {
            BillPackage billPackage = billPackages.get(i);
            int split = 0;
            for (int j = 1; j < billLadingDetails.size(); j++) {
                split += billLadingDetails.get(j).getBillPackages().get(i).getQuantity_package();
            }
            listGoods.get(i).setQuantity_package(billPackage.getQuantity_package() - split);
            listGoods.get(i).setRest(billPackage.getQuantity_package() - split);
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_warehouse, container, false);
        selectWareHouseVM = ViewModelProviders.of(this).get(SelectWareHouseVM.class);
        binding.setViewModel(selectWareHouseVM);
        binding.setListener(this);
        isConfirm = false;
        selectWareHouseVM.setOrderVM(orderVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return binding.getRoot();
    }

    private void initView() {

//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.HOUR_OF_DAY, 6);
//        c.set(Calendar.MINUTE, 0);
//        String date = AppController.formatTime.format(c.getTime());
//
//        binding.txtFromDate.setText(date);
//        selectWareHouseVM.getBillLadingDetail().get().setExpected_from_time(new OdooDateTime(c.getTime().getTime()));
//
//        c.set(Calendar.HOUR_OF_DAY, 18);
//        c.set(Calendar.MINUTE, 0);
//        date = AppController.formatTime.format(c.getTime());
//        binding.txtToDate.setText(date);
//        selectWareHouseVM.getBillLadingDetail().get().setExpected_to_time(new OdooDateTime(c.getTime().getTime()));


        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        if (billLadingDetail != null) {
            binding.btnDelete.setVisibility(View.VISIBLE);
            binding.btnConfirm.setText(R.string.update_receipt_warehouse);
            selectWareHouseVM.getBillLadingDetail().set(billLadingDetail);
        } else {
            selectWareHouseVM.getBillLadingDetail().get().setBillPackages(listGoods);
        }
        goodsAdapter = new XBaseAdapter(R.layout.item_goods_edit, selectWareHouseVM.getBillLadingDetail().get().getBillPackages(), this);
        binding.rcGoods.setAdapter(goodsAdapter);
        binding.rcGoods.setLayoutManager(new LinearLayoutManager(getContext()));


        //clear data select box
        binding.spSelectWareHouse.setEndIconOnClickListener(v -> {
            selectWareHouseVM.getBillLadingDetail().get().setWarehouse(null);
            selectWareHouseVM.getBillLadingDetail().notifyChange();
            binding.etWareHouse.setText("");
        });
//        binding.ipFromDate.setEndIconOnClickListener(v -> {
//            selectWareHouseVM.getBillLadingDetail().get().setExpected_from_time(null);
//            orderVM.getBillLadingField().notifyChange();
//        });
//        binding.ipToDate.setEndIconOnClickListener(v -> {
//            selectWareHouseVM.getBillLadingDetail().get().setExpected_to_time(null);
//            orderVM.getBillLadingField().notifyChange();
//        });

        binding.spSelectWareHouse.setEndIconOnClickListener(v -> {
            orderVM.getSelectedWarehouses().remove(selectWareHouseVM.getBillLadingDetail().get().getWarehouse().getId());
            selectWareHouseVM.getBillLadingDetail().get().setWarehouse(null);
            binding.etWareHouse.setText(null);
        });

        binding.btnDelete.setOnClickListener(v -> deleteWarehouse(getString(R.string.msg_confirm_delete_receipt_warehouse)));
    }

    public void deleteWarehouse(String msg) {
        dialogConfirm = new DialogConfirm(getString(R.string.confirm), msg, v -> {
            onConfirm.onDelete(billLadingDetail);
            dialogConfirm.dismiss();
            dismiss();
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    public void selectWarehouse() {
        if(!orderVM.getIsLoading().get()){
            BillLadingDetail billDetail = selectWareHouseVM.getBillLadingDetail().get();
            WarehouseDialog dialog = new WarehouseDialog(billDetail.getWarehouse(), orderVM.getSelectedWarehouses(), ((selectedWareHouse) -> {
                if (selectedWareHouse.getId() == null) {
                    orderVM.checkIsExistWareHouse(selectedWareHouse, this::runUi);
                } else {
                    orderVM.getDestination(getContext(),selectedWareHouse, false, this::runUi);
                }
            }));
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getTag());
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "checkWarehouseSuccess":
                orderVM.getDestination(getContext(),(Warehouse) objects[1], false, this::runUi);
                break;
            case "checkWarehouseFail":
            case "getDistanceFail":
                showDialogConfirm(getString(R.string.msg_error_pickup_warehouse));
                break;
            case "getDistanceSuccess":
                onSelectedWarehouse((Warehouse) objects[1], (DistanceDTO) objects[2]);
                break;
            case "warehouseInfoInvalid":
                showDialogConfirm(getString(R.string.msg_error_info_warehouse));
                break;
        }
    }

    /**
     * @param warehouse kho trả hàng - drop
     * @param distanceDTO     thông tin quãng đường từ Hub đến warehouse
     */
    private void onSelectedWarehouse(Warehouse warehouse, DistanceDTO distanceDTO) {
        if(selectWareHouseVM==null){
            return;
        }
        List<AreaDistance> distanceList = new ArrayList<>();
        BillLadingDetail billDetail = selectWareHouseVM.getBillLadingDetail().get();

        //lấy area của pickup warehouse - phần tử đầu tiên.
        Area areaPickup = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getAreaInfo();

        assert billDetail != null;
        //nếu kho nhận khác hub với kho trả
        if (!areaPickup.getHubInfo().getName_seq().equals(warehouse.getAreaInfo().getHubInfo().getName_seq())) {
            //nếu kho nhận cùng zone với kho trả : hub -> hub
            if (areaPickup.getZoneInfo().getName_seq().equals(warehouse.getAreaInfo().getZoneInfo().getName_seq())) {
                distanceList.add(
                        selectWareHouseVM.getAreaDistance(areaPickup.getHubInfo(), warehouse.getAreaInfo().getHubInfo(), 2)
                );
                billDetail.setOrder_type(Constants.TYPE_IN_ZONE);
            } else { //nếu kho nhận khác zone với kho trả : hub -> depot -> depot -> hub
                //hub -> depot
                distanceList.add(
                        selectWareHouseVM.getAreaDistance(areaPickup.getHubInfo(), areaPickup.getZoneInfo().getDepotInfo(), 3)
                );
                //depot -> depot
                distanceList.add(
                        selectWareHouseVM.getAreaDistance(areaPickup.getZoneInfo().getDepotInfo(), warehouse.getAreaInfo().getZoneInfo().getDepotInfo(), 4)
                );
                //depot -> hub
                distanceList.add(
                        selectWareHouseVM.getAreaDistance(warehouse.getAreaInfo().getZoneInfo().getDepotInfo(), warehouse.getAreaInfo().getHubInfo(), 5)
                );
                billDetail.setOrder_type(Constants.TYPE_OUT_ZONE);
            }
        } else {
            billDetail.setOrder_type(Constants.TYPE_IN_ZONE);
        }

        //hub -> kho
        AreaDistance hubToWarehouse = new AreaDistance();
        hubToWarehouse.setType(6);
        hubToWarehouse.setDistance(distanceDTO.getCost());
        hubToWarehouse.setDuration(distanceDTO.getMinutes());
        hubToWarehouse.setFrom_name_seq(warehouse.getAreaInfo().getHubInfo().getName_seq());
        hubToWarehouse.setTo_name_seq(warehouse.getName_seq());
        hubToWarehouse.setFrom_warehouse_name(warehouse.getAreaInfo().getHubInfo().getName());
        hubToWarehouse.setTo_warehouse_name(warehouse.getName());
        hubToWarehouse.setFromLocation(new LatLng(warehouse.getAreaInfo().getHubInfo().getLatitude(), warehouse.getAreaInfo().getHubInfo().getLongitude()));
        hubToWarehouse.setToLocation(new LatLng(warehouse.getLatitude(), warehouse.getLongitude()));

        distanceList.add(hubToWarehouse);


        warehouse.setFromWareHouseCode(orderVM.getFromWareHouseCode());
        billDetail.setAreaDistances(distanceList);


        billDetail.setLatitude(warehouse.getLatitude());
        billDetail.setLongitude(warehouse.getLongitude());
        billDetail.setWarehouse(warehouse);
        billDetail.setWjson_address(warehouse.getWjson_address());
        billDetail.setAddress(warehouse.getAddress());
        billDetail.setZone_area_id(warehouse.getAreaInfo().getZoneInfo().getId());
        binding.etWareHouse.setText(warehouse.getName());
        binding.etPhone.setText(warehouse.getPhone());
        binding.spSelectWareHouse.setError(null);

        selectWareHouseVM.getBillLadingDetail().notifyChange();
    }

    private void showDialogConfirm(String msg) {
        dialogConfirm = new DialogConfirm(getString(R.string.receipt),
                msg, true, true, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogConfirm.dismiss();
            }
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    public void selectService() {
        dialogFragment = new ListDialogFragment(R.layout.item_service, R.string.select_attach_service,
                (listVM, runUi) -> listVM.getService(runUi)
                , this);
        dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }

    public void addReceiptWareHouse() {
        int valid = selectWareHouseVM.addReceiptWareHouse();
        goodsAdapter.notifyDataSetChanged();
        if (valid == 0) {
            orderVM.setCapacity(selectWareHouseVM.getBillLadingDetail().get(), false);
            onConfirm.onConfirm(selectWareHouseVM.getBillLadingDetail().get());
            isConfirm = true;
            this.dismiss();
        } else if (valid == 1) {
            binding.scrContent.scrollTo(0, 0);
        }else if(valid==3){
            ToastUtils.showToast(getString(R.string.Invalid_package_number));
        } else {
            if (this.billLadingDetail != null) {
                deleteWarehouse(getString(R.string.msg_empty_bill_package_delete_warehouse));
            } else {
                ToastUtils.showToast(getString(R.string.some_item_is_invalid));
            }
        }
    }

//    public void selectDate(View v) {
//        if (getContext() != null) {
//            Calendar c = Calendar.getInstance();
//            int hour = c.get(Calendar.HOUR_OF_DAY);
//            int minute = c.get(Calendar.MINUTE);
//            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), (view, hour1, minute1) -> {
//                Calendar calendar = Calendar.getInstance();
//                calendar.set(Calendar.HOUR_OF_DAY, hour1);
//                calendar.set(Calendar.MINUTE, minute1);
//                String date = AppController.formatTime.format(calendar.getTime());
//                switch (v.getId()) {
//                    case R.id.txtFromDate:
//                        ((TextView) v).setText(date);
//                        binding.ipFromDate.setError(null);
//                        selectWareHouseVM.getBillLadingDetail().get().setExpected_from_time(new OdooDateTime(calendar.getTime().getTime()));
//                        break;
//                    case R.id.txtToDate:
//                        ((TextView) v).setText(date);
//                        binding.ipToDate.setError(null);
//                        selectWareHouseVM.getBillLadingDetail().get().setExpected_to_time(new OdooDateTime(calendar.getTime().getTime()));
//                        break;
//                }
//            }, hour, minute, DateFormat.is24HourFormat(getActivity()));
//            timePickerDialog.show();
//        }
//    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        //trả lại rest nếu không confirm
        if (!isConfirm) {
            if (selectWareHouseVM.getBillLadingDetail().get().getWarehouse() != null &&
                    selectWareHouseVM.getBillLadingDetail().get().getWarehouse().getId() != null) {
                orderVM.getSelectedWarehouses().remove(selectWareHouseVM.getBillLadingDetail().get().getWarehouse().getId());
            }
            if (billLadingDetail != null) {
                for (int i = 0; i < billLadingDetail.getBillPackages().size(); i++) {
                    //trả lại số lượng ban đầu nếu ko confirm
                    billLadingDetail.getBillPackages().get(i).setQuantity_package(itemQuantities.get(i));
                    billLadingDetail.getBillPackages().get(i).setSelect(itemsChecked.get(i));
                }
                billLadingDetail.setBillService(billServices);
                billLadingDetail.setWarehouse(warehouse);
            }
        }
        selectWareHouseVM = null;
        super.onDismiss(dialog);

    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onItemClick(View view, Object o) {
        switch (view.getId()) {
            case R.id.chkSelect:
                BillPackage billPackage = (BillPackage) o;
                billPackage.setSelect(!billPackage.isSelect());
                goodsAdapter.notifyItemChanged(billPackage.index - 1);
                break;
            case R.id.itemService:
                onSelectedService((BillService) o);
                break;
        }
    }

    private void onSelectedService(BillService o) {
        if (selectWareHouseVM.getBillLadingDetail().get().getBillService().contains(o)) {
            ToastUtils.showToast(R.string.msg_service_exist);
        } else {
            selectWareHouseVM.getBillLadingDetail().get().getBillService().add(o);
            selectWareHouseVM.getBillLadingDetail().notifyChange();
        }
        dialogFragment.dismiss();
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }

    public interface OnConfirm {
        void onConfirm(BillLadingDetail bill);

        void onDelete(BillLadingDetail bill);
    }
}
