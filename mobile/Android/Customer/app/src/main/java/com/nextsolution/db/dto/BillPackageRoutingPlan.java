package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.util.StringUtils;
import com.tsolution.base.BaseModel;

import org.jetbrains.annotations.NotNull;

@lombok.Getter
@lombok.Setter
public class BillPackageRoutingPlan extends BaseModel implements Cloneable {
    private String __typename;
    private String QRchar;
    private Integer bill_lading_detail_id;
    private Integer bill_package_id;
    private Double capacity;
    private java.util.Date create_date;
    private String create_user;
    private Integer from_warehouse_id;
    private Double height;
    private Integer id;
    private String insurance_name;
    private String item_name;
    private Double length;
    private Integer quantity_plan;
    private Integer routing_plan_day_id;
    private String service_name;
    private Integer to_warehouse_id;
    private Double total_weight;
    private java.util.Date update_date;
    private String update_user;
    private Double width;
    private boolean hasQrCode;


    private int quantity;

    //properties for ui
    private int rest;
    private boolean isSelect;
    private int splitQuantity;//số lượng đã tách

    public String getQuantityStr() {
        return this.quantity == 0 ? "" : this.quantity + "";
    }

    public void setQuantityStr(String quantityStr) {
        if (!StringUtils.isNullOrEmpty(quantityStr)) {
            quantity = Integer.parseInt(quantityStr);
        } else {
            quantity = 0;
        }
    }

    public String getLengthStr() {
        return this.getLength() != null ? this.getLength().toString() : "";
    }

    public void setLengthStr(String lengthStr) {
        if (!StringUtils.isNullOrEmpty(lengthStr)) {
            if (lengthStr.charAt(0) == '.') {
                lengthStr = lengthStr.replace(".", "0.");
            }
            this.setLength(Double.parseDouble(lengthStr));
        } else {
            this.setLength(0d);
        }
    }

    public String getWidthStr() {
        return this.getWidth() != null ? this.getWidth().toString() : "";
    }

    public void setWidthStr(String widthStr) {
        if (!StringUtils.isNullOrEmpty(widthStr)) {
            if (widthStr.charAt(0) == '.') {
                widthStr = widthStr.replace(".", "0.");
            }
            this.setWidth(Double.parseDouble(widthStr));
        } else {
            this.setWidth(0d);
        }
    }

    public String getHeightStr() {
        return this.getHeight() != null ? this.getHeight().toString() : "";
    }

    public void setHeightStr(String heightStr) {
        if (!StringUtils.isNullOrEmpty(heightStr)) {
            if (heightStr.charAt(0) == '.') {
                heightStr = heightStr.replace(".", "0.");
            }
            this.setHeight(Double.parseDouble(heightStr));
        } else {
            this.setHeight(0d);
        }
    }

    public String getWeightStr() {
        return this.getTotal_weight() != null ? this.getTotal_weight().toString() : "";
    }

    public void setWeightStr(String weightStr) {
        if (!StringUtils.isNullOrEmpty(weightStr)) {
            if (weightStr.charAt(0) == '.') {
                weightStr = weightStr.replace(".", "0.");
            }
            this.setTotal_weight(Double.parseDouble(weightStr));
        } else {
            this.setTotal_weight(0d);
        }
    }

    @NotNull
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
            // This should never happen
        }
    }
}

