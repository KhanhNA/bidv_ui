package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SizeStandard extends BaseModel {
    private Integer length;
    private Integer width;
    private Integer height;
    private String type;
    private Integer price;
    private String from_weight;
    private String to_weight;
    private String long_unit_moved0;
    private String weight_unit_moved0;
    private Integer price_id;
    private Integer long_unit;
    private Integer weight_unit;
}
