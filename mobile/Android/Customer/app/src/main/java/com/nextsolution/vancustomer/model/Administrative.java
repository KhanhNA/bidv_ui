package com.nextsolution.vancustomer.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Administrative extends BaseModel {
    private Integer id;
    private Integer parentId;
    private String name;
    private String code;
    private Integer country_id;
}
