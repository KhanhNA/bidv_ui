package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.model.Vehicle;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingDetail extends BaseModel {
    private Integer id;
    private String type;// 0 nhận, 1 trả.(Đối với kho. Với vai trò là driver thì ngược lại)
    private String order_type; // 0 cố định, 1 phát sinh.
    private String insurance_name;
    private List<BillService> service_name = new ArrayList<>();
    private String routing_plan_detail_code;
    private Integer status;


    private String date_plan;
    private String expected_from_time;
    private String expected_to_time;
    private Vehicle vehicle ;
    private Double latitude;
    private Double longitude;
    private String address;
    private String phone;
    private String warehouse_name;
    private List<BillPackage> list_bill_package_import = new ArrayList<>();
    private List<BillPackage> list_bill_package_export = new ArrayList<>();

    private String routing_plan_day_code;
}