package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.StatusDialogVM;
import com.nextsolution.vancustomer.databinding.StatusDialogFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;

import java.util.Date;

public class StatusDialogFragment extends DialogFragment {
    Integer position;
    IGetRadioChecked iGetRadioChecked;
    StatusDialogVM statusDialogVM;
    StatusDialogFragmentBinding mBinding;
    RadioGroup.OnCheckedChangeListener onCheckedChangeListener;
    Date date;

    public StatusDialogFragment(Date date, Integer position, IGetRadioChecked iGetRadioChecked) {
        this.position = position;
        this.onCheckedChangeListener = onCheckedChangeListener;
        this.date = date;
        this.iGetRadioChecked=iGetRadioChecked;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.status_dialog_fragment, container, false);
        statusDialogVM = ViewModelProviders.of(this).get(StatusDialogVM.class);
        mBinding.setViewModel(statusDialogVM);
        mBinding.setListener(this);
        mBinding.btnClose.setOnClickListener(v->{dismiss();});
        init();
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        getData();
        return mBinding.getRoot();
    }

    private void getData() {
        statusDialogVM.getQuantityTypeStatusRoutingDay(this::runUI, date);
    }

    public void init() {
        switch (position) {
            case 1:
                mBinding.rbUnConfirm.setChecked(true);
                break;
            case 2:
                mBinding.rbDriverConfirm.setChecked(true);
                break;
            case 3:
                mBinding.rbConfirmAndComplete.setChecked(true);
                break;
            case 4:
                mBinding.rbDelete.setChecked(true);
                break;
            case 5:
                mBinding.rbWaitConfirm.setChecked(true);
                break;
            default:
                mBinding.rbAll.setChecked(true);
                break;
        }
    }
    public void onClick(View view){
        switch (view.getId()) {
            case R.id.rbAll:
                iGetRadioChecked.getRadioCheck(0,getString(R.string.ALL));
                break;
            case R.id.rbUnConfirm:
                iGetRadioChecked.getRadioCheck(1,getString(R.string.Shipping));
                break;
            case R.id.rbDriverConfirm:
                iGetRadioChecked.getRadioCheck(2,getString(R.string.Driver_confirmed));
                break;
            case R.id.rbConfirmAndComplete:
                iGetRadioChecked.getRadioCheck(3,getString(R.string.completed));
                break;
            case R.id.rbDelete:
                iGetRadioChecked.getRadioCheck(4,getString(R.string.Order_cancelled));
                break;
            case R.id.rbWaitConfirm:
                iGetRadioChecked.getRadioCheck(5,getString(R.string.waiting_for_confirm_change));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    public void runUI(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_FAIL)) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.NOT_FOUND_DATA), getResources().getDrawable(R.drawable.ic_fail));
        } else if (action.equals(Constants.GET_DATA_SUCCESS)) {
            mBinding.rbUnConfirm.setText(getString(R.string.Shipping) + " (" + statusDialogVM.getList().get(0) + ")");
            mBinding.rbDriverConfirm.setText(getString(R.string.Driver_confirmed) + " (" + statusDialogVM.getList().get(1) + ")");
            mBinding.rbConfirmAndComplete.setText(getString(R.string.completed) + " (" + statusDialogVM.getList().get(2) + ")");
            mBinding.rbDelete.setText(getString(R.string.Order_cancelled) + " (" + statusDialogVM.getList().get(3) + ")");
            mBinding.rbWaitConfirm.setText(getString(R.string.waiting_for_confirm_change) + " (" + statusDialogVM.getList().get(4) + ")");
            mBinding.rbAll.setText(getString(R.string.ALL) + " (" + statusDialogVM.getList().get(5) + ")");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
    public interface IGetRadioChecked{
        void getRadioCheck(int position,String txt);
    }
}
