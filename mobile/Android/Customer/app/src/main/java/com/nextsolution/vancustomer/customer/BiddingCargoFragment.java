package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.king.zxing.Intents;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.customer.vm.BiddingCargoVM;
import com.nextsolution.vancustomer.databinding.BiddingCargoFragmentBinding;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.nextsolution.vancustomer.ui.MainActivity;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.INotifyData;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class BiddingCargoFragment extends BaseFragment {
    private final MainActivity mActivity;
    BiddingCargoFragmentBinding mBinding;
    private MenuItem preItem;
    BiddingCargoVM biddingCargoVM;

    List<Integer> notReceiveStatus;
    List<Integer> receivedStatus;
    BiddingCargoChildFragment haveNotReceive;
    BiddingCargoChildFragment Received;


    private final AppBarLayout.OnOffsetChangedListener appBarOffsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                mActivity.animationFAB(getTag(), false);
            } else {
                mActivity.animationFAB(getTag(), true);
            }

        }
    };

    public BiddingCargoFragment(MainActivity activity) {
        this.mActivity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (BiddingCargoFragmentBinding) binding;
        biddingCargoVM = (BiddingCargoVM) viewModel;

        mBinding.mainAppbar.addOnOffsetChangedListener(appBarOffsetChangedListener);

        navigationView();
        return v;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        switch (view.getId()) {
            case R.id.qrCode:
                try {
                    Intent qrScan = new Intent(getContext(), QrScannerActivity.class);
                    startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnChooseType:
                openDialogSelectType(view);
                break;
        }
    }

    @SuppressLint("RestrictedApi")
    private void openDialogSelectType(View view) {
//Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_select_type, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            Integer type = 0;
            switch (item.getItemId()) {
                case R.id.action_export:
                    type = 0;
                    break;
                case R.id.action_import:
                    type = 1;
                    break;
            }
            if (type != biddingCargoVM.getType().get()) {
                biddingCargoVM.getType().set(type);
                refreshData();
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void refreshData() {
        notReceiveStatus.clear();
        receivedStatus.clear();
        if (biddingCargoVM.getType().get() == 0) {
            mBinding.navigation.getMenu().getItem(0).setTitle(R.string.not_yet_export);
            mBinding.navigation.getMenu().getItem(1).setTitle(R.string.exported);
            mBinding.navigation.getMenu().getItem(1).setIcon(R.drawable.ic_vans);
            notReceiveStatus.add(0);

            receivedStatus.add(1);
        } else {
            mBinding.navigation.getMenu().getItem(0).setTitle(R.string.Have_not_received);
            mBinding.navigation.getMenu().getItem(1).setTitle(R.string.received);
            mBinding.navigation.getMenu().getItem(1).setIcon(R.drawable.ic_access);
            notReceiveStatus.add(0);
            notReceiveStatus.add(1);

        }
        receivedStatus.add(2);
        haveNotReceive.onRefresh(biddingCargoVM.getType().get());
        Received.onRefresh(biddingCargoVM.getType().get());

    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                handleData();
                break;
            case Constants.GET_DATA_FAIL:
                break;
        }
    }

    private void handleData() {
        if (biddingCargoVM.getBiddingOrder().getStatus().equals("2")) {
            mBinding.frameContainer.setCurrentItem(1);
        } else {
            mBinding.frameContainer.setCurrentItem(2);
        }
        EventBus.getDefault().post(biddingCargoVM.getBiddingOrder());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Intents.Scan.RESULT)) {
            biddingCargoVM.setTxtSearch(data.getStringExtra(Intents.Scan.RESULT));
            List<Integer> status = new ArrayList<>();
            status.add(0);
            status.add(1);
            status.add(2);
            biddingCargoVM.getBiddingCargo(1, status, this::runUi);
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }


    /**
     * type 0: xuất hàng - depotId = fromDepotId, 1: nhập hàng - depotId = toDepotId
     * orderStatus: 0: lái xe chưa nhận hàng, 1: toàn bộ lái xe đã nhận hàng, 2: toàn bộ hàng đã được giao thành công.
     * <p>
     *      - Nếu this.type = 0 (xuất hàng)
     *          + order status : 0 - chưa xuất hàng
     *          + order status : 1, 2 - đã xuất hàng thành công
     *      - Nếu this.type = 1 (Nhập hàng)
     *          + order status : 0, 1 - chưa nhận hàng
     *          + order status : 2 - đã nhận hàng thành công.
     * </p
     */
    private void navigationView() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        notReceiveStatus = new ArrayList<>();
        receivedStatus = new ArrayList<>();

        if (biddingCargoVM.getType().get() == 0) {
            notReceiveStatus.add(0);

            receivedStatus.add(1);
            receivedStatus.add(2);
        } else {
            notReceiveStatus.add(0);
            notReceiveStatus.add(1);

            receivedStatus.add(2);
        }

        haveNotReceive = new BiddingCargoChildFragment(biddingCargoVM.getType().get(), notReceiveStatus);
        Received = new BiddingCargoChildFragment(biddingCargoVM.getType().get(), receivedStatus);
        haveNotReceive.setINotifyData(new INotifyData() {
            @Override
            public void reloadData(boolean b) {
                Received.reLoad();
            }
        });

        myPagerAdapter.addFragment(haveNotReceive);
        myPagerAdapter.addFragment(Received);


        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.have_not_received:
                    biddingCargoVM.getHideMaps().set(false);
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.received:
                    biddingCargoVM.getHideMaps().set(true);
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }


    @Override
    public int getLayoutRes() {
        return R.layout.bidding_cargo_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BiddingCargoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
