package com.nextsolution.db.api;

import android.app.Notification;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.NotificationDTO;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.squareup.moshi.Json;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class NotificationAPI extends BaseApi {
    private static final String GET_NOTIFICATION = "/notification";
    private static final String SEEN_NOTIFICATION = "/notification/is_read";
    private static final String IS_NOTIFICATION = "/notification/check_user_read_message";
    public static void getNotification(Integer offset,String type,Integer id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("type",type);
            params.put("notification_id",id);
            params.put("offset",offset);
            mOdoo.callRoute(GET_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationDTO> notificationDTOOdooResultDto) {
                    result.onSuccess(notificationDTOOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
    public static void getNotificationById(Integer id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("id", mGson.toJson(id));
            mOdoo.callRoute(GET_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationDTO> notifications) {
                        result.onSuccess(notifications);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
    public static void seenNotification(Integer is_read, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("id",is_read);
            mOdoo.callRoute(SEEN_NOTIFICATION, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
    public static void isNotification( IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            mOdoo.callRoute(IS_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationDTO> notificationDTOOdooResultDto) {
                    result.onSuccess(notificationDTOOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
