package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.CargoBillPackage;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoBillPackageDialogVM extends BaseViewModel {
    ObservableField<CargoBillPackage> cargobillPackage =  new ObservableField<>();
    ObservableField<String> qr_code= new ObservableField<>();
    public CargoBillPackageDialogVM(@NonNull Application application) {
        super(application);
    }
}
