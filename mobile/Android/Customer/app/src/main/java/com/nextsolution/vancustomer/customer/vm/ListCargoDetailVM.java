package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.CargoBillLadingDetail;
import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.db.dto.CargoDTO;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListCargoDetailVM extends BaseViewModel {
    ObservableField<CargoDTO> cargoDto = new ObservableField<>();
    ObservableList<CargoBillPackage> cargoBillPackages = new ObservableArrayList<>();
    public ArrayList<CargoBillPackage> arrCargoBillPackage = new ArrayList<>();
    ObservableBoolean emptyData = new ObservableBoolean();
    ArrayList<String> qrCode = new ArrayList<>();
    ArrayList<Integer> indexQrResult = new ArrayList<>();
    ArrayList<Integer> listIdBillPackage = new ArrayList<>();

    public ListCargoDetailVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
    }

    public void getCargoDetail(RunUi runUi) {
        DepotAPI.getCargoDetail(cargoDto.get().getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        OdooResultDto<CargoBillLadingDetail> resultDto = (OdooResultDto<CargoBillLadingDetail>) o;
        try {
            cargoBillPackages.clear();
            setBillPackage(resultDto.getRecords());
            setQrCode();
            if (resultDto.getRecords().size() == 0) {
                emptyData.set(true);
            } else
                emptyData.set(false);
            runUi.run(Constants.GET_DATA_SUCCESS);
        } catch (Exception e) {
            emptyData.set(true);
            e.printStackTrace();
        }
    }

    public void gomCargo(RunUi runUi) {
        DepotAPI.gomCargo(cargoDto.get().getId(), listIdBillPackage, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                Boolean aBoolean = (Boolean) o;
                if (aBoolean) {
                    runUi.run(Constants.GOM_CARGO_SUCCESS);
                } else {
                    runUi.run(Constants.GOM_CARGO_FAIL);
                }

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public Boolean checkValidate() {
        listIdBillPackage.clear();
        if (cargoBillPackages.size() != indexQrResult.size() || cargoBillPackages.size()==0) {
            return false;
        }
        try {
            for (CargoBillPackage item : cargoBillPackages) {
                listIdBillPackage.add(item.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void setBillPackage(List<CargoBillLadingDetail> data) {
        try {
            for (CargoBillLadingDetail item : data) {
                cargoBillPackages.addAll(item.getList_bill_package());
                arrCargoBillPackage.addAll(item.getList_bill_package());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setQrCode() {
        try {
            for (int i = 0; i < arrCargoBillPackage.size(); i++) {
                qrCode.add(arrCargoBillPackage.get(i).getBill_package_routing_plan().getQr_char());
                arrCargoBillPackage.get(i).setQr_char();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
