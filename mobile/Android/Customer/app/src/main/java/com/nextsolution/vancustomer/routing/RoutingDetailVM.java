package com.nextsolution.vancustomer.routing;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillPackageRoutingPlan;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.ShipmentStandStill;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;

/**
 * @author Good_Boy
 */
@Getter

public class RoutingDetailVM extends BaseViewModel {
    ObservableField<RoutingVanDay> routingVanDay;
    List<BillPackageRoutingPlan> billPackageRoutingPlanList;
    HashMap<Integer, BillPackageRoutingPlan> billPackageRoutingPlanMap;
    ObservableBoolean enablePickup = new ObservableBoolean(false);

    public RoutingDetailVM(@NonNull Application application) {
        super(application);
        billPackageRoutingPlanList = new ArrayList<>();
        billPackageRoutingPlanMap = new HashMap<>();
        routingVanDay = new ObservableField<>();
    }

    public void setRoutingVanDay(ShipmentStandStill shipmentStandStill) {
        routingVanDay.set((RoutingVanDay) shipmentStandStill);
    }

    public void getRoutingDetail(RoutingVanDay aRoute, RunUi runUi) {
        routingVanDay.set(aRoute);
        RoutingApi.getRoutingDetail(aRoute.getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responseRouting(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        }, runUi);
    }

    private void responseRouting(Object response, RunUi runUi) {
        System.out.println("result" + response);

        billPackageRoutingPlanList.clear();
        if (response == null) {
            System.out.println("response null");
            return;
        }

        List<BillPackageRoutingPlan> billPackageRoutingPlans = (List<BillPackageRoutingPlan>) response;
        for (BillPackageRoutingPlan bill : billPackageRoutingPlans) {
            billPackageRoutingPlanList.add(bill);
        }
        runUi.run(billPackageRoutingPlanList);
    }


    public void updateVanForNewRoute(Integer routeId, RunUi runUi) {
        Integer vanId = StaticData.getVanId();
        RoutingApi.updateVanForNewRoute(routeId, vanId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responseUpdateVanForNewRoute(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        }, runUi);
    }

    private void responseUpdateVanForNewRoute(Object response, RunUi runUi) {
        System.out.println("result" + response);
//        StaticData.clearRoutes();
//        StaticData.setRoutes( ((GetRoutingVanDayQuery.Data)response.data()).getRoutingVanDay());
//
//        RunUi runUi = (RunUi)params[0];
//        runUi.run(response, throwable);
    }


    public void updateBillPackage(String qrCode) {
        if (qrCode == null || "".equals(qrCode)) {
            return;
        }
        boolean meet = false;
        int qrCodeCount = 0;
        for (BillPackageRoutingPlan item : billPackageRoutingPlanList) {

            if (!meet && qrCode.equals(item.getQRchar())) {
                item.setHasQrCode(true);
                meet = true;
//                break;
            }
            if (item.isHasQrCode()) {
                qrCodeCount++;
            }
        }
        if (qrCodeCount == billPackageRoutingPlanList.size()) {
            enablePickup.set(true);
        }


    }

    public void pickup() {
        if (routingVanDay == null) {
            System.out.println("error: routingVanDay is null");
            return;
        }
        RoutingApi.pickup(routingVanDay.get().getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responsePickup(o);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void responsePickup(Object response) {
        System.out.println("wait for customer reply");
    }


    public void putPackageScan(BillPackageRoutingPlan billPackageRoutingPlan) {
        billPackageRoutingPlanMap.put(billPackageRoutingPlan.getId(), billPackageRoutingPlan);
    }

    public void updateBillPackage(int id, String qrCode) {
        BillPackageRoutingPlan billPackageRoutingPlan = billPackageRoutingPlanMap.get(id);
        if (billPackageRoutingPlan == null) {
            return;
        }
        if (qrCode != null && (qrCode.equals(billPackageRoutingPlan.getItem_name()) || "nguyễn văn thọ".equals(qrCode))) {
            billPackageRoutingPlan.setHasQrCode(true);
        } else {
            billPackageRoutingPlan.setHasQrCode(false);
        }
    }
}
