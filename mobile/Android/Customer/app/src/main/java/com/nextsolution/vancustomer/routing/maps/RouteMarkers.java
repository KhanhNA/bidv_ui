package com.nextsolution.vancustomer.routing.maps;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.ShipmentStandStill;
import com.nextsolution.db.dto.Van;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.routing.RoutingDetailFragment;
import com.nextsolution.vancustomer.routing.RoutingVM;
import com.nextsolution.vancustomer.base.StaticData;


import com.tsolution.base.BaseActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;

public class RouteMarkers extends AsyncTask<String, Void, RoutingVanDay> implements GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMapClickListener {

    GoogleMap map;
    private RoutingVM routingVM;
    private Marker mSelectedMarker;
    private Activity activity;

    public RouteMarkers(BaseActivity activity, RoutingVM routingVM, GoogleMap map) {
        this.map = map;
        this.activity = activity;
        this.routingVM = routingVM;
        map.setOnInfoWindowClickListener(this);
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);
//        map.setInfoWindowAdapter(new RoutingInfoWindowAdapter(activity));
    }

    //    public void addMarkers(List<RoutingVanDay> routings) throws IOException {
//        //map.clear();
//        // Creating MarkerOptions
//
//        for(RoutingVanDay aRoute: routings){
//            if(aRoute.getId() >=  4){
//                continue;
//            }
//            addMarker(aRoute);
//        }
//        // Add new marker to the Google Map Android API V2
//
//
//    }
    public void addRouteMarkerForVan(Van van) throws IOException {
        map.clear();
        // Creating MarkerOptions
        addMarker(van);
        RoutingVanDay aRoute = van.getFirstRoute();

        while (aRoute != null) {
            addMarker(aRoute);
            aRoute = aRoute.getNext();
        }

    }

    public void addRouteMarkerForCustomer() throws IOException {
        map.clear();
        // Creating MarkerOptions

        for (RoutingVanDay aRoute : StaticData.getRoutingVanDays()) {
            addMarker(aRoute);
        }

    }

    public void addMarker(RoutingVanDay aRoute) throws IOException {
        new DownloadImageTask().execute(aRoute);
    }

    public void addMarker(Van van) {
        new DownloadImageTask().execute(van);
    }

    @Override
    protected RoutingVanDay doInBackground(String... strings) {
        return null;
    }

    private class DownloadImageTask extends AsyncTask<ShipmentStandStill, Void, Bitmap> {
        ShipmentStandStill standStill;

        @Override
        protected Bitmap doInBackground(ShipmentStandStill... routings) {
            Bitmap bitmap = null;
            try {
                this.standStill = routings[0];
                final URL url = standStill.getAvatarUrl();//routings[0].create_user();


                final InputStream inputStream = url.openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception malformedUrlException) {
                // Handle error
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap downloadedBitmap) {
            MarkerOptions options = new MarkerOptions();
            if (downloadedBitmap != null) {

                options.icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(downloadedBitmap, 50, 60, false)));
            } else {
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.badge_qld));
            }

            options.position(standStill.getLatLng());
            Marker marker = map.addMarker(options);
            marker.setTag(standStill);
            standStill.setMarker(marker);
            if (standStill instanceof RoutingVanDay && standStill != null && ((RoutingVanDay) standStill).getVan() != null) {
                addMarker(((RoutingVanDay) standStill).getVan());
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        System.out.println("tesssssssssssssssssst: click marker");
//        RouteActionFragment addPhotoBottomDialogFragment =
//                RouteActionFragment.newInstance((ShipmentStandStill)marker.getTag());
//        addPhotoBottomDialogFragment.show(((BaseActivity)activity).getSupportFragmentManager(),
//                "add_photo_dialog_fragment");
        // The user has re-tapped on the marker which was already showing an info window.
        if (marker.equals(mSelectedMarker)) {
            // The showing info window has already been closed - that's the first thing to happen
            // when any marker is clicked.
            // Return true to indicate we have consumed the event and that we do not want the
            // the default behavior to occur (which is for the camera to move such that the
            // marker is centered and for the marker's info window to open, if it has one).
            mSelectedMarker = null;
            routingVM.getFabOkVisible().set(false);
            return true;
        }

        mSelectedMarker = marker;
        ShipmentStandStill shipment = (ShipmentStandStill)marker.getTag();
        routingVM.getStandStill().set(shipment);

        if(shipment instanceof Van){
            routingVM.getIsRouting().set(false);
        }else {
            routingVM.getIsRouting().set(true);
        }
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
//        Intent intent = new Intent(getBaseActivity(), CommonActivity.class);
//        intent.putExtra(StaticData.FRAGMENT, RoutingDetailFragment.class);
//        intent.putExtra(StaticData.ROUTE_DETAIL, );
//        startActivity(intent);
//
        Serializable tag = (Serializable) marker.getTag();
        if (tag instanceof RoutingVanDay) {
            AppController.getInstance().newActivity(activity, RoutingDetailFragment.class, StaticData.ROUTE_DETAIL, tag);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mSelectedMarker = null;
        RoutingVanDay aRoute = StaticData.getCurrentRouteProcessed();
        routingVM.getStandStill().set(aRoute);
        Van van = StaticData.getStaffVan().getVan();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(van.getLatLng())
                .zoom(16).build()));
    }

}
