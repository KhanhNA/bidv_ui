package com.nextsolution.db.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.nextsolution.db.dto.Area;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.model.Administrative;
import com.nextsolution.vancustomer.model.PlacesPOJO;
import com.nextsolution.vancustomer.network.SOGraphService;
import com.nextsolution.vancustomer.network.SOService;
import com.nextsolution.vancustomer.util.TsUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.BaseClient;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarehouseApi extends BaseApi {
    private static final String GET_WAREHOUSE = "/share_van_order/get_warehouse";
    private static final String ROUTE_CHECK_WAREHOUSE = "/share_van_order/check_warehouse_info";
    public static final String googleApi = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_PLACE_API_KEY = "AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk";

    public static void getCustWarehouse(Long companyId, IResponse result) {
        JSONObject params;
        try {
            Log.d("getCustWarehouse", "getCustWarehouse: ");
            params = new JSONObject();
            params.put("companyId", companyId);
            mOdoo.callRoute(GET_WAREHOUSE, params, new SharingOdooResponse<OdooResultDto<Warehouse>>() {
                @Override
                public void onSuccess(OdooResultDto<Warehouse> area) {
                    result.onSuccess(area);

                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void checkIsExistWarehouse(String wJsonAddress, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("warehouse", wJsonAddress);
            mOdoo.callRoute(ROUTE_CHECK_WAREHOUSE, params, new SharingOdooResponse<List<Area>>() {
                @Override
                public void onSuccess(List<Area> area) {
                    response.onSuccess(area);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }


    public static void doPlaces(Context context, String key, IResponse result) {
        BaseClient<SOGraphService> client = new BaseClient<>(context, googleApi, SOGraphService.class);
        client.getServices().doPlaces(key, GOOGLE_PLACE_API_KEY).enqueue(new Callback<PlacesPOJO.Root>() {
            @Override
            public void onResponse(Call<PlacesPOJO.Root> call, Response<PlacesPOJO.Root> response) {
                if (response == null || response.errorBody() != null) {
                    ErrorHandler.create().handle(new Exception(response.errorBody().toString()));
                    return;
                }
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<PlacesPOJO.Root> call, Throwable t) {
                ErrorHandler.create().handle(t);
            }
        });
    }

    public static void getPlaceDetail(Context context, String placeId, IResponse result) {
        BaseClient<SOGraphService> client = new BaseClient<>(context, googleApi, SOGraphService.class);
        client.getServices().getPlaceDetail(placeId, "formatted_address,address_component", GOOGLE_PLACE_API_KEY).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response == null || response.errorBody() != null) {
                    ErrorHandler.create().handle(new Exception(response.errorBody().toString()));
                    return;
                }
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                ErrorHandler.create().handle(t);
            }
        });
    }


    public static void getAdministrative(int parent_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("parent_id", parent_id);
            mOdoo.callRoute("/customer/get_allow_area", params, new SharingOdooResponse<OdooResultDto<Administrative>>() {

                @Override
                public void onSuccess(OdooResultDto<Administrative> administrative) {
                    if (administrative != null && TsUtils.isNotNull(administrative.getRecords())) {
                        result.onSuccess(administrative.getRecords());
                    } else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }


    public static void saveWarehouseInfo(Warehouse addressDto, boolean isDelete, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            if (isDelete) {
                String json = "{\"id\": " + +addressDto.getId() +
                        ", \"status\": \"deleted\"}";
                params.put("warehouse", new JSONObject(json));
            } else {
                params.put("warehouse", new JSONObject(mGson.toJson(addressDto)));
            }
            mOdoo.callRoute("/customer/create_update_warehouse", params, new SharingOdooResponse<OdooResultDto<Warehouse>>() {

                @Override
                public void onSuccess(OdooResultDto<Warehouse> odooResultDto) {
                    if (odooResultDto != null) {
                        response.onSuccess(odooResultDto.getRecords());
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void editWarehouseInfo(Warehouse addressDto, Boolean isDelete, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        RequestBody bodyJson;
        try {
            if (isDelete) {
                String json = "{\"id\": " + +addressDto.getId() +
                        ", \"status\": \"deleted\"}";
                bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
            } else {
                if (addressDto.getAreaInfo() != null)
                    addressDto.setArea_id(addressDto.getAreaInfo().getId());
                bodyJson = RequestBody.create(MediaType.parse("application/json"), mGson.toJson(addressDto));

            }
            Call<List<Warehouse>> call = soService.createWarehouse(StaticData.sessionCookie, bodyJson);
            call.enqueue(new Callback<List<Warehouse>>() {
                @Override
                public void onResponse(Call<List<Warehouse>> call, Response<List<Warehouse>> response) {
                    result.onSuccess(response);
                }

                @Override
                public void onFailure(Call<List<Warehouse>> call, Throwable t) {
                    result.onFail(t.getCause());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
