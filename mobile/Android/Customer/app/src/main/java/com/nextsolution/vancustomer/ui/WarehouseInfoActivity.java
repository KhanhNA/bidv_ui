package com.nextsolution.vancustomer.ui;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.databinding.WarehouseInfoActivityBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.ErrorSaveWarehouse;
import com.nextsolution.vancustomer.ui.fragment.AddressBottomSheetFragment;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.viewmodel.WarehouseInfoVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.io.IOException;
import java.util.List;

public class WarehouseInfoActivity extends BaseActivity<WarehouseInfoActivityBinding> {
    WarehouseInfoVM warehouseInfoVM;
    private int action;//1.Thêm, 2.Sửa, 3.Xóa
    private boolean delete_able;//có cho phép xóa hay ko
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        warehouseInfoVM = (WarehouseInfoVM) viewModel;
        initToolbar();
        initView();
        setMessage();
    }

    private void initView() {
        Intent intent = getIntent();
        Warehouse warehouseAddress = (Warehouse) intent.getSerializableExtra("WAREHOUSE");
        position = intent.getIntExtra("POSITION", -1);
        delete_able = intent.getBooleanExtra("DELETE_ABLE", true);
        if (warehouseAddress != null) {
            action = 2;
            warehouseInfoVM.getModel().set(warehouseAddress);
            binding.province.setText(warehouseAddress.getAdministrative());
            // không cho sửa địa chỉ
            binding.province.setFocusable(false);
            binding.province.setLongClickable(false);

            binding.etAddress.setFocusable(false);
            binding.etAddress.setLongClickable(false);
            getSupportActionBar().setTitle(R.string.edit_warehouse);
        } else {
            action = 1;
        }

        binding.etFullName.setFilters(AppController.getFilter());
        binding.etAddress.setFilters(AppController.getFilter());
        if (action != 2)
            binding.province.setOnClickListener(v -> selectAdministrative());
        binding.btnConfirm.setOnClickListener(v -> {
            if (getLatLngFromAddress()) {
                warehouseInfoVM.saveWarehouseInfo(this::runUi);
            }
        });

        binding.btnDelete.setOnClickListener(v -> {
            if (delete_able) {
                showDialogDeleteWarehouse();
            } else {
                ToastUtils.showToast(getString(R.string.msg_can_not_delete_warehouse));
            }

        });

        binding.txtProvince.setEndIconOnClickListener(v -> {
            warehouseInfoVM.getModelE().setProvince_name(null);
            warehouseInfoVM.getModelE().setDistrict_name(null);
            warehouseInfoVM.getModelE().setWard_name(null);
            binding.province.setText("");
        });
    }

    private boolean getLatLngFromAddress() {
        if (warehouseInfoVM.isValid(warehouseInfoVM.getModelE())) {
            warehouseInfoVM.getIsLoading().set(true);
            Geocoder coder = new Geocoder(this);
            List<Address> address;
            try {
                address = coder.getFromLocationName(warehouseInfoVM.getModelE().getAddressDetail(), 1);
                if (address == null || address.size() == 0) {
                    binding.txtAddress.setEndIconMode(TextInputLayout.END_ICON_NONE);
                    warehouseInfoVM.addError("address", getString(R.string.this_address_not_valid), true);
                    warehouseInfoVM.getIsLoading().set(false);
                    return false;
                }
                Address location = address.get(0);
                warehouseInfoVM.getModelE().setLatitude(location.getLatitude());
                warehouseInfoVM.getModelE().setLongitude(location.getLongitude());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    private void runUi(Object[] objects) {
        String action = (String) objects[0];

        switch (action) {
            case "saveAddressSuccess":
                if (this.action == 2) {
                    ToastUtils.showToast(getString(R.string.update_success));
                } else {
                    ToastUtils.showToast(getString(R.string.insert_success));
                }
                Intent intent = new Intent();
                intent.putExtra("WAREHOUSE", warehouseInfoVM.getWarehouse());
                intent.putExtra("POSITION", position);
                intent.putExtra("ACTION", this.action);
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            case "saveAddressFalse":
                if (objects.length > 1) {
                    String action_save_fail = (String) objects[1];
                    handleSaveWarehouseFail(action_save_fail);
                } else {
                    ToastUtils.showToast(getString(R.string.msg_save_false));
                }
                warehouseInfoVM.getIsLoading().set(false);
                break;
            case "deleteAddressFail":
                ToastUtils.showToast(getString(R.string.msg_delete_fail));
                warehouseInfoVM.getIsLoading().set(false);
                break;
            case "deleteAddressSuccess":
                Intent intent1 = new Intent();
                this.action = 3;
                intent1.putExtra("POSITION", position);
                intent1.putExtra("ACTION", this.action);
                setResult(Activity.RESULT_OK, intent1);
                ToastUtils.showToast(getString(R.string.delete_success));
                finish();
        }

    }

    public void handleSaveWarehouseFail(String action) {
        switch (action) {
            case ErrorSaveWarehouse.DEACTIVATE_FAIL:
                ToastUtils.showToast(getString(R.string.delete_warehouse_fail));
                break;
            case ErrorSaveWarehouse.DEACTIVATE_SUCCESSFUL:
                ToastUtils.showToast(getString(R.string.delete_success));
                break;
            case ErrorSaveWarehouse.NOT_PERMISSION:
                ToastUtils.showToast(getString(R.string.not_permission));
                break;
            case ErrorSaveWarehouse.SAME_ADDRESS:
                ToastUtils.showToast(getString(R.string.Address_already_exists));
                break;
            case ErrorSaveWarehouse.SAME_NAME:
                ToastUtils.showToast(getString(R.string.Name_warehouse_already_exists));
                break;
            case ErrorSaveWarehouse.SAME_PHONE:
                ToastUtils.showToast(getString(R.string.Phone_already_exists));
                break;
            case ErrorSaveWarehouse.WAREHOUSE_RUNNING:
                ToastUtils.showToast(getString(R.string.WAREHOUSE_RUNNING));
                break;
            default:
                ToastUtils.showToast(getString(R.string.msg_save_false));
                break;
        }
    }

    private void setMessage() {
        warehouseInfoVM.setMessagePleaseEnterAddress(getString(R.string.please_enter_address));
        warehouseInfoVM.setMessagePleaseEnterFullName(getString(R.string.please_enter_warehouse_name));
        warehouseInfoVM.setMessagePleaseEnterPhoneNumber(getString(R.string.please_enter_phone_number));
        warehouseInfoVM.setMessageRequiteProvince(getString(R.string.requite_province));
    }

    private void selectAdministrative() {
        AddressBottomSheetFragment dialogProvince = new AddressBottomSheetFragment(0, Constants.ADMINISTRATIVE_ACTION_PROVINCE, (administrativeActionProvince, province) -> {
            if (administrativeActionProvince == Constants.ADMINISTRATIVE_ACTION_PROVINCE && province != null) {
                AddressBottomSheetFragment dialogDistrict = new AddressBottomSheetFragment(province.getId(), Constants.ADMINISTRATIVE_ACTION_DISTRICT, (administrativeActionDistrict, district) -> {
                    if (administrativeActionDistrict == Constants.ADMINISTRATIVE_ACTION_DISTRICT && district != null) {
                        AddressBottomSheetFragment dialogVillage = new AddressBottomSheetFragment(district.getId(), Constants.ADMINISTRATIVE_ACTION_VILLAGE, (administrativeAction, village) -> {
                            if (administrativeAction == Constants.ADMINISTRATIVE_ACTION_VILLAGE && village != null) {
                                binding.province.setText(String.format(getString(R.string.address_register), province.getName(), district.getName(), village.getName()));
                                warehouseInfoVM.getModelE().setState_id(province.getId());
                                warehouseInfoVM.getModelE().setDistrict(district.getId());
                                warehouseInfoVM.getModelE().setWard(village.getId());
                                warehouseInfoVM.getModelE().setCountry_id(province.getCountry_id());

                                warehouseInfoVM.getModelE().setProvince_name(province.getName());
                                warehouseInfoVM.getModelE().setDistrict_name(district.getName());
                                warehouseInfoVM.getModelE().setWard_name(village.getName());
                                binding.etAddress.setText("");
                            }
                        }, province.getName(), district.getName());
                        dialogVillage.show(getSupportFragmentManager(), dialogVillage.getTag());
                    }
                }, province.getName());
                dialogDistrict.show(getSupportFragmentManager(), dialogDistrict.getTag());
            }
        });
        dialogProvince.show(getSupportFragmentManager(), dialogProvince.getTag());
    }


    private void showDialogDeleteWarehouse() {
        DialogConfirm dialog = new DialogConfirm(getString(R.string.confirm), getString(R.string.msg_confirm_delete_rwarehouse),
                v -> warehouseInfoVM.deleteWarehouse(WarehouseInfoActivity.this::runUi));
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.warehouse_info_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WarehouseInfoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
