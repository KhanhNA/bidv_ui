package com.nextsolution.vancustomer.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.CreateAccountChildVM;
import com.nextsolution.vancustomer.databinding.CreateAccountFragmentBinding;
import com.nextsolution.vancustomer.widget.NonSwipeableViewPager;
import com.nextsolution.vancustomer.widget.TabAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CreateAccountFragment extends BaseFragment {
    CreateAccountChildVM viewModel;
    CreateAccountFragmentBinding mBinding;
    int tabPosition = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(CreateAccountChildVM.class);
        binding.setVariable(BR.viewModel, viewModel);
        mBinding = (CreateAccountFragmentBinding) binding;
        navigationView();
        initView();
        return v;
    }

    private void initView() {
        mBinding.toolbar.setTitle(R.string.create_account);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(Integer position) {
        mBinding.tabLayout.getTabAt(position).select();
        mBinding.viewPager.setCurrentItem(position, false);
    }
    private void navigationView() {

        mBinding.viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.LEFT);

        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new CreateAccountChildFragment(R.layout.create_account_step1,0),getString(R.string.step1));
        adapter.addFragment(new CreateAccountChildFragment(R.layout.create_account_step2,1),getString(R.string.step2));

        mBinding.viewPager.setAdapter(adapter);
        mBinding.viewPager.setOffscreenPageLimit(1);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);

        mBinding.viewPager.setAdapter(adapter);
        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mBinding.tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
        LinearLayout tabStrip = ((LinearLayout) mBinding.tabLayout.getChildAt(0));

        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            int finalI = i;
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> finalI > tabPosition);
        }
        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.create_account_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateAccountChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
