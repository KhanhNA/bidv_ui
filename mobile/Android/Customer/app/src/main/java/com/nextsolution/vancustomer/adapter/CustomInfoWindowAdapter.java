package com.nextsolution.vancustomer.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.nfc.Tag;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.nextsolution.db.dto.Depot;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.util.MyDateUtils;

import javax.annotation.Resource;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private TextView txtName;
    private TextView txtPhone;
    private TextView txtAddress;
    private TextView txtTime;
    private TextView txtStatus;
    private Context context;
    private TextView rout;
    private int id;
    ListenIndex onClickListener;

    public CustomInfoWindowAdapter(Context context, ListenIndex onClickListener) {
        this.context = context;
        this.onClickListener =onClickListener;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @SuppressLint("SetTextI18n")
    private void render(Marker marker) {
        if(marker.getTag() instanceof RoutingDay){
            RoutingDay routingPlan = (RoutingDay) marker.getTag();

            if (routingPlan != null) {
                if(routingPlan.getStatus().equals("-1")){
                    Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_call_gray16);
                    txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);
                    txtAddress.setText(routingPlan.getDriver().getPhone());
                    txtAddress.setTextColor(context.getResources().getColor(R.color.action_text_color));

                    Drawable iconPhone = context.getResources().getDrawable(R.drawable.ic_driver);
                    txtPhone.setCompoundDrawablesWithIntrinsicBounds(iconPhone, null, null, null);
                    txtPhone.setText(routingPlan.getDriver().getName());
                    txtPhone.setTextColor(context.getResources().getColor(R.color.color_normal_text));


                    Drawable iconTime = context.getResources().getDrawable(R.drawable.ic_driver_code);
                    txtTime.setCompoundDrawablesWithIntrinsicBounds(iconTime, null, null, null);
                    txtTime.setText(routingPlan.getDriver().getDriver_code());

                    Drawable iconVan = context.getResources().getDrawable(R.drawable.ic_vans_green);
                    txtName.setCompoundDrawablesWithIntrinsicBounds(iconVan, null, null, null);
                    txtName.setText(routingPlan.getName()+" - "+routingPlan.getLicense_plate());

                }else{
                    txtPhone.setText(routingPlan.getPhone());
                    txtAddress.setText(routingPlan.getAddress());
                    txtName.setText(routingPlan.getWarehouse_name());
                    txtTime.setText(MyDateUtils.convertDateToString(routingPlan.getExpected_from_time(),MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss) + " - " + MyDateUtils.convertDateToString(routingPlan.getExpected_to_time(),MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss));
                    Resources resource = txtStatus.getResources();
                    switch (routingPlan.getStatus()){
                        case "0":
                            txtStatus.setTextColor(resource.getColor(R.color.primary_text));
                            txtStatus.setText(" - "+resource.getString(R.string.Shipping));
                            break;
                        case "1":
                            txtStatus.setTextColor(context.getResources().getColor(R.color.color_pending));
                            txtStatus.setText(" - "+resource.getString(R.string.Driver_confirmed));
                            break;
                        case "2":
                            txtStatus.setTextColor(context.getResources().getColor(R.color.primaryColor));
                            txtStatus.setText(" - "+resource.getString(R.string.Complete));
                            break;
                        case "3":
                            txtStatus.setTextColor(context.getResources().getColor(R.color.primaryColor));
                            txtStatus.setText(" - "+resource.getString(R.string.Deleted));
                            break;
                    }
                }
            }
        }else if(marker.getTag() instanceof Vehicle){
            Vehicle vehicle = (Vehicle) marker.getTag();

            if (vehicle != null && vehicle.getDriver()!=null) {
                    Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_call_gray16);
                    txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);
                    txtAddress.setText(vehicle.getDriver().getPhone());
                    txtAddress.setTextColor(context.getResources().getColor(R.color.action_text_color));

                    Drawable iconPhone = context.getResources().getDrawable(R.drawable.ic_driver);
                    txtPhone.setCompoundDrawablesWithIntrinsicBounds(iconPhone, null, null, null);
                    txtPhone.setText(vehicle.getDriver().getName());
                    txtPhone.setTextColor(context.getResources().getColor(R.color.color_normal_text));


                    Drawable iconTime = context.getResources().getDrawable(R.drawable.ic_driver_code);
                    txtTime.setCompoundDrawablesWithIntrinsicBounds(iconTime, null, null, null);
                    txtTime.setText(vehicle.getLicense_plate());

                    Drawable iconVan = context.getResources().getDrawable(R.drawable.ic_vans_green);
                    txtName.setCompoundDrawablesWithIntrinsicBounds(iconVan, null, null, null);
                    txtName.setText(vehicle.getName());
                    this.id=vehicle.getId();
                    onClickListener.onClickIndex(vehicle.getId());
            }

        }else if(marker.getTag() instanceof Depot){
            Depot depot= (Depot) marker.getTag();
            txtPhone.setText(depot.getPhone());
            txtAddress.setText(depot.getAddress());
            txtName.setText(depot.getName());
//            txtTime.setText(MyDateUtils.convertDateToString(depot.,MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss) + " - " + MyDateUtils.convertDateToString(routingPlan.getExpected_to_time(),MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss));

            switch (depot.getType()){
                case 0:
                    txtStatus.setTextColor(context.getResources().getColor(R.color.primary_text));
//                    txtStatus.setText(" - "+context.getString(R.string.WAREHOUSE_IMPORT));
                    break;

                case 1:
                    txtStatus.setTextColor(context.getResources().getColor(R.color.primaryColor));
//                    txtStatus.setText(" - "+context.getString(R.string.WAREHOUSE_EXPORT));
                    break;
            }
        }

    }

    private void initMarkerWarehouse(View view, Marker marker) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        txtAddress = view.findViewById(R.id.txtAddress);
        txtPhone = view.findViewById(R.id.txtPhoneNumber);
        txtName = view.findViewById(R.id.txtName);
        txtTime = view.findViewById(R.id.txtExpectedTime);
        txtStatus = view.findViewById(R.id.txtStatus);

        txtName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_location);
        txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);

        Drawable iconPhone = context.getResources().getDrawable(R.drawable.icon_call_green);
        txtPhone.setCompoundDrawablesWithIntrinsicBounds(iconPhone, null, null, null);

        Drawable iconTime = context.getResources().getDrawable(R.drawable.ic_time);
        txtTime.setCompoundDrawablesWithIntrinsicBounds(iconTime, null, null, null);

        render(marker);

    }

    private void initMarkerVans(View view) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        txtAddress = view.findViewById(R.id.txtAddress);
        txtName = view.findViewById(R.id.txtName);

        Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_location);
        txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);

        Drawable iconVan = context.getResources().getDrawable(R.drawable.ic_vans_green);
        txtName.setCompoundDrawablesWithIntrinsicBounds(iconVan, null, null, null);

//        txtAddress.setText(StaticData.getDriver().getVehicle().getParking_point().getAddress());
//        txtName.setText(StaticData.getDriver().getVehicle().getName());


    }

    @Override
    public View getInfoContents(final Marker marker) {
        View view;
        if(marker.getTag() instanceof RoutingDay || marker.getTag() instanceof Depot || marker.getTag() instanceof Vehicle ) {
            view = ((Activity) context).getLayoutInflater()
                    .inflate(R.layout.map_window_properties, null);
            initMarkerWarehouse(view, marker);
        }else {
            view = ((Activity) context).getLayoutInflater()
                    .inflate(R.layout.map_window_vans, null);
            initMarkerVans(view);
        }
        return view;
    }

}