package com.nextsolution.db.dto;

import android.content.Intent;

import com.nextsolution.vancustomer.model.Vehicle;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceArisingDTO extends BaseModel {
    Integer id;
    Integer driver_id;
    OdooDateTime create_date;
    Integer create_uid;
    Integer vehicle_id;
    Integer routing_plan_day_id;
    String note;
    Double latitude;
    Double longitude;
    WarningType warning_type;
    Vehicle vehicle;
    Driver driver;
    List<String> attach_image;
}
