package com.nextsolution.vancustomer.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.NotificationSystemActivity;
import com.nextsolution.vancustomer.databinding.NotificationChildFragmentBinding;
import com.nextsolution.vancustomer.viewmodel.NotificationVM;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.nextsolution.vancustomer.enums.Constants.*;

public class NotificationChildFragment extends BaseFragment {
    int layoutItem;
    String type;
    NotificationChildFragmentBinding mBinding;
    NotificationVM notificationVM;
    String plan_routing_day_code;
    NotificationDTO notificationDTO = new NotificationDTO();
    BaseAdapterV3 notificationAdapter;
    Integer positionNotification = 0;

    public NotificationChildFragment(@LayoutRes int layoutItem, String type) {
        this.layoutItem = layoutItem;
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (NotificationChildFragmentBinding) binding;
        notificationVM = (NotificationVM) viewModel;
        initView();
        notificationVM.getNotification(type, false, this::runUi);
        initLoadMore();
        return v;
    }

    public void pushNotification(NotificationDTO notificationModel) {
        notificationVM.getListNotification().add(0, notificationModel);
        notificationAdapter.notifyItemInserted(0);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case GET_DATA_SUCCESS:
                notificationVM.getIsLoading().set(false);
                notificationAdapter.getLoadMoreModule().loadMoreComplete();
                notificationAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                notificationVM.getIsLoading().set(false);
                notificationAdapter.getLoadMoreModule().loadMoreEnd();
                notificationAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (!notificationDTO.getIs_read()) {
                notificationVM.seenNotification(notificationDTO.getId());
            }
            notificationVM.getListNotification().get(positionNotification).setIs_read(true);
            notificationAdapter.notifyItemChanged(positionNotification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        notificationAdapter = new BaseAdapterV3(this.layoutItem, notificationVM.getListNotification(), this);
        mBinding.rcNotification.setAdapter(notificationAdapter);
        mBinding.swRefresh.setOnRefreshListener(() -> {
            onRefresh();
        });
    }

    public void onRefresh() {
        notificationVM.setOffset(0);
        notificationVM.getIsLoading().set(true);
        notificationVM.getNotification(type, false, this::runUi);
    }

    private void initLoadMore() {
        notificationAdapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            notificationVM.loadMore(type, this::runUi);
        });
        notificationAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        notificationAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    @Override
    public void onItemClick(View v, Object o) {
        notificationDTO = (NotificationDTO) o;
        plan_routing_day_code = notificationDTO.getItem_id();
        positionNotification = ((NotificationDTO) o).index - 1;
        // chuyển đến detail
        if (v.getId() == R.id.itemNotification) {
            //Start activity depending on notificationDTO.getClick_action()
            gotoDetailActivity(notificationDTO);
        } else if (v.getId() == R.id.notificationTypeSystem) {
            gotoNotificationSystemActivity();
        }
    }

    public void gotoNotificationSystemActivity() {
        Intent intent = new Intent(getBaseActivity(), NotificationSystemActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ID, notificationDTO.getNotification_id());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void gotoDetailActivity(NotificationDTO notificationDTO) {
        Intent intent = new Intent(notificationDTO.getClick_action());
        Bundle bundle = new Bundle();
        bundle.putString(ITEM_ID, notificationDTO.getItem_id());
        bundle.putString(OBJECT_STATUS, notificationDTO.getObject_status());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
