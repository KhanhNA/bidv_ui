package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.DriverApi;
import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.StaticData;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoCustomerVM extends BaseViewModel<Partner> {
    ObservableField<Partner> partner= new ObservableField<>();

    public InfoCustomerVM(@NonNull Application application) {
        super(application);
        partner.set(StaticData.getPartner());
    }

    public void deleteTokenFireBase() {
        DriverApi.deleteToken();
    }
}
