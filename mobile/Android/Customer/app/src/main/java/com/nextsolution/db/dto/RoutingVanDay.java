package com.nextsolution.db.dto;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;


import java.net.URL;

@lombok.Getter
@lombok.Setter
public class RoutingVanDay implements ShipmentStandStill {
  private String __typename;
  private Integer id;
  private java.util.Date date_plan;
  private Integer routing_plan_day_type;
  private Integer routing_van_id;
  private Integer order_number;
  private Integer warehouse_id;
  private Integer customer_id;
  private Integer van_id;
  private Integer depot_id;
  private Integer capacity_expected;
  private Double latitude;
  private Double longitude;
  private java.util.Date ready_time;
  private java.util.Date due_time;
  private Integer warehouse_type;
  private java.util.Date routing_time;
  private Integer status;
  private java.util.Date create_date;
  private String create_user;
  private java.util.Date update_date;
  private String update_user;
  private java.util.Date expected_from_time;
  private java.util.Date expected_to_time;
  private java.util.Date actual_time;
  private String routing_plan_code;
  private String group_code;
  private String previous_id;
  private String next_id;
  private Customer customer;
  private Warehouse warehouse;
  private Van van;
  RoutingVanDay next = null;
  ShipmentStandStill previous = null;

  @Override
  public LatLng getLatLng() {
    return null;
  }

  @Override
  public String getAvatarStr() {
    if(customer.getAvatar_url() != null){
      return customer.getAvatar_url();
    }
    if(van.getAvatar_url() != null) {
      return van.getAvatar_url();
    }
    return null;
  }

  @Override
  public URL getAvatarUrl() throws Exception {
    return null;
  }

  @Override
  public void setMarker(Marker marker1) {

  }

  @Override
  public Marker getMarker() {
    return null;
  }

  @Override
  public String getServiceTime() {
    return null;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }

  @Override
  public Double getCapacity() {
    return null;
  }

  @Override
  public String getArriveTime() {
    return null;
  }
}

