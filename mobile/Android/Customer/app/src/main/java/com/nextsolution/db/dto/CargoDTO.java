package com.nextsolution.db.dto;

import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoDTO extends BaseModel {
    private int id;
    private String cargo_number;
    private int from_depot_id;
    private int to_depot_id;
    private int distance;
    private int size_id;
    private String create_date;
    private String write_date;
    private int weight;
    private String description;
    private Double price;
    private Double from_latitude;
    private Double to_latitude;
    private Integer bidding_package_id;
    private Double from_longitude;
    private Double to_longitude;
    private OdooDate pack_plant_day;
    private String status;
    private SizeStandard size_standard;
}
