package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.api.BaseApi;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.vm.InfoCustomerVM;
import com.nextsolution.vancustomer.databinding.InfoCustomerFragmentBinding;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.ui.LoginActivityV2;
import com.nextsolution.vancustomer.ui.MainActivity;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoCustomerFragment extends BaseFragment {
    private final MainActivity mActivity;
    private InfoCustomerVM infoCustomerVM;
    private InfoCustomerFragmentBinding mBinding;
    ArrayList<String> uriString=new ArrayList<>();
    LocalMedia url_avatar=new LocalMedia();

    public InfoCustomerFragment(MainActivity mActivity){
        this.mActivity = mActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        infoCustomerVM = (InfoCustomerVM) viewModel;
        mBinding = (InfoCustomerFragmentBinding) binding;
        return view;
    }
    public void zoomAvatar(){
        if(infoCustomerVM.getPartner().get().getUri_path()!=null){
            List<LocalMedia> localMedia= new ArrayList<>();
            url_avatar.setPath(infoCustomerVM.getPartner().get().getUri_path());
            localMedia.add(url_avatar);
            try {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_WeChat_style)
                        .isWeChatStyle(true)
                        .loadImageEngine(GlideEngine.createGlideEngine())
                        .openExternalPreview(0, localMedia);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openProfileFragment(){
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, EditInfoCustomerFragment.class);
        startActivityForResult(intent,Constants.REQUEST_FRAGMENT_RESULT);
    }

    public void showDialogLogout(){
        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.confirm), getString(R.string.message_logout),
                v -> {
                    // Delete token firebase
                    infoCustomerVM.deleteTokenFireBase();
                    logout();
                });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    public void changePassword(){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        changePasswordDialog.setTargetFragment(this, Constants.CHANGE_PASSWORD);
        changePasswordDialog.show(fm, "ABC");
    }

    public void openListOrder(){
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, BillLadingHistoryFragment.class);
        startActivity(intent);
    }
    public void openRankingFragment(){
        Intent intent = new Intent(getContext(), CommonActivity.class);
        intent.putExtra(Constants.FRAGMENT, MyRankingFragment.class);
        startActivity(intent);
    }

    public void navigateToLisCargo(){
        mActivity.navigateToPosition(1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == Constants.CHANGE_PASSWORD && resultCode == Constants.RESULT_OK){
            ToastUtils.showToast(getActivity(),getResources().getString(R.string.CHANGE_PASSWORD_SUCCESS), getResources().getDrawable(R.drawable.ic_check));
            new Handler().postDelayed(() -> {
                infoCustomerVM.deleteTokenFireBase();
                logout();
            },1000);
        }else if(requestCode == Constants.REQUEST_FRAGMENT_RESULT && resultCode == Constants.RESULT_OK){
            infoCustomerVM.getPartner().get().setEmail(StaticData.getPartner().getEmail());
            infoCustomerVM.getPartner().get().setUri_path(StaticData.getPartner().getUri_path());
            infoCustomerVM.getPartner().notifyChange();
        }
    }
    public void logout(){
        BaseApi.logout();
        // Delete user & pass
        AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
        // Delete cache
        StaticData.setOdooSessionDto(null);
        StaticData.setPartner(null);
        StaticData.sessionCookie = "";

        // Intent loginActivity
        Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }
    @Override
    public int getLayoutRes() {
        return R.layout.info_customer_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return InfoCustomerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}
