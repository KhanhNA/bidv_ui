package com.nextsolution.vancustomer.confirm_change;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.ReceiptWarehouseAdapter;
import com.nextsolution.vancustomer.databinding.DetailImportsFragmentBinding;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.DialogWithEditText;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class DetailImportsFragment extends BaseFragment {
    ConfirmChangeVM confirmChangeVM;
    DetailImportsFragmentBinding mBinding;
    EditBillPackageDialog editDialog;
    ReceiptWarehouseAdapter adapter;
    private boolean isLoaded;
    private boolean isPriceCal;//đã tính tiền chưa
    DialogConfirm confirm;
    DialogWithEditText confirmEditText;
    ICallBackConfirmEditRoutingDay iCallBackConfirmEditRoutingDay;

    public DetailImportsFragment(ICallBackConfirmEditRoutingDay iCallBackConfirmEditRoutingDay) {
        this.iCallBackConfirmEditRoutingDay = iCallBackConfirmEditRoutingDay;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isPriceCal) {
            isValidBill(confirmChangeVM.mapPickup, true);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (DetailImportsFragmentBinding) binding;
        confirmChangeVM = ViewModelProviders.of(getBaseActivity()).get(ConfirmChangeVM.class);
        binding.setVariable(BR.viewModel, confirmChangeVM);

        initView();

        return v;
    }

    private void initView() {
        adapter = new ReceiptWarehouseAdapter(R.layout.item_receipt_warehouse, confirmChangeVM.getModelE().getArrBillLadingDetail(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                openEditBillPackage((BillLadingDetail) o);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        }) {
            @Override
            public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
                super.onBindViewHolder(viewHolder, position);
                viewHolder.bindWithVM(confirmChangeVM);
            }
        };
        mBinding.rcBillDetail.setAdapter(adapter);

        mBinding.btnConfirm.setOnClickListener((v) -> callOrder());
        mBinding.btnCancel.setOnClickListener((v) -> cancelOrder());


    }

    private void cancelOrder() {
        confirmEditText = new DialogWithEditText(getString(R.string.confirm_cancel),
                getString(R.string.msg_cancel_update_order), getString(R.string.reason_cancel_order), editText -> {
            confirmEditText.dismiss();
            confirmChangeVM.callCancelBill(editText.getText().toString(), this::runUi);
        });
        confirmEditText.show(getChildFragmentManager(), confirmEditText.getTag());
    }

    private void callOrder() {
        if (isValidBill(confirmChangeVM.mapPickup, false)) {
            confirm = new DialogConfirm(getString(R.string.confirm_change),
                    getString(R.string.msg_confirm_eidt_bill_lading), v -> {
                confirm.dismiss();
                confirmChangeVM.callUpdateBilLading(this::runUi);
            });
            confirm.show(getChildFragmentManager(), confirm.getTag());
        } else {
            Toast.makeText(getContext(), R.string.please_split_all_package, Toast.LENGTH_SHORT).show();
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "updateOrderSuccess":
                ToastUtils.showToast(getActivity(), getString(R.string.update_order_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
                iCallBackConfirmEditRoutingDay.callBack();
                getActivity().finish();
                break;
            case "updateOrderFail":
                ToastUtils.showToast(getActivity(), getString(R.string.update_order_fail), getResources().getDrawable(R.drawable.ic_danger));
                break;
        }
    }


    private void openEditBillPackage(BillLadingDetail o) {

//        //check số lượng còn lại và số lượng đã chia
//        List<BillLadingDetail> billLadingDetails = confirmChangeVM.getModelE().getArrBillLadingDetail();
//        for (int i = 1; i < billLadingDetails.size(); i++) {
//            BillLadingDetail billLadingDetail = billLadingDetails.get(i);
//            for (BillPackage billPackage : billLadingDetail.getBillPackages()) {
//                BillPackage pickup = confirmChangeVM.mapPickup.get(billPackage.getFrom_bill_package_id());
//                if (pickup == null) continue;
//                pickup.setRest(pickup.getRest() - billPackage.getQuantity_package());
//                pickup.setSplitQuantity(pickup.getQuantity_package() + billPackage.getQuantity_package());
//            }
//        }

        editDialog =
                new EditBillPackageDialog(confirmChangeVM, confirmChangeVM.mapPickup, o, bill -> {
                    confirmChangeVM.getModelE().getArrBillLadingDetail().set(o.index - 1, bill);
                    confirmChangeVM.getModel().notifyChange();
                    isValidBill(confirmChangeVM.mapPickup, true);
                    adapter.notifyDataSetChanged();
                });
        editDialog.show(requireFragmentManager(), editDialog.getTag());
    }

    private boolean isValidBill(HashMap<Long, BillPackage> mapPickup, boolean callPrice) {
        boolean isValid = true;
        //FOR LOOP
        for (Map.Entry<Long, BillPackage> me : mapPickup.entrySet()) {
            if (me.getValue().getRest() != 0) {
                isValid = false;
            }
        }
        if (isValid && callPrice) {
            confirmChangeVM.priceCalculate();
            isPriceCal = true;
        }
        confirmChangeVM.isValid.set(isValid);
        return isValid;
    }

    public void notifyDataSetChanged() {
        adapter.update(confirmChangeVM.getModelE().getArrBillLadingDetail());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.detail_imports_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmChangeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public interface ICallBackConfirmEditRoutingDay {
        void callBack();
    }
}
