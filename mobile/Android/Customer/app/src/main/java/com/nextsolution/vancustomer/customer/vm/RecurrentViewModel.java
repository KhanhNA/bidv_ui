package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.SubscribeApi;
import com.nextsolution.db.dto.Subscribe;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.RecurrentModel;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecurrentViewModel extends BaseViewModel {
    private List<Subscribe> listSubs;
    private ObservableField<RecurrentModel> recurrentModel = new ObservableField<>();

    public RecurrentViewModel(@NonNull Application application) {
        super(application);
        listSubs = new ArrayList<>();
        recurrentModel.set(new RecurrentModel());
    }


    public void getSubs(RunUi runUi){
        SubscribeApi.getSubscribe(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                listSubs.clear();
                listSubs.addAll(((OdooResultDto<Subscribe>)o).getRecords());
                runUi.run("getSubs");
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }


}
