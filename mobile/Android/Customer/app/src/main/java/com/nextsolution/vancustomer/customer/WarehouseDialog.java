package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.WareHouseVM;
import com.nextsolution.vancustomer.databinding.DialogWarehouseBinding;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.ui.WarehouseInfoActivity;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.listener.AdapterListener;

import java.util.HashMap;
import java.util.List;


public class WarehouseDialog extends BottomSheetDialogFragment {
    private static final int REQUEST_ADD_WAREHOUSE_CODE = 42541;
    private DialogWarehouseBinding binding;
    private OnConfirmWarehouse onConfirm;
    private WareHouseVM wareHouseVM;
    private Handler handler;
    private XBaseAdapter wareHouseAdapter;
    private Warehouse selectedWareHouse;
    private HashMap<Integer, Warehouse> selectedWareHouses;

    WarehouseDialog(Warehouse selectedWareHouse, HashMap<Integer, Warehouse> selectedWareHouses, OnConfirmWarehouse onConfirm) {
        this.onConfirm = onConfirm;
        this.selectedWareHouse = selectedWareHouse;
        this.selectedWareHouses = selectedWareHouses;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_warehouse, container, false);
        wareHouseVM = ViewModelProviders.of(this).get(WareHouseVM.class);
        binding.setViewModel(wareHouseVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        wareHouseAdapter = new XBaseAdapter(R.layout.item_warehouse, wareHouseVM.listWarehouses, new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                Warehouse warehouse = (Warehouse) o;
                if (view.getId() == R.id.ic_menu) {
                    if (selectedWareHouses.get(warehouse.getId()) != null) {
                        Toast.makeText(getContext(), R.string.Do_not_edit_selected_warehouse_information, Toast.LENGTH_SHORT).show();
                    }else{
                        openEditWarehouse(warehouse);
                    }
                } else {
                    if (warehouse.getId() != null) {
                        if (selectedWareHouses.get(warehouse.getId()) != null) {
                            Toast.makeText(getContext(), R.string.you_chose_this_warehosue_before, Toast.LENGTH_SHORT).show();
                        } else {
                            if (selectedWareHouse != null && selectedWareHouse.getId() != null) {
                                if (selectedWareHouses.get(selectedWareHouse.getId()) != null) {
                                    selectedWareHouses.remove(selectedWareHouse.getId());
                                }
                            }
                            selectedWareHouses.put(warehouse.getId(), warehouse);
                            onConfirm.getSelectedWarehouse((Warehouse) o);
                            dismiss();
                        }
                    }
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });

        binding.rcWareHouse.setAdapter(wareHouseAdapter);
        handler = new Handler();
        binding.txtEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(() -> wareHouseVM.getPlaces(s.toString(), wareHouseVM.listWarehouses, WarehouseDialog.this::afterFetchDetail), 350);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        binding.root.setMinimumHeight(height);
        if (StaticData.getPartner().getCompany_id() == null) {
            ToastUtils.showToast(getActivity(), getString(R.string.msg_can_not_find_company));
        } else {
            wareHouseVM.getWareHouse(StaticData.getPartner().getCompany_id().getId(), WarehouseDialog.this::afterFetchDetail);
        }

        binding.btnInsert.setOnClickListener(v -> openWareHouseInfoActivity());

        return binding.getRoot();
    }

    private void openEditWarehouse(Warehouse o) {
        Intent intent = new Intent(getActivity(), WarehouseInfoActivity.class);
        intent.putExtra("WAREHOUSE", o);
        intent.putExtra("POSITION", o.index - 1);
        if (selectedWareHouse != null) {
            intent.putExtra("DELETE_ABLE", !o.getId().equals(selectedWareHouse.getId()));
        }
        startActivityForResult(intent, REQUEST_ADD_WAREHOUSE_CODE);
    }

    private void openWareHouseInfoActivity() {
        Intent intent = new Intent(getActivity(), WarehouseInfoActivity.class);
        startActivityForResult(intent, REQUEST_ADD_WAREHOUSE_CODE);
    }

    private void afterFetchDetail(Object... objects) {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(() -> {
            try {
                if (objects.length > 0) {
                    wareHouseAdapter.notifyDataSetChanged((List<Warehouse>) objects[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_WAREHOUSE_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                int action = data.getIntExtra("ACTION", -1);
                switch (action) {
                    case 1://thêm
                        Warehouse warehouse = (Warehouse) data.getSerializableExtra("WAREHOUSE");
                        if (warehouse != null) {
                            warehouse.setArea_id(warehouse.getAreaInfo().getId());
                            wareHouseVM.listWarehouses.add(0, warehouse);
                            wareHouseAdapter.notifyItemInserted(0);
                            binding.rcWareHouse.smoothScrollToPosition(0);
                        }
                        break;
                    case 2://sửa
                        Warehouse warehouseUpdate = (Warehouse) data.getSerializableExtra("WAREHOUSE");
                        int position = data.getIntExtra("POSITION", -1);
                        if (warehouseUpdate != null && position != -1) {
                            wareHouseVM.listWarehouses.set(position, warehouseUpdate);
                            wareHouseAdapter.notifyItemChanged(position);
                            binding.rcWareHouse.smoothScrollToPosition(position);
                        }
                        break;
                    case 3://xóa
                        int positionDelete = data.getIntExtra("POSITION", -1);
                        if (positionDelete > -1) {
                            wareHouseVM.listWarehouses.remove(positionDelete);
                            wareHouseAdapter.notifyItemRemoved(positionDelete);
                        }
                        break;

                }
            }

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    public interface OnConfirmWarehouse {
        void getSelectedWarehouse(Warehouse selected);
    }

}
