package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.github.gongw.VerifyCodeView;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.ConfirmOTPDialogVM;
import com.nextsolution.vancustomer.databinding.ConfirmOtpDialogBinding;

public class ConfirmOTPDialog extends DialogFragment {

    View.OnClickListener onClickListener;
    ConfirmOtpDialogBinding mBinding;
    ConfirmOTPDialogVM confirmOTPDialogVM;
    String phone;
    public ConfirmOTPDialog(String phone,View.OnClickListener onClickListener) {
        this.phone=phone;
        this.onClickListener=onClickListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.confirm_otp_dialog, container, false);
        confirmOTPDialogVM = ViewModelProviders.of(this).get(ConfirmOTPDialogVM.class);
        mBinding.setViewModel(confirmOTPDialogVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        mBinding.btnConfirm.setOnClickListener(onClickListener);
        VerifyCodeView.OnAllFilledListener listener = new VerifyCodeView.OnAllFilledListener() {
            @Override
            public void onAllFilled(String text) {
                mBinding.btnConfirm.setEnabled(true);
            }
        };
        mBinding.otpView.setOnAllFilledListener(listener);
        mBinding.txtTitle.setText(getString(R.string.verification_code)+this.phone);
        return mBinding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.BaseStyleDialog);
    }


}
