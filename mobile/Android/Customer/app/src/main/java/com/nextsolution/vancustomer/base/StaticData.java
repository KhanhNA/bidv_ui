package com.nextsolution.vancustomer.base;


import com.nextsolution.db.dto.Partner;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.StaffVan;
import com.nextsolution.db.dto.Van;
import com.ns.odoolib_retrofit.model.OdooSessionDto;

import java.util.ArrayList;
import java.util.List;

public final class StaticData {
    private static StaffVan staffVan;
    public static String sessionCookie;

    public static String userName;
    public static String password;

    private static OdooSessionDto odooSessionDto;

    private static Partner partner;

    private static RoutingVanDay currentRouteProcessed = null;
    private static List<RoutingVanDay> routingVanDays = new ArrayList<>();
    private static List<RoutingVanDay> routingVanDayAdded = new ArrayList<>();
    public static final String FRAGMENT = "FRAGMENT";
    public static final String ROUTE_DETAIL = "ROUTE_DETAIL";
    public static final String ORDER_VM = "ORDER_VM";
    public static final String RESULT_SCAN = "RESULT_SCAN";
    public static final String PREFIX_VEHICLE_ID="D";
    public static final String PREFIX_WAREHOUSE_ID="W";
    public static final Integer ROUTE_STATUS_NOT_PROCESS=0;


    public static void clearRoutes() {
        routingVanDays = reset(routingVanDays);
    }

    public static void clearRoutingVanDayAdded() {
        routingVanDayAdded = reset(routingVanDayAdded);
    }

    public static void setRoutes(List<RoutingVanDay> routings) {


        int count = 0;
        String nextId, previousId;
        RoutingVanDay firstRoute = null;
        for(RoutingVanDay aRoute: routings){
            count = 0;
            nextId = aRoute.getNext_id();
            previousId = aRoute.getPrevious_id();

            for(RoutingVanDay aRoute1: routings){

                if(nextId != null && nextId.equals(PREFIX_WAREHOUSE_ID + aRoute1.getId())){
                    aRoute.setNext(aRoute1);
                    count ++;

                }
                if(previousId != null && previousId.equals(PREFIX_WAREHOUSE_ID + aRoute1.getId())){
                    aRoute.setPrevious(aRoute1);
                    count ++;
                }
                if(count >= 2){
                    break;
                }
            }

        }
        routingVanDays.clear();
        currentRouteProcessed = null;
        RoutingVanDay tmp = firstRoute;
        while (tmp != null){
            routingVanDays.add(tmp);

            if(tmp.getStatus() == ROUTE_STATUS_NOT_PROCESS && currentRouteProcessed == null) {
                currentRouteProcessed = tmp;
            }
            tmp = tmp.getNext();
        }

    }


    public static void setRoutesOfCustomer(List<RoutingVanDay> routings) {
        routingVanDays.clear();
        currentRouteProcessed = null;

        for(RoutingVanDay tmp: routings){
            routingVanDays.add(tmp);
            if(tmp.getStatus() == ROUTE_STATUS_NOT_PROCESS && currentRouteProcessed == null) {
                currentRouteProcessed = tmp;
            }
        }

    }


    public static void setRoutesAdded(List<RoutingVanDay> routings) {
        routingVanDayAdded.clear();

        routingVanDayAdded.addAll(routings);
    }

    public static void addNewRoute(RoutingVanDay aRoute) {
        routingVanDayAdded.add(aRoute);
    }

    public static List<RoutingVanDay> getRoutingVanDays() {
        List<RoutingVanDay> lst = new ArrayList<>();
        if(routingVanDays == null || routingVanDays.size() < 2){
            return lst;
        }
        lst.add(routingVanDays.get(0));
        lst.add(routingVanDays.get(1));
//        return routingVanDays;
        return lst;
    }

    public static List<RoutingVanDay> getRoutingVanDayAdded() {
        return routingVanDayAdded;
    }

    public static StaffVan getStaffVan() {
        return staffVan;
    }

    public static void setStaffVan(StaffVan staffVan) {
        StaticData.staffVan = staffVan;
    }



    public static String getUserLogin() {
        if(odooSessionDto == null){
            return "";
        }
        return odooSessionDto.getUsername();
    }
    public static Integer getVanId() {

        return StaticData.staffVan == null ? null : StaticData.staffVan.getVan().getId();
    }
    public static Van getVan() {

        return StaticData.staffVan == null ? null : (StaticData.staffVan.getVan());
    }



    public static RoutingVanDay getCurrentRouteProcessed() {
        return currentRouteProcessed;
    }

    public static void setCurrentRouteProcessed(RoutingVanDay currentRouteProcessed) {
        StaticData.currentRouteProcessed = currentRouteProcessed;
    }



    private static List reset(List list) {
        if (list == null) {
            list = new ArrayList<>();
        } else {
            list.clear();
        }
        return list;
    }

    public static OdooSessionDto getOdooSessionDto() {
        return odooSessionDto;
    }

    public static void setOdooSessionDto(OdooSessionDto odooSessionDto) {
        StaticData.odooSessionDto = odooSessionDto;
    }

    public static Partner getPartner() {
        return partner;
    }

    public static void setPartner(Partner partner) {
        StaticData.partner = partner;
    }
}
