package com.nextsolution.db.dto;

import android.annotation.SuppressLint;

import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.StatusType;
import com.nextsolution.vancustomer.util.StringUtils;
import com.tsolution.base.BaseModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillPackage extends BaseModel implements Cloneable {
    private Integer quantityQrChecked = 0;
    private boolean qrChecked = false;
    private Long id;
    private String key_map;//map được bill package giữa kho xuất hàng và kho nhận hàng
    private String warehouse_name;
    private String address;
    private String phone;

    private Long from_bill_package_id;
    private Long from_change_bill_package_id;
    private Integer bill_package_id;

    private Integer bill_lading_detail_id;
    private String item_name;
    private Double net_weight;// weight sau khi tính toán
    private transient Double weight;//weight do người dùng nhập vào

    private Integer quantity_package;
    private Double length;
    private Double width;
    private Double height;
    private Double totalVol;
    private Double capacity;
    private Double total_weight;
    private Integer quantity_import;
    private Integer quantity_export;

    private String product_type_name;
    private String status = StatusType.RUNNING;

    private ProductType productType;

    private String qr_char;

    private String insurance_name;
    private String subscribe_name;
    private BillPackage origin_bill_package;


    //properties for ui
    private transient int rest = 0; // số lượng còn lại
    private transient boolean isSelect;
    private transient int splitQuantity = 0;//số lượng đã tách


    public void setInfo(BillPackage billPackage) {
        this.item_name = billPackage.item_name;
        this.height = billPackage.height;
        this.width = billPackage.width;
        this.net_weight = billPackage.net_weight;
        this.length = billPackage.length;
        this.product_type_name = billPackage.product_type_name;
        this.productType = billPackage.productType;
    }


    public double getTotalVol() {
        return this.height * this.length * this.width * this.quantity_package;
    }

    /**
     * Tính toán trọng lượng ước tính theo tham số dài rộng cao
     *
     * @param longs  chiều dài. Đơn vị m
     * @param width  chiều rộng. Đơn vị m
     * @param height chiều cao. Đơn vị m
     * @return trọng lượng ước tính.
     */
    public double calculateNetWeight(Double longs, Double width, Double height) {
        if (longs != null && width != null && height != null) {
            longs *= 100;
            width *= 100;
            height *= 100;
//            từ đơn vị m * 100 -> đơn vị cm
            return (longs * width * height) / StaticData.getOdooSessionDto().getWeight_constant_order();
        }
        return 0;
    }

    @NotNull
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new BillPackage();
            // This should never happen
        }
    }

    public String getQuantityStr() {
        return this.quantity_package != null ? quantity_package + "" : "";
    }

    public void setQuantityStr(String quantityStr) {
        if (!StringUtils.isNullOrEmpty(quantityStr)) {
            quantityStr = quantityStr.replace(",", "");
            quantity_package = Integer.parseInt(quantityStr);
        } else {
            quantity_package = 0;
        }
    }


    //chuyển về đơn vị m
    public String getLengthStr() {
        return this.getLength() != null ?  Math.round(length*100)+"": "";
    }

    //chuyển về đơn vị m
    public void setLengthStr(String lengthStr) {
        if (!StringUtils.isNullOrEmpty(lengthStr)) {
            if (lengthStr.charAt(0) == '.') {
                lengthStr = lengthStr.replace(".", "0.");
            }
            lengthStr = lengthStr.replace(",", "");
            this.setLength(Double.parseDouble(lengthStr) / 100);
        } else {
            this.setLength(0d);
        }
    }

    //chuyển về đơn vị m
    public String getWidthStr() {
        return this.getWidth() != null ?  Math.round(width*100)+"" : "";
    }

    //chuyển về đơn vị m
    public void setWidthStr(String widthStr) {
        if (!StringUtils.isNullOrEmpty(widthStr)) {
            if (widthStr.charAt(0) == '.') {
                widthStr = widthStr.replace(".", "0.");
            }
            widthStr = widthStr.replace(",", "");
            this.setWidth(Double.parseDouble(widthStr) / 100);
        } else {
            this.setWidth(0d);
        }
    }

    //chuyển về đơn vị cm
    public String getHeightStr() {
        return this.getHeight() != null ?  Math.round(height*100)+"" : "";
    }

    //chuyển về đơn vị m
    public void setHeightStr(String heightStr) {
        if (!StringUtils.isNullOrEmpty(heightStr)) {
            if (heightStr.charAt(0) == '.') {
                heightStr = heightStr.replace(".", "0.");
            }
            heightStr = heightStr.replace(",", "");
            this.setHeight(Double.parseDouble(heightStr) / 100);
        } else {
            this.setHeight(0d);
        }
    }

    public String getWeightStr() {
        return this.weight != null ? AppController.getInstance().formatQuantity(weight) : "";
    }

    public String getTotal_weight_str() {
        return this.total_weight != null ? total_weight + "kg" : "";
    }

    public void setWeightStr(String weightStr) {
        if (!StringUtils.isNullOrEmpty(weightStr)) {
            if (weightStr.charAt(0) == '.') {
                weightStr = weightStr.replace(".", "0.");
            }
            weightStr = weightStr.replace(",", "");
            this.weight = (Double.parseDouble(weightStr));
        } else {
            this.weight = (0d);
        }
    }

    @SuppressLint("DefaultLocale")
    public String getNet_weight_str() {
        return this.net_weight != null ? AppController.getInstance().formatQuantity(net_weight) : "";
    }

    public boolean mEquals(BillPackage bill) {
        boolean flag = true;
        if (!this.length.equals(bill.length))
            flag = false;
        if (!this.width.equals(bill.width))
            flag = false;
        if (!this.height.equals(bill.height))
            flag = false;
        if (!this.getNet_weight().equals(bill.getNet_weight()))
            flag = false;
        if (!this.productType.getId().equals(bill.productType.getId()))
            flag = false;
        if (!this.item_name.equals(bill.item_name))
            flag = false;
        return flag;
    }
}
