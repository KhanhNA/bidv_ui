package com.nextsolution.vancustomer.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Pair;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.http.HttpHelper;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.NetworkUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.CommonActivity;
import com.workable.errorhandler.ErrorHandler;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;


public class AppController extends MyApplication {
    private static AppController mInstance;

    public static final String API_GOOGLE_MAP_SEARCH = "AIzaSyDLtkodzb8ivLLg50q19eObdg4zs9MT04I";
    public static final String API_GOOGLE_MAP_DESTINATION = "AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk";

    public static String SERVER_URL = "http://192.168.1.69";
    public static String DATABASE = "DLP";
    public static String TRACKING_SOCKET = "http://192.168.1.69:8082";
    public static String WS_SOCKET = "ws://192.168.1.69:8082";

    public static String BILLING_URL = "http://192.168.1.69:3004";
    public static String DISTANCE_URL = "http://192.168.1.69:3003";

    public static final String BASE_URL = SERVER_URL + ":8070";

    public static final String BASE_URL_IMAGE = BASE_URL + "/images/";


    public static final String HTTP = "http://";


    public static final String CLIENT_ID = "Dcommerce";
    public static final String CLIENT_SECRET = "1qazXSW@3edcVFR$5tgbNHY^7ujm<KI*9ol.?:P)";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String DATE_PATTERN = "dd/MM/yyyy";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String DATE_TIME_PATTERN = "HH:mm '-' dd/MM/yyyy";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CACHE_USER = "CACHE_USER";


    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDateTime = new SimpleDateFormat(AppController.DATE_TIME_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(AppController.DATE_PATTERN);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatTime = new SimpleDateFormat(AppController.TIME_PATTERN);

    private SharedPreferences sharedPreferences;
    private DecimalFormat formatterCurrency;
    private DecimalFormat formatterNumber;
    public static Long languageId = 1L;
    HashMap<String, Object> clientCache;
    public static String[] LANGUAGE_CODE = {"vi", "en", "my"};

    public static InputFilter[] getFilter() {
        InputFilter EMOJI_FILTER = (source, start, end, dest, dstart, dend) -> {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || type == Character.NON_SPACING_MARK
                        || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        };
        return new InputFilter[]{EMOJI_FILTER};
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        clientCache = new HashMap<>();
        ErrorHandler
                .defaultErrorHandler()
                // Always log to a crash/error reporting service
                .always((throwable, errorHandler) -> {
                    throwable.printStackTrace();
                    if (!NetworkUtils.isNetworkConnected(this)) {
                        EventBus.getDefault().post(Constants.DISCONNECTED_NETWORK);
                        ToastUtils.showToast(getResources().getString(R.string.network_error));
                    } else {
                        ToastUtils.showToast(getInstance().getResources().getString(R.string.An_error_occurred_while_retrieving_data));
                    }
                });

    }


    public Bundle getDataApp() {
        Bundle bundle = new Bundle();
        bundle.putString("userName", AppController.getInstance().getSharePre().getString(Constants.USER_NAME, ""));
        bundle.putString("token", HttpHelper.TOKEN_DCOM);
        return bundle;
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }


    public String formatCurrency(Double money) {
        if (money == null) {
            return "0 " + StaticData.getOdooSessionDto().getCurrency();
        }
        if (formatterCurrency == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterCurrency = new DecimalFormat("###,###,###", symbols);
        }

        return formatterCurrency.format(money) + " " + StaticData.getOdooSessionDto().getCurrency();
    }
    public String formatNumberV3(Double number) {
        if (number == null) {
            return "0";
        }
        if (formatterCurrency == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterCurrency = new DecimalFormat("######", symbols);
        }

        return formatterCurrency.format(number) + "";
    }

    public String formatCurrency(BigDecimal money) {
        if (money == null) {
            return "0 " + StaticData.getOdooSessionDto().getCurrency();
        }
        if (formatterNumber == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterNumber = new DecimalFormat("###,###,###", symbols);
        }

        return formatterNumber.format(money) + " " + StaticData.getOdooSessionDto().getCurrency();
    }

    public String formatNumber(Integer money) {
        if (money == null) {
            return "0";
        }
        if (formatterCurrency == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterCurrency = new DecimalFormat("###,###,###", symbols);
        }
        return formatterCurrency.format(money);
    }

    public String formatNumber(Double money) {
        if (money == null) {
            return "0";
        }
        if (formatterNumber == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterNumber = new DecimalFormat("###,###,###.##", symbols);
        }

        return formatterNumber.format(money);
    }
    public String formatQuantity(Double quantity) {
        if (quantity == null) {
            return "0";
        }
        if (formatterNumber == null) {
            Locale locale = new Locale("en", "UK");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            formatterNumber = new DecimalFormat("###,###,###.####", symbols);
        }

        return formatterNumber.format(quantity);
    }

    public String formatDistance(Double distance) {
        if (distance != null) {
            distance /= 1000;
            return formatNumber(distance);
        }
        return "0";
    }


    public void putCache(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }


    @SuppressLint("DefaultLocale")
    public static String formatNumberV2(double number) {
        if (number >= 1000000000) {
            return String.format("%.1fT", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.1fB", number / 1000000.0);
        }

        if (number >= 100000) {
            return String.format("%.1fM", number / 100000.0);
        }

        if (number >= 1000) {
            return String.format("%.1fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    public void logError(Throwable e) {
        ErrorHandler.create().handle(e);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
    }

    public void newActivity(Activity previousActivity, Class fragmentClazz, String extra, Serializable objExtra) {
        Intent intent = new Intent(previousActivity, CommonActivity.class);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (extra != null) {
            intent.putExtra(extra, objExtra);
        }
        previousActivity.startActivity(intent);
    }

    public void newActivity(Activity previousActivity, Class fragmentClazz, boolean result, int code, Pair<String, Serializable>... objs) {
        Intent intent = new Intent(previousActivity, CommonActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (objs != null) {
            for (Pair<String, Serializable> obj : objs) {
                intent.putExtra(obj.first, obj.second);
            }
        }
        if (result) {
            previousActivity.startActivityForResult(intent, code);
        } else {
            previousActivity.startActivity(intent);
        }
    }

    public void newActivity(BaseFragment fragment, Class fragmentClazz, boolean result, int code, Pair<String, Serializable>... objs) {
        Intent intent = new Intent(fragment.getBaseActivity(), CommonActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(StaticData.FRAGMENT, fragmentClazz);
        if (objs != null) {
            for (Pair<String, Serializable> obj : objs) {
                intent.putExtra(obj.first, obj.second);
            }
        }
        if (result) {
            fragment.startActivityForResult(intent, code);
        } else {
            fragment.startActivity(intent);
        }
    }

    public void openOtherApplication(String packageName, Activity context) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(launchIntent);
        } else {
            // TODO: 1/7/2020 show dialog download icon_chat app
            Toast.makeText(context, "You must download mingalaba icon_chat app", Toast.LENGTH_LONG).show();
        }
    }

    public BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void callAppChat() {
        try {
            Intent launchIntent = AppController.getInstance().getPackageManager().getLaunchIntentForPackage(Constants.chatAppId);
            if (launchIntent != null) {
                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = AppController.getInstance().getDataApp();
//                bundle.putString("INTENT_USER_NAME");
//                bundle.putString("AVATAR_URL");
//                bundle.putString("DISPLAY_NAME");
                launchIntent.putExtras(bundle);
                startActivity(launchIntent);
            } else {
                // TODO: 1/7/2020 show dialog download chat app
                ToastUtils.showToast("You must download mingalaba chat app");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public String getOdooUrl() {
//        return BASE_URL;
//    }
//
//    @Override
//    public void registerResponse(OdooV2 odooV2, VolleyError volleyError) {
//        System.out.println("error:" + volleyError);
//        BaseApi.setOdoo(getOdoo());
//    }
}
