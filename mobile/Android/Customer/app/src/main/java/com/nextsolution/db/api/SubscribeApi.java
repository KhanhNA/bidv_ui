package com.nextsolution.db.api;

import com.nextsolution.db.dto.Subscribe;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class SubscribeApi extends BaseApi {
    private static final String ROUTE_LIST_ACTIVE = "/share_van_order/subscribe/list";
    private String status = "active";
    public static void getSubscribe(IResponse result) {
        JSONObject params = null;
        String status_service = StatusType.RUNNING;
        try {
            params = new JSONObject();
            params.put("status",status_service);
            mOdoo.callRoute(ROUTE_LIST_ACTIVE, params, new SharingOdooResponse<OdooResultDto<Subscribe>>() {
                @Override
                public void onSuccess(OdooResultDto<Subscribe> subscribes) {
                    result.onSuccess(subscribes);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
