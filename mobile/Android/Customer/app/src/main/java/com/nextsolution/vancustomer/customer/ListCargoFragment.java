package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.nextsolution.db.dto.CargoDTO;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.vm.ListCargoVM;
import com.nextsolution.vancustomer.databinding.ListCargoFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class ListCargoFragment extends BaseFragment {

    ListCargoVM listCargoVM;
    ListCargoFragmentBinding mBinding;
    Integer positionCargoType=0;
    private BaseAdapterV3<CargoDTO> adapterV3;
    Integer indexItemClick=-1;
    String status;
    public ListCargoFragment(String status) {
        this.status = status;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view= super.onCreateView(inflater, container, savedInstanceState);
        listCargoVM = (ListCargoVM) viewModel;
        mBinding= (ListCargoFragmentBinding) binding;
        initView();
        setUpRecycleView();
        listCargoVM.getListCargo(this::runUI);
        initLoadMore();
        return view;
    }
    public void initView()
    {
        mBinding.searchCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });
    }
    public void setTextSearch(String textSearch){
        listCargoVM.getCargoCode().set(textSearch);
        search();
    }
    public void search(){
        listCargoVM.getListCargo(this::runUI);
    }

    @Override
    public void onItemClick(View v, Object o) {
        CargoDTO cargoDTO= (CargoDTO) o;
        indexItemClick =((CargoDTO) o).index-1;
        if(v.getId()==R.id.cargo_item){
            Intent intent= new Intent(getContext(), CommonActivity.class);
            Bundle bundle= new Bundle();
            bundle.putSerializable(Constants.DATA,cargoDTO);
            bundle.putSerializable(Constants.FRAGMENT,ListCargoDetailFragment.class);
            intent.putExtras(bundle);
            startActivityForResult(intent,Constants.REQUEST_FRAGMENT_RESULT);
        }
    }

    public void setUpRecycleView(){
        adapterV3 = new BaseAdapterV3<>(R.layout.list_cargo_item, listCargoVM.getListData(), this);
        mBinding.rcListCargo.setAdapter(adapterV3);
    }
    private void initLoadMore() {
        adapterV3.getLoadMoreModule().setOnLoadMoreListener(() -> {
            listCargoVM.loadMore(this::runUI);
        });
        adapterV3.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapterV3.getLoadMoreModule().setAutoLoadMore(true);
    }
    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if(view.getId()==R.id.toolbar){
            getActivity().onBackPressed();
        }
    }
    public void runUI(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_FAIL:
                ToastUtils.showToast(getActivity(),getResources().getString(R.string.NOT_FOUND_DATA), getResources().getDrawable(R.drawable.ic_fail));
                break;
            case Constants.GET_DATA_SUCCESS:
                adapterV3.getLoadMoreModule().loadMoreComplete();
                adapterV3.notifyDataSetChanged();
                listCargoVM.getEmptyData().set(false);
                break;
            case "noMore":
                adapterV3.getLoadMoreModule().loadMoreEnd();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==Constants.REQUEST_CODE && resultCode==Constants.RESULT_OK){
            positionCargoType=data.getIntExtra(Constants.POSITION_SELECTED,0);
            if(positionCargoType>0){
                listCargoVM.setSize_id(((CargoTypes) data.getSerializableExtra(Constants.DATA)).getId());
                listCargoVM.getCargoTypes().set(((CargoTypes) data.getSerializableExtra(Constants.DATA)));
            }else{
                listCargoVM.setSize_id(0);
                CargoTypes cargoTypes= new CargoTypes();
                cargoTypes.setType(getResources().getString(R.string.ALL));
                listCargoVM.getCargoTypes().set(cargoTypes);
            }
            listCargoVM.getListCargo(this::runUI);
        }
        else if(requestCode==Constants.REQUEST_FRAGMENT_RESULT && resultCode == Constants.RESULT_OK){
            if(data.hasExtra("isGomCargo")){
                if(data.getBooleanExtra("isGomCargo",false)){
                    listCargoVM.getListData().get(indexItemClick).setStatus("1");
                    adapterV3.notifyItemChanged(indexItemClick);
                }
            }
        }
        closeProcess();
    }

    public void chooseCargo() {
        FragmentManager fm = getFragmentManager();
        ChooseCargoDialog dialogFragment = new ChooseCargoDialog();
        Bundle bundle= new Bundle();
        bundle.putInt(Constants.DATA,positionCargoType);
        dialogFragment.setArguments(bundle);
        dialogFragment.setTargetFragment(this, Constants.REQUEST_CODE);
        dialogFragment.show(fm, "ABC");
    }
    @Override
    public int getLayoutRes() {
        return R.layout.list_cargo_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListCargoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcListCargo;
    }
}
