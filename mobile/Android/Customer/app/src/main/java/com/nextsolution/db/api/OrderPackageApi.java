package com.nextsolution.db.api;

import com.nextsolution.vancustomer.model.OrderPackage;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class OrderPackageApi extends BaseApi {
    private String status = "active";
    public static void getOrderPackages(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            mOdoo.callRoute("/share_van_order/get_bill_order_package", params, new SharingOdooResponse<OdooResultDto<OrderPackage>>() {
                @Override
                public void onSuccess(OdooResultDto<OrderPackage> subscribes) {
                    result.onSuccess(subscribes);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
