package com.nextsolution.vancustomer.service.service_dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenFbMerchantDTO {
    private Long user_id;
    private String token;
    private String androidId;
    private Date createDate;
    private Boolean isDelete;
}
