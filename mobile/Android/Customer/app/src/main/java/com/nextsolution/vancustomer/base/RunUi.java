package com.nextsolution.vancustomer.base;

public interface RunUi {
    void run(Object... params);
}
