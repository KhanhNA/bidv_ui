package com.nextsolution.vancustomer.routing;

public interface RouteBriefInfoInterface {
    void onItemChildClick();
    void onRegisterNewRoute();
    void onCancelNewRoute();
}
