package com.nextsolution.vancustomer.view_by_bill_lading;

import android.app.Application;
import android.util.Log;

import com.haibin.calendarview.Calendar;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillLadingVM extends BaseViewModel {
    private int currentPage = 0;
    private int totalPage = 0;
    HashMap<String, Calendar> mScheme;

    private ObservableBoolean isLoading = new ObservableBoolean(true);
    private ObservableBoolean emptyData = new ObservableBoolean(true);
    private List<BillRouting> billLadingList = new ArrayList<>();

    public BillLadingVM(@NonNull Application application) {
        super(application);
        mScheme = new HashMap<>();
    }

    public void getBillLadingByDate(String date, boolean isLoadMore, RunUi runUi) {
        if (!isLoadMore) {
            currentPage = 0;
            totalPage = 0;
            billLadingList.clear();
        }
        isLoading.set(true);
        RoutingApi.getBillLadingByDate(date, currentPage, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<BillRouting> rs = (OdooResultDto<BillRouting>) o;
                if (rs != null && rs.getRecords() != null && rs.getRecords().size() > 0) {
                    billLadingList.addAll(rs.getRecords());
                    if (rs.getTotal_record() % 10 == 0) {
                        totalPage = rs.getTotal_record() / 10;
                    } else {
                        totalPage = rs.getTotal_record() / 10 + 1;
                    }
                    emptyData.set(false);
                    runUi.run("getBillLadingSuccess");

                } else {
                    emptyData.set(true);
                    runUi.run("noMore");
                }
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                emptyData.set(true);
            }
        });
    }

    public void getListDate(java.util.Date fromDate, java.util.Date toDate, RunUi runUi) {

        OrderApi.getListDate(fromDate, toDate, true, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<OdooDate> odooResultDto = (OdooResultDto<OdooDate>) o;

                if (odooResultDto != null && odooResultDto.getRecords() != null) {
                    List<OdooDate> listDate = odooResultDto.getRecords();
                    DumpData(listDate);

                    runUi.run("getListDate");
                } else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });


    }

    private void DumpData(List<OdooDate> result) {
        mScheme.clear();
        for (OdooDate temp : result) {
            mScheme.put(getSchemeCalendar(temp, 0xFF40db25, "").toString(),
                    getSchemeCalendar(temp, 0xFF40db25, ""));
        }

    }

    private Calendar getSchemeCalendar(Date date, int color, String text) {
        java.util.Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int year = cal.get(java.util.Calendar.YEAR);
        int month = cal.get(java.util.Calendar.MONTH) + 1;
        int day = cal.get(java.util.Calendar.DAY_OF_MONTH);

        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);
        calendar.setScheme(text);
        return calendar;
    }

    public void loadMore(String date, RunUi runUi) {
        currentPage += 1;
        if (totalPage > 0 && currentPage < totalPage) {
            getBillLadingByDate(date, true, runUi);
        } else {
            runUi.run("noMore");
        }
    }

    public void onRefresh(String date, RunUi runUi) {
        currentPage = 0;
        totalPage = 0;
        getBillLadingByDate(date, false, runUi);
    }
}
