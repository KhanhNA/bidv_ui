package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;

import java.util.Calendar;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillRouting extends BaseModel {
    private Integer id;
    private Integer from_bill_lading_id;
    private Integer origin_bill_lading_detail_id;
    private String insurance_name;
    private String insurance_code;
    private String subscribe_name;
    private boolean routing_scan;
    private Long company_id;
//    private OdooRelType insurance_id = new OdooRelType();
    private Double total_weight;
    private Integer total_parcel;
    private Double total_amount;
    private Double total_volume;
    private Double vat;
    private List<BillRoutingDetail> arrBillLadingDetail;
    private String name_seq = "New";
    private String name;
    private String code;
    private String status = StatusType.RUNNING;
    private String status_routing;
    private String trouble_type;
    private OdooDate create_date;
    private Integer change_bill_lading_detail_id;
    private OdooDate start_date;
    private OdooDate end_date;

    private String startDateStr;
    private String description;//lý do hủy đơn

    private Insurance insurance;
//    private RecurrentModel recurrent;

    private Integer frequency;// 1 daily, 2 weekly, 3 monthly
    private Integer day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
    private Integer day_of_month;// if frequency = 3.
    private Subscribe subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...
    private String order_package_name;//Loại đơn hàng: đơn express, ...

    public void setStartDateStr(OdooDate start_date){
        if(start_date == null){
            return ;
        }
        startDateStr =  AppController.formatDate.format(start_date);
    }

    public void setStart_date(OdooDate start_date) {
        this.start_date = start_date;
        setStartDateStr(start_date);
    }
    public String getTotal_amountStr(){
        return AppController.getInstance().formatCurrency(total_amount);
    }

    public String getTotal_weight_str(){
        if(total_weight == null) return "0 kg";
        return AppController.getInstance().formatNumber(total_weight) + " kg";
    }

    public String getTotal_volume_str(){
        if(total_volume == null) return "";
        return AppController.getInstance().formatNumber(total_volume);
    }



    public String getStart_date_str(){
        if(start_date != null){
            return AppController.formatDate.format(start_date);
        }
        return  "";
    }

    public String getEnd_date_str(){
        if(end_date != null){
            return AppController.formatDate.format(end_date);
        }
        return  "";
    }

    public void setEndDate(Subscribe subscribe){
        if(subscribe == null || start_date == null){
            end_date = null;
            return;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(start_date);
        c.add(Calendar.DATE, subscribe.getValue());
        end_date = new OdooDate(c.getTime().getTime());
    }


}
