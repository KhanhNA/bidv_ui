package com.nextsolution.vancustomer.enums;

public class OrderPackageType{
    public static String NORMAL = "normal";
    public static String ECONOMIC = "economic";
    public static String EXPRESS = "express";

}
