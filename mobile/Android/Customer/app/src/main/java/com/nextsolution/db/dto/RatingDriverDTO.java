package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatingDriverDTO extends BaseModel {
    Integer id;
    Integer num_rating;
    String note;
    List<RatingBadges> badges_routings;
}
