package com.nextsolution.vancustomer.util;

/**
 * 0: kho xuất
 * 1: kho nhập
 */
public class RoutingDayType {
    public static final String WAREHOUSE_EXPORT = "0";
    public static final String WAREHOUSE_IMPORT = "1";
}