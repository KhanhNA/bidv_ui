package com.nextsolution.vancustomer.ui.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.databinding.BottomSheetAddressBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.model.Administrative;
import com.nextsolution.vancustomer.viewmodel.AdministrativeVM;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class AddressBottomSheetFragment extends BottomSheetDialogFragment {
    private AdministrativeVM addressRegisterVM;
    private BottomSheetAddressBinding binding;
    private int administrativeId;
    private int administrativeAction = 0;
    private BaseAdapterV3<Administrative> adapterAddress;
    private OnDismiss onDismiss;
    private Administrative administrative;
    private String addressProvince;
    private String addressDistrict;
    private boolean isDismiss;

    public AddressBottomSheetFragment() {
        super();
    }

    public AddressBottomSheetFragment(int administrativeId, int administrativeAction, OnDismiss onDismiss, String addressProvince, String addressDistrict) {
        this.administrativeId = administrativeId;
        this.administrativeAction = administrativeAction;
        this.onDismiss = onDismiss;
        this.addressProvince = addressProvince;
        this.addressDistrict = addressDistrict;
    }

    public AddressBottomSheetFragment(int administrativeId, int administrativeAction, OnDismiss onDismiss, String addressProvince) {
        this.administrativeId = administrativeId;
        this.administrativeAction = administrativeAction;
        this.onDismiss = onDismiss;
        this.addressProvince = addressProvince;
    }

    public AddressBottomSheetFragment(int administrativeId, int administrativeAction, OnDismiss onDismiss) {
        this.administrativeId = administrativeId;
        this.administrativeAction = administrativeAction;
        this.onDismiss = onDismiss;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        addressRegisterVM = ViewModelProviders.of(this).get(AdministrativeVM.class);
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_address, container, false);
        binding.setViewModel(addressRegisterVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        //xử lý sự kiện cuộn
        binding.rcAdministrative.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                v.getParent().requestDisallowInterceptTouchEvent(false);
            }
            return false;
        });
        initView();
        return binding.getRoot();
    }

    private void initView() {
        isDismiss = false;
        if (administrativeAction == Constants.ADMINISTRATIVE_ACTION_DISTRICT) {
            binding.linear2.setVisibility(View.VISIBLE);
            binding.linear1.setVisibility(View.GONE);
            binding.titleVillage.setVisibility(View.GONE);
            binding.titleProvince.setText(addressProvince);
        }
        if (administrativeAction == Constants.ADMINISTRATIVE_ACTION_VILLAGE) {
            binding.linear2.setVisibility(View.VISIBLE);
            binding.titleProvince.setText(addressProvince);
            binding.titleDistrict.setText(addressDistrict);
            binding.titleDistrict.setTextColor(getResources().getColor(R.color.state_background_text_color));
        }
        addressRegisterVM.getAdministrative(administrativeId, this::runUi);
        binding.close.setOnClickListener(view -> {
            administrativeAction = 0;
            isDismiss = false;
            dismiss();
        });
        adapterAddress = new BaseAdapterV3(R.layout.item_address_register, addressRegisterVM.getLstAdministrative());
        adapterAddress.addChildClickViewIds(R.id.itemAddress);
        adapterAddress.setOnItemChildClickListener((adapter, view, position) -> {
            if (view.getId() == R.id.itemAddress) {
                administrative = addressRegisterVM.getLstAdministrative().get(position);
                isDismiss = true;
                dismiss();
            }
        });
        binding.rcAdministrative.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rcAdministrative.setAdapter(adapterAddress);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if(action.equals("getAdministrativeSuccess")){
            adapterAddress.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        assert dialog != null;
        FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        assert bottomSheet != null;
        BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    behavior.setPeekHeight(500);
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        if (isDismiss) {
            onDismiss.onDismiss(administrativeAction, administrative);
        } else {
            onDismiss.onDismiss(0,null);
        }
        super.onDismiss(dialog);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public interface OnDismiss {
        void onDismiss(Integer administrativeAction, Administrative administrative);
    }
}
