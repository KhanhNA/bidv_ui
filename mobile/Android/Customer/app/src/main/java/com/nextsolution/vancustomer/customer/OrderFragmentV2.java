package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.OrderVMV2;
import com.nextsolution.vancustomer.enums.ActionClick;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.service.service_dto.NotificationService;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.NonSwipeableViewPager;
import com.nextsolution.vancustomer.widget.TabAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;

public class OrderFragmentV2 extends BaseFragment implements NetworkManager.NetworkHandler {

    private NonSwipeableViewPager viewPager;

    Toolbar toolbar;
    private OrderVMV2 orderVM;

    TextView tvProgressLayout;
    boolean isOnline;
    private NetworkManager networkManager;

    int tabPosition = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(OrderVMV2.class);
        binding.setVariable(BR.viewModel, viewModel);
        orderVM = (OrderVMV2) viewModel;
        orderVM.initBillLading();
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.new_oder);
        tvProgressLayout = view.findViewById(R.id.tv_network);
        getBaseActivity().setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        TabLayout tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.LEFT);

        TabAdapter adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new PickupWarehouseFragment(), getContext().getResources().getString(R.string.WAREHOUSE_EXPORT));
        adapter.addFragment(new ReceiptWarehouseFragment(), getContext().getResources().getString(R.string.WAREHOUSE_IMPORT));
        adapter.addFragment(new OrderConfirmFragment(), getContext().getResources().getString(R.string.confirm_order));

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));

        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            int finalI = i;
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> finalI > tabPosition);
        }

        return view;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {
        if (notificationService.getClick_action().equals(ActionClick.TARGET_BILL_LADING_INFO_ACTIVITY)) {
            ToastUtils.showToast(getActivity(), getString(R.string.create_order_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
            getActivity().finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.exit), getString(R.string.msg_exit_new_order)
                    , v -> getBaseActivity().onBackPressed());
            dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Integer pageSelected) {
        if (pageSelected != null) {
            switch (pageSelected) {
                case 0:
                    viewPager.setCurrentItem(0);
                    break;
                case 1:
                    viewPager.setCurrentItem(1);
                    break;
                case 2:
                    viewPager.setCurrentItem(2);
                    break;
                case 3:
                    viewPager.setCurrentItem(3);
                    break;
                case 999:
                    requireActivity().onBackPressed();
                    break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        networkManager.stop();
    }


    @Override
    public int getLayoutRes() {
        return R.layout.order_fragment_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVMV2.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            tvProgressLayout.setVisibility(View.VISIBLE);
        } else {
            tvProgressLayout.setVisibility(View.GONE);
        }
    }
}
