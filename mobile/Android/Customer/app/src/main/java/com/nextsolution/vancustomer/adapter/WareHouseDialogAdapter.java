package com.nextsolution.vancustomer.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public class WareHouseDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Warehouse> listWareHouse, listPlaces, data;
    private Warehouse header;
    private Warehouse header1;

    private AdapterListener adapterListener;
    private Warehouse selectedWareHouse;
    public WareHouseDialogAdapter(Warehouse selectedWarehouse, List<Warehouse> listWareHouse, List<Warehouse> listPlaces, AdapterListener adapterListener) {
        this.listWareHouse = listWareHouse;
        this.listPlaces = listPlaces;
        this.data = new ArrayList<>();
        header = new Warehouse();
        header.setId(-1);
        this.data.addAll(listWareHouse);
        header1 = new Warehouse();
        header1.setId(-2);
        this.data.add(header1);
        this.data.addAll(listPlaces);
        this.selectedWareHouse = selectedWarehouse;
        checkedWarehouse(selectedWareHouse, listWareHouse);

        this.adapterListener = adapterListener;
    }

    private void checkedWarehouse(Warehouse selectedWarehouse, List<Warehouse> listWareHouse) {
        for(Warehouse temp : listWareHouse){
            if(selectedWarehouse != null && selectedWarehouse.getId() != null){
                if(temp.getId() != null && Objects.equals(selectedWarehouse.getId(), temp.getId())){
//                    temp.selected = true;
                }
            }
        }
    }

    public void update(){
        checkedWarehouse(selectedWareHouse, listWareHouse);
        this.data.clear();
        this.data.add(header);
        this.data.addAll(listWareHouse);
        this.data.add(header1);
        this.data.addAll(listPlaces);
        notifyDataSetChanged();
    }

    public void update(List<Warehouse> list){
        checkedWarehouse(selectedWareHouse, listWareHouse);
        this.data.clear();
        this.data.add(header);
        this.data.addAll(list);
        this.data.add(header1);
        this.data.addAll(listPlaces);
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 1){
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_warehouse, parent, false);
            return new ViewHolder(binding);
        }
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View headerView = inflater.inflate(R.layout.item_header_warehouse, parent, false);
        return new ViewHolderHeader(headerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == 0){
            ((ViewHolderHeader)holder).bind(data.get(position).getId());
        }else {
            ((ViewHolder)holder).bind(data.get(position));

        }
    }

    @Override
    public int getItemViewType(int position) {
        Integer id = data.get(position).getId();
        if(id != null && id < 0){//type header
            return 0;
        }else {// type warehouse
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding itemProductBinding;

        ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.itemProductBinding = view;
            view.getRoot().setOnClickListener(v -> {
                adapterListener.onItemClick(v, data.get(getAdapterPosition()));
            });
        }

        void bind(Object obj) {
            itemProductBinding.setVariable(BR.viewHolder, obj);
            itemProductBinding.setVariable(BR.listenerAdapter, adapterListener);
            itemProductBinding.executePendingBindings();
        }

        public void bindWithVM(BaseViewModel viewModel) {
            itemProductBinding.setVariable(BR.viewModel, viewModel);
        }
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        private TextView txtHeader;

        ViewHolderHeader(View view) {
            super(view);
            this.txtHeader = view.findViewById(R.id.lbWareHouse);
        }

        void bind(long id) {
            if(id == -1){
                txtHeader.setText(R.string.WAREHOUSE);
            }
            else if(id == -2){
                txtHeader.setText(R.string.places);
            }
        }
    }
}
