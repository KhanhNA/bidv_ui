package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cargo extends BaseModel {

    private boolean qrChecked=false;
    private Integer id;
    private String cargo_number;
    private Integer from_depot_id;
    private Integer to_depot_id;
    private Integer distance;
    private Integer size_id;
    private Integer weight;
    private String description;
    private Object price;
    private Double from_latitude;
    private Double to_latitude;
    private Integer bidding_package_id;
    private Double from_longitude;
    private Double to_longitude;
    private String size_standard;
    private String qr_code;
}
