package com.nextsolution.vancustomer.enums;

public class ErrorCreateAccount {
    public static final String NOT_ENOUGH_IMAGE="600";
    public static final String NO_PHONE_NUMBER="601";
    public static final String NO_NAME="602";
    public static final String NO_BIRTHDAY="603";
    public static final String NO_GENDER="604";
    public static final String NO_ADDRESS="605";
    public static final String NO_PROVINCE="606";
    public static final String NO_DISTRICT="607";
    public static final String ACCOUNT_ALREADY_EXISTS="608";
    public static final String CREATE_COMPANY_FAIL="610";
}
