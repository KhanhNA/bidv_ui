package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.vm.ConfirmDialogVm;
import com.nextsolution.vancustomer.databinding.ConfirmDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;

public class ConfirmDialog extends DialogFragment {
    ConfirmDialogBinding mBinding;
    ConfirmDialogVm confirmDialogVm;
    String title;
    String description;

    public ConfirmDialog(Integer title, String description) {
        this.title = AppController.getInstance().getResources().getString(title);
        this.description = description;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.confirm_dialog, container, false);
        confirmDialogVm = ViewModelProviders.of(this).get(ConfirmDialogVm.class);
        mBinding.setViewModel(confirmDialogVm);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return mBinding.getRoot();
    }
    public void initView(){
        mBinding.lbTitle.setText(title);
        mBinding.lbDescription.setText(description);
    }
    public void onClick(View v){
        Intent intent = new Intent();
        Bundle bundle= new Bundle();
        if(v.getId()==R.id.btnYes) {
            bundle.putBoolean(Constants.DATA, true);
        }else{
            bundle.putBoolean(Constants.DATA, false);
        }
        this.dismiss();
        intent.putExtras(bundle);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
