package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.CustomerType;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditInfoCustomerVM extends BaseViewModel {

    ObservableBoolean isEdit= new ObservableBoolean();
    ObservableBoolean changeAvatar= new ObservableBoolean(); // Check đã đổi Avatar
    ObservableBoolean isEditAvatar= new ObservableBoolean(); // check có cho phép đổi avatar
    ObservableBoolean isChangeData= new ObservableBoolean(); // check thông tin có thay đổi không
    ObservableField<Partner> infoCustomer = new ObservableField<>();
    Partner partner = new Partner();
    ObservableField<String> img1=new ObservableField<>();
    LocalMedia url_avatar=new LocalMedia();

    // Clone data
    public EditInfoCustomerVM(@NonNull Application application) {
        super(application);
        initData();
    }
    private void initData(){
        changeAvatar.set(false);
        partner.setId(StaticData.getPartner().getId());
        partner.setDisplay_name(StaticData.getPartner().getDisplay_name());
        partner.setPhone(StaticData.getPartner().getPhone());
        partner.setStreet(StaticData.getPartner().getStreet());
        partner.setCity(StaticData.getPartner().getCity());
        partner.setZip(StaticData.getPartner().getZip());
        partner.setEmail(StaticData.getPartner().getEmail());
        partner.setUri_path(StaticData.getPartner().getUri_path());
        infoCustomer.set(partner);
        isEdit.set(false);
        isChangeData.set(false);
        if(CustomerType.FREE_CUSTOMER.equals(StaticData.getPartner().getCustomer_type())){
            isEditAvatar.set(true);
        }else{
            isEditAvatar.set(false);
        }

    }
    public void updateCustomer(RunUi runUi) {
        CustomerApi.updateInfoCustomer(infoCustomer.get(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                Boolean aBoolean= (Boolean) o;
                if(aBoolean){
                    if(changeAvatar.get()){
                        StaticData.getPartner().setUri_path(img1.get());
                    }
                    runUi.run(Constants.GET_DATA_SUCCESS);
                }else{
                    runUi.run(Constants.FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }
    public void editAvatar(RunUi runUi){
        CustomerApi.editAvatar(url_avatar, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o!=null){
                    StaticData.getPartner().setUri_path(img1.get());
                    runUi.run("EDIT_AVATAR_SUCCESS");
                }else{
                    runUi.run("EDIT_AVATAR_FAIL");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
