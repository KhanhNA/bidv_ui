package com.nextsolution.vancustomer.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.CreateAccountFragment;
import com.nextsolution.vancustomer.databinding.ActivityLoginV2Binding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.model.UserInfo;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.DialogServerConfig;
import com.nextsolution.vancustomer.util.ApiResponse;
import com.nextsolution.vancustomer.util.ImageUtils;
import com.nextsolution.vancustomer.util.LanguageUtils;
import com.nextsolution.vancustomer.util.NetworkUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;


public class
LoginActivityV2 extends BaseActivity<ActivityLoginV2Binding> implements ActionsListener {
    private LoginVM loginVM;

    private CircularProgressButton btnLogin;
    private AppCompatSpinner spLanguage;
    private int check = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginVM = (LoginVM) viewModel;
        btnLogin = binding.btnLogin;
        spLanguage = binding.spin;
        loginVM.loginResponse().observe(this, this::consumeResponse);
        spLanguage.setSelection(AppController.getInstance().getSharePre().getInt("current_language", 0));
        //
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++check > 1) {
                    try {
                        LanguageUtils.changeLanguage(LoginActivityV2.this, AppController.LANGUAGE_CODE[i]);
                        AppController.getInstance().getSharePre().edit().putInt("current_language", i).apply();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TextWatcher loginTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userName = binding.txtUserName.getText().toString().trim();
                String password = binding.txtPassword.getText().toString().trim();
                binding.btnLogin.setEnabled(!userName.isEmpty() && !password.isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        binding.txtUserName.addTextChangedListener(loginTextWatcher);
        binding.txtPassword.addTextChangedListener(loginTextWatcher);

        binding.imgSettingServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogServerConfig dialogChangeConfig = new DialogServerConfig();
                dialogChangeConfig.show(getSupportFragmentManager(), "confirm_url");
            }
        });
        binding.btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCreateAccountFragment();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(AppController.getInstance().getSharePre().getString(Constants.USER_NAME,"")!=""){
            UserInfo userInfo= new UserInfo();
            userInfo.setIsSave(AppController.getInstance().getSharePre().getBoolean("chkSave", true));
            userInfo.setPassWord(AppController.getInstance().getSharePre().getString(Constants.MK, ""));
            loginVM.getModel().set(userInfo);
        }
    }

    private void gotoCreateAccountFragment(){
        Intent intent= new Intent(this, CommonActivity.class);
        Bundle bundle= new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, CreateAccountFragment.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_ACTIVITY_RESULT);
    }


    private void consumeResponse(ApiResponse apiResponse) {
        switch (apiResponse.status) {
            case LOADING:
//                progressDialog.show();
                break;

            case SUCCESS:
                binding.txtLoginFail.setVisibility(View.INVISIBLE);
                startActivity(new Intent(LoginActivityV2.this, MainActivity.class));
                finish();
                break;

            case ERROR:
                binding.txtLoginFail.setVisibility(View.VISIBLE);
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;
            case NOT_CONNECT:
                Toast.makeText(this, R.string.not_connect_server, Toast.LENGTH_SHORT).show();
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;

            default:
                break;
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login_v2;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
//        loginVM.getModelE().setUserName("admin");
//        loginVM.getModelE().setPassWord("admin");
        if (R.id.btnLogin == v.getId()) {
            if (isValid()) {
                if (!NetworkUtils.isNetworkConnected(this)) {
                    Toast.makeText(LoginActivityV2.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    btnLogin.startAnimation();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loginVM.requestLogin();
                        }
                    }, 1000);
                }
            }
        }
    }

    private boolean isValid() {
        if (loginVM.getModelE().getUserName().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_username));
            return false;
        } else if (loginVM.getModelE().getPassWord().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_password));
            return false;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        btnLogin.dispose();
    }

    @Override
    public void onBackPressed() {
        DialogConfirm dialogConfirm = new DialogConfirm(getResources().getString(R.string.notice), getResources().getString(R.string.msg_exit_app)).setOnClickListener(v -> finish());
        dialogConfirm.show(getSupportFragmentManager(), LoginActivityV2.class.getName());
    }
}
