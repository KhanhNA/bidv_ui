package com.nextsolution.vancustomer.model;


import com.google.android.material.chip.ChipGroup;
import com.nextsolution.db.dto.Subscribe;
import com.tsolution.base.BaseModel;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecurrentModel extends BaseModel {
    private Integer frequency;// 1 daily, 2 weekly, 3 monthly, 4 express, 5 one time.
    private Integer day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
    private Integer day_of_month;// if frequency = 3.
    private Subscribe subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...

    @BindingAdapter("checkedDayOfWeek")
    public static void checkedDayOfWeek(ChipGroup chipGroup, int dayOfWeek){
        chipGroup.check(chipGroup.getChildAt(dayOfWeek).getId());
    }
}
