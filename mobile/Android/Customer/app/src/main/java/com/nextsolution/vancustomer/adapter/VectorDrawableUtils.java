package com.nextsolution.vancustomer.adapter;


import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

/**
 * Created by Vipul Asri on 28/12/16.
 */

public class VectorDrawableUtils {

    public static Drawable getDrawable(Context context, Integer drawableResId) {
        return VectorDrawableCompat.create(context.getResources(), drawableResId, context.getTheme());
    }

    public static Drawable getDrawable(Context context, Integer drawableResId, Integer colorFilter)  {
        Drawable drawable = getDrawable(context, drawableResId);
        drawable.setColorFilter(colorFilter, PorterDuff.Mode.SRC_IN);
        return drawable;
    }


}
