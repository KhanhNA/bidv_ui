package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.google.gson.JsonObject;
import com.nextsolution.db.api.IResponse;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.WarehouseApi;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.PlacesPOJO;
import com.nextsolution.vancustomer.util.StringUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WareHouseVM extends BaseViewModel {

    private ObservableBoolean isLoading = new ObservableBoolean(false);
    public List<Warehouse> listWarehouses;


    public WareHouseVM(@NonNull Application application) {
        super(application);

        listWarehouses = new ArrayList<>();
//        WarehouseApi.getCustWarehouse(StaticData.getPartner().getId(), new IResponse<OdooResultDto<Warehouse>>() {
//            @Override
//            public void onSuccess(OdooResultDto<Warehouse> result, Object... params) {
//                List<Warehouse> list = result.getRecords();
//                listWarehouses.addAll(list);
//            }
//        });
    }

    public void getPlaces(String key, List<Warehouse> listWarehouses, RunUi runUi) {
        List<Warehouse> rs = new ArrayList<>();
        for (Warehouse temp : listWarehouses) {
            if (StringUtils.isNullOrEmpty(temp.getName()) || StringUtils.isNullOrEmpty(temp.getAddress())) {
                rs.add(temp);
            } else if (temp.getName().toUpperCase().contains(key.toUpperCase()) || temp.getAddress().contains(key)) {
                rs.add(temp);
            }
        }

        runUi.run(rs);

//        WarehouseApi.doPlaces(getApplication().getApplicationContext(), key, o -> {
//            listPlaces.clear();
//            PlacesPOJO.Root root = (PlacesPOJO.Root) o;
//            for (PlacesPOJO.CustomA customA : root.customA) {
//                Warehouse wareHouseModel = new Warehouse();
//                wareHouseModel.setLatitude(customA.geometry.locationA.lat);
//                wareHouseModel.setLongitude(customA.geometry.locationA.lng);
//                wareHouseModel.setName(customA.name);
//                wareHouseModel.setPlaceId(customA.place_id);
//                wareHouseModel.setAddressByAdministrative(customA.formatted_address);
//                listPlaces.add(wareHouseModel);
//            }
//            isLoading.set(false);
//        });

    }

    public void getWareHouse(Long companyId, RunUi runUi) {
        isLoading.set(true);
        WarehouseApi.getCustWarehouse(companyId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getWarehouseResult(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void getWarehouseResult(Object response, RunUi runUi) {
        isLoading.set(false);
        this.listWarehouses.clear();
        OdooResultDto<Warehouse> resultDto = (OdooResultDto<Warehouse>) response;

        this.listWarehouses.addAll(resultDto.getRecords());
        runUi.run(listWarehouses);
    }

    public void getPlaceDetail(Warehouse warehouse, RunUi runUi) {
        isLoading.set(true);
        WarehouseApi.getPlaceDetail(getApplication().getApplicationContext(), warehouse.getPlaceId(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                String json = ((JsonObject) o).get("result").toString();
                runUi.run(json, warehouse);
            }

            @Override
            public void onFail(Throwable error) {
                
            }
        });
    }


}
