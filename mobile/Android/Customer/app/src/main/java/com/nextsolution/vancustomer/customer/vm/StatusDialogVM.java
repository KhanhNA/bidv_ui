package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusDialogVM extends BaseViewModel {
    Integer position = 0;
    List<Integer> list= new ArrayList<>();

    public StatusDialogVM(@NonNull Application application) {
        super(application);
    }

    public void getQuantityTypeStatusRoutingDay(RunUi runUi, Date date) {
        OrderApi.getQuantityTypeStatusRoutingDay(date, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {

                list.addAll((Collection<? extends Integer>) o);
                runUi.run(Constants.GET_DATA_SUCCESS);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
