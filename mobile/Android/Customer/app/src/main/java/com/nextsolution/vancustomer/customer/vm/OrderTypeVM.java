package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Good_Boy
 */
@Getter
@Setter
public class OrderTypeVM extends BaseViewModel {
    ObservableField<String> typeIndex;

    public OrderTypeVM(@NonNull Application application) {
        super(application);
        typeIndex = new ObservableField<>("1");
    }

}
