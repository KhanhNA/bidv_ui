package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Warehouse extends BaseModel implements Cloneable {
    private Integer id;
    private String warehouse_code;
    private String name;

    private Integer state_id;
    private Integer area_id;
    private Integer district;
    private Integer ward;
    private Integer country_id;

    private String province_name;
    private String district_name;
    private String ward_name;
    private String address;

    private Double latitude;
    private Double longitude;
    private String phone;
    private Area areaInfo;

    private String name_seq;

    private String fromWareHouseCode;

    private transient String wjson_address;
    private transient String placeId;

    public String getAddressDetail() {
        return  address+ "\n"
                + getAdministrative();
    }
    public String getFullAddress() {
        return  address+", "+ getVillage() + ", " + getDistrictName() + ", " + getProvinceName();
    }

    public String getAdministrative() {
        return getVillage() + ", " + getDistrictName() + ", " + getProvinceName();
    }

    public String getVillage() {
        return ward_name != null ? this.ward_name : "";
    }

    public String getDistrictName() {
        return district_name != null ? this.district_name : "";
    }

    public String getProvinceName() {
        return province_name != null ? this.province_name : "";
    }

    @NotNull
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new BillPackage();
            // This should never happen
        }
    }

}

