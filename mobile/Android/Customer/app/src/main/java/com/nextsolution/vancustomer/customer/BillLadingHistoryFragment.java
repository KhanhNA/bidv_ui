package com.nextsolution.vancustomer.customer;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextsolution.db.dto.BillLading;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.vm.BillLadingHistoryVM;
import com.nextsolution.vancustomer.databinding.BillLadingHistoryFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.Calendar;
import java.util.Date;

public class BillLadingHistoryFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, NetworkManager.NetworkHandler {
    private BillLadingHistoryVM billLadingHistoryVM;
    private BillLadingHistoryFragmentBinding mBinding;
    private boolean isFromDate = false;
    StatusBillLadingDialogFragment dialogFragment;
    private BaseAdapterV3 adapter;

    boolean isOnline;
    private NetworkManager networkManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        billLadingHistoryVM = (BillLadingHistoryVM) viewModel;

        mBinding = (BillLadingHistoryFragmentBinding) binding;
        initView();
        billLadingHistoryVM.init(this::runUi);
        initLoadMore();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }
    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void initView() {
        mBinding.toolbar.setTitle(R.string.menu_order_list);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mBinding.orderCodeSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    getBilllading();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBinding.orderCodeSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getData();
                    return true;
                }
                return false;
            }
        });
        mBinding.btnSearch.setOnClickListener(v->{getData();});
        mBinding.lbChooseStatus.setEndIconOnClickListener(v -> {
            showStatusBillLadingDialog();
        });

        Calendar cal = Calendar.getInstance();
        billLadingHistoryVM.setTDate(cal.getTime());
        cal.set(Calendar.DAY_OF_MONTH, 1);
        billLadingHistoryVM.setFDate(cal.getTime());
        setUpRecycleView();
        mBinding.swRefresh.setOnRefreshListener(() -> {
            getData();
        });
    }
    public void getBilllading(){
        billLadingHistoryVM.getData(this::runUi, false);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                adapter.getLoadMoreModule().loadMoreComplete();
                adapter.notifyDataSetChanged();
                billLadingHistoryVM.getEmptyData().set(false);
                break;
            case Constants.GET_DATA_FAIL:
                billLadingHistoryVM.getEmptyData().set(true);
                break;
            case "noMore":
                adapter.getLoadMoreModule().loadMoreEnd();
                break;
        }
    }

    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            billLadingHistoryVM.loadMore(this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void setUpRecycleView() {
        adapter = new BaseAdapterV3(R.layout.bill_lading_history_item, billLadingHistoryVM.getListBillByDay(), this);
        mBinding.orderHistory.setAdapter(adapter);
    }

    private void showStatusBillLadingDialog() {
        dialogFragment = new StatusBillLadingDialogFragment(billLadingHistoryVM.getStatus().get(), new StatusBillLadingDialogFragment.IChooseStatusBillLading() {
            @Override
            public void chooseStatusBillLading(String status) {
                billLadingHistoryVM.getStatus().set(status);
                getData();
                dialogFragment.dismiss();
            }
        });
        dialogFragment.show(getChildFragmentManager(), "ABC");
    }


    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemOrderHistory) {
            Intent intent = new Intent(getContext(), BillLadingInfoActivity.class);
            BillLading billLading = (BillLading) o;
            intent.putExtra(Constants.ITEM_ID, billLading.getId()+"");
            startActivity(intent);
        }
    }

    public void getData() {
        billLadingHistoryVM.getIsLoading().set(true);
        billLadingHistoryVM.getData(this::runUi, false);
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.toolbar) {
            getBaseActivity().onBackPressed();
        } else if (R.id.fDate == view.getId()) {
            isFromDate = true;
            showDialogDatePicker(null, null);
        } else if (R.id.tDate == view.getId()) {
            isFromDate = false;
            showDialogDatePicker(null, null);
        } else if (R.id.chooseStatus == view.getId()) {
            showStatusBillLadingDialog();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDialogDatePicker(Long minDate, Long maxDate) {
        Context scene = getContext();
        if (scene != null) {
            Calendar c = Calendar.getInstance();
            if (isFromDate) {
                c.setTime(billLadingHistoryVM.getFDate());
            } else {
                c.setTime(billLadingHistoryVM.getTDate());
            }
            Integer year = c.get(Calendar.YEAR);
            Integer month = c.get(Calendar.MONTH);
            Integer day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            if (maxDate != null) {
                datePickerDialog.getDatePicker().setMaxDate(maxDate);
            }
            if (minDate != null) {
                datePickerDialog.getDatePicker().setMinDate(minDate);
            }
            datePickerDialog.show();
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.bill_lading_history_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillLadingHistoryVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.order_history;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        if (isFromDate) {
            billLadingHistoryVM.getFDate().setTime(calendar.getTime().getTime());
            billLadingHistoryVM.getFromDate().set(MyDateUtils.convertDateToString(MyDateUtils.convertDayMonthYearToDate(dayOfMonth, month, year), MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        } else {
            billLadingHistoryVM.getTDate().setTime(calendar.getTime().getTime());
            billLadingHistoryVM.getToDate().set(MyDateUtils.convertDateToString(MyDateUtils.convertDayMonthYearToDate(dayOfMonth, month, year), MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        }
        getData();

    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.tvNetwork.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.tvNetwork.setVisibility(View.GONE);
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
