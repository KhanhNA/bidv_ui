package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.dto.BillPackage;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillPackageDialogVM extends BaseViewModel {
    ObservableField<BillPackage> billPackage =  new ObservableField<>();
    public BillPackageDialogVM(@NonNull Application application) {
        super(application);
    }
}
