package com.nextsolution.vancustomer.routing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

import java.util.List;

public class RoutingsFragment extends BaseFragment implements ActionsListener {


    private RoutingVM routingVM;

    public RoutingsFragment()  {
//        getMapAsync(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        routingVM = (RoutingVM)viewModel;
        BaseAdapterV3<RoutingVanDay> adapter = new BaseAdapterV3<>(R.layout.routing_item, StaticData.getRoutingVanDays(), this);

        recyclerView.setAdapter(adapter);
        return binding.getRoot();

    }

    public void onItemChildClick(RoutingVanDay routingVanDay) {
        System.out.println("okkkkkkkkkkkkkkkkkk");
        if(routingVanDay == null){
            return;
        }
        AppController.getInstance().newActivity(getBaseActivity(), RoutingDetailFragment.class, StaticData.ROUTE_DETAIL, routingVanDay);
    }

    public void callChatApp(){
        AppController.getInstance().callAppChat();
    }


    public void setAdapterList(List data){
        ((BaseAdapterV3)recyclerView.getAdapter()).setList(data);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_routes;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.routes;
    }



    @Override
    public void action(Object... objects) {

    }

}