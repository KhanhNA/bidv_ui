package com.nextsolution.vancustomer.view_by_bill_lading;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.nextsolution.db.dto.BillRoutingDetail;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.BillLadingInfoActivity;
import com.nextsolution.vancustomer.databinding.BillRoutingDetailActivityBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.ui.fragment.DialogWithEditText;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;

public class BillRoutingDetailActivity extends BaseActivity<BillRoutingDetailActivityBinding> implements NetworkManager.NetworkHandler {
    BillRoutingDetailVM billRoutingDetailVM;
    XBaseAdapter billRoutingDetailAdapter;
    DialogWithEditText dialogConfirm;

    boolean isOnline;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billRoutingDetailVM = (BillRoutingDetailVM) viewModel;

        initToolBar();
        try{
            if (getIntent().hasExtra(Constants.ITEM_ID)) {
                String bill_routing_id = getIntent().getStringExtra(Constants.ITEM_ID);
                billRoutingDetailVM.getBillRoutingDetail(Integer.parseInt(bill_routing_id), this::runUi);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        initView();
    }

    private void initView() {
        billRoutingDetailAdapter = new XBaseAdapter(R.layout.item_bill_routing_detail, billRoutingDetailVM.getModelE().getArrBillLadingDetail(), this);
        binding.rcBillRoutingDetail.setAdapter(billRoutingDetailAdapter);

        binding.txtRoutingCode.setOnClickListener(v -> goToOriginBillLading());
        binding.btnCancel.setOnClickListener(v -> cancelBillRouting());

    }

    @Override
    protected void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }
    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void goToOriginBillLading() {
        Intent intent = new Intent(this, BillLadingInfoActivity.class);
        intent.putExtra(Constants.ITEM_ID, billRoutingDetailVM.getModelE().getFrom_bill_lading_id());
        startActivity(intent);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getBillRoutingSuccess":
                billRoutingDetailAdapter.notifyDataSetChanged(billRoutingDetailVM.getModelE().getArrBillLadingDetail());
                break;
            case "updateOrderSuccess":
                ToastUtils.showToast(this, getString(R.string.cancel_success), getResources().getDrawable(R.drawable.ic_check));
                setResult();
                finish();
                break;
            case "updateOrderFail":
                ToastUtils.showToast(this, getString(R.string.cancel_fail), getResources().getDrawable(R.drawable.ic_fail));
                break;
        }
    }

    private void setResult() {
        setResult(Constants.RESULT_OK);
    }

    @Override
    public void onItemClick(View v, Object o) {
        showBillPackage((BillRoutingDetail) o);
    }

    private void showBillPackage(BillRoutingDetail routingDetail) {
        BillPackagesDialog billPackagesDialog = new BillPackagesDialog(routingDetail);
        billPackagesDialog.show(getSupportFragmentManager(), billPackagesDialog.getTag());
    }

    public void cancelBillRouting() {
        dialogConfirm = new DialogWithEditText(getString(R.string.cancel),
                getString(R.string.msg_cancel_update_order),
                getString(R.string.reason_cancel_order)
                , v -> {
            billRoutingDetailVM.cancelOrder(v.getText().toString(), BillRoutingDetailActivity.this::runUi);
            dialogConfirm.dismiss();
        });
        dialogConfirm.show(getSupportFragmentManager(), dialogConfirm.getTag());
    }

    private void initToolBar() {
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bill_routing_detail_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillRoutingDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            binding.tvNetwork.setVisibility(View.GONE);
            binding.progressLayout.setVisibility(View.GONE);
        }
    }
}
