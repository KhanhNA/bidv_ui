package com.nextsolution.db.dto;

@lombok.Getter
@lombok.Setter
public class User  {
  private String __typename;
  private String avatar;
  private String banner_url;
  private java.util.Date birthday;
  private String description;
  private String email;
  private String first_name;
  private Integer id;
  private String jwt;
  private java.util.Date lastActiveAt;
  private String last_name;
  private String username;

}

