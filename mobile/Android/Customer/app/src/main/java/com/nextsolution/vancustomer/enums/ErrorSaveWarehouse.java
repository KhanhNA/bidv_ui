package com.nextsolution.vancustomer.enums;

public class ErrorSaveWarehouse {
    public static final String NOT_PERMISSION="401"; // Không có quyền
    public static final String WAREHOUSE_RUNNING="403"; // kho có đơn đang chạy
    public static final String SAME_NAME="406"; // Tên kho đã tồn tại
    public static final String SAME_PHONE="407"; // Số điện thoại đã tồn tại
    public static final String SAME_ADDRESS="408"; // Địa chỉ đã tồn tại
    public static final String DEACTIVATE_SUCCESSFUL="200"; // xóa kho thành công
    public static final String DEACTIVATE_FAIL="500"; // xóa kho thất bại
}
