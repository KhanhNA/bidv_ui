package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.ProductTypeApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.ProductType;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.util.StringUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Good_Boy
 */
@Getter
@Setter
public class AddOrderItemVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableField<BillPackage> billPackage = new ObservableField<>();
    private List<ProductType> listProductType;
    public AddOrderItemVM(@NonNull Application application) {
        super(application);
        billPackage.set(new BillPackage());
        listProductType = new ArrayList<>();
    }

//    public boolean isValid() {
//        boolean isValid = true;
//        boolean isSelectedType = false;
//        BillPackage model = billPackage.get();
//        if (model != null) {
//            if (StringUtils.isNullOrEmpty(model.getItem_name())) {
//                isValid = false;
//                addError("itemName", R.string.INVALID_FIELD, true);
//            } else {
//                clearErro("itemName");
//            }
//
//            for(ProductType type : listProductType){
//                if(type.checked != null && type.checked){
//                    billPackage.get().setProduct_type_name(type.getName());
//                        billPackage.get().setProductType(type);
//                    isSelectedType = true;
//                    break;
//                }
//            }
//
//            if(!isSelectedType){
//                isValid = false;
//                addError("itemType", R.string.INVALID_FIELD, true);
//            }else {
//                clearErro("itemType");
//
//            }
//
//            if (model.getQuantity_package() == null ||model.getQuantity_package() <= 0) {
//                isValid = false;
//                addError("quantity", R.string.INVALID_FIELD, true);
//            } else {
//                clearErro("quantity");
//            }
//
//
//            if (model.getLength() == null || model.getLength() <= 0) {
//                isValid = false;
//                addError("longs", R.string.INVALID_FIELD, true);
//            }else if(model.getLength() < 0.01){
//                isValid = false;
//                addError("longs", R.string.GREATER_THAN_001M, true);
//            }else {
//                clearErro("longs");
//            }
//
//            if (model.getWidth() == null || model.getWidth() <= 0) {
//                isValid = false;
//                addError("width", R.string.INVALID_FIELD, true);
//            }else if(model.getWidth() < 0.01){
//                isValid = false;
//                addError("width", R.string.GREATER_THAN_001M, true);
//            }else {
//                clearErro("width");
//            }
//
//            if (model.getHeight() == null || model.getHeight() <= 0) {
//                isValid = false;
//                addError("height", R.string.INVALID_FIELD, true);
//            }else if(model.getHeight() < 0.01){
//                isValid = false;
//                addError("height", R.string.GREATER_THAN_001M, true);
//            }else {
//                clearErro("height");
//            }
//
//            if (model.getWeight() == null || model.getWeight() <= 0) {
//                isValid = false;
//                addError("weight", R.string.INVALID_FIELD, true);
//            }else if(model.getWeight() < 0.1){
//                isValid = false;
//                addError("weight", R.string.GREATER_THAN_001KG, true);
//            }else {
//                clearErro("weight");
//            }
//        }
//        return  isValid;
//    }

    public void getListType(RunUi runUi){
        isLoading.set(true);
        ProductTypeApi.getProductType(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o != null){
                    isLoading.set(false);
                    listProductType.clear();
                    listProductType.addAll(((OdooResultDto<ProductType>)o).getRecords());
                    runUi.run("getListTypeSuccess");
                }else {
                    isLoading.set(false);
                    runUi.run("getListTypeFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void compareProductType(ProductType selectedType, RunUi runUi){
        isLoading.set(true);
        ProductTypeApi.checkProductType(selectedType, billPackage.get().getProductType(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                Boolean isValid = (Boolean) o;
                if(isValid != null && isValid){
                    runUi.run("compareSuccess");
                }else {
                    runUi.run("compareFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("compareFail");

            }
        });
    }

}
