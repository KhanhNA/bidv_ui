package com.nextsolution.vancustomer.scan;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.Cargo;
import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.BillPackageDialog;
import com.nextsolution.vancustomer.customer.CargoBillPackageDialog;
import com.nextsolution.vancustomer.customer.CargoTypesDialog;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.RoutingDayType;
import com.nextsolution.vancustomer.util.ToastUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;

public class QRCodeScannerActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERA = 1;
    private Toolbar toolbar;
    private SurfaceView mCameraPreview;
    private CameraSource mCameraSource;
    private boolean scanMultiQrCode = false;
    private String FROM_FRAGMENT;

    //LIST_CARGO_DETAIL_FRAGMENT
    private ArrayList<Integer> arrQrResult = new ArrayList<>();
    private ArrayList<String> data = new ArrayList<>();
    private String qrCodeResult;
    private List<CargoBillPackage> cargoBillPackages = new ArrayList<>();
    private static final String LIST_CARGO_DETAIL_FRAGMENT = "LIST_CARGO_DETAIL_FRAGMENT";

    //BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT
    private static final String BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT = "BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT";
    private BiddingVehicle biddingVehicle = new BiddingVehicle();
    private List<CargoTypes> listCargoTypes = new ArrayList<>();
    private int indexCargoTypesItem;
    private int quantityQrChecked = 0;
    private int totalQr;


    //LIST_ROUTING_VAN_DAY_ACTIVITY
    private static final String LIST_ROUTING_VAN_DAY_ACTIVITY = "LIST_ROUTING_VAN_DAY_ACTIVITY";
    private HashMap<Integer, HashMap<String, Boolean>> qrDataChecked = new HashMap<>();
    private RoutingDay routingPlan = new RoutingDay();
    private List<BillPackage> listBillPackage = new ArrayList<>();
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        setContentView(R.layout.fragment_scan);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mCameraPreview = findViewById(R.id.camera_view);
        toolbar = findViewById(R.id.toolbar);
        initToolbar();
        BarcodeDetector mBarcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();

        mCameraSource = new CameraSource.Builder(this, mBarcodeDetector).setFacing(
                CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .build();

        mCameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                try {
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mCameraSource.start(mCameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                mCameraSource.stop();
            }
        });
        getData();

        mBarcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> barcodeList = detections.getDetectedItems();
                if (barcodeList != null && barcodeList.size() > 0) {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                    qrCodeResult = barcodeList.valueAt(0).displayValue;
                    if (!scanMultiQrCode) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(StaticData.RESULT_SCAN, qrCodeResult);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        processResult();
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (scanMultiQrCode) {
                setResultQR(false);
            }
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mCameraSource.start(mCameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private void showDialogQrScan() {
        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:

                CargoBillPackageDialog cargoBillPackageDialog = new CargoBillPackageDialog();
                bundle.putSerializable(Constants.MODEL, cargoBillPackages.get(data.indexOf(qrCodeResult)));
                bundle.putSerializable(Constants.QR_CODE, qrCodeResult);
                cargoBillPackageDialog.setArguments(bundle);
                cargoBillPackageDialog.show(fm, "ABC");
                break;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                BillPackageDialog billPackageDialog = new BillPackageDialog(listBillPackage.get(index), qrCodeResult);
                billPackageDialog.show(fm, "ABC");
                break;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                CargoTypesDialog cargoTypesDialog = new CargoTypesDialog(listCargoTypes.get(indexCargoTypesItem), qrCodeResult);
                cargoTypesDialog.show(fm, "ABC");
                break;
        }
    }

    public Integer getIndex(String qrCode) {
        for (int i = 0; i < cargoBillPackages.size(); i++) {
            if (cargoBillPackages.get(i).getQr_char().equals(qrCode)) {
                return i;
            }
        }
        return -1;
    }

    private void processResult() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(StaticData.RESULT_SCAN, qrCodeResult);
        Integer resultScan = handleDataScan(qrCodeResult);
        if (resultScan == 1) {
            showToast(R.string.QrCode_has_been_scanned, R.drawable.ic_danger);
        } else if (resultScan == 2) {
            showToast(R.string.QrCode_result_wrong, R.drawable.ic_close16);
        } else if (resultScan == 3) {
            setResultQR(true);
            finish();
        } else {
            showDialogQrScan();
        }


    }

    /**
     * @param qrCode 0 : Đúng mã
     *               1 : Đã quét
     *               2 : Sai mã
     *               3 : Hoàn thành
     */
    private Integer handleDataScan(String qrCode) {
        Integer index = 0;
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:
                index = data.indexOf(qrCode);
                if (index < 0) {
                    return -1;
                } else if (arrQrResult.size() == 0 || arrQrResult.indexOf(index) < 0) {
                    arrQrResult.add(index);
                    if (arrQrResult.size() == data.size()) {
                        return 3;
                    }
                    return 0;
                }
                return 2;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                index = 0;
                for (BillPackage item : listBillPackage) {
                    Boolean check = qrDataChecked.get(item.getBill_package_id()).get(qrCode);
                    if (check != null) {
                        if (check) {
                            return 1;
                        } else {
                            qrDataChecked.get(item.getBill_package_id()).put(qrCode, true);
                            item.setQuantityQrChecked(item.getQuantityQrChecked() + 1);
                            if (checkComplete()) {
                                return 3;
                            }
                            this.index = index;
                            return 0;
                        }
                    }
                    index++;
                }
                return 2;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                indexCargoTypesItem = 0;
                for (CargoTypes item : listCargoTypes) {
                    // check da quet
                    if (item.getQrChecked() != null && item.getQrChecked().indexOf(qrCodeResult) >= 0) {
                        return 1;
                    }
                    //index update checked
                    index = 0;
                    // check chua quet
                    for (Cargo cargo : item.getCargos()) {

                        if (cargo.getQr_code().equals(qrCodeResult)) {
                            item.setQrChecked(item.getQrChecked() + qrCodeResult);
                            item.getCargos().get(index).setQrChecked(true);
                            quantityQrChecked++;
                            // check hoàn thánh
                            if (quantityQrChecked == totalQr) {
                                return 3;
                            }
                            return 0;
                        }
                        index++;
                    }
                    indexCargoTypesItem++;
                }
                return 2;
        }
        return null;
    }


    private boolean checkComplete() {
        for (BillPackage item : listBillPackage) {
            if (routingPlan.getType().equals(RoutingDayType.WAREHOUSE_IMPORT)) {
                if (item.getQuantity_export() - item.getQuantityQrChecked() != 0) {
                    return false;
                }
            } else {
                if (item.getQuantity_import() - item.getQuantityQrChecked() != 0) {
                    return false;
                }
            }
            item.setQrChecked(true);
        }

        return true;
    }

    private void getData() {
        try {
            Intent intent = getIntent();
            if (intent.hasExtra(Constants.SCAN_MULTI_QR_CODE)) {
                scanMultiQrCode = intent.getBooleanExtra(Constants.SCAN_MULTI_QR_CODE, false);
            }
            if (scanMultiQrCode && intent.hasExtra(Constants.FROM_FRAGMENT)) {
                FROM_FRAGMENT = intent.getStringExtra(Constants.FROM_FRAGMENT);
                switch (FROM_FRAGMENT) {
                    case LIST_CARGO_DETAIL_FRAGMENT:
                        arrQrResult.clear();
                        arrQrResult.addAll(intent.getIntegerArrayListExtra("QR_SCANED"));
                        data.addAll(intent.getStringArrayListExtra(Constants.DATA));
                        cargoBillPackages.addAll(intent.getParcelableArrayListExtra(Constants.MODEL));
                        break;
                    case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                        biddingVehicle = (BiddingVehicle) intent.getSerializableExtra(Constants.MODEL);
                        listCargoTypes.addAll(biddingVehicle.getCargo_types());
                        getTotalQrCodeBiddingCargoTypes(listCargoTypes);
                        break;
                    case LIST_ROUTING_VAN_DAY_ACTIVITY:
                        qrDataChecked = (HashMap<Integer, HashMap<String, Boolean>>) intent.getSerializableExtra(Constants.QR_DATA);
                        routingPlan = (RoutingDay) intent.getSerializableExtra(Constants.DATA);
                        listBillPackage.addAll(routingPlan.getList_bill_package());
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTotalQrCodeBiddingCargoTypes(List<CargoTypes> cargoTypes) {
        if (cargoTypes.size() > 0) {
            for (CargoTypes item : cargoTypes) {
                totalQr += item.getCargo_quantity();
            }
        }
    }

    // set Data result
    private void setResultQR(Boolean isSuccess) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:
                bundle.putIntegerArrayList(Constants.DATA_RESULT, arrQrResult);
                bundle.putBoolean(Constants.SUCCESS, isSuccess);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                biddingVehicle.getCargo_types().clear();
                biddingVehicle.getCargo_types().addAll(listCargoTypes);
                bundle.putSerializable(Constants.DATA_RESULT, biddingVehicle);
                bundle.putBoolean(Constants.SUCCESS, isSuccess);
                bundle.putInt(Constants.QUANTITY_QR_CHECKED, totalQr);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                bundle.putBoolean(Constants.SUCCESS, isSuccess);
                routingPlan.getList_bill_package().clear();
                routingPlan.getList_bill_package().addAll(listBillPackage);
                bundle.putSerializable(Constants.DATA, routingPlan);
                intent.putExtra(Constants.QR_DATA, qrDataChecked);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
        }
    }

    private void showToast(Integer toast, Integer icon) {
        Log.d("Show_toast", getResources().getString(toast));
        this.runOnUiThread(new Runnable() {
            public void run() {
                ToastUtils.showToast(QRCodeScannerActivity.this, getResources().getString(toast), getResources().getDrawable(icon));
            }
        });
    }

}
