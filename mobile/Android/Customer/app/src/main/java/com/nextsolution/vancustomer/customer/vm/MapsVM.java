package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.db.dto.BiddingPackage;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.Depot;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapsVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<RoutingDay> routingPlans;
    private ObservableField<RoutingDay> routingDay;
    private ObservableField<RoutingDay> driver;
    private LatLng currentRoutingLocation;
    private ObservableField<DistanceDTO> distanceDTO = new ObservableField<>();
    private String STATUS_DRIVER = "-1";
    Integer total_call_api_google = 0;
    //bidding cargo maps
    ObservableBoolean isBiddingCargoMaps= new ObservableBoolean();
    BiddingPackage biddingPackage;
    BiddingOrder biddingOrder;
    List<BiddingVehicle> biddingVehicle = new ArrayList<>();
    List<Vehicle> vehicles = new ArrayList<>();
    ObservableField<Depot> myDepot= new ObservableField<>();
    ObservableField<Depot> toDepot= new ObservableField<>();


    public MapsVM(@NonNull Application application) {
        super(application);
        isBiddingCargoMaps.set(false);
        routingPlans = new ArrayList<>();
        routingDay = new ObservableField<>();
        driver = new ObservableField<>();
    }

    /**
     * lấy danh sách routing plan
     *
     * @param runUi callBack
     */
    public void getDriver(Integer routingPlanDayId, RunUi runUi) {
        OrderApi.getDriverLocation(routingPlanDayId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDriverLocationSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDriverLocationSuccess(Object o, RunUi runUi) {
        OdooResultDto<RoutingDay> resultDto = (OdooResultDto<RoutingDay>) o;
        if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
            driver.set(resultDto.getRecords().get(0));
            driver.get().setStatus(STATUS_DRIVER);
            runUi.run(Constants.GET_DRIVER_LOCATION_SUCCESS);
        } else {
            runUi.run(Constants.GET_DATA_FAIL);
        }
    }

    public void getBiddingOrderDetail(int depotType,Integer biddingOrderId, RunUi runUi) {
        isLoading.set(true);
        DepotAPI.getBiddingCargoDetail(biddingOrderId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    biddingPackage = (BiddingPackage) o;
                    biddingOrder = biddingPackage.getBidding_order();
                    biddingVehicle.addAll(biddingOrder.getBidding_vehicles());
                    if(depotType==0){
                        myDepot.set(biddingOrder.getFrom_depot());
                        toDepot.set(biddingOrder.getTo_depot());
                    }else{
                        myDepot.set(biddingOrder.getTo_depot());
                        toDepot.set(biddingOrder.getFrom_depot());
                    }

                    getVehicles(biddingVehicle, runUi);
                } else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getVehicles(List<BiddingVehicle> biddingVehicle, RunUi runUi) {

        DepotAPI.getLocationVehicles(getVehicleId(biddingVehicle), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    getVehicles().addAll((Collection<? extends Vehicle>) o);
                    runUi.run(Constants.GET_DRIVER_LOCATION_SUCCESS);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private List<Integer> getVehicleId(List<BiddingVehicle> biddingVehicle) {
        if (biddingVehicle == null)
            return null;
        List<Integer> integers = new ArrayList<>();
        for (BiddingVehicle item : biddingVehicle) {
            integers.add(item.getVehicle_id());
        }
        return integers;
    }


    public void getRouting(String routingPlanDayCode, RunUi runUi) {
        Log.d("first_load", "getRouting: ");
        OrderApi.getRoutingDayPlanDetail(routingPlanDayCode, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        OdooResultDto<RoutingDay> resultDto = (OdooResultDto<RoutingDay>) o;
        try {
            if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                routingDay.set(resultDto.getRecords().get(0));
                runUi.run(Constants.GET_DATA_SUCCESS);
            } else {
                runUi.run(Constants.GET_DATA_FAIL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//

    /**
     * lấy danh sách lat long đường đi của tài xế để vẽ lộ trình
     *
     * @param start điểm bắt đầu
     * @param end   điểm kết thúc
     */
    public void getDestination(LatLng start, LatLng end) {
        RoutingApi.getDistanceByLatAndLong(start, end, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if(o!=null){
                    distanceDTO.set((DistanceDTO) o);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    /**
     * trả về location điểm đến tiếp theo
     *
     * @return
     */

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


}
