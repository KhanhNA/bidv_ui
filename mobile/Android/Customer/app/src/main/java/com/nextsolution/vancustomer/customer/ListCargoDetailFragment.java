package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.db.dto.CargoDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.vm.ListCargoDetailVM;
import com.nextsolution.vancustomer.databinding.ListCargoDetailFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import pub.devrel.easypermissions.EasyPermissions;

public class ListCargoDetailFragment extends BaseFragment {
    private final int REQUEST_PERMISSION_CAMERA = 1001;
    ListCargoDetailFragmentBinding mBinding;
    ListCargoDetailVM listCargoDetailVM;
    BaseAdapterV3 adapter;
    Integer index;
    boolean isGomCargo = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        listCargoDetailVM = (ListCargoDetailVM) viewModel;
        mBinding = (ListCargoDetailFragmentBinding) binding;
        setUpRecycleView();
        getData();
        listCargoDetailVM.getCargoDetail(this::runUI);
        return view;
    }

    public void getData() {
        Intent intent = getBaseActivity().getIntent();
        try {
            listCargoDetailVM.getCargoDto().set((CargoDTO) intent.getSerializableExtra(Constants.DATA));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUpRecycleView() {
        adapter = new BaseAdapterV3(R.layout.list_cargo_detail_item, listCargoDetailVM.getCargoBillPackages(), this);
        mBinding.rcListCargoDetail.setAdapter(adapter);

    }

    public void gomCargo() {
        if (listCargoDetailVM.checkValidate()) {
            listCargoDetailVM.gomCargo(this::runUI);
        } else {
            ToastUtils.showToast(getActivity(), getString(R.string.Scan_qr_code_not_yet_complete), getResources().getDrawable(R.drawable.ic_fail));
        }

    }

    public void runUI(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_FAIL:
                ToastUtils.showToast(getActivity(), getString(R.string.NOT_FOUND_DATA), getResources().getDrawable(R.drawable.ic_fail));
                break;
            case Constants.GET_DATA_SUCCESS:
                adapter.notifyDataSetChanged();
                break;
            case Constants.GOM_CARGO_SUCCESS:
                mBinding.txtStatus.setText(getResources().getString(R.string.Packaged));
                mBinding.txtStatus.setTextColor(getResources().getColor(R.color.green_text));
                ToastUtils.showToast(getActivity(), getString(R.string.GOM_CARGO_SUCCESS), getResources().getDrawable(R.drawable.ic_access));
                isGomCargo = true;
                break;
            case Constants.GOM_CARGO_FAIL:
                ToastUtils.showToast(getActivity(), getString(R.string.GOM_CARGO_FAIL), getResources().getDrawable(R.drawable.ic_fail));
                break;
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Constants.RESULT_OK;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Constants.DATA_RESULT)) {
            boolean isSuccess = data.getBooleanExtra(Constants.SUCCESS, false);
            if (isSuccess) {
                ToastUtils.showToast(getActivity(), getResources().getString(R.string.SUCCESS), getResources().getDrawable(R.drawable.ic_access));
            }
            listCargoDetailVM.setIndexQrResult(data.getIntegerArrayListExtra(Constants.DATA_RESULT));
            checkedQr();
        } else if (requestCode == Constants.REQUEST_CODE && resultCode == Constants.RESULT_OK) {
            if (data.getBooleanExtra(Constants.DATA, false)) {
                listCargoDetailVM.getCargoBillPackages().get(index).checked = false;
                adapter.notifyItemChanged(index);
            }
        }
        closeProcess();
    }

    public void checkedQr() {
        for (Integer i : listCargoDetailVM.getIndexQrResult()) {
            listCargoDetailVM.getCargoBillPackages().get(i).checked = true;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View v, Object o) {
        index = ((CargoBillPackage) o).index - 1;
        if (((CargoBillPackage) o).checked != null && ((CargoBillPackage) o).checked) {
            String description = getResources().getString(R.string.description_uncheck_qr_code) + " " + ((CargoBillPackage) o).getQr_char() + " ?";
            ConfirmDialog confirmDialog = new ConfirmDialog(R.string.title_uncheck_qr_code, description);
            confirmDialog.setTargetFragment(this, Constants.REQUEST_CODE);
            confirmDialog.show(getChildFragmentManager(), "ABC");

        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.qrCode) {
            String[] perms = {Manifest.permission.CAMERA};
            if (EasyPermissions.hasPermissions(getActivity(), perms)) {
                gotoQrScanActivity();
            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                        REQUEST_PERMISSION_CAMERA, perms);
            }
        } else if (view.getId() == R.id.toolbar) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("isGomCargo", isGomCargo);
            getBaseActivity().setResult(Constants.RESULT_OK, returnIntent);
            getBaseActivity().finish();
        }
    }

    private void gotoQrScanActivity() {
        Intent qrScan = new Intent(getContext(), QrScannerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.FROM_FRAGMENT, "LIST_CARGO_DETAIL_FRAGMENT");
        bundle.putBoolean(Constants.SCAN_MULTI_QR_CODE, true);
        bundle.putIntegerArrayList("QR_SCANED", listCargoDetailVM.getIndexQrResult());
        bundle.putParcelableArrayList(Constants.MODEL, listCargoDetailVM.getArrCargoBillPackage());
        bundle.putStringArrayList(Constants.DATA, listCargoDetailVM.getQrCode());
        qrScan.putExtras(bundle);
        startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            gotoQrScanActivity();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_cargo_detail_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListCargoDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcListCargoDetail;
    }
}
