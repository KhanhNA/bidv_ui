package com.nextsolution.db.dto;

import com.ns.odoolib_retrofit.model.OdooRelType;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area implements Cloneable, Serializable {
    private Integer id;
    private OdooRelType country_id;
    private String name;
    private String code;
    private String province_name;
    private OdooRelType zone_area_id;

    private Hub hubInfo;
    private Zone zoneInfo;
}
