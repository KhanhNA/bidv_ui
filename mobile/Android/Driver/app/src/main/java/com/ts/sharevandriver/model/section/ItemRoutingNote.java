package com.ts.sharevandriver.model.section;

import com.chad.library.adapter.base.entity.node.BaseNode;


import java.io.Serializable;
import java.util.List;

import androidx.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRoutingNote extends BaseNode implements Serializable {
    private Integer id;
    private String routing_plan_day_code;
    private Integer orderNumber;
    private String bill_routing_name;
    private String type;
    private String trouble_type;
    private Integer status;
    private Float lat;
    private Float lng;
    private boolean check_point;
    public boolean checked;
    public int position;//


    public ItemRoutingNote(Integer id, String routing_plan_day_code, Integer orderNumber, String bill_routing_name,
                           String type, Integer status, Float lat, Float lng, Boolean check_point,String trouble_type) {
        this.id = id;
        this.bill_routing_name = bill_routing_name;
        this.orderNumber = orderNumber;
        this.routing_plan_day_code = routing_plan_day_code;
        this.type = type;
        this.status = status;
        this.lat = lat;
        this.lng = lng;
        this.check_point = check_point;
        this.trouble_type=trouble_type;
    }

    @Nullable
    @Override
    public List<BaseNode> getChildNode() {
        return null;
    }
    // End


}