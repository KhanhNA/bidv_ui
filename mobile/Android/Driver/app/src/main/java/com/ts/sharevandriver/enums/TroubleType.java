package com.ts.sharevandriver.enums;

public class TroubleType {
    public static String NORMAL = "0";
    public static String SOS = "1";
    public static String RETRY = "2";
    public static String RETURN = "3";
    private static String PICKUP_FAIL = "4";
}
