package com.ts.sharevandriver.base;

public interface RunUi {
    void run(Object... params);
}
