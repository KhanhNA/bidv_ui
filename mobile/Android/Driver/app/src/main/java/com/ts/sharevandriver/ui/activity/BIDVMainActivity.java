package com.ts.sharevandriver.ui.activity;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.ActivityMainBidvBinding;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class BIDVMainActivity extends BaseActivity<ActivityMainBidvBinding> {
    @Override
    public int getLayoutRes() {
        return R.layout.activity_main_bidv;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
