package com.ts.sharevandriver.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.api.CustomerApi;
import com.ts.sharevandriver.api.SharingOdooResponse;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.ChangingPassword;
import com.tsolution.base.BaseViewModel;

import java.lang.ref.WeakReference;

import lombok.Getter;
import lombok.Setter;

import static com.ts.sharevandriver.utils.StringUtils.isNullOrEmpty;


@Getter
@Setter
public class ChangePasswordVM extends BaseViewModel {

    private ObservableField<ChangingPassword> changingPassword = new ObservableField<>();
    ObservableBoolean isLoading = new ObservableBoolean(false);
    public ChangePasswordVM(@NonNull Application application) {
        super(application);
        changingPassword.set(new ChangingPassword());

    }

    private WeakReference<Context> context;

    public void setContext(Context context) {
        this.context = new WeakReference<>(context);
    }


    public void changePassword(RunUi runUi){
        if(isValid()) {
            isLoading.set(true);
            ChangingPassword changeRequest = changingPassword.get();
            CustomerApi.changePassword(changeRequest.getOldPassword(), changeRequest.getNewPassword(), new SharingOdooResponse() {
                @Override
                public void onSuccess(Object o) {
                    getSuccess(o, runUi);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        }
    }

    public void getSuccess(Object o,RunUi runUi){
        isLoading.set(false);
        try{
            Boolean result = (Boolean) o;
            if(result){
                runUi.run(Constants.SUCCESS_API);
            }else{
                addError("oldPass", context.get().getString(R.string.INCORRECT_CURRENT_PASSWORD), true);
            }
        }catch (Exception e){
            addError("oldPass", context.get().getString(R.string.INCORRECT_CURRENT_PASSWORD), true);
            e.printStackTrace();
        }
    }

    private boolean isValid() {
        ChangingPassword temp = changingPassword.get();
        boolean isValid = true;
        if (temp != null) {
            if (isNullOrEmpty(temp.getOldPassword())) {
                isValid = false;
                addError("oldPass", context.get().getString(R.string.please_enter_password), true);
            } else {
                clearErro("oldPass");
            }

            if (isNullOrEmpty(temp.getNewPassword())) {
                isValid = false;
                addError("newPass", context.get().getString(R.string.please_enter_new_password), true);
            } else if (temp.getNewPassword().length() < 6) {
                isValid = false;
                addError("newPass", context.get().getString(R.string.PASSWORD_TOO_SHORT), true);
            } else if (isTheSamePassword(temp.getOldPassword(), temp.getNewPassword())) {
                isValid = false;
                addError("newPass", context.get().getString(R.string.SAME_PASSWORD), true);
            } else {
                clearErro("newPass");
            }


            if (!isTheSamePassword(temp.getNewPassword(), temp.getNewConfirmPassword())) {
                isValid = false;
                addError("confirmPass", context.get().getString(R.string.incorrect_password), true);
            } else {
                clearErro("confirmPass");
            }
        }
        return isValid;
    }

    private boolean isTheSamePassword(String pass1, String pass2) {
        return !isNullOrEmpty(pass1) && !isNullOrEmpty(pass2) && pass1.equals(pass2);
    }
}
