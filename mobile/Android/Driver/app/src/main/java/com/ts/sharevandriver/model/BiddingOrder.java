package com.ts.sharevandriver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingOrder extends BaseModel {
    private Integer id;
    private Integer bidding_order_id;
    private String from_expected_time;
    private String to_expected_time;
    private String depot_id;
    private OdooDateTime actual_time;
    private String stock_man_id;
    private String status;
    private String description;
    private String create_date;
    private String write_date;
    private Integer bidding_order_vehicle_id;
}