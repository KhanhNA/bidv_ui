package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.tsolution.base.BaseViewModel;

public class MainActivityVM extends BaseViewModel {
    public MainActivityVM(@NonNull Application application) {
        super(application);
    }
}
