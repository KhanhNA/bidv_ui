package com.ts.sharevandriver.api;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.luck.picture.lib.entity.LocalMedia;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.SOService;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.Driver;
import com.ts.sharevandriver.model.DriverDistance;
import com.ts.sharevandriver.model.DriverReward;
import com.ts.sharevandriver.model.Equipment;
import com.ts.sharevandriver.model.StatisticalWalletDTO;
import com.ts.sharevandriver.model.Vehicle;
import com.ts.sharevandriver.model.WalletDTO;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverApi extends BaseApi {

    private static String GET_WALLET_DRIVER = "/market/get_driver_wallet";
    private static String GET_DRIVER_COMPUTE = "/market/get_driver_compute";
    private static String GET_TOTAL_AMOUNT = "/market/get_driver_last_money";
    private static String UPDATE_RATING_CUSTOMER_CHECK = "/routing_plan_day/update_rating_customer_check";

    public static void updateRatingCustomerCheck(Integer routing_plan_day_id,IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id",routing_plan_day_id);
            mOdoo.callRoute(UPDATE_RATING_CUSTOMER_CHECK, params, new SharingOdooResponse<Long>() {

                @Override
                public void onSuccess(Long aLong) {
                    if(aLong!=null){
                        result.onSuccess(true);
                    }else{
                        result.onSuccess(false);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
    public static void getTotalAmount(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_TOTAL_AMOUNT, params, new SharingOdooResponse<Double>() {

                @Override
                public void onSuccess(Double aDouble) {
                    result.onSuccess(aDouble);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getWalletDriver(String receive_type, Integer month, Integer year, int offset, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("receive_type", receive_type);
            params.put("month", month);
            params.put("year", year);
            params.put("offset", offset);
            params.put("limit", 10);

            mOdoo.callRoute(GET_WALLET_DRIVER, params, new SharingOdooResponse<OdooResultDto<WalletDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<WalletDTO> walletDTOOdooResultDto) {
                    if (walletDTOOdooResultDto != null && walletDTOOdooResultDto.getTotal_record() > 0) {
                        result.onSuccess(walletDTOOdooResultDto);
                    } else {
                        result.onSuccess(null);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getStatisticalWallet(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_DRIVER_COMPUTE, params, new SharingOdooResponse<OdooResultDto<StatisticalWalletDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<StatisticalWalletDTO> resultDto) {
                    if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                        result.onSuccess(resultDto);
                    } else {
                        result.onSuccess(null);
                    }
                }


                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void updateToken(String fcmToken, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("fcm_token", fcmToken);

            mOdoo.callRoute("/server/save_token", params, new SharingOdooResponse<OdooResultDto<String>>() {
                @Override
                public void onSuccess(OdooResultDto<String> obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getDriverReward(IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("driver_id", StaticData.getDriver().getId());

            mOdoo.callRoute("/fleet_vehicle/get_driver_rating", params, new SharingOdooResponse<OdooResultDto<DriverReward>>() {
                @Override
                public void onSuccess(OdooResultDto<DriverReward> obj) {
                    if (obj != null && obj.getRecords() != null && obj.getRecords().size() > 0) {
                        response.onSuccess(obj.getRecords().get(0));
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getVansInfo(Integer log_id, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("assignation_log_id", log_id);

            mOdoo.callRoute("/fleet_vehicle/info_vehicle", params, new SharingOdooResponse<OdooResultDto<Vehicle>>() {
                @Override
                public void onSuccess(OdooResultDto<Vehicle> obj) {
                    if (obj != null && obj.getRecords() != null && obj.getRecords().size() > 0) {
                        response.onSuccess(obj.getRecords().get(0));
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void acceptComing(Integer routing_plan_day_id, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id", routing_plan_day_id);

            mOdoo.callRoute("/routing_plan_day/accept_comming", params, new SharingOdooResponse<Integer>() {
                @Override
                public void onSuccess(Integer obj) {
                    if(obj != null){
                        response.onSuccess(obj);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    /**
     * @param description     mô tả
     * @param lstFileSelected danh sách ảnh
     * @param result          callback
     */
    public static void updateVanStatus(String description, List<LocalMedia> lstFileSelected, List<Equipment> equipments, Location location, String status, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part[] parts = null;
        String equipmentJson = new Gson().toJson(equipments);
        String json = "{\"vehicle_id\":" + StaticData.getDriver().getVehicle().getId()
                + ", \"driver_id\":" + StaticData.getDriver().getId()
                + ", \"status\": \"" + status + "\""
                + ", \"assignation_log_id\":" + StaticData.getDriver().getAssignation_log().getId()
                + ", \"latitude\":" + location.getLatitude()
                + ", \"longitude\":" + location.getLongitude()
                + ", \"description\":\"" + description + " " + "\""
                + ", \"equipments\" : " + equipmentJson + "}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("ufile", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        Call<ApiResponseModel> call = soService.updateVanStatus(mOdoo.getSessionCokie()
                , bodyJson, parts);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                if (response.body() != null) {
                    result.onSuccess(response.body());
                } else {
                    result.onFail(new Throwable());

                }
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                result.onFail(t);
            }
        });
    }

    public static void getVehicleHistory(String date, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("date", date);

            mOdoo.callRoute("/fleet/history_vehicle", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
                @Override
                public void onSuccess(OdooResultDto<Driver> obj) {
                    if (obj != null && obj.getRecords() != null) {
                        response.onSuccess(obj.getRecords());
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getDirection(LatLng from, LatLng to, IResponse response) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("from_latitude", from.latitude);
            params.put("from_longitude", from.longitude);
                params.put("to_latitude", to.latitude);
            params.put("to_longitude", to.longitude);

            mOdoo.callRoute("/routing_plan_day/check_driver_waiting_time", params, new SharingOdooResponse<DriverDistance>() {
                @Override
                public void onSuccess(DriverDistance obj) {
                    if (obj != null) {
                        response.onSuccess(obj);
                    } else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }


    public static void logout() {

        SOService soService = mOdoo.getClient().createService(SOService.class);

        Call<ResponseBody> call = soService.logOut(mOdoo.getSessionCokie(), StaticData.getOdooSessionDto().getUid() + "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

    }


}
