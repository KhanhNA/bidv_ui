package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableDouble;
import androidx.databinding.ObservableField;

import com.ts.sharevandriver.model.BillPackage;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillPackageDialogVM extends BaseViewModel {
    ObservableField<BillPackage> billPackage= new ObservableField<>();
    ObservableField<String> type= new ObservableField<>();
    public BillPackageDialogVM(@NonNull Application application) {
        super(application);
    }
}
