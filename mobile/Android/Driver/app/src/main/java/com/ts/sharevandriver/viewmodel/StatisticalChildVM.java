package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Observable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.api.SharingOdooResponse;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.StatisticalWalletDTO;
import com.ts.sharevandriver.model.WalletDTO;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticalChildVM extends BaseViewModel {
    ObservableBoolean isRefresh = new ObservableBoolean();
    ObservableBoolean emptyData= new ObservableBoolean();

    public StatisticalChildVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
    }
}
