package com.ts.sharevandriver.model.section;

import com.chad.library.adapter.base.entity.node.BaseExpandNode;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.entity.node.NodeFooterImp;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RootNode extends BaseExpandNode implements Serializable {

    private List<BaseNode> childNode;
    private String warehouseName;
    private String warehouseAddress;
    private String warehousePhone;
    private Float lat;
    private Float lng;
    private String lat_lng; // "lat,lng"
    private boolean status=true; // status : false chưa hoàn thành, true đã hoàn thành

    public RootNode(List<BaseNode> childNode, String warehouseName, String warehouseAddress, String warehousePhone) {
        this.childNode = childNode;
        this.warehouseName = warehouseName;
        this.warehouseAddress = warehouseAddress;
        this.warehousePhone = warehousePhone;
    }

    public RootNode(List<BaseNode> childNode, String warehouseName, String warehouseAddress, String warehousePhone, Float lat, Float lng,String lat_lng) {
        this.childNode = childNode;
        this.warehouseName = warehouseName;
        this.warehouseAddress = warehouseAddress;
        this.warehousePhone = warehousePhone;
        this.lat = lat;
        this.lng = lng;
        this.lat_lng=lat_lng;
    }
    public void setStatus()
    {
        this.status=false;
    }
    public boolean getStatus()
    {
        return this.status;
    }

    /**
     * {@link BaseNode}
     *
     * @return child nodes
     */
    @Nullable
    @Override
    public List<BaseNode> getChildNode() {
        return childNode;
    }

    /**
     * {@link NodeFooterImp}
     * （可选实现）
     * @return
     */
//    @Nullable
//    @Override
//    public BaseNode getFooterNode() {
//        return new RootFooterNode("...");
//    }
}
