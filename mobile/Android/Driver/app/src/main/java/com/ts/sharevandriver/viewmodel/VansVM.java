package com.ts.sharevandriver.viewmodel;

import android.app.Application;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.luck.picture.lib.entity.LocalMedia;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.CustomerApi;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.Driver;
import com.ts.sharevandriver.model.Equipment;
import com.ts.sharevandriver.model.Vehicle;
import com.ts.sharevandriver.utils.ToastUtils;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VansVM extends BaseViewModel {
    public ObservableBoolean isNotEmptyImage = new ObservableBoolean();
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableField<Vehicle> vehicle = new ObservableField<>();
    private List<LocalMedia> lstFileSelected;

    public VansVM(@NonNull Application application) {
        super(application);
        vehicle.set(new Vehicle());

    }

    public void getVansInfo(Integer log_id, RunUi runUi) {
        isLoading.set(true);
        DriverApi.getVansInfo(log_id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                vehicle.set((Vehicle) o);
                setData(((Vehicle) o).getList_image());
                StaticData.getDriver().setVehicle(vehicle.get());
                runUi.run("getVansSuccess");
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getVansSuccessFail");
            }
        });
    }

    public void updateVanStatus(String description, Location location, String status, RunUi runUi) {
        List<Equipment> equipments = new ArrayList<>();
        for (Equipment equipment : vehicle.get().getEquipments()) {
            if (equipment.isIsselect() && equipment.getQuantity_take() > 0) {
                equipment.setAssignation_log_id(StaticData.getDriver().getAssignation_log().getId());
                equipments.add(equipment);
            }
            if (equipment.getQuantity_take() < equipment.getQuantity_return()) {
                ToastUtils.showToast("some thing wrong !");
                return;
            }
        }
        isLoading.set(true);
        DriverApi.updateVanStatus(description, lstFileSelected, equipments, location, status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                ApiResponseModel apiResponseModel = (ApiResponseModel) o;
                if (apiResponseModel.status == 200) {
                    runUi.run("updateSuccess");
                    StaticData.getDriver().getVehicle().setStatus(status);
                } else if (apiResponseModel.status == 205) {//status code thể cần phải hoàn thành hết tuyến
                    runUi.run("needToCompleteRout");
                } else if (apiResponseModel.status == 1001) {
                    runUi.run("needToFinishCalendar", apiResponseModel.message);
                } else {
                    runUi.run("updateFail", apiResponseModel.message);
                }
                isLoading.set(false);

            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("updateFail", error.getMessage());
                isLoading.set(false);
            }
        });
    }


    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }

    public void getUserInfo(RunUi runUi) {
        CustomerApi.getDriver(new IResponse<OdooResultDto<Driver>>() {
            @Override
            public void onSuccess(OdooResultDto<Driver> o) {
                if (o != null && o.getRecords().size() > 0) {
                    Driver driver = o.getRecords().get(0);
                    StaticData.setDriver(driver);
                    vehicle.set(StaticData.getDriver().getVehicle());
                    runUi.run("Get Info Success");
                }
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }
}
