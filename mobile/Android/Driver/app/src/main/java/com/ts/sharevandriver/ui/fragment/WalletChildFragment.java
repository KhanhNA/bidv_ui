package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;

import com.bigkoo.pickerview.MyOptionsPickerView;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.BaseAdapterV3;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.WalletChildFragmentBinding;
import com.ts.sharevandriver.model.StatisticalWalletDTO;
import com.ts.sharevandriver.model.WalletDTO;
import com.ts.sharevandriver.viewmodel.WalletChildVM;
import com.ts.sharevandriver.widget.CustomLoadMoreView;
import com.ts.sharevandriver.widget.OnSingleClickListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.Calendar;

public class WalletChildFragment extends BaseFragment {
    WalletChildVM walletChildVM;
    WalletChildFragmentBinding mBinding;
    BaseAdapterV3 adapter;
    MyOptionsPickerView singlePicker;
    StatisticalWalletDTO statisticalWalletDTO= new StatisticalWalletDTO();
    ListDialogFragment listDialogFragment;
    Button btnConfirm;
    Button btnCancel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        walletChildVM = (WalletChildVM) viewModel;
        mBinding = (WalletChildFragmentBinding) binding;
        initView();
        getData();
        initLoadMore();
        return view;
    }

    private void initView() {
        mBinding.swRefresh.setOnRefreshListener(() -> {
            walletChildVM.getIsRefresh().set(true);
            walletChildVM.getData(false, this::runUi);
        });
        setUpRecycleView();

        //set up Picker
        singlePicker = new MyOptionsPickerView(getContext());
        ArrayList<String> items = new ArrayList<String>();
        for (String item : walletChildVM.getMonth_history()) {
            items.add(item);
        }
        singlePicker.setPicker((ArrayList) walletChildVM.getMonth_history());
        singlePicker.setTitle(getString(R.string.month));
        btnConfirm = (Button) singlePicker.getBtnSubmit();
        btnConfirm.setText(getString(R.string.select));
        btnConfirm.setTextSize(14);
        btnConfirm.setTextColor(getResources().getColor(R.color.colorPrimary));

        btnCancel = (Button) singlePicker.getBtnCancel();
        btnCancel.setText(getString(R.string.cancel));
        btnCancel.setTextSize(14);
        btnCancel.setTextColor(getResources().getColor(R.color.color_price));

        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(0);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                handleData(items.get(options1));
            }
        });

        //set up toolbar
        mBinding.toolbar.setTitle(R.string.Transaction_history);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handleData(String str) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(walletChildVM.getDateHashMap().get(str));
        walletChildVM.getMonth().set(cal.get(Calendar.MONTH) + 1);
        walletChildVM.getYear().set(cal.get(Calendar.YEAR));
        mBinding.btnChooseDate.setText(getString(R.string.month) + " " + str);
        walletChildVM.getData(false, this::runUi);
    }

    private void getData() {
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(Constants.ONLY_READ)) {
            walletChildVM.getOnlyRead().set(intent.getBooleanExtra(Constants.ONLY_READ, false));
            if (walletChildVM.getOnlyRead().get()) {
                if (intent.hasExtra("receive_type")) {
                    walletChildVM.setReceive_type(intent.getStringExtra("receive_type"));
                }
                if (intent.hasExtra("month")) {
                    walletChildVM.getMonth().set(intent.getIntExtra("month", 0));
                }
                if (intent.hasExtra("year")) {
                    walletChildVM.getYear().set(intent.getIntExtra("year", 0));
                }
                if (intent.hasExtra(Constants.DATA_PASS_FRAGMENT)) {
                    statisticalWalletDTO = (StatisticalWalletDTO) intent.getSerializableExtra(Constants.DATA_PASS_FRAGMENT);
                    mBinding.lbMonth.setText(getString(R.string.month)+" "+statisticalWalletDTO.getCreateDateStr());
                    reView();
                }
            }
        }

        walletChildVM.getData(false, this::runUi);
    }
    public void reView(){
        if(walletChildVM.getReceive_type().equals("0")){
            mBinding.lbMoney.setText(statisticalWalletDTO.getTotal_saveStr());
            mBinding.lbMoney.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.arrow_down_gray),null,getResources().getDrawable(R.drawable.ic_arrow_down_receive),null);
        }else if(walletChildVM.getReceive_type().equals("1")){
            mBinding.lbMoney.setText(statisticalWalletDTO.getTotal_payStr());
            mBinding.lbMoney.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.arrow_down_gray),null,getResources().getDrawable(R.drawable.ic_arrow_up_accent),null);
        }else{
            mBinding.lbMoney.setText(getString(R.string.total_revenue_and_expenditure)+": "+statisticalWalletDTO.getTotal_amountStr());
            mBinding.lbMoney.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.arrow_down_gray),null,null,null);
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                walletChildVM.getEmptyData().set(false);
                adapter.getLoadMoreModule().loadMoreComplete();
                adapter.notifyDataSetChanged();
                break;
            case "noMore":
                adapter.getLoadMoreModule().loadMoreEnd();
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnChooseDate) {
            singlePicker.show();
        }else if(view.getId()==R.id.lbMoney){
            openDialogSelectType(view);
        }
    }
    @SuppressLint("RestrictedApi")
    private void openDialogSelectType(View view) {
//Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popuo_menu_select_reveice_type, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            String type="";
            switch (item.getItemId()) {
                case R.id.nvReceive:
                    type="0";
                    break;
                case R.id.nvUse:
                    type="1";
                    break;
                case R.id.nvTotal:
                    type="2";
                    break;
            }
            if (!type.equals(walletChildVM.getReceive_type())) {
                walletChildVM.setReceive_type(type);
                reView();
                walletChildVM.getData(false, this::runUi);
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            walletChildVM.loadMore(this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void setUpRecycleView() {
        adapter = new BaseAdapterV3(R.layout.wallet_child_fragment_item, walletChildVM.getWalletDTOS(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                WalletDTO walletDTO= (WalletDTO) o;
                DialogWalletTransactionHistory dialog = new DialogWalletTransactionHistory(walletDTO);
                dialog.show(getChildFragmentManager(),"abc");
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcContent.setAdapter(adapter);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.wallet_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WalletChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }
}
