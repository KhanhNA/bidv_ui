package com.ts.sharevandriver.model;

import com.ns.odoolib_retrofit.model.OdooRelType;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area extends BaseModel {
    private Integer id;
    private String name;
    private String code;
}
