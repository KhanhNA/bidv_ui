package com.ts.sharevandriver.api;

import android.content.Context;

import com.google.gson.Gson;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.BaseClient;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.MyCallBack;
import com.ts.sharevandriver.base.SOSocketService;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.Area;
import com.ts.sharevandriver.model.DriverMarketInfo;
import com.ts.sharevandriver.model.SocketId;
import com.ts.sharevandriver.utils.TsUtils;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.net.Socket;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MarketPlaceApi extends BaseApi {

    public static void getAreas(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("location_type", "province");

            mOdoo.callRoute("/market_place/get_area", params, new SharingOdooResponse<OdooResultDto<Area>>() {
                @Override
                public void onSuccess(OdooResultDto<Area> obj) {
                    if(obj != null && TsUtils.isNotNull(obj.getRecords())){
                        result.onSuccess(obj.getRecords());
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void registerInfo(DriverMarketInfo driverMarketInfo, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routingDriver", new JSONObject(mGson.toJson(driverMarketInfo)));

            mOdoo.callRoute("/market_place/assign_routing_driver", params, new SharingOdooResponse<Integer>() {
                @Override
                public void onSuccess(Integer obj) {
                    if(obj != null){
                        result.onSuccess(obj);
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void setSocketId(Context context, String socketId, IResponse response){
        BaseClient<SOSocketService> soService = new BaseClient(context, AppController.URL_SOCKET_, SOSocketService.class);

        SocketId socket = new SocketId();
        socket.setId(StaticData.getOdooSessionDto().getUid());
        socket.setLogin(StaticData.getOdooSessionDto().getUsername());
        socket.setSocketId(socketId);

        soService.getServices().setSocketId("Bearer " + StaticData.getOdooSessionDto().getAccess_token(),socket).enqueue(new MyCallBack<>(response));
    }

    public static void getDriverInfo(IResponse response){
        JSONObject params;
        try {
            params = new JSONObject();
            mOdoo.callRoute("/market_place/get_driver_assign", params, new SharingOdooResponse<OdooResultDto<DriverMarketInfo>>() {
                @Override
                public void onSuccess(OdooResultDto<DriverMarketInfo> obj) {
                    if(obj != null && obj.getRecords() != null && obj.getRecords().size() > 0){
                        response.onSuccess(obj.getRecords());
                    }else {
                        response.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    response.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void acceptOrder(Integer bidding_package_id, boolean isAccept, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bidding_package_id", bidding_package_id);
            params.put("type", isAccept);

            mOdoo.callRoute("/driver/market_place_confirm_order", params, new SharingOdooResponse<ApiResponseModel>() {
                @Override
                public void onSuccess(ApiResponseModel obj) {
                    if(obj != null ){
                        result.onSuccess(obj);
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

}
