package com.ts.sharevandriver.ui.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.databinding.ProgressFragmentBinding;
import com.ts.sharevandriver.viewmodel.ProgressVM;

public class ProgressFragment extends DialogFragment {


    ProgressFragmentBinding mBinding;
    ProgressVM progressVM;
    IProgressFragment iProgressFragment;
    String messenger;
    CountDownTimer countDownTimer;
    int timeMin = 0;

    public ProgressFragment(String messenger, IProgressFragment iProgressFragment) {
        this.iProgressFragment = iProgressFragment;
        this.messenger = messenger;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.progress_fragment, container, false);
        progressVM = ViewModelProviders.of(this).get(ProgressVM.class);
        mBinding.setViewModel(progressVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        mBinding.textMessenger.setText(messenger);
        autoFinishFragment();
        return mBinding.getRoot();
    }

    public void autoFinishFragment() {
        if (messenger.equals(getString(R.string.get_your_loaction))) {
            countDownTimer = new CountDownTimer(10000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timeMin += 1000;
                    LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Log.d("location", "SS " + manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && AppController.getInstance().checkHighAccuracyLocationMode() && timeMin > 5000) {
                        Log.d("location", "SS");
                        iProgressFragment.handleProgress();
                        countDownTimer.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    iProgressFragment.handleProgress();
                }
            };
            countDownTimer.start();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.bottomSheetStyleWrapper);
    }

    public interface IProgressFragment {
        void handleProgress();
    }
}
