package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatingBadges extends BaseModel {
    private Integer id;
    private String name;
    private String description;
    private String image;
    private String rating_level;// Badges ứng với số sao rating
    private boolean check=false;
}