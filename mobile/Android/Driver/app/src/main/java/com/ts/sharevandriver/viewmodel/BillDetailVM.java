package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import com.luck.picture.lib.entity.LocalMedia;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.api.RoutingApi;
import com.ts.sharevandriver.api.SharingOdooResponse;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.enums.RoutingType;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.BillPackage;
import com.ts.sharevandriver.model.Driver;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.Warehouse;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillDetailVM extends BaseViewModel {
    private ObservableBoolean isRequire = new ObservableBoolean(); // kiểm tra có đúng theo thứ tự
    private ObservableBoolean checked = new ObservableBoolean();
    ObservableBoolean rated = new ObservableBoolean();
    public ObservableBoolean isNotEmptyImage = new ObservableBoolean();
    public ObservableBoolean isEdit = new ObservableBoolean();// biến kiếm tra có đang trong trạng thái thay đổi hay ko
    public ObservableBoolean isShowQR = new ObservableBoolean();
    private List<LocalMedia> lstFileSelected;
    private List<String> listImageRouting= new ArrayList<>();

    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableBoolean isRatingCustomer = new ObservableBoolean();
    public ObservableField<ShareVanRoutingPlan> routingPlan = new ObservableField<>();
    public ShareVanRoutingPlan routingPlanClone = new ShareVanRoutingPlan(); // lưu giá trị clone đẩy sang màn hình quét qr
    private boolean isValidQrSo;//biến kiểm tra qr_so sau khi quét có hợp lệ hay ko.
    private List<LocalMedia> listImageCancelOrder = new ArrayList<>();
    private List<String> listImageNotContactCustomer = new ArrayList<>();
    private String description_cancel;
    private List<Integer> listRoutingPlanId = new ArrayList<>();
    private Integer type;// thời gian dự kiến quay lại 0: quay lại, 1: trả về kho
    private String time_notContactCustomer;


    private List<BillPackage> packageList = new ArrayList<>();//list bill Package clone để báo cáo sai số.
    public ObservableField<HashMap<String, Boolean>> mapEditPackage = new ObservableField<>();//map những billPacakge đã được chỉnh sửa.


    /**
     * hashMap<bill_package_id,quantityQrChecked>
     * id bill_package
     * quantityQrChecked số qr bill_package tương ứng đã quét
     */
    private HashMap<Integer, HashMap<String, Boolean>> qrDataChecked = new HashMap<>();

    public BillDetailVM(@NonNull Application application) {
        super(application);
        mapEditPackage.set(new HashMap<>());
        routingPlan.set(new ShareVanRoutingPlan());
        checked.set(false);
        isRequire.set(false);
        isRatingCustomer.set(false);
    }

    public void cancelOrder(RunUi runUi) {
        isLoading.set(true);
        RoutingApi.cancelRoutingPlanDay(description_cancel, listImageCancelOrder, routingPlan.get().getBill_lading_detail_id(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    runUi.run(Constants.CANCEL_ORDER_SUCCESS, 3);
                } else {
                    runUi.run(Constants.CANCEL_ORDER_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void notContactCustomer(RunUi runUi) {
        isLoading.set(true);
        RoutingApi.notContactCustomer(listImageNotContactCustomer, listRoutingPlanId, type, time_notContactCustomer, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    runUi.run(Constants.NOT_CONTACT_CUSTOMER_SUCCESS,4);
                } else {
                    runUi.run(Constants.NOT_CONTACT_CUSTOMER_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }


    public void getBillDetail(String routingCode, RunUi runUi) {
        isLoading.set(true);
        listImageRouting.clear();
        RoutingApi.getRoutingDetail(routingCode, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    List<ShareVanRoutingPlan> rs = ((OdooResultDto<ShareVanRoutingPlan>) o).getRecords();
                    if (rs != null && rs.size() > 0) {
                        if (rs.get(0).getId() != null) {
                            routingPlan.set(rs.get(0));
                            isRatingCustomer.set(routingPlan.get().isRating_customer());

                            //nếu ko phải so_type thì ko cần check biến isValidQR -> cho biến đó thành true
                            if (!routingPlan.get().getSo_type()) {
                                isValidQrSo = true;
                            }
                            if (RoutingType.Drop.equals(routingPlan.get().getType())) {
                                isValidQrSo = true;
                            }
                            cloneList();
                            listImageRouting.addAll(routingPlan.get().getImage_urls());
                            runUi.run(Constants.GET_ROUTING_DETAIL);
                        }
                    }
                }
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }

    public void updateRatingCustomer() {
        DriverApi.updateRatingCustomerCheck(routingPlan.get().getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public boolean checkIsUpdate(BillPackage billPackage, BillPackage origin) {
        boolean isUpdate = false;
        if (!billPackage.getQuantity_import().equals(billPackage.getQuantityClone())) {
            isUpdate = true;
        }
        if (!billPackage.getLength().equals(origin.getLength())) {
            isUpdate = true;
        }
        if (!billPackage.getWidth().equals(origin.getWidth())) {
            isUpdate = true;
        }
        if (!billPackage.getHeight().equals(origin.getHeight())) {
            isUpdate = true;
        }
        if (!billPackage.getTotal_weight().equals(origin.getTotal_weight())) {
            isUpdate = true;
        }
        return isUpdate;
    }

    public void confirmRouting(String description, RunUi runUi) {
        if (isValidQrSo) {
            if (!routingPlan.get().isArrived_check()) {
                runUi.run(Constants.CONFIRM_ARRIVED_FIRST);
                return;
            }
            isLoading.set(true);
            if (mapEditPackage.get().size() > 0 && isEdit.get()) {
                handleDataBeforeReport();
                routingPlan.get().setList_bill_package(packageList);
                RoutingApi.reportBillPackageChange(description, routingPlan.get(), lstFileSelected, new IResponse() {
                    @Override
                    public void onSuccess(Object o) {
                        if (o != null && ((ApiResponseModel) o).status == 200) {
                            runUi.run(Constants.CONFIRM_SUCCESS, 4);
                        } else {
                            runUi.run(Constants.CONFIRM_FAIL);
                        }
                        isLoading.set(false);
                    }

                    @Override
                    public void onFail(Throwable error) {
                        runUi.run(Constants.CONFIRM_FAIL);
                        isLoading.set(false);
                    }
                });
            } else {
                RoutingApi.confirmRouting(description, routingPlan.get(), null, new IResponse() {
                    @Override
                    public void onSuccess(Object o) {
                        if (RoutingType.Pickup.equals(routingPlan.get().getType()) || routingPlan.get().getSo_type())
                            runUi.run(Constants.CONFIRM_SUCCESS, 2);
                        else
                            runUi.run(Constants.CONFIRM_SUCCESS, 1);
                        isLoading.set(false);
                    }

                    @Override
                    public void onFail(Throwable error) {
                        runUi.run(Constants.CONFIRM_FAIL);
                        isLoading.set(false);

                    }
                });
            }
        } else {
            runUi.run(Constants.INVALID_QR_SO);
        }
    }

    /**
     * set số lượng đã quét dc vào số lượng nhận để báo cáo
     */
    private void handleDataBeforeReport() {
        for (BillPackage item : packageList) {
            if (RoutingType.Pickup.equals(routingPlan.get().getType())) {
                item.setQuantity_import(item.getQuantityQrChecked());
            }
        }
    }

    public void confirmArrived(RunUi runUi) {
        isLoading.set(true);
        RoutingApi.confirmArrived(listRoutingPlanId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if ("200".equals(o)) {
                    routingPlan.get().setArrived_check(true);
                    routingPlan.notifyChange();
                } else {
                    runUi.run(Constants.CONFIRM_ARRIVED_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run(Constants.CONFIRM_ARRIVED_FAIL);
            }
        });
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }

    public void cloneList() {
        if (routingPlan.get() != null) {
            checked.set(false);
            routingPlanClone = routingPlan.get();
            packageList.clear();
            routingPlan.get().setTotal_qr_check(0);
            if (routingPlan.get().getList_bill_package() == null) return;
            for (BillPackage temp : routingPlan.get().getList_bill_package()) {
                temp.setQuantityClone(temp.getQuantity_import());
                BillPackage clone;
                clone = (BillPackage) temp.clone();
                clone.setQuantityClone(clone.getQuantity_import());
                packageList.add(clone);
                mapEditPackage.get().put(temp.getQr_char(), true);

                // gen Data Qr Code
                String[] strQr = temp.getQr_char().split(",");
                routingPlan.get().setTotal_qr_check(routingPlan.get().getTotal_qr_check() + strQr.length);
                HashMap<String, Boolean> tempHashMap = new HashMap<>();
                for (String item : strQr) {
                    tempHashMap.put(item, false);
                }
                qrDataChecked.put(temp.getBill_package_id(), tempHashMap);
            }
        }
    }
}
