package com.ts.sharevandriver.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.DialogConfirmBinding;
import com.ts.sharevandriver.utils.StringUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;


public class DialogConfirm extends DialogFragment {
    private String title;
    private String actionTitle;
    private String msg;
    private View.OnClickListener onClickListener;
    private boolean isCancelable;
    private boolean isHideCancelButton;
    private int animationResId = -1;
    private String animationFile;
    private DialogConfirmBinding binding;
    // check xử lý khi cancel
    private boolean cancelHandle;

    public DialogConfirm(@NonNull String title, String msg) {
        this.msg = msg;
        this.title = title;
        this.isCancelable = true;
    }
    public DialogConfirm(@NonNull String title, String msg,Boolean cancelHandle) {
        this.msg = msg;
        this.title = title;
        this.isCancelable = true;
        this.cancelHandle =cancelHandle;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        setCancelable(isCancelable);
        binding.btnCancel.setVisibility(isCancelable ? View.VISIBLE : View.INVISIBLE);
        if(!cancelHandle){
            binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        }else{
            binding.btnDismiss.setOnClickListener(onClickListener);
        }
        binding.txtTitle.setText(title);

        if (actionTitle != null) {
            binding.btnConfirm.setText(actionTitle);
        }
        if (StringUtils.isNullOrEmpty(msg)) {
            binding.txtMsg.setVisibility(View.GONE);
        } else {
            binding.txtMsg.setText(msg);
        }
        if (isHideCancelButton) {
            binding.btnDismiss.setVisibility(View.GONE);
        }

        // Set Animation from Resource
        if (animationResId != -1) {
            binding.animationView.setVisibility(View.VISIBLE);
            binding.animationView.setAnimation(animationResId);
            binding.animationView.playAnimation();

            // Set Animation from Assets File
        } else if (animationFile != null) {
            binding.animationView.setVisibility(View.VISIBLE);
            binding.animationView.setAnimation(animationFile);
            binding.animationView.playAnimation();
        } else {
            binding.animationView.setVisibility(View.GONE);
        }
        if(!cancelHandle){
            binding.btnCancel.setOnClickListener(v -> this.dismiss());
        }else{
            binding.btnCancel.setOnClickListener(onClickListener);
        }
        binding.btnConfirm.setOnClickListener(onClickListener);
        return binding.getRoot();
    }

    public DialogConfirm setAnimationFile(String animationFile) {
        this.animationFile = animationFile;
        return this;
    }


    public DialogConfirm setActionTitle(String actionTitle) {
        this.actionTitle = actionTitle;
        return this;
    }

    public DialogConfirm setMCancelable(boolean cancelable) {
        isCancelable = cancelable;
        return this;
    }

    public DialogConfirm setHideCancelButton(boolean hideCancelButton) {
        isHideCancelButton = hideCancelButton;
        return this;
    }

    public DialogConfirm setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    public DialogConfirm setAnimationResId(int animationResId) {
        this.animationResId = animationResId;
        return this;
    }
}