package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingVehicle extends BaseModel {
    private Integer id;

    private Object code;

    private String lisence_plate;

    private String driver_phone_number;

    private String driver_name;

    private String expiry_time;

    private Integer company_id;

    private String status;

    private String description;

    private String id_card;

    private Integer res_partner_id;

    private Integer tonnage;

    private Integer vehicle_type;

    private Integer weight_unit;

    private String bidding_vehicle_seq;
    private BiddingOrder bidding_order_receive;
    private BiddingOrder bidding_order_return;
    //list cargo ứng với xe
    private List<CargoType> cargo_types;
}