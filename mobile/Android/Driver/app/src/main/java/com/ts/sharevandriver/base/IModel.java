package com.ts.sharevandriver.base;

public interface IModel {
    String getModelName();
}
