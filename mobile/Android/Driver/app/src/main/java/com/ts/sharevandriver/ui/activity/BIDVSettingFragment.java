package com.ts.sharevandriver.ui.activity;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.FragmentSettingBidvBinding;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class BIDVSettingFragment extends BaseFragment<FragmentSettingBidvBinding> {
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_setting_bidv;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
