package com.ts.sharevandriver.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ts.sharevandriver.BR;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.DialogChooseSosTypeBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.DialogFragment;


public class DialogChooseSOSType extends DialogFragment {
    private DialogChooseSosTypeBinding binding;
    private final ChooseSOSType chooseSOSType;

    public ObservableBoolean continuable = new ObservableBoolean(true);

    public DialogChooseSOSType(ChooseSOSType chooseSOSType) {
        this.chooseSOSType = chooseSOSType;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_choose_sos_type, container, false);
        binding.setVariable(BR.listener, this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        binding.btnCancel.setOnClickListener(v -> dismiss());
        binding.btnDismiss.setOnClickListener(v -> dismiss());

        binding.btnConfirm.setOnClickListener(v -> {
            if (continuable.get()) {
                if (binding.txtEditText.getText().length() > 0) {
                    int delayTime = Integer.parseInt(binding.txtEditText.getText().toString());
                    chooseSOSType.onChooseSOSType(continuable.get(), delayTime);
                    dismiss();
                } else {
                    binding.txtEditText.setError(getString(R.string.please_estimate_delay_time));
                }
            } else {
                chooseSOSType.onChooseSOSType(continuable.get(), 0);
                dismiss();
            }

        });

        binding.continuable.setOnClickListener(v -> continuable.set(true));
        binding.canNotContinue.setOnClickListener(v -> continuable.set(false));

        return binding.getRoot();
    }

    public interface ChooseSOSType {
        /**
         * @param continuable có thể tiếp tục đi hay ko
         * @param delayTime   thời gian chậm ước lượng.
         */
        void onChooseSOSType(boolean continuable, int delayTime);
    }
}