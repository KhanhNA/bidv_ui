package com.ts.sharevandriver.enums;

public class ObjectStatus {
    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";//Update routing
    public static final String THREE = "3";//Logout
    public static final String FOUR = "4";//Update thông tin
    public static final String FIVE = "5";//Được gán xe
    public static final String SIX = "6";//Nhận trả xe
//    UpdateInformation = '4'
//    AssignCarWorking = '5'
//    ManagerApprovedCar = '6'
}
