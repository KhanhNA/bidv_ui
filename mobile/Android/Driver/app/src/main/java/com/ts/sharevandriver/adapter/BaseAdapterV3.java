package com.ts.sharevandriver.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.ts.sharevandriver.BR;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

public class BaseAdapterV3<T> extends BaseQuickAdapter<T, BaseDataBindingHolder> implements LoadMoreModule {
    private AdapterListener listenerAdapter;

    public BaseAdapterV3(@LayoutRes int itemLayoutId) {
        super(itemLayoutId);
    }

    public BaseAdapterV3(@LayoutRes int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public BaseAdapterV3(@LayoutRes int layoutResId, @Nullable List<T> data, AdapterListener listener) {
        super(layoutResId, data);
        this.listenerAdapter = listener;
    }

    @Override
    protected void convert(@NonNull BaseDataBindingHolder holder, T o) {
        //  Binding
        ((BaseModel) o).index = holder.getAdapterPosition()+1;
        ViewDataBinding binding = holder.getDataBinding();
        if (binding != null) {
            binding.setVariable(BR.viewHolder, o);
            if (listenerAdapter != null) {
                binding.setVariable(BR.listenerAdapter, this.listenerAdapter);
            }
            binding.executePendingBindings();
        }
    }

}
