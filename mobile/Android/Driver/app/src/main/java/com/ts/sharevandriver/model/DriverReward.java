package com.ts.sharevandriver.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverReward {
    private List<Reward> title_award;
    private List<Integer> num_rating;
    private List<Armorial> badges;
}
