package com.ts.sharevandriver.socket_notification;

public interface SocketIONavigationProvider {
    SocketIONavigation socketNavigation();
}
