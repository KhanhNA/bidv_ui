package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.enums.RoutingType;
import com.ts.sharevandriver.model.BillPackage;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

public class EditBillPackageVM extends BaseViewModel {
    public String type;//nhận hàng hay trả hàng.
    public ObservableField<BillPackage> billPackage = new ObservableField<>();
    public int originQuantityImport;

    public EditBillPackageVM(@NonNull Application application) {
        super(application);
        billPackage.set(new BillPackage());
    }


    public void addQuantity(){
        if(type.equals(RoutingType.Drop)) {
            billPackage.get().setQuantity_export(billPackage.get().getQuantity_export() + 1);
            billPackage.notifyChange();
        }else {
            if(originQuantityImport > billPackage.get().getQuantity_import()) {
                billPackage.get().setQuantity_import(billPackage.get().getQuantity_import() + 1);
                billPackage.notifyChange();
            }
        }

    }
    public void minusQuantity(){
        if(type.equals(RoutingType.Drop)) {
            int export = billPackage.get().getQuantity_export();
            if(export > 0){
                billPackage.get().setQuantity_export(export - 1);
                billPackage.notifyChange();
            }
        }else {
            int quantity_import = billPackage.get().getQuantity_import();
            if(quantity_import > 0) {
                billPackage.get().setQuantity_import(quantity_import - 1);
                billPackage.notifyChange();
            }
        }
    }

    public boolean isValid() {
        boolean isValid = true;
        BillPackage model = billPackage.get();
        if (model != null) {
            if (model.getLength() == null || model.getLength() <= 0) {
                isValid = false;
                addError("longs", R.string.INVALID_FIELD, true);
            }else if(model.getLength() < 0.01){
                isValid = false;
                addError("longs", R.string.GREATER_THAN_1CM, true);
            }else {
                clearErro("longs");
            }

            if (model.getWidth() == null || model.getWidth() <= 0) {
                isValid = false;
                addError("width", R.string.INVALID_FIELD, true);
            }else if(model.getWidth() < 0.01){
                isValid = false;
                addError("width", R.string.GREATER_THAN_1CM, true);
            }else {
                clearErro("width");
            }

            if (model.getHeight() == null || model.getHeight() <= 0) {
                isValid = false;
                addError("height", R.string.INVALID_FIELD, true);
            }else if(model.getHeight() < 0.01){
                isValid = false;
                addError("height", R.string.GREATER_THAN_1CM, true);
            }else {
                clearErro("height");
            }

            if (model.getTotal_weight() == null || model.getTotal_weight() <= 0) {
                isValid = false;
                addError("weight", R.string.INVALID_FIELD, true);
            }else if(model.getTotal_weight() < 0.1){
                isValid = false;
                addError("weight", R.string.GREATER_THAN_01KG, true);
            }else {
                clearErro("weight");
            }
        }
        return  isValid;
    }

}
