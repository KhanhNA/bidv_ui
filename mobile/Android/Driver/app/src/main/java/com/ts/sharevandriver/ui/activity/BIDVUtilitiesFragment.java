package com.ts.sharevandriver.ui.activity;

import com.ts.sharevandriver.R;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class BIDVUtilitiesFragment extends BaseFragment {
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_utilities_bidv;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return null;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
