package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.CustomerApi;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.Driver;
import com.ts.sharevandriver.model.DriverReward;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileVM extends BaseViewModel {

    ObservableField<DriverReward> driverReward = new ObservableField<>();
    ObservableField<Driver> driverObservable = new ObservableField<>();

    public ProfileVM(@NonNull Application application) {
        super(application);
        driverReward.set(new DriverReward());
    }

    public void getDriverReward(RunUi runUi) {
        DriverApi.getDriverReward(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                driverReward.set((DriverReward) o);
                runUi.run("getRewardSuccess");
            }

            @Override
            public void onFail(Throwable error) {

            }

        });
    }

    public void getUserInfo(RunUi runUi) {

        CustomerApi.getDriver(new IResponse<OdooResultDto<Driver>>() {
            @Override
            public void onSuccess(OdooResultDto<Driver> o) {
                if (o != null && o.getRecords().size() > 0) {
                    Driver driver = o.getRecords().get(0);
                    StaticData.setDriver(driver);
                    driverObservable.set(driver);
                    runUi.run("Get Info Success");
                }
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }
}
