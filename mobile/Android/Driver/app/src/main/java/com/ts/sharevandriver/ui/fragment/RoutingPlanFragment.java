package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.RoutingNodeAdapter;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.INotifyDataRouting;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.RoutingFragmentBinding;
import com.ts.sharevandriver.enums.ClickAction;
import com.ts.sharevandriver.enums.ObjectStatus;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.NotificationModel;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.mBool;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.model.section.RootNode;
import com.ts.sharevandriver.service.notification_service.NotificationService;
import com.ts.sharevandriver.ui.activity.RoutingDetailActivity;
import com.ts.sharevandriver.viewmodel.RoutingVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class RoutingPlanFragment extends BaseFragment {
    public static final String LIST_ROUTING_PLAN_ID = "LIST_ROUTING_PLAN_ID";
    public static final String HASH_MAP_STATUS_ROUTING = "HASH_MAP_STATUS_ROUTING";
    public static final String IS_REQUIRE = "IS_REQUIRE";
    public static final String POSITION = "POSITION";
    public static final String NODE_POSITION = "NODE_POSITION";
    public static final String ROOT_NODE = "ROOT_NODE";
    private RoutingFragmentBinding mBinding;
    private RoutingVM routingVM;
    private RoutingNodeAdapter nodeAdapter;
    private final int REQUEST_CODE_CONFIRM_ROUTING = 999;
    private HashMap<Integer, Integer> hashMapStatusRouting = new HashMap<>();
    private boolean isRequire = true; // kiểm tra có đúng theo thứ tự
    private boolean firstPick = true; // kiểm tra nút đầu tiên có bật
    private int lastIndexRequire = 1;
    private final int tab;
    private mBool isChanged;
    private INotifyDataRouting iNotifyDataRouting;
    private ArrayList<Integer> listRoutingId = new ArrayList<>();
    private String txtSearch = "";
    ArrayList<Integer> status;

    public RoutingPlanFragment(int tab, mBool isChange) {
        this.tab = tab;
        this.isChanged = isChange;
    }

    public void setINotifyDataRouting(INotifyDataRouting iNotifyDataRouting) {
        this.iNotifyDataRouting = iNotifyDataRouting;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (RoutingFragmentBinding) binding;
        routingVM = (RoutingVM) viewModel;
        initView();

        status = new ArrayList<>();
        if (tab == 0) {
            status.add(0);
            status.add(1);
            status.add(4);
        } else {
            status.add(2);
            status.add(3);

        }
        mBinding.swipeRefresh.setOnRefreshListener(() -> {
            txtSearch = "";
            mBinding.edtSearch.setText(txtSearch);
        });

        routingVM.getRoutingByStatus(txtSearch, status, this::runUi);
        eventChangeEdtSearch();
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAG", "onResume: ");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        NotificationModel notificationModel = gson.fromJson(notificationService.getMess_object(), NotificationModel.class);
        if (notificationModel.getClick_action().equals(ClickAction.ROUTING)) {
            onRefreshData();
            iNotifyDataRouting.notifyData(true);
        }
    }

    public void onRefreshData() {
        routingVM.getRoutingByStatus("", status, this::runUi);
    }

    private long mLastClickTime;

    private void initView() {
        mBinding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    txtSearch = mBinding.edtSearch.getText().toString().trim();
                    routingVM.getRoutingByStatus(txtSearch, status, RoutingPlanFragment.this::runUi);
                    return true;
                }
                return false;
            }
        });
        nodeAdapter = new RoutingNodeAdapter(routingVM);
        nodeAdapter.addChildClickViewIds(R.id.card_view, R.id.itemRouting, R.id.expand);
        nodeAdapter.setOnItemChildClickListener((adapter, view, position) -> {
//            if (view.getId() == R.id.map) {
//                Log.d("TAG", "show_map");
//            } else {
            long currentClickTime = SystemClock.elapsedRealtime();
            long elapsedTime = currentClickTime - mLastClickTime;
            mLastClickTime = currentClickTime;
            if (elapsedTime <= 500)
                return;
            Bundle bundle = new Bundle();
            bundle.putSerializable(HASH_MAP_STATUS_ROUTING, hashMapStatusRouting);
            getListRoutingPlanId();
            if (adapter.getItem(position) instanceof ItemRoutingNote) {
                lastIndexRequire = ((RootNode) adapter.getItem(0)).getChildNode().size() + 1;
                isRequire = (position <= lastIndexRequire && firstPick);
                Intent intent = new Intent(getActivity(), RoutingDetailActivity.class);
                intent.putExtra(Constants.ITEM_ID, ((ItemRoutingNote) adapter.getItem(position)).getRouting_plan_day_code());
                intent.putExtra(LIST_ROUTING_PLAN_ID, listRoutingId);
                intent.putExtra(IS_REQUIRE, isRequire);
                intent.putExtra(POSITION, position);
                startActivityForResult(intent, REQUEST_CODE_CONFIRM_ROUTING);
            } else if (adapter.getItem(position) instanceof RootNode) {
                if (view.getId() != R.id.expand) {
                    isRequire = position == 0;
                    int node_position = 0;
                    List<BaseNode> children = ((RootNode) adapter.getItem(position)).getChildNode();
                    for (int i = 0; i < children.size(); i++) {
                        if (((ItemRoutingNote) children.get(i)).getStatus().equals(0)) {
                            node_position = i;
                            break;
                        }
                    }
                    Intent intent = new Intent(getActivity(), RoutingDetailActivity.class);
                    intent.putExtra(ROOT_NODE, (RootNode) adapter.getItem(position));
                    intent.putExtra(Constants.ITEM_ID, ((ItemRoutingNote) children.get(node_position)).getRouting_plan_day_code());
                    intent.putExtra(POSITION, position);
                    intent.putExtra(IS_REQUIRE, isRequire);
                    intent.putExtra(NODE_POSITION, node_position);
                    if (isRequire) {
                        bundle.putIntegerArrayList(LIST_ROUTING_PLAN_ID, listRoutingId);
                        intent.putExtras(bundle);
                    }
                    startActivityForResult(intent, REQUEST_CODE_CONFIRM_ROUTING);
                } else {
                    if (position == 0) {
                        firstPick = !firstPick;
                    }
                    nodeAdapter.expandOrCollapse(position, true, true, RoutingNodeAdapter.EXPAND_COLLAPSE_PAYLOAD);
                }
            }

//            }
        });
        nodeAdapter.setOnItemClickListener((adapter, view, position) -> {

        });
        mBinding.rcBillLading.setAdapter(nodeAdapter);
    }

    private void getListRoutingPlanId() {
        BaseNode baseNode = routingVM.getLstCartNode().get(0);
        listRoutingId.clear();
        for (BaseNode item : baseNode.getChildNode()) {
            listRoutingId.add(((ItemRoutingNote) item).getId());
        }
    }

    private void eventChangeEdtSearch() {
        mBinding.btnSearch.setOnClickListener(v -> {
            if (mBinding.edtSearch.getText().toString().trim().isEmpty())
                return;
            txtSearch = mBinding.edtSearch.getText().toString().trim();
            routingVM.getRoutingByStatus(txtSearch, status, RoutingPlanFragment.this::runUi);
        });

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    txtSearch = "";
                    routingVM.getRoutingByStatus(txtSearch, status, RoutingPlanFragment.this::runUi);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals("getRouting")) {
            nodeAdapter.setList(routingVM.getLstCartNode());
            if (nodeAdapter.getData().size() > 0) {
                setDataHashMap();
            }
//            adapter.notifyDataSetChanged();
//            String title = tab == 0 ? getString(R.string.pending) : getString(R.string.completed_rout);
            String title = getString(R.string.total_import_and_export_points);
            mBinding.txtCount.setText(title + ": " + routingVM.getLstCartNode().size());
        }
    }

    private void setDataHashMap() {
        hashMapStatusRouting.clear();
        List<BaseNode> children = nodeAdapter.getItem(0).getChildNode();
        for (int i = 0; i < children.size(); i++) {
            hashMapStatusRouting.put(((ItemRoutingNote) children.get(i)).getId(), ((ItemRoutingNote) children.get(i)).getStatus());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONFIRM_ROUTING && (resultCode == Activity.RESULT_OK || resultCode == Constants.RESULT_OK)) {
            boolean isChange = false;
            if (data != null) {
                int position = data.getIntExtra("POSITION", 0);
                boolean isCancelOrder = data.getBooleanExtra("IS_CANCEL_ORDER", false);
                if (isCancelOrder) {
                    routingVM.getRoutingByStatus("", status, this::runUi);
                    iNotifyDataRouting.notifyData(true);
                    isChanged.aBoolean = true;
                    return;
                }
                if (position == 0) {
                    if (data.hasExtra("HASH_MAP_STATUS_ROUTING")) {
                        hashMapStatusRouting.clear();
                        hashMapStatusRouting = (HashMap<Integer, Integer>) data.getSerializableExtra("HASH_MAP_STATUS_ROUTING");
                        for (int i = routingVM.getLstCartNode().get(0).getChildNode().size() - 1; i >= 0; i--) {
                            Integer status = hashMapStatusRouting.get(((ItemRoutingNote) routingVM.getLstCartNode().get(0).getChildNode().get(i)).getId());
                            if (status != null) {
                                if (status == 2) {
                                    if (routingVM.getLstCartNode().get(0).getChildNode().size() == 1) {
                                        removeFirstNote();
                                        isChanged.aBoolean = true;
                                        return;
                                    } else {
                                        routingVM.getLstCartNode().get(0).getChildNode().remove(i);
                                    }
                                    isChange = true;
                                } else if (status == 1 || status == 4) {
                                    ((ItemRoutingNote) routingVM.getLstCartNode().get(0).getChildNode().get(i)).setStatus(status);
                                }
                            }
                        }
                        nodeAdapter.setList(routingVM.getLstCartNode());
                        if (routingVM.getLstCartNode().size() > 0) {
                            nodeAdapter.expand(0, true, true, RoutingNodeAdapter.EXPAND_COLLAPSE_PAYLOAD);
                            firstPick = true;
                        }
                    }
                } else {
                    int status = data.getIntExtra("STATUS", 0);
                    if (status == 2) {
                        if (nodeAdapter.getItem(0).getChildNode().size() == 1) {
                            removeFirstNote();
                        } else {
                            routingVM.getLstCartNode().get(0).getChildNode().remove(position - 1);
                        }
                        isChange = true;
                        nodeAdapter.setList(routingVM.getLstCartNode());
                        if (routingVM.getLstCartNode().size() > 0) {
                            nodeAdapter.expand(0, true, true, RoutingNodeAdapter.EXPAND_COLLAPSE_PAYLOAD);
                            firstPick = true;
                        }
                    } else if (status == 1 || status == 4) {
                        ((ItemRoutingNote) routingVM.getLstCartNode().get(0).getChildNode().get(position - 1)).setStatus(status);
                        nodeAdapter.notifyItemChanged(position);
                    }
                }
                String title = getString(R.string.total_warehouse);
                mBinding.txtCount.setText(title + ": " + routingVM.getLstCartNode().size());
                iNotifyDataRouting.notifyData(isChange);
//                nodeAdapter.notifyItemChanged(position);
                isChanged.aBoolean = true;
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void removeFirstNote() {
        routingVM.getLstCartNode().remove(0);
        String title = getString(R.string.total_warehouse);
        mBinding.txtCount.setText(title + ": " + routingVM.getLstCartNode().size());

        nodeAdapter.setList(routingVM.getLstCartNode());
        iNotifyDataRouting.notifyData(true);
        if (routingVM.getLstCartNode().size() > 0) {
            nodeAdapter.expand(0, true, true, RoutingNodeAdapter.EXPAND_COLLAPSE_PAYLOAD);
            firstPick = true;
        }
        ItemRoutingNote routingPlan = routingVM.currentLocationRoutingPlan();
        if (routingPlan != null) {
            if (VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus()) && !routingPlan.isCheck_point()) {
                // TODO: 07/10/2020 interval, distance lấy từ server
                AppController.getInstance().startLocationService(routingPlan.getId(), StaticData.getOdooSessionDto().getSave_log_duration(),
                        StaticData.getOdooSessionDto().getDistance_check_point(),
                        StaticData.getOdooSessionDto().getTime_mobile_notification_key(),
                        routingPlan.getLat(), routingPlan.getLng());
            } else {
                AppController.getInstance().stopLocationService();
            }
        }

    }


    @Override
    public int getLayoutRes() {
        return R.layout.routing_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
