package ns.vtc.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="fleet_driver")
@Data
@Getter
@Setter
public class FleetDriver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "company_id")
    private Integer companyId;

    @Column(name="driver_code")
    private String driverCode;

    @Column(name = "address")
    private String address;

    @Column(name = "employee_type")
    private String employeeType;

    @Column(name = "status")
    private String status;
    @Column(name = "point")
    private Integer point;

    @Column(name = "driver_license_date")
    private java.sql.Date driverLicenseDate;

    @Column(name = "expires_date")
    private java.sql.Date expiresDate;

    @Column(name = "fleet_management_id")
    private Integer fleetManagementId;



    @Transient
    private String classDriver;
    @Transient
    private Double maxTonage;
    @Transient
    private Integer numTrip;

    public FleetDriver() {
    }

    public FleetDriver(Integer id,String name, String displayName, Integer companyId, String driverCode, String address, String classDriver, Double maxTonage) {
        this.name = name;
        this.displayName = displayName;
        this.companyId = companyId;
        this.driverCode = driverCode;
        this.address = address;
        this.classDriver = classDriver;
        this.maxTonage = maxTonage;
    }
}
