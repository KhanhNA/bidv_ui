package ns.vtc.service;

import ns.utils.DateUtils;
import ns.utils.JPAUtility;
import ns.vtc.constants.Constants;
import ns.vtc.entity.*;
import ns.vtc.enums.*;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBService {
    private static DBService instance;

    public static DBService getInstance() {
        if (instance == null) {
            instance = new DBService();
        }
        return instance;
    }

    public SolutionDay getSolution(String groupCode, LocalDate planDate){
        HashMap<String, Object> params = new HashMap<>();
//        params.put("groupCode", groupCode);
        params.put("datePlan", DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
    //    System.out.println("");
        return JPAUtility.getInstance()
                .jpaFindOne("from SolutionDay where " +
                "                   datePlan = TO_date(:datePlan,'dd/mm/yyyy') and name is not null and softScore = 0 ", params);
    }
    public List<SolutionDay> getUnsolvedSolution(String groupCode, Integer limit){
        HashMap<String, Object> params = new HashMap<>();
        String sqlLimit = limit == null? "": " limit " + limit;
        return JPAUtility.getInstance()
                .jpaFindAll("from SolutionDay where solveTime is null and softScore = 0 order by datePlan " + sqlLimit, null);

    }
    public List<SolutionDay> getSolutionByDay(String datePlan){
        HashMap<String,Object> param = new HashMap<>();
        param.put("datePlan",datePlan);
        return JPAUtility.getInstance().jpaFindAll("from SolutionDay where solveTime is null and softScore = 0 and datePlan=to_date(:datePlan,'dd/mm/yyyy') order by id desc",param);
    }
    public List<SolutionDay> getUnsolvedSolution(String groupCode){
        return getUnsolvedSolution(groupCode, null);
    }

    public List<LocationData> getLocationData(){
        return JPAUtility.getInstance().jpaFindAll("from LocationData", null);
    }
    public Long insertSolutionDay(String groupCode, LocalDate datePlan, Long softScore, Long hardScore) throws Exception {

        List<Object> params = new ArrayList<>();
        params.add(DateUtils.toDate(datePlan));
        params.add(softScore);
        params.add(hardScore);

        String sql = "INSERT INTO solution_day (date_plan, solve_time, soft_score, hard_score, group_code) VALUES(?,current_date,?,?,'" + groupCode + "')" +
                " ON CONFLICT (id) DO UPDATE set solve_time = current_date, soft_score = EXCLUDED.soft_score, " +
                "                           hard_score = EXCLUDED.hard_score, group_code = '" + groupCode + "'";
        return JPAUtility.getInstance().executeJdbcInsert(sql, params);
    }
    public int getConfigParamRouting(String info){ //value in (durationTime, maxDistance)
        String field = "";

        if (Constants.DRIVER_CHECK_POINT_DURATION.equals(info)){
            field = Constants.DRIVER_CHECK_POINT_DURATION;
        }
        else if(Constants.MAX_DISTANCE_ROUTING.equals(info)){
            field = Constants.MAX_DISTANCE_ROUTING;
        }
        if(!"".equals(field)){
            String sql = "select " + field + " as value from res_config_settings order by id desc limit 1";
            try {
                ResultSet re = JPAUtility.getInstance().getJdbcResultSet(sql,null, new HashMap());
                if(re.next()){
                    return re.getInt("value");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;

    }
    public FleetVehicleAssignationLog findSchedule(FleetVehicleAssignationLog calendar){
        HashMap<String,Object> param = new HashMap<>();
        param.put("vehicleId",calendar.getVehicleId());
        param.put("driverId",calendar.getDriverId());
        param.put("startDate", DateUtils.toString(calendar.getStartDate(),DateUtils.DD_MM_YYYY));
        param.put("endDate", DateUtils.toString(calendar.getEndDate(),DateUtils.DD_MM_YYYY));
        param.put("status",StatusType.RUNNING.getValue());
        return JPAUtility.getInstance().jpaFindOne(
                "from FleetVehicleAssignationLog \n" +
                "where vehicleId = :vehicleId\n" +
                " and driverId = :driverId\n" +
                " and startDate = to_date(:startDate,'dd/mm/yyyy')\n" +
                " and endDate= to_date(:endDate,'dd/mm/yyyy')\n" +
                " and status = :status \n" +
                " and driverStatus = '1' ",param);
    }
    public static FleetVehicleAssignationLog getDriverByFleetAssignationLog(String datePlan, Long vehicleId){
        HashMap<String,Object> param = new HashMap<>();
        param.put("vehicleId", vehicleId);
        param.put("startDate", datePlan);
        param.put("status",StatusType.RUNNING.getValue());
        return JPAUtility.getInstance().jpaFindOne(
                "from FleetVehicleAssignationLog \n" +
                        "where vehicleId = :vehicleId\n" +
                     //   " and driverId = :driverId\n" +
                        " and startDate = to_date(:startDate,'dd/mm/yyyy')\n" +
                        " and giveCarBack is  null \n" +
                        " and status = :status \n" +
                        " and driverStatus = '1' ",param);
    }
    public static void updateOldAssignationLog(String datePlan) throws Exception {
        List<Object> param = new ArrayList<>();
        param.add(datePlan);
        JPAUtility.getInstance().executeJdbcUpdate("update fleet_vehicle_assignation_log " +
                " set driver_status = '0' " +
                " where date_start= to_date(?,'dd/mm/yyyy')" +
                " and give_car_back is null " +
                " and receive_car is null", param);
    }
}
