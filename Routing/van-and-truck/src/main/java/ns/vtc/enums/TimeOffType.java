package ns.vtc.enums;

public enum TimeOffType {
    WHOLE_DAY("1"),
    MORNING("2"),
    AFTERNOON("3");
    private String value;

    TimeOffType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static TimeOffType of(String value) throws Exception {
        if(WHOLE_DAY.getValue().equals(value)){
            return WHOLE_DAY;
        }
        if(MORNING.getValue().equals(value)){
            return MORNING;
        }
        if(AFTERNOON.getValue().equals(value)){
            return AFTERNOON;
        }
        throw new Exception ("Wrong value!");
    }
}
