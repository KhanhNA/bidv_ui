package ns.vtc.enums;

public enum ShipType {
    IN_ZONE("0"), LONG_HOLD("1");
    //Instance variable
    private String value;
    //Constructor to initialize the instance variable
    ShipType(String v) {
        this.value = v;
    }
    public String getType() {
        return this.value;
    }

    public static ShipType of(String value) throws Exception {

        if(IN_ZONE.getType().equals(value)){
            return IN_ZONE;
        }
        if(LONG_HOLD.getType().equals(value)){
            return LONG_HOLD;
        }

        throw new Exception ("Wrong value!");
    }
}
