package ns.vtc.enums;

public enum StaffType {
    MANAGER("manager"),
    DRIVER("driver");
    private String value;

    StaffType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public static StaffType of(String value) throws Exception {
        if(MANAGER.getValue().equals(value)){
            return MANAGER;
        }
        if(DRIVER.getValue().equals(value)){
            return DRIVER;
        }

        throw new Exception ("Wrong value!");
    }
}
