package ns.vtc.enums;
public enum StatusType {
    RUNNING("running"),
    DRAFT("draft"),
    DELETED("deleted");
    private String value;

    StatusType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static StatusType of(String value) throws Exception {
        if(RUNNING.getValue().equals(value)){
            return RUNNING;
        }
        if(DRAFT.getValue().equals(value)){
            return DRAFT;
        }
        if(DELETED.getValue().equals(value)){
            return DELETED;
        }
        throw new Exception ("Wrong value!");
    }
}
