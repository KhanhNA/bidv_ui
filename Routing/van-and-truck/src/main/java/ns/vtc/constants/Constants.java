package ns.vtc.constants;

import java.util.HashMap;

public class Constants {
    public static String MAX_DISTANCE_ROUTING="max_distance_routing_key";
    public static String DRIVER_CHECK_POINT_DURATION = "driver_check_point_duration_key";
    public static final String SECRET_KEY = "1qazXSW@3edcVFR$5tgbNHY^7ujm<KI*9ol.?:P)";
    public static final String API_NOTIFICATION_DRIVER = "http://demo.aggregatoricapaci.com:8070/send/notification_routing";
    public static final String API_NOTIFICATION_OUTDATE_LICENSE = "http://demo.aggregatoricapaci.com:8070/send/notification_license_expired";
    public static int DEFAULT_MAX_DISTANCE_ROUTING = 200000;
    public static int DELTA_DISTANCE = 50000;
    public static int DELTA_DAYS_CHECK_OUTDATE_LICENSE = 30;


}
