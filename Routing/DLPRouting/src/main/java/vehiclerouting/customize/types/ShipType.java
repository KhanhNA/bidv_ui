package vehiclerouting.customize.types;

public enum ShipType {
    PICKUP("0"), DELIVERY("1");
    //Instance variable
    private String value;
    //Constructor to initialize the instance variable
    ShipType(String v) {
        this.value = v;
    }
    public String getType() {
        return this.value;
    }

    public static ShipType of(String v){
        switch (v){
            case "0":
                return PICKUP;
            case "1":
                return DELIVERY;
        }
        return PICKUP;
    }
}
