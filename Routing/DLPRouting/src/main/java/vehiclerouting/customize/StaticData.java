package vehiclerouting.customize;

import drivingschedule.domain.*;
import maps.GoogleDistanceAPI;
import ns.utils.DateUtils;
import ns.utils.JPAUtility;
import ns.utils.ObjectUtils;
import ns.vtc.ApiManager;
import ns.vtc.entity.*;
import ns.vtc.service.DBService;
import ns.vtc.service.DriverService;
import ns.vtc.service.RoutingService;
import ns.vtc.service.VehicleService;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.customize.types.DistanceInfo;
import vehiclerouting.customize.types.ShipType;
import vehiclerouting.domain.Depot;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.location.AirLocation;
import vehiclerouting.domain.location.DistanceType;
import vehiclerouting.domain.location.Location;
import vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class StaticData {
    private static HashMap<String, DistanceInfo<Double, Double>> locDataMap;

    public static HashMap<String,LocationData> locationDataHashMap = new HashMap<>();
    public static final String SOLUTION_GROUP_CODE = "default";
    public static final LocalDate SOLUTION_DAY = LocalDate.of(2020, 11, 14);//LocalDate.now();

    public static int LOCATION_ID = 0;

    public static void reset() {//Load du lieu chi phi (cost), thoi gian giua cac kho
        if (locDataMap != null) {
            locDataMap.clear();
        }
        locDataMap = new HashMap<>();

    }

    public static Double getTime(ShipmentStandstill from, ShipmentStandstill to) {
        if (locDataMap == null) {
            return 0d;
        }
        DistanceInfo<Double, Double> d = getDistanceInfo(from, to);
        return d.getMinute();
//        Random generator = new Random();
//        double minutes = generator.nextInt(5) + 1;
//        LocationData data = new LocationData( from.getLocation().getLatitude(), from.getLocation().getLongitude(), to.getLocation().getLatitude(), to.getLocation().getLongitude(),minutes*10,minutes*1000,"A","B");
//        locationDataHashMap.put(data.getCode(),data);
//        return minutes*1000;



    }

    //chi phi lay trong bang location_data: ma la toa do cua from va to (refer bang locationData)
    public static Double getCost(ShipmentStandstill from, ShipmentStandstill to) {
        if (locDataMap == null) {
            return 0d;
        }
        DistanceInfo<Double, Double> d = getDistanceInfo(from, to);
        return d==null ? 0 : d.getCost();
//        Random generator = new Random();
//        double minutes = generator.nextInt(5) + 1;
//        LocationData data = new LocationData( from.getLocation().getLatitude(), from.getLocation().getLongitude(), to.getLocation().getLatitude(), to.getLocation().getLongitude(),minutes*10,minutes*1000,"A","B");
//        locationDataHashMap.put(data.getCode(),data);
//
//        return minutes*1000;
    }

    public static void addLocationData(List<LocationData> locationDatas) {
//        locDataMap = new HashMap<>();
        for (LocationData ld : locationDatas) {
//            locDataMap.put(ld.getCode(),DistanceInfo.of(ld.getMinutes() * 60, ld.getCost()));
            addLocationData(ld);
        }
    }



//    public static void addDepotData(List<ParkingPointData> parkingPointData) {
//
//        for (ParkingPointData ppd : parkingPointData) {
//            locDataMap.put("FDLAT" + ppd.get() + "-FDLONG" + ppd.getWarehouseId() + "TDLAT" + ppd.getParkingPointId() + "-TWLON" + ppd.getWarehouseId(),
//                    DistanceInfo.of((ppd.getMinutes() * 60), ppd.getCost()));
//        }
//    }

    public static HashMap<String, DistanceInfo<Double, Double>> getLocDataMap() {
        return locDataMap;
    }

    public static VehicleRoutingSolutionV2 loadProblem(SolutionDay solutionDay) {
        VehicleRoutingSolutionV2 problem = new TimeWindowedVehicleRoutingSolution();
        List<Location> locationList = new ArrayList<>();
//        HashMap<String, VehicleV2> vehicleMap;

//        HashMap<String, TimeWindowedShipmentPot> shipmentsMap;
        HashMap<Integer, TimeWindowedDepot> depotMap = new HashMap<>(); //key la location
        LOCATION_ID = 0;
        System.out.println("date_plan1222223: " + solutionDay.getDatePlan() + " id:" + solutionDay.getId());
        Map vehicleMap = loadVehices(locationList, depotMap,DateUtils.toString(solutionDay.getDatePlan(),DateUtils.DD_MM_YYYY));
        Map shipmentsMap = loadShimentPots(solutionDay, locationList, vehicleMap, depotMap);
        updateProblem(problem, depotMap, vehicleMap, shipmentsMap, locationList,solutionDay);

        //customer
        JPAUtility.getInstance().close();
        return problem;

    }

    private static void updateProblem(VehicleRoutingSolutionV2 problem,
                                      Map depotMap,
                                      Map vehicleMap,
                                      Map shipmentMap, List<Location> locationList,
                                      SolutionDay solutionDay) {
        if(solutionDay == null)
            solutionDay = DBService.getInstance().getSolution(SOLUTION_GROUP_CODE, SOLUTION_DAY);
        System.out.println("tttl: " + solutionDay.getDatePlan());
        List<LocationData> locationData = DBService.getInstance().getLocationData();
//        List<ParkingPointData> parkingPointData = DBService.getInstance().getParkingPointData();

        problem.setDepotList(new ArrayList<Depot>(depotMap.values()));
        problem.setVehicleList(new ArrayList<VehicleV2>(vehicleMap.values()));
        problem.setShipmentPotList(new ArrayList<ShipmentPot>(shipmentMap.values()));
        problem.setLocationList(locationList);
        problem.setDistanceType(DistanceType.AIR_DISTANCE);
        StaticData.reset();
        StaticData.addLocationData(locationData);
//        StaticData.addDepotData(parkingPointData);

        if (solutionDay != null) {
            problem.setScore(HardSoftLongScore.of(solutionDay.getHardScore(), solutionDay.getSoftScore()));
            problem.setDatePlan(solutionDay.getDatePlan());
        }
    }

    private static ShipmentStandstill getStandstill(String id, VehicleV2 vehicle,
                                                    Map shipmentsMap) {
        ShipmentStandstill tmpShipment = null;
        if (id != null && id.startsWith("W")) {
            tmpShipment = (ShipmentStandstill)shipmentsMap.get(id);
        } else if (id != null && id.startsWith("D")) {
            tmpShipment = vehicle;
        }
        return tmpShipment;
    }

    private static Map loadShimentPots(SolutionDay solutionDay, List<Location> locationList,
                                       Map vehicleMap, Map depotMap
                                       ) {
        Location locTmp;
//        long locationId = 1;
        HashMap<String, TimeWindowedShipmentPot> shipmentsMap = new HashMap<>();
        ShipmentStandstill preTmpShipmentPot, nextTmpShipmentPot;
        TimeWindowedShipmentPot tmpShipmentPot;
        List<RoutingPlanDay> routingPlanDays;
        if(solutionDay == null)
            routingPlanDays = RoutingService.getInstance().getRoutingPlanDay(solutionDay.getId(), SOLUTION_GROUP_CODE, SOLUTION_DAY);
        else
            routingPlanDays = RoutingService.getInstance().getRoutingPlanDay(solutionDay.getId(), SOLUTION_GROUP_CODE, solutionDay.getDatePlan());
        VehicleV2 vehicle;
        //System.out.println("size: " + routingPlanDays.size());
        for (RoutingPlanDay routing : routingPlanDays) {
            locTmp = new AirLocation(LOCATION_ID++, routing.getLatitude(), routing.getLongitude());

            tmpShipmentPot = new TimeWindowedShipmentPot(routing.getId(), locTmp, routing.getCapacityExpected(),
                    routing.getReadyTime(), routing.getDueTime(), 1, "" + routing.getPartnerId(), routing.getDepotId(),
                    routing.getWarehouseId(), routing.getPreviousId(), routing.getNextId(), ShipType.of(routing.getType()),
                    routing.getFromRoutingPlanDayId(), routing.getNumReceice(), routing.getDatePlan().toString(), routing.getSoType());
//            tmpShipmentPot.setArrivalTime(DateUtils.toSeconds(routing.getExpectedFromTime()).doubleValue());
            vehicle = routing.getVehicleId() == null ? null : (VehicleV2) vehicleMap.get("D" + routing.getVehicleId());
            tmpShipmentPot.setVehicle(vehicle);

            shipmentsMap.put(tmpShipmentPot.getCodeFromId(), tmpShipmentPot);

            locationList.add(locTmp);
        }
        return shipmentsMap;

    }

    //load data from database
    //map: van -> vehicle; parkingPoint -> depot
    private static HashMap<String, VehicleV2> loadVehices(List<Location> locationList,
                                                          HashMap<Integer, TimeWindowedDepot> depotMap, String datePlan) {

        ParkingPoint ppTmp;
        Integer key;
        TimeWindowedDepot depotTmp;
        AirLocation locTmp;
//        long locationId = 1;
        HashMap<String, VehicleV2> vehicleMap = new HashMap<>();
        List<FleetVehicle> vehicles = VehicleService.getInstance().getVehicles(datePlan);
        System.out.println("vehicle size: " + vehicles.size());
        vehicles = ObjectUtils.safe(vehicles);

        for (FleetVehicle v : vehicles) {
            System.out.println("van:" + v.toString());
        }
        for (FleetVehicle vehicle : vehicles) {
            ppTmp = vehicle.getParkingPoint();
            if (ppTmp == null) {
                continue;
            }
            key = ppTmp.getId();
            if (key == null) {
                continue;
            }
            depotTmp = depotMap.get(ppTmp.getId());
            //update depot
            if (depotTmp == null) {
                locTmp = new AirLocation(LOCATION_ID++, ppTmp.getLatitude(), ppTmp.getLongitude());
                depotTmp = new TimeWindowedDepot(ppTmp.getId(), locTmp, ppTmp.getDayReadyTime(), ppTmp.getDayDueTime());
                depotMap.put(key, depotTmp);
                locationList.add(locTmp);
            }
            //update location
            vehicleMap.put("D" + vehicle.getId(), new VehicleV2(vehicle.getName(), vehicle.getId(), vehicle.getCapacity(), depotTmp, vehicle.getCost(), vehicle.getCostPerUnit(), vehicle.getPriority()));
        }
        return vehicleMap;
    }

    public static <Solution_> void updateBestSolution(Solution_ solution) throws Exception {
        if (!(solution instanceof VehicleRoutingSolutionV2)) {
            return;
        }
        VehicleRoutingSolutionV2 routingSolution = (VehicleRoutingSolutionV2) solution;
        if(!routingSolution.isSaved()){
            return;
        }
        Long id = DBService.getInstance().insertSolutionDay(SOLUTION_GROUP_CODE, routingSolution.getDatePlan(),
                routingSolution.getScore().getSoftScore(), routingSolution.getScore().getHardScore());
        updateRoutingPlan(id, routingSolution);
    }
    private static Long getDriverByAssignationLog(HashMap<Long,Long> mapDriver, Long vehicleId){
        if(mapDriver.get(vehicleId) != null){
            return mapDriver.get(vehicleId);
        }
        return null;
    }

    public static <Solution_> void updateRoutingPlan(Long solId, VehicleRoutingSolutionV2 routingSolution) throws Exception {

        List<ShipmentPot> shipmentPotList = routingSolution.getShipmentPotList();
        boolean isSO = shipmentPotList.get(0).getSoType();
        StringBuffer insertInfo = new StringBuffer(shipmentPotList.size());
//        StringBuffer vanIds = new StringBuffer(shipmentPotList.size());
//        StringBuffer arrivalTimes = new StringBuffer(shipmentPotList.size());
//        StringBuffer updateUser = new StringBuffer(shipmentPotList.size());
//        StringBuffer updateDate = new StringBuffer(shipmentPotList.size());
        TimeWindowedShipmentPot timeShipmentPot;
        String timeTmp;
        ShipmentStandstill previous;
        HashMap<Long,Long> mapDriver = new HashMap<>();
        String lstDriverIds = "";
        for (ShipmentPot shipmentPot : shipmentPotList) {
            Long driverId = getDriverByAssignationLog(mapDriver,shipmentPot.getVehicle().getId());
            if(driverId == null){
                //System.out.println("date_plan: " + shipmentPot.getDate_plan());
                FleetVehicleAssignationLog assignationLog = DBService.getDriverByFleetAssignationLog(DateUtils.toString(LocalDate.parse(shipmentPot.getDate_plan()), DateUtils.DD_MM_YYYY) ,shipmentPot.getVehicle().getId());
                if(assignationLog != null){
                    mapDriver.put(shipmentPot.getVehicle().getId(),assignationLog.getDriverId());
                    driverId = assignationLog.getDriverId();
                    lstDriverIds += driverId + ",";
                }
            }
            timeShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
            timeTmp = timeShipmentPot.getArrivalTime() == null?null:DateUtils.toString(timeShipmentPot.getArrivalTime().longValue(), null);
            timeTmp = "TO_TIMESTAMP('" + timeTmp + "','dd/mm/YYYY hh24:mi:ss')";
            previous = shipmentPot.getPreviousStandstill();
            String previousCode = previous == null ? null : previous.getCodeFromId();//previous instanceof ShipmentPot?((ShipmentPot) previous).getId(): ((VehicleV2)previous).getId();
            String nextCode = timeShipmentPot.getNextShipmentPot() == null ? null : timeShipmentPot.getNextShipmentPot().getCodeFromId();
            if(shipmentPot.getFromExpectedTime() == null){
                insertInfo.append("(").append(timeShipmentPot.getId()).append(",") //route id
                        .append(timeShipmentPot.getVehicle().getId()).append(",") //van id
                        .append("'").append(nextCode).append("',") //next_id
                        .append("'").append(previousCode).append("',") //next_id
//                    .append(timeTmp).append(",") //route time
                        .append("current_date,")
                        .append("2, 'running',")
                        .append("").append(shipmentPot.getFromExpectedTime()).append(",")
                        .append("").append(shipmentPot.getToExpectedTime()).append(",")
                        .append(shipmentPot.getOrderNumber()).append(",")
                        .append(shipmentPot.getCapacityVehicle()).append(",")
                        .append(driverId).append(",")
                        .append("0").append(")")
                        .append(",");
            }
            else
                insertInfo.append("(").append(timeShipmentPot.getId()).append(",") //route id
                        .append(timeShipmentPot.getVehicle().getId()).append(",") //van id
                        .append("'").append(nextCode).append("',") //next_id
                        .append("'").append(previousCode).append("',") //next_id
    //                    .append(timeTmp).append(",") //route time
                        .append("current_date,")
                        .append("2, 'running',")
                        .append("'").append(shipmentPot.getFromExpectedTime()).append("',")
                        .append("'").append(shipmentPot.getToExpectedTime()).append("',")
                        .append(shipmentPot.getOrderNumber()).append(",")

                        .append(shipmentPot.getCapacityVehicle()).append(",")
                        .append(driverId).append(",")
                        .append("0").append(")")
                        .append(",");

        }
        insertInfo.deleteCharAt(insertInfo.length() - 1);
        RoutingService.getInstance().updateRoutingPlan(solId, insertInfo.toString());
        if(isSO == false){
            lstDriverIds = lstDriverIds.substring(0,lstDriverIds.length()-1);
            ApiManager.callNotificationOdoo(lstDriverIds,"NOTIFICATION_DRIVER");
        }

    }


    public static DistanceInfo getDistanceInfo(ShipmentStandstill from, ShipmentStandstill to) {
        Location f = from.getLocation();
        Location t = to.getLocation();
        String timeKey = LocationData.getLocationCode(f.getLatitude(), f.getLongitude(),
                t.getLatitude(), t.getLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
        DistanceInfo<Double, Double> d = locDataMap.get(timeKey);
        //System.out.println(f.toString());
        //System.out.println(t.toString());
        if(d == null){
            //rollback code
            LocationData data = GoogleDistanceAPI.getDistance(from, to);
            if( Double.compare(data.getMinutes(),0.0) == 0  &&
                Double.compare(data.getCost(),0.0) == 0   &&
                !data.getFromLatitude().equals(data.getToLatitude())){
                getDistanceInfo(from,to);
            }
            else{
                data = JPAUtility.getInstance().save(data);
                System.out.println("cost: " + data.getCost() + " time: " + data.getMinutes());
                JPAUtility.getInstance().close();
                d = addLocationData(data);
            }

        }
        return d;
    }
    @Transactional(rollbackOn = Exception.class)
    public LocationData getLocationData(ShipmentStandstill from, ShipmentStandstill to) {
        Location f = from.getLocation();
        Location t = to.getLocation();
       // LocationData data = null;
        String timeKey = LocationData.getLocationCode(f.getLatitude(), f.getLongitude(),
                t.getLatitude(), t.getLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
        LocationData data = locationDataHashMap.get(timeKey);
        //System.out.println(f.toString());
        //System.out.println(t.toString());
        if(data == null){
            data = GoogleDistanceAPI.getDistance(from, to);
            if( Double.compare(data.getMinutes(),0.0) == 0  &&
                    Double.compare(data.getCost(),0.0) == 0   &&
                    !data.getFromLatitude().equals(data.getToLatitude())){
                getLocationData(from,to);
            }
            else{
                data = JPAUtility.getInstance().save(data);
                JPAUtility.getInstance().close();
                addLocationData(data);
            }
        }
        return data;
    }

    private static DistanceInfo addLocationData(LocationData ld){
        DistanceInfo d = DistanceInfo.of(ld.getMinutes() * 60, ld.getCost());
        locDataMap.put(ld.getCode(),d);
        locationDataHashMap.put(ld.getCode(),ld);
        return d;
    }
    public static List getUnsolvedSolution(){
        return DBService.getInstance().getUnsolvedSolution(SOLUTION_GROUP_CODE);
    }
    public static SolutionDay getFirstUnsolvedSolution(){
        List<SolutionDay> solutionDays = DBService.getInstance().getUnsolvedSolution(SOLUTION_GROUP_CODE, 1);
        if(solutionDays == null || solutionDays.isEmpty()){
            return null;
        }
        return solutionDays.get(0);
    }
    public static SolutionDay getSolution(String datePlan){
        List<SolutionDay> solutionDay = DBService.getInstance().getSolutionByDay(datePlan);
        if(solutionDay == null || solutionDay.isEmpty()){
            return null;
        }
        return solutionDay.get(0);
    }

    public static DrivingScheduleSolution loadProblemForDrivingSchedule(String datePlan, String type) {
        DrivingScheduleSolution problem = new DrivingScheduleSolution();
        List<Driver> lstDriver = loadLstDriver(datePlan);
        List<DrivingSchedule> lstSchedule = loadLstSchedule(datePlan, type);
        JPAUtility.getInstance().close();
        problem.setLstDriver(lstDriver);
        problem.setLstSchedule(lstSchedule);
        problem.setLstVehicle(loadLstVehicle(datePlan));
        for(Driver d: lstDriver){
            System.out.println(" driver: " + d.getId() + " company: " + d.getCompanyId() + " license: " + d.getLicenseType().getName());
        }

        for(DrivingSchedule v: lstSchedule){
            System.out.println("vehicle: " + v.getVehicle().getId() + " company: " + v.getVehicle().getCompanyId() + " capcity: " + v.getVehicle().getTonage());
        }
        return problem;

    }
    public static List<Driver> loadLstDriver(String datePlan){
        List<FleetDriver> lstFleetDriver = DriverService.getInstance().getListDriver(datePlan);
        List<Driver> lstDriver = new ArrayList<>();
        for(FleetDriver driver : lstFleetDriver){
            System.out.println("id:" + driver.getId());
            Driver d = new Driver();
            d.setId((long) driver.getId());
            d.setName(driver.getName());
            d.setLicenseType(new LicenseType(driver.getClassDriver(),driver.getMaxTonage()));
            d.setCompanyId(driver.getCompanyId());
            d.setMaxTonage(driver.getMaxTonage());
            d.setNumTrip(driver.getNumTrip());
            d.setPoint(driver.getPoint());
            lstDriver.add(d);
           // lstDriver.add(new Driver(driver.getId(),driver.getName(),new LicenseType(0,driver.getClassDriver(),driver.getMaxTonage()),driver.getCompanyId(),new HashMap<>()));
        }
        return lstDriver;
    }
    public static List<DrivingSchedule> loadLstSchedule(String datePlan, String type){
      //  List<FleetDriver> lstFleetDriver = DBService.getInstance().getListDriver();
        List<DrivingSchedule> lstSchedule = new ArrayList<>();
        List<FleetVehicle> lstFleetVehicle = null;
        //tuyen binh thuong
        if(type.equals("DLP"))
            lstFleetVehicle = VehicleService.getInstance().getListVehicleRouting(datePlan);

        //don SO
        else if(type.equals("SO"))
           lstFleetVehicle = VehicleService.getInstance().getListVehicleRoutingSO(datePlan);


        //   LocalDateTime ldt = LocalDateTime.now();
      //  List<Vehicle> lstVehicle = new ArrayList<>();
        for(FleetVehicle vehicle: lstFleetVehicle){
            DrivingSchedule schedule = new DrivingSchedule();
            Vehicle v = new Vehicle();
            v.setId((long) vehicle.getId());
            v.setName(vehicle.getName());
            v.setLatitude(vehicle.getLatitude());
            v.setLongitude(vehicle.getLongitude());
            v.setCompanyId(vehicle.getCompanyId());
            v.setTonage(vehicle.getCapacity());
            HashMap<Integer,Integer> lstHisDriver = VehicleService.getInstance().getHisDriverByVehicle(vehicle.getId(),datePlan);
            v.setLstHisDriver(lstHisDriver);
            schedule.setVehicle(v);
            schedule.setId(v.getId());
            lstSchedule.add(schedule);
        }
        return lstSchedule;
    }
    public static List<Vehicle> loadLstVehicle(String datePlan){
        List<FleetVehicle> lstFleetVehicle = VehicleService.getInstance().getVehicles(datePlan);
        List<Vehicle> lstVehicle = new ArrayList<>();
        LocalDateTime ldt = LocalDateTime.now();
        for(FleetVehicle vehicle: lstFleetVehicle){
            Vehicle v = new Vehicle();
            v.setId((long) vehicle.getId());
            v.setName(vehicle.getName());
            v.setLatitude(vehicle.getLatitude());
            v.setLongitude(vehicle.getLongitude());
            v.setCompanyId(vehicle.getCompanyId());
            v.setTonage(vehicle.getCapacity());
            HashMap<Integer,Integer> lstHisDriver = VehicleService.getInstance().getHisDriverByVehicle(vehicle.getId(),DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
            v.setLstHisDriver(lstHisDriver);
            lstVehicle.add(v);
            //lstVehicle.add(new Vehicle(vehicle.getId(),vehicle.getName(),vehicle.getLatitude(),vehicle.getLongitude(),vehicle.getCompanyId(),vehicle.getCapacity()));
        }
        return lstVehicle;
    }
    public static void setDriverForRouting(String datePlan, long driverId, long vehicleId,String type) throws Exception {
        RoutingService.getInstance().addDriverForRouting(datePlan,vehicleId,driverId,type);
    }
    public static FleetVehicleAssignationLog createDriverCalendar(FleetVehicleAssignationLog calendar){
        calendar = DriverService.getInstance().createDriverCalendar(calendar);
       // System.out.println("kakakaka: " + calendar.getId());
        return calendar;
    }
    /* method for Mstore phase*/

    public static VehicleRoutingSolutionV2 loadProblemMstore(LocalDate datePlan) {
        VehicleRoutingSolutionV2 problem = new TimeWindowedVehicleRoutingSolution();
        List<Location> locationList = new ArrayList<>();
//        HashMap<String, VehicleV2> vehicleMap;

//        HashMap<String, TimeWindowedShipmentPot> shipmentsMap;
        HashMap<Integer, TimeWindowedDepot> depotMap = new HashMap<>(); //key la location
        LOCATION_ID = 0;
       // System.out.println("date_plan1222223: " + solutionDay.getDatePlan() + " id:" + solutionDay.getId());
        Map vehicleMap = loadVehices(locationList, depotMap, DateUtils.toString(datePlan, DateUtils.DD_MM_YYYY));
        Map shipmentsMap = loadShimentPotsMstore(datePlan, locationList, vehicleMap, depotMap);
        updateProblemMstore(problem, depotMap, vehicleMap, shipmentsMap, locationList,datePlan);

        //customer
        JPAUtility.getInstance().close();
        return problem;

    }
    private static Map loadShimentPotsMstore(LocalDate datePlan, List<Location> locationList,
                                       Map vehicleMap, Map depotMap
    ) {
        Location locTmp;
//        long locationId = 1;
        HashMap<String, TimeWindowedShipmentPot> shipmentsMap = new HashMap<>();
        ShipmentStandstill preTmpShipmentPot, nextTmpShipmentPot;
        TimeWindowedShipmentPot tmpShipmentPot;
        List<RoutingPlanDay> routingPlanDays = RoutingService.getInstance().getListRoutingSO(DateUtils.toString(datePlan,DateUtils.DD_MM_YYYY));

        //        if(solutionDay == null)
//            routingPlanDays = DBService.getInstance().getRoutingPlanDay(solutionDay.getId(), SOLUTION_GROUP_CODE, SOLUTION_DAY);
//        else
//            routingPlanDays = DBService.getInstance().getRoutingPlanDay(solutionDay.getId(), SOLUTION_GROUP_CODE, solutionDay.getDatePlan());
        VehicleV2 vehicle;
        System.out.println("size: " + routingPlanDays.size());
        for (RoutingPlanDay routing : routingPlanDays) {
            locTmp = new AirLocation(LOCATION_ID++, routing.getLatitude(), routing.getLongitude());

            tmpShipmentPot = new TimeWindowedShipmentPot(routing.getId(), locTmp, routing.getCapacityExpected(),
                    routing.getReadyTime(), routing.getDueTime(), 1, "" + routing.getPartnerId(), routing.getDepotId(),
                    routing.getWarehouseId(), routing.getPreviousId(), routing.getNextId(), ShipType.of(routing.getType()),
                    routing.getFromRoutingPlanDayId(), routing.getNumReceice(), routing.getDatePlan().toString(), routing.getSoType());
//            tmpShipmentPot.setArrivalTime(DateUtils.toSeconds(routing.getExpectedFromTime()).doubleValue());
            vehicle = routing.getVehicleId() == null ? null : (VehicleV2) vehicleMap.get("D" + routing.getVehicleId());
            tmpShipmentPot.setVehicle(vehicle);

            shipmentsMap.put(tmpShipmentPot.getCodeFromId(), tmpShipmentPot);

            locationList.add(locTmp);
        }
        return shipmentsMap;

    }
    private static void updateProblemMstore(VehicleRoutingSolutionV2 problem,
                                      Map depotMap,
                                      Map vehicleMap,
                                      Map shipmentMap, List<Location> locationList,
                                      LocalDate datePlan) {
//        if(solutionDay == null)
//            solutionDay = DBService.getInstance().getSolution(SOLUTION_GROUP_CODE, SOLUTION_DAY);
//        System.out.println("tttl: " + solutionDay.getDatePlan());
        List<LocationData> locationData = DBService.getInstance().getLocationData();
//        List<ParkingPointData> parkingPointData = DBService.getInstance().getParkingPointData();

        problem.setDepotList(new ArrayList<Depot>(depotMap.values()));
        problem.setVehicleList(new ArrayList<VehicleV2>(vehicleMap.values()));
        problem.setShipmentPotList(new ArrayList<ShipmentPot>(shipmentMap.values()));
        problem.setLocationList(locationList);
        problem.setDistanceType(DistanceType.AIR_DISTANCE);
        StaticData.reset();
        StaticData.addLocationData(locationData);
//        StaticData.addDepotData(parkingPointData);
        problem.setScore(HardSoftLongScore.of(0,0));
        problem.setDatePlan(datePlan);
//        if (solutionDay != null) {
//            problem.setScore(HardSoftLongScore.of(solutionDay.getHardScore(), solutionDay.getSoftScore()));
//            problem.setDatePlan(solutionDay.getDatePlan());
//        }
    }
    public static FleetVehicleAssignationLog getScheduleByAssignLog(FleetVehicleAssignationLog calendar){
        return DBService.getInstance().findSchedule(calendar);
    }
    public static FleetVehicleStatus createFleetVehicleStatus(FleetVehicleStatus vehicleStatus){
        return JPAUtility.getInstance().save(vehicleStatus);
    }
    public static FleetVehicleState getVehicleStateByCode(String vehicleState){
        return VehicleService.getInstance().getVehicleStateByCode(vehicleState);
    }
    public static void updateStateVehicle(long vehicleId, int stateId) throws Exception {
        VehicleService.getInstance().updateStateVehicle(vehicleId,stateId);
    }
    /* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
    public static List<Vehicle> loadLstVehicleTest(String datePlan){
        List<FleetVehicle> lstFleetVehicle = VehicleService.getInstance().getVehiclesTest(datePlan);
        List<Vehicle> lstVehicle = new ArrayList<>();
        LocalDateTime ldt = LocalDateTime.now();
        for(FleetVehicle vehicle: lstFleetVehicle){
            Vehicle v = new Vehicle();
            v.setId((long) vehicle.getId());
            v.setName(vehicle.getName());
            v.setLatitude(vehicle.getLatitude());
            v.setLongitude(vehicle.getLongitude());
            v.setCompanyId(vehicle.getCompanyId());
            v.setTonage(vehicle.getCapacity());
            HashMap<Integer,Integer> lstHisDriver = VehicleService.getInstance().getHisDriverByVehicle(vehicle.getId(),DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
            v.setLstHisDriver(lstHisDriver);
            lstVehicle.add(v);
            //lstVehicle.add(new Vehicle(vehicle.getId(),vehicle.getName(),vehicle.getLatitude(),vehicle.getLongitude(),vehicle.getCompanyId(),vehicle.getCapacity()));
        }
        return lstVehicle;
    }
    public static List<Driver> loadLstDriverTest(String datePlan){
        List<FleetDriver> lstFleetDriver = DriverService.getInstance().getListDriverTest(datePlan);
        List<Driver> lstDriver = new ArrayList<>();
        for(FleetDriver driver : lstFleetDriver){
            System.out.println("id:" + driver.getId());
            Driver d = new Driver();
            d.setId((long) driver.getId());
            d.setName(driver.getName());
            d.setLicenseType(new LicenseType(driver.getClassDriver(),driver.getMaxTonage()));
            d.setCompanyId(driver.getCompanyId());
            d.setMaxTonage(driver.getMaxTonage());
            d.setPoint(driver.getPoint());
            d.setNumTrip(driver.getNumTrip());
            d.setTeamId(driver.getFleetManagementId());
            lstDriver.add(d);
            // lstDriver.add(new Driver(driver.getId(),driver.getName(),new LicenseType(0,driver.getClassDriver(),driver.getMaxTonage()),driver.getCompanyId(),new HashMap<>()));
        }
        return lstDriver;
    }
    public static List<DrivingSchedule> loadLstScheduleTest(String datePlan){
        //  List<FleetDriver> lstFleetDriver = DBService.getInstance().getListDriver();
        List<DrivingSchedule> lstSchedule = new ArrayList<>();
        List<FleetVehicle> lstFleetVehicle = null;
        //tuyen binh thuong
//        if(type.equals("DLP"))
//            lstFleetVehicle = DBService.getInstance().getListVehicleRouting(datePlan);
//
//            //don SO
//        else if(type.equals("SO"))
//            lstFleetVehicle = DBService.getInstance().getListVehicleRoutingSO(datePlan);


        //   LocalDateTime ldt = LocalDateTime.now();
        //  List<Vehicle> lstVehicle = new ArrayList<>();
        lstFleetVehicle = VehicleService.getInstance().getVehiclesTest(datePlan);;
        for(FleetVehicle vehicle: lstFleetVehicle){
            DrivingSchedule schedule = new DrivingSchedule();
            Vehicle v = new Vehicle();
            v.setId((long) vehicle.getId());
            v.setName(vehicle.getName());
            v.setLatitude(vehicle.getLatitude());
            v.setLongitude(vehicle.getLongitude());
            v.setCompanyId(vehicle.getCompanyId());
            v.setTonage(vehicle.getCapacity());
            v.setTeamId(vehicle.getFleetManagementId());
            HashMap<Integer,Integer> lstHisDriver = VehicleService.getInstance().getHisDriverByVehicle(vehicle.getId(),datePlan);
            v.setLstHisDriver(lstHisDriver);
            schedule.setVehicle(v);
            schedule.setId(v.getId());
            lstSchedule.add(schedule);
        }
        return lstSchedule;
    }
    public static DrivingScheduleSolution loadProblemForDrivingScheduleTest(String datePlan) {
        DrivingScheduleSolution problem = new DrivingScheduleSolution();
        List<Driver> lstDriver = loadLstDriverTest(datePlan);
        List<DrivingSchedule> lstSchedule = loadLstScheduleTest(datePlan);
        JPAUtility.getInstance().close();
        problem.setLstDriver(lstDriver);
        problem.setLstSchedule(lstSchedule);
        problem.setLstVehicle(loadLstVehicleTest(datePlan));
        System.out.println("driver_sIze: " + lstDriver.size());
        System.out.println("vehicle_sIze: " + problem.getLstVehicle().size());
        for(Driver d: lstDriver){
            System.out.println(" driver: " + d.getId() + " company: " + d.getCompanyId() + " license: " + d.getLicenseType().getName());
        }

        for(DrivingSchedule v: lstSchedule){
            System.out.println("vehicle: " + v.getVehicle().getId() + " company: " + v.getVehicle().getCompanyId() + " capcity: " + v.getVehicle().getTonage());
        }
        return problem;
    }
    public static void updateOldAssignationLog(String datePlan) throws Exception {
        DBService.updateOldAssignationLog(datePlan);
    }
    public static List<FleetDriver> getListDriverOutdateLicense(String datePlan){
        return DriverService.getInstance().getLstDriverOnOutdateLicense(datePlan);
    }
}
