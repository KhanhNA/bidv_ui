/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.domain.timewindowed.solver;

import org.optaplanner.core.api.score.director.ScoreDirector;
import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;

import java.util.Objects;

// TODO When this class is added only for TimeWindowedCustomer, use TimeWindowedCustomer instead of Customer
public class ArrivalTimeUpdatingVariableListener implements VariableListener<ShipmentPot> {

    @Override
    public void beforeEntityAdded(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterEntityAdded(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        if (shipmentPot instanceof TimeWindowedShipmentPot) {
            updateArrivalTime(scoreDirector, (TimeWindowedShipmentPot) shipmentPot);
        }
    }

    @Override
    public void beforeVariableChanged(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterVariableChanged(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        if (shipmentPot instanceof TimeWindowedShipmentPot) {
            updateArrivalTime(scoreDirector, (TimeWindowedShipmentPot) shipmentPot);
        }
    }

    @Override
    public void beforeEntityRemoved(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterEntityRemoved(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    protected void updateArrivalTime(ScoreDirector scoreDirector, TimeWindowedShipmentPot sourceCustomer) {
        ShipmentStandstill previousStandstill = sourceCustomer.getPreviousStandstill();
        Double departureTime = previousStandstill == null ? null
                : (previousStandstill instanceof TimeWindowedShipmentPot)
                        ? ((TimeWindowedShipmentPot) previousStandstill).getDepartureTime()
                        : ((TimeWindowedDepot) ((VehicleV2) previousStandstill).getDepot()).getReadyTime();
        TimeWindowedShipmentPot shadowCustomer = sourceCustomer;
        Double arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
        while (shadowCustomer != null && !Objects.equals(shadowCustomer.getArrivalTime(), arrivalTime)) {
            scoreDirector.beforeVariableChanged(shadowCustomer, "arrivalTime");
            shadowCustomer.setArrivalTime(arrivalTime);
            scoreDirector.afterVariableChanged(shadowCustomer, "arrivalTime");
            departureTime = shadowCustomer.getDepartureTime();
            shadowCustomer = shadowCustomer.getNextShipmentPot();
            arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
        }
    }

    private Double calculateArrivalTime(TimeWindowedShipmentPot windowedShipmentPot, Double previousDepartureTime) {
        if (windowedShipmentPot == null || windowedShipmentPot.getPreviousStandstill() == null) {
            return null;
        }
        if (windowedShipmentPot.getPreviousStandstill() instanceof VehicleV2) {
            // PreviousStandstill is the Vehicle, so we leave from the Depot at the best suitable time
//            System.out.println("ready time: " + windowedShipmentPot.getReadyTime());
//            System.out.println("departure time: " + previousDepartureTime);
//            System.out.println("previous time: " + windowedShipmentPot.getDistanceFromPreviousStandstill());
            return Math.max(windowedShipmentPot.getReadyTime(),
                    previousDepartureTime == null ? 0: previousDepartureTime + windowedShipmentPot.getDistanceFromPreviousStandstill());
        }
        return previousDepartureTime + windowedShipmentPot.getDistanceFromPreviousStandstill();
    }

}
