package vehiclerouting.domain;

import lombok.Getter;
import lombok.Setter;
import vehiclerouting.customize.types.DistanceInfo;

@Getter
@Setter
public class DataGoogleApi {
    private String startAddress;

    private String endAddress;


    private DistanceInfo<Double, Double> distanceInfo;
}
