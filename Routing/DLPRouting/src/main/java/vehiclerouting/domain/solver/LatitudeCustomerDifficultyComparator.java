/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.domain.solver;



import vehiclerouting.customize.entity.ShipmentPot;

import java.util.Comparator;

/**
 * On large datasets, the constructed solution looks like a zebra crossing.
 */
public class LatitudeCustomerDifficultyComparator implements Comparator<ShipmentPot> {

    private static final Comparator<ShipmentPot> COMPARATOR = Comparator
            .comparingDouble((ShipmentPot customer) -> customer.getLocation().getLatitude())
            .thenComparingDouble(customer -> customer.getLocation().getLongitude())
            .thenComparingDouble(ShipmentPot::getDemand)
            .thenComparingLong(ShipmentPot::getId);

    @Override
    public int compare(ShipmentPot a, ShipmentPot b) {
        return COMPARATOR.compare(a, b);
    }

}
