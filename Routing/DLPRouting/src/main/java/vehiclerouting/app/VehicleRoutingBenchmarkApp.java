/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.app;


import common.app.CommonBenchmarkApp;

public class VehicleRoutingBenchmarkApp extends CommonBenchmarkApp {

    public static void main(String[] args) {
        new VehicleRoutingBenchmarkApp().buildAndBenchmark(args);
    }

    public VehicleRoutingBenchmarkApp() {
        super(
                new ArgOption("default",
                        "vehiclerouting/benchmark/vehicleRoutingBenchmarkConfig.xml"),
                new ArgOption("stepLimit",
                        "vehiclerouting/benchmark/vehicleRoutingStepLimitBenchmarkConfig.xml"),
                new ArgOption("scoreDirector",
                        "vehiclerouting/benchmark/vehicleRoutingScoreDirectorBenchmarkConfig.xml"),
                new ArgOption("template",
                        "vehiclerouting/benchmark/vehicleRoutingBenchmarkConfigTemplate.xml.ftl",
                        true));
    }

}
