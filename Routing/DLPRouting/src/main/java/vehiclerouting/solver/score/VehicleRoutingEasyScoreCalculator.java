/*
 * Copyright 2013 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.solver.score;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VehicleRoutingEasyScoreCalculator implements EasyScoreCalculator<VehicleRoutingSolutionV2> {

    @Override
    public HardSoftLongScore calculateScore(VehicleRoutingSolutionV2 solution) {
        boolean timeWindowed = solution instanceof TimeWindowedVehicleRoutingSolution;
        List<ShipmentPot> shipmentPots = solution.getShipmentPotList();
        List<VehicleV2> vehicleList = solution.getVehicleList();
        Map<VehicleV2, Double> vehicleDemandMap = new HashMap<>(vehicleList.size());
        for (VehicleV2 vehicle : vehicleList) {
            vehicleDemandMap.put(vehicle, 0d);
        }
        long hardScore = 0L;
        long softScore = 0L;
        for (ShipmentPot shipmentPot : shipmentPots) {
            ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
            if (previousStandstill != null) {
                VehicleV2 vehicle = shipmentPot.getVehicle();
                vehicleDemandMap.put(vehicle, vehicleDemandMap.get(vehicle) + shipmentPot.getDemand());
                // Score constraint distanceToPreviousStandstill
                softScore -= shipmentPot.getDistanceFromPreviousStandstill();
                if (shipmentPot.getNextShipmentPot() == null) {
                    // Score constraint distanceFromLastCustomerToDepot
                    softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
                }
                if (timeWindowed) {
                    TimeWindowedShipmentPot timeWindowedShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
                    Double dueTime = timeWindowedShipmentPot.getDueTime();
                    Double arrivalTime = timeWindowedShipmentPot.getArrivalTime();
                    if (dueTime < arrivalTime) {
                        // Score constraint arrivalAfterDueTime
                        hardScore -= (arrivalTime - dueTime);
                    }
                }
            }
        }
        for (Map.Entry<VehicleV2, Double> entry : vehicleDemandMap.entrySet()) {
            Double capacity = entry.getKey().getCapacity();
            Double demand = entry.getValue();
            if (demand > capacity) {
                // Score constraint vehicleCapacity
                hardScore -= (demand - capacity);
            }
        }
        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
        return HardSoftLongScore.of(hardScore, softScore);
    }

}
