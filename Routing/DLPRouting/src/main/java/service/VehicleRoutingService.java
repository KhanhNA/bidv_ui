/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package service;

import common.app.CommonApp;
import common.persistence.AbstractSolutionImporter;
import common.swingui.SolverAndPersistenceFrame;
import ns.vtc.entity.SolutionDay;
import org.optaplanner.persistence.common.api.domain.solution.SolutionFileIO;
import vehiclerouting.customize.StaticData;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;
import vehiclerouting.persistence.VehicleRoutingImporter;
import vehiclerouting.persistence.VehicleRoutingXmlSolutionFileIO;
import vehiclerouting.swingui.VehicleRoutingPanel;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class VehicleRoutingService extends CommonApp<VehicleRoutingSolutionV2> {

    public static final String SOLVER_CONFIG = "vehiclerouting/solver/vehicleRoutingSolverConfig.xml";

    public static final String DATA_DIR_NAME = "vehiclerouting";

    public static void main(String[] args) {
        VehicleRoutingService service = new VehicleRoutingService();
//        service.init();

        service.start();
    }

    public VehicleRoutingService() {
        super("Vehicle routing",
                "Official competition name: Capacitated vehicle routing problem (CVRP), " +
                        "optionally with time windows (CVRPTW)\n\n" +
                        "Pick up all items of all customers with a few vehicles.\n\n" +
                        "Find the shortest route possible.\n" +
                        "Do not overload the capacity of the vehicles.\n" +
                        "Arrive within the time window of each customer.",
                SOLVER_CONFIG, DATA_DIR_NAME,
                VehicleRoutingPanel.LOGO_PATH);

    }
    public void start(){
        solutionBusiness = createSolutionBusiness();

        SolutionDay solutionDay = StaticData.getFirstUnsolvedSolution();
        VehicleRoutingSolutionV2 problem = StaticData.loadProblem(solutionDay);
//        new SolveWorker(problem).execute();
        solutionBusiness.registerForBestSolutionChanges(null);
        solutionBusiness.solve(problem);
    }

    @Override
    protected VehicleRoutingPanel createSolutionPanel() {
        return new VehicleRoutingPanel();
    }

    @Override
    public SolutionFileIO<VehicleRoutingSolutionV2> createSolutionFileIO() {
        return new VehicleRoutingXmlSolutionFileIO();
    }

    @Override
    protected AbstractSolutionImporter[] createSolutionImporters() {
        return new AbstractSolutionImporter[] {
                new VehicleRoutingImporter()
        };
    }

    protected class SolveWorker extends SwingWorker<VehicleRoutingSolutionV2, Void> {

        protected final VehicleRoutingSolutionV2 problem;

        public SolveWorker(VehicleRoutingSolutionV2 problem) {
            this.problem = problem;
        }

        @Override
        protected VehicleRoutingSolutionV2 doInBackground() throws Exception {
            return solutionBusiness.solve(problem);
        }

        @Override
        protected void done() {
            try {
                VehicleRoutingSolutionV2 bestSolution = get();
                solutionBusiness.setSolution(bestSolution);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException("Solving was interrupted.", e);
            } catch (ExecutionException e) {
                throw new IllegalStateException("Solving failed.", e.getCause());
            } finally {
//                setSolvingState(false);
//                resetScreen();
            }
        }

    }

}
