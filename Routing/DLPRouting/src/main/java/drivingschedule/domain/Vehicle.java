package drivingschedule.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import common.domain.AbstractPersistable;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@XStreamAlias("DrsVehicle")
@Getter
@Setter
public class Vehicle extends AbstractPersistable {
    private String name;
    private Double latitude;
    private Double longitude;
    private Integer companyId;
    private Integer parentId;
    private Double tonage;
    private HashMap<Integer,Integer> lstHisDriver;
    private Integer teamId;
    public Vehicle(long id, String name) {
        super(id);
        this.name = name;
    }

    public Vehicle(long id, String name, Double latitude, Double longitude, Integer companyId, Double tonage) {
        super(id);
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.companyId = companyId;
        this.tonage = tonage;
    }

    public Vehicle() {
    }
}
