package drivingschedule.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import common.domain.AbstractPersistable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.criteria.CriteriaBuilder;

@XStreamAlias("DrsDriver")
@Getter
@Setter
public class Driver extends AbstractPersistable {
    private String name;
    private LicenseType licenseType;
    private Integer companyId;
    private Integer parentId;
    private Double maxTonage;
    private Integer numTrip;
    private Integer point;
    private Integer teamId;

    public Driver() {

    }

    public Driver(long id, String name, LicenseType licenseType, Integer companyId, Double maxTonage) {
        super(id);
        this.maxTonage = maxTonage;
        this.name = name;
        this.licenseType = licenseType;
        this.companyId = companyId;
        //this.parentId = parentId;

    }
}
