package drivingschedule.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import common.domain.AbstractPersistable;
import common.swingui.components.Labeled;
import drivingschedule.solver.domain.DriverScheduleDifficultyComparator;
import drivingschedule.solver.domain.DriverStrengthComparator;
import drivingschedule.solver.score.DrivingScheduleEasyScoreCalculator;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity(difficultyComparatorClass = DriverScheduleDifficultyComparator.class)
@XStreamAlias("DrivingSchedule")

public class DrivingSchedule  extends AbstractPersistable implements Labeled {
    private Driver driver;
    // Planning variables: changes during planning, between score calculations.
    private Vehicle vehicle;

    public DrivingSchedule() {
    }

    public DrivingSchedule(long id, Driver driver, Vehicle vehicle) {
        super(id);
        this.driver = driver;
        this.vehicle = vehicle;
    }

    public DrivingSchedule(long id,  Driver driver) {
        super(id);
        this.driver = driver;
    }


    public Vehicle getVehicle() {
        return vehicle;
    }

    @PlanningVariable(valueRangeProviderRefs = { "driverRange" }, strengthComparatorClass = DriverStrengthComparator.class)
    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String getLabel() {
        return null;
    }

    public int getListHisDriver(){
        Vehicle v = this.vehicle;
        int total = 0;
        for(Integer i: v.getLstHisDriver().keySet()){
            total += v.getLstHisDriver().get(i);
        }
        return total;
    }
}
