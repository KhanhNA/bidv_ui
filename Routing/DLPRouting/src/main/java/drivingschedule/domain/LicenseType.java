package drivingschedule.domain;

import common.constants.Constansts;
import common.domain.AbstractPersistable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LicenseType extends AbstractPersistable {
    public LicenseType() {
    }
    private String name;
    private Double maxTonage;

    public LicenseType( String name, Double maxTonage) {
        this.name = name;
        this.maxTonage = maxTonage;
    }
    public static String checkRuleLicense(double tonage){
        if(tonage <= Constansts.FIRST_GROSS_TON){ //1000
            return "A4,B1,B2,FB2,C,D,E,F,FE,FD,FC";
        }
        if(tonage > Constansts.FIRST_GROSS_TON && tonage < Constansts.SECOND_GROSS_TON){ //(1000,3500)
            return "B1,B2,FB2,C,D,E,F,FE,FD,FC";
        }
        if(tonage >= Constansts.SECOND_GROSS_TON){ //3500
            return "C,D,E,F,FE,FD,FC";
        }
        return "";
    }
    public static boolean checkLicense(Driver driver, Vehicle vehicle){
//        double tonage = vehicle.getTonage();
//        String lstPermitLicense[] = checkRuleLicense(tonage).split(",");
//        String license = driver.getLicenseType().name;
//        if(lstPermitLicense == null)
//            return false;
//        for(int i = 0; i < lstPermitLicense.length; i++){
//            if(lstPermitLicense[0].equals(license)){
//                return true;
//            }
//        }
//        return false;
        double maxTonage = driver.getMaxTonage();
        double capacity = vehicle.getTonage().intValue();
        if(maxTonage >= capacity){
            return true;
        }
        return false;
    }
}
