package ac.dlp.locationService.controller;

import ac.dlp.locationService.model.LocationLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ac.dlp.locationService.model.Employee;
import ac.dlp.locationService.service.RabbitMQSender;

import java.util.List;

@RestController
@RequestMapping(value = "/location-service/")
public class RabbitMQWebController {

    @Autowired
    RabbitMQSender rabbitMQSender;

    @PostMapping(value = "/producer")
    public String producer(@RequestBody List<LocationLog> locationLogs) {

        rabbitMQSender.send(locationLogs);

        return "Message sent to the RabbitMQ JavaInUse Successfully";
    }

}

