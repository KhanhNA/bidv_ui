package ac.dlp.locationService.service;

import ac.dlp.locationService.model.Employee;
import ac.dlp.locationService.model.LocationLog;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RabbitMQSender {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${location.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${location.rabbitmq.routingkey}")
	private String routingkey;	
	String kafkaTopic = "java_in_use_topic";
	
	public void send(List<LocationLog> locationLog) {
		amqpTemplate.convertAndSend(exchange, routingkey, locationLog);
		System.out.println("Send msg = " + locationLog);
	    
	}
}