package ac.dlp.locationService;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringRabbitMQListnerApplication {
	@Bean
	public Jackson2JsonMessageConverter converter() {
		return new Jackson2JsonMessageConverter();
	}
	public static void main(String[] args) {
		SpringApplication.run(
				new Object[] { SpringRabbitMQListnerApplication.class }, args);
//		SpringApplication.run(SpringRabbitMQListnerApplication.class, args);
	}
}
