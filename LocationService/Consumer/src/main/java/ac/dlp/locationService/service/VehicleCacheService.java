/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ac.dlp.locationService.service;

import ac.dlp.locationService.model.FleetVehicle;
import ac.dlp.locationService.model.FleetVehicleTokens;
import ac.dlp.locationService.model.FleetVehicleLocationLog;
import ac.dlp.locationService.model.PushNotificationEvent;
import ns.utils.JPAUtility;
import org.redisson.Redisson;
import org.redisson.api.*;
import org.redisson.config.Config;
import org.springframework.context.ApplicationEventPublisher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VehicleCacheService {
    protected static RedissonClient redisson = null;
    protected static RLiveObjectService liveObjectService;

    private static void init() {
        if (redisson != null) {
            return;
        }
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.1.69:6379");
//        config.addNodeAddress("redis://192.168.1.69:6379");

        redisson = Redisson.create(config);
        liveObjectService = redisson.getLiveObjectService();
    }

    public static List<FleetVehicle> updateCache(Object source, ApplicationEventPublisher applicationEventPublisher, HashMap<Integer, FleetVehicleLocationLog> mapLastLocation) {

        List<FleetVehicle> lstNotificationVehicles = new ArrayList<>();
        HashMap<String, Object> params = new HashMap<>();
        FleetVehicleLocationLog locationLog;
        FleetVehicle fleetVehicle = null, fleetVehicleDB, tmpFV;
        init();
        for (Map.Entry el : mapLastLocation.entrySet()) {
            //1. lay du lieu location cap nhat cuoi
            locationLog = (FleetVehicleLocationLog) el.getValue();

            //2. lay tu cache
            tmpFV = liveObjectService.get(FleetVehicle.class, locationLog.getVehicleId());

            if (tmpFV != null) {
//                liveObjectService.delete(fleetVehicle);
                fleetVehicle = liveObjectService.detach(tmpFV);
                liveObjectService.delete(tmpFV);
            }

            if (fleetVehicle == null || fleetVehicle == null
                    || fleetVehicle.getLastUpdateLocationTime() == null
                    || fleetVehicle.getLastUpdateLocationTime().compareTo(locationLog.getAssignDateTime()) < 0) {
                params.put("id", locationLog.getVehicleId());
                fleetVehicleDB = JPAUtility.getInstance().jpaFindOne("from FleetVehicle where id = :id", params);
                if (fleetVehicleDB.getLastUpdateLocationTime() == null ||
                        fleetVehicleDB.getLastUpdateLocationTime().compareTo(locationLog.getAssignDateTime()) < 0) {
                    fleetVehicleDB.setLongitude(locationLog.getLongitude());
                    fleetVehicleDB.setLatitude(locationLog.getLatitude());
                    fleetVehicleDB.setLastUpdateLocationTime(locationLog.getAssignDateTime());
                    JPAUtility.getInstance().save(fleetVehicleDB);

                }

                if (fleetVehicle != null) {
                    fleetVehicleDB.tokens = (fleetVehicle.tokens);

                }

                liveObjectService.merge(fleetVehicleDB);
                FleetVehicle notificationVehicle = NotificationCacheService.add(fleetVehicleDB);
                if(notificationVehicle != null){
                    lstNotificationVehicles.add(notificationVehicle);
                }


            }

        }

        return lstNotificationVehicles;
//        redisson.shutdown();
    }

    public static void registerNotify(Integer vehicleId, String token) {
        init();
        //1. lay tu cache
        FleetVehicle f = new FleetVehicle(vehicleId);
//        f = liveObjectService.persist(f);
//        f.addToken(token);


//        List<String> t = f.tokens;
//        if(t == null){
//            t = new ArrayList<>();
//            f.tokens = t;
//        }
//        t.add(token);

        f = liveObjectService.get(FleetVehicle.class, vehicleId);
        FleetVehicle f1 = liveObjectService.detach(f);
        f1.addToken(token);
        liveObjectService.delete(f);
//        f.addToken(token);
        liveObjectService.merge(f1);

//        cacheVehicle = liveObjectService.merge(cacheVehicle);
    }

    public static void removeNotify(Integer vehicleId, String token) {
        init();
        //1. lay tu cache
        FleetVehicleTokens cacheVehicle = liveObjectService.get(FleetVehicleTokens.class, vehicleId);
        cacheVehicle.removeToken(token);

    }
}
