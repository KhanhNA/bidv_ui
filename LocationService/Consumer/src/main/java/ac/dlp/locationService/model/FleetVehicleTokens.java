package ac.dlp.locationService.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;
import org.redisson.liveobject.resolver.UUIDGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@REntity

@Getter
@Setter
public class FleetVehicleTokens {


    @RId(generator = UUIDGenerator.class)
    public String id;

    public FleetVehicle fleetVehicle;
    private List<String> tokens;


    public FleetVehicleTokens() {
    }

    public FleetVehicleTokens(FleetVehicle fv) {
        super();
        this.fleetVehicle = fv;
    }

    public void addToken(String token){
        if(tokens == null){
            tokens = new ArrayList<>();
        }
        tokens.add(token);
    }

    public void removeToken(String token){
        if(tokens == null || tokens.size() == 0){
            return;
        }
        tokens.remove(token);
    }
}
