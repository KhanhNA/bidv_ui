package ac.dlp.locationService.model.firebase;

import lombok.*;

import java.io.Serializable;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TokenFbId implements Serializable {
    private Long merchantId;
    private String token;
}
