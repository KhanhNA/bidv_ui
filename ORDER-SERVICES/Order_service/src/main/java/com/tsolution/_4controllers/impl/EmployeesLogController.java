package com.tsolution._4controllers.impl;

import com.tsolution._1entities.EmployeeLog;
import com.tsolution._3services.EmployeesLogService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employeelogs")
public class EmployeesLogController extends BaseController {

    @Autowired
    private EmployeesLogService employeesLogService;

    @GetMapping("/logscode")
    @PreAuthorize("hasAuthority('get/employeelogs/logscode')")
    public ResponseEntity<Object> findAllEmployeeLogCode(
            @RequestParam("employeeCode") String employeeCode
    ) throws BusinessException {
        return this.employeesLogService.findByEmployeeCode(employeeCode);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/employeelogs')")
    public ResponseEntity<Object> create(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody EmployeeLog entity
    )
            throws BusinessException {
        return this.employeesLogService.create(authorization,acceptLanguage,entity) ;
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/employeelogs/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody EmployeeLog entity,
            @PathVariable("id") Long id
    )
            throws BusinessException {
        return this.employeesLogService.update(authorization,acceptLanguage,id , entity);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/employeelogs/all')")
    public ResponseEntity<Object> findAll() {
        return this.employeesLogService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/employeelogs/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.employeesLogService.findById(id);
    }

}
