package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingVan;
import com.tsolution._3services.BillPackageRoutingService;
import com.tsolution._3services.RoutingPlanDayService;
import com.tsolution._3services.RoutingVanService;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/rout_plan")
public class RoutingVanController extends BaseController implements IBaseController<RoutingVan> {

    @Autowired
    private RoutingVanService routingVanService;
    @Autowired
    private BillPackageRoutingService billPackageRoutingService;
    @Autowired
    RoutingPlanDayService routingPlanDayService;


    @Override
    public ResponseEntity<Object> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> create(List<RoutingVan> entity) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, Optional<RoutingVan> source) throws BusinessException {
        return null;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/rout_plan/{id}')")
    @ApiOperation(value = "Get rout by vanId",
            notes = "get rout",
            response = RoutingVan.class,
            responseContainer = "List")
    //lay theo van
    // Id
    public ResponseEntity<Object> getRoutingPlanDayByVan(@PathVariable("id") Long vanId) throws BusinessException{
        List<RoutingVan> list = this.routingVanService.getRoutPlanForVanToday(vanId);
        return  new ResponseEntity<>(list, HttpStatus.OK);
    }


    @PatchMapping("/package/{id}")
    @PreAuthorize("hasAuthority('patch/rout_plan/package/{id}')")
    @ApiOperation(value = "update bill package routing",
            notes = "update bill package routing ",
            response = BillPackageRouting.class,
            responseContainer = "List")
    public  ResponseEntity<Object> updateBillPackageRouting(@PathVariable("id") Long packageRoutingId,@RequestBody BillPackageRouting billPackageRouting) throws BusinessException {
        return this.billPackageRoutingService.update(packageRoutingId,billPackageRouting);
    }

    @GetMapping("/{id}/routing_plan_day")
    @PreAuthorize("hasAuthority('get/rout_plan/{id}/routing_plan_day')")
    public ResponseEntity<Object> getNextDestinationInRouting(@PathVariable("id") Long routingVanId ) throws BusinessException{
        List<RoutingPlanDay> lst = this.routingVanService.getNextDestinationInRouting(routingVanId);
        if(lst.isEmpty()){
            return new ResponseEntity<>(lst,HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lst,HttpStatus.OK);
    }
    @PatchMapping("/routing_plan_day")
    @PreAuthorize("hasAuthority('patch/rout_plan/routing_plan_day')")
    public ResponseEntity<Object> createRoutingPlanDay(@RequestParam("id") Long routingVanId, @RequestParam("status") Integer status) throws BusinessException{

        return new ResponseEntity<>(this.routingPlanDayService.updateStatusRoutingPlanDay(routingVanId,status), HttpStatus.OK);
    }
    @GetMapping("/routing_plan_day")
   // @PreAuthorize("hasAuthority('get/rout_plan/routing_plan_day')")
    public ResponseEntity<Object>  getHistoryRoutingPlanDay(@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fTime,
                                                            @RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime tTime,
                                                            @RequestParam(required = false) Long vanId) throws BusinessException {

       //List<RoutingPlanDay> result = this.routingVanService.getRoutingPlanDayHistory(fTime,tTime,driverId);
        List<RoutingPlanDay> result = this.routingPlanDayService.getRoutPlanForVanByDate(vanId, fTime, tTime);
       if(result.isEmpty()){
           return new ResponseEntity<>(result,HttpStatus.NO_CONTENT);
       }
        return new ResponseEntity<>(result,HttpStatus.OK);

    }
//    @GetMapping("/routing_plan_day")
//    public ResponseEntity<Object> getRoutPlanForVanByDate(@RequestParam(required = false) Long vanId,
//                                                          @RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime dateCheck) throws BusinessException {
//        List<RoutingPlanDay>  lstRoutingPlanDay = this.routingPlanDayService.getRoutPlanForVanByDate(vanId,dateCheck);
//        if(lstRoutingPlanDay.isEmpty()){
//            return new ResponseEntity<>(lstRoutingPlanDay,HttpStatus.NO_CONTENT);
//        }
//        return new ResponseEntity<>(lstRoutingPlanDay,HttpStatus.OK);
//    }
}