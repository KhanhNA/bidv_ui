package com.tsolution._4controllers.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.tsolution._1entities.Organization;
import com.tsolution._3services.OrganizationService;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/organizations")
public class OrganizationController extends BaseController implements IBaseController<Organization> {

	@Autowired
	private OrganizationService organizationService;

	@Override
	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/organizations/all')")
	public ResponseEntity<Object> findAll() {
		return this.organizationService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/organizations/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.organizationService.findById(id);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/organizations')")
	public ResponseEntity<Object> create(@RequestBody List<Organization> entity) throws BusinessException {
		return this.organizationService.create(entity);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/organizations/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<Organization> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.organizationService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/organizations')")
	public ResponseEntity<Object> find(@RequestParam(required = false) String code,
									   @RequestParam(required = false) String name,
									   @RequestParam(required = false) Boolean status,
									   @RequestParam Integer pageNumber,
									   @RequestParam Integer pageSize) throws BusinessException {
		Organization org = new Organization();
		org.setCode(code);
		org.setName(name);
		org.setStatus(status);

		return this.organizationService.find(org, pageNumber, pageSize);
	}

	@PostMapping("/active")
	@PreAuthorize("hasAuthority('post/organizations/active')")
	public ResponseEntity<Object> active(@RequestBody Organization org)
			throws BusinessException {
		return this.organizationService.activeOrDeactivate(org.getId(), true);
	}

	@PostMapping("/deactivate")
	@PreAuthorize("hasAuthority('post/organizations/deactivate')")
	public ResponseEntity<Object> deactivate(@RequestBody Organization org)
			throws BusinessException {
		return this.organizationService.activeOrDeactivate(org.getId(), false);
	}
}
