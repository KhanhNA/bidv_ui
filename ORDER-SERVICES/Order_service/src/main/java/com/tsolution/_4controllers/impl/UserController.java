package com.tsolution._4controllers.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.User;
import com.tsolution._1entities.enums.UserType;
import com.tsolution._3services.UserService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/users")
public class UserController {

/*	private static final String USER_INPUT_MISSING_BODY = "user.input.missing.body";
	@Autowired
	private UserService userService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/users/all')")
	public ResponseEntity<Object> findAll() {
		return this.userService.findAll();
	}

	@GetMapping("/{username}")
	@PreAuthorize("hasAuthority('get/users/{username}')")
	public ResponseEntity<Object> findById(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @PathVariable("username") String username)
			throws BusinessException {
		return this.userService.findById(authorization, acceptLanguage, username);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/users')")
	public ResponseEntity<Object> find(@RequestParam(required = false) String username,
			@RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName,
			@RequestParam(required = false) String tel, @RequestParam(required = false) String email,
			@RequestParam(required = false) String type, @RequestParam(required = false) Boolean status,
			@RequestParam(required = false) Long organizationId, @RequestParam(required = true) Integer pageNumber,
			@RequestParam(required = true) Integer pageSize) throws BusinessException {
		User user = new User();
		user.setUsername(username);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setTel(tel);
		user.setEmail(email);
		user.setType(StringUtils.isNullOrEmpty(type) ? null : UserType.valueOf(type));
		user.setStatus(status);
		user.setOrganizationId(organizationId);
		return this.userService.find(user, pageNumber, pageSize);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('post/users')")
	public ResponseEntity<Object> create(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @RequestBody(required = true) Optional<User> oUser)
			throws BusinessException {
		if (!oUser.isPresent() || (oUser.get().getUsername() == null)) {
			throw new BusinessException(this.translator.toLocale(UserController.USER_INPUT_MISSING_BODY));
		}
		return this.userService.create(authorization, acceptLanguage, oUser.get());
	}

	@PatchMapping
	@PreAuthorize("hasAuthority('patch/users')")
	public ResponseEntity<Object> update(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @RequestBody(required = true) Optional<User> oUser)
			throws BusinessException {
		if (!oUser.isPresent() || (oUser.get().getUsername() == null)) {
			throw new BusinessException(this.translator.toLocale(UserController.USER_INPUT_MISSING_BODY));
		}
		return this.userService.update(authorization, acceptLanguage, oUser.get().getUsername(), oUser.get());
	}

	@PostMapping("/active")
	@PreAuthorize("hasAuthority('post/users/active')")
	public ResponseEntity<Object> active(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @RequestBody(required = true) Optional<User> oUser)
			throws BusinessException {
		if (!oUser.isPresent() || (oUser.get().getUsername() == null)) {
			throw new BusinessException(this.translator.toLocale(UserController.USER_INPUT_MISSING_BODY));
		}
		return this.userService.active(authorization, acceptLanguage, oUser.get());
	}

	@PostMapping("/deactive")
	@PreAuthorize("hasAuthority('post/users/deactive')")
	public ResponseEntity<Object> deactive(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @RequestBody(required = true) Optional<User> oUser)
			throws BusinessException {
		if (!oUser.isPresent() || (oUser.get().getUsername() == null)) {
			throw new BusinessException(this.translator.toLocale(UserController.USER_INPUT_MISSING_BODY));
		}
		return this.userService.deactive(authorization, acceptLanguage, oUser.get());
	}

	@PostMapping("/reset-pass")
	@PreAuthorize("hasAuthority('post/users/reset-pass')")
	public ResponseEntity<Object> resetPassword(@RequestHeader("Authorization") String authorization,
			@RequestHeader("Accept-Language") String acceptLanguage, @RequestBody(required = true) Optional<User> oUser)
			throws BusinessException {
		if (!oUser.isPresent() || (oUser.get().getUsername() == null)) {
			throw new BusinessException(this.translator.toLocale(UserController.USER_INPUT_MISSING_BODY));
		}
		return this.userService.resetPassword(authorization, acceptLanguage, oUser.get());
	}*/

}
