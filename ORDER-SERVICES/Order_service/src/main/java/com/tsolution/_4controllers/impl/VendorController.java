package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Vendor;
import com.tsolution._3services.VendorService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vendor")
public class VendorController extends BaseController{

    @Autowired
    private VendorService vendorService;

    @GetMapping("/findAll")
    @PreAuthorize("hasAuthority('get/vendor/findAll')")
    public ResponseEntity<Object> findAll() {
        return vendorService.findAll();
    }

    @GetMapping("/findById/{id}")
    @PreAuthorize("hasAuthority('get/vendor/findById/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return vendorService.findById(id);
    }

    @PostMapping("/insert")
    @PreAuthorize("hasAuthority('post/vendor/insert')")
    public ResponseEntity<Object> create(
            @RequestBody Vendor vendor) throws BusinessException {
        return vendorService.create(vendor);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('post/vendor/update')")
    public ResponseEntity<Object> update(
            @RequestBody Vendor vendor) throws BusinessException {
        return vendorService.update(vendor);
    }
}
