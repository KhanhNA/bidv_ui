package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillLadingDetail;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution._3services.BillLadingDetailService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/bill-lading-stock")
public class BillLadingDetailController extends BaseController implements IBaseController<BillLadingDetail> {
    @Autowired
    private BillLadingDetailService billLadingDetailService;

    @Override
    @RequestMapping("/all")
    @PreAuthorize("hasAuthority('get/bill-lading-stock/all')")
    public ResponseEntity<Object> findAll() {
        return this.billLadingDetailService.findAll();
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/bill-lading-stock/{id}')")
    @Override
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.billLadingDetailService.findById(id);
    }

    @Override
    public ResponseEntity<Object> create(List<BillLadingDetail> entity) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, Optional<BillLadingDetail> source) throws BusinessException {
        return null;
    }
}