package com.tsolution._4controllers.impl;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution._1entities.dto.PushNotificationResponse;
import com.tsolution._3services.PushNotificationService;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/push")
public class PushNotificationController {

    @Autowired
    private PushNotificationService pushNotificationService;


    @PostMapping("/notification/topic")
    public ResponseEntity sendNotification(@RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendPushNotificationWithoutData(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PostMapping("/notification/android")
    @PreAuthorize("hasAuthority('post/push/notification/android')")
    @ApiOperation(value = "Send message for app by token",
            notes = "send message by token Firebase",
            response = PushNotificationRequest.class,
            responseContainer = "Object")
    public ResponseEntity sendTokenNotification(
            @RequestHeader("Authorization") String authorization,
            @RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendPushNotificationToToken(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PostMapping("/notification/app")
    @PreAuthorize("hasAuthority('post/push/notification/app')")
    @ApiOperation(value = "Send message for app by staff id",
            notes = "send message for staff(2,3) or employee(0,1) by objectId check by objectType",
            response = PushNotificationRequest.class,
            responseContainer = "Object")
    public ResponseEntity sendAppNotification(
            @RequestHeader("Authorization") String authorization,
            @RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendAppNotification(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PostMapping("/notification/web")
    @PreAuthorize("hasAuthority('post/push/notification/web')")
    @ApiOperation(value = "Send message for web by token",
            notes = "send message by token Firebase",
            response = PushNotificationRequest.class,
            responseContainer = "Object")
    public ResponseEntity sendNotificationWeb(
            @RequestHeader("Authorization") String authorization,
            @RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendPushNotificationWeb(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
    @PostMapping("/notification/all")
    @PreAuthorize("hasAuthority('post/push/notification/all')")
    @ApiOperation(value = "Send message for all web by token",
            notes = "send message by token Firebase to all device",
            response = PushNotificationRequest.class,
            responseContainer = "Object")
    public ResponseEntity sendNotificationAllWeb(
            @RequestHeader("Authorization") String authorization,
            @RequestBody PushNotificationRequest request) throws BusinessException, FirebaseMessagingException {
        pushNotificationService.sendMulticastAndHandleErrorsWeb(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
    @PostMapping("/notification/appAll")
    @PreAuthorize("hasAuthority('post/push/notification/appAll')")
    @ApiOperation(value = "Send message for all by token",
            notes = "send message by token Firebase to all device",
            response = PushNotificationRequest.class,
            responseContainer = "Object")
    public ResponseEntity sendNotificationAllDevice(
            @RequestHeader("Authorization") String authorization,
            @RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendAllAppNotification(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
  /*  @PostMapping("/notification/data")
    public ResponseEntity sendDataNotification(@RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendPushNotification(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }*/

    @GetMapping("/notification/send")
    @PreAuthorize("hasAuthority('get/push/notification/send')")
    public ResponseEntity sendSampleNotification(@RequestBody PushNotificationRequest request) throws BusinessException {
        pushNotificationService.sendPushNotificationWeb(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
}
