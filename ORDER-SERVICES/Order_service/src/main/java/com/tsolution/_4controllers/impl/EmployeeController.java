package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Employee;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution._3services.EmployeeService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController extends BaseController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    @PreAuthorize("hasAuthority('post/employees')")
    public ResponseEntity<Object> create(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody Employee entity
    )
            throws BusinessException {
        return this.employeeService.create(authorization,acceptLanguage,entity) ;
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('patch/employees')")
    public ResponseEntity<Object> update(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody Employee entity
    )
            throws BusinessException {
        return this.employeeService.update(authorization,acceptLanguage, entity);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/employees/all')")
    public ResponseEntity<Object> findAll() {
        return this.employeeService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/employees/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.employeeService.findById(id);
    }

    @PatchMapping("/{id}/active-or-deactivate")
    @PreAuthorize("hasAuthority('patch/employees/{id}/active-or-deactivate')")
    public ResponseEntity<Object> activeOrDeactivate(@RequestHeader("Authorization") String authorization,
                                             @RequestHeader("Accept-Language") String acceptLanguage,
                                                     @PathVariable("id") Long id,
                                             @RequestParam("enabled") int enabled)
            throws BusinessException {
        return this.employeeService.activeOrDeactivate(authorization, acceptLanguage, id, enabled ==1 ? true : false);
    }

    @PatchMapping(path = "/{id}/add-image", consumes = { "multipart/form-data" })
    @PreAuthorize("hasAuthority('patch/employees/{id}/add-image')")
    public ResponseEntity<Object> addEmployeeImage(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestParam(name = "files", required = false) MultipartFile[] files) throws BusinessException {
        return this.employeeService.addEmployeeImage(acceptLanguage,id, files);
    }

    @PatchMapping(path = "/{id}/delete-image", consumes = { "multipart/form-data" })
    @PreAuthorize("hasAuthority('patch/employees/{id}/delete-image')")
    public ResponseEntity<Object> deleteEmployeeImage(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestPart(name = "delete", required = false) ObjAttachment delete
            )throws BusinessException {
        return this.employeeService.deleteEmployeeImage(acceptLanguage,id, delete);
    }

}
