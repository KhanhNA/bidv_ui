package com.tsolution._4controllers.impl;


import com.tsolution._1entities.ObjAttachment;
import com.tsolution._3services.ObjAttachmentService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/obj_attachment")
public class ObjAttachmentController extends BaseController {

    @Autowired
    private ObjAttachmentService objAttachmentService;

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('get/customers/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.objAttachmentService.findById(id);
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('post/customers')")
    public ResponseEntity<Object> create(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody ObjAttachment objAttachment)
            throws BusinessException, ParseException {
        return this.objAttachmentService.create( objAttachment);
    }

 /*   @PatchMapping("/{id}")
//    @PreAuthorize("hasAuthority('patch/claim_reports/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestBody ObjAttachment objAttachment
    ) throws BusinessException, ParseException {
        return this.objAttachmentService.update(objAttachment);
    }*/
}
