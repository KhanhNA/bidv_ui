/*trigger bảng employee khi thêm sửa*/

use sharevan;
DELIMITER $$
 
CREATE TRIGGER TRIGGER_EMPLOYEE
AFTER INSERT
ON EMPLOYEE FOR EACH ROW
BEGIN
    IF NEW.id IS NOT NULL THEN
        INSERT INTO `sharevan`.`employee_log`(`app_param_value_id`,`employee_code`,`employee_name`,`has_image`,`has_attachment`,`birthday`,
						`province`,`nation`,`country`,`address`,`phone`,`email`,`driver_licence_id`,`driver_licence_type`,`create_date`,`hire_date`,
						`leave_date`,`labor_rate`,`billing_rate`,`SSN`,`latitude`,`longitude`,`state_province`,`license_note`,`create_user`,
						`update_date`,`imei`,`point`,`object_id`,`status`,`object_type`,`description`,`update_user`,`id_no`,`from_time`,
						`to_time`,`is_active`,`pre_id`,`employee_id`)
		VALUES 					(NEW.app_param_value_id, NEW.employee_code,NEW.employee_name,NEW.has_image,NEW.has_attachment,NEW.birthday,
			NEW.province,NEW.nation,NEW.country,NEW.address,NEW.phone,NEW.email,NEW.driver_licence_id,NEW.driver_licence_type,NEW.create_date,NEW.hire_date,
			NEW.leave_date,NEW.labor_rate,NEW.billing_rate,NEW.SSN,NEW.latitude,NEW.longitude,NEW.state_province,NEW.license_note,'TRIGGER_EMPLOYEE',
            NEW.update_date,NEW.imei,NEW.point,NEW.object_id,NEW.status,NEW.object_type,NEW.description,NEW.update_user,NEW.id_no,NEW.create_date,
            null,1,null,NEW.id);
    END IF;
END$$
 
DELIMITER ;


/* sửa thông tin employee */
use sharevan;
DELIMITER $$
 
CREATE TRIGGER TRIGGER_EMPLOYEE_UPDATE
AFTER UPDATE
ON EMPLOYEE FOR EACH ROW
BEGIN
	 DECLARE OLD_ID INT;
     SET OLD_ID = ( SELECT LOG.ID FROM EMPLOYEE_LOG LOG WHERE LOG.EMPLOYEE_ID= OLD.id and LOG.IS_ACTIVE=1 );
   
    IF NEW.id IS NOT NULL THEN
		UPDATE EMPLOYEE_LOG LOG SET LOG.IS_ACTIVE = 0 and LOG.update_Date= NEW.create_date
			AND LOG.to_time= NEW.CREATE_DATE 
			WHERE LOG.EMPLOYEE_ID= OLD.id and LOG.IS_ACTIVE=1;
        INSERT INTO `sharevan`.`employee_log`(`app_param_value_id`,`employee_code`,`employee_name`,`has_image`,`has_attachment`,`birthday`,
						`province`,`nation`,`country`,`address`,`phone`,`email`,`driver_licence_id`,`driver_licence_type`,`create_date`,`hire_date`,
						`leave_date`,`labor_rate`,`billing_rate`,`SSN`,`latitude`,`longitude`,`state_province`,`license_note`,`create_user`,
						`update_date`,`imei`,`point`,`object_id`,`status`,`object_type`,`description`,`update_user`,`id_no`,`from_time`,
						`to_time`,`is_active`,`pre_id`,`employee_id`)
		VALUES 					(NEW.app_param_value_id, NEW.employee_code,NEW.employee_name,NEW.has_image,NEW.has_attachment,NEW.birthday,
			NEW.province,NEW.nation,NEW.country,NEW.address,NEW.phone,NEW.email,NEW.driver_licence_id,NEW.driver_licence_type,NEW.create_date,NEW.hire_date,
			NEW.leave_date,NEW.labor_rate,NEW.billing_rate,NEW.SSN,NEW.latitude,NEW.longitude,NEW.state_province,NEW.license_note,'TRIGGER',
            NEW.update_date,NEW.imei,NEW.point,NEW.object_id,NEW.status,NEW.object_type,NEW.description,NEW.update_user,NEW.id_no,NEW.update_date,
            null,1,OLD_ID,OLD.id);
    END IF;
    UPDATE EMPLOYEE_LOG LOG SET  LOG.to_time= sysdate()
			WHERE LOG.EMPLOYEE_ID= OLD.ID and LOG.IS_ACTIVE=0 
				AND LOG.ID= (SELECT LOG.PRE_ID FROM EMPLOYEE_LOG LOG WHERE LOG.EMPLOYEE_ID= OLD.ID and LOG.IS_ACTIVE=1);
END$$
 
DELIMITER ;