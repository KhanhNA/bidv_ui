package com.tsolution.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
	private String uploadDir;
	private String tempExportExcel;
	private String excelTemplate;

	public String getUploadDir() {
		return this.uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}

	public String getTempExportExcel() {
		return this.tempExportExcel;
	}

	public void setTempExportExcel(String tempExportExcel) {
		this.tempExportExcel = tempExportExcel;
	}

	public String getExcelTemplate() {
		return excelTemplate;
	}

	public void setExcelTemplate(String excelTemplate) {
		this.excelTemplate = excelTemplate;
	}
}
