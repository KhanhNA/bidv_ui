package com.tsolution.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.tsolution.utils.Constants;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDateTime localDateTime) {
		return LocalDateTimeConverter.convertLocalDateTimeToDate(localDateTime);
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Date sqlDate) {
		if (sqlDate == null) {
			return null;
		}
		java.util.Date date = new java.util.Date(sqlDate.getTime());
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static Date toJavaUtilDate(LocalDateTime localDateTime) {
		return LocalDateTimeConverter.convertLocalDateTimeToDate(localDateTime);
	}

	public static java.sql.Date convertToJavaSqlDate(LocalDateTime localDateTime) {
		java.util.Date date = LocalDateTimeConverter.convertLocalDateTimeToDate(localDateTime);
		return date == null ? null : new java.sql.Date(date.getTime());
	}

	private static Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
		if (localDateTime == null) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_PATTERN);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constants.DATE_PATTERN);
		java.util.Date utilDate = null;
		try {
			utilDate = simpleDateFormat.parse(localDateTime.format(dateTimeFormatter));
		} catch (ParseException e) {
			utilDate = null;
		}
		return utilDate;
	}
}