package com.tsolution._3services;

import com.tsolution._1entities.Employee;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface EmployeeService{
    ResponseEntity<Object> create(String authorization, String acceptLanguage, Employee entities) throws BusinessException;
    ResponseEntity<Object> update(String authorization, String acceptLanguage, Employee entities) throws BusinessException;
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> addEmployeeImage(String acceptLanguage,Long id, MultipartFile[] multipartFiles) throws BusinessException;
    ResponseEntity<Object> deleteEmployeeImage(String acceptLanguage,Long id,ObjAttachment delete) throws BusinessException;
    ResponseEntity<Object> activeOrDeactivate(String authorization, String acceptLanguage, Long id, boolean isActive)
            throws BusinessException;

}
