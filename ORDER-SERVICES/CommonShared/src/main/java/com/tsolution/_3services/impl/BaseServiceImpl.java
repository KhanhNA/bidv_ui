package com.tsolution._3services.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tsolution._1entities.*;
import com.tsolution._2repositories.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution.config.AuthenticationFacade;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Translator;

public abstract class BaseServiceImpl {

	private static final Logger log = LogManager.getLogger(BaseServiceImpl.class);

	private OAuth2AuthenticationDto oAuth2AuthenticationDto;
	@Autowired
	protected AuthenticationFacade authenticationFacade;

	@Autowired
	protected Translator translator;

	@Autowired
	protected OrganizationRepository organizationRepository;

	@Autowired
	protected CustomerRepository customerRespository;

	@Autowired
	protected UserRepository userRepository;

	@Autowired
	protected ApparamGroupRepository apparamGroupRepository;

	@Autowired
	protected AppParamRepository appParamRepository;

	@Autowired
	protected ApparamValueRepository apparamValueRepository;

	@Autowired
	protected StaffRepository staffRepository;

	@Autowired
	protected DriverLicenseRepository driverLicenseRepository;

	@Autowired
	protected ObjAttachmentRepository objAttachmentRepository;

	@Autowired
	protected VendorRepository vendorRepository;


	protected OAuth2AuthenticationDto getCurrentOAuth2Details() throws BusinessException {
		if (this.authenticationFacade.getAuthentication() instanceof OAuth2Authentication) {
			OAuth2Authentication auth2Authentication = (OAuth2Authentication) this.authenticationFacade
					.getAuthentication();
			Authentication authentication = auth2Authentication.getUserAuthentication();
			if (authentication.getDetails() instanceof Map<?, ?>) {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = (Map<String, Object>) authentication.getDetails();
				String json = null;
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
					return mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT).readValue(json,
							OAuth2AuthenticationDto.class);
				} catch (IOException e) {
					BaseServiceImpl.log.error(e.getMessage(), e);
					throw new BusinessException("ERROR OAuth2AuthenticationDto mapper");
				}
			}
		}
		return null;
	}

	protected <T> List<T> getListValueFromResponse(List<?> listObj, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listObj);
			JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
			return mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT).readValue(json, type);
		} catch (Exception e) {
			BaseServiceImpl.log.error(e.getMessage(), e);
			return Collections.emptyList();
		}
	}

	protected String getCurrentUsername() throws BusinessException {
		return this.getCurrentOAuth2Details().getUserAuthentication().getPrincipal().getUsername();
	}

	protected Organization getCurrentOrganization() throws BusinessException {
		String userName = this.getCurrentUsername();
		Optional<User> oUser = this.userRepository.findOneByUsername(userName);
		if (oUser.isPresent()) {
			return oUser.get().getOrganization();
		}
		return null;
	}

	protected Customer getCurrentCustomer() throws BusinessException {
		String userName = this.getCurrentUsername();
		Optional<Customer> oCustomer = this.customerRespository.findByCode(userName);
		if (oCustomer.isPresent()) {
			return oCustomer.get();
		} else {
			return null;
		}
	}

	@Bean
	protected LocalDateTime getSysdate() {
		return this.organizationRepository.getSysdate();
	}

	protected void validFromDateToDate(LocalDateTime fromDate, LocalDateTime toDate, Boolean isAdd,
			LocalDateTime sysdate) throws BusinessException {
		if (fromDate == null) {
			throw new BusinessException(this.translator.toLocale("common.input.missing.from.date"));
		}
		if (toDate == null) {
			throw new BusinessException(this.translator.toLocale("common.input.missing.to.date"));
		}
		if (Boolean.TRUE.equals(isAdd) && fromDate.isBefore(sysdate)) {
			throw new BusinessException(this.translator.toLocale("common.input.from.date.can.not.less.than.sysdate"));
		}
		if (toDate.isBefore(sysdate)) {
			throw new BusinessException(this.translator.toLocale("common.input.to.date.can.not.less.than.sysdate"));
		}
		if (toDate.isBefore(fromDate)) {
			throw new BusinessException(
					this.translator.toLocale("common.input.to.date.must.be.greater.or.equal.from.date"));
		}

	}

	protected void validFromDateToDatePlusOne(LocalDateTime fromDate, LocalDateTime toDate, Boolean isAdd,
			LocalDateTime sysdate) throws BusinessException {
		this.validFromDateToDate(fromDate, toDate, isAdd, sysdate);
		if (fromDate.isEqual(sysdate)) {
			throw new BusinessException(this.translator.toLocale("from.date.must.be.greater.than.today"));
		}
		if (toDate.isEqual(sysdate)) {
			throw new BusinessException(this.translator.toLocale("to.date.must.be.greater.or.equal.today"));
		}
	}

	protected Boolean isInRange(LocalDateTime date, LocalDateTime fromDate, LocalDateTime toDate) {
		LocalDate xDate = date.toLocalDate();
		LocalDate xFromDate = fromDate.toLocalDate();
		LocalDate xToDate = toDate.toLocalDate();
		return !(xDate.isBefore(xFromDate) || xDate.isAfter(xToDate));
	}

	protected <T> T getObject(String jSon, Class<T> clazz) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(jSon, clazz);
		} catch (IOException e) {
			BaseServiceImpl.log.error(e.getMessage(), e);
			return null;
		}
	}

	protected Optional<Staff> getCurrentStaff() throws BusinessException {
		oAuth2AuthenticationDto = this.getCurrentOAuth2Details();
		String userName = oAuth2AuthenticationDto.getName();
		Optional<Staff> optionalStaff = this.staffRepository.findByUserName(userName);
		if (!optionalStaff.isPresent()) {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}
		return optionalStaff;
	}

}
