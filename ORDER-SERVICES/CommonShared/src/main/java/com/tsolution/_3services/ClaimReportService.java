package com.tsolution._3services;

import com.tsolution._1entities.ClaimReport;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ClaimReportService {
     ResponseEntity<Object> findAll();

     ResponseEntity<Object> findById(Long id) throws BusinessException;

     ResponseEntity<Object> create(String acceptLanguage,ClaimReport entity) throws BusinessException;

     ResponseEntity<Object> update(String acceptLanguage,Long id, ClaimReport source) throws BusinessException;

     ResponseEntity<Object> updateClaimReportImage(String acceptLanguage,Long id, List<ObjAttachment> deletes,
                                                       List<ObjAttachment> addOrEdits, MultipartFile[] multipartFiles) throws BusinessException;

    ResponseEntity<Object> findByRoutingPlanDetail(Long id) throws BusinessException;
}
