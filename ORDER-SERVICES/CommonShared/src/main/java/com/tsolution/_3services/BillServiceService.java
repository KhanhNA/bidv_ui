package com.tsolution._3services;

import com.tsolution._1entities.BillService;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface BillServiceService {

    ResponseEntity<Object> findServiceByType(int type);

    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findByActive();
    ResponseEntity<Object> findById(Long id) throws BusinessException;

    ResponseEntity<Object> find(BillService service, Integer pageNumber, Integer pageSize) throws BusinessException;

    ResponseEntity<Object> create(BillService entities);

    ResponseEntity<Object> update(BillService source)
            throws BusinessException;
}
