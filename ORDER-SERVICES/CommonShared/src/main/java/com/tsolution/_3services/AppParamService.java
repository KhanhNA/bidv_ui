package com.tsolution._3services;

import com.tsolution._1entities.AppParam;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface AppParamService {
    ResponseEntity<Object> create( AppParam entities) throws BusinessException;
    ResponseEntity<Object> findAll() throws BusinessException;
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> find(String apparamCode,Integer status) throws BusinessException;
    ResponseEntity<Object> update(Long id,AppParam entities) throws BusinessException;


}
