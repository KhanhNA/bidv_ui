package com.tsolution._3services;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ApparamGroupService {

    ResponseEntity<Object> create(List<ApparamGroup> entities) throws BusinessException;
    ResponseEntity<Object> update(Long ApparamGroupId, ApparamGroup entities) throws BusinessException;
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> find( ApparamGroup entities,Integer pageNumber, Integer pageSize) throws BusinessException;

}
