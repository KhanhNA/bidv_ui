package com.tsolution._3services;

import com.tsolution._1entities.EmployeeLog;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface EmployeesLogService {

    ResponseEntity<Object> findByEmployeeCode(String  code) throws BusinessException;
    ResponseEntity<Object> create(String authorization, String acceptLanguage, EmployeeLog entities) throws BusinessException;
    ResponseEntity<Object> update(String authorization, String acceptLanguage,Long employeeId, EmployeeLog entities) throws BusinessException;
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
}
