package com.tsolution._3services.impl;

import com.tsolution._1entities.BillLadingDetail;
import com.tsolution._2repositories.BillLadingDetailRepository;
import com.tsolution._3services.BillLadingDetailService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BillLadingDetailServiceImpl extends BaseServiceImpl implements BillLadingDetailService {


    private static  final  String BILL_LADING_DETAIL_ID_NOT_EXISITS = "bill.lading.detail.id.is.not.exist";
    @Autowired
    private BillLadingDetailRepository billLadingDetailRepository;

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.billLadingDetailRepository.findAll(), HttpStatus.OK);    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<BillLadingDetail> optionalBillLadingDetail = this.billLadingDetailRepository.findById(id);
        if (!optionalBillLadingDetail.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(BillLadingDetailServiceImpl.BILL_LADING_DETAIL_ID_NOT_EXISITS, id));
        }
        return new ResponseEntity<>(optionalBillLadingDetail.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(List<BillLadingDetail> entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, BillLadingDetail source) throws BusinessException {
        return null;
    }
}
