package com.tsolution._3services;

import com.tsolution._1entities.Notification;
import com.tsolution._1entities.Rating;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface RatingService {

    ResponseEntity<Object> create(String acceptLanguage, Rating entities) throws BusinessException;

    ResponseEntity<Object> update(String acceptLanguage, Long notificationId, Rating entities) throws BusinessException;

    ResponseEntity<Object> findAll();

    ResponseEntity<Object> findById(Long id) throws BusinessException;

    ResponseEntity<Object> find(Rating entities) throws BusinessException;

    ResponseEntity<Object> findByStaffId(Long staffId) throws BusinessException;

    ResponseEntity<Object> findByDriverId(Long id);

    ResponseEntity<Object> find100LatestOfDriver(Long id);

}
