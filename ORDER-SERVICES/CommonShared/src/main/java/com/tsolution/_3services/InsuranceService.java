package com.tsolution._3services;

import com.tsolution._1entities.Insurance;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface InsuranceService {
    ResponseEntity<Object> create(String acceptLanguage,Insurance entities) throws BusinessException;
    ResponseEntity<Object> update(String acceptLanguage,Long InsuranceId, Insurance entities) throws BusinessException;
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findAvaiable();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
}
