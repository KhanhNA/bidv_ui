package com.tsolution._3services.impl;

import com.tsolution._1entities.DriverVan;
import com.tsolution._2repositories.DriverVanRepository;
import com.tsolution._3services.DriverVanService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
@Service
public class DriverVanServiceImpl extends BaseServiceImpl implements DriverVanService {
    @Autowired
    private  DriverVanRepository driverVanRepository;
    @Override
    public ResponseEntity<Object> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> create(List<DriverVan> entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, DriverVan source) throws BusinessException {
        return null;
    }

    @Override
    public List<DriverVan> getVanForDriverByDate(Long driverId, LocalDateTime dateCheck) throws BusinessException {
        if(driverId == null || dateCheck == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        List<DriverVan> lstDriverVan = this.driverVanRepository.getVanForDriver(driverId,dateCheck);


        return lstDriverVan;
    }

}
