package com.tsolution._3services.impl;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._3services.ApparamGroupService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ApparamGroupServiceImpl extends BaseServiceImpl implements ApparamGroupService {
    private SingleSignOnUtils singleSignOnUtils;


    @Autowired
    public ApparamGroupServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(List<ApparamGroup> entities) throws BusinessException {
        List<ApparamGroup> apparamGroups = new ArrayList<>();
        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        for (ApparamGroup entity: entities){
            Optional<ApparamGroup> oApparamGroup;
            if (entity.getGroupCode() != null) {
                String appGrCode = entity.getGroupCode();
                oApparamGroup = this.apparamGroupRepository.findByGroupCode(appGrCode);
                if (oApparamGroup.isPresent()) {
                    throw new BusinessException(this.translator.toLocaleByFormatString("apparamgr.is.already.exist.code", entity.getGroupCode()));
                }
            }
            else{
                throw new BusinessException(this.translator.toLocaleByFormatString("apparamgr.input.missing.grcode"));
            }
            if (entity.getStatus()==null || "".equals(entity.getStatus())){
                entity.setStatus(StatusType.RUNNING.getValue());
            }
            entity.setAppParams(null);
            entity = this.apparamGroupRepository.save(entity);
            apparamGroups.add(entity);
        }

        return new ResponseEntity<>(apparamGroups, HttpStatus.OK);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(Long ApparamGrId, ApparamGroup entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(
                    this.translator.toLocale("common.input.info.invalid"));
        }
        if (ApparamGrId == null || entities.getId() == null || !ApparamGrId.equals(entities.getId())) {
            throw new BusinessException(
                    this.translator.toLocale("apparamgr.input.missing.id"));
        }
        if (!StringUtils.isNullOrEmpty(entities.getGroupCode())) {
            throw new BusinessException(this.translator.toLocale("apparamgr.input.grcode"));
        }

        Optional<ApparamGroup> optionalApparamGroup = this.apparamGroupRepository.findById(ApparamGrId);
        if (!optionalApparamGroup.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("apparamgr.does.not.exist", ApparamGrId));
        }

        ApparamGroup newApparam = optionalApparamGroup.get();
        newApparam.setGroupName(entities.getGroupName());
        newApparam.setOrd(entities.getOrd());
        newApparam.setStatus(entities.getStatus());
        newApparam = this.apparamGroupRepository.save(newApparam);
        return new ResponseEntity<>(newApparam, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.apparamGroupRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<ApparamGroup> oApparamGroup = this.apparamGroupRepository.findById(id);
        if (oApparamGroup.isPresent()) {
            return new ResponseEntity<>(oApparamGroup.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @Override
    public ResponseEntity<Object> find(ApparamGroup entities,Integer pageNumber, Integer pageSize) throws BusinessException {
        return new ResponseEntity<>(this.apparamGroupRepository.find(entities, PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
    }
}
