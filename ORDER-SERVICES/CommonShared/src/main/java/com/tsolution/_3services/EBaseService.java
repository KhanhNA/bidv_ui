package com.tsolution._3services;

import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.text.ParseException;

public interface EBaseService<T extends Serializable> {
	public ResponseEntity<Object> findAll();

	public ResponseEntity<Object> findById(Long id) throws BusinessException;

	public ResponseEntity<Object> create(String acceptLanguage, T entity) throws BusinessException, ParseException;

	public ResponseEntity<Object> update(String acceptLanguage, Long id, T source) throws BusinessException, ParseException;

}
