package com.tsolution._3services.impl;

import com.tsolution._1entities.BillPackage;
import com.tsolution._2repositories.BillPackageRepository;
import com.tsolution._3services.BillPackageService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BillPackageServiceImpl extends BaseServiceImpl implements BillPackageService {

    private static  final  String LIST_PACKAGE_ID_NOT_EXISITS = "list.package.id.is.not.exist";

    @Autowired
    BillPackageRepository billPackageRepository;

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.billPackageRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<BillPackage> optionalBillPackage = this.billPackageRepository.findById(id);
        if (!optionalBillPackage.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(BillPackageServiceImpl.LIST_PACKAGE_ID_NOT_EXISITS, id));
        }
        return new ResponseEntity<>(optionalBillPackage.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(List<BillPackage> entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, BillPackage source) throws BusinessException {
        return null;
    }
}
