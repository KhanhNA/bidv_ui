package com.tsolution._3services;

import com.tsolution._1entities.BillLading;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

public interface BillLadingService extends EBaseService<BillLading> {
     ResponseEntity<Object> findByEntity(BillLading entities) throws BusinessException;
    ResponseEntity<Object> cancel(Long id) throws BusinessException;
     ResponseEntity<Object> caculateMoneyPreCustomerAcceptOrder(String acceptLanguage,BillLading billLading) throws BusinessException ;
    Page<BillLading> find(String billLadingCode,Long customerId, Integer status, LocalDateTime fDate, LocalDateTime tDate,Integer billCycleType, Integer pageNumber, Integer pageSize) throws BusinessException;
}
