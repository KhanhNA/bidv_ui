package com.tsolution._3services.impl;

import java.util.List;
import java.util.Optional;

import com.tsolution._1entities.enums.OrganizationStatus;
import com.tsolution.utils.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.Organization;
import com.tsolution._3services.OrganizationService;
import com.tsolution.excetions.BusinessException;

@Service
public class OrganizationServiceImpl extends BaseServiceImpl implements OrganizationService {

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.organizationRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<Organization> oOrganization = this.organizationRepository.findById(id);
		if (!oOrganization.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("organization.id.not.exists", id));
		}
		return new ResponseEntity<>(oOrganization.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<Organization> organizations) throws BusinessException{
		if (organizations != null && organizations.size() > 0) {
			for (Organization org : organizations) {
				Optional<Organization> oOrg;
				if (org.getId() != null) {
					Long orgId = org.getId();
					oOrg = this.organizationRepository.findById(orgId);
					if (oOrg.isPresent()) {
						throw new BusinessException(this.translator.toLocaleByFormatString("organization.id.existed", orgId));
					}
				}

				org.setStatus(false);
				this.validOrganization(org);

				oOrg  = this.organizationRepository.findByCode(org.getCode());
				if (oOrg.isPresent()) {
					throw new BusinessException(this.translator.toLocaleByFormatString("organization.code.existed", org.getCode()));
				}
			}
			this.organizationRepository.saveAll(organizations);
			return new ResponseEntity<>(organizations, HttpStatus.OK);
		}
		return new ResponseEntity<>(this.translator.toLocale("common.input.info.invalid"), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long orgId, Organization orgInput) throws BusinessException{
		if (orgId == null || !orgId.equals(orgInput.getId())) {
			throw new BusinessException(this.translator.toLocale("organization.input.missing.id"));
		}

		this.validOrganization(orgInput);
		Optional<Organization> oOrg = this.organizationRepository.findById(orgId);
		if (!oOrg.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("organization.id.not.exists", orgId));
		}

		Organization org = oOrg.get();
		org.setName(orgInput.getName());
		this.organizationRepository.save(org);

		return new ResponseEntity<>(org, HttpStatus.OK);
	}

	private void validOrganization (Organization org) throws BusinessException{
		if (StringUtils.isNullOrEmpty(org.getCode())) {
			throw new BusinessException(this.translator.toLocale("organization.input.missing.code"));
		}
		if (StringUtils.isNullOrEmpty(org.getName())) {
			throw new BusinessException(this.translator.toLocale("organization.input.missing.name"));
		}
		if (org.getStatus() == null) {
			throw new BusinessException(this.translator.toLocale("organization.input.missing.status"));
		}
	}

	@Override
	public ResponseEntity<Object> find(Organization org, Integer pageNumber, Integer pageSize) throws BusinessException {
		return new ResponseEntity<>(
				this.organizationRepository.find(org, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> activeOrDeactivate(Long id, boolean isActive) throws BusinessException {
        if (id == null) {
            throw new BusinessException(this.translator.toLocale("organization.input.missing.id"));
        }
		Optional<Organization> oOrg = this.organizationRepository.findById(id);
		if (!oOrg.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("customer.id.not.exists", id));
		}

		Organization org = oOrg.get();
		org.setStatus(isActive ? OrganizationStatus.ACTIVE.getValue() : OrganizationStatus.DEACTIVE.getValue());
		this.organizationRepository.save(org);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
