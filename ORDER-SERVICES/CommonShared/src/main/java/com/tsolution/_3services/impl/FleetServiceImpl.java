package com.tsolution._3services.impl;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution._1entities.Fleet;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.FleetRepository;
import com.tsolution._3services.FleetService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FleetServiceImpl extends BaseServiceImpl implements FleetService {

    private SingleSignOnUtils singleSignOnUtils;

    @Autowired
    protected FleetRepository fleetRepository;

    @Autowired
    public FleetServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    public ResponseEntity<Object> findAll() throws BusinessException {
        return new ResponseEntity<>(this.fleetRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.fleetRepository.findById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(String acceptLanguage, Fleet entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }

        if (StringUtils.isNullOrEmpty(entities.getFleetCode())){
            throw new BusinessException(this.translator.toLocale("fleet.input.missing.code"));
        }

        Optional<Fleet> oFleet;
        if (!StringUtils.isNullOrEmpty(entities.getFleetCode())) {
            String fleetCode = entities.getFleetCode();
            oFleet = this.fleetRepository.findByFleetCode(fleetCode);
            if (oFleet.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("fleet.is.already.exist.code"));
            }
        }

        if (entities.getStatus()==null || "".equals(entities.getStatus())){
            entities.setStatus(StatusType.STOPPED.getValue());
        }
        entities = this.fleetRepository.save(entities);
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(String acceptLanguage, Long id, Fleet entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(
                    this.translator.toLocale("common.input.info.invalid"));
        }
        if (entities.getId() == null || !id.equals(entities.getId())) {
            throw new BusinessException(
                    this.translator.toLocale("fleet.input.missing.id"));
        }
        Optional<Fleet> oFleet= this.fleetRepository.findById(id);
        if (!oFleet.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("fleet.does.not.exist", id));
        }

        Fleet newFleet = oFleet.get();
        newFleet.setFleetName(entities.getFleetName());
        newFleet.setStatus(entities.getStatus());
        newFleet.setBusinessRegistration(entities.getBusinessRegistration());
        newFleet.setAddressRegistration(entities.getAddressRegistration());
        newFleet.setAddress1(entities.getAddress1());
        newFleet.setAddress2(entities.getAddress2());
        newFleet.setContactName(entities.getContactName());
        newFleet.setCity(entities.getCity());
        newFleet.setPostalCode(entities.getPostalCode());
        newFleet.setTaxCode(entities.getTaxCode());
        newFleet.setPhone1(entities.getPhone1());
        newFleet.setPhone2(entities.getPhone2());
        newFleet.setFax(entities.getFax());
        newFleet.setWebsite(entities.getWebsite());
        newFleet.setEmail(entities.getEmail());
        newFleet = this.fleetRepository.save(newFleet);
        return new ResponseEntity<>(newFleet, HttpStatus.OK);
    }
}
