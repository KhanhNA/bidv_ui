package com.tsolution._3services.impl;

import com.google.firebase.messaging.*;
import com.tsolution._1entities.Employee;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.TokenFbSharevan;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution._1entities.enums.MESSAGE_TYPE;
import com.tsolution._1entities.enums.NotificationType;
import com.tsolution._1entities.enums.TokenFbStatus;
import com.tsolution._2repositories.EmployeeRepository;
import com.tsolution._2repositories.TokenFbSharevanRepository;
import com.tsolution._3services.NotificationService;
import com.tsolution._3services.PushNotificationService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class PushNotificationServiceImpl extends BaseServiceImpl implements PushNotificationService {

   /* @Value("#{${app.notifications.defaults}}")
    private Map<String, String> defaults;*/

    private final Logger logger = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

    private final FCMService fcmService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TokenFbSharevanRepository tokenFbSharevanRepository;

    public PushNotificationServiceImpl(FCMService fcmService) {
        this.fcmService = fcmService;
    }

    public void sendPushNotificationWeb(PushNotificationRequest request) {
        try {
            fcmService.sendToToken(request);
            notificationService.saveMessage(request);
        } catch (FirebaseMessagingException | BusinessException e) {
            logger.error(e.getMessage());
        }
    }

   /* public void sendPushNotification(PushNotificationRequest request) {
        try {
            fcmService.sendMessage(getSamplePayloadData(), request);
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
        }
    }*/

    //    @Scheduled(initialDelay = 60000, fixedDelay = 60000)
    public void sendPushNotificationWeb() {
        try {
            Map<String, String> data = new HashMap<>();
            String datetime = LocalDateTime.now().toString();
            data.put("hello", "world" + datetime);
            data.put("tile", "chào mừng đến với sharing van group");
            // This registration token comes from the client FCM SDKs.
            String registrationToken = "ewHmKRx3SuE:APA91bFTMfD81QeBn78PxLgZYxmHHu2mgrpOlXfD93LShBgNWHqvzLfIZYfJ0368zW77cp3GNs4yidr6xHDiPMktdgGD5rIDIBbBlrgJUCivm5k57C8612JPaDK2PwCCcJ2uWBI6U9AJ";
            PushNotificationRequest pushNotificationRequest = new PushNotificationRequest();
            pushNotificationRequest.setTitle("Thông báo hệ thống");
            pushNotificationRequest.setToken(registrationToken);
//            fcmService.sendMessage(data, getPushNotificationRequest());
//            fcmService.sendMessageWithoutData(getPushNotificationRequest());
            fcmService.sendToToken(pushNotificationRequest);
        } catch (FirebaseMessagingException | BusinessException e) {
            logger.error(e.getMessage());
        }
    }

    public void sendPushNotificationWithoutData(PushNotificationRequest request) {
        try {
            fcmService.sendMessageWithoutData(request);
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
        }
    }


    public void sendPushNotificationToToken(PushNotificationRequest request) {
        try {
            fcmService.sendMessageToAndroid(request);
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
        }
    }

    public ResponseEntity<Object> sendMulticastAndHandleErrorsWeb(PushNotificationRequest pushNotificationRequest) throws FirebaseMessagingException {
        // get all website registration tokens.
        List<TokenFbSharevan> lstTokenFbSharevans = this.tokenFbSharevanRepository.findAll();
        List<String> registrationTokens = new ArrayList<>();
        for (TokenFbSharevan tokenFbSharevan : lstTokenFbSharevans) {
            if (tokenFbSharevan.getToken() != null) {
                registrationTokens.add(tokenFbSharevan.getToken());
            }
        }

        Map<String, String> data = new HashMap<>();
        String datetime = LocalDateTime.now().toString();
        data.put("time", datetime);
        data.put("title", pushNotificationRequest.getTitle());
        data.put("message", pushNotificationRequest.getMessage());
        data.put("topic", pushNotificationRequest.getTopic());
        data.put("score", "850");
        MulticastMessage message = MulticastMessage.builder()
                .putAllData(data)
                .addAllTokens(registrationTokens)
                .build();
        BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
        if (response.getFailureCount() > 0) {
            List<SendResponse> responses = response.getResponses();
            List<String> failedTokens = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                if (!responses.get(i).isSuccessful()) {
                    // The order of responses corresponds to the order of the registration tokens.
                    failedTokens.add(registrationTokens.get(i));
                }
            }

            System.out.println("List of tokens that caused failures: " + failedTokens);
        }
        notificationService.saveMessage(pushNotificationRequest);
        // [END send_multicast_error]
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> sendAppNotification(PushNotificationRequest pushNotificationRequest) throws BusinessException {
        if (pushNotificationRequest == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (pushNotificationRequest.getObjectType() == null ||
                pushNotificationRequest.getObjectId() == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (StringUtils.isNullOrEmpty(pushNotificationRequest.getMessage())
                || StringUtils.isNullOrEmpty(pushNotificationRequest.getTopic())
                || StringUtils.isNullOrEmpty(pushNotificationRequest.getTitle())) {
            throw new BusinessException(this.translator.toLocale("push.notification.information.not.exist"));
        }

        if (pushNotificationRequest.getObjectType().equals(TokenFbStatus.EMPLOYEE.getValue()) || pushNotificationRequest.getObjectType().equals(TokenFbStatus.STORE_KEEPER.getValue())) {
            Optional<Employee> oEmployee = this.employeeRepository.findById(pushNotificationRequest.getObjectId());
            if (!oEmployee.isPresent()) {
                this.translator.toLocaleByFormatString("token.account.id.not.exisits", pushNotificationRequest.getObjectId());
            }
            TokenFbSharevan tokenFbSharevan = this.tokenFbSharevanRepository.findByImei(oEmployee.get().getImei());
            if (tokenFbSharevan == null) {
                throw new BusinessException(this.translator.toLocale("firebase.token.not.exists"));
            }
            pushNotificationRequest.setToken(tokenFbSharevan.getToken());
            this.sendPushNotificationToToken(pushNotificationRequest);
            pushNotificationRequest.setNotifycationType(NotificationType.CUSTOMER.getValue());
            notificationService.saveMessage(pushNotificationRequest);
        } else if (pushNotificationRequest.getObjectType().equals(TokenFbStatus.DRIVER.getValue()) || pushNotificationRequest.getObjectType().equals(TokenFbStatus.FLEET_MANAGER.getValue())) {
            Optional<Staff> oStaff = this.staffRepository.findById(pushNotificationRequest.getObjectId());
            if (!oStaff.isPresent()) {
                this.translator.toLocaleByFormatString("token.account.id.not.exisits", pushNotificationRequest.getObjectId());
            }
            TokenFbSharevan tokenFbSharevan = this.tokenFbSharevanRepository.findByImei(oStaff.get().getImei());
            if (tokenFbSharevan == null) {
                throw new BusinessException(this.translator.toLocale("firebase.token.not.exists"));
            }
            pushNotificationRequest.setToken(tokenFbSharevan.getToken());
            this.sendPushNotificationToToken(pushNotificationRequest);
            pushNotificationRequest.setNotifycationType(NotificationType.DRIVER.getValue());
            notificationService.saveMessage(pushNotificationRequest);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> sendAllAppNotification(PushNotificationRequest pushNotificationRequest) throws BusinessException {
        if (pushNotificationRequest == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (pushNotificationRequest.getMessageType() == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (StringUtils.isNullOrEmpty(pushNotificationRequest.getMessage())
                || StringUtils.isNullOrEmpty(pushNotificationRequest.getTopic())
                || StringUtils.isNullOrEmpty(pushNotificationRequest.getTitle())) {
            throw new BusinessException(this.translator.toLocale("push.notification.information.not.exist"));
        }

        if (pushNotificationRequest.getMessageType().equals(MESSAGE_TYPE.CUSTOMER_APP.getValue())) {
            this.sendAllMessageAndSave(pushNotificationRequest, TokenFbStatus.STORE_KEEPER.getValue());
        } else if (pushNotificationRequest.getMessageType().equals(MESSAGE_TYPE.DRIVER_APP.getValue())) {
            this.sendAllMessageAndSave(pushNotificationRequest, TokenFbStatus.DRIVER.getValue());
        } else if (pushNotificationRequest.getMessageType().equals(MESSAGE_TYPE.SALE_APP.getValue())) {
            this.sendAllMessageAndSave(pushNotificationRequest, TokenFbStatus.SALE_MAN.getValue());
        } else {
            throw new BusinessException(this.translator.toLocale("message.type.not.exists"));
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void sendAllMessageAndSave(PushNotificationRequest pushNotificationRequest, Integer messageType) throws BusinessException {
        List<TokenFbSharevan> lstTokenFbSharevan = this.tokenFbSharevanRepository.findByObjectType(messageType);
        if (lstTokenFbSharevan.isEmpty()) {
            throw new BusinessException(this.translator.toLocale("firebase.token.not.exists"));
        }
        Integer notificationType=0;
        if(messageType.equals(TokenFbStatus.STORE_KEEPER.getValue())){
            notificationType=NotificationType.CUSTOMER.getValue();
        }else if(messageType.equals(TokenFbStatus.DRIVER.getValue())){
            notificationType=NotificationType.DRIVER.getValue();
        }else if(messageType.equals(TokenFbStatus.SALE_MAN.getValue())){
            notificationType=NotificationType.SALE_MAN.getValue();
        }
        for (TokenFbSharevan tokenFbSharevan : lstTokenFbSharevan) {
            pushNotificationRequest.setToken(tokenFbSharevan.getToken());
            this.sendPushNotificationToToken(pushNotificationRequest);
            pushNotificationRequest.setNotifycationType(notificationType);
            notificationService.saveMessage(pushNotificationRequest);
        }
    }
   /* private Map<String, String> getSamplePayloadData() {
        Map<String, String> pushData = new HashMap<>();
        pushData.put("messageId", defaults.get("payloadMessageId"));
        pushData.put("text", defaults.get("payloadData") + " " + LocalDateTime.now());
        return pushData;
    }


    private PushNotificationRequest getPushNotificationRequest() {
        PushNotificationRequest request = new PushNotificationRequest(defaults.get("title"),
                defaults.get("message"),
                defaults.get("topic"));
        return request;
    }*/


}
