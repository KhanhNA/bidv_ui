package com.tsolution._3services.impl;

import com.tsolution._1entities.Fleet;
import com.tsolution._1entities.ParkingPoint;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution._2repositories.FleetRepository;
import com.tsolution._2repositories.ParkingPointRepository;
import com.tsolution._2repositories.VanRepository;
import com.tsolution._3services.ParkingPointService;
import com.tsolution.excetions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ParkingPointServiceImpl extends BaseServiceImpl implements ParkingPointService {

    private static final Logger log = LogManager.getLogger(ParkingPointServiceImpl.class);
    @Autowired
    ParkingPointRepository parkingPointRepository;
    @Autowired
    FleetRepository fleetRepository;
    @Autowired
    VanRepository vanRepository;

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.parkingPointRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) {
        return new ResponseEntity<>(this.parkingPointRepository.findById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByFleetId(Long id) {
        return new ResponseEntity<>(this.parkingPointRepository.findByFleetId(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByToken() throws BusinessException {
        OAuth2AuthenticationDto oAuth2AuthenticationDto = this.getCurrentOAuth2Details();
        String userName = oAuth2AuthenticationDto.getName();
        Optional<Staff> optionalStaff = this.staffRepository.findByUserName(userName);
        if (!optionalStaff.isPresent()) {
            throw new BusinessException(this.translator.toLocale("employee.not.exists"));
        }
        Long fleetId= optionalStaff.get().getFleetId();
        Optional<Fleet> optionalFleet = this.fleetRepository.findById(fleetId);
        if (!optionalFleet.isPresent()) {
            throw new BusinessException(this.translator.toLocale("fleet.does.not.exist"));
        }
        if(optionalFleet.get().getParentId() != null)
            return new ResponseEntity<>(this.parkingPointRepository.findByFleetId(optionalFleet.get().getParentId()), HttpStatus.OK);
        else{
            return new ResponseEntity<>(this.parkingPointRepository.findByFleetId(optionalFleet.get().getId()), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<Object> find(ParkingPoint searchData, Integer pageNumber, Integer pageSize) throws BusinessException {
        return null;
    }

    @Override
    @Transactional
    public ResponseEntity<Object> create(ParkingPoint entity) throws BusinessException {
        String msg = validateCreate(entity);
        if(!StringUtils.isBlank(msg)){
            throw new BusinessException(this.translator.toLocale(msg));
        }
        entity.setStatus(StatusType.RUNNING.getValue());
        entity = this.parkingPointRepository.save(entity);
        return new ResponseEntity<>(entity,HttpStatus.OK);
    }

    private String validateCreate(ParkingPoint entity){
        if(entity == null || entity.getId() != null ||
                entity.getFleetId() == null || entity.getFleetId() == 0
                || StringUtils.isBlank(entity.getAddress())){
            return "common.input.info.invalid";
        }
        if(entity.getLatitude() == null || entity.getLongitude()== null){
            return "parking-point.validate.coordinate.empty";
        }
        if(entity.getDayReadyTime() != null && entity.getDayDueTime() != null
                && entity.getDayReadyTime().isAfter(entity.getDayDueTime())){
            return "parking-point.validate.start&due-time.invalid";
        }
        Optional<Fleet> oFleet =  this.fleetRepository.findById(entity.getFleetId());
        if(!oFleet.isPresent()){
            return "parking-point.validate.fleet.invalid";
        }
        return null;
    }
    @Override
    @Transactional
    public ResponseEntity<Object> update(ParkingPoint entity) throws BusinessException {
        String msg = validateCreate(entity);
        if(!StringUtils.isBlank(msg)){
            throw new BusinessException(this.translator.toLocale(msg));
        }
        entity = this.parkingPointRepository.save(entity);
        return new ResponseEntity<>(entity,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> delete(Long id) throws BusinessException {
        if(id == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        try{
            Optional<ParkingPoint> oPark = this.parkingPointRepository.findByActiveById(id);
            if(oPark.isPresent()) {
                ParkingPoint att = oPark.get();
                att.setStatus(StatusType.STOPPED.getValue());
                this.parkingPointRepository.save(att);
                return new ResponseEntity<>(HttpStatus.OK);
            }else{
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
    }
}
