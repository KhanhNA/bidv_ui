package com.tsolution._3services;

import com.tsolution._1entities.Notification;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface NotificationService extends EBaseService<Notification> {

    ResponseEntity<Object> findNotificationByToken(String authorization) throws BusinessException;

    ResponseEntity<Object> findById(Long id) throws BusinessException;

    void saveMessage(PushNotificationRequest pushNotificationRequest) ;
}
