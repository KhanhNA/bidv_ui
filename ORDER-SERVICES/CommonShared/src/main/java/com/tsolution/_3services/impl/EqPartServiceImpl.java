package com.tsolution._3services.impl;

import com.tsolution._1entities.EqPart;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.Van;
import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.EqPartRepository;
import com.tsolution._2repositories.EqPartRepositoryCustom;
import com.tsolution._3services.EqPartService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
public class EqPartServiceImpl extends BaseServiceImpl implements EqPartService {
    private static final Logger log = LogManager.getLogger(EqPartServiceImpl.class);
    private static final String objAttachmentType = OBJECT_ATTACHMENT.EQUIPMENT.getValue();
    @Autowired
    EqPartRepository partRepository;
    @Autowired
    ObjAttachmentServiceImpl objAttachmentService;
    @Autowired
    EqPartRepositoryCustom partRepositoryCustom;
    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.partRepository.findById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByVanId(Long id) throws BusinessException {
        return new ResponseEntity<>(this.partRepository.findByVanId(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByCondition(EqPart searchData, Pageable pageable) throws BusinessException {
        return new ResponseEntity<>(this.partRepositoryCustom.findByCondition(searchData, pageable), HttpStatus.OK);
//        return new ResponseEntity<>(this.partRepository.findByCondition(pageable, searchData.getVanId(), searchData.getName(),
//                searchData.getVendorId(), searchData.getCategoryType()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(EqPart entity) throws BusinessException {
        String msg = validateCreate(entity);
        if (!CommonUtil.isBlank(msg)) {
            throw new BusinessException(this.translator.toLocale(msg));
        } else {
            this.partRepository.save(extractData(new EqPart(), entity));
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private String validateCreate(EqPart entity) {
        if (entity.getId() != null) {
            return "common.input.info.invalid";
        }
        if (entity.getVanId() == null) {
            return "eq-part.error.message.noVanId";
        }
        if (entity.getUniversalProductCode() == null) {
            return "";
        }

        return null;
    }

    @Override
    public ResponseEntity<Object> update(EqPart entity) throws BusinessException {
        String msg = validateUpdate(entity);
        if (!CommonUtil.isBlank(msg)) {
            throw new BusinessException(this.translator.toLocale(msg));
        } else {
            Optional<EqPart> oPart = this.partRepository.findById(entity.getId());
            if(!oPart.isPresent()){
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
            entity = this.partRepository.save(extractData(new EqPart(), entity));
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    private String validateUpdate(EqPart entity) {
        if (entity.getId() == null) {
            return "common.input.info.invalid";
        }
        if (entity.getVanId() == null) {
            return "eq-part.error.message.noVanId";
        }
        if (CommonUtil.isBlank(entity.getUniversalProductCode())) {
            return "";
        }

        return null;
    }

    private EqPart extractData(EqPart base, EqPart entity) {
        EqPart temp = base;
        if (temp == null) temp = new EqPart();
        temp.setVanId(entity.getVanId());
        temp.setPartNo(entity.getPartNo());
        temp.setUniversalProductCode(entity.getUniversalProductCode());
        temp.setName(entity.getName());
        temp.setDescription(entity.getDescription());
        temp.setVendorId(entity.getVendorId());
        temp.setCategoryType(entity.getCategoryType());
        temp.setUnitCost(entity.getUnitCost());
        temp.setUnitMeasure(entity.getUnitMeasure());
        temp.setParentPartId(entity.getParentPartId());
        temp.setStatus(entity.getStatus());
        return temp;
    }

    @Override
    public ResponseEntity<Object> delete(Long id) throws BusinessException {
        if (id == null || id.equals(0L)) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        try {
            Optional<EqPart> oPart = this.partRepository.findById(id);
            EqPart part = oPart.get();
            part.setStatus(StatusType.STOPPED.getValue());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
    }

    @Override
    @Transactional
    public ResponseEntity<Object> addOrEditAttachments(EqPart entity) throws BusinessException {
        Collection<ObjAttachment> attachments = entity.getListAttachments();
        Collection<ObjAttachment> images = entity.getListImages();
        Collection<ObjAttachment> removedAttachments = entity.getListRemovedAttachments();
        Collection<ObjAttachment> removedImages = entity.getListRemovedImages();

        if (images != null) {
            for (ObjAttachment obj : images) {
                if (CommonUtil.isBlank(obj.getAttachUrl())) {
                    throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
                }
            }
        }

        if (attachments != null) {
            for (ObjAttachment obj : attachments) {
                if (CommonUtil.isBlank(obj.getAttachUrl())) {
                    throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
                }
            }
        }

        if (removedImages != null && !this.objAttachmentService.deleteAttachment(entity.getId(), removedImages)) {//Delete images
            throw new BusinessException(this.translator.toLocale("van.error.updatingImages"));
        }
        if (removedAttachments != null && !this.objAttachmentService.deleteAttachment(entity.getId(), removedAttachments)) {//Delete attachments
            throw new BusinessException(this.translator.toLocale("van.error.updatingAttachment"));
        }
        if (images != null && !this.objAttachmentService.saveAttachment(entity.getId(), images, 1,
                objAttachmentType)) {//Saving images
            throw new BusinessException(this.translator.toLocale("van.error.updatingImages"));
        }
        if (attachments != null && !this.objAttachmentService.saveAttachment(entity.getId(), attachments, 2,
                objAttachmentType)) {//Saving attachment
            throw new BusinessException(this.translator.toLocale("van.error.savingAttachment"));
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
