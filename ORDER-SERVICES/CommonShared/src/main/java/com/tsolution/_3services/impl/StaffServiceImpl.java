package com.tsolution._3services.impl;

import com.tsolution._1entities.DayOff;
import com.tsolution._1entities.DriverLicense;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.dto.ChannelTypeDTO;
import com.tsolution._1entities.enums.*;
import com.tsolution._1entities.oauth2.RoleDto;
import com.tsolution._1entities.oauth2.UserDto;
import com.tsolution._2repositories.DayOffRepository;
import com.tsolution._2repositories.DriverLicenseRepository;
import com.tsolution._3services.StaffService;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StaffServiceImpl extends BaseServiceImpl implements StaffService {

	private SingleSignOnUtils singleSignOnUtils;
	private static final Logger log = LogManager.getLogger(StaffServiceImpl.class);
	@Autowired
	private BaseExcelService baseExcelService;
	@Autowired
	private FileStorageService fileStorageService;
	@Autowired
	public StaffServiceImpl(SingleSignOnUtils singleSignOnUtils) {
		this.singleSignOnUtils = singleSignOnUtils;
	}

	@Autowired
	protected DayOffRepository dayOffRepository;

	@Autowired
	protected DriverLicenseRepository driverLicenseRepository;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(String authorization, String language, Staff entities)
			throws BusinessException {
		validateValueStaffInput(ACTION.INSERT.getValue(), entities);
		// insert vao staff
		entities.setStatus(StaffStatus.STATUS_DEACTIVE.getValue());
		Staff staff = staffRepository.save(entities);
		boolean isDriver = false;
		boolean isDriverManager = false;
		List<RoleDto> userRoles = entities.getUserRoles();
		for (RoleDto role : userRoles) {
			if (StaffRole.ROLE_NAME_DRIVER.getValue().equals(role.getRoleName())) {
				isDriver = true;
			}else if(StaffRole.ROLE_NAME_DRIVER_MANAGER.getValue().equals(role.getRoleName())){
				isDriverManager = true;
			}
		}
		if(!isDriver && !isDriverManager){
			throw new BusinessException(this.translator.toLocale("staff.input.role.invalid"));
		}
		if (isDriver && entities.getDriverLicence() != null) {
			DriverLicense driverLicence = entities.getDriverLicence();
			driverLicence.setStaffId(staff.getId());
			validateDriverLicenseInput(ACTION.INSERT.getValue(), driverLicence);
			DriverLicense driverLicence1 = driverLicenseRepository.save(driverLicence);
			if (!CommonUtil.isEmpty(driverLicence.getListAttachments())) {
				for (ObjAttachment attachment : driverLicence.getListAttachments()) {
					attachment.setObjId(driverLicence1.getId());
					attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue());
					attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
					attachment.setAttachName(OBJECT_ATTACHMENT.OBJ_ATTACHMENT_DRIVER_LICENSE.getValue());
					validateObjAttactment(ACTION.INSERT.getValue(), attachment);
					objAttachmentRepository.save(attachment);
				}
			}
		}

		if (!CommonUtil.isEmpty(entities.getListAttachments())) {
			List<ObjAttachment> objAttachmentList = entities.getListAttachments();
			for (ObjAttachment attachment : objAttachmentList) {
				attachment.setObjId(entities.getId());
				attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_FILE.getValue());
				attachment.setAttachName(OBJECT_ATTACHMENT.STAFF.getValue());
				attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
				validateObjAttactment(ACTION.INSERT.getValue(), attachment);
				objAttachmentRepository.save(attachment);
			}
		}
		if (!CommonUtil.isEmpty(entities.getListImages())) {
			List<ObjAttachment> objAttachmentList = entities.getListImages();
			for (ObjAttachment attachment : objAttachmentList) {
				attachment.setObjId(entities.getId());
				attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue());
				attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
				attachment.setAttachName(OBJECT_ATTACHMENT.STAFF.getValue());
				validateObjAttactment(ACTION.INSERT.getValue(), attachment);
				objAttachmentRepository.save(attachment);
			}
		}
		// map tra ve user
		UserDto user = mapStaffToUser(entities);
		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, language, "/user", user);
		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("staff.add.failed"));
		}
		return new ResponseEntity<>(entities, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(String authorization, String acceptLanguage, Staff entitie)
			throws BusinessException {
		validateValueStaffInput(ACTION.UPDATE.getValue(), entitie);
		Integer action = ACTION.INSERT.getValue();
		DriverLicense drivertest = entitie.getDriverLicence();

		if(entitie.getDriverLicence() != null && (entitie.getDriverLicence().getId() == null || entitie.getDriverLicence().getStaffId()==null)){
			entitie.setDriverLicence(null);
		}

		staffRepository.save(entitie);
		if (entitie.getObjectType().equals(StaffRole.ROLE_NAME_DRIVER.getValue())) {
			DriverLicense driverLicense =  drivertest;
			if (driverLicense.getStaffId()==null){
				driverLicense.setStaffId(entitie.getId());
				Optional<DriverLicense> dLicense1 = driverLicenseRepository.findByStaffId(entitie.getId());
				if (dLicense1.isPresent()) {
					DriverLicense dLicense2 = dLicense1.get();
					driverLicense.setId(dLicense2.getId());
				}
			}
			if(driverLicense != null && driverLicense.getId() != null){
				action = ACTION.UPDATE.getValue();
			}
			validateDriverLicenseInput(action, driverLicense);
			DriverLicense driverLicense1 = driverLicenseRepository.save(driverLicense);
			if (!CommonUtil.isEmpty(driverLicense.getListAttachments())) {
				List<ObjAttachment> listAttachments = driverLicense.getListAttachments();
				for (ObjAttachment attachment : listAttachments) {
					if (attachment.getId() == null) {
						attachment.setObjId(driverLicense1.getId());
						attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue());
						attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
						attachment.setAttachName(OBJECT_ATTACHMENT.OBJ_ATTACHMENT_DRIVER_LICENSE.getValue());
						validateObjAttactment(ACTION.INSERT.getValue(), attachment);
						objAttachmentRepository.save(attachment);
					}

				}
			}


			if (!CommonUtil.isEmpty(driverLicense.getListAttachmentRemove())) {
				for (ObjAttachment attachment : driverLicense.getListAttachmentRemove()) {
					attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_DELETED.getValue());
					validateObjAttactment(ACTION.UPDATE.getValue(), attachment);
					objAttachmentRepository.save(attachment);
				}
			}

			if (!CommonUtil.isEmpty(entitie.getListAttachments())) {
				for (ObjAttachment attachment : entitie.getListAttachments()) {
					if (attachment.getId() == null) {
						attachment.setObjId(entitie.getId());
						attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_FILE.getValue());
						attachment.setAttachName(OBJECT_ATTACHMENT.STAFF.getValue());
						attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
						validateObjAttactment(ACTION.INSERT.getValue(), attachment);
						objAttachmentRepository.save(attachment);
					}
				}
			}

			if (!CommonUtil.isEmpty(entitie.getListRemovedAttachments())) {
				for (ObjAttachment attachment : entitie.getListRemovedAttachments()) {
					attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_DELETED.getValue());
					validateObjAttactment(ACTION.UPDATE.getValue(), attachment);
					objAttachmentRepository.save(attachment);
				}
			}

			if (!CommonUtil.isEmpty(entitie.getListImages())) {
				for (ObjAttachment attachment : entitie.getListImages()) {
					if (attachment.getId() == null) {
						attachment.setObjId(entitie.getId());
						attachment.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue());
						attachment.setAttachName(OBJECT_ATTACHMENT.STAFF.getValue());
						attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_USING.getValue());
						validateObjAttactment(ACTION.INSERT.getValue(), attachment);
						objAttachmentRepository.save(attachment);
					}
				}
			}

			if (!CommonUtil.isEmpty(entitie.getListRemovedImages())) {
				for (ObjAttachment attachment : entitie.getListRemovedImages()) {
					attachment.setStatus(OBJECT_ATTACHMENT_STATUS.OBJ_ATTACHMENT_STATUS_DELETED.getValue());
					validateObjAttactment(ACTION.UPDATE.getValue(), attachment);
					objAttachmentRepository.save(attachment);
				}
			}
		}
		UserDto user = mapStaffToUser(entitie);
		Map<String, Long> pathVariables = new HashMap<>();
		pathVariables.put("id", 0L);

		ResponseEntity<Object> result = this.singleSignOnUtils.patch(authorization, acceptLanguage, "/user/{id}",
				pathVariables, user);

		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("staff.update.failed"));
		}
		return new ResponseEntity<>(entitie, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.staffRepository.findAll(),HttpStatus.OK) ;
	}

	@Override
	public ResponseEntity<Staff> findById( Long id)
			throws BusinessException {
		Optional<Staff> staff = staffRepository.findById(id);
		if (!staff.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("staff.find.failed", id));
		}
		Staff result = staff.get();
		List<ObjAttachment> listAttachs = (List<ObjAttachment>) objAttachmentRepository.findByObjId(result.getId(),
				OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_FILE.getValue(), OBJECT_ATTACHMENT.STAFF.getValue());
		if (!CommonUtil.isEmpty(listAttachs)) {
			result.setListAttachments(listAttachs);
		}
		List<ObjAttachment> listImages = (List<ObjAttachment>) objAttachmentRepository.findByObjId(result.getId(),
				OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue(), OBJECT_ATTACHMENT.STAFF.getValue());
		if (!CommonUtil.isEmpty(listImages)) {
			result.setListImages(listImages);
		}
		if (result.getDriverLicence() != null) {
			DriverLicense driverLicense = result.getDriverLicence();
			List<ObjAttachment> listLicenseAttachs = (List<ObjAttachment>) objAttachmentRepository.findByObjId(
					driverLicense.getId(), OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue(),
					OBJECT_ATTACHMENT.OBJ_ATTACHMENT_DRIVER_LICENSE.getValue());
			if (!CommonUtil.isEmpty(listLicenseAttachs)) {
				result.getDriverLicence().setListAttachments(listLicenseAttachs);
			}
		}
//		ResponseEntity<Object> userRole = this.singleSignOnUtils.get(authorization, acceptLanguage,
//				"/user/" + result.getUserName() + "/role");
//		if (!HttpStatus.OK.equals(userRole.getStatusCode())) {
//			throw new BusinessException(this.translator.toLocale("staff.find.failed"));
//		}
//
//		List<RoleDto> RoleDto = (List<RoleDto>) userRole.getBody();
//		if (CommonUtil.isEmpty(RoleDto)) {
//			throw new BusinessException(this.translator.toLocale("staff.find.failed"));
//		}
//		result.setUserRoles(RoleDto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, Staff entity)
			throws BusinessException {
		Optional<Staff> optionalStaff = this.staffRepository.findById(entity.getId());
		if (!optionalStaff.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("staff.id.not.exists", entity.getId()));
		}

		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage,
				"/user/reset-password", entity.getUserName());
		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("staff.reset.password.failed"));
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> activeOrDeActivate(String authorization, String acceptLanguage, Staff entity,
			boolean isActive) throws BusinessException {
		if (entity == null) {
			throw new BusinessException(this.translator.toLocale("staff.input.info.invalid"));
		}
		if (entity.getId() == null) {
			throw new BusinessException(this.translator.toLocale("staff.input.missing.id"));
		}

		Optional<Staff> optionalStaff = this.staffRepository.findById(entity.getId());
		if (!optionalStaff.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("staff.id.not.exists", entity.getId()));
		}

		entity.setStatus(isActive ? StaffStatus.STATUS_ACTIVE.getValue() : StaffStatus.STATUS_DEACTIVE.getValue());
		this.staffRepository.save(entity);

		if (isActive) {
			ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/active",
					entity.getUserName());

			if (!HttpStatus.OK.equals(result.getStatusCode())) {
				throw new BusinessException(this.translator.toLocale("staff.active.failed"));
			}
		} else {
			ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/deactive",
					entity.getUserName());

			if (!HttpStatus.OK.equals(result.getStatusCode())) {
				throw new BusinessException(this.translator.toLocale("staff.deactivate.failed"));
			}
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/* Check validate value staff */
	public void validateValueStaffInput(Integer action, Staff entity) throws BusinessException {
		if (entity == null) {
			throw new BusinessException(this.translator.toLocale("staff.input.info.invalid"));
		}
		if (ACTION.UPDATE.getValue() == action && entity.getId() == null) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.id"));
		}
//		if (StringUtils.isNullOrEmpty(entity.getStaffCode())) {
//			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.code"));
//		}
		if (StringUtils.isNullOrEmpty(entity.getFullName())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.fullName"));
		}
		if (CommonUtil.isEmpty(entity.getUserRoles())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.user.role"));
		}
		if (StringUtils.isNullOrEmpty(entity.getEmail())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.email"));
		}
		if (!CommonUtil.isEmail(entity.getEmail())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.email"));
		}
		if (!StringUtils.isNullOrEmpty(entity.getPhone()) && !CommonUtil.isPhoneNumber(entity.getPhone())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.phoneNumber"));
		}
		if (StringUtils.isNullOrEmpty(entity.getUserName())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.user.name"));
		}
		if (ACTION.INSERT.getValue() == action) {
			Optional<Staff> staff = staffRepository.findByUserName(entity.getUserName());
			if (staff.isPresent()) {
				throw new BusinessException(this.translator.toLocale("staff.input.invalid.user.name.exists"));
			}
		}
		if (StringUtils.isNullOrEmpty(entity.getFirstName())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.first.name"));
		}
		if (StringUtils.isNullOrEmpty(entity.getLastName())) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.last.name"));
		}
		entity.setUserName(entity.getUserName().trim());
		if (entity.getUserName().length() > 140) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.index.out.of.bound.user.name"));
		}
		if (entity.getFleetId() == null) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.organization.id"));
		}

	}

	/* check input value driver license */
	public void validateDriverLicenseInput(Integer action, DriverLicense driverLicence) throws BusinessException {
		if (driverLicence == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null"));
		}
		if (ACTION.UPDATE.getValue() == action && driverLicence.getId() == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.id"));
		}
        if (ACTION.UPDATE.getValue() == action ) {
        	Optional<DriverLicense> dLicense = driverLicenseRepository.findById(driverLicence.getId());
        	if(!dLicense.isPresent()){
				throw new BusinessException(this.translator.toLocaleByFormatString("driver.license.input.invalid.missing.id",driverLicence.getId()));
			}
        }
		if (driverLicence.getStaffId() == null && ACTION.UPDATE.getValue() == action) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.staff.id"));
		}
		/* check ton tai trong db ko */
		if (driverLicence.getLicenseCode() == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.code"));
		}
		if (driverLicence.getLicenseType() == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.type"));
		}
		if (driverLicence.getRangeDate() == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.range.date"));
		}
		if (driverLicence.getEndDate() == null) {
			throw new BusinessException(this.translator.toLocale("driver.license.input.invalid.null.end.date"));
		}
		if(driverLicence.getEndDate().isBefore(driverLicence.getRangeDate()) || driverLicence.getEndDate().equals(driverLicence.getRangeDate())){
			throw new BusinessException(this.translator.toLocale("driver.license.input.end.date.before.range.date"));
		}
		if(driverLicence.getEndDate().isBefore(LocalDateTime.now())){
			throw new BusinessException(this.translator.toLocale("driver.license.input.end.date.before.date.now"));
		}
		if(driverLicence.getRangeDate().isAfter(LocalDateTime.now())){
			throw new BusinessException(this.translator.toLocale("driver.license.input.range.date.after.date.now"));
		}
	}

	public void validateObjAttactment(Integer action, ObjAttachment objAttachment) throws BusinessException {
		if (objAttachment == null) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null"));
		}
		if (ACTION.UPDATE.getValue() == action && objAttachment.getId() == null) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null.id"));
		}
		if (objAttachment.getObjId() == null) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null.objId"));
		}
		if (StringUtils.isNullOrEmpty(objAttachment.getAttachName())) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null.attach.name"));
		}
		if (objAttachment.getAttachType() == null) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null.attach.type"));
		}
		if (objAttachment.getAttachUrl() == null) {
			throw new BusinessException(this.translator.toLocale("obj.attachment.input.invalid.null.attach.url"));
		}
	}

	/* map den staff */
	public UserDto mapStaffToUser(Staff staff) {
		UserDto user = new UserDto();
		user.setEnabled(true);
		user.setFirstName(staff.getFirstName());
		user.setLastName(staff.getLastName());
		user.setUsername(staff.getUserName());
		user.setRoles(staff.getUserRoles());
		return user;
	}

	@Override
	public ResponseEntity<Object> findAllStaffAvailable(LocalDateTime fdate, LocalDateTime tdate)
			throws BusinessException {
		if (fdate == null) {
			throw new BusinessException(this.translator.toLocale("common.input.missing.from.date"));
		}
		if (tdate == null) {
			throw new BusinessException(this.translator.toLocale("common.input.missing.to.date"));
		}
		// type 1: trong ngày type2: nhiều ngày
		Integer type = 1;

		List<Integer> staffList = this.staffRepository.getAllStaffIdByStatus(StatusType.RUNNING.getValue());

		// count generate date fdate- todate
		List<LocalDateTime> dates = Stream.iterate(fdate, date -> date.plusDays(1))
				.limit(ChronoUnit.DAYS.between(fdate, tdate)).collect(Collectors.toList());
		dates.add(tdate);

		// đi nhiều ngày
		if (type == 2) {
			for (int i = dates.size(); i <= dates.size(); i++) {
				dates.add(tdate.plusDays(i));
			}
		}

		Map<Integer, Integer> lstStaffisNotDayOff = new HashMap<>();

		// check staff xem ngày đó có lịch nghỉ không?
		for (Integer staff : staffList) {

			List<DayOff> dayoff = new ArrayList<DayOff>();

			List<Integer> staffDriverVan = this.staffRepository.getStaffIdByStatusRunningInDrivervan(staff.longValue(),
					1);// có đã đc gán tuyến hay k
			for (LocalDateTime date : dates) {
				DayOff d = this.dayOffRepository.findAvailable(staff.longValue(),
						LocalDateTimeConverter.convertToJavaSqlDate(date),
						LocalDateTimeConverter.convertToJavaSqlDate(date));
				if (d != null) {
					dayoff.add(d);
				}
			}

			// nếu tuyến dài ngày xuất phát từ buổi chiều? status=1
			// check = ngày bắt đầu là chiều && nghỉ sáng
			if (dayoff != null && dayoff.size() == 1) {
				for (DayOff offMorning : dayoff) {
					if ((DateTimeFormatter.ISO_DATE.format(offMorning.getFromDate())
							.equals(DateTimeFormatter.ISO_DATE.format(fdate))) && (offMorning.getSpec() == 1)) {
						lstStaffisNotDayOff.put(staff, 1); // loại staff
					}
				}
			}

			if (dayoff.size() == 0 && staffDriverVan.size() == 0) {
				lstStaffisNotDayOff.put(staff, 0);
			}
		}

		List<Object> lstStaff = new ArrayList<>();
		lstStaff.add(lstStaffisNotDayOff);

		System.out.println(lstStaffisNotDayOff);
		return new ResponseEntity<>(lstStaffisNotDayOff, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Staff> findByUserName(String authorization, String acceptLanguage, String username) throws BusinessException {
		if(StringUtils.isNullOrEmpty(username)){
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.null.user.name"));
		}
		Optional<Staff> staff = staffRepository.findByUserName(username);
		if(!staff.isPresent()){
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.not.exist.user.name"));
		}
		Staff result = staff.get();
		List<ObjAttachment> listAttachs = (List<ObjAttachment>) objAttachmentRepository.findByObjId(result.getId(),
				OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_FILE.getValue(), OBJECT_ATTACHMENT.STAFF.getValue());
		if (!CommonUtil.isEmpty(listAttachs)) {
			result.setListAttachments(listAttachs);
		}
		List<ObjAttachment> listImages = (List<ObjAttachment>) objAttachmentRepository.findByObjId(result.getId(),
				OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue(), OBJECT_ATTACHMENT.STAFF.getValue());
		if (!CommonUtil.isEmpty(listImages)) {
			result.setListImages(listImages);
		}
		if (result.getDriverLicence() != null) {
			DriverLicense driverLicense = result.getDriverLicence();
			List<ObjAttachment> listLicenseAttachs = (List<ObjAttachment>) objAttachmentRepository.findByObjId(
					driverLicense.getId(), OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue(),
					OBJECT_ATTACHMENT.OBJ_ATTACHMENT_DRIVER_LICENSE.getValue());
			if (!CommonUtil.isEmpty(listLicenseAttachs)) {
				result.getDriverLicence().setListAttachments(listLicenseAttachs);
			}
		}
		ResponseEntity<Object> userRole = this.singleSignOnUtils.get(authorization, acceptLanguage,
				"/user/" + result.getUserName() + "/role");
		if (!HttpStatus.OK.equals(userRole.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("staff.find.failed"));
		}

		List<RoleDto> RoleDto = (List<RoleDto>) userRole.getBody();
		if (CommonUtil.isEmpty(RoleDto)) {
			throw new BusinessException(this.translator.toLocale("staff.find.failed"));
		}
		result.setUserRoles(RoleDto);
		return new ResponseEntity<>(result,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findStaff(Integer fleetId,String userName,String fullName,String objType,String email,String phoneNumber,Integer status, Integer pageNumber, Integer pageSize) throws BusinessException {
		return new ResponseEntity<Object>(this.staffRepository.findStaff( fleetId ,  userName ,  fullName, objType,email,phoneNumber,status,PageRequest.of(pageNumber - 1, pageSize)),HttpStatus.OK);
	}

	@Override
	public Resource getExcelTemplate(String fileName) throws BusinessException, IOException {
		Map<String, Object> params = new HashMap<>();
		List<ChannelTypeDTO> lstStaffType= new ArrayList<>();
		ChannelTypeDTO staff = new ChannelTypeDTO();
		staff.setCodeChannelType(this.translator.toLocale("staff.staff.type.code"));
		staff.setCodeChannelType(this.translator.toLocale("staff.staff.type"));
		lstStaffType.add(staff);

		ChannelTypeDTO staffManager = new ChannelTypeDTO();
		staffManager.setCodeChannelType(this.translator.toLocale("staff.manager.type.code"));
		staffManager.setCodeChannelType(this.translator.toLocale("staff.manager.type"));
		lstStaffType.add(staffManager);
		List<String> lstCity= new ArrayList<>();
		lstCity.add(this.translator.toLocale("common.status.activated"));
		lstCity.add(this.translator.toLocale("common.status.deactivated"));

		String staffType= this.translator.toLocale("staff.manager.type.code")+","+this.translator.toLocale("staff.staff.type.code");
		params.put("lstCustomerTypes",lstStaffType);
		params.put("lstCity",lstCity);
        params.put("lstArea",lstCity);
		params.put("customerType",staffType);
	/*	params.put("lstArea", lstData.get(0)); //$NON-NLS-1$
		final int DYNAMIC_ATTRIBUTE_INDEX = 1;
		if (dataSize > DYNAMIC_ATTRIBUTE_INDEX) {
			params.put("dynamicAttribute", lstData.get(DYNAMIC_ATTRIBUTE_INDEX)); //$NON-NLS-1$
		}
		params.put("lstCity", lstData.get(2));
		params.put("customerType", this.getListCustomerTypeForTemplate()); //$NON-NLS-1$
		params.put("lstCustomerTypes", this.channelTypeMgr.getListLeafChannelTypeVO()); //$NON-NLS-1$
		params.put("lstTitle", this.lstTitle); //$NON-NLS-1$
		params.put("salePositionOptions", this.getJoinedCustomerSalePositionOptions()); //$NON-NLS-1$*/

		return this.baseExcelService.updateExcelFile(params,BaseExcelService.ExcelTemplate.STAFF_EXCEL_ADD);
}

	@Override
	public ResponseEntity<Object> updateStaffByExcel(MultipartFile file) throws IOException, InvalidFormatException {
		return null;
	}


}
