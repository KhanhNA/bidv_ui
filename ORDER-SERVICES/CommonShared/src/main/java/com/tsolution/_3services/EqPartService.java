package com.tsolution._3services;

import com.tsolution._1entities.EqPart;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;


public interface EqPartService {
    ResponseEntity<Object> findById(Long id) throws BusinessException;

    ResponseEntity<Object> findByVanId(Long id) throws BusinessException;

    ResponseEntity<Object> findByCondition(EqPart searchData, Pageable pageable) throws BusinessException;

    ResponseEntity<Object> create(EqPart entity) throws BusinessException;

    ResponseEntity<Object> update(EqPart entity) throws BusinessException;

    ResponseEntity delete(Long id) throws BusinessException;

    ResponseEntity<Object> addOrEditAttachments(EqPart entity) throws BusinessException;
}
