package com.tsolution._3services.impl;

import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.Van;
import com.tsolution._1entities.dto.VanDto;
import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.FleetRepository;
import com.tsolution._2repositories.ObjAttachmentRepository;
import com.tsolution._2repositories.VanRepository;
import com.tsolution._3services.VanService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class VanServiceImpl extends BaseServiceImpl implements VanService {
    private static final Logger log = LogManager.getLogger(VanServiceImpl.class);
    private static final String objType = OBJECT_ATTACHMENT.VAN.getValue();
    @Autowired
    VanRepository vanRepository;
    @Autowired
    ObjAttachmentServiceImpl objAttachmentService;
    @Autowired
    FileStorageService fileStorageService;
    @Autowired
    FleetRepository fleetRepository;
    @Autowired
    protected ObjAttachmentRepository objAttachmentRepository;
    @Autowired
    private VanExcelService vanExcelService;
    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.vanRepository.findAll(),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<Van> opt = this.vanRepository.findById(id);
        if (opt.isPresent()) {
            Van entity = opt.get();
            Collection<ObjAttachment> listImages = this.objAttachmentRepository
                    .findByObjId(entity.getId(), 1, OBJECT_ATTACHMENT.VAN.getValue());
            entity.setListImages(new ArrayList<>(listImages));

            Collection<ObjAttachment> listAttachments = this.objAttachmentRepository
                    .findByObjId(entity.getId(), 2, OBJECT_ATTACHMENT.VAN.getValue());
            entity.setListAttachments(new ArrayList<>(listAttachments));

            return new ResponseEntity<>(entity, HttpStatus.OK);
        }
        return new ResponseEntity<>(this.translator.toLocale("common.input.info.invalid"), HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> findByCondition(VanDto searchData, Pageable pageable) throws BusinessException {
        try {
            Optional<Staff> staff = getCurrentStaff();
            if(staff.isPresent())
                searchData.setFleetId(staff.get().getFleetId());

            if (!CommonUtil.isBlank(searchData.getVehicleTonnage())) {
                if(searchData.getVehicleTonnage().contains(">")){
                    searchData.setTonnageFrom(Integer.parseInt(searchData.getVehicleTonnage().replace(">","").trim()));
                }else{
                    String tonnages[] = searchData.getVehicleTonnage().split("-");
                    searchData.setTonnageFrom(Integer.parseInt(tonnages[0].trim()));
                    searchData.setTonnageTo(Integer.parseInt(tonnages[1].trim()));
                }
            }

            Page<VanDto> page= this.vanRepository.findByCondition(searchData, pageable);
            return new ResponseEntity<>(page, HttpStatus.OK);
        }  catch (BusinessException e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(this.translator.toLocale("van.error.findByCondition.noFleet"));
        }
    }

    @Override
    @Transactional
    public ResponseEntity<Object> create(Van entity) throws BusinessException {
        try{
            String msg = validateCreate(entity);
            if (!CommonUtil.isBlank(msg)) {
                throw new BusinessException(this.translator.toLocale(msg));
            } else {
                entity = this.vanRepository.save(extractData(new Van(), entity));
                addOrEditAttachments(entity);
            }
            return new ResponseEntity<>(this.findById(entity.getId()), HttpStatus.OK);
        } catch (Exception e){
            log.error(e);
            throw new BusinessException("common.error");
        }
    }

    private String validateCreate(Van entity) throws BusinessException {
        if (entity == null || entity.getId() != null || CommonUtil.isBlank(entity.getLicencePlate())
                || CommonUtil.isBlank(entity.getVehicleRegistration())) {
            return "common.input.info.invalid";
        }
        Optional<Staff> optionalStaff = getCurrentStaff();
        if (!optionalStaff.isPresent()) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Long fleetId = optionalStaff.get().getFleetId();
        if (fleetId == null || fleetId == 0) {
            return "van.error.findByCondition.noFleet";
        }
        entity.setFleetId(fleetId);
        Optional<Van> oVan = this.vanRepository.findByLicencePlate(entity.getLicencePlate());
        if (oVan.isPresent()) {
            return "van.error.addExistingLicencePlate";
        }
        Optional<Van> oVan2 = this.vanRepository.findByVehicleRegistration(entity.getVehicleRegistration());
        if (oVan2.isPresent()) {
            return "van.error.addExistingVehicleRegistration";
        }
        if(CommonUtil.isBlank(entity.getLocation())){
            return "van.validate.empty.location";
        }
        if(entity.getCost() == null || entity.getCost().equals(0)){
            return "van.validate.empty.cost";
        }
        return null;
    }

    @Override
    @Transactional
    public ResponseEntity<Object> update(Van entity) throws BusinessException {
        String msg = validateUpdate(entity);
        if (!CommonUtil.isBlank(msg)) {
            throw new BusinessException(this.translator.toLocale(msg));
        } else {
            Optional<Van> oVan = this.vanRepository.findById(entity.getId());
            if (!oVan.isPresent()) {
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
            addOrEditAttachments(entity);
            Van temp = extractData(oVan.get(), entity);
            temp.setId(entity.getId());
            this.vanRepository.save(temp);
            entity = this.vanRepository.save(temp);
        }
        return new ResponseEntity<>(this.findById(entity.getId()), HttpStatus.OK);
    }

    private String validateUpdate(Van entity) throws BusinessException {
        if(entity == null || entity.getId() == null || CommonUtil.isBlank(entity.getLicencePlate())
                || CommonUtil.isBlank(entity.getVehicleRegistration())){
            return "common.input.info.invalid";
        }
        Optional<Staff> optionalStaff = getCurrentStaff();
        if (!optionalStaff.isPresent()) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Long fleetId = optionalStaff.get().getFleetId();
        Optional<Van> oVan = this.vanRepository.findByLicencePlate(entity.getLicencePlate());
        if (oVan.isPresent() && !oVan.get().getId().equals(entity.getId())) {// Duplicate plate and different van
            return "van.error.addExistingLicencePlate";
        }
        oVan = this.vanRepository.findById(entity.getId());
        if (!fleetId.equals(oVan.get().getFleetId())) {
            return "parkingPoint.error.van.fleet.notMatch";
        }
        Optional<Van> oVan2 = this.vanRepository.findByVehicleRegistration(entity.getVehicleRegistration());
        if (oVan2.isPresent() && !oVan2.get().getId().equals(entity.getId())) {
            return "van.error.addExistingVehicleRegistration";
        }
        if(CommonUtil.isBlank(entity.getLocation())){
            return "van.validate.empty.location";
        }
        if(entity.getCost() == null || entity.getCost().equals(0)){
            return "van.validate.empty.cost";
        }
        return null;
    }

    private Van extractData(Van base, Van entity) {
        Van temp = base;
        //General
        temp.setName(entity.getName());
        temp.setLicencePlate(entity.getLicencePlate());
        temp.setVehicleRegistration(entity.getVehicleRegistration());
        temp.setYear(entity.getYear());
        temp.setVehicleTonnage(entity.getVehicleTonnage());
        temp.setStatusCar(entity.getStatusCar());
        temp.setParkingPointId(entity.getParkingPointId());
        temp.setVanTypeId(entity.getVanTypeId());
        temp.setFleetId(entity.getFleetId());
        temp.setCost(entity.getCost());
        temp.setLocation(entity.getLocation());
        //Detail
        temp.setAppParamValueId(entity.getAppParamValueId());
        temp.setColor(entity.getColor());
        temp.setVanInspection(entity.getVanInspection());
        temp.setInspectionDueDate( entity.getInspectionDueDate());
        temp.setCapacity(entity.getCapacity());
        temp.setStatusAvailable(entity.getStatusAvailable());
        temp.setFuelTypeId(entity.getFuelTypeId());
        temp.setNote(entity.getNote());
        // Technical
        temp.setBodyLength(entity.getBodyLength());
        temp.setBodyWidth(entity.getBodyWidth());
        temp.setHeight(entity.getHeight());
        temp.setEngineSize(entity.getEngineSize());
        // Tires
        temp.setAxles(entity.getAxles());
        temp.setWheelbase(entity.getWheelbase());
        temp.setTireFrontSize(entity.getTireFrontSize());
        temp.setTireFrontSize(entity.getTireFrontPressure());
        temp.setTireRearSize(entity.getTireRearSize());
        temp.setTireRearSize(entity.getTireRearPressure());
        return temp;
    }

    @Override
    public ResponseEntity<Object> deactivate(Long id) throws BusinessException {
        if (id == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        } else {
            Optional<Van> oVan = this.vanRepository.findById(id);
            if (!oVan.isPresent()) {
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
            Van comp = oVan.get();
            comp.setStatusCar(StatusType.STOPPED.getValue());
            this.vanRepository.save(comp);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> activate(Long id) throws BusinessException {
        if (id == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        } else {
            Optional<Van> oVan = this.vanRepository.findById(id);
            if (!oVan.isPresent()) {
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
            Van comp = oVan.get();
            comp.setStatusCar(StatusType.RUNNING.getValue());
            this.vanRepository.save(comp);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> addOrEditAttachments(Van entity) throws BusinessException {
        Collection<ObjAttachment> attachments = entity.getListAttachments();
        Collection<ObjAttachment> images = entity.getListImages();
        Collection<ObjAttachment> removedAttachments = entity.getListRemovedAttachments();
        Collection<ObjAttachment> removedImages = entity.getListRemovedImages();

        if(images != null){
            for(ObjAttachment obj: images){
                if(CommonUtil.isBlank(obj.getAttachUrl())){
                    throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
                }
            }
        }

        if(attachments!=null){
            for(ObjAttachment obj: attachments){
                if(CommonUtil.isBlank(obj.getAttachUrl())){
                    throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
                }
            }
        }

        if(removedImages!=null && !this.objAttachmentService.deleteAttachment(entity.getId(), removedImages)){//Delete images
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(removedAttachments!= null && !this.objAttachmentService.deleteAttachment(entity.getId(), removedAttachments)){//Delete attachments
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(images != null && !this.objAttachmentService.saveAttachment(entity.getId(), images, 1,
                objType)){//Saving images
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(attachments != null && !this.objAttachmentService.saveAttachment(entity.getId(), attachments, 2,
                objType)){//Saving attachment
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<Object> importExcel(MultipartFile file) throws BusinessException{
        this.vanExcelService.setDataRow(1);
        return this.vanExcelService.importExcel(file);
    }

    @Override
    public Resource getExcelTemplate(String fileName) throws Exception {
        return this.vanExcelService.exportTemplate();
    }

}
