package com.tsolution.utils;

public class MathUtils {
    public static String fixLengthString (long number, int length) {

        String numberAsString = String.valueOf(number);
        StringBuilder sb = new StringBuilder();
        if (numberAsString.length() >= length) {
            return numberAsString;
        }
        while(sb.length()+numberAsString.length()<length) {
            sb.append('0');
        }
        sb.append(number);
        String paddedNumberAsString = sb.toString();
        return paddedNumberAsString;

    }
}
