package com.tsolution.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtil {
    private static Logger log = LogManager.getLogger(StringUtils.class);
    public static boolean isEmpty(List<?> list) {
        if (list == null || list.isEmpty())
            return true;
        return false;
    }

    public static boolean isEmpty(String v){
        if(v == null || v.isEmpty()){
            return true;
        }
        return false;
    }
    public static boolean isBlank(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }
    /*
    * Check phone number
    */
    public static boolean isPhoneNumber(String phoneNumber) {
        if ((phoneNumber != null) && !phoneNumber.isEmpty()) {
            boolean isValid = false;
            String expression = "^\\+[0-9]{1,3}\\-[0-9]{8,10}$";
            CharSequence inputStr = phoneNumber;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if ((phoneNumber.length() >= 9) || (phoneNumber.length() <= 13)) {
                if (matcher.matches()) {
                    isValid = true;
                }
            } else {
                isValid = false;
            }
            return isValid;
        }
        return false;
    }

    /**
     * Check email
     */
    public static boolean isEmail(String s) {
        if (CommonUtil.isEmpty(s)) {
            return false;
        }
        try {
            Pattern pattern = Pattern.compile("^[A-Za-z0-9+_.-]{4,64}+@(.+){3,10}$");
            Matcher matcher = pattern.matcher(s.trim());
            if (matcher.find()) {
                return true;
            }
        } catch (Exception e) {
            log.error(e);
        }
        return false;
    }

    /*
     * Check fax number
     */
    public static boolean isFaxNumber(String fax) {
        if ((fax != null) && !fax.isEmpty()) {
            boolean isValid = false;
            String expression = "^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$";
            CharSequence inputStr = fax;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(inputStr);
            if ((fax.length() >= 11) || (fax.length() <= 13)) {
                if (matcher.matches()) {
                    isValid = true;
                }
            } else {
                isValid = false;
            }
            return isValid;
        }
        return false;
    }

    public static <T> List<T> removeDuplicates(List<T> list) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : list) {
            // then add it
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }
        return newList;
    }
    public static String join(CharSequence delimiter, Iterable tokens) {
        StringBuilder sb = new StringBuilder();
        Iterator<?> it = tokens.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(delimiter);
                sb.append(it.next());
            }
        }
        return sb.toString();
    }

    public static Long tryToLong(String strLong) {
        try {
            return Long.parseLong(strLong);
        } catch (NumberFormatException ex) {
            return null;
        }

    }

    public static Double tryToDouble(String strDouble) {
        try {
            return Double.parseDouble(strDouble);
        } catch (NumberFormatException ex) {
            return null;
        }

    }
    public static LocalDateTime convertToLocalDateTimeViaMilisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
