package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "routing_plan_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RoutingPlanDetail extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "bill_lading_detail_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer billLadingDetailId;

    @Column(name = "routing_plan_day_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer routingPlanDayId;

    /**
     *  -1: hủy vận đơn, 1: bình thường
     */
    @Column(name = "status", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = " bill_package_routing_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long  billPackageRoutingId ;

    public Integer getBillLadingDetailId() {
        return billLadingDetailId;
    }

    public void setBillLadingDetailId(Integer billLadingDetailId) {
        this.billLadingDetailId = billLadingDetailId;
    }

    public Integer getRoutingPlanDayId() {
        return routingPlanDayId;
    }

    public void setRoutingPlanDayId(Integer routingPlanDayId) {
        this.routingPlanDayId = routingPlanDayId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getBillPackageRoutingId() {
        return billPackageRoutingId;
    }

    public void setBillPackageRoutingId(Long billPackageRoutingId) {
        this.billPackageRoutingId = billPackageRoutingId;
    }
}