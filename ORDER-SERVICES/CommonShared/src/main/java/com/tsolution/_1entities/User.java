package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;

import com.tsolution._1entities.oauth2.RoleDto;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.enums.UserType;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "users")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class User implements Serializable {

	private static final long serialVersionUID = -6533431224377312877L;

	@Id
	@Column(name = "username")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String username;

	@Column(name = "first_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String firstName;

	@Column(name = "last_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String lastName;

	@Column(name = "tel")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String tel;

	@Column(name = "email")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserType type;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@Column(name = "organization_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long organizationId;

	@ManyToOne
	@JoinColumn(name = "organization_id", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Organization organization;

	@CreatedDate
	@Column(name = "create_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView({ JsonEntityViewer.GOD.class })
	private LocalDateTime createDate;

	@CreatedBy
	@Column(name = "create_user")
	@JsonView({ JsonEntityViewer.GOD.class })
	private String createUser;

	@LastModifiedDate
	@Column(name = "update_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.GOD.class)
	private LocalDateTime updateDate;

	@LastModifiedBy
	@Column(name = "update_user")
	@JsonView(JsonEntityViewer.GOD.class)
	private String updateUser;

	@Transient
	@JsonView(JsonEntityViewer.GOD.class)
	private List<RoleDto> roles;

	public List<RoleDto> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleDto> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserType getType() {
		return this.type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LocalDateTime getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
