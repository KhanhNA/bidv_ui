package com.tsolution._1entities.enums;

public enum OBJECT_ATTACHMENT_TYPE {
	OBJ_ATTACH_TYPE_IMAGE(0), OBJ_ATTACH_TYPE_FILE(1);

	private Integer value;

	OBJECT_ATTACHMENT_TYPE(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

}
