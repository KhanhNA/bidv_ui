package com.tsolution._1entities.enums;

public enum RatingType {

    Friendly(1),/*Than thien*/
    PoorAttitude(2),/*Thai do kem*/
    EnthusiasticDriver(3); /*Lai xe nhiet tinh*/
    private Integer value;

    public Integer getValue() {
        return value;
    }

    RatingType(Integer value) {
        this.value = value;
    }
}
