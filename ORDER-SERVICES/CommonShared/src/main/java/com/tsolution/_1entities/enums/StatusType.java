package com.tsolution._1entities.enums;

public enum StatusType {
    DELETED(-1),

    STOPPED(0),

    RUNNING(1),

    WAITING(2),

    ALLDAY(1);

    private Integer value;

    public Integer getValue() {
        return value;
    }

    StatusType(Integer value) {
        this.value = value;
    }

}
