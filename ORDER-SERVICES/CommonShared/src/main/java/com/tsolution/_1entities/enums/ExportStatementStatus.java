package com.tsolution._1entities.enums;

public enum ExportStatementStatus {
	NEW(0), ACTUALLY_EXPORTED(1), CANCEL(2),IMAGE(0), ATTACHMENT(1);

	private Integer value;

	ExportStatementStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
