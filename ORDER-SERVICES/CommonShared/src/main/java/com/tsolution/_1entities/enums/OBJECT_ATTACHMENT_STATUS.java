package com.tsolution._1entities.enums;

public enum OBJECT_ATTACHMENT_STATUS {
	OBJ_ATTACHMENT_STATUS_USING(1), OBJ_ATTACHMENT_STATUS_DELETED(0);

	private Integer value;

	OBJECT_ATTACHMENT_STATUS(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
