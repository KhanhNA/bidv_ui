package com.tsolution._1entities.enums;

public enum NotificationType {
    SYSTEM_MESSAGE(0),

    DRIVER(1),

    CUSTOMER(2), SALE_MAN(3);

    private Integer value;

    public Integer getValue() {
        return value;
    }

    NotificationType(Integer value) {
        this.value = value;
    }

}
