package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.dto.BillServiceDto;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.tsolution.utils.SerializeTimeHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

@Table(name = "bill_lading_detail")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillLadingDetail extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "bill_lading_detail_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String billLadingDetailCode;

    @Column(name = "bill_lading_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long billLadingId;

    @Column(name = "warehouse_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer warehouseType;

    @Column(name = "total_weight")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double totalWeight;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "status_order")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer statusOrder;

    @Column(name = "approved_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer approvedType;

    @Column(name = "from_bill_lading_detail_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fromBillLadingDetailId;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "from_warehouse_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fromWarehouseId;

    @ManyToOne
    @JoinColumn(name = "warehouse_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @OneToMany
    @JoinColumn(name = "bill_lading_detail_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<BillPackage> listBillPackages;

    @JsonFormat(pattern = Constants.TIME_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeTimeHandler.class)
    @Column(name = "expected_from_time", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalTime expectedFromTime;

    @JsonFormat(pattern = Constants.TIME_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeTimeHandler.class)
    @Column(name = "expected_to_time", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalTime expectedToTime;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "bill_lading_detail_service", joinColumns = @JoinColumn(name = "bill_lading_detail_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "service_id", referencedColumnName = "id"))
    Set<BillService> services = new HashSet<>();

    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private List<BillServiceDto> billServiceDtoList;

    public String getBillLadingDetailCode() {
        return billLadingDetailCode;
    }

    public void setBillLadingDetailCode(String billLadingDetailCode) {
        this.billLadingDetailCode = billLadingDetailCode;
    }

    public Long getBillLadingId() {
        return billLadingId;
    }

    public void setBillLadingId(Long billLadingId) {
        this.billLadingId = billLadingId;
    }

    public Integer getWarehouseType() {
        return warehouseType;
    }

    public void setWarehouseType(Integer warehouseType) {
        this.warehouseType = warehouseType;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(Integer statusOrder) {
        this.statusOrder = statusOrder;
    }

    public Integer getApprovedType() {
        return approvedType;
    }

    public void setApprovedType(Integer approvedType) {
        this.approvedType = approvedType;
    }

    public Long getFromBillLadingDetailId() {
        return fromBillLadingDetailId;
    }

    public void setFromBillLadingDetailId(Long fromBillLadingDetailId) {
        this.fromBillLadingDetailId = fromBillLadingDetailId;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalTime getExpectedFromTime() {
        return expectedFromTime;
    }

    public void setExpectedFromTime(LocalTime expectedFromTime) {
        this.expectedFromTime = expectedFromTime;
    }

    public LocalTime getExpectedToTime() {
        return expectedToTime;
    }

    public void setExpectedToTime(LocalTime expectedToTime) {
        this.expectedToTime = expectedToTime;
    }

    public List<BillPackage> getListBillPackages() {
        return listBillPackages;
    }

    public void setListBillPackages(List<BillPackage> listBillPackages) {
        this.listBillPackages = listBillPackages;
    }

    public Set<BillService> getServices() {
        return services;
    }

    public void setServices(Set<BillService> services) {
        this.services = services;
    }

    public List<BillServiceDto> getBillServiceDtoList() {
        return billServiceDtoList;
    }

    public void setBillServiceDtoList(List<BillServiceDto> billServiceDtoList) {
        this.billServiceDtoList = billServiceDtoList;
    }

    public Long getFromWarehouseId() {
        return fromWarehouseId;
    }

    public void setFromWarehouseId(Long fromWarehouseId) {
        this.fromWarehouseId = fromWarehouseId;
    }

    public String toString() {
        return "BillLadingDetail{billLadingDetailCode=" + billLadingDetailCode +
                ", billLadingId=" + billLadingId +
                ", warehouseId=" + "" +
                ", warehouseType=" + warehouseType +
                ", totalWeight=" + totalWeight +
                ", status=" + status +
                ", statusOrder=" + statusOrder +
                ", approvedType=" + approvedType +
                ", fromBillLadingDetailId=" + fromBillLadingDetailId +
                ", description=" + description +
                ", serviceId=" + "" +
                ", expectedFromTime=" + expectedFromTime +
                ", expectedToTime=" + expectedToTime +
                "}";
    }
}