package com.tsolution._1entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "warehouse")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Warehouse extends SuperEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4192052442467510279L;

    @Column(name = "code", nullable = true)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String code;

    @Column(name = "name", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String name;

    @Column(name = "description", nullable = true)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @ManyToOne
    @JoinColumn(name = "organization_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Column(name = "address", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "phone", nullable = true)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "lat", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal lat;

    @Column(name = "lng", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal lng;

    /**
     * diện tích đơn vị M^2
     */
    @Column(name = "area", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer area;

    @Column(name = "status", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "approve_date", nullable = true)
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime approveDate;

    @Column(name = "approve_user", nullable = true)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String approveUser;

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Customer customer;


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getLat() {
        return this.lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return this.lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public Integer getArea() {
        return this.area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getApproveDate() {
        return this.approveDate;
    }

    public void setApproveDate(LocalDateTime approveDate) {
        this.approveDate = approveDate;
    }

    public String getApproveUser() {
        return this.approveUser;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }




}
