package com.tsolution._1entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VanDto implements Serializable {
    private static final long serialVersionUID = 11651651434L;
    private Long id;
    private String licencePlate;
    private Long fleetId;
    private Integer vanTypeId;
    private Integer fuelTypeId;
    private Integer year;
    private String name;
    private String vehicleTonnage;
    private Integer tonnageFrom;
    private Integer tonnageTo;
    private Integer statusAvailable;
    private String inspectionDueDate;
    private Long parkingPointId;
    private String parkingPointName;
    private Integer statusCar;
    private String multiCode;

}
