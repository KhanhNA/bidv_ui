package com.tsolution._1entities.enums;

public enum CLAIM_HAS_UPLOAD_TYPE {
	NO_UPLOAD(0), UPLOADED(1), DELETE_UPLOADED(-1);

	private Integer value;

	CLAIM_HAS_UPLOAD_TYPE(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
