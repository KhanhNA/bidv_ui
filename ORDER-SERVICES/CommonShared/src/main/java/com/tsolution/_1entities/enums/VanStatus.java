package com.tsolution._1entities.enums;

public enum VanStatus {

    DEACTIVATED(0),
    ACTIVE(1),
    MAINTAINENCE(2);
    private Integer value;

    public Integer getValue() {
        return value;
    }

    VanStatus(Integer value) {
        this.value = value;
    }
}
