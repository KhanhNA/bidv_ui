package com.tsolution._1entities.enums;

public enum ACTION {
	INSERT(1), UPDATE(2);

	private Integer value;

	private ACTION(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

}
