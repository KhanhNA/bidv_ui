package com.tsolution._1entities.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Entity;
import java.io.Serializable;


@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillServiceDto implements Serializable {
    private static final long serialVersionUID = -66259723250072947L;

    private Long billServiceId;
    private String billServiceName;

    public Long getBillServiceId() {
        return billServiceId;
    }

    public void setBillServiceId(Long billServiceId) {
        this.billServiceId = billServiceId;
    }

    public String getBillServiceName() {
        return billServiceName;
    }

    public void setBillServiceName(String billServiceName) {
        this.billServiceName = billServiceName;
    }
}
