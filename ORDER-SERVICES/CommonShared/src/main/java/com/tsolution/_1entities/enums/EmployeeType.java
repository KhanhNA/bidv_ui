package com.tsolution._1entities.enums;

public enum EmployeeType {
    ADMIN_GROUP(0),
    GROUP_STOCK_MANAGER(1),
    STOCK_KEEPER(2);

    private Integer value;

    EmployeeType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
