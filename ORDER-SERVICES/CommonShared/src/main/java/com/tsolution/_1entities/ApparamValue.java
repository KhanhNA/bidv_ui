package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "apparam_value")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ApparamValue extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    @ManyToOne
    @JoinColumn(name = "param_id",nullable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private AppParam appParam;

    @Column(name = "param_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String paramCode;

    @Column(name = "param_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String paramName;

    @Column(name = "ord")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer ord;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    public AppParam getAppParam() {
        return appParam;
    }

    public void setAppParam(AppParam appParam) {
        this.appParam = appParam;
    }

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
