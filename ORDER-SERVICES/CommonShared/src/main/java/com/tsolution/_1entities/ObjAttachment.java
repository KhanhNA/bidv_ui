package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "obj_attachment")
public class ObjAttachment extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "obj_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long objId;

    @Column(name = "attach_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer attachType;

    @Column(name = "attach_url")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String attachUrl;

    @Column(name = "ord")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer ord;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "attach_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String attachName;

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public Long getObjId() {
        return objId;
    }

    public void setObjId(Long objId) {
        this.objId = objId;
    }

    public Integer getAttachType() {
        return attachType;
    }

    public void setAttachType(Integer attachType) {
        this.attachType = attachType;
    }

    public String getAttachUrl() {
        return attachUrl;
    }

    public void setAttachUrl(String attachUrl) {
        this.attachUrl = attachUrl;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        return "ObjAttachment{objId=" + objId +
                ", attachType=" + attachType +
                ", attachType=" + attachName +
                ", attachUrl=" + attachUrl +
                ", ord=" + ord +
                ", status=" + status +
                "}";
    }
}