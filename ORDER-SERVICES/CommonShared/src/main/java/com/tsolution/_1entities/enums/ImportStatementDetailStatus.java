package com.tsolution._1entities.enums;

public enum ImportStatementDetailStatus {
	NEW_IMPORT(0), ACTUALLY_IMPORTED(1), NEW_EXPORT(2), ACTUALLY_EXPORTED(3), CANCEL(4);

	private Integer value;

	ImportStatementDetailStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
