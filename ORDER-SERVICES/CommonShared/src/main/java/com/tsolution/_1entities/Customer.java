package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "customer")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Customer extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5996377597713841726L;

	/**
	 * tương ứng với username để đăng nhập
	 */
	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "address", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address;

	@Column(name = "phone", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone;

	/**
	 * 0: Không sử dụng dịch vụ nữa, 1: đang sử dụng dịch vụ
	 */
	@Column(name = "status", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@Column(name = "is_vip", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean vip;

	@Column(name = "address1")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address1;

	@Column(name = "address2")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address2;

	@Column(name = "tax_code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String taxCode;

	@Column(name = "contact_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String contactName;

	@Column(name = "city")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String city;

	@Column(name = "state_province")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String stateProvince;

	@Column(name = "postal_code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String postalCode;

	@Column(name = "phone1")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone1;

	@Column(name = "phone2")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone2;

	@Column(name = "fax")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String fax;

	@Column(name = "website")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String website;

	@Column(name = "has_image")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer hasImage;

	@Column(name = "has_attachment")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer hasAttachment;

	@Column(name = "email")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	@Column(name = "payment_type")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String paymentType;

	@Column(name = "pay_by_day")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime payByDay;

	@Column(name = "discount_type")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String discountType;

	@Column(name = "discount")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double discount;

	@Column(name = "app_param_value_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long appParamValueId;

	@Column(name = "mobile_phone")
	private String mobilePhone;

	@Column(name = "idNo")
	private String idNo;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean getVip() {
		return vip;
	}

	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Integer getHasImage() {
		return hasImage;
	}

	public void setHasImage(Integer hasImage) {
		this.hasImage = hasImage;
	}

	public Integer getHasAttachment() {
		return hasAttachment;
	}

	public void setHasAttachment(Integer hasAttachment) {
		this.hasAttachment = hasAttachment;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public LocalDateTime getPayByDay() {
		return payByDay;
	}

	public void setPayByDay(LocalDateTime payByDay) {
		this.payByDay = payByDay;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Long getAppParamValueId() {
		return appParamValueId;
	}

	public void setAppParamValueId(Long appParamValueId) {
		this.appParamValueId = appParamValueId;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
}
