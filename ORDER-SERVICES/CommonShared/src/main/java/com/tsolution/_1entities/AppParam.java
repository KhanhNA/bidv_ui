package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "app_param")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class AppParam extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "app_param_group_id",nullable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private ApparamGroup apparamGroup;

    @Column(name = "app_param_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String appParamCode;

    @Column(name = "app_param_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String appParamName;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "ord")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer ord;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @OneToMany
    @JoinColumn(name = "param_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<ApparamValue> apparamValues;


    public ApparamGroup getApparamGroup() {
        return apparamGroup;
    }

    public void setApparamGroup(ApparamGroup apparamGroup) {
        this.apparamGroup = apparamGroup;
    }

    public String getAppParamCode() {
        return appParamCode;
    }

    public void setAppParamCode(String appParamCode) {
        this.appParamCode = appParamCode;
    }

    public String getAppParamName() {
        return appParamName;
    }

    public void setAppParamName(String appParamName) {
        this.appParamName = appParamName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<ApparamValue> getApparamValues() {
        return apparamValues;
    }

    public void setApparamValues(List<ApparamValue> apparamValues) {
        this.apparamValues = apparamValues;
    }

}
