package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "van")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Van extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String name;

    @Column(name = "licence_plate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String licencePlate;

    @Column(name = "location")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String location;

    @Column(name = "cost")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double cost;

    @Column(name = "driver")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String driver;

    @Column(name = "cost_center")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double costCenter;

    @Column(name = "maintenance_template_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer maintenanceTemplateId;

    @Column(name = "axles")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double axles;

    @Column(name = "tire_front_size")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double tireFrontSize;

    @Column(name = "tire_front_pressure")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double tireFrontPressure;

    @Column(name = "tire_rear_size")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double tireRearSize;

    @Column(name = "tire_rear_pressure")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double tireRearPressure;

    @Column(name = "has_image")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasImage;

    @Column(name = "has_attachment")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasAttachment;

    @Column(name = "note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String note;

    @Column(name = "warranty_name1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String warrantyName1;

    @Column(name = "warranty_date1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime warrantyDate1;

    @Column(name = "warranty_meter1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double warrantyMeter1;

    @Column(name = "warranty_name2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String warrantyName2;

    @Column(name = "warranty_date2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime warrantyDate2;

    @Column(name = "warranty_meter2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double warrantyMeter2;

    @Column(name = "van_type_Id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long vanTypeId;

    @Column(name = "app_param_value_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long appParamValueId;

    @Column(name = "vehicle_tonnage")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double vehicleTonnage;

    @Column(name = "van_inspection")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String vanInspection;

    @Column(name = "capacity")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double capacity;

    @Column(name = "available_capacity")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double availableCapacity;

    @Column(name = "status_car")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer statusCar;

    @Column(name = "status_available")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer statusAvailable;

    @Column(name = "fleet_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fleetId;

    @Column(name = "vehicle_registration")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String vehicleRegistration;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "body_length")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double bodyLength;

    @Column(name = "body_width")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double bodyWidth;

    @Column(name = "height")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double height;

    @Column(name = "wheelbase")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double wheelbase;

    @Column(name = "gross_weight")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double grossWeight;

    @Column(name = "engine_size")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double engineSize;

    @Column(name = "fuel_type_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer fuelTypeId;

    @Column(name = "parking_point_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long parkingPointId;

    @Column(name = "inspection_due_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    private LocalDateTime inspectionDueDate;

    @Column(name = "year")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer year;

    @Column(name = "color")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String color;

    @Column(name = "longitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double longitude;

    @Column(name = "latitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double latitude;

    @Transient
    private String multiCode;
    @Transient
    private String parkingPointName;
    @Transient
    private List<ObjAttachment> listAttachments;
    @Transient
    private List<ObjAttachment> listRemovedAttachments;
    @Transient
    private List<ObjAttachment> listImages;
    @Transient
    private List<ObjAttachment> listRemovedImages;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<ObjAttachment> getListAttachments() {
        return listAttachments;
    }

    public void setListAttachments(List<ObjAttachment> listAttachments) {
        this.listAttachments = listAttachments;
    }

    public List<ObjAttachment> getListRemovedAttachments() {
        return listRemovedAttachments;
    }

    public void setListRemovedAttachments(List<ObjAttachment> listRemovedAttachments) {
        this.listRemovedAttachments = listRemovedAttachments;
    }

    public List<ObjAttachment> getListImages() {
        return listImages;
    }

    public void setListImages(List<ObjAttachment> listImages) {
        this.listImages = listImages;
    }

    public List<ObjAttachment> getListRemovedImages() {
        return listRemovedImages;
    }

    public void setListRemovedImages(List<ObjAttachment> listRemovedImages) {
        this.listRemovedImages = listRemovedImages;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Double getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(Double costCenter) {
        this.costCenter = costCenter;
    }

    public Integer getMaintenanceTemplateId() {
        return maintenanceTemplateId;
    }

    public void setMaintenanceTemplateId(Integer maintenanceTemplateId) {
        this.maintenanceTemplateId = maintenanceTemplateId;
    }

    public Double getAxles() {
        return axles;
    }

    public void setAxles(Double axles) {
        this.axles = axles;
    }

    public Double getTireFrontSize() {
        return tireFrontSize;
    }

    public void setTireFrontSize(Double tireFrontSize) {
        this.tireFrontSize = tireFrontSize;
    }

    public Double getTireFrontPressure() {
        return tireFrontPressure;
    }

    public void setTireFrontPressure(Double tireFrontPressure) {
        this.tireFrontPressure = tireFrontPressure;
    }

    public Double getTireRearSize() {
        return tireRearSize;
    }

    public void setTireRearSize(Double tireRearSize) {
        this.tireRearSize = tireRearSize;
    }

    public Double getTireRearPressure() {
        return tireRearPressure;
    }

    public void setTireRearPressure(Double tireRearPressure) {
        this.tireRearPressure = tireRearPressure;
    }

    public Integer getHasImage() {
        return hasImage;
    }

    public void setHasImage(Integer hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getWarrantyName1() {
        return warrantyName1;
    }

    public void setWarrantyName1(String warrantyName1) {
        this.warrantyName1 = warrantyName1;
    }

    public LocalDateTime getWarrantyDate1() {
        return warrantyDate1;
    }

    public void setWarrantyDate1(LocalDateTime warrantyDate1) {
        this.warrantyDate1 = warrantyDate1;
    }

    public Double getWarrantyMeter1() {
        return warrantyMeter1;
    }

    public void setWarrantyMeter1(Double warrantyMeter1) {
        this.warrantyMeter1 = warrantyMeter1;
    }

    public String getWarrantyName2() {
        return warrantyName2;
    }

    public void setWarrantyName2(String warrantyName2) {
        this.warrantyName2 = warrantyName2;
    }

    public LocalDateTime getWarrantyDate2() {
        return warrantyDate2;
    }

    public void setWarrantyDate2(LocalDateTime warrantyDate2) {
        this.warrantyDate2 = warrantyDate2;
    }

    public Double getWarrantyMeter2() {
        return warrantyMeter2;
    }

    public void setWarrantyMeter2(Double warrantyMeter2) {
        this.warrantyMeter2 = warrantyMeter2;
    }

    public Long getVanTypeId() {
        return vanTypeId;
    }

    public void setVanTypeId(Long vanTypeId) {
        this.vanTypeId = vanTypeId;
    }

    public Long getAppParamValueId() {
        return appParamValueId;
    }

    public void setAppParamValueId(Long appParamValueId) {
        this.appParamValueId = appParamValueId;
    }

    public Double getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(Double vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getVanInspection() {
        return vanInspection;
    }

    public void setVanInspection(String vanInspection) {
        this.vanInspection = vanInspection;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Double getAvailableCapacity() {
        return availableCapacity;
    }

    public void setAvailableCapacity(Double availableCapacity) {
        this.availableCapacity = availableCapacity;
    }

    public Integer getStatusCar() {
        return statusCar;
    }

    public void setStatusCar(Integer statusCar) {
        this.statusCar = statusCar;
    }

    public Integer getStatusAvailable() {
        return statusAvailable;
    }

    public void setStatusAvailable(Integer statusAvailable) {
        this.statusAvailable = statusAvailable;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public String getVehicleRegistration() {
        return vehicleRegistration;
    }

    public void setVehicleRegistration(String vehicleRegistration) {
        this.vehicleRegistration = vehicleRegistration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getBodyLength() {
        return bodyLength;
    }

    public void setBodyLength(Double bodyLength) {
        this.bodyLength = bodyLength;
    }

    public Double getBodyWidth() {
        return bodyWidth;
    }

    public void setBodyWidth(Double bodyWidth) {
        this.bodyWidth = bodyWidth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWheelbase() {
        return wheelbase;
    }

    public void setWheelbase(Double wheelbase) {
        this.wheelbase = wheelbase;
    }

    public Double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Double grossWeight) {
        this.grossWeight = grossWeight;
    }

    public Double getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(Double engineSize) {
        this.engineSize = engineSize;
    }

    public Integer getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public Long getParkingPointId() {
        return parkingPointId;
    }

    public void setParkingPointId(Long parkingPointId) {
        this.parkingPointId = parkingPointId;
    }

    public String getParkingPointName() {
        return parkingPointName;
    }

    public void setParkingPointName(String parkingPointName) {
        this.parkingPointName = parkingPointName;
    }

    public String getMultiCode() {
        return multiCode;
    }

    public void setMultiCode(String multiCode) {
        this.multiCode = multiCode;
    }

    public LocalDateTime getInspectionDueDate() {
        return inspectionDueDate;
    }

    public void setInspectionDueDate(LocalDateTime inspectionDueDate) {
        this.inspectionDueDate = inspectionDueDate;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String toString() {
        return "Van{licencePlate=" + licencePlate +
                ", location=" + location +
                ", driver=" + driver +
                ", costCenter=" + costCenter +
                ", maintenanceTemplateId=" + maintenanceTemplateId +
                ", axles=" + axles +
                ", tireFrontSize=" + tireFrontSize +
                ", tireFrontPressure=" + tireFrontPressure +
                ", tireRearSize=" + tireRearSize +
                ", tireRearPressure=" + tireRearPressure +
                ", hasImage=" + hasImage +
                ", hasAttachment=" + hasAttachment +
                ", note=" + note +
                ", warrantyName1=" + warrantyName1 +
                ", warrantyDate1=" + warrantyDate1 +
                ", warrantyMeter1=" + warrantyMeter1 +
                ", warrantyName2=" + warrantyName2 +
                ", warrantyDate2=" + warrantyDate2 +
                ", warrantyMeter2=" + warrantyMeter2 +
                ", vanTypeId=" + vanTypeId +
                ", appParamValueId=" + appParamValueId +
                ", vehicleTonnage=" + vehicleTonnage +
                ", vanInspection=" + vanInspection +
                ", capacity=" + capacity +
                ", availableCapacity=" + availableCapacity +
                ", statusCar=" + statusCar +
                ", statusAvailable=" + statusAvailable +
                ", fleetId=" + fleetId +
                ", vehicleRegistration=" + vehicleRegistration +
                ", description=" + description +
                ", bodyLength=" + bodyLength +
                ", bodyWidth=" + bodyWidth +
                ", height=" + height +
                ", wheelbase=" + wheelbase +
                ", grossWeight=" + grossWeight +
                ", engineSize=" + engineSize +
                ", fuelTypeId=" + fuelTypeId +
                "}";
    }
}