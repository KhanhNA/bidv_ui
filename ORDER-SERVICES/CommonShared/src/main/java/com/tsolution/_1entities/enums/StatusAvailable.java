package com.tsolution._1entities.enums;

public enum StatusAvailable {
    UNAVAILABLE(0), AVAILABLE(1);

    private Integer value;

    public Integer getValue() {
        return value;
    }

    StatusAvailable(Integer value) {
        this.value = value;
    }
}
