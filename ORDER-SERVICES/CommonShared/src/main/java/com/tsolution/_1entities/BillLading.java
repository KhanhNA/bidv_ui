package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.*;

@Table(name = "bill_lading")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillLading extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "bill_lading_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String billLadingCode;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Customer customer;



    @ManyToOne
    @JoinColumn(name = "insurance_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "bill_cycle_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BillCycle billCycle;

    @OneToMany
    @JoinColumn(name = "bill_lading_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<BillLadingDetail> billLadingDetails;

    @Column(name = "total_weight")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double totalWeight;

    /* Total money hasn't  VAT*/
    @Column(name = "total_amount")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal  totalAmount;

    /* Total money has  VAT*/
    @Column(name = "total_payment")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal  totalPayment;

    /* Phu phi cau duong*/
    @Column(name = "tolls")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal  tolls;

    /* Phu phi cau duong*/
    @Column(name = "surcharge")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BigDecimal  surcharge;


    /* Tong the tich, tinh theo m3*/
    @Column(name = "total_volume")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double  totalVolumn;

    @Column(name = "vat")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer  vat;

    @Column(name = "promotion_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String promotionCode;

    @Column(name = "release_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer releaseType;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "total_parcel")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer totalPacel;



    public String getBillLadingCode() {
        return billLadingCode;
    }

    public void setBillLadingCode(String billLadingCode) {
        this.billLadingCode = billLadingCode;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getReleaseType() {
        return releaseType;
    }

    public void setReleaseType(Integer releaseType) {
        this.releaseType = releaseType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }

    public BigDecimal getTolls() {
        return tolls;
    }

    public void setTolls(BigDecimal tolls) {
        this.tolls = tolls;
    }

    public BigDecimal getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(BigDecimal surcharge) {
        this.surcharge = surcharge;
    }

    public Double getTotalVolumn() {
        return totalVolumn;
    }

    public void setTotalVolumn(Double totalVolumn) {
        this.totalVolumn = totalVolumn;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public BillCycle getBillCycle() {
        return billCycle;
    }

    public void setBillCycle(BillCycle billCycle) {
        this.billCycle = billCycle;
    }

    public List<BillLadingDetail> getBillLadingDetails() {
        return billLadingDetails;
    }

    public void setBillLadingDetails(List<BillLadingDetail> billLadingDetails) {
        this.billLadingDetails = billLadingDetails;
    }

    public Integer getTotalPacel() {
        return totalPacel;
    }

    public void setTotalPacel(Integer totalPacel) {
        this.totalPacel = totalPacel;
    }

    @Override
    public String toString() {
        return "BillLading{" +
                "billLadingCode='" + billLadingCode + '\'' +
                ", customer=" + customer +
                ", insurance=" + insurance +
                ", billCycle=" + billCycle +
                ", totalWeight=" + totalWeight +
                ", totalAmount=" + totalAmount +
                ", totalPayment=" + totalPayment +
                ", tolls=" + tolls +
                ", surcharge=" + surcharge +
                ", totalVolumn=" + totalVolumn +
                ", vat=" + vat +
                ", promotionCode='" + promotionCode + '\'' +
                ", releaseType=" + releaseType +
                ", status=" + status +
                '}';
    }
}