package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "eq_part")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class EqPart extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 516519874941L;

    @Column(name = "van_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long vanId;

    @Column(name = "part_no")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long partNo;

    @Column(name = "name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String name;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "vendor_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long vendorId;

    @Column(name = "category_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long categoryType;

    @Column(name = "unit_cost")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String unitCost;

    @Column(name = "unit_measure")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String unitMeasure;

    @Column(name = "universal_product_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String universalProductCode;

    @Column(name = "parent_part_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String parentPartId;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private int status;

    @Transient
    private List<ObjAttachment> listAttachments;
    @Transient
    private List<ObjAttachment> listRemovedAttachments;
    @Transient
    private List<ObjAttachment> listImages;
    @Transient
    private List<ObjAttachment> listRemovedImages;

    private String vendorName;

    private String categoryName;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ObjAttachment> getListAttachments() {
        return listAttachments;
    }

    public void setListAttachments(List<ObjAttachment> listAttachments) {
        this.listAttachments = listAttachments;
    }

    public List<ObjAttachment> getListRemovedAttachments() {
        return listRemovedAttachments;
    }

    public void setListRemovedAttachments(List<ObjAttachment> listRemovedAttachments) {
        this.listRemovedAttachments = listRemovedAttachments;
    }

    public List<ObjAttachment> getListImages() {
        return listImages;
    }

    public void setListImages(List<ObjAttachment> listImages) {
        this.listImages = listImages;
    }

    public List<ObjAttachment> getListRemovedImages() {
        return listRemovedImages;
    }

    public void setListRemovedImages(List<ObjAttachment> listRemovedImages) {
        this.listRemovedImages = listRemovedImages;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getVanId() {
        return vanId;
    }

    public void setVanId(Long vanId) {
        this.vanId = vanId;
    }

    public Long getPartNo() {
        return partNo;
    }

    public void setPartNo(Long partNo) {
        this.partNo = partNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Long categoryType) {
        this.categoryType = categoryType;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getUniversalProductCode() {
        return universalProductCode;
    }

    public void setUniversalProductCode(String universalProductCode) {
        this.universalProductCode = universalProductCode;
    }

    public String getParentPartId() {
        return parentPartId;
    }

    public void setParentPartId(String parentPartId) {
        this.parentPartId = parentPartId;
    }

    public String getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(String unitCost) {
        this.unitCost = unitCost;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
