package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "driver_van")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class DriverVan extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn( name = "staff_id", columnDefinition = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Staff staff;

    @ManyToOne
    @JoinColumn(name = "van_id",columnDefinition = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Van van;

    @Column(name = "from_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime toDate;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;


    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Van getVan() {
        return van;
    }

    public void setVan(Van van) {
        this.van = van;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

//    public String toString() {
//      return "DriverVan{staffId=" + staffId +
//        ", vanId=" + vanId +
//        ", fromDate=" + fromDate +
//        ", toDate=" + toDate +
//        ", status=" + status +
//        "}";
//    }
}