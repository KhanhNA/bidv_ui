package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "claim_report")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ClaimReport extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "quantity")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer quantity;

    @Column(name = "has_image")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasImage;

    @Column(name = "has_attachment")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasAttachment;

    @Column(name = "note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String note;

    @ManyToOne
    @JoinColumn(name = "warehouse_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "driver_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Staff driver;

    @ManyToOne
    @JoinColumn(name = "stock_man_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Employee stockman;

    @ManyToOne
    @JoinColumn(name = "app_param_value_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private ApparamValue apparamValue;

    @OneToMany
    @Transient
    @JsonIgnore
    @JoinColumn(name = "id", referencedColumnName = "object_id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<ObjAttachment> objAttachments;

    @ManyToOne
    @JoinColumn(name = "routing_plan_detail_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private RoutingPlanDetail routingPlanDetail;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    public List<ObjAttachment> getObjAttachments() {
        return objAttachments;
    }

    public void setObjAttachments(List<ObjAttachment> objAttachments) {
        this.objAttachments = objAttachments;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getHasImage() {
        return hasImage;
    }

    public void setHasImage(Integer hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Staff getDriver() {
        return driver;
    }

    public void setDriver(Staff driver) {
        this.driver = driver;
    }

    public Employee getStockman() {
        return stockman;
    }

    public void setStockman(Employee stockman) {
        this.stockman = stockman;
    }

    public ApparamValue getApparamValue() {
        return apparamValue;
    }

    public void setApparamValue(ApparamValue apparamValue) {
        this.apparamValue = apparamValue;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public RoutingPlanDetail getRoutingPlanDetail() {
        return routingPlanDetail;
    }

    public void setRoutingPlanDetail(RoutingPlanDetail routingPlanDetail) {
        this.routingPlanDetail = routingPlanDetail;
    }
}