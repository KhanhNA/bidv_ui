package com.tsolution._2repositories;

import com.tsolution._1entities.BillService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface BillServiceRepository extends JpaRepository<BillService, Long>, JpaSpecificationExecutor<BillService> {
    Optional<BillService> findByServiceCode(String code);
    Optional<BillService> findByServiceName(String name);

    List<BillService> findByStatus(int status);

    @Query(value = "Select * from service where service_type =?1",
            countQuery = "select count(*) from service",
            nativeQuery = true)
    Collection<BillService> findServiceByType(int type);
}
