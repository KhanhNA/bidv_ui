package com.tsolution._2repositories;

import com.tsolution._1entities.Organization;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
public interface OrganizationRepositoryCustom {
    Page<Organization> find(Organization organization, Pageable pageable) throws BusinessException;

}
