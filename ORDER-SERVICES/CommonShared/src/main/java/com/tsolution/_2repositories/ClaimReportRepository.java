package com.tsolution._2repositories;

import com.tsolution._1entities.RoutingPlanDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ClaimReport;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClaimReportRepository extends JpaRepository<ClaimReport, Long>, JpaSpecificationExecutor<ClaimReport> {
    @Query("SELECT u FROM ClaimReport u WHERE u.routingPlanDetail = ?1 and u.status = 1")
    List<ClaimReport> findAllByRoutingPlanDetail(RoutingPlanDetail routingPlanDetail);
}