package com.tsolution._2repositories.impl;

import com.tsolution._1entities.DayOff;
import com.tsolution._2repositories.DayOffRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DayOffRepositoryCustomImpl implements DayOffRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DayOff> find(Long staffId, Date fDate, Date tDate, Integer spec , Integer status) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sb.append(" FROM day_off do  WHERE 1=1 ");
        if (staffId !=null){
            sb.append(" and do.staff_id = :staff_id ");
            params.put("staff_id", staffId);
        }
        if (status != null){
            sb.append(" and do.status = :status");
            params.put("status", status);
        }
        if (fDate !=null && tDate !=null )
        {
            sb.append(" and do.from_date >= :fdate ");
            sb.append(" and do.to_date <= :tdate ");
            params.put("fdate",fDate);
            params.put("tdate",tDate);
        }
        if (spec != null){

            sb.append(" and do.spec = :spec ");
            params.put("spec",spec);
        }
        StringBuilder strQuery = new StringBuilder(" SELECT do.* ");
        strQuery.append(sb);

        return BaseRepository.getResultListNativeQuery(this.em,strQuery.toString(),params, DayOff.class);

    }

    /*@Override
    public List<DayOff> findAvailable(Long staffId,Date fdate, Date tdate) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sb.append(" FROM day_off do WHERE 1=1 and do.status = 1  ");

        if (staffId !=null){
            sb.append(" and do.staff_id = :staff_id ");
            params.put("staff_id", staffId);
        }

        if (fdate !=null && tdate !=null )
        {
            sb.append(" and DATE_FORMAT(do.from_date, '%Y-%m-%d') <= :fdate ");
            sb.append(" and do.to_date >= :tdate ");
            params.put("fdate",fdate);
            params.put("tdate",tdate);
        }

        StringBuilder strQuery = new StringBuilder(" SELECT do.* ");
        strQuery.append(sb);

        return BaseRepository.getResultListNativeQuery(this.em,strQuery.toString(),params, DayOff.class);
    }*/
}
