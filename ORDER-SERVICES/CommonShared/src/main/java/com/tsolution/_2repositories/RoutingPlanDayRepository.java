package com.tsolution._2repositories;

import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingVan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoutingPlanDayRepository extends JpaRepository<RoutingPlanDay, Long>, JpaSpecificationExecutor<RoutingPlanDay> , RoutingPlanDayRepositoryCustom {
    @Query("SELECT r FROM RoutingPlanDay r WHERE r.status = ?1 and routingVan = ?2")
    List<RoutingPlanDay> findAllByStatusAndRoutingVan(Integer status, RoutingVan routingVan);
}