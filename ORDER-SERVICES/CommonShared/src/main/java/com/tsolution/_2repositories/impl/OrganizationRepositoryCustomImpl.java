package com.tsolution._2repositories.impl;

import com.tsolution._1entities.Organization;
import com.tsolution._2repositories.OrganizationRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class OrganizationRepositoryCustomImpl implements OrganizationRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Organization> find(Organization org, Pageable pageable) throws BusinessException {
        StringBuilder query = new StringBuilder();

        query.append("  FROM organization o WHERE");
        query.append("  (:status IS NULL OR o.status = :status) AND ");
        query.append("  (:code IS NULL OR LOWER(o.code) LIKE LOWER(CONCAT('%',:code,'%'))) AND ");
        query.append("  (:name IS NULL OR LOWER(o.name) LIKE LOWER(CONCAT('%',:name,'%'))) ");
        query.append("  ORDER BY o.code, o.name ");

        Map<String, Object> params = new HashMap<>();
        params.put("status", org.getStatus());
        params.put("code", org.getCode());
        params.put("name", org.getName());

        StringBuilder strListQuery = new StringBuilder(" SELECT o.* ");
        strListQuery.append(query);

        StringBuilder strCountQuery = new StringBuilder(" SELECT count(o.id) ");
        strCountQuery.append(query);

        return BaseRepository.getPagedNativeQuery(this.em, strListQuery.toString(), strCountQuery.toString(),
                params, pageable, Organization.class);
    }
}
