package com.tsolution._2repositories;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.Organization;

public interface OrganizationRepository
		extends JpaRepository<Organization, Long>, JpaSpecificationExecutor<Organization>, OrganizationRepositoryCustom {
	@Query(value = "SELECT SYSDATE()", nativeQuery = true)
	LocalDateTime getSysdate();

    Optional<Organization> findByCode(String code);

}
