package com.tsolution._2repositories;

import com.tsolution._1entities.DriverLicense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface DriverLicenseRepository extends JpaRepository<DriverLicense, Long>, JpaSpecificationExecutor<DriverLicense> {
    Optional<DriverLicense> findByStaffId(Long staffId);
    Optional<DriverLicense> findById(Long staffId);
}
