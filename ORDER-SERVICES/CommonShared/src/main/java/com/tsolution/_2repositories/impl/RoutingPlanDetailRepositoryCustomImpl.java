package com.tsolution._2repositories.impl;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingPlanDetail;
import com.tsolution._2repositories.RoutingPlanDetailRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
@Repository
public class RoutingPlanDetailRepositoryCustomImpl implements RoutingPlanDetailRepositoryCustom {
    @PersistenceContext
    EntityManager em;
    @Override
    public List<RoutingPlanDetail> getRoutingPlanDetailByRoutingVanAndBillPackage(RoutingPlanDay rpd, BillPackageRouting bpr) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select rpd.* from sharevan.routing_plan_detail rpd ") ;
        sb.append("join sharevan.bill_package_routing bpr on bpr.id = rpd.bill_package_routing_id ") ;
        sb.append("join sharevan.routing_plan_day rp on rp.id = rpd.routing_plan_day_id ");
        sb.append("where rp.id = :routingPlanId and bpr.id = :billPackageRoutingId");
        HashMap<String,Object> params = new HashMap<>();
        params.put("routingPlanId", rpd.getId());
        params.put("billPackageRoutingId", bpr.getId());
        return BaseRepository.getResultListNativeQuery(em,sb.toString(),params, RoutingPlanDetail.class);
    }
}
