package com.tsolution._2repositories;

import com.tsolution._1entities.WarehouseDistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WarehouseDistanceRepository extends JpaRepository<WarehouseDistance, Integer>, JpaSpecificationExecutor<WarehouseDistance> {

}