package com.tsolution._2repositories;

import com.tsolution._1entities.EqPart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EqPartRepository extends JpaRepository<EqPart, Long>, JpaSpecificationExecutor<EqPart>{
    List<EqPart> findByVanId(@Param("vanId") Long id);
}
