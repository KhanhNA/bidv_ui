package com.tsolution._2repositories;

import com.tsolution._1entities.ApparamGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.AppParam;

import java.util.Optional;

public interface AppParamRepository extends JpaRepository<AppParam, Long>, JpaSpecificationExecutor<AppParam> {
    Optional<AppParam> findByAppParamCodeAndAndStatus (String code,Integer status);
    Optional<AppParam> findByAppParamCode(String code);
}
