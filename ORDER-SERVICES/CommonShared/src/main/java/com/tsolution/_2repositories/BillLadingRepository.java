package com.tsolution._2repositories;

import com.tsolution._1entities.BillService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.BillLading;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

public interface BillLadingRepository extends JpaRepository<BillLading, Long>, JpaSpecificationExecutor<BillLading> {
    @Query(value = "SELECT COUNT(*) FROM bill_lading bill  WHERE bill.bill_lading_code like LOWER(CONCAT('%',?1))",
            countQuery = "select count(*) from bill_lading",
            nativeQuery = true)
    BigDecimal getOneResult(String param);

    @Query(value = "SELECT * FROM bill_lading bill  WHERE bill.bill_lading_code like LOWER(CONCAT('%',?1,'%'))",
            countQuery = "select count(*) from bill_lading",
            nativeQuery = true)
    Optional<BillLading> getBillLadingByCode(String code);
}