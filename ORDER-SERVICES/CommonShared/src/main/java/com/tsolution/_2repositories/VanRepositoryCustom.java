package com.tsolution._2repositories;

import com.tsolution._1entities.DriverVan;
import com.tsolution._1entities.dto.VanDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface VanRepositoryCustom {
    List<DriverVan> setDriverForVanInDay(Long driver_id, Long van_id);

    Page<VanDto> findByCondition(VanDto searchData, Pageable pageable);
}
