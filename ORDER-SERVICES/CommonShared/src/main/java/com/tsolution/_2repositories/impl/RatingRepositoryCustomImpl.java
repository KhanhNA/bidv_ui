package com.tsolution._2repositories.impl;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution._1entities.Rating;
import com.tsolution._2repositories.RatingRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RatingRepositoryCustomImpl implements RatingRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Rating> findByStaffId(Long driverId) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append(" FROM rating r ");
        sb.append("	WHERE r.status = 1");
        sb.append(" AND r.driver_id = :driverId ");

        Map<String, Object> params = new HashMap<>();
        params.put("driverId", driverId);
        StringBuilder strQuery = new StringBuilder(" SELECT r.* ");
        strQuery.append(sb);

        return BaseRepository.getResultListNativeQuery(this.em,strQuery.toString(),params, Rating.class);
    }
}
