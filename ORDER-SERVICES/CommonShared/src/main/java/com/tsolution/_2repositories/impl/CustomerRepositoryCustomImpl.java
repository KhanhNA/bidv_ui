package com.tsolution._2repositories.impl;

import com.tsolution._1entities.Customer;
import com.tsolution._2repositories.CustomerRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CustomerRepositoryCustomImpl implements CustomerRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Customer> find(Customer customer, Pageable pageable) throws BusinessException {
        StringBuilder query = new StringBuilder();

        query.append("  FROM customer c WHERE");
        query.append("  (:code IS NULL OR LOWER(c.code) LIKE LOWER(CONCAT('%',:code,'%'))) AND ");
        query.append("  (:name IS NULL OR LOWER(c.name) LIKE LOWER(CONCAT('%',:name,'%'))) AND ");
        query.append("	(:address IS NULL OR LOWER(c.address) LIKE LOWER(CONCAT('%',:address,'%'))) AND ");
        query.append("	(:phone IS NULL OR LOWER(c.phone) LIKE LOWER(CONCAT('%',:phone,'%'))) AND ");
        query.append("  (:status IS NULL OR c.status = :status) AND ");
        query.append("  (:isVip IS NULL OR c.is_vip = :isVip) ");
        query.append("  ORDER BY c.code, c.name ");

        Map<String, Object> params = new HashMap<>();
        params.put("code", customer.getCode());
        params.put("name", customer.getName());
        params.put("address", customer.getAddress());
        params.put("phone", customer.getPhone());
        params.put("status", customer.getStatus());
        params.put("isVip", customer.getVip());

        StringBuilder strListQuery = new StringBuilder(" SELECT c.* ");
        strListQuery.append(query);

        StringBuilder strCountQuery = new StringBuilder(" SELECT count(c.id) ");
        strCountQuery.append(query);

        return BaseRepository.getPagedNativeQuery(this.em, strListQuery.toString(), strCountQuery.toString(), params,
                pageable, Customer.class);
    }

}
