package com.tsolution._2repositories;

import com.tsolution._2repositories.RoutingPlanDetailRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.RoutingPlanDetail;

public interface RoutingPlanDetailRepository extends JpaRepository<RoutingPlanDetail, Long>, JpaSpecificationExecutor<RoutingPlanDetail> , RoutingPlanDetailRepositoryCustom {

}