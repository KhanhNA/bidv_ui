package com.tsolution._2repositories;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingPlanDetail;
import com.tsolution.excetions.BusinessException;

import java.util.List;

public interface RoutingPlanDetailRepositoryCustom {
    List<RoutingPlanDetail> getRoutingPlanDetailByRoutingVanAndBillPackage(RoutingPlanDay rpd, BillPackageRouting bpr) throws BusinessException;

}
