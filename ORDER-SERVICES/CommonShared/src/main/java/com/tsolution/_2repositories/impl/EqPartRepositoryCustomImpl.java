package com.tsolution._2repositories.impl;

import com.tsolution._1entities.EqPart;
import com.tsolution._2repositories.EqPartRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EqPartRepositoryCustomImpl implements EqPartRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<EqPart> findByCondition(EqPart searchData, Pageable pageable) throws BusinessException {
        StringBuilder fromClause = new StringBuilder(), whereClause = new StringBuilder(), select = new StringBuilder();
        select.append(" SELECT ep.*, ve.vendor_name vendorName, ap.app_param_name category_name ");

        fromClause.append(" FROM eq_part ep ");
        fromClause.append(" LEFT JOIN vendor ve ON ep.vendor_id = ve.id ");
        fromClause.append(" left join app_param ap on ap.id = ep.category_type ");

        whereClause.append(" WHERE ep.van_id = :vanId ");
        whereClause.append(" AND ( :name is null OR LOWER(ep.name) LIKE LOWER(CONCAT('%', :name ,'%'))) ");
        whereClause.append(" AND ( :vendorId is null OR ep.vendor_id = :vendorId ) ");
        whereClause.append(" AND ( :categoryType is null OR ep.category_type = :categoryType )");

        Map<String, Object> params = buildParams(searchData);

        String searchQuery = select.toString() + fromClause.toString() + whereClause.toString();
        String countQuery = "SELECT count(1) " + fromClause.toString() + whereClause.toString();

        return BaseRepository.getPagedNativeQuery(this.em, searchQuery, countQuery, params, pageable, EqPart.class);
    }

    private Map<String, Object> buildParams(EqPart searchData) {
        Map<String, Object> params = new HashMap<>();
        params.put("vanId", searchData.getVanId());
        params.put("name", searchData.getName());
        params.put("vendorId", searchData.getVendorId());
        params.put("categoryType", searchData.getCategoryType());
        return params;
    }
}
