package com.tsolution._2repositories;

import com.tsolution._1entities.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface VendorRepository extends JpaRepository<Vendor, Long>, JpaSpecificationExecutor<Vendor> {
    Optional<Vendor> findVendorByVendorNameAndContactName(String vendorName,String contactName);
    Optional<Vendor> findVendorById(Long id);
}