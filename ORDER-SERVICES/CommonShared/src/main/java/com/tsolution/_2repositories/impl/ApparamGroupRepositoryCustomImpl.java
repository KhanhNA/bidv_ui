package com.tsolution._2repositories.impl;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution._2repositories.ApparamGroupRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ApparamGroupRepositoryCustomImpl implements ApparamGroupRepositoryCustom {

    @PersistenceContext
    private EntityManager em;


    @Override
    public Page<ApparamGroup> find(ApparamGroup entity, Pageable pageable) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append(" FROM apparam_group ap ");
        sb.append("	WHERE (:status is null OR ap.status = :status) AND ");
        sb.append("		  (:groupCode is null OR LOWER(ap.group_code) like LOWER(CONCAT('%',:groupCode,'%'))) AND ");
        sb.append("		  (:groupName is null OR LOWER(ap.group_name) like LOWER(CONCAT('%',:groupName,'%'))) ");
        sb.append(" ORDER BY ap.group_code, ap.group_name, ap.ord  ");

        Map<String, Object> params = new HashMap<>();
        params.put("groupCode", entity.getGroupCode());
        params.put("groupName", entity.getGroupName());
        params.put("status", entity.getStatus());


        StringBuilder strQuery = new StringBuilder(" SELECT ap.* ");
        strQuery.append(sb);

        StringBuilder strCountQuery = new StringBuilder();
        strCountQuery.append(" SELECT COUNT(DISTINCT ap.id) ");
        strCountQuery.append(sb);

       // return BaseRepository.getResultListNativeQuery(this.em,strQuery.toString(),params, ApparamGroup.class);
        return BaseRepository.getPagedNativeQuery(this.em,strQuery.toString(),strCountQuery.toString(),params,pageable,ApparamGroup.class);
    }
}
