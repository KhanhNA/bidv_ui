package com.tsolution._2repositories;

import java.sql.Date;
import java.util.List;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution._1entities.Warehouse;
//import com.tsolution._1entities.dto.FreeVolumeDto;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface WarehouseRepositoryCustom {
//    List<FreeVolumeDto> calculateFreeVolumeWarehouse(Date fromDate, Date toDate, List<Long> warehouseIds,
//                                                     List<Integer> customerRentStatus) throws BusinessException;

    List<Warehouse> getListWarehouseByCustomer(Long customerId) throws BusinessException;

    public List<ApparamValue> getListProductType() throws BusinessException;

    Page<Warehouse> findListWareHouse(Integer customerId,String code, String name, Integer status, Pageable pageable)throws BusinessException;
        //Warehouse findWarehouseByCode(String code) throws BusinessException;
}
