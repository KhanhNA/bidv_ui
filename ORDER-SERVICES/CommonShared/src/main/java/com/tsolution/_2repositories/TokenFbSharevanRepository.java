package com.tsolution._2repositories;

import com.tsolution._1entities.TokenFbSharevan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TokenFbSharevanRepository extends JpaRepository<TokenFbSharevan, Long>, JpaSpecificationExecutor<TokenFbSharevan> {

    @Query("SELECT u FROM TokenFbSharevan u WHERE u.objectId = ?1 and u.objectType =?2")
    Optional<TokenFbSharevan> findByObjectId(Long id, Integer objectType);

    List<TokenFbSharevan> findByObjectType(Integer objectType);

    TokenFbSharevan findByImei(String imei);
}