package com.tsolution._2repositories;

import com.tsolution._1entities.BillLading;
import com.tsolution._1entities.Organization;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BillLadingRepositoryCustom {
    List<BillLading> findByEntity(BillLading entity) throws BusinessException;
    public Page<BillLading> findByCustomerIdAndDate(String billLadingCode,Integer status, Long customerId, LocalDateTime fromDate, LocalDateTime toDate,Integer billCycleType, Pageable pageable) throws BusinessException;
}
