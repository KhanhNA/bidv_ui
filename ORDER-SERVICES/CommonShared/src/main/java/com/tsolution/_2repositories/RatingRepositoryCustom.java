package com.tsolution._2repositories;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution._1entities.Rating;
import com.tsolution.excetions.BusinessException;

import java.util.List;

public interface RatingRepositoryCustom {
    List<Rating> findByStaffId(Long staffId) throws BusinessException;

}
