package com.tsolution._2repositories;

import com.tsolution._1entities.DriverVan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverVanRepository extends JpaRepository<DriverVan, Long>, JpaSpecificationExecutor<DriverVan>, DriverVanRepositoryCustom {

}