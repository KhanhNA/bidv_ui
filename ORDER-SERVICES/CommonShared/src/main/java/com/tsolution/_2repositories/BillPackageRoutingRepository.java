package com.tsolution._2repositories;

import com.tsolution._2repositories.BillPackageRoutingRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.BillPackageRouting;

public interface BillPackageRoutingRepository extends JpaRepository<BillPackageRouting, Long>, JpaSpecificationExecutor<BillPackageRouting>, BillPackageRoutingRepositoryCustom {

}