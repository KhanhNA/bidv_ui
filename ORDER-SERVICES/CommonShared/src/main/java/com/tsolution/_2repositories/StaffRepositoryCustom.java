package com.tsolution._2repositories;

import com.tsolution._1entities.Staff;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StaffRepositoryCustom {
    Page<Staff> findStaff(Integer feetId,String userName,String fullName,String objType,String email,String phoneNumber,Integer status, Pageable pageable) throws BusinessException;
}
