package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.Fleet;

import java.util.Optional;

public interface FleetRepository extends JpaRepository<Fleet, Long>, JpaSpecificationExecutor<Fleet> {
    Optional<Fleet> findByFleetCode (String code);
}