package com.tsolution._2repositories;

import com.tsolution._1entities.Van;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface VanRepository extends JpaRepository<Van, Long>, JpaSpecificationExecutor<Van>, VanRepositoryCustom {
    Optional<Van> findByLicencePlate(String licencePlate);
    Optional<Van> findByVehicleRegistration(String vehicleRegistration);
    @Override
    @Query(value = "Select * from van where status_car = 1",
            countQuery = "select count(*) from van",
            nativeQuery = true)
    List<Van> findAll();
}