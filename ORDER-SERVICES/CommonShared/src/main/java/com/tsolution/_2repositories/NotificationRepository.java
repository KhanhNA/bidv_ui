package com.tsolution._2repositories;

import com.tsolution._1entities.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {

    @Query("SELECT u FROM Notification u WHERE u.objectId = ?1 and u.objectType = ?2")
    List<Notification> findByObjectIdAndObjectType(Long objectId, Integer objectType);
}