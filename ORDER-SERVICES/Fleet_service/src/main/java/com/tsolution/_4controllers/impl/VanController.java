package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Van;
import com.tsolution._1entities.dto.VanDto;
import com.tsolution._3services.VanService;
import com.tsolution._3services.impl.BaseExcelService;
import com.tsolution._3services.impl.FileStorageService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@RestController
@RequestMapping("/vans")
public class VanController extends BaseController {

    @Autowired
    private VanService vanService;
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/vans/all')")
    @ApiOperation(httpMethod = "GET",
            value = "Finds all vans with status = 1(RUNNING)",
            notes = "No parameter required",
            response = Van.class, responseContainer = "List")
    public ResponseEntity<Object> findAll() {
        return this.vanService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/vans/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Find van with provided id",
            notes = "Required 1 Long parameter",
            response = Van.class, responseContainer = "List",
            nickname = "findVanById")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.vanService.findById(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('get/vans')")
    @ApiOperation(httpMethod = "GET",
            value = "Can take multiple value and search base on that, return 1 page of result at a time",
            response = Van.class, responseContainer = "List",
            nickname = "findVanByCondition")
    public ResponseEntity<Object> findByCondition(VanDto searchData, Integer pageNumber, Integer pageSize) throws BusinessException {
        return this.vanService.findByCondition(searchData, PageRequest.of(pageNumber-1, pageSize));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/vans')")
    @ApiOperation(httpMethod = "POST",
            value = "Create a new van",
            notes = "Required 1 Van entity, no Id and Licence plate must be new",
            response = Van.class, responseContainer = "List",
            nickname = "createVan")
    public ResponseEntity<Object> create(@RequestBody Van entity) throws BusinessException {
        return this.vanService.create(entity);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('patch/vans')")
    @ApiOperation(httpMethod = "PATCH",
            value = "Update van",
            notes = "Can include images, removedImages, attachment, removedAttachment but not required." +
                    " Licence plate must be new or unchanged",
            response = Van.class, responseContainer = "List",
            nickname = "updateVan")
    public ResponseEntity<Object> update(@RequestBody Van entity) throws BusinessException {
        return this.vanService.update(entity);
    }

    @PatchMapping("/deactivate/{id}")
    @PreAuthorize("hasAuthority('patch/vans/deactivate/{id}')")
    @ApiOperation(httpMethod = "PATCH",
            value = "Change status to stopped")
    public ResponseEntity<Object> deactivate(@PathVariable("id") Long id) throws BusinessException{
        return this.vanService.deactivate(id);
    }

    @PatchMapping("/activate/{id}")
    @PreAuthorize("hasAuthority('patch/vans/activate/{id}')")
    @ApiOperation(httpMethod = "PATCH",
            value = "Change status to running")
    public ResponseEntity<Object> activate(@PathVariable("id") Long id) throws BusinessException{
        return this.vanService.activate(id);
    }

    @PostMapping("/import")
//    @PreAuthorize("hasAuthority('post/vans/import')")
    @ApiOperation(httpMethod = "POST",
            value = "Import data to database",
            notes = "need to be the right format")
    public ResponseEntity<Object> importExcel(MultipartFile file) throws BusinessException{
        return this.vanService.importExcel(file);
    }


    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName) throws Exception {
        Resource resource = this.vanService.getExcelTemplate(fileName);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
    }
}
