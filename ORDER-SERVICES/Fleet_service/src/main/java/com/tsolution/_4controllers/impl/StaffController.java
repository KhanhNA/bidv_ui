package com.tsolution._4controllers.impl;

import com.tsolution._1entities.DriverVan;
import com.tsolution._1entities.Staff;
import com.tsolution._3services.DriverVanService;
import com.tsolution._3services.StaffService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/staff")
public class StaffController extends BaseController {
    @Autowired
    private StaffService staffService;
    @Autowired
    private DriverVanService driverVanService;


    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll() {
        return staffService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Staff> findById(
                                          @PathVariable("id") Long id) throws BusinessException {
        return staffService.findById( id);
    }

    @PostMapping("/insert")
    public ResponseEntity<Object> create(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Staff staff) throws BusinessException {
        return staffService.create(authorization, acceptLanguage, staff);
    }

    @PatchMapping("/update/{id}")
    public ResponseEntity<Object> update(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Staff staff) throws BusinessException {
        return staffService.update(authorization, acceptLanguage, staff);
    }

    @PostMapping("/active")
    public ResponseEntity<Object> active(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Staff staff)
            throws BusinessException {
        return this.staffService.activeOrDeActivate(authorization, acceptLanguage, staff, true);
    }

    @PostMapping("/deactivate")
    public ResponseEntity<Object> deactivate(@RequestHeader("Authorization") String authorization,
                                             @RequestHeader("Accept-Language") String acceptLanguage,
                                             @RequestBody Staff staff)
            throws BusinessException {
        return this.staffService.activeOrDeActivate(authorization, acceptLanguage, staff, false);
    }

    @PostMapping("/reset-pass")
    public ResponseEntity<Object> resetPassword(@RequestHeader("Authorization") String authorization,
                                                @RequestHeader("Accept-Language") String acceptLanguage,
                                                @RequestBody Staff staff) throws BusinessException {
        return this.staffService.resetPassword(authorization, acceptLanguage, staff);
    }


    @GetMapping("/available")
    public ResponseEntity<Object> findStaffAvailable(@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fdate,
                                                     @RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime tdate) throws BusinessException {
        return this.staffService.findAllStaffAvailable(fdate, tdate);
    }

    @GetMapping("/van_day")
    public ResponseEntity<Object> getVanForDriverByDate(@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime date,
                                                        @RequestParam(required = false) Long driverId) throws BusinessException {
        List<DriverVan> lstDriverVan = this.driverVanService.getVanForDriverByDate(driverId, date);
        if (lstDriverVan.isEmpty()) {
            return new ResponseEntity<>(lstDriverVan, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lstDriverVan, HttpStatus.OK);
    }

    @GetMapping("/findByUsername")
    public ResponseEntity<Staff> findByUsername(@RequestHeader("Authorization") String authorization,
                                                @RequestHeader("Accept-Language") String acceptLanguage,
                                                @RequestHeader("username") String username) throws BusinessException {
        return this.staffService.findByUserName(authorization,acceptLanguage,username);
    }

    @GetMapping("/findStaff")
    public ResponseEntity<Object> findStaff(

                                            @RequestParam("fleetId") Integer fleetId,
                                            @RequestParam(required = false,value="userName") String userName,
                                            @RequestParam(required = false,value="fullName") String fullName,
                                            @RequestParam(required = false,value="objectType") String objType,
                                            @RequestParam(required = false,value="email") String email,
                                            @RequestParam(required = false,value="phone") String phone,
                                            @RequestParam(required = false,value="status") Integer status,
                                            @RequestParam("pageNumber") Integer pageNumber,
                                            @RequestParam("pageSize") Integer pageSize
                                           ) throws BusinessException {
        return this.staffService.findStaff( fleetId, userName, fullName, objType,email,phone,status, pageNumber, pageSize);
    }
    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName) throws BusinessException, IOException {
        Resource resource = this.staffService.getExcelTemplate(fileName);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/import")
    public ResponseEntity<Object> importExcel(MultipartFile file) throws BusinessException, IOException, InvalidFormatException {
        return this.staffService.updateStaffByExcel(file);
    }
}
