package com.tsolution._4controllers.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._3services.impl.BaseExcelService;
import com.tsolution.utils.Translator;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.tsolution._3services.impl.FileStorageService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/files")
public class FileController {
	@Autowired
	private FileStorageService fileStorageService;

    @Autowired
	private  BaseExcelService baseExcelService;

	@Autowired
	protected Translator translator;

	@GetMapping("/get-urls")
	public ResponseEntity<Object> getUrls() {
		List<String> urls = new ArrayList<>();
		urls.add("/files/T7_2019.png");
		return new ResponseEntity<>(urls, HttpStatus.OK);
	}

	@GetMapping("/**")
	@PreAuthorize("hasAuthority('get/files/**')")
	@ApiOperation(httpMethod = "GET",
			value = "Get file from input Url",
			notes = "Input url in Path, e.g: '/files/claim-images/D.PNG'. Return file if exist",
			response = Resource.class, responseContainer = "List")
	public ResponseEntity<Object> get(HttpServletRequest request) throws BusinessException, IOException {
		String relativeUrl = this.extractRelativeUrl(request);
		Resource resource = this.fileStorageService.loadFileAsResource(relativeUrl);
		HttpHeaders httpHeaders = this.fileStorageService.loadHttpHeaders(resource);
		return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
	}

	@PostMapping("/upload")
	@PreAuthorize("hasAuthority('post/files/upload')")
	@ApiOperation(httpMethod = "POST",
			value = "Upload file",
			notes = "Input: file to upload and objType in OBJECT_ATTACHMENT. Response return url after upload",
			response = String.class, responseContainer = "List")
	public ResponseEntity<Object> upload(@RequestPart("file") MultipartFile multipartFile,
										 @RequestPart("objType") String objType) throws BusinessException {
		String result, relativePath = "";
		for(OBJECT_ATTACHMENT st: OBJECT_ATTACHMENT.values()){
			if(st.getValue().equalsIgnoreCase(objType)){
				relativePath = st.getValue()+"-attachment";
				break;
			}
		}
		if(StringUtils.isBlank(relativePath))	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		result = this.fileStorageService.saveFile(relativePath.toLowerCase(), multipartFile);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping("/uploadMultiple")
	@PreAuthorize("hasAuthority('post/files/uploadMultiple')")
	@ApiOperation(httpMethod = "POST",
			value = "Upload multiple files",
			notes = "Input: list of files to upload and objType in OBJECT_ATTACHMENT Response return list of urls after upload",
			response = String.class, responseContainer = "List")
	public ResponseEntity<Object> uploadMultiple(@RequestPart("listFile") List<MultipartFile> listFiles,
										 @RequestPart("objType") String objType) throws BusinessException {
		String result, relativePath = "";
		for(OBJECT_ATTACHMENT st: OBJECT_ATTACHMENT.values()){
			if(st.getValue().equalsIgnoreCase(objType)){
				relativePath = st.getValue()+"-attachment";
				relativePath = relativePath.toLowerCase();
				break;
			}
		}
		if(StringUtils.isBlank(relativePath))	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		List<String> listResult = new ArrayList<>();
		for(MultipartFile file: listFiles){
			result = this.fileStorageService.saveFile(relativePath, file);
			listResult.add(result);
		}
		return new ResponseEntity<>(listResult, HttpStatus.OK);
	}

	private String extractRelativeUrl(HttpServletRequest request) {
		String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE); // /files/relativeUrl
		String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE); // /files/**
		return new AntPathMatcher().extractPathWithinPattern(bestMatchPattern, path); // relativeUrl
	}
	/*@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) throws BusinessException, IOException {
		///String outFileName=this.baseExcelService.updateExcelTemplate(fileName);
		Resource resource = this.fileStorageService.loadFileAsResource("template/"+outFileName);
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			throw new BusinessException(this.translator.toLocale("file.not.found"));
		}
		if(contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}*/
}
