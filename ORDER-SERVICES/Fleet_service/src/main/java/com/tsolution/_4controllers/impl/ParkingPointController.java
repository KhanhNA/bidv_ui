package com.tsolution._4controllers.impl;

import com.tsolution._1entities.ParkingPoint;
import com.tsolution._3services.ParkingPointService;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/parking-point")
public class ParkingPointController {

    @Autowired
    ParkingPointService parkingPointService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/parking-point/all')")
    @ApiOperation(httpMethod = "GET",
            value = "Get all parking point",
            notes = "No parameter required",
            response = ParkingPoint.class, responseContainer = "List")
    public ResponseEntity<Object> findAll() {
        return this.parkingPointService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/parking-point/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Get Parking point with input Id",
            response = ParkingPoint.class, responseContainer = "List")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.parkingPointService.findById(id);
    }

    @GetMapping("/fleet/{id}")
    @PreAuthorize("hasAuthority('get/parking-point/fleet/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Get parking point with the input vanId",
            response = ParkingPoint.class, responseContainer = "List")
    public ResponseEntity<Object> findByFleetId(@PathVariable("id") Long id) throws BusinessException {
        return this.parkingPointService.findByFleetId(id);
    }

    @GetMapping("/fleet")
    @PreAuthorize("hasAuthority('get/parking-point/fleet/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Get parking point with the input vanId",
            response = ParkingPoint.class, responseContainer = "List")
    public ResponseEntity<Object> findByToken() throws BusinessException {
        return this.parkingPointService.findByToken();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/parking-point')")
    @ApiOperation(httpMethod = "POST",
            value = "Create new Parking point",
            notes = "Van id and fleet id must ve valid, and van must be part of fleet, address is not blank",
            response = ParkingPoint.class, responseContainer = "ParkingPoint")
    public ResponseEntity<Object> create(@RequestBody ParkingPoint entity) throws BusinessException {
        return this.parkingPointService.create(entity);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('patch/parking-point')")
    @ApiOperation(httpMethod = "PATCH",
            value = "update Parking point",
            notes = "Van id and fleet id must ve valid, and van must be part of fleet, address is not blank",
            response = ParkingPoint.class, responseContainer = "ParkingPoint")
    public ResponseEntity<Object> update(@RequestBody ParkingPoint entity) throws BusinessException {
        return this.parkingPointService.update(entity);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('delete/parking-point/{id}')")
    @ApiOperation(httpMethod = "DELETE",
            value = "Delete parking point",
            notes = "Update status value to DELETE(-1)",
            response = HttpStatus.class, responseContainer = "HttpStatus")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) throws BusinessException {
        return this.parkingPointService.delete(id);
    }
}
