package com.tsolution._4controllers.impl;


import com.tsolution._3services.RatingService;
import com.tsolution._4controllers.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/rating")
public class RatingController extends BaseController {

    @Autowired
    RatingService ratingService;

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/rating/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Get all rating of driver",
            notes = "Input is driver ID",
            response = Map.class, responseContainer = "Map")
    public ResponseEntity<Object> findByDriverId(@PathVariable("id") Long id) {
        return this.ratingService.findByDriverId(id);
    }

    @GetMapping("/get100/{id}")
    @PreAuthorize("hasAuthority('get/rating/get100/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Get latest 100 rating of driver",
            notes = "Input is driver ID",
            response = Map.class, responseContainer = "Map")
    public ResponseEntity<Object> find100LatestOfDriver(@PathVariable("id") Long id) {
        return this.ratingService.find100LatestOfDriver(id);
    }
}
