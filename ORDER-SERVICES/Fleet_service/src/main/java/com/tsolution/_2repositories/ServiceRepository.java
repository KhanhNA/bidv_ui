package com.tsolution._2repositories;

import com.tsolution._1entities.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface ServiceRepository extends JpaRepository<Service, Long>, JpaSpecificationExecutor<Service> {

    @Query(value = "Select * from service where service_type =?1",
            countQuery = "select count(*) from service",
            nativeQuery = true)
    Collection<Service> findServiceByType(int type);
}