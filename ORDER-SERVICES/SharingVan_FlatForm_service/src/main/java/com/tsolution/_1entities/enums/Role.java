package com.tsolution._1entities.enums;

public enum Role {
	ADMIN("ADMIN"), STORE_KEEPER("STORE_KEEPER"), MERCHANT("MERCHANT"), GOD("GOD");

	private String value;

	Role(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
