package com.tsolution._1entities.enums;

public enum WarehouseStatus {
	NOT_ACTIVED(0), ACTIVED(1), DEACTIVED(2);

	private Integer value;

	WarehouseStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
