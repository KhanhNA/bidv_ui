package com.tsolution._1entities.enums;

public enum ObjAttachmentType {
    CLAIMREPORT("CLAIMREPORT"),
    STAFF("STAFF"),
    WAREHOUSE("WAREHOUSE"),
    EQUIPMENT("EQUIPMENT"),
    PARCEL("PARCEL"),
    CUSTOMER("CUSTOMER"),
    VAN("VAN"),
    EMPLOYEE("EMPLOYEE");

    private String value;

    ObjAttachmentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
