package com.tsolution._1entities.enums;

public enum ImportStatementStatus {
	NEW(0), ACTUALLY_IMPORTED(1), CANCEL(2);

	private Integer value;

	ImportStatementStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
