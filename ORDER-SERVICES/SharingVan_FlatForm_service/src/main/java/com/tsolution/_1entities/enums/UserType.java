package com.tsolution._1entities.enums;

public enum UserType {
	ADMIN("ADMIN"), FAMILY("FAMILY"), ORGANIZATION("ORGANIZATION"), STORE_KEEPER("STORE_KEEPER");

	private String value;

	UserType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
