package com.tsolution._2repositories.sql;

public class CustomerWarehouseVolumeSql {
	private CustomerWarehouseVolumeSql() {
	}

	public static final String NEARLY_TO_EXPIRE_OVER_QUOTE_WAREHOUSE = " FROM customer_warehouse_volume cwv "
			+ "      JOIN warehouse w ON w.id = cwv.warehouse_id AND (:organizationId is null OR w.organization_id = :organizationId) "
			+ " where cwv.free_volume <= 0 AND DATE(SYSDATE()) <= cwv.day AND cwv.day < DATE(SYSDATE() + INTERVAL (:numberOfDays + 1) DAY) "
			+ " 	  AND (:customerId is null OR cwv.customer_id = :customerId) ";
}
