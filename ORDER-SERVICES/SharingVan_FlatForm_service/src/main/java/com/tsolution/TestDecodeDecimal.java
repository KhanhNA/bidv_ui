package com.tsolution;

import io.swagger.models.auth.In;
import org.aspectj.lang.annotation.Before;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestDecodeDecimal {
    public static void main(String[] args) {
        /*List<Integer> list = new ArrayList<Integer>();
        for(int i = 1; i< 10; i++){
            list.add(i);
        }

        Stream<Integer> stream = list.stream();
        List<Integer> evt = stream.filter(i -> i%2==0).collect(Collectors.toList());
        System.out.println(evt);*/

        List<String> memberNames = new ArrayList<>();
        memberNames.add("Amitabh");
        memberNames.add("AShekhar");
        memberNames.add("AAman");
        memberNames.add("ARahul");
        memberNames.add("AShahrukh");
        memberNames.add("ASalman");
        memberNames.add("AYana");
        memberNames.add("ALokesh");
        /*List<String> memberNames2 = memberNames.stream().filter(s->s.startsWith("A")).map(s ->s.replace("A", "X")).collect(Collectors.toList());
        List<String> memberNames3 = memberNames.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(memberNames3);*/

        /*boolean matchedResult = memberNames.stream().allMatch(s -> s.startsWith("A"));
        System.out.println(matchedResult);*/

       /* Optional<String> reduced = memberNames.stream().reduce((s,s1) ->s.toUpperCase() +"$"+ s1.toLowerCase());
        reduced.ifPresent(System.out::println);*/
        List<String> pointList = new ArrayList();
        pointList.add("1");
        pointList.add("2");

        List<Integer> lstNumber = Arrays.asList(1,2,3,4,5,6,7,8,9);
       Optional<Integer> xx=  lstNumber.stream().reduce(Math::max);
        System.out.println(xx);

    }
}
