package com.sample;


public class SolutionDay {

  private long id;
  private java.sql.Date datePlan;
  private String groupCode;
  private long hardScore;
  private long softScore;
  private java.sql.Timestamp solveTime;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public java.sql.Date getDatePlan() {
    return datePlan;
  }

  public void setDatePlan(java.sql.Date datePlan) {
    this.datePlan = datePlan;
  }


  public String getGroupCode() {
    return groupCode;
  }

  public void setGroupCode(String groupCode) {
    this.groupCode = groupCode;
  }


  public long getHardScore() {
    return hardScore;
  }

  public void setHardScore(long hardScore) {
    this.hardScore = hardScore;
  }


  public long getSoftScore() {
    return softScore;
  }

  public void setSoftScore(long softScore) {
    this.softScore = softScore;
  }


  public java.sql.Timestamp getSolveTime() {
    return solveTime;
  }

  public void setSolveTime(java.sql.Timestamp solveTime) {
    this.solveTime = solveTime;
  }

}
