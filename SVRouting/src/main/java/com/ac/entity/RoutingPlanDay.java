package com.ac.entity;

import com.ac.Constants;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "sharevan_routing_plan_day")
public class RoutingPlanDay {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "routing_Plan_Day_Code")
  private String routingPlanDayCode;

  @Column(name = "date_Plan")
  private Date datePlan;

  @Column(name = "vehicle_Id")
  private Integer vehicleId;

  @Column(name = "driver_Id")
  private Integer driverId;

  @Column(name = "latitude")
  private Double latitude;

  @Column(name = "longitude")
  private Double longitude;

  @Column(name = "order_Number")
  private Integer orderNumber;

  @Column(name = "status")
  private String status;

  @Column(name = "capacity_Expected")
  private Double capacityExpected;

  @Column(name = "expected_From_Time")
  private java.sql.Timestamp expectedFromTime;

  @Column(name = "expected_To_Time")
  private java.sql.Timestamp expectedToTime;

  @Column(name = "actual_Time")
  private java.sql.Timestamp actualTime;

  @Column(name = "warehouse_Id")
  private Integer warehouseId;

  @Column(name = "zone_Area_Id")
  private Integer zoneAreaId;


  @Column(name = "address")
  private String address;

  @Column(name = "bill_lading_detail_id")
  private Integer billLadingDetailId;

  @Column(name = "bill_lading_detail_code")
  private String billLadingDetailCode;

  @Column(name = "from_routing_plan_day_id")
  private Integer fromRoutingPlanDayId;

  @Column(name = "type")
  private String type;

  @Column(name = "ship_type")
  private String shipType; //refer to ShipType

  @Column(name = "depot_id")
  private Integer depotId;

  @Column(name = "next_id")
  private String nextId;

  @Column(name = "previous_id")
  private String previousId;

  @Column(name = "stock_Man_Id")
  private Integer stockManId;

  @Column(name = "company_id")
  private Integer companyId;

  @Column(name = "create_Uid")
  private Integer createUid;

  @Column(name = "create_Date")
  private java.sql.Timestamp createDate;

  @Column(name = "write_Uid")
  private Integer writeUid;

  @Column(name = "write_Date")
  private java.sql.Timestamp writeDate;

  @Column(name = "solution_day_id")
  private Integer solutionDayId;

  @Column(name= "qr_so")
  private String qrSO;

  @Column(name="so_type")
  private Boolean soType;

  @Column(name = "assess_amount")
  private Double assessAmount;

  @Column(name="insurance_id")
  private Integer insuranceId;

  @Column(name="phone")
  private String phone;

  @Column(name = "partner_id")
  private Integer partnerId;

  @Column(name = "check_point")
  private Boolean checkPoint;

  @Column(name = "warehouse_name")
  private String warehouseName;

  @Column(name = "num_receive")
  private Integer numReceice;

  @Column(name = "total_volume")
  private Double totalVolume;

  @Column(name="packaged_cargo")
  private String packagedIntoCargo;

  @Column(name = "capacity_vehicle")
  private Double capacityVehicle;

  @Column(name = "qr_gen_check")
  private Boolean qrGenCheck;

  @Column(name = "driver_sos_id")
  private Integer driverSosId;

  @Column(name = "bill_routing_id")
  private Integer billRoutingId;

  @Column(name = "trouble_type")
  private String troubleType;

  @Column(name = "total_package")
  private Integer totalPackage;

  @Column(name = "toogle")
  private Boolean toogle;

  @Column(name = "capacity_actual")
  private Double capacityActual;

  @Override
  public String toString() {
    return "RoutingPlanDay{" +
            "id=" + id +
            ", routingPlanDayCode='" + routingPlanDayCode + '\'' +
            ", datePlan=" + datePlan +
            ", vehicleId=" + vehicleId +
            ", driverId=" + driverId +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            ", orderNumber=" + orderNumber +
            ", status='" + status + '\'' +
            ", capacityExpected=" + capacityExpected +
            ", expectedFromTime=" + expectedFromTime +
            ", expectedToTime=" + expectedToTime +
            ", actualTime=" + actualTime +
            ", warehouseId=" + warehouseId +
            ", zoneAreaId=" + zoneAreaId +
            ", address='" + address + '\'' +
            ", billLadingDetailId=" + billLadingDetailId +
            ", billLadingDetailCode='" + billLadingDetailCode + '\'' +
            ", fromRoutingPlanDayId=" + fromRoutingPlanDayId +
            ", type='" + type + '\'' +
            ", shipType='" + shipType + '\'' +
            ", depotId=" + depotId +
            ", nextId='" + nextId + '\'' +
            ", previousId='" + previousId + '\'' +
            ", stockManId=" + stockManId +
            ", companyId=" + companyId +
            ", createUid=" + createUid +
            ", createDate=" + createDate +
            ", writeUid=" + writeUid +
            ", writeDate=" + writeDate +
            ", solutionDayId=" + solutionDayId +
            ", qrSO='" + qrSO + '\'' +
            ", soType=" + soType +
            ", assessAmount=" + assessAmount +
            ", insuranceId=" + insuranceId +
            ", phone='" + phone + '\'' +
            ", partnerId=" + partnerId +
            ", checkPoint=" + checkPoint +
            ", warehouseName='" + warehouseName + '\'' +
            ", numReceice=" + numReceice +
            ", totalVolume=" + totalVolume +
            ", packagedIntoCargo='" + packagedIntoCargo + '\'' +
            ", capacityVehicle=" + capacityVehicle +
            ", qrGenCheck=" + qrGenCheck +
            ", driverSosId=" + driverSosId +
            '}';
  }

  public RoutingPlanDay() {
  }

  public RoutingPlanDay(RoutingPlanDay other) {
  //  this.id = other.id;
    this.routingPlanDayCode = other.routingPlanDayCode;
    this.datePlan = other.datePlan;
    this.vehicleId = other.vehicleId;
    this.driverId = other.driverId;
    this.latitude = other.latitude;
    this.longitude = other.longitude;
    this.orderNumber = other.orderNumber;
    this.status = other.status;
    this.capacityExpected = other.capacityExpected;
    this.expectedFromTime = other.expectedFromTime;
    this.expectedToTime = other.expectedToTime;
    this.actualTime = other.actualTime;
    this.warehouseId = other.warehouseId;
    this.zoneAreaId = other.zoneAreaId;
    this.address = other.address;
    this.billLadingDetailId = other.billLadingDetailId;
    this.billLadingDetailCode = other.billLadingDetailCode;
    this.fromRoutingPlanDayId = other.fromRoutingPlanDayId;
    this.type = other.type;
    this.shipType = other.shipType;
    this.depotId = other.depotId;
    this.nextId = other.nextId;
    this.previousId = other.previousId;
    this.stockManId = other.stockManId;
    this.companyId = other.companyId;
    this.createUid = other.createUid;
    this.createDate = Constants.getNowUTC();
    this.writeUid = other.writeUid;
    this.writeDate = Constants.getNowUTC();
    this.solutionDayId = other.solutionDayId;
    this.qrSO = other.qrSO;
    this.soType = other.soType;
    this.assessAmount = other.assessAmount;
    this.insuranceId = other.insuranceId;
    this.phone = other.phone;
    this.partnerId = other.partnerId;
    this.checkPoint = other.checkPoint;
    this.warehouseName = other.warehouseName;
    this.numReceice = other.numReceice;
    this.totalVolume = other.totalVolume;
    this.packagedIntoCargo = other.packagedIntoCargo;
    this.capacityVehicle = other.capacityVehicle;
    this.qrGenCheck = other.qrGenCheck;
    this.driverSosId = other.driverSosId;
    this.billRoutingId = other.billRoutingId;
    this.troubleType = other.troubleType;
    this.toogle = other.toogle;
    this.capacityActual = other.capacityActual;
  }
}
