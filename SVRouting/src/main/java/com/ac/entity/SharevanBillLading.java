package com.ac.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * bill lading
 */
@Entity
@Getter
@Setter
@Table(name = "sharevan_bill_lading")
public class SharevanBillLading implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    /**
     * Bill lading Code
     */
    @Column(name = "name_seq", nullable = false)
    private String nameSeq;

    /**
     * Insurance
     */
    @Column(name = "insurance_id")
    private Integer insuranceId;

    /**
     * Total weight
     */
    @Column(name = "total_weight")
    private Double totalWeight;

    /**
     * Total amount
     */
    @Column(name = "total_amount")
    private Double totalAmount;

    /**
     * Tolls
     */
    @Column(name = "tolls")
    private Double tolls;

    /**
     * Surcharge
     */
    @Column(name = "surcharge")
    private Double surcharge;

    /**
     * Total volume
     */
    @Column(name = "total_volume")
    private Double totalVolume;

    /**
     * VAT(%)
     */
    @Column(name = "vat")
    private Double vat;

    /**
     * Promotion code
     */
    @Column(name = "promotion_code")
    private Double promotionCode;

    /**
     * Release type
     */
    @Column(name = "release_type")
    private Integer releaseType;

    /**
     * Total parcel
     */
    @Column(name = "total_parcel")
    private Integer totalParcel;

    /**
     * Company
     */
    @Column(name = "company_id")
    private Integer companyId;

    /**
     * Start Date
     */
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    /**
     * End Date
     */
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    /**
     * Subscribe
     */
    @Column(name = "subscribe_id")
    private Integer subscribeId;

    /**
     * Frequency
     */
    @Column(name = "frequency")
    private Integer frequency;

    /**
     * QR Code
     */
    @Column(name = "qr_code")
    private String qrCode;

    /**
     * Sbl type
     */
    @Column(name = "sbl_type")
    private String sblType;

    /**
     * Day Of Week
     */
    @Column(name = "day_of_week")
    private Integer dayOfWeek;

    /**
     * Day In Month
     */
    @Column(name = "day_of_month")
    private Integer dayOfMonth;

    /**
     * Status
     */
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * Description
     */
    @Column(name = "description")
    private String description;

    /**
     * Bill Lading Reference
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * From bill lading
     */
    @Column(name = "from_bill_lading_id")
    private Integer fromBillLadingId;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private java.sql.Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private java.sql.Timestamp writeDate;

    /**
     * Order package
     */
    @Column(name = "order_package_id", nullable = false)
    private Integer orderPackageId;

    /**
     * Award company
     */
    @Column(name = "award_company_id")
    private Integer awardCompanyId;

    @Column(name = "last_scan")
    private Timestamp lastScan;

    @Column(name = "order_num")
    private Integer orderNum;

    @Column(name = "inzone_num")
    private Integer inzoneNum;

    @Column(name = "outzone_num")
    private Integer outzoneNum;


}
