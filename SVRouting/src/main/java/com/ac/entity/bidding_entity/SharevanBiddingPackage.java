package com.ac.entity.bidding_entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * Bidding package
 */
@Table(name = "sharevan_bidding_package")
@Entity
@Getter
@Setter
public class SharevanBiddingPackage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * Bidding order
     */
    @Column(name = "bidding_order_id")
    private Integer bidding_order_id;

    /**
     * Bidding package number
     */
    @Column(name = "bidding_package_number")
    private String bidding_package_number;

    /**
     * Status bidding
     */
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * Confirm time
     */
    @Column(name = "confirm_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp confirm_time;

    /**
     * Release time
     */
    @Column(name = "release_time", nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp release_time;

    /**
     * Bidding time
     */
    @Column(name = "bidding_time", nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp bidding_time;

    /**
     * Max count
     */
    @Column(name = "max_count", nullable = false)
    private Integer max_count;

    /**
     * From Depot
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "from_depot_id", nullable = false)
    private SharevanDepot from_depot;

    /**
     * To Depot
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "to_depot_id", nullable = false)
    private SharevanDepot to_depot;

    /**
     * Total weight
     */
    @Column(name = "total_weight")
    private Float total_weight;

    /**
     * Distance
     */
    @Column(name = "distance", nullable = false)
    private Float distance;

    /**
     * From latitude
     */
    @Column(name = "from_latitude")
    private Float from_latitude;

    /**
     * To latitude
     */
    @Column(name = "to_latitude")
    private Float to_latitude;

    /**
     * Pick up from time
     */
    @Column(name = "from_receive_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp from_receive_time;

    /**
     * Pick up to time
     */
    @Column(name = "to_receive_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp to_receive_time;

    /**
     * Drop from time
     */
    @Column(name = "from_return_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp from_return_time;

    /**
     * Drop to time
     */
    @Column(name = "to_return_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp to_return_time;

    /**
     * Price origin
     */
    @Column(name = "price_origin")
    private Float price_origin;

    /**
     * Price
     */
    @Column(name = "price")
    private Float price;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer create_uid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp create_date;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer write_uid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp write_date;

    /**
     * Delta time
     */
    @Column(name = "countdown_time", nullable = false)
    private Integer countdown_time;

    /**
     * Time change price
     */
    @Column(name = "price_time_change")
    private Integer price_time_change;

    /**
     * Level change price
     */
    @Column(name = "price_level_change")
    private Float price_level_change;

    /**
     * Cargo
     */
//    @Column(name = "cargo_ids")
//    private Integer cargoIds;

    /**
     * From longitude
     */
    @Column(name = "from_longitude")
    private Float from_longitude;

    /**
     * To longitude
     */
    @Column(name = "to_longitude")
    private Float to_longitude;

    /**
     * Change price time
     */
    @Column(name = "change_price_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp change_price_time;

    @Column(name = "max_confirm_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp max_confirm_time;

    @Transient
    private Integer total_cargo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBidding_order_id() {
        return bidding_order_id;
    }

    public void setBidding_order_id(Integer bidding_order_id) {
        this.bidding_order_id = bidding_order_id;
    }

    public String getBidding_package_number() {
        return bidding_package_number;
    }

    public void setBidding_package_number(String bidding_package_number) {
        this.bidding_package_number = bidding_package_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getConfirm_time() {
        return confirm_time;
    }

    public void setConfirm_time(Timestamp confirm_time) {
        this.confirm_time = confirm_time;
    }

    public Timestamp getRelease_time() {
        return release_time;
    }

    public void setRelease_time(Timestamp release_time) {
        this.release_time = release_time;
    }

    public Timestamp getBidding_time() {
        return bidding_time;
    }

    public void setBidding_time(Timestamp bidding_time) {
        this.bidding_time = bidding_time;
    }

    public Integer getMax_count() {
        return max_count;
    }

    public void setMax_count(Integer max_count) {
        this.max_count = max_count;
    }

    public SharevanDepot getFrom_depot() {
        return from_depot;
    }

    public void setFrom_depot(SharevanDepot from_depot) {
        this.from_depot = from_depot;
    }

    public SharevanDepot getTo_depot() {
        return to_depot;
    }

    public void setTo_depot(SharevanDepot to_depot) {
        this.to_depot = to_depot;
    }

    public Float getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(Float total_weight) {
        this.total_weight = total_weight;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getFrom_latitude() {
        return from_latitude;
    }

    public void setFrom_latitude(Float from_latitude) {
        this.from_latitude = from_latitude;
    }

    public Float getTo_latitude() {
        return to_latitude;
    }

    public void setTo_latitude(Float to_latitude) {
        this.to_latitude = to_latitude;
    }

    public Timestamp getFrom_receive_time() {
        return from_receive_time;
    }

    public void setFrom_receive_time(Timestamp from_receive_time) {
        this.from_receive_time = from_receive_time;
    }

    public Timestamp getTo_receive_time() {
        return to_receive_time;
    }

    public void setTo_receive_time(Timestamp to_receive_time) {
        this.to_receive_time = to_receive_time;
    }

    public Timestamp getFrom_return_time() {
        return from_return_time;
    }

    public void setFrom_return_time(Timestamp from_return_time) {
        this.from_return_time = from_return_time;
    }

    public Timestamp getTo_return_time() {
        return to_return_time;
    }

    public void setTo_return_time(Timestamp to_return_time) {
        this.to_return_time = to_return_time;
    }

    public Float getPrice_origin() {
        return price_origin;
    }

    public void setPrice_origin(Float price_origin) {
        this.price_origin = price_origin;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getCreate_uid() {
        return create_uid;
    }

    public void setCreate_uid(Integer create_uid) {
        this.create_uid = create_uid;
    }

    public Timestamp getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }

    public Integer getWrite_uid() {
        return write_uid;
    }

    public void setWrite_uid(Integer write_uid) {
        this.write_uid = write_uid;
    }

    public Timestamp getWrite_date() {
        return write_date;
    }

    public void setWrite_date(Timestamp write_date) {
        this.write_date = write_date;
    }

    public Integer getCountdown_time() {
        return countdown_time;
    }

    public void setCountdown_time(Integer countdown_time) {
        this.countdown_time = countdown_time;
    }

    public Integer getPrice_time_change() {
        return price_time_change;
    }

    public void setPrice_time_change(Integer price_time_change) {
        this.price_time_change = price_time_change;
    }

    public Float getPrice_level_change() {
        return price_level_change;
    }

    public void setPrice_level_change(Float price_level_change) {
        this.price_level_change = price_level_change;
    }

    public Float getFrom_longitude() {
        return from_longitude;
    }

    public void setFrom_longitude(Float from_longitude) {
        this.from_longitude = from_longitude;
    }

    public Float getTo_longitude() {
        return to_longitude;
    }

    public void setTo_longitude(Float to_longitude) {
        this.to_longitude = to_longitude;
    }

    public Timestamp getChange_price_time() {
        return change_price_time;
    }

    public void setChange_price_time(Timestamp change_price_time) {
        this.change_price_time = change_price_time;
    }

    public Timestamp getMax_confirm_time() {
        return max_confirm_time;
    }

    public void setMax_confirm_time(Timestamp max_confirm_time) {
        this.max_confirm_time = max_confirm_time;
    }

    public Integer getTotal_cargo() {
        return total_cargo;
    }

    public void setTotal_cargo(Integer total_cargo) {
        this.total_cargo = total_cargo;
    }
}