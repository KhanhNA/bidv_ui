package com.ac.entity;

import com.ac.Constants;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * bill package routing when export
 */
@Entity
@Getter
@Setter
@Table(name = "sharevan_bill_package_routing_export")
public class SharevanBillPackageRoutingExport implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * quantity by export
     */
    @Column(name = "quantity_export")
    private Integer quantityExport;

    /**
     * length of plan pacel
     */
    @Column(name = "length")
    private Double length;

    /**
     * width of plan pacel
     */
    @Column(name = "width")
    private Double width;

    /**
     * height of plan pacel
     */
    @Column(name = "height")
    private Double height;

    /**
     * total_weight of plan pacel
     */
    @Column(name = "total_weight")
    private Double totalWeight;

    /**
     * capacity of plan pacel
     */
    @Column(name = "capacity")
    private Double capacity;

    /**
     * Product type
     */

    /**
     *  Product package type
     */
    @Column(name = "product_package_type_id")
    private Integer productPackageTypeId;

    /**
     * Bill package
     */
    @Column(name = "bill_package_id")
    private Integer billPackageId;

    /**
     * Bill lading detail
     */
    @Column(name = "bill_lading_detail_id")
    private Integer billLadingDetailId;

    /**
     * note
     */
    @Column(name = "note")
    private String note;

    /**
     * item name
     */
    @Column(name = "item_name")
    private String itemName;

    /**
     * Insurance name
     */
    @Column(name = "insurance_name")
    private String insuranceName;

    /**
     * Service name
     */
    @Column(name = "service_name")
    private String serviceName;

    /**
     * From warehouse
     */
    @Column(name = "from_warehouse_id")
    private Integer fromWarehouseId;

    /**
     * To warehouse
     */
    @Column(name = "to_warehouse_id")
    private Integer toWarehouseId;

    /**
     * QR code
     */
//    @Column(name = "QRchar")
//    private String QRchar;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private Timestamp writeDate;

    /**
     * QR code
     */
    @Column(name = "qr_char")
    private String qrChar;

    /**
     * Product Type
     */
    @Column(name = "product_type_id")
    private Integer productTypeId;

    /**
     * Package plan
     */
    @Column(name = "routing_package_plan")
    private Integer routingPackagePlan;

    /**
     * Routing plan day
     */
    @Column(name = "routing_plan_day_id")
    private Integer routingPlanDayId;

    /**
     * Status
     */
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * List char comfirm
     */
    @Column(name = "qr_char_confirms")
    private String qrCharConfirms;

    @Column(name = "key_map")
    private String keyMap;

    public SharevanBillPackageRoutingExport() {
    }

    public SharevanBillPackageRoutingExport(SharevanBillPackageRoutingExport other) {
       // this.id = other.id;
        this.quantityExport = other.quantityExport;
        this.length = other.length;
        this.width = other.width;
        this.height = other.height;
        this.totalWeight = other.totalWeight;
        this.capacity = other.capacity;
        this.productPackageTypeId = other.productPackageTypeId;
        this.billPackageId = other.billPackageId;
        this.billLadingDetailId = other.billLadingDetailId;
        this.note = other.note;
        this.itemName = other.itemName;
        this.insuranceName = other.insuranceName;
        this.serviceName = other.serviceName;
        this.fromWarehouseId = other.fromWarehouseId;
        this.toWarehouseId = other.toWarehouseId;
        this.createUid = other.createUid;
        this.createDate = Constants.getNowUTC();
        this.writeUid = other.writeUid;
        this.writeDate = Constants.getNowUTC();
        this.qrChar = other.qrChar;
        this.productTypeId = other.productTypeId;
        this.routingPackagePlan = other.routingPackagePlan;
        this.routingPlanDayId = other.routingPlanDayId;
        this.status = other.status;
        this.qrCharConfirms = other.qrCharConfirms;
        this.keyMap = other.keyMap;
    }
}