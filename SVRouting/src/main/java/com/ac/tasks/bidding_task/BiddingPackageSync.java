package com.ac.tasks.bidding_task;

import com.ac.service.bidding_service.BiddingPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.sql.SQLException;

@Component
public class BiddingPackageSync {
    @Autowired
    private BiddingPackageService biddingPackageService;

    @Scheduled(initialDelay = 0, fixedDelay = 60000)
    public void runBiddingtimeBiddingpackage() {
        System.out.println("kakaka");
        try {
            //   routingPlanDayService.getBillLading();
            biddingPackageService.getListBiddingPackageForBidding();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    @Scheduled(initialDelay = 0, fixedDelay = 60000)
//    public void runReleasetimeBiddingpackage() {
//        System.out.println("release time");
//        try {
//            //   routingPlanDayService.getBillLading();
//            biddingPackageService.getListBiddingPackageForRelease();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    @Scheduled(initialDelay = 0,fixedDelay = 60000)
    public void runChangingPriceBiddingpackage(){
        System.out.println("Change price");
        try {
            biddingPackageService.getListBiddingChangePrice();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(initialDelay = 0, fixedDelay = 60000)
    public void runCheckOutdateConfirmBiddingpackage(){
        System.out.println("out date");
        try {
            try {
                biddingPackageService.checkOutdateBidding();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Scheduled(cron = "0 0 0-23 * * *")
    public void runChangeTimeSlotBidding(){
        System.out.println("change time slot");
        try {
            biddingPackageService.getTimeSlotChangeBidding();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
