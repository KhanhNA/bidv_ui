package com.ac.tasks;

import com.ac.Constants;
import com.ac.api.ApiManager;
import com.ac.entity.SharevanBillRouting;
import com.ac.model.DepotDto;
import com.ac.service.BillRoutingService;
import com.ac.service.RoutingPlanDayService;
import com.ac.service.WarehouseService;
import ns.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Component
public class RoutingPlanDaySync {

    @Autowired
    private RoutingPlanDayService routingPlanDayService;
    @Autowired
    private WarehouseService warehouseService;



//    @Scheduled(cron = "${scheduled.tasks.sync}", initialDelay = 0)
    @Scheduled(initialDelay = 0, fixedDelay = 10000)
    public void run() {
//        System.out.println("hello");
//        try {
////            Date date = new Date();
//           LocalDateTime ldt = LocalDateTime.now();
//         //   LocalDateTime ldt = LocalDateTime.of(2020,11,03,0,0);
//        //    String date = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(ldt);
//         //   routingPlanDayService.getBillLading("30-09-2020");
//            Set<Integer> lstBill = routingPlanDayService.getBillLading(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
//            if(lstBill != null && !lstBill.isEmpty()){
//                callNotificationOdoo(lstBill);
//                calculateCostForNewLocationWithLstOldLocation(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY),lstBill);
//                //warehouseService.calculateCostForNewLocationWithLstOldLocation(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY),lstBill);
//            }
//            System.out.println("finish");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    private void callNotificationOdoo(Set<Integer> lstBill){
        String lst = "";
        for(Integer i : lstBill){
            lst += i + ",";
        }
        lst = lst.substring(0,lst.length()-1);
        lst = "[" + lst + "]";
        String json = "{\n" +
                "\"jsonrpc\": \"2.0\",\n" +
                "\"params\": {\n" +
                "        \"bill_lading_ids\": " + lst +",\n" +
                "        \"secret_key\":\"" + Constants.ODOO_KEY +"\",\n" +
                "        \"user\":\"" + Constants.USER_NAME +"\"\n" +
                "}\n" +
                "}";
        try {
            ApiManager.callApiSocket(json, Constants.API_CREATE_BILL_ROUTING);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void calculateCostForNewLocationWithLstOldLocation(String datePlan, Set<Integer> lstBill) throws SQLException {
            List<DepotDto> lstNewLocation = warehouseService.getListLocationRouting(datePlan,lstBill);
            HashMap<Integer, List<DepotDto>> lstOldLocation = warehouseService.getListWareHouseCheckDistance();
            warehouseService.calculateDistance(lstNewLocation,lstOldLocation);
    }
}
