package com.ac.service;

import com.ac.entity.LocationData;
import com.ac.model.DepotDto;
import com.ac.repository.LocationDataRepository;
import com.ac.tasks.GoogleDistanceAPI;
import com.mysql.cj.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Service
public class WarehouseService {
    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    LocationDataRepository dataRepository;


//    @Transactional
//    public void calculateCostForNewLocationWithLstOldLocation(String datePlan, Set<Integer> lstBill) throws SQLException {
//        List<DepotDto> lstNewLocation = getListLocationRouting(datePlan,lstBill);
//        HashMap<Integer, List<DepotDto>> lstOldLocation = getListWareHouseCheckDistance();
//        calculateDistance(lstNewLocation,lstOldLocation);
//    }
    @Transactional
    public List<DepotDto> getListLocationRouting(String datePlan, Set<Integer> lstBill) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();

        if(cnn != null){
            if (lstBill == null || lstBill.size() == 0) {
                return null;
            }
            String ids = org.apache.commons.lang3.StringUtils.join(lstBill, ",");
            String sql = "select DISTINCT rpd.latitude, rpd.longitude, rpd.zone_area_id from sharevan_routing_plan_day rpd \n" +
                    "\tjoin sharevan_bill_lading_detail bld on rpd.bill_lading_detail_id = bld.id\n" +
                    "\tjoin sharevan_bill_lading bl on bl.id = bld.bill_lading_id\n" +
                    "\twhere bl.id in (" + ids + ") and rpd.date_plan = to_date(?,'dd/mm/yyyy')";
            PreparedStatement ps = cnn.prepareStatement(sql);
            ps.setString(1,datePlan);
            ResultSet re = ps.executeQuery();
            List<DepotDto> lstResult = new ArrayList<>();
            while (re.next()){
                DepotDto d = new DepotDto();
                d.setLatitude(re.getDouble("latitude"));
                d.setLongitude(re.getDouble("longitude"));
                d.setZoneId(re.getInt("zone_area_id"));
                lstResult.add(d);
            }
            return  lstResult;
        }
        return null;
    }
    @Transactional
    public HashMap<Integer, List<DepotDto>> getListWareHouseCheckDistance() throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        if(cnn != null){
            HashMap<Integer,List<DepotDto>> lstWarehouse = new HashMap<>();
            String sql = "select DISTINCT latitude, longitude, sa.zone_area_id from sharevan_warehouse sw\n" +
                    "\tjoin sharevan_area sa on sa.id = sw.area_id\n" +
                    "\twhere sa.zone_area_id is not null and sw.status = 'running'" +
                    " order by sa.zone_area_id";
            PreparedStatement ps = cnn.prepareStatement(sql);
            ResultSet re = ps.executeQuery();
            while (re.next()){
                int zoneId = re.getInt("zone_area_id");
                if(lstWarehouse.get(zoneId) != null){
                    List<DepotDto> data = lstWarehouse.get(zoneId);
                    DepotDto dto = new DepotDto( );
                    //re.getDouble("longitude"),re.getDouble("latitude")
                    dto.setLatitude(re.getDouble("latitude"));
                    dto.setLongitude(re.getDouble("longitude"));
                    data.add(dto);
                    lstWarehouse.put(zoneId,data);
                }
                else{
                    lstWarehouse.put(zoneId,new ArrayList<>());
                }
            }
            List<DepotDto> lstDepot = getListDepot(cnn);
            for(Integer id : lstWarehouse.keySet()){
                List<DepotDto> lstData = lstWarehouse.get(id);
                lstData.addAll(lstDepot);
                lstWarehouse.put(id,lstData);
            }
            return lstWarehouse;
        }
        return null;
    }
    private List<DepotDto> getListDepot (Connection cnn) throws SQLException {
        String sql = "select DISTINCT latitude, longitude from sharevan_depot p \n" +
                        " where p.status = 'running'";
        List<DepotDto> lstDepot = new ArrayList<>();
        PreparedStatement ps = cnn.prepareStatement(sql);
        ResultSet re = ps.executeQuery();
        while (re.next()){
            lstDepot.add(new DepotDto(re.getDouble("longitude"),re.getDouble("latitude")));
        }
        return lstDepot;
    }
    public void calculateDistance(List<DepotDto> lstNewLocation, HashMap<Integer,List<DepotDto>> lstOldLocation){
        List<LocationData> lstLocation = dataRepository.findAll();
        for(DepotDto newLocation : lstNewLocation){
            List<DepotDto> lstLocationInDb = lstOldLocation.get(newLocation.getZoneId());
            if(lstLocationInDb != null && !lstLocationInDb.isEmpty()){
                for(DepotDto oldLocation : lstLocationInDb){
                    //check distance from A-> B
                    String timeKey1 = LocationData.getLocationCode(newLocation.getLatitude(), newLocation.getLongitude(),
                            oldLocation.getLatitude(), oldLocation.getLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
                    boolean check = isExistLocationInDb(timeKey1,lstLocation );
                    LocationData dataNew1, dataNew2;
                    if(check == false){
//                        dataNew1 = GoogleDistanceAPI.getDistance(newLocation.getLatitude(), newLocation.getLongitude(),
//                                    oldLocation.getLatitude(), oldLocation.getLongitude());
//                        dataNew1 = dataRepository.save(dataNew1);
//                        System.out.println("d1: " + dataNew1.getCode() + " cost: " + dataNew1.getCost() + " minutes: " + dataNew1.getMinutes());
//                        lstLocation.add(dataNew1);
                        lstLocation = addNewLocation(newLocation,oldLocation,lstLocation);
                    }

                    //check distance from B -> A
                    String timeKey2 = LocationData.getLocationCode(oldLocation.getLatitude(), oldLocation.getLongitude(),
                            newLocation.getLatitude(), newLocation.getLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
                    boolean check2 = isExistLocationInDb(timeKey2,lstLocation );
                    if(check == false){
//                        dataNew2 = GoogleDistanceAPI.getDistance(oldLocation.getLatitude(), oldLocation.getLongitude(),
//                                newLocation.getLatitude(), newLocation.getLongitude());
//                        dataNew2 = dataRepository.save(dataNew2);
//                        System.out.println("d2: " + dataNew2.getCode() + " cost: " + dataNew2.getCost() + " minutes: " + dataNew2.getMinutes());
//
//                        lstLocation.add(dataNew2);
                        lstLocation = addNewLocation(oldLocation,newLocation,lstLocation);
                    }

                }
            }
        }
    }

    private boolean isExistLocationInDb(String codeCheck, List<LocationData> lstData){
        for(LocationData data: lstData){
            if(codeCheck.equals(data.getCode())){
                return true;
            }
        }
        return false;
    }
    private List<LocationData> addNewLocation(DepotDto newLocation, DepotDto oldLocation,List<LocationData> lstLocation ){
        LocationData dataNew1 = GoogleDistanceAPI.getDistance(newLocation.getLatitude(), newLocation.getLongitude(),
                oldLocation.getLatitude(), oldLocation.getLongitude());
        if( Double.compare(dataNew1.getCost(),0.0)==0 && Double.compare(dataNew1.getMinutes(),0.0)==0 && newLocation.getLatitude() != oldLocation.getLatitude()){
            addNewLocation(newLocation,oldLocation,lstLocation);
        }
        else{
            dataNew1 = dataRepository.save(dataNew1);
            System.out.println("d1: " + dataNew1.getCode() + " cost: " + dataNew1.getCost() + " minutes: " + dataNew1.getMinutes());
            lstLocation.add(dataNew1);
        }
        return lstLocation;
    }
}
