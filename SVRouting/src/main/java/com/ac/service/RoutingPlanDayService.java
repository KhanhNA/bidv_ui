package com.ac.service;

import com.ac.Constants;
import com.ac.api.ApiManager;
import com.ac.entity.*;
import com.ac.enums.*;
import com.ac.model.DepotDto;
import com.ac.model.RoutingDetail;
import com.ac.repository.ConfigParameterRepository;
import com.ac.repository.LocationDataRepository;
import com.ac.repository.SharevanBillRoutingRepository;
import com.ac.tasks.GoogleDistanceAPI;
import ns.utils.DateUtils;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.net.URISyntaxException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class RoutingPlanDayService {

    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    protected BillPackageService billPackageService;
    @Autowired
    protected LocationDataRepository dataRepository;
    @Autowired
    protected VehicleService vehicleService;

    @Autowired
    protected BillPackageRoutingPlanService billPackageRoutingPlanService;
    @Autowired
    protected BillPackageRoutingImportService billPackageRoutingImportService;
    @Autowired
    protected BillPackageRoutingExportService billPackageRoutingExportService;
    @Autowired
    protected BillRoutingService billRoutingService;
    @Autowired
    protected ServiceRoutingService routingService;
    @Autowired
    protected ConfigParameterRepository parameterRepository;
    @Autowired
    protected SharevanBillRoutingRepository billRoutingRepository;

    public static float returnAmountPercent = 0;

    Integer fromRoutingPlanId = null;

    HashMap<Integer,RoutingPlanDay> routingMap = new HashMap<>();

    @Transactional
    public Set<Integer>  getBillLading(String date) throws Exception {

        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String sql =
                " select COALESCE(w.longitude, b.longitude) as longitude,   " +
                        " COALESCE(w.latitude, b.latitude) as latitude,   " +
                        " COALESCE( b.address,w.address) as address,   " +
                        " COALESCE(w.phone, b.phone) as phone,   " +
                        " COALESCE(w.name, b.address) as warehouse_name,   " +
                        " sd.name depot_name, " +
                        " sd.phone depot_phone, " +
                        " sd.latitude depot_lat, " +
                        " sd.longitude depot_lgn, " +

                        "   start_date, " +
                        "   b.name bill_lading_detail_code, " +
                        "   end_date, " +
                        "   ru.id partner_id , " +
                        "   b.total_volume , " +
//                        "--                               b.name as orderNumber,   " +
                        "                               b.expected_from_time,   " +
                        "                               b.expected_to_time,   " +
                        "                               b.warehouse_id,   " +
                        "                               b.warehouse_type,   " +
                        "                               b.status,   " +
                        "                               b.zone_area_id,   " +
                        "                               sz.depot_id,  " +
                        "                               b.bill_lading_id,  " +
                        "                               b.id as bill_lading_detail_id,   " +
                        "                               b.total_weight,   " +
                        "                               b.address,   " +
                        "                               sd.address depot_address,   " +
                        "                               sbl.company_id,   " +
                        "                               sbl.qr_code,   " +
                        "                               sbl.sbl_type,   " +
                        "                               b.price,   " +
                        "                               sbl.insurance_id   " +
                        "                         from sharevan_bill_lading_detail as b   " +
                        "                               join sharevan_bill_lading as sbl on b.bill_lading_id = sbl.id   " +
                        "                               left join sharevan_warehouse w on b.warehouse_id = w.id   " +
                        "                               left join sharevan_zone sz on b.zone_area_id = sz.id   " +
                        "                               left join sharevan_depot sd on sz.depot_id = sd.id    " +
                        "                               left join res_partner ru on ru.user_id = sbl.create_uid   " +
                        "                         where   " +
                        "                         --      sz.depot_id is not null and \n " +
                        "                               sbl.start_date <= to_date(?,'dd/mm/yyyy') " +
                        "                               and b.status_order = 'running' " +
                        "                               and sbl.status = 'running' " +
                        "                               and (sbl.end_date is null or sbl.end_date >= to_date(?,'dd/mm/yyyy'))   " +
                        "                               and (case when sbl.frequency in (4,5) and sbl.start_date = to_date(?,'dd/mm/yyyy') then 1     " + //express, one time
                        "                                      when sbl.frequency in (1) then 1    " + //daily
                        "                                      when sbl.frequency in (2) and day_of_week = EXTRACT (dow from to_date(?,'dd/mm/yyyy')) then 1    " + //----weekly
                        "                                      when sbl.frequency in (3) and day_of_month = extract(day from to_date(?,'dd/mm/yyyy')) then 1  " +
                        "                                      else 0   " +
                        "                                end) =1   " +
                        "                         and (b.last_scan is null or b.last_scan < to_date(?,'dd/mm/yyyy')) " +
//                        "--                         where b.status = '1'   " +
                        "                         order by bill_lading_id, warehouse_type";
        System.out.println("date: " + date);
        PreparedStatement st = cnn.prepareStatement(sql);
        int i = 1;
        for (; i < 7; i++) {
            st.setObject(i, date);
        }

        ResultSet rs = st.executeQuery();
        if (rs == null) {
            return null;
        }

        List<BillLadingDetail> result = new ArrayList<>();
        i = 0;
        List<Integer> billDetailIds = new ArrayList<>();
        Integer pickBillId = null, deliveryBillId, tmpBillLadingId;
//        Integer pickZoneAreaId = null, deliveryZoneAreaId = null, tmpZoneAreaId;
        Integer pickDepotId = null, deliveryDepotId = null, tmpDepotId;
        String pickDepotAddress = "", deliveryDepotAddress = "", tmpDepotAddress;
        WarehouseType warehouseType;
        DepotDto pickupDepot = new DepotDto(), deliveryDepot = new DepotDto(), tmpDepot = new DepotDto();
        fromRoutingPlanId = null;

        //insert vao solution_day 1 ban ghi
        if (!rs.next()) { //ko co ban ghi nao
            return null;
        }
        SolutionDay solutionDay = insertSolutionDay(date, rs);

        Set<Integer> lstIdBillRouting = new HashSet<>();


        HashMap<Integer,SharevanBillRouting> lstBillRouting = new HashMap<>();

        do {//duyet bill_lading_detail

            i++;
            tmpBillLadingId = rs.getInt("bill_lading_id");
            tmpDepotId = rs.getInt("depot_id");
            tmpDepotAddress = rs.getString("depot_address");
//            tmpZoneAreaId = rs.getInt("zone_area_id");
            warehouseType = WarehouseType.of(rs.getString("warehouse_type"));

            billDetailIds.add(rs.getInt("bill_lading_detail_id"));
            SharevanBillRouting br = lstBillRouting.get(tmpBillLadingId);
            if(br == null){
                br = billRoutingRepository.findByFromBillLadingIdAndStartDate(tmpBillLadingId,new SimpleDateFormat("dd/MM/yyyy").parse(date));
                if(br == null){
                    //them moi sharevan_bill_routing
                    SharevanBillLading bld = entityManager.find(SharevanBillLading.class,tmpBillLadingId);
                    if(bld != null){
                        br = billRoutingService.createBillRoutingByBOL(bld,date);

                    }
                }
                if (br != null)
                    br.setStatusRouting(BillRoutingStatus.WAITING.getValue());
                lstBillRouting.put(tmpBillLadingId,br);
            }
            lstIdBillRouting.add(br.getId());

            if (warehouseType == WarehouseType.PICKUP) {//neu la pickup -> luu lai id de cap nhat cho cac bg delivery
//                pickZoneAreaId = tmpZoneAreaId;
                pickBillId = tmpBillLadingId;
                pickDepotId = tmpDepotId;
                pickDepotAddress = tmpDepotAddress;
                pickupDepot.setId(pickDepotId);
                pickupDepot.setAddress(pickDepotAddress);
                pickupDepot.setName(rs.getString("depot_name"));
                pickupDepot.setPhone(rs.getString("depot_phone"));
                pickupDepot.setLatitude(rs.getDouble("depot_lat"));
                pickupDepot.setLongitude(rs.getDouble("depot_lgn"));
                pickupDepot.setZoneId(rs.getInt("zone_area_id"));
            } else { //neu la delivery
//                if (pickZoneAreaId == null) {
//                    throw new Exception("khong co kho nhan");
//                }
                if (pickBillId == null || tmpBillLadingId == null || !pickBillId.equals(tmpBillLadingId)) {
                    throw new Exception("khong co don nhan" + tmpBillLadingId + "_" + pickBillId);
                }
//                deliveryZoneAreaId = tmpZoneAreaId;
                deliveryDepotId = tmpDepotId;
                deliveryDepotAddress = tmpDepotAddress;
                deliveryDepot.setId(deliveryDepotId);
                deliveryDepot.setAddress(deliveryDepotAddress);
                deliveryDepot.setName(rs.getString("depot_name"));
                deliveryDepot.setPhone(rs.getString("depot_phone"));
                deliveryDepot.setLatitude(rs.getDouble("depot_lat"));
                deliveryDepot.setLongitude(rs.getDouble("depot_lgn"));
                deliveryDepot.setZoneId(rs.getInt("zone_area_id"));
            }
//            if (pickZoneAreaId == null) {
//                throw new Exception("kho null:" + pickZoneAreaId + "_" + deliveryZoneAreaId);
//            }
            System.out.println("warehouse_type: " + warehouseType.getValue());

            //t/h insert theo cac tinh huong: pickup, delivery in zone, delivery out zone
            insertRoutingPlanDay(solutionDay, rs, date, warehouseType, pickupDepot, deliveryDepot,br);
        } while (rs.next());
        updateBillStatus(cnn, billDetailIds, date);
        updateNumReceive(date,cnn);
        for(Integer key : lstBillRouting.keySet()){
            SharevanBillRouting br = lstBillRouting.get(key);
            br = entityManager.merge(br);
        }
        System.out.println("a:" + result.toString());


        return lstIdBillRouting;
    }


    protected void updateBillStatus(Connection cnn, List<Integer> billIds, String date) throws SQLException {
        if (billIds == null || billIds.size() == 0) {
            return;
        }
        String ids = org.apache.commons.lang3.StringUtils.join(billIds, ",");
        //String sql = "update sharevan_bill_lading_detail set status = '2', last_scan=? where id in (" + ids + ")";
        String sql = "update sharevan_bill_lading_detail set last_scan=? where id in (" + ids + ")";
        PreparedStatement stmt = cnn.prepareStatement(sql);
        date += " 00:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

        stmt.setObject(1, dateTime);
        stmt.executeUpdate();

    }
    @Transactional
    protected void insertRoutingPlanDay(SolutionDay solutionDay, ResultSet rs, String date, WarehouseType warehouseType,
                                        DepotDto pickupDepot,
                                        DepotDto deliveryDepot,
                                        SharevanBillRouting br) throws Exception {
        //den kho PICKUP -> capacity cua xe tang
        //den kho DELIVERY -> capacity cua xe giam
        double pick = rs.getDouble("total_weight");
        double delivery = rs.getDouble("total_weight")*(-1);

        List<SharevanRoutingPlanDayService> lstService = null;
        Integer warehouseId = rs.getInt("warehouse_id") == 0 ? null : rs.getInt("warehouse_id");
        RoutingPlanDay routingPlanDay = new RoutingPlanDay();
        RoutingPlanDay longHoldPick, longHoldDelevery;
        routingPlanDay.setCheckPoint(false);
        routingPlanDay.setNumReceice(0);
        routingPlanDay.setTotalVolume(rs.getDouble("total_volume"));
        routingPlanDay.setPackagedIntoCargo(CargoPackingStatus.IN_ROUTING.getValue());
        routingPlanDay.setSolutionDayId(solutionDay.getId());
        routingPlanDay.setLatitude(rs.getDouble("latitude"));
        routingPlanDay.setLongitude(rs.getDouble("longitude"));
        routingPlanDay.setBillLadingDetailCode(rs.getString("bill_lading_detail_code"));
//        routingPlanDay.setOrderNumber(rs.getString("orderNumber"));
        routingPlanDay.setExpectedFromTime(rs.getTimestamp("expected_from_time"));
        routingPlanDay.setExpectedToTime(rs.getTimestamp("expected_to_time"));
        routingPlanDay.setType(warehouseType.getValue());
        routingPlanDay.setBillLadingDetailId(rs.getInt("bill_lading_detail_id"));
        routingPlanDay.setCompanyId(rs.getInt("company_id"));
        routingPlanDay.setStatus(StatusRouting.NOT_CONFIRM.getValue());
        routingPlanDay.setStockManId(rs.getInt("partner_id")==0 ? null : rs.getInt("partner_id"));
        routingPlanDay.setCapacityExpected(pick);
        routingPlanDay.setCapacityActual(pick);
        routingPlanDay.setTroubleType("0");
        routingPlanDay.setPhone(rs.getString("phone"));
        routingPlanDay.setPartnerId(rs.getInt("partner_id")==0 ? null : rs.getInt("partner_id"));
        routingPlanDay.setDepotId(pickupDepot.getId());
        routingPlanDay.setQrGenCheck(false);
        routingPlanDay.setToogle(false);
        if(br != null){
            routingPlanDay.setBillRoutingId(br.getId());
            br.setRoutingScan(true);
        }

        if(warehouseType.getValue().equals(WarehouseType.DELIVERY.getValue())){
            routingPlanDay.setCapacityExpected(delivery);
            routingPlanDay.setDepotId(deliveryDepot.getId());
            routingPlanDay.setCapacityActual(delivery);
        }
        routingPlanDay.setWarehouseId(warehouseId);
        routingPlanDay.setDatePlan(DateUtils.toDate(date, DateUtils.DD_MM_YYYY));
        routingPlanDay.setCreateDate(Constants.getNowUTC());
        routingPlanDay.setAssessAmount(rs.getDouble("price"));
        routingPlanDay.setInsuranceId(rs.getInt("insurance_id")==0 ? null : rs.getInt("insurance_id"));
        routingPlanDay.setSoType(false);
        //bo sung check don la cua ben mstore thi bo sung them ma QR
        if(BillLadingType.SO.getValue().equals(rs.getString("sbl_type"))){
            routingPlanDay.setQrGenCheck(true);
            routingPlanDay.setQrSO(rs.getString("qr_code"));
            routingPlanDay.setStatus(StatusRouting.DRAFT.getValue());
            routingPlanDay.setSoType(true);
            if (warehouseType.equals(WarehouseType.PICKUP)) {
                routingPlanDay.setNumReceice(1);
            }
        }
        //lay toan bo dich vu ke hoach cua don bill_lading_detail

        if(rs.getInt("bill_lading_detail_id") > 0){
            lstService = getListServiceRouting(rs.getInt("bill_lading_detail_id"));
        }


        //neu la pickup hoac in zone thi them 1 bg vao routing_plan
        if (warehouseType.getValue().equals(WarehouseType.PICKUP.getValue()) || pickupDepot.getId() == deliveryDepot.getId()) {
            routingPlanDay.setAddress(rs.getString("address"));
            routingPlanDay.setShipType(ShipType.IN_ZONE.getValue());
            routingPlanDay.setWarehouseName(rs.getString("warehouse_name"));
            routingPlanDay.setZoneAreaId(pickupDepot.getZoneId()==0? null: pickupDepot.getZoneId());
//            routingPlanDay.setZoneAreaId(pickupZoneAreaId);
            if (warehouseType.getValue().equals(WarehouseType.DELIVERY.getValue()) && (pickupDepot.getId() == deliveryDepot.getId())) {
                routingPlanDay.setFromRoutingPlanDayId(fromRoutingPlanId);
                routingPlanDay.setZoneAreaId(deliveryDepot.getZoneId()==0? null: deliveryDepot.getZoneId());
            }
            routingPlanDay = entityManager.merge(routingPlanDay);
            if(lstService != null && !lstService.isEmpty()){
                insertServicePlan(routingPlanDay,lstService);
            }
            if (warehouseType.equals(WarehouseType.PICKUP)) {
                fromRoutingPlanId = routingPlanDay.getId();
            }
            //gen code cho routing_plan_day
            System.out.println("prefix: " + Constants.ROUTING_PLAN_CODE_PRE + " id: " + routingPlanDay.getId() + " padding: " + Constants.padding);
            String code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, routingPlanDay.getId(), Constants.padding);
            routingPlanDay.setRoutingPlanDayCode(code);
            routingPlanDay = entityManager.merge(routingPlanDay);
            routingMap.put(routingPlanDay.getId(),routingPlanDay);
            //updateRoutingPlanDayCode(routingPlanDay);
            //them cacs ban ghi bill_package_plan/import/export
            billPackageService.createBillPackageRouting(routingPlanDay, rs.getString("bill_lading_detail_code"));
            routingPlanDay = entityManager.merge(routingPlanDay);
            return;
        }

//            routingPlanDay.setZoneAreaId(pickupZoneId);
        //0: A -> Depot A -> Depot B -> B
        //1. insert 1 bg delivery toi depot cua A
        routingPlanDay.setDepotId(pickupDepot.getId());
        routingPlanDay.setZoneAreaId(pickupDepot.getZoneId()==0? null:pickupDepot.getZoneId());
        routingPlanDay.setWarehouseId(null);

        routingPlanDay.setType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setCapacityExpected(delivery);
        routingPlanDay.setCapacityActual(delivery);
        routingPlanDay.setAddress(pickupDepot.getAddress());
        routingPlanDay.setLatitude(pickupDepot.getLatitude());
        routingPlanDay.setLongitude(pickupDepot.getLongitude());
        routingPlanDay.setWarehouseName(pickupDepot.getName());
        routingPlanDay.setPhone(pickupDepot.getPhone());
        routingPlanDay.setShipType(ShipType.IN_ZONE.getValue());
        routingPlanDay.setPreviousId("");
        routingPlanDay.setNextId("");
        routingPlanDay.setFromRoutingPlanDayId(fromRoutingPlanId);
//        routingPlanDay.setZoneAreaId(pickupZoneAreaId);
        RoutingPlanDay routingPlanDay1 = entityManager.merge(routingPlanDay);
        //insert cacs dich vu cua don
        if(lstService != null && !lstService.isEmpty()){
            insertServicePlan(routingPlanDay1,lstService);
        }
        //gen code cho routing_plan_day
        String code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, routingPlanDay1.getId(), Constants.padding);
        routingPlanDay1.setRoutingPlanDayCode(code);
        routingPlanDay1 = entityManager.merge(routingPlanDay1);
        routingMap.put(routingPlanDay1.getId(),routingPlanDay1);
        //updateRoutingPlanDayCode(routingPlanDay1);
        //them cacs ban ghi bill_package_plan/import/export
        billPackageService.createBillPackageRouting(routingPlanDay1, rs.getString("bill_lading_detail_code"));
        routingPlanDay1 = entityManager.merge(routingPlanDay1);
        //2. insert 1 bg Pickup o depot A
       // routingPlanDay.setDepotId(pickupDepotId);
        routingPlanDay.setWarehouseId(null);
        routingPlanDay.setType(WarehouseType.PICKUP.getValue());
        routingPlanDay.setStatus(StatusRouting.DRAFT.getValue());
      //  routingPlanDay.setAddress(pickupDepotAddress);
        routingPlanDay.setCapacityExpected(pick);
        routingPlanDay.setCapacityActual(pick);
        routingPlanDay.setShipType(ShipType.LONG_HOLD.getValue());
        routingPlanDay.setFromRoutingPlanDayId(routingPlanDay1.getId());
        routingPlanDay.setPreviousId("");
        routingPlanDay.setNextId("");
        longHoldPick = entityManager.merge(routingPlanDay);
       // System.out.println("prefix: " + Constants.ROUTING_PLAN_CODE_PRE + " id: " + routingPlanDay.getId() + " padding: " + Constants.padding);
        code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, longHoldPick.getId(), Constants.padding);
        longHoldPick.setRoutingPlanDayCode(code);
        longHoldPick = entityManager.merge(longHoldPick);
        routingMap.put(longHoldPick.getId(),longHoldPick);
        //updateRoutingPlanDayCode(routingPlanDay);
        //them cacs ban ghi bill_package_plan/import/export
        billPackageService.createBillPackageRouting(longHoldPick, rs.getString("bill_lading_detail_code"));

        //2. insert 1 delivery toi depot cua zone B
        routingPlanDay.setDepotId(deliveryDepot.getId());
        routingPlanDay.setZoneAreaId(deliveryDepot.getZoneId()==0? null:deliveryDepot.getZoneId());
        routingPlanDay.setWarehouseId(null);
        routingPlanDay.setType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setAddress(deliveryDepot.getAddress());
        routingPlanDay.setPhone(deliveryDepot.getPhone());
        routingPlanDay.setWarehouseName(deliveryDepot.getName());
        routingPlanDay.setLatitude(deliveryDepot.getLatitude());
        routingPlanDay.setLongitude(deliveryDepot.getLongitude());
        routingPlanDay.setCapacityExpected(delivery);
        routingPlanDay.setCapacityActual(delivery);
        routingPlanDay.setShipType(ShipType.LONG_HOLD.getValue());
        routingPlanDay.setNextId("");
        routingPlanDay.setPreviousId("" + longHoldPick.getId());
        routingPlanDay.setFromRoutingPlanDayId(longHoldPick.getId());
        longHoldDelevery = entityManager.merge(routingPlanDay);
        code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, longHoldDelevery.getId(), Constants.padding);
        longHoldDelevery.setRoutingPlanDayCode(code);
        longHoldDelevery = entityManager.merge(longHoldDelevery);
        routingMap.put(longHoldDelevery.getId(),longHoldDelevery);
        //updateRoutingPlanDayCode(routingPlanDay);
        //them cacs ban ghi bill_package_plan/import/export
        billPackageService.createBillPackageRouting(longHoldDelevery, rs.getString("bill_lading_detail_code"));

        //update next cua longholdpick
        longHoldPick.setNextId("" + longHoldDelevery.getId());
        entityManager.merge(longHoldPick);


        //3. insert 1 pickup toi depot cua zone B
        Integer fromPlanId = null;
       // routingPlanDay.setDepotId(deliveryDepotId);
        routingPlanDay.setWarehouseId(null);
        routingPlanDay.setType(WarehouseType.PICKUP.getValue());
        routingPlanDay.setShipType(ShipType.IN_ZONE.getValue());
    //    routingPlanDay.setAddress(deliveryDepotAddress);
        routingPlanDay.setCapacityExpected(pick);
        routingPlanDay.setCapacityActual(pick);
        routingPlanDay.setPreviousId("");
        routingPlanDay.setNextId("");
        routingPlanDay.setFromRoutingPlanDayId(longHoldDelevery.getId());
        RoutingPlanDay routingPlanDay2 = entityManager.merge(routingPlanDay);
        //insert cacs dich vu cua don
        if(lstService != null && !lstService.isEmpty()){
            insertServicePlan(routingPlanDay2,lstService);
        }
        fromPlanId = routingPlanDay2.getId();
        //gen code cho routing_plan_day
        code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, routingPlanDay2.getId(), Constants.padding);
        routingPlanDay2.setRoutingPlanDayCode(code);
        routingPlanDay2 = entityManager.merge(routingPlanDay2);
        routingMap.put(routingPlanDay2.getId(),routingPlanDay2);
        //updateRoutingPlanDayCode(routingPlanDay2);
        //them cacs ban ghi bill_package_plan/import/export
        billPackageService.createBillPackageRouting(routingPlanDay2, rs.getString("bill_lading_detail_code"));
        //4. insert 1 delivery toi warehouse cua khach hang
        routingPlanDay.setDepotId(null);
        routingPlanDay.setWarehouseId(warehouseId);
        routingPlanDay.setType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setShipType(ShipType.IN_ZONE.getValue());
        routingPlanDay.setAddress(rs.getString("address"));
        routingPlanDay.setWarehouseName(rs.getString("warehouse_name"));
        routingPlanDay.setLatitude(rs.getDouble("latitude"));
        routingPlanDay.setLongitude(rs.getDouble("longitude"));
        routingPlanDay.setPhone(rs.getString("phone"));
        routingPlanDay.setPreviousId("");
        routingPlanDay.setCapacityExpected(delivery);
        routingPlanDay.setCapacityActual(delivery);
        routingPlanDay.setNextId("");
        routingPlanDay.setFromRoutingPlanDayId(routingPlanDay2.getId());
//        routingPlanDay.setZoneAreaId(deliveryZoneAreaId);
        RoutingPlanDay routingPlanDay3 = entityManager.merge(routingPlanDay);
        //insert cacs dich vu cua don
        if(lstService != null && !lstService.isEmpty()){
            insertServicePlan(routingPlanDay3,lstService);
        }
        //gen code cho routing_plan_day
        code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, routingPlanDay3.getId(), Constants.padding);
        routingPlanDay3.setRoutingPlanDayCode(code);
        routingPlanDay3 = entityManager.merge(routingPlanDay3);
        routingMap.put(routingPlanDay3.getId(),routingPlanDay3);
        //updateRoutingPlanDayCode(routingPlanDay3);
        //them cacs ban ghi bill_package_plan/import/export
        billPackageService.createBillPackageRouting(routingPlanDay3, rs.getString("bill_lading_detail_code"));

    }

    protected SolutionDay insertSolutionDay(String date, ResultSet rs) throws Exception {
        SolutionDay solutionDay = new SolutionDay();
        solutionDay.setDatePlan( DateUtils.toDate(date, DateUtils.DD_MM_YYYY).toLocalDate());
        solutionDay.setName(date);
        return entityManager.merge(solutionDay);
    }

//    protected void updateRoutingPlanDayCode(RoutingPlanDay routingPlanDay) {
//        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
//        String sql = "update sharevan_routing_plan_day rpd set routing_plan_day_code=? where rpd.id=?";
//        try {
//            PreparedStatement ps = cnn.prepareStatement(sql);
//            System.out.println("prefix: " + Constants.ROUTING_PLAN_CODE_PRE + " id: " + routingPlanDay.getId() + " padding: " + Constants.padding);
//            String code = Constants.genCode(Constants.ROUTING_PLAN_CODE_PRE, routingPlanDay.getId(), Constants.padding);
//            routingPlanDay.setRoutingPlanDayCode(code);
//            ps.setString(1, code);
//            ps.setInt(2, routingPlanDay.getId());
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
    private List<SharevanRoutingPlanDayService> getListServiceRouting(int billLadingDetailId)  {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        List<SharevanRoutingPlanDayService> lstService = new ArrayList<>();
        String sql = "select st.name, " +
                     "  st.id service_id, " +
                     " tmp.sharevan_bill_lading_detail_id " +
                "from sharevan_bill_lading_detail_sharevan_service_type_rel tmp " +
                "join sharevan_service_type st on st.id = tmp.sharevan_service_type_id " +
                "where tmp.sharevan_bill_lading_detail_id =?";
        PreparedStatement ps = null;
        try {
            ps = cnn.prepareStatement(sql);
            ps.setInt(1,billLadingDetailId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                SharevanRoutingPlanDayService service = new SharevanRoutingPlanDayService();
                service.setCreateDate(Constants.getNowUTC());
               // service.setName(rs.getString("name"));
                service.setServiceId(rs.getInt("service_id"));
                service.setBillLadingDetailId(rs.getInt("sharevan_bill_lading_detail_id"));
                service.setType(ServiceType.PLAN.getValue());
                lstService.add(service);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
       return lstService;

    }
    private void insertServicePlan(RoutingPlanDay rpd, List<SharevanRoutingPlanDayService> lstService){
        for(SharevanRoutingPlanDayService service: lstService){
            service.setRoutingPlanDayId(rpd.getId());
            entityManager.merge(service);
        }
    }
    private void updateNumReceive(String date_plan, Connection cnn){
       String sql = " select rpd1.id, count(DISTINCT rpd2.id ) dem " +
                        " from sharevan_routing_plan_day rpd1 " +
                        " join sharevan_routing_plan_day rpd2 on rpd1.id = rpd2.from_routing_plan_day_id " +
                        " where rpd1.date_plan = to_date( ?,'dd/mm/yyyy') and rpd1.type='0' and rpd1.ship_type='0' and rpd1.status = '0' and rpd2.status = '0' " +
                        " group by rpd1.id ";
        try {
            PreparedStatement ps = cnn.prepareStatement(sql);
            ps.setString(1,date_plan);
            ResultSet re = ps.executeQuery();
            while (re.next()){
                int id = re.getInt("id");
                RoutingPlanDay data = routingMap.get(id);
                if(data == null){
                    continue;
                }
                else{
                    data.setNumReceice(re.getInt("dem"));
                    data = entityManager.merge(data);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    /*-------------------------Tach don------------------------*/
    public int  getMaxTonage(Connection cnn){
        String sql = "select max(capacity) max_capacity from fleet_vehicle";
        try {
            PreparedStatement psvm = cnn.prepareStatement(sql);
            ResultSet re = psvm.executeQuery();
            if(re.next()){
                return re.getInt("max_capacity");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }
//    public List<RoutingPlanDay> getListSeparateRouting(String datePlan){
//        Query query  = entityManager.createQuery( "SELECT * " +
//                "FROM sharevan_routing_plan_day rpd\n" +
//                "WHERE (capacity_expected > ?\n" +
//                "       OR from_routing_plan_day_id in\n" +
//                "         (SELECT id\n" +
//                "          FROM sharevan_routing_plan_day\n" +
//                "          WHERE capacity_expected > ?))\n" +
//                "  AND date_plan = to_date(?, 'dd/mm/yyyy')\n" +
//                "  AND status in ('0',\n" +
//                "                 '5')";
//    }
    public void assignVehicleForRouting(List<RoutingPlanDay> lstRouting){
        for(RoutingPlanDay routing: lstRouting){
            final int changes =
                    entityManager.createQuery("update RoutingPlanDay  set driverId = :driverId , vehicleId= :vehicleId," +
                            "                         orderNumber = :orderNumber,  " +
                            "                         status = :status,  " +
                            "                         driverSosId = :driverSosId  " +
                            " where id = :id")
                            .setParameter("driverId", routing.getDriverId())
                            .setParameter("vehicleId", routing.getVehicleId())
                            .setParameter("id", routing.getId())
                            .setParameter("orderNumber",routing.getOrderNumber())
                            .setParameter("status",StatusRouting.NOT_CONFIRM.getValue())
                            .setParameter("driverSosId",null)
                            .executeUpdate();
        }
    }
    public List<RoutingPlanDay> assignRoutingSOS(List<RoutingPlanDay> lstRouting, String datePlan){
        //sap xep tuyen
        lstRouting = sortRoutingByOrderNumber(lstRouting);
        for(RoutingPlanDay r: lstRouting){
            System.out.println(r.getOrderNumber() + "->");
        }
        RoutingPlanDay routingMaxCapacity = Collections.max(lstRouting,
                                                        Comparator.comparing(routingPlanDay -> routingPlanDay.getCapacityExpected()));
        System.out.println("max capacity: " + routingMaxCapacity.getCapacityExpected());
        List<FleetVehicle> lstVehicle = vehicleService.getListAvailableVehicle(datePlan);
        if(lstVehicle == null || lstVehicle.isEmpty()){
            //call notification de thang nhan vien sharevan tim xe ngoai
            return null;
        }
        // tim xe phu hop voi chi phi nho nhat
        List<LocationData> lstLocation = dataRepository.findAll();
       // List<FleetVehicle> lstVehicleAvailable = new ArrayList<>();
        Map<FleetVehicle,Double> lstVehicleAvailable = new HashMap<FleetVehicle,Double>();
        int oldDriver, oldVehicle;
        oldDriver = lstRouting.get(0).getDriverId();
        oldVehicle = lstRouting.get(0).getVehicleId();
        String lstBillRoutingId = "";

        for(FleetVehicle vehicle: lstVehicle){
            if(Double.compare(routingMaxCapacity.getCapacityExpected(),vehicle.getCapacity()) <= 0){
                boolean check = true;
                double totalCapacity = 0;
                double maxCapacity = vehicle.getCapacity();
                ParkingPoint parkingPoint = vehicle.getParkingPoint();
                double cost = 0;
                LocationData location = getCostByLocation(lstLocation,parkingPoint.getLatitude(), parkingPoint.getLongitude(),lstRouting.get(0).getLatitude(), lstRouting.get(0).getLongitude());
                cost += location == null ? 0: location.getCost()*vehicle.getCostPerUnit();
                for(int i = 0; i < lstRouting.size(); i++){
                    totalCapacity += lstRouting.get(i).getCapacityExpected();
                    if(Double.compare(totalCapacity,maxCapacity) > 0 ){
                        check = false;
                        break;
                    }
                    if(i == lstRouting.size() - 1){
                        location = getCostByLocation(lstLocation,lstRouting.get(i).getLatitude(), lstRouting.get(i).getLongitude(),parkingPoint.getLatitude(), parkingPoint.getLongitude());
                        cost += location == null ? 0: location.getCost()*vehicle.getCostPerUnit();
                    }
                    else{
                        location = getCostByLocation(lstLocation,lstRouting.get(i).getLatitude(), lstRouting.get(i).getLongitude(),lstRouting.get(i+1).getLatitude(), lstRouting.get(i+1).getLongitude());
                        cost += location == null ? 0: location.getCost()*vehicle.getCostPerUnit();
                    }
                }
                if(check == true){
                    lstVehicleAvailable.put(vehicle,cost);
                }
            }
            else{
                continue;
            }
        }
        if(!lstVehicleAvailable.isEmpty()){
           // FleetVehicle minKey = (FleetVehicle) Collections.min(lstVehicleAvailable.keySet());
            Map.Entry<FleetVehicle,Double> maxEntry = Collections.min(lstVehicleAvailable.entrySet(), new Comparator<Map.Entry<FleetVehicle,Double>>() {
                public int compare(Map.Entry<FleetVehicle,Double> e1, Map.Entry<FleetVehicle,Double> e2) {
                    return e1.getValue()
                            .compareTo(e2.getValue());
                }
            });
            if(maxEntry != null ){
                FleetVehicle vehicle = maxEntry.getKey();
                FleetDriver fleetDriver = vehicleService.getDriverByAssignationLog(datePlan, vehicle.getId());
                if(fleetDriver == null){

                }
                else{
                    HashMap<Integer,String> lstBillRoutingByCompany = new HashMap<>();
                    int orderNumber = 1;
                    for(RoutingPlanDay routing : lstRouting){
                        lstBillRoutingId += routing.getBillRoutingId() + ",";
                        routing.setVehicleId(vehicle.getId());
                        routing.setDriverId(fleetDriver.getId());
                        routing.setOrderNumber(orderNumber);
                        routing.setStatus(StatusRouting.NOT_CONFIRM.getValue());
                        routing.setDriverSosId(null);
                        orderNumber ++;
                    }
                    lstBillRoutingId = lstBillRoutingId.substring(0,lstBillRoutingId.length()-1);
                    assignVehicleForRouting(lstRouting);
                    String lstBillRoutingCode = getLstBillRoutingCode(lstBillRoutingId);

                    ApiManager.callNotificationSOSAfterRouting(fleetDriver.getId(),oldDriver,vehicle.getId());
                }
            }
        }
        else{
            //chuyen sang chia don
        }
        return null;
    }
    private LocationData getCostByLocation(List<LocationData> lstLocation,Double f_lat,Double f_lon, Double t_lat, Double t_lon){
        String timeKey = LocationData.getLocationCode(f_lat, f_lon,
                t_lat, t_lon); //"F" + from.getCode() + "-T" + to.getCode();
        for(LocationData data : lstLocation){
            if(timeKey.equals(data.getCode())){
                System.out.println(data.getCode());
                return  data;
            }
        }
        LocationData data = GoogleDistanceAPI.getDistance(f_lat,f_lon,t_lat,t_lon);
        if(data != null){
            lstLocation.add(data);
        }
        return data;
    }
    private List<RoutingPlanDay> sortRoutingByOrderNumber(List<RoutingPlanDay> lstRouting){
        Collections.sort(lstRouting, new Comparator<RoutingPlanDay>() {
            @Override
            public int compare(RoutingPlanDay o1, RoutingPlanDay o2) {
               return Integer.compare(o1.getOrderNumber(),o2.getOrderNumber());
            }
        });
        return lstRouting;
    }
    private String  getLstBillRoutingCode(String lstBillRouting){
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String billRoutingCode = "";
        String sql = " select id,code from sharevan_bill_routing where id in ( " + lstBillRouting + ") ";
        try {
            PreparedStatement ps = cnn.prepareStatement(sql);
            ResultSet re = ps.executeQuery();
            while (re.next()){
                billRoutingCode += re.getString("code") + ",";
            }
            billRoutingCode = billRoutingCode.substring(0,billRoutingCode.length()-1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return billRoutingCode;
    }

    /*               customer not in warehouse                 */
    @Transactional
    public List<Integer> assignRoutingNoCustomer(Integer[] lstIdRouting, String type, String datePlan){
        List<RoutingPlanDay> lstRouting = getListRoutingByRoutingPlanDayId(lstIdRouting[0],datePlan);
        List<Integer> lstIdNewRouting = new ArrayList<>();
        if(type.equals(BillLadingType.SO.getValue())){
            //lay danh sach tuyen cua xe do

            if(lstRouting != null && !lstRouting.isEmpty()){

                ConfigParameter configParameter = parameterRepository.findByKey(Constants.RETURN_AMOUNT_PERCENT);
                returnAmountPercent = configParameter.getValue();
                Arrays.sort(lstIdRouting);
                HashMap<RoutingPlanDay, RoutingDetail> mapDetailRoutingClaim = new HashMap<>();
                List<RoutingPlanDay> lstRoutingClaim = findRoutingPlanDayClaim(lstIdRouting,lstRouting,mapDetailRoutingClaim);



                //lay het thong tin sao chep cua don su co
                mapDetailRoutingClaim = billPackageRoutingPlanService.getMapBillPackageRoutingPlan(lstIdRouting,lstRoutingClaim,mapDetailRoutingClaim);
                mapDetailRoutingClaim = billPackageRoutingImportService.getMapBillPackageRoutingImport(lstIdRouting,lstRoutingClaim,mapDetailRoutingClaim);
                mapDetailRoutingClaim = billPackageRoutingExportService.getMapBillPackageRoutingExport(lstIdRouting,lstRoutingClaim,mapDetailRoutingClaim);
                mapDetailRoutingClaim = billRoutingService.getMapBillRouting(lstRoutingClaim,mapDetailRoutingClaim);
                mapDetailRoutingClaim = routingService.getMapService(lstIdRouting,lstRoutingClaim,mapDetailRoutingClaim);

                HashMap<RoutingPlanDay,RoutingDetail> mapRoutingReturn = new HashMap<>(); //don moi sinh ra tu don cu
                //sap xep lai theo thu tu
                lstRoutingClaim = sortRoutingByOrderNumber(lstRoutingClaim);

                for(RoutingPlanDay claim : lstRoutingClaim){
                    for(int i = 0; i< lstRouting.size();i++ ){
                        if(claim.getId().equals(lstRouting.get(i).getId())){
                            RoutingDetail detail = mapDetailRoutingClaim.get(claim);
                            RoutingPlanDay routing = lstRouting.get(i);
                            routing.setStatus(StatusRouting.COMPLETED.getValue());
                            routing.setTroubleType(TroubleType.RETURN.getValue());
                            RoutingPlanDay rpd = new RoutingPlanDay(claim);
                            claim.setCapacityActual((double) 0);
                            //chinh lai thong tin cac truong
                            //tao them 1 routing_plan_day moi, diem tra hang la ve DC
                            RoutingPlanDay fromRouting = detail.getFromRoutingPlanDay();
                            rpd.setAssessAmount(rpd.getAssessAmount()*returnAmountPercent/100); //tinh lai gia tien cho RPD moi
                            rpd.setLatitude(fromRouting.getLatitude());
                            rpd.setLongitude(fromRouting.getLongitude());
                            rpd.setAddress(fromRouting.getAddress());
                            rpd.setWarehouseName(fromRouting.getWarehouseName());
                            rpd.setWarehouseId(fromRouting.getWarehouseId());
                            rpd.setDepotId(fromRouting.getDepotId());
                            rpd.setPhone(fromRouting.getPhone());
                            rpd.setToogle(true);
                            rpd.setStatus(StatusRouting.NOT_CONFIRM.getValue());
                            rpd.setTroubleType(TroubleType.RETURN.getValue());
                            rpd.setFromRoutingPlanDayId(claim.getId());
                            rpd.setRoutingPlanDayCode(rpd.getRoutingPlanDayCode() + Constants.SEPARATE_CHAR + Constants.NOCUSTOMER_CODE);
                            RoutingDetail newRoutingDetail = createDetailRoutingReturn(detail);
                            //tinh lai tien cho don hang
                            SharevanBillRouting billRouting = newRoutingDetail.getBillRouting();
                            billRouting.setPriceActual(billRouting.getPriceActual() + rpd.getAssessAmount() - claim.getAssessAmount());
                            billRouting.setTroubleType(TroubleType.RETURN.getValue());
                            mapRoutingReturn.put(rpd,newRoutingDetail);
                            boolean flag = false;
                            for(int j = i +1; j < lstRouting.size(); j++){
                                RoutingPlanDay check = lstRouting.get(j);
                                if(check.getLatitude().equals(rpd.getLatitude()) &&
                                   check.getLongitude().equals(rpd.getLongitude())){

                                    lstRouting.add(j,rpd);
                                    flag = true;
                                    break;
                                }
                            }
                            if(flag == false){
                                lstRouting.add(rpd);
                            }
                            break;
                        }
                    }
                }
                //insert thong tin vao db
                for(RoutingPlanDay r : lstRouting){
                    System.out.println(r.toString());
                    System.out.println("-----------------------------------------------");
                }
                lstIdNewRouting = insertInfoNewRouting(lstRouting,mapRoutingReturn);
                //call thong bao cho driver
            }
        }
        else if(type.equals(BillLadingType.DLP.getValue())){
            //pending
        }

        return lstIdNewRouting;
    }

    public List<Integer> insertInfoNewRouting(List<RoutingPlanDay> lstRouting, HashMap<RoutingPlanDay,RoutingDetail> lstRoutingReturn){
     //   Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        List<Integer> lstIdRouting = new ArrayList<>();
        for(int i = 0; i < lstRouting.size(); i++){
            RoutingPlanDay routing = lstRouting.get(i);
            routing.setOrderNumber(i+1); //chinh lai thu tu tuyen moi
            RoutingDetail detail = lstRoutingReturn.get(routing);
            routing = entityManager.merge(routing);
            System.out.println(routing.getId());
            if(detail != null){
                lstIdRouting.add(routing.getId());
                List<SharevanBillPackageRoutingPlan> lstPackageRoutingPlan = detail.getBillPackageRoutingPlan();
                List<SharevanBillPackageRoutingImport> lstPackageRoutingImport = detail.getBillPackageRoutingImport();
                List<SharevanBillPackageRoutingExport> lstPackageRoutingExport = detail.getBillPackageRoutingExport();
                //update lai routing_plan_day_id cua bill_package_routig_plan/import/export
                for(int j = 0; j< lstPackageRoutingPlan.size(); j++){
                    SharevanBillPackageRoutingPlan packagePlan = lstPackageRoutingPlan.get(j);
                    packagePlan.setRoutingPlanDayId(routing.getId());
                    packagePlan = entityManager.merge(packagePlan);

                    if(lstPackageRoutingImport != null && !lstPackageRoutingImport.isEmpty()){
                        SharevanBillPackageRoutingImport packageImport = lstPackageRoutingImport.get(j);
                        packageImport.setRoutingPlanDayId(routing.getId());
                        packageImport = entityManager.merge(packageImport);
                    }
                    if(lstPackageRoutingExport != null && !lstPackageRoutingExport.isEmpty()){
                        SharevanBillPackageRoutingExport packageExport = lstPackageRoutingExport.get(j);
                        packageExport.setRoutingPlanDayId(routing.getId());
                        packageExport = entityManager.merge(packageExport);
                    }
                }

                for(SharevanRoutingPlanDayService service : detail.getRoutingPlanDayService()){
                    service.setRoutingPlanDayId(routing.getId());
                    service = entityManager.merge(service);
                }
                SharevanBillRouting billRouting = detail.getBillRouting();
                billRouting = entityManager.merge(billRouting);
            }
        }
        return lstIdRouting;
    }
    private RoutingDetail createDetailRoutingReturn(RoutingDetail detail){
        RoutingDetail newRoutingDetail = new RoutingDetail();

        //Tao thogn tin bill package plan/import/export
        List<SharevanBillPackageRoutingPlan> lstNewPackagePlan = new ArrayList<>();
        if(detail.getBillPackageRoutingPlan() != null && !detail.getBillPackageRoutingPlan().isEmpty()){
            for(SharevanBillPackageRoutingPlan packageRoutingPlan: detail.getBillPackageRoutingPlan()){
                lstNewPackagePlan.add(new SharevanBillPackageRoutingPlan(packageRoutingPlan));
            }
            newRoutingDetail.setBillPackageRoutingPlan(lstNewPackagePlan);
        }

        if(detail.getBillPackageRoutingImport() !=null && !detail.getBillPackageRoutingImport().isEmpty()){
            List<SharevanBillPackageRoutingImport> lstNewPackageImport = new ArrayList<>();
            for(SharevanBillPackageRoutingImport packageRoutingImport: detail.getBillPackageRoutingImport()){
                lstNewPackageImport.add(new SharevanBillPackageRoutingImport(packageRoutingImport));
            }
            newRoutingDetail.setBillPackageRoutingImport(lstNewPackageImport);
        }

        if(detail.getBillPackageRoutingExport() != null && !detail.getBillPackageRoutingExport().isEmpty()){
            List<SharevanBillPackageRoutingExport> lstNewPackageExport = new ArrayList<>();
            for(SharevanBillPackageRoutingExport billPackageRoutingExport: detail.getBillPackageRoutingExport()){
                lstNewPackageExport.add(new SharevanBillPackageRoutingExport(billPackageRoutingExport));
            }
            newRoutingDetail.setBillPackageRoutingExport(lstNewPackageExport);
        }

        SharevanBillRouting billRouting = detail.getBillRouting();
        newRoutingDetail.setBillRouting(billRouting);
        //update gia tien trong bill routing
        if(detail.getRoutingPlanDayService() != null && !detail.getRoutingPlanDayService().isEmpty()){
            List<SharevanRoutingPlanDayService> lstNewService = new ArrayList<>();
            for(SharevanRoutingPlanDayService service : detail.getRoutingPlanDayService()){
                lstNewService.add(new SharevanRoutingPlanDayService(service));
            }
            newRoutingDetail.setRoutingPlanDayService(lstNewService);
        }
        return newRoutingDetail;
    }
    private List<RoutingPlanDay> findRoutingPlanDayClaim(Integer[] lstIdRouting,
                                                         List<RoutingPlanDay> lstRouting,
                                                         HashMap<RoutingPlanDay,RoutingDetail> mapRoutingClaim){

        List<RoutingPlanDay> lstRoutingClaim = new ArrayList<>();
        for(RoutingPlanDay routing: lstRouting){
            for(int i = 0; i< lstIdRouting.length; i++){
                if(lstIdRouting[i].equals(routing.getId())){
                    lstRoutingClaim.add(routing);
                    mapRoutingClaim.put(routing,new RoutingDetail());
                }
            }
        }
        for(RoutingPlanDay claim: mapRoutingClaim.keySet()){
            RoutingDetail detail = mapRoutingClaim.get(claim);
            if( claim.getType().equals(WarehouseType.PICKUP.getValue())){
                continue;
            }
         //   if(detail != null){
                for(RoutingPlanDay routing: lstRouting){
                    if(claim.getFromRoutingPlanDayId().equals(routing.getId())){
                        detail.setFromRoutingPlanDay(routing);
                        break;
                    }
                }
          //  }

        }
        return lstRoutingClaim;
    }
    private List<RoutingPlanDay> getListRoutingByRoutingPlanDayId(Integer id, String datePlan){
        Query query  = entityManager.createQuery
                ("select rpd from RoutingPlanDay rpd \n" +
                        "where rpd.datePlan=to_date(:datePlan,'dd/mm/yyyy') \n" +
                        "and rpd.vehicleId = (select vehicleId from RoutingPlanDay where id = :id) " +
                        "and rpd.driverId = (select driverId from RoutingPlanDay where id = :id) \n" +
                        "and rpd.soType = :soType\n" +
                        "and rpd.orderNumber is not null \n" +
                        "order by rpd.orderNumber",RoutingPlanDay.class);

        query.setParameter("datePlan",datePlan);
        query.setParameter("id",id);
        query.setParameter("soType",true);
        List<RoutingPlanDay> result = query.getResultList();
        if(result != null && !result.isEmpty()){
            return result;
        }
        return null;
    }
}
