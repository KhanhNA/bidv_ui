package com.ac.service;

import com.ac.Constants;
import com.ac.entity.*;
import com.ac.enums.TroubleType;
import com.ac.model.RoutingDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
@Service
public class BillRoutingService {
    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    protected BillLadingService billLadingService;

    public List<SharevanBillRouting> getListBillRoutingByLstRoutingId(Integer[] ids){
        String sql = "select routing from SharevanBillRouting routing where routing.id in " ;
        String idInput = "(";
        for(int i =0; i < ids.length; i++){
            idInput += ":id" + i + ",";
        }
        idInput = idInput.substring(0,idInput.length()-1) + ")";
        sql += idInput  ;

        Query query = entityManager.createQuery(sql, SharevanBillRouting.class);
        for(int i =0; i < ids.length; i++){
            query.setParameter("id" + i,ids[i]);
        }
        List<SharevanBillRouting> result = query.getResultList();

        return result;
    }
    public HashMap<RoutingPlanDay, RoutingDetail> getMapBillRouting(List<RoutingPlanDay> lstClaim, HashMap<RoutingPlanDay,RoutingDetail> mapClaimDetail){
        Integer[] ids = new Integer[lstClaim.size() + 3];
        for(int i = 0; i < lstClaim.size(); i++){
            ids[i] = lstClaim.get(i).getBillRoutingId();
        }
        List<SharevanBillRouting> result = getListBillRoutingByLstRoutingId(ids);
       // HashMap<RoutingPlanDay,SharevanBillRouting> mapPackagePlan = new HashMap<>();
        if(result != null && !result.isEmpty()){
            for(RoutingPlanDay routing: lstClaim){
                for(SharevanBillRouting plan : result){
                    if(routing.getBillRoutingId().equals(plan.getId())){
                        RoutingDetail detail = mapClaimDetail.get(routing);
                        detail.setBillRouting(plan);
                        break;
                    }
                }
            }
        }
        return mapClaimDetail;
    }
    @Transactional
    public void createListBillRoutingToday(String datePlan) throws ParseException { //dinh dang DD/MM/YYYY
        List<SharevanBillLading> lstBOL = billLadingService.getLstBOLToday(datePlan);

        if(lstBOL != null && !lstBOL.isEmpty()){
            for(SharevanBillLading bol: lstBOL){
                createBillRoutingByBOL(bol,datePlan);
            }
        }
    }
    public SharevanBillRouting createBillRoutingByBOL(SharevanBillLading bol, String datePlan) throws ParseException {
        SharevanBillRouting br = new SharevanBillRouting();
        br.setCode(bol.getName());
        br.setInsuranceId(bol.getInsuranceId());
        br.setTotalWeight(bol.getTotalWeight());
        br.setTotalAmount(bol.getTotalAmount());
        br.setPriceActual(bol.getTotalAmount());
        br.setRoutingScan(false);
        br.setOrderPackageId(bol.getOrderPackageId());
        br.setTolls(bol.getTolls());
        br.setSurcharge(bol.getSurcharge());
        br.setTotalVolume(bol.getTotalVolume());
        br.setVat(bol.getVat());
        br.setPromotionCode(bol.getPromotionCode());
        br.setReleaseType(bol.getReleaseType());
        br.setTotalParcel(bol.getTotalParcel());
        br.setCompanyId(bol.getCompanyId());
        br.setStartDate(new SimpleDateFormat("dd/MM/yyyy").parse(datePlan));
        br.setSubscribeId(bol.getSubscribeId());
        br.setCycleType(bol.getFrequency().toString());
        br.setWeekChoose(bol.getDayOfWeek().toString());
        br.setAwardCompanyId(bol.getAwardCompanyId());
        //  br.setDayChoose(bol.getDayOfMonth().toString());
        br.setQrCode(bol.getQrCode());
        br.setSblType(bol.getSblType());
        br.setStatusRouting("1");
        br.setDescription(bol.getDescription());
        br.setTroubleType(TroubleType.NORMAL.getValue());
        br.setCreateDate(Constants.getNowUTC());
        br.setOutzoneNum(bol.getOutzoneNum());
        br.setInzoneNum(bol.getInzoneNum());
        br.setCreateBOL(bol.getCreateDate());
        String name = bol.getName() + Constants.SEPARATE_CHAR + datePlan.substring(6)
                + Constants.SEPARATE_CHAR + datePlan.substring(3, 5)
                + Constants.SEPARATE_CHAR + datePlan.substring(0, 2);
        br.setName(name);
        br.setFromBillLadingId(bol.getId());
        br = entityManager.merge(br);


        bol.setLastScan(Constants.getNowUTC());
        bol = entityManager.merge(bol);
        return br;
    }
}
