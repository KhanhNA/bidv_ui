package com.ac.service;

import com.ac.entity.RoutingPlanDay;
import com.ac.entity.SharevanBillPackageRoutingExport;
import com.ac.entity.SharevanBillPackageRoutingPlan;
import com.ac.entity.SharevanRoutingPlanDayService;
import com.ac.model.RoutingDetail;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ServiceRoutingService {
    @PersistenceContext
    protected EntityManager entityManager;
    public List<SharevanRoutingPlanDayService> getListServiceForLstRoutingId(Integer[] ids){
        String sql = "select routing from SharevanRoutingPlanDayService routing where routing.routingPlanDayId in " ;
        String idInput = "(";
        for(int i =0; i < ids.length; i++){
            idInput += ":id" + i + ",";
        }
        idInput = idInput.substring(0,idInput.length()-1) + ")";
        sql += idInput + " order by routing.routingPlanDayId" ;

        Query query = entityManager.createQuery(sql,SharevanRoutingPlanDayService.class);
        for(int i =0; i < ids.length; i++){
            query.setParameter("id" + i,ids[i]);
        }
        List<SharevanRoutingPlanDayService> result = query.getResultList();

        return result;
    }
    public HashMap<RoutingPlanDay,RoutingDetail> getMapService(Integer[] ids, List<RoutingPlanDay> lstClaim, HashMap<RoutingPlanDay, RoutingDetail> mapDetailClaim){
        List<SharevanRoutingPlanDayService> result = getListServiceForLstRoutingId(ids);
        //  HashMap<RoutingPlanDay,List<SharevanRoutingPlanDayService>> mapPackagePlan = new HashMap<>();
        if(result != null && !result.isEmpty()){
            for(RoutingPlanDay routing: lstClaim){
                List<SharevanRoutingPlanDayService> lstService = new ArrayList<>();
                for(SharevanRoutingPlanDayService claim : result){
                    if(claim.getRoutingPlanDayId().equals(routing.getId())){
                        lstService.add(claim);
                    }
                }
                RoutingDetail detail = mapDetailClaim.get(routing);
                detail.setRoutingPlanDayService(lstService);
//                List<SharevanRoutingPlanDayService> lstServiceClaim = detail.getRoutingPlanDayService();
//                lstServiceClaim.addAll(lstService);
            }
        }
        return mapDetailClaim;
    }
}
