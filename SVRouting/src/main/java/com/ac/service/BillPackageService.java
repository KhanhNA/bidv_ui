package com.ac.service;

import com.ac.Constants;
import com.ac.entity.RoutingPlanDay;
import com.ac.entity.SharevanBillPackageRoutingExport;
import com.ac.entity.SharevanBillPackageRoutingImport;
import com.ac.entity.SharevanBillPackageRoutingPlan;
import com.ac.enums.StatusType;
import com.ac.enums.WarehouseType;
import com.mysql.cj.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class BillPackageService {
    @PersistenceContext
    protected EntityManager entityManager;
    protected String qrCode = "";
    @Transactional
    public void createBillPackageRouting(RoutingPlanDay routingPlanDay, String billLadingDetailCode) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        if(cnn != null){
            String sql = "select  bp.id bill_package_id, \n" +
                    " bp.net_weight total_weight,\n" +
                    " bp.quantity_package quantity,\n" +
                    " bp.length,\n" +
                    " bp.width,\n" +
                    " bp.height,\n" +
                    " bp.capacity,\n" +
                    " bp.product_type_id,\n" +
                    " COALESCE(bp.item_name,pt.name) item_name, \n" +
                    " bp.key_map key_map \n" +
                    " from sharevan_bill_package bp \n" +
                    " join sharevan_bill_lading_detail bld on bp.bill_lading_detail_id = bld.id\n" +
                    " join sharevan_product_type pt on bp.product_type_id = pt.id\n" +
                    " where bp.status = 'running'\n" +
                    " and bp.bill_lading_detail_id = ?";
            PreparedStatement ps = cnn.prepareStatement(sql);
            ps.setInt(1,routingPlanDay.getBillLadingDetailId());
            ResultSet re = ps.executeQuery();
            if(re != null){
                ArrayList<SharevanBillPackageRoutingPlan> lstPackagePlan = new ArrayList<>();
                int dem = 0;
                while (re.next()){
                    SharevanBillPackageRoutingPlan bpkPlan = new SharevanBillPackageRoutingPlan();
                    bpkPlan.setBillLadingDetailId(routingPlanDay.getBillLadingDetailId());
                    bpkPlan.setBillPackageId(re.getInt("bill_package_id"));
                    bpkPlan.setQuantity(re.getInt("quantity"));
                    bpkPlan.setLength(re.getDouble("length"));
                    bpkPlan.setHeight(re.getDouble("height"));
                    bpkPlan.setWidth(re.getDouble("width"));
                    bpkPlan.setTotalWeight(re.getDouble("total_weight"));
                    bpkPlan.setProductTypeId(re.getInt("product_type_id"));
                    bpkPlan.setItemName(re.getString("item_name"));
                    bpkPlan.setRoutingPlanDayId(routingPlanDay.getId());
                    double capacity = re.getInt("quantity") * re.getDouble("length")*re.getDouble("height")*re.getDouble("width");
                    bpkPlan.setCapacity(capacity);
                    bpkPlan.setStatus(StatusType.RUNNING.getValue());
                    bpkPlan.setCreateDate(Constants.getNowUTC());
                    bpkPlan.setWriteDate(null);
                    bpkPlan.setKeyMap(re.getString("key_map"));
                    bpkPlan = entityManager.merge(bpkPlan);
                    //sinh ma + gen QR cho bill_package_routing_plan
                    bpkPlan = updateBillPackagePlanCode(bpkPlan, routingPlanDay,billLadingDetailCode);
                    lstPackagePlan.add(bpkPlan);
                    //sinh cac ban ghi bill_package_import(PICKUP), bill_package_export(DELEVERY)
                    addBillPackageImportOrExport(bpkPlan,routingPlanDay);
                    dem += re.getInt("quantity");
                    routingPlanDay.setTotalPackage(dem);
                }
            }
        }
    }
    protected String getQrCodeBillPackageRouting(SharevanBillPackageRoutingPlan pkgPlan, RoutingPlanDay routing){

        Query query  = entityManager.createQuery
                ("SELECT t FROM SharevanBillPackageRoutingPlan t " +
                        " JOIN RoutingPlanDay rpd ON rpd.id = t.routingPlanDayId " +
                        " where " +
                        " rpd.id = :id and\n" +
                        "  t.productTypeId= :productTypeId and t.length = :length and t.width= :width and t.height= :height" +
                        " and t.totalWeight = :totalWeight ",SharevanBillPackageRoutingPlan.class);

        query.setParameter("id",routing.getFromRoutingPlanDayId());
        query.setParameter("productTypeId",pkgPlan.getProductTypeId());
        query.setParameter("length",pkgPlan.getLength());
        query.setParameter("width",pkgPlan.getWidth());
        query.setParameter("height",pkgPlan.getHeight());
        query.setParameter("totalWeight",pkgPlan.getTotalWeight());
        List<SharevanBillPackageRoutingPlan> result = query.getResultList();
        if(result != null && !result.isEmpty()){
            return result.get(0).getQrChar();
        }
        return null;
    }

    protected SharevanBillPackageRoutingPlan updateBillPackagePlanCode(SharevanBillPackageRoutingPlan pkgPlan, RoutingPlanDay routing, String billLadingDetailCode){
        String code = Constants.genCode(Constants.BILL_PACKAGE_PLAN_PRE,pkgPlan.getId(),Constants.padding);
        pkgPlan.setName(code);
        if(WarehouseType.PICKUP.getValue().equals(routing.getType())){
            pkgPlan.setQrChar(code+"_"+billLadingDetailCode);
        }
        else if(WarehouseType.DELIVERY.getValue().equals(routing.getType())){
            pkgPlan.setQrChar(getQrCodeBillPackageRouting(pkgPlan,routing));
        }
        pkgPlan = entityManager.merge(pkgPlan);
        return pkgPlan;
    }

    protected void addBillPackageImportOrExport(SharevanBillPackageRoutingPlan bpkPlan,RoutingPlanDay routingPlanDay){
        if(WarehouseType.PICKUP.getValue().equals(routingPlanDay.getType())){
            SharevanBillPackageRoutingImport pkgImport = new SharevanBillPackageRoutingImport();
            pkgImport.setBillLadingDetailId(bpkPlan.getBillLadingDetailId());
            pkgImport.setBillPackageId(bpkPlan.getBillPackageId());
            pkgImport.setProductTypeId(bpkPlan.getProductTypeId());
            pkgImport.setRoutingPackagePlan(bpkPlan.getId());
            pkgImport.setWidth(bpkPlan.getWidth());
            pkgImport.setHeight(bpkPlan.getHeight());
            pkgImport.setLength(bpkPlan.getLength());
            pkgImport.setCapacity(bpkPlan.getCapacity());
            pkgImport.setItemName(bpkPlan.getItemName());
            pkgImport.setQuantityImport(bpkPlan.getQuantity());
            pkgImport.setTotalWeight(bpkPlan.getTotalWeight());
            pkgImport.setRoutingPlanDayId(routingPlanDay.getId());
            pkgImport.setCreateDate(Constants.getNowUTC());
            pkgImport.setWriteDate(null);
            pkgImport.setStatus(StatusType.RUNNING.getValue());
            pkgImport.setKeyMap(bpkPlan.getKeyMap());
            entityManager.merge(pkgImport);

        }
        else if(WarehouseType.DELIVERY.getValue().equals(routingPlanDay.getType())){
            SharevanBillPackageRoutingExport pkgExport = new SharevanBillPackageRoutingExport();
            pkgExport.setBillLadingDetailId(bpkPlan.getBillLadingDetailId());
            pkgExport.setBillPackageId(bpkPlan.getBillPackageId());
            pkgExport.setProductTypeId(bpkPlan.getProductTypeId());
            pkgExport.setRoutingPackagePlan(bpkPlan.getId());
            pkgExport.setWidth(bpkPlan.getWidth());
            pkgExport.setHeight(bpkPlan.getHeight());
            pkgExport.setLength(bpkPlan.getLength());
            pkgExport.setItemName(bpkPlan.getItemName());
            pkgExport.setCapacity(bpkPlan.getCapacity());
            pkgExport.setQuantityExport(bpkPlan.getQuantity());
            pkgExport.setTotalWeight(bpkPlan.getTotalWeight());
            pkgExport.setRoutingPlanDayId(routingPlanDay.getId());
            pkgExport.setCreateDate(Constants.getNowUTC());
            pkgExport.setWriteDate(null);
            pkgExport.setStatus(StatusType.RUNNING.getValue());
            pkgExport.setKeyMap(bpkPlan.getKeyMap());
            entityManager.merge(pkgExport);
        }
    }
}
