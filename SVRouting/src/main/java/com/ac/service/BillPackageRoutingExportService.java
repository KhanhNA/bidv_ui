package com.ac.service;

import com.ac.entity.RoutingPlanDay;
import com.ac.entity.SharevanBillPackageRoutingExport;
import com.ac.entity.SharevanBillPackageRoutingImport;
import com.ac.model.RoutingDetail;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Service
public class BillPackageRoutingExportService {
    @PersistenceContext
    protected EntityManager entityManager;

    public List<SharevanBillPackageRoutingExport> getListBillRoutingExportByLstRoutingId(Integer[] ids){
        String sql = "select routing from SharevanBillPackageRoutingExport routing where routing.routingPlanDayId in " ;
        String idInput = "(";
        for(int i =0; i < ids.length; i++){
            idInput += ":id" + i + ",";
        }
        idInput = idInput.substring(0,idInput.length()-1) + ")";
        sql += idInput + " order by routing.routingPlanDayId"; ;

        Query query = entityManager.createQuery(sql,SharevanBillPackageRoutingExport.class);
        for(int i =0; i < ids.length; i++){
            query.setParameter("id" + i,ids[i]);
        }
        List<SharevanBillPackageRoutingExport> result = query.getResultList();

        return result;
    }
    public HashMap<RoutingPlanDay,RoutingDetail> getMapBillPackageRoutingExport(Integer[] ids, List<RoutingPlanDay> lstClaim, HashMap<RoutingPlanDay, RoutingDetail> mapDetailClaim){
        List<SharevanBillPackageRoutingExport> result = getListBillRoutingExportByLstRoutingId(ids);
      //  HashMap<RoutingPlanDay,SharevanBillPackageRoutingExport> mapPackageExport = new HashMap<>();
        if(result != null && !result.isEmpty()){
            for(RoutingPlanDay routing: lstClaim){
                List<SharevanBillPackageRoutingExport> lstNewExport = new ArrayList<>();
                for(SharevanBillPackageRoutingExport plan : result){
                    if(routing.getId().equals(plan.getRoutingPlanDayId())){
                        lstNewExport.add(plan);
//                        RoutingDetail detail = mapDetailClaim.get(routing);
//                        detail.setBillPackageRoutingExport(plan);
//                        break;
                    }
                }
                RoutingDetail detail = mapDetailClaim.get(routing);
                detail.setBillPackageRoutingExport(lstNewExport);
            }
        }
        return mapDetailClaim;
    }
}
