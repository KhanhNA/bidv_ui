package com.ac.repository;

import com.ac.entity.bidding_entity.SharevanCargo;
import com.ac.repository.custom.SharevanCargoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanCargoRepository extends JpaRepository<SharevanCargo, Integer>, JpaSpecificationExecutor<SharevanCargo>, SharevanCargoRepositoryCustom {

}