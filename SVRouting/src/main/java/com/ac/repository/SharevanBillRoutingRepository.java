package com.ac.repository;

import com.ac.entity.ConfigParameter;
import com.ac.entity.SharevanBillRouting;
import com.ac.entity.bidding_entity.SharevanCargo;
import com.ac.repository.custom.SharevanCargoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SharevanBillRoutingRepository extends JpaRepository<SharevanBillRouting, Integer>,
        JpaSpecificationExecutor<SharevanBillRouting>,SharevanCargoRepositoryCustom {

    SharevanBillRouting findByFromBillLadingIdAndStartDate(Integer fromBillLadingId,Date startDate );
}
