package com.ac.repository;

import com.ac.entity.SharevanBillPackageRoutingPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanBillPackageRoutingPlanRepository extends JpaRepository<SharevanBillPackageRoutingPlan, Integer>, JpaSpecificationExecutor<SharevanBillPackageRoutingPlan> {

}