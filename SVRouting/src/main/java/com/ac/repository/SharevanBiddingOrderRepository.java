package com.ac.repository;

import com.ac.entity.bidding_entity.SharevanBiddingOrder;
import com.ac.repository.custom.SharevanBiddingOrderRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanBiddingOrderRepository extends JpaRepository<SharevanBiddingOrder, Integer>, JpaSpecificationExecutor<SharevanBiddingOrder>, SharevanBiddingOrderRepositoryCustom {

}