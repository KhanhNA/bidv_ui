package com.ac.repository;

import com.ac.entity.SharevanBillPackageRoutingExport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanBillPackageRoutingExportRepository extends JpaRepository<SharevanBillPackageRoutingExport, Integer>, JpaSpecificationExecutor<SharevanBillPackageRoutingExport> {

}