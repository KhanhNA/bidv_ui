package com.ac.repository;

import com.ac.entity.WarehouseLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WarehouseLogRepository extends JpaRepository<WarehouseLog, Integer>, JpaSpecificationExecutor<WarehouseLog> {

    List<WarehouseLog> findAll();

    @Query(nativeQuery = true, value = "select * from sharevan_warehouse_log where scan_check = :check order by id desc LIMIT 30 ")
    List<WarehouseLog> getLstWarehouseLogCompute(@Param("check") boolean check);

    @Query(nativeQuery = true, value = "select * from sharevan_warehouse_log where scan_check = :check order by id  LIMIT 100 ")
    List<WarehouseLog> getLstWarehouseLogClean(@Param("check") boolean check);

}