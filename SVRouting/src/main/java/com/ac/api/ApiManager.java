package com.ac.api;

import com.ac.Constants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

public class ApiManager {
    public static  void callApiSocket(String data, String url) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(url);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(data, headers);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
        System.out.println(result.getStatusCode());
    }
    //call notification fleet/DLP/new driver/old driver after assigning routing SOS
    public static void callNotificationSOSAfterRouting(Integer newDriverId, Integer oldDriverId, Integer vehicleId){

        String json = "{\n" +
                "\"jsonrpc\": \"2.0\",\n" +
                "\"params\": {\n" +
                "        \"secret_key\":\"" + Constants.ODOO_KEY +"\",\n" +
                "        \"vehicle_id\": " + vehicleId +",\n" +
                "        \"driver_id\":\"" + oldDriverId +"\",\n" +
                "        \"new_driver_id\":\"" + newDriverId +"\"\n" +
                "}\n" +
                "}";

        try {
            ApiManager.callApiSocket(json, Constants.API_NOTIFICATION_SOS_ROUTING);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    //send notification customer after assigning routing SOS
    public static void callNotificationSOSForCustomerAfterRouting(Integer newDriverId,
                                                                  Integer vehicleId,
                                                                  Integer companyId,
                                                                  String lstBillRouting,
                                                                  String type){
        String title = "Notify change driver, vehicle!";
        String body = "Notify change driver, vehicle";
        String json = "{\n" +
                "\"jsonrpc\": \"2.0\",\n" +
                "\"params\": {\n" +
                "        \"secret_key\":\"" + Constants.ODOO_KEY +"\",\n" +
                "        \"order_code\": [" + lstBillRouting +"],\n" +
                "        \"vehicle_id\":\"" + vehicleId +"\",\n" +
                "        \"new_driver_id\":\"" + newDriverId +"\",\n" +
                "        \"company_id\":\"" + companyId +"\"\n" +
                "        \"type\":\"" + type +"\"\n" +
                "        \"title\":\"" + title +"\"\n" +
                "        \"body\":\"" + body +"\"\n" +
                "}\n" +
                "}";

        try {
            ApiManager.callApiSocket(json, Constants.API_NOTIFICATION_CUSTOMER);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


}
