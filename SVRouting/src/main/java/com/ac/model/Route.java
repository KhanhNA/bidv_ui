package com.ac.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Route {
    private List<Leg> legs;
}
