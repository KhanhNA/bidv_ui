package com.ac.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GoogleDistance {
    private List<Route> routes;
}
