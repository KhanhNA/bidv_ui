package com.ac.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Leg {
    private Data2Point distance;
    private Data2Point duration;
    private String start_address;
    private String end_address;
}
