package com.ac.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Data2Point {
    String text;
    Double value;
}
