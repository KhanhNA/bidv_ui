package com.ac;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Constants {
    public static final String CHANGE_PRICE = "changePriceAction";
    public static final String OVERTIME_BIDDING = "overtimeBiddingAction";
    public static final String BIDDING_TIME = "biddingTime";
    public static final String ROUTING_PLAN_CODE_PRE = "RPD";
    public static final String BILL_PACKAGE_PLAN_PRE = "BPR";
    public static final int padding = 12;
    public static final String ODOO_KEY = "1qazXSW@3edcVFR$5tgbNHY^7ujm<KI*9ol.?:P)";
    public static final String USER_NAME = "0388685501";
    public static final String API_CREATE_BILL_ROUTING = "http://demo.aggregatoricapaci.com:8070/share_van_order/create_bill_routings";
    public static final String API_NOTIFICATION_DRIVER = "http://demo.aggregatoricapaci.com:8070/driver/send_noti";
    public static final String API_NOTIFICATION_WEB_USER = "http://demo.aggregatoricapaci.com:8070/web/send_noti";
    public static final String API_NOTIFICATION_SOS_ROUTING = "http://demo.aggregatoricapaci.com:8070/web/send_noti_routing";
    public static final String API_NOTIFICATION_CUSTOMER ="http://demo.aggregatoricapaci.com:8070/web/send_noti_customer";
    public static final String RETURN_AMOUNT_PERCENT = "return.amount.percent";
    public static final String SEPARATE_CHAR = "_";
    public static final String NOCUSTOMER_CODE = "NO_CUSTOMER";
    public static String genCode(String prefix, int id, int padding){
        String code = "" + id;
        while (code.length() < padding){
            code = "0" + code;
        }
        return (prefix + code);
    }
    public static Timestamp getNowUTC(){
        return Timestamp.valueOf(LocalDateTime.now(ZoneOffset.UTC));
    }
}
