package com.ac.enums;

public enum BiddingOrderType {
    NOT_APPROVE("0"),
    APPROVED ("1"),
    WAITING("2"),
    CANCEL("-1");
    String value;

    public String getValue() {
        return value;
    }

    BiddingOrderType(String value) {
        this.value = value;
    }
    public static BiddingOrderType  of(String v) throws Exception {
        if(NOT_APPROVE.getValue().equals(v)){
            return NOT_APPROVE;
        }
        else if(APPROVED.getValue().equals(v)){
            return APPROVED;
        }
        else if(WAITING.getValue().equals(v)){
            return WAITING;
        }
        else if(CANCEL.getValue().equals(v)){
            return CANCEL;
        }
        else{
            throw new Exception("WrongValue");
        }
    }
}
