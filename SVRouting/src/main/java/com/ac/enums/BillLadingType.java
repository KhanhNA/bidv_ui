package com.ac.enums;

public enum BillLadingType {
    DLP("DLP"),
    SO("SO");
    protected String value;

    BillLadingType(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    public static BillLadingType of(String value) throws Exception {
        if(DLP.getValue().equals(value)){
            return DLP;
        }
        if(SO.getValue().equals(value)){
            return SO;
        }

        throw new Exception ("Wrong value!");
    }
}
