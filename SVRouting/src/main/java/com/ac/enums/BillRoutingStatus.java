package com.ac.enums;

public enum BillRoutingStatus {
//    [('-1', 'Cancel'), ('0', 'In Claim'),
//            ('1', 'Shipping'), ('2', 'Finished'), ('3', 'Waiting')],
    CANCEL("-1"),
    IN_CLAIM("0"),
    SHIPPING("1"),
    FINISHED("2"),
    WAITING("3");
    private String value;

    BillRoutingStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
