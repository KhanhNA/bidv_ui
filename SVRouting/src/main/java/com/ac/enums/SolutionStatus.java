package com.ac.enums;

public enum SolutionStatus {
    NEW("0"),
    RESOLVED("1"),
    CANCEL("-1");
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    SolutionStatus(String status) {
        this.status = status;
    }

}
