package com.ac.enums;

public enum TroubleType {
//       [('0', 'Normal'),('1', 'Sos'),
//               ('2', 'Retry'), ('3', 'Return')],
    NORMAL("0"),
    SOS("1"),
    RETRY("2"),
    RETURN("3");
    private String value;

    TroubleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public static TroubleType of(String value) throws Exception {
        if(NORMAL.getValue().equals(value)){
            return NORMAL;
        }
        if(SOS.getValue().equals(value)){
            return SOS;
        }
        if(RETRY.getValue().equals(value)){
            return RETRY;
        }
        if(RETURN.getValue().equals(value)){
            return RETURN;
        }
        throw new Exception ("Wrong value!");
    }
}
