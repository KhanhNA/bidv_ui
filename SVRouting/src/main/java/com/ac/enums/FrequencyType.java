package com.ac.enums;

public enum FrequencyType {

    // 1 daily, 2 weekly, 3 monthly.
    //dayOfWeek: 0: cn, 1: thu 2, 2: thu 3, ...
    //dayOfMonth: 1..31
    DAILLY(1), WEEKKY(2),
    MONTHLY(3), EXPRESS(4),ONE_TIME(5);
    //Instance variable
    private Integer value;
    //Constructor to initialize the instance variable
    FrequencyType(Integer v) {
        this.value = v;
    }
    public Integer getType() {
        return this.value;
    }

    public static FrequencyType of(Integer v){
        for (FrequencyType st: FrequencyType.values()){
            if(st.value.equals(v)){
                return st;
            }
        }
        return ONE_TIME;
    }
}
