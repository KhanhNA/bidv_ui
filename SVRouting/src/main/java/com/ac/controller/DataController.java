package com.ac.controller;

import com.ac.entity.LocationData;
import com.ac.entity.RoutingPlanDay;
import com.ac.repository.LocationDataRepository;
import com.ac.service.RoutingPlanDayService;
import com.ac.tasks.GoogleDistanceAPI;
import ns.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/location")
public class DataController {
    @Autowired
    LocationDataRepository dataRepository;

    @Autowired
    RoutingPlanDayService routingPlanDayService;

    @GetMapping("/assign_routing/nocustomer")
    public ResponseEntity<Object> assignRoutingNotCustomer(@RequestParam(required = true) Integer[] lstIdRouting,
                                                           @RequestParam(required = true) String type
    ) {
// Integer[] lstIdRouting = claim.getLstIdRouting();
// String type = claim.getType();
        System.out.println(type);

        LocalDateTime ldt = LocalDateTime.now();
        if (lstIdRouting != null && lstIdRouting.length > 0 && type != null) {
            List<Integer> lstIdNewRouting = routingPlanDayService.assignRoutingNoCustomer(lstIdRouting, type, DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
            return new ResponseEntity<>(lstIdNewRouting, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<RoutingPlanDay>(), HttpStatus.NO_CONTENT);
    }

    @GetMapping
    public ResponseEntity<Object> find(@RequestParam(required = true) double f_lat,
                                       @RequestParam(required = true) double f_lon,
                                       @RequestParam(required = true) double t_lat,
                                       @RequestParam(required = true) double t_lon
    ) {
        List<LocationData> lstLocation = dataRepository.findAll();
        String timeKey = LocationData.getLocationCode(f_lat, f_lon,
                t_lat, t_lon); //"F" + from.getCode() + "-T" + to.getCode();
        for (LocationData data : lstLocation) {
            if (timeKey.equals(data.getCode())) {
                System.out.println(data.getCode());
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        }
        LocationData data = GoogleDistanceAPI.getDistance(f_lat, f_lon, t_lat, t_lon);
        if (data == null) {
            return new ResponseEntity<>(data, HttpStatus.NO_CONTENT);
        }
        System.out.println(data.getCode());
        data = dataRepository.save(data);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
