package com.ns.walletlib;

import com.tsolution.base.TsBaseModel;

import java.util.List;

public interface WalletInterface {
    String getAccountBalance();
    String getTotalPoint();
    String getTotalCoin();
    void onRefresh();
    boolean isLoading();
    List<TsBaseModel> getItems();
}
