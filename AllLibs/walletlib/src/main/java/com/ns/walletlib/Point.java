
package com.ns.walletlib;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.ns.tsutils.TsUtils;
import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Point extends TsBaseModel {

    @SerializedName("accountId")
    @Expose
    private Integer accountId;
    @SerializedName("clientId")
    @Expose
    private Integer clientId;
    @SerializedName("savingsProductId")
    @Expose
    private Integer savingsProductId;
    @SerializedName("savingsProductName")
    @Expose
    private String savingsProductName;
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("currencyName")
    @Expose
    private String currencyName;
    @SerializedName("totalPoint")
    @Expose
    private Double totalPoint;
    public String getTotalPointStr(){
        return TsUtils.formatCurrencyV2(totalPoint);
    }
}
