package com.ns.walletlib.utils;

import android.content.Context;
import android.util.Base64;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ns.tsutils.HttpLogger;
import com.ns.walletlib.SOServiceWallet;
import com.tsolution.base.BaseViewModelV2;
import com.tsolution.base.listener.ResponseResult;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ns.tsutils.TsUtils.checkNotNull;


/**
 * @author：PhamBien
 */
public class HttpHelper {

    public static final String DOMAIN_URL = "http://dev.nextsolutions.com.vn";
    private static volatile HttpHelper mHttpHelper = null;
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CLIENT_ID = "Dcommerce";
    public static final String CLIENT_SECRET = "A31b4c24l3kj35d4AKJQ";
    public static String BASE_LOGIN_URL = DOMAIN_URL + ":9999";
    public static String MOBILE_SERVICE_URL = DOMAIN_URL + ":8234/api/v1/";
    public static String WALLET_SERVICE_URL = "http://dev.nextsolutions.com.vn:9191/payment-gateway/api/v1/";//clients/{walletClientId}/accounts";

    //    private static Retrofit mRetrofit;
//    private static Retrofit mRetrofitLogistic;
    public static Long languageId = 1L;

    public static String TOKEN_DCOM = "";


    private HttpHelper() {
    }

    public static HttpHelper getInstance() {
        if (mHttpHelper == null) {
            synchronized (HttpHelper.class) {
                if (mHttpHelper == null) {
                    mHttpHelper = new HttpHelper();
                }
            }
        }
        return mHttpHelper;
    }


    public static void requestLogin(String userName, String password, Context context,
                                    ResponseResult result) {
        String x = Credentials.basic(CLIENT_ID, CLIENT_SECRET);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("grant_type", "password");
        builder.add("username", userName.trim());
        builder.add("password", password.trim());

        final Request request = new Request.Builder()
                .url(BASE_LOGIN_URL + "/oauth/token").addHeader("origin", "abc")
                .addHeader("Authorization", x)
                .addHeader("Accept", "application/json, text/plain, */*")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .addHeader("Accept-Language", "" + languageId)
                .post(builder.build())
                .build();

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();

        builder1.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                result.onResponse(null, null, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String token = response.body().string();
                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Gson gson = new Gson();
                    Map<String, String> myMap = gson.fromJson(token, type);
                    TOKEN_DCOM = myMap.get("token_type") + " " + myMap.get("access_token");
                    new HttpHelper.Builder(context)
                            .initOkHttp(languageId)
                            .createRetrofit(MOBILE_SERVICE_URL)
                            .build();
                    result.onResponse(null, null, token, null);
                } else {
                    result.onResponse(null, null, null, null);
                }

            }
        });
    }


    public static class Builder {
        private OkHttpClient mOkHttpClient;

        private OkHttpClient.Builder mBuilder;

        private Retrofit mRetrofit;
        private Retrofit mRetrofitLogistic;

        private Context mContext;

        public Builder(Context context) {
            this.mContext = context;
        }

        /**
         * create  OKHttpClient
         *
         * @return Builder
         */
        public Builder initOkHttp(Long languageId) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            if (mBuilder == null) {
                synchronized (HttpHelper.class) {
                    if (mBuilder == null) {
                        Cache cache = new Cache(new File(mContext.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
                        mBuilder = new OkHttpClient.Builder()
                                .cache(cache)
                                .addInterceptor(interceptor)
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(chain -> {
                                    Request request = chain.request();
                                    Request.Builder newRequest = request.newBuilder().addHeader("Authorization", TOKEN_DCOM)
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept-Language", "" + languageId);
                                    return chain.proceed(newRequest.build());
                                });
                        ;
                    }
                }
            }

            return this;
        }

        /**
         * create retrofit
         *
         * @param baseUrl baseUrl
         * @return Builder
         */
        public Builder createRetrofit(String baseUrl) {
            checkNotNull(baseUrl);
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat(DATE_PATTERN_GSON);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl(baseUrl);
            this.mOkHttpClient = mBuilder.build();
            this.mRetrofit = builder.client(mOkHttpClient).build();
            return this;
        }

        /**
         * create retrofit logistic
         *
         * @param baseUrlLogistic baseUrl
         * @return Builder
         */
        public Builder createRetrofitLogistic(String baseUrlLogistic) {
            checkNotNull(baseUrlLogistic);
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat(DATE_PATTERN_GSON);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl(baseUrlLogistic);
            this.mOkHttpClient = mBuilder.build();
            this.mRetrofitLogistic = builder.client(mOkHttpClient).build();
            return this;
        }

        public void build() {
            HttpHelper.getInstance().build(this);
        }

    }

    private void build(Builder builder) {
        checkNotNull(builder);
        checkNotNull(builder.mBuilder);
        checkNotNull(builder.mOkHttpClient);
        checkNotNull(builder.mRetrofit);
        BaseViewModelV2.setApiService(builder.mRetrofit, SOServiceWallet.class, true);
    }
    public static Retrofit getWalletRetrofit(String username, String password, Context context, Long languageId){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        String authen = String.format("Basic %s", Base64.encodeToString(
                String.format("%s:%s", username, password).getBytes(), Base64.DEFAULT)).replaceAll("(\\r|\\n|\\r\\n)+", "");

        synchronized (HttpHelper.class) {

            Cache cache = new Cache(new File(context.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
            OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(interceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        Request.Builder newRequest = request.newBuilder().addHeader("Authorization", authen)
                                .addHeader("Content-Type", "application/json")
                                .addHeader("Accept-Language", "" + languageId);
                        return chain.proceed(newRequest.build());
                    });
            ;
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat(DATE_PATTERN_GSON);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl(WALLET_SERVICE_URL);

            return builder.client(okClientBuilder.build()).build();
        }
    }
}
