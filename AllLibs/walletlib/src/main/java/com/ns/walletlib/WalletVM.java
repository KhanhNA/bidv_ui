package com.ns.walletlib;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.ns.tsutils.TsUtils;
import com.tsolution.base.ServiceResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class WalletVM extends com.tsolution.base.BaseViewModelV2<SOServiceWallet>{

    public static boolean isLogin = false;
    private ObservableField<WalletDto> walletDto = new ObservableField<>();
    private ObservableField<AccountPayment> accountPayment;
    public ObservableBoolean isLoading = new ObservableBoolean();
    public static final String DOMAIN_URL = "http://dev.nextsolutions.com.vn";
    public static String BASE_PAYMENT = DOMAIN_URL + ":9191";
    private Long merchantId;
    private Long walletClientId;
    String merchantCode;
    SOServiceWallet walletService;
    public WalletVM(@NonNull Application application) {
        super(application);
        accountPayment = new ObservableField<>();

    }

//    public void initClass(Context context, ActionsListener view) {
//        WalletDto walletDto = new WalletDto();
//        walletDto.merchantId = merchantId;
//        this.walletDto.set(walletDto);
//        setUpdateAdappType(0);
//        Retrofit retrofit = HttpHelper.getWalletRetrofit("0978100914", "123113", context, 1L);
//        walletService = retrofit.create(SOService.class);
//        callApiV2(walletService.getWalletAmount(47L), (call1, response, body, t) -> {
//            if(view != null) {
//                view.action(response);
//            }
//        });
//
//
//    }

    private <Response> void getWalletSuccess(Call<Response> responseCall, retrofit2.Response<Response> responseResponse, Object o, Throwable throwable) {
        System.out.println("abcccccccccccccccccccccccc");
    }

    private void success(Call<ServiceResponse<WalletDto>> serviceResponseCall, Response<ServiceResponse<WalletDto>> serviceResponseResponse, Object o, Throwable throwable) {

        System.out.println("successssssssssssssssss");
    }

    private void success(Context context, ServiceResponse<WalletDto> o) {
        if (o != null && TsUtils.isNotNull(o.getArrData())) {
            List<WalletDto> serviceResponse = o.getArrData();
            walletDto.set(serviceResponse.get(0));
            getWalletHistory(context);
        }
    }



    public void getWalletHistory(Context context) {
        AccountPayment account = accountPayment.get();
        Wallet wallet;
        if(account == null || (( wallet = account.getWallet())== null)){
            return;
        }
//        Wallet wallet = account.getWallet();

        callApiV2(walletService.getSavingsAccount(wallet.getAccountId()), (call1, response, body, t) -> {
            System.out.println("abccccccccccccccccccccccc");
            SavingsAccount savingsAccount = (SavingsAccount)body;
            baseModels.set(savingsAccount.getTransactions());

//            if(view != null) {
//                view.action(call1, response, body, t);
//            }
        });


    }




    private void successWalletHistory(List<WalletHisDto> dto) {
        if (dto != null) {
            setData(dto);
            baseModels.notifyChange();
        }
        isLoading.set(false);
    }

    public void onRefresh() {
        if(getApplication() == null || getApplication().getApplicationContext() == null){
            return;
        }
        isLoading.set(true);

        getWalletHistory(getApplication().getApplicationContext());
    }

    public void setAccountPaymentValue(AccountPayment accountPayment) {
        this.accountPayment.set(accountPayment);
    }


    public void setWallet(Long clientId, String clientAccount) {
        AccountPayment account = accountPayment.get();
        if(account == null){
            account = new AccountPayment();
        }
        account.setClientId(clientId);
        account.setClientAccount(clientAccount);
        accountPayment.set(account);
    }
}
