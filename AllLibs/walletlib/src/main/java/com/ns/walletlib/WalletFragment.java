package com.ns.walletlib;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.tsolution.base.BaseAdapter;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.TsBaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterActionsListener;

public class WalletFragment extends BaseFragment implements AdapterActionsListener {
    private WalletVM walletVM;
    private Long clientId;
    private String clientAccount;

    public WalletFragment(Long clientId, String clientAccount) {
        this.clientId = clientId;
        this.clientAccount = clientAccount;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.layout_item_wallet_his, viewModel, this::onItemClick, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        walletVM = (WalletVM) viewModel;
        recyclerView.setNestedScrollingEnabled(true);
        walletVM.setWallet(clientId, clientAccount);
//        walletVM.getAccountPayment().setValue((AccountPayment) intent.getSerializableExtra("ACCOUNT_PAYMENT"));
//
//        //lấy lịch sử giao dịch của ví
//        walletVM.getWalletHistory(getBaseActivity());
//        walletVM.initClass(getBaseActivity());

//        recyclerView.setNestedScrollingEnabled(true);
//        baseAdapter.registerScroll(recyclerView, this::loadData);
//            walletVM.initClass(this.getContext());

        showDialogInputPassword();


        return binding.getRoot();

    }


    private void showDialogInputPassword() {
        DialogInputWalletPassword login = new DialogInputWalletPassword(walletVM, new DialogInputWalletPassword.CallBack() {
            @Override
            public void callBack(WalletVM vm, String token) {
                System.out.println("dismissssssssssss");
                if(!walletVM.isLogin){
                    return;
                }
                getWalletInfo();
            }
        });
        login.show(getBaseActivity().getSupportFragmentManager(), null);
    }
    private void getWalletInfo(){
        //lấy lịch sử giao dịch của ví
        loadData();
        ((BaseAdapter)recyclerView.getAdapter()).registerScroll(recyclerView, this::loadData);
    }
    public void loadData() {
        this.walletVM.getWalletHistory(getBaseActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toolbar toolbar = binding.getRoot().findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return WalletVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.wallet_history;
    }
    public void onAdapterClicked(View v, AccountTran accountTran){
        System.out.println("onclick");
    }
    public void onItemClick(View view, TsBaseModel baseModel) {
//        Bundle bundle = new Bundle();
//        Intent intent = new Intent(getContext(), OrderDetailActivity.class);
//        OrderV2DTO orderDTO = OrderV2DTO.builder()
//                .orderId(((WalletHisDto) baseModel).orderId)
//                .merchantId(walletVM.getWalletDto().get().merchantId)
//                .build();
//        bundle.putSerializable("BASE_MODEL", orderDTO);
//        intent.putExtras(bundle);
//        if (getContext() != null) {
//            getContext().startActivity(intent);
//        }
//        closeProcess();

    }


}
