package com.ns.walletlib;

import com.ns.tsutils.TsUtils;
import com.tsolution.base.TsBaseModel;

import java.util.Date;

import lombok.Builder;

/**
 * - khi thực hiện order thành công thì sẽ đẩy tiền chiết khấu sang ví
 *
 * @author ts-client01
 * Create at 2019-06-21 11:55
 */
@Builder
public class WalletHisDto extends TsBaseModel {

    public Long walletHisId;

    /**
     *
     */
    public Long orderId;

    /**
     *
     */
    public Double amount;
    public String getAmountStr(){
        return TsUtils.formatCurrency(amount);
    }
    /**
     *
     */
    public Date createDate;
    public String getCreateDateStr(){
        return createDate == null? "": TsUtils.formatDate.format(this.createDate);
    }

    /**
     *
     */
    public Long walletId;
}