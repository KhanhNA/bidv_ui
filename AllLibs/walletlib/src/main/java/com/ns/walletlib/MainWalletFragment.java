package com.ns.walletlib;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.BaseViewModelV2;

public class MainWalletFragment extends BaseFragment {
    private BaseViewModelV2 vm;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Class clazz;
        if(vm.isLogin){
            clazz = WalletFragment.class;
        }else {
            clazz = DialogInputWalletPassword.class;
        }
//        showDialogInputPassword();
        replaceFragments(getBaseActivity(), clazz);
        return binding.getRoot();

    }

    private void replaceFragments(Activity activity, Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment)
                .commit();
    }



    @Override
    public int getLayoutRes() {
        return R.layout.fragment_main_wallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModelV2.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }




}
