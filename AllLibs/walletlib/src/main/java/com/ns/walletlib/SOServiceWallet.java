package com.ns.walletlib;


import com.tsolution.base.ServiceResponse;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SOServiceWallet {
    //------------------------------
    @POST("common/list")
    Flowable<ServiceResponse<WalletDto>> getWallet(@Body WalletDto object, @Query("entityName") String entityName, @Query("page") Integer page, @Query("langId") Long langId);

    @GET("get-order-transfer")
    Flowable<List<WalletHisDto>> getWalletHistories(@Query("merchantId") Long merchantId);

    @GET("clients/{walletClientId}/accounts")
    Flowable<AccountPayment> getWalletAmount(@Path("walletClientId") Long clientId);

    @GET("savingsaccounts/{accountId}")
    Flowable<SavingsAccount> getSavingsAccount(@Path("accountId") Long accountId);
}
