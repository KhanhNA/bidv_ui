package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Mutation;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.User;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class LoginMutation implements Mutation<LoginMutation.Data, LoginMutation.Data, LoginMutation.Variables> {
  public static String OPERATION_DEFINITION = "mutation Login($username: String!, $password: String!) {   login(username: $username, password: $password) {     ...User   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "Login";
    }
  };
  private LoginMutation.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public LoginMutation.Data wrapData(LoginMutation.Data data) {
    return data;
  }
  
  @Override
   public LoginMutation.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<LoginMutation.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "e8c9df591a8f24ffb40f07093001e6f1";
  }
  
  public LoginMutation(@Nonnull String username, @Nonnull String password) {
    Utils.checkNotNull(username, "username == null");
    Utils.checkNotNull(password, "password == null");
    this.variables = new LoginMutation.Variables(username, password);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable User login;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("login", "login", new UnmodifiableMapBuilder<String, Object>(2).put("username", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "username").build()).put("password", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "password").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable User login) {
      this.login = login;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "login=" + login + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.login == null) ? (that.login == null) : this.login.equals(that.login));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (login == null) ? 0 : login.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], login != null ? login.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private User.Mapper loginFieldMapper = new User.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final User login = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<User>() {
                  @Override
                  public User read(ResponseReader reader) {
                    return loginFieldMapper.map(reader);
                  }
                });
        return new Data(login);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable String username;
    private @Nullable String password;
    Builder() {
      
    }
    
    public Builder username(@Nullable String username) {
      this.username = username;
      return this;
    }
    
    public Builder password(@Nullable String password) {
      this.password = password;
      return this;
    }
    
    public LoginMutation build() {
      return new LoginMutation(username, password);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull String username;
    private @Nonnull String password;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public String username() {
      return username;
    }
    
    public String password() {
      return password;
    }
    
    public Variables(@Nonnull String username, @Nonnull String password) {
      this.username = username;
      this.valueMap.put("username", username);
      this.password = password;
      this.valueMap.put("password", password);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeString("username", username);
          writer.writeString("password", password);
        }
      };
    }
  }
  
}

