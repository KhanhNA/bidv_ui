package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import java.lang.Integer;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class User implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable String avatar;
  private @Nullable String banner_url;
  private @Nullable java.util.Date birthday;
  private @Nullable String description;
  private @Nullable String email;
  private @Nullable String first_name;
  private @Nonnull Integer id;
  private @Nullable String jwt;
  private @Nullable java.util.Date lastActiveAt;
  private @Nullable String last_name;
  private @Nullable String username;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("avatar", "avatar", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("banner_url", "banner_url", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("birthday", "birthday", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("description", "description", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("email", "email", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("first_name", "first_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("jwt", "jwt", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("lastActiveAt", "lastActiveAt", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("last_name", "last_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("username", "username", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment User on UserDto {   avatar   banner_url   birthday   description   email   first_name   id   jwt   lastActiveAt   last_name   username }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("UserDto"));
  public User(@Nonnull String __typename, @Nullable String avatar, @Nullable String banner_url, @Nullable java.util.Date birthday, @Nullable String description, @Nullable String email, @Nullable String first_name, @Nonnull Integer id, @Nullable String jwt, @Nullable java.util.Date lastActiveAt, @Nullable String last_name, @Nullable String username) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.avatar = avatar;
    this.banner_url = banner_url;
    this.birthday = birthday;
    this.description = description;
    this.email = email;
    this.first_name = first_name;
    this.id = Utils.checkNotNull(id, "id == null");
    this.jwt = jwt;
    this.lastActiveAt = lastActiveAt;
    this.last_name = last_name;
    this.username = username;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "User{"
        + "__typename=" + __typename + ", "
        + "avatar=" + avatar + ", "
        + "banner_url=" + banner_url + ", "
        + "birthday=" + birthday + ", "
        + "description=" + description + ", "
        + "email=" + email + ", "
        + "first_name=" + first_name + ", "
        + "id=" + id + ", "
        + "jwt=" + jwt + ", "
        + "lastActiveAt=" + lastActiveAt + ", "
        + "last_name=" + last_name + ", "
        + "username=" + username + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public User() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof User) {
      User that = (User) o;
      return this.__typename.equals(that.__typename) && ((this.avatar == null) ? (that.avatar == null) : this.avatar.equals(that.avatar)) && ((this.banner_url == null) ? (that.banner_url == null) : this.banner_url.equals(that.banner_url)) && ((this.birthday == null) ? (that.birthday == null) : this.birthday.equals(that.birthday)) && ((this.description == null) ? (that.description == null) : this.description.equals(that.description)) && ((this.email == null) ? (that.email == null) : this.email.equals(that.email)) && ((this.first_name == null) ? (that.first_name == null) : this.first_name.equals(that.first_name)) && this.id.equals(that.id) && ((this.jwt == null) ? (that.jwt == null) : this.jwt.equals(that.jwt)) && ((this.lastActiveAt == null) ? (that.lastActiveAt == null) : this.lastActiveAt.equals(that.lastActiveAt)) && ((this.last_name == null) ? (that.last_name == null) : this.last_name.equals(that.last_name)) && ((this.username == null) ? (that.username == null) : this.username.equals(that.username));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (avatar == null) ? 0 : avatar.hashCode();
      h *= 1000003;
      h ^= (banner_url == null) ? 0 : banner_url.hashCode();
      h *= 1000003;
      h ^= (birthday == null) ? 0 : birthday.hashCode();
      h *= 1000003;
      h ^= (description == null) ? 0 : description.hashCode();
      h *= 1000003;
      h ^= (email == null) ? 0 : email.hashCode();
      h *= 1000003;
      h ^= (first_name == null) ? 0 : first_name.hashCode();
      h *= 1000003;
      h ^= id.hashCode();
      h *= 1000003;
      h ^= (jwt == null) ? 0 : jwt.hashCode();
      h *= 1000003;
      h ^= (lastActiveAt == null) ? 0 : lastActiveAt.hashCode();
      h *= 1000003;
      h ^= (last_name == null) ? 0 : last_name.hashCode();
      h *= 1000003;
      h ^= (username == null) ? 0 : username.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeString($responseFields[1], avatar != null ? avatar : null);
        writer.writeString($responseFields[2], banner_url != null ? banner_url : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[3], birthday != null ? birthday : null);
        writer.writeString($responseFields[4], description != null ? description : null);
        writer.writeString($responseFields[5], email != null ? email : null);
        writer.writeString($responseFields[6], first_name != null ? first_name : null);
        writer.writeInt($responseFields[7], id);
        writer.writeString($responseFields[8], jwt != null ? jwt : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[9], lastActiveAt != null ? lastActiveAt : null);
        writer.writeString($responseFields[10], last_name != null ? last_name : null);
        writer.writeString($responseFields[11], username != null ? username : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<User> {
    @Override
     public User map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final String avatar = reader.readString($responseFields[1]);
      final String banner_url = reader.readString($responseFields[2]);
      final java.util.Date birthday = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[3]);
      final String description = reader.readString($responseFields[4]);
      final String email = reader.readString($responseFields[5]);
      final String first_name = reader.readString($responseFields[6]);
      final Integer id = reader.readInt($responseFields[7]);
      final String jwt = reader.readString($responseFields[8]);
      final java.util.Date lastActiveAt = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[9]);
      final String last_name = reader.readString($responseFields[10]);
      final String username = reader.readString($responseFields[11]);
      return new User(__typename, avatar, banner_url, birthday, description, email, first_name, id, jwt, lastActiveAt, last_name, username);
    }
  }
  
}

