package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import com.nextsolution.graphql.codegen.dto.Customer;
import com.nextsolution.graphql.codegen.dto.Warehouse;
import com.nextsolution.graphql.codegen.dto.Van;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class RoutingVanDay implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable Integer id;
  private @Nullable java.util.Date date_plan;
  private @Nullable Integer routing_plan_day_type;
  private @Nullable Integer routing_van_id;
  private @Nullable Integer order_number;
  private @Nullable Integer warehouse_id;
  private @Nullable Integer customer_id;
  private @Nullable Integer van_id;
  private @Nullable Integer depot_id;
  private @Nullable Integer capacity_expected;
  private @Nullable Double latitude;
  private @Nullable Double longitude;
  private @Nullable java.util.Date ready_time;
  private @Nullable java.util.Date due_time;
  private @Nullable Integer warehouse_type;
  private @Nullable java.util.Date routing_time;
  private @Nullable Integer status;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable java.util.Date expected_from_time;
  private @Nullable java.util.Date expected_to_time;
  private @Nullable java.util.Date actual_time;
  private @Nullable String routing_plan_code;
  private @Nullable String group_code;
  private @Nullable String previous_id;
  private @Nullable String next_id;
  private @Nullable Customer customer;
  private @Nullable Warehouse warehouse;
  private @Nullable Van van;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("date_plan", "date_plan", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("routing_plan_day_type", "routing_plan_day_type", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("routing_van_id", "routing_van_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("order_number", "order_number", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("warehouse_id", "warehouse_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("customer_id", "customer_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("van_id", "van_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("depot_id", "depot_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("capacity_expected", "capacity_expected", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("latitude", "latitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("longitude", "longitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("ready_time", "ready_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("due_time", "due_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("warehouse_type", "warehouse_type", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("routing_time", "routing_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("expected_from_time", "expected_from_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("expected_to_time", "expected_to_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("actual_time", "actual_time", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("routing_plan_code", "routing_plan_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("group_code", "group_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("previous_id", "previous_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("next_id", "next_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("customer", "customer", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("warehouse", "warehouse", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("van", "van", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment RoutingVanDay on RoutingPlanDayDto {   id   date_plan   routing_plan_day_type   routing_van_id   order_number   warehouse_id   customer_id   van_id   depot_id   capacity_expected   latitude   longitude   ready_time   due_time   warehouse_type   routing_time   status   create_date   create_user   update_date   update_user   expected_from_time   expected_to_time   actual_time   routing_plan_code   group_code   previous_id   next_id   customer {     ...Customer   }   warehouse {     ...Warehouse   }   van {     ...Van   } }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("RoutingPlanDayDto"));
  public RoutingVanDay(@Nonnull String __typename, @Nullable Integer id, @Nullable java.util.Date date_plan, @Nullable Integer routing_plan_day_type, @Nullable Integer routing_van_id, @Nullable Integer order_number, @Nullable Integer warehouse_id, @Nullable Integer customer_id, @Nullable Integer van_id, @Nullable Integer depot_id, @Nullable Integer capacity_expected, @Nullable Double latitude, @Nullable Double longitude, @Nullable java.util.Date ready_time, @Nullable java.util.Date due_time, @Nullable Integer warehouse_type, @Nullable java.util.Date routing_time, @Nullable Integer status, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable java.util.Date expected_from_time, @Nullable java.util.Date expected_to_time, @Nullable java.util.Date actual_time, @Nullable String routing_plan_code, @Nullable String group_code, @Nullable String previous_id, @Nullable String next_id, @Nullable Customer customer, @Nullable Warehouse warehouse, @Nullable Van van) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.id = id;
    this.date_plan = date_plan;
    this.routing_plan_day_type = routing_plan_day_type;
    this.routing_van_id = routing_van_id;
    this.order_number = order_number;
    this.warehouse_id = warehouse_id;
    this.customer_id = customer_id;
    this.van_id = van_id;
    this.depot_id = depot_id;
    this.capacity_expected = capacity_expected;
    this.latitude = latitude;
    this.longitude = longitude;
    this.ready_time = ready_time;
    this.due_time = due_time;
    this.warehouse_type = warehouse_type;
    this.routing_time = routing_time;
    this.status = status;
    this.create_date = create_date;
    this.create_user = create_user;
    this.update_date = update_date;
    this.update_user = update_user;
    this.expected_from_time = expected_from_time;
    this.expected_to_time = expected_to_time;
    this.actual_time = actual_time;
    this.routing_plan_code = routing_plan_code;
    this.group_code = group_code;
    this.previous_id = previous_id;
    this.next_id = next_id;
    this.customer = customer;
    this.warehouse = warehouse;
    this.van = van;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "RoutingVanDay{"
        + "__typename=" + __typename + ", "
        + "id=" + id + ", "
        + "date_plan=" + date_plan + ", "
        + "routing_plan_day_type=" + routing_plan_day_type + ", "
        + "routing_van_id=" + routing_van_id + ", "
        + "order_number=" + order_number + ", "
        + "warehouse_id=" + warehouse_id + ", "
        + "customer_id=" + customer_id + ", "
        + "van_id=" + van_id + ", "
        + "depot_id=" + depot_id + ", "
        + "capacity_expected=" + capacity_expected + ", "
        + "latitude=" + latitude + ", "
        + "longitude=" + longitude + ", "
        + "ready_time=" + ready_time + ", "
        + "due_time=" + due_time + ", "
        + "warehouse_type=" + warehouse_type + ", "
        + "routing_time=" + routing_time + ", "
        + "status=" + status + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "expected_from_time=" + expected_from_time + ", "
        + "expected_to_time=" + expected_to_time + ", "
        + "actual_time=" + actual_time + ", "
        + "routing_plan_code=" + routing_plan_code + ", "
        + "group_code=" + group_code + ", "
        + "previous_id=" + previous_id + ", "
        + "next_id=" + next_id + ", "
        + "customer=" + customer + ", "
        + "warehouse=" + warehouse + ", "
        + "van=" + van + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public RoutingVanDay() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof RoutingVanDay) {
      RoutingVanDay that = (RoutingVanDay) o;
      return this.__typename.equals(that.__typename) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.date_plan == null) ? (that.date_plan == null) : this.date_plan.equals(that.date_plan)) && ((this.routing_plan_day_type == null) ? (that.routing_plan_day_type == null) : this.routing_plan_day_type.equals(that.routing_plan_day_type)) && ((this.routing_van_id == null) ? (that.routing_van_id == null) : this.routing_van_id.equals(that.routing_van_id)) && ((this.order_number == null) ? (that.order_number == null) : this.order_number.equals(that.order_number)) && ((this.warehouse_id == null) ? (that.warehouse_id == null) : this.warehouse_id.equals(that.warehouse_id)) && ((this.customer_id == null) ? (that.customer_id == null) : this.customer_id.equals(that.customer_id)) && ((this.van_id == null) ? (that.van_id == null) : this.van_id.equals(that.van_id)) && ((this.depot_id == null) ? (that.depot_id == null) : this.depot_id.equals(that.depot_id)) && ((this.capacity_expected == null) ? (that.capacity_expected == null) : this.capacity_expected.equals(that.capacity_expected)) && ((this.latitude == null) ? (that.latitude == null) : this.latitude.equals(that.latitude)) && ((this.longitude == null) ? (that.longitude == null) : this.longitude.equals(that.longitude)) && ((this.ready_time == null) ? (that.ready_time == null) : this.ready_time.equals(that.ready_time)) && ((this.due_time == null) ? (that.due_time == null) : this.due_time.equals(that.due_time)) && ((this.warehouse_type == null) ? (that.warehouse_type == null) : this.warehouse_type.equals(that.warehouse_type)) && ((this.routing_time == null) ? (that.routing_time == null) : this.routing_time.equals(that.routing_time)) && ((this.status == null) ? (that.status == null) : this.status.equals(that.status)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.expected_from_time == null) ? (that.expected_from_time == null) : this.expected_from_time.equals(that.expected_from_time)) && ((this.expected_to_time == null) ? (that.expected_to_time == null) : this.expected_to_time.equals(that.expected_to_time)) && ((this.actual_time == null) ? (that.actual_time == null) : this.actual_time.equals(that.actual_time)) && ((this.routing_plan_code == null) ? (that.routing_plan_code == null) : this.routing_plan_code.equals(that.routing_plan_code)) && ((this.group_code == null) ? (that.group_code == null) : this.group_code.equals(that.group_code)) && ((this.previous_id == null) ? (that.previous_id == null) : this.previous_id.equals(that.previous_id)) && ((this.next_id == null) ? (that.next_id == null) : this.next_id.equals(that.next_id)) && ((this.customer == null) ? (that.customer == null) : this.customer.equals(that.customer)) && ((this.warehouse == null) ? (that.warehouse == null) : this.warehouse.equals(that.warehouse)) && ((this.van == null) ? (that.van == null) : this.van.equals(that.van));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (date_plan == null) ? 0 : date_plan.hashCode();
      h *= 1000003;
      h ^= (routing_plan_day_type == null) ? 0 : routing_plan_day_type.hashCode();
      h *= 1000003;
      h ^= (routing_van_id == null) ? 0 : routing_van_id.hashCode();
      h *= 1000003;
      h ^= (order_number == null) ? 0 : order_number.hashCode();
      h *= 1000003;
      h ^= (warehouse_id == null) ? 0 : warehouse_id.hashCode();
      h *= 1000003;
      h ^= (customer_id == null) ? 0 : customer_id.hashCode();
      h *= 1000003;
      h ^= (van_id == null) ? 0 : van_id.hashCode();
      h *= 1000003;
      h ^= (depot_id == null) ? 0 : depot_id.hashCode();
      h *= 1000003;
      h ^= (capacity_expected == null) ? 0 : capacity_expected.hashCode();
      h *= 1000003;
      h ^= (latitude == null) ? 0 : latitude.hashCode();
      h *= 1000003;
      h ^= (longitude == null) ? 0 : longitude.hashCode();
      h *= 1000003;
      h ^= (ready_time == null) ? 0 : ready_time.hashCode();
      h *= 1000003;
      h ^= (due_time == null) ? 0 : due_time.hashCode();
      h *= 1000003;
      h ^= (warehouse_type == null) ? 0 : warehouse_type.hashCode();
      h *= 1000003;
      h ^= (routing_time == null) ? 0 : routing_time.hashCode();
      h *= 1000003;
      h ^= (status == null) ? 0 : status.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (expected_from_time == null) ? 0 : expected_from_time.hashCode();
      h *= 1000003;
      h ^= (expected_to_time == null) ? 0 : expected_to_time.hashCode();
      h *= 1000003;
      h ^= (actual_time == null) ? 0 : actual_time.hashCode();
      h *= 1000003;
      h ^= (routing_plan_code == null) ? 0 : routing_plan_code.hashCode();
      h *= 1000003;
      h ^= (group_code == null) ? 0 : group_code.hashCode();
      h *= 1000003;
      h ^= (previous_id == null) ? 0 : previous_id.hashCode();
      h *= 1000003;
      h ^= (next_id == null) ? 0 : next_id.hashCode();
      h *= 1000003;
      h ^= (customer == null) ? 0 : customer.hashCode();
      h *= 1000003;
      h ^= (warehouse == null) ? 0 : warehouse.hashCode();
      h *= 1000003;
      h ^= (van == null) ? 0 : van.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeInt($responseFields[1], id != null ? id : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[2], date_plan != null ? date_plan : null);
        writer.writeInt($responseFields[3], routing_plan_day_type != null ? routing_plan_day_type : null);
        writer.writeInt($responseFields[4], routing_van_id != null ? routing_van_id : null);
        writer.writeInt($responseFields[5], order_number != null ? order_number : null);
        writer.writeInt($responseFields[6], warehouse_id != null ? warehouse_id : null);
        writer.writeInt($responseFields[7], customer_id != null ? customer_id : null);
        writer.writeInt($responseFields[8], van_id != null ? van_id : null);
        writer.writeInt($responseFields[9], depot_id != null ? depot_id : null);
        writer.writeInt($responseFields[10], capacity_expected != null ? capacity_expected : null);
        writer.writeDouble($responseFields[11], latitude != null ? latitude : null);
        writer.writeDouble($responseFields[12], longitude != null ? longitude : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[13], ready_time != null ? ready_time : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[14], due_time != null ? due_time : null);
        writer.writeInt($responseFields[15], warehouse_type != null ? warehouse_type : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[16], routing_time != null ? routing_time : null);
        writer.writeInt($responseFields[17], status != null ? status : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[18], create_date != null ? create_date : null);
        writer.writeString($responseFields[19], create_user != null ? create_user : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[20], update_date != null ? update_date : null);
        writer.writeString($responseFields[21], update_user != null ? update_user : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[22], expected_from_time != null ? expected_from_time : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[23], expected_to_time != null ? expected_to_time : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[24], actual_time != null ? actual_time : null);
        writer.writeString($responseFields[25], routing_plan_code != null ? routing_plan_code : null);
        writer.writeString($responseFields[26], group_code != null ? group_code : null);
        writer.writeString($responseFields[27], previous_id != null ? previous_id : null);
        writer.writeString($responseFields[28], next_id != null ? next_id : null);
        writer.writeObject($responseFields[29], customer != null ? customer.marshaller() : null);
        writer.writeObject($responseFields[30], warehouse != null ? warehouse.marshaller() : null);
        writer.writeObject($responseFields[31], van != null ? van.marshaller() : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<RoutingVanDay> {
    private Customer.Mapper customerFieldMapper = new Customer.Mapper();
    private Warehouse.Mapper warehouseFieldMapper = new Warehouse.Mapper();
    private Van.Mapper vanFieldMapper = new Van.Mapper();
    @Override
     public RoutingVanDay map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final Integer id = reader.readInt($responseFields[1]);
      final java.util.Date date_plan = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[2]);
      final Integer routing_plan_day_type = reader.readInt($responseFields[3]);
      final Integer routing_van_id = reader.readInt($responseFields[4]);
      final Integer order_number = reader.readInt($responseFields[5]);
      final Integer warehouse_id = reader.readInt($responseFields[6]);
      final Integer customer_id = reader.readInt($responseFields[7]);
      final Integer van_id = reader.readInt($responseFields[8]);
      final Integer depot_id = reader.readInt($responseFields[9]);
      final Integer capacity_expected = reader.readInt($responseFields[10]);
      final Double latitude = reader.readDouble($responseFields[11]);
      final Double longitude = reader.readDouble($responseFields[12]);
      final java.util.Date ready_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[13]);
      final java.util.Date due_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[14]);
      final Integer warehouse_type = reader.readInt($responseFields[15]);
      final java.util.Date routing_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[16]);
      final Integer status = reader.readInt($responseFields[17]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[18]);
      final String create_user = reader.readString($responseFields[19]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[20]);
      final String update_user = reader.readString($responseFields[21]);
      final java.util.Date expected_from_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[22]);
      final java.util.Date expected_to_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[23]);
      final java.util.Date actual_time = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[24]);
      final String routing_plan_code = reader.readString($responseFields[25]);
      final String group_code = reader.readString($responseFields[26]);
      final String previous_id = reader.readString($responseFields[27]);
      final String next_id = reader.readString($responseFields[28]);
      final Customer customer = reader.readObject($responseFields[29], new ResponseReader.ObjectReader<Customer>() {
                @Override
                public Customer read(ResponseReader reader) {
                  return customerFieldMapper.map(reader);
                }
              });
      final Warehouse warehouse = reader.readObject($responseFields[30], new ResponseReader.ObjectReader<Warehouse>() {
                @Override
                public Warehouse read(ResponseReader reader) {
                  return warehouseFieldMapper.map(reader);
                }
              });
      final Van van = reader.readObject($responseFields[31], new ResponseReader.ObjectReader<Van>() {
                @Override
                public Van read(ResponseReader reader) {
                  return vanFieldMapper.map(reader);
                }
              });
      return new RoutingVanDay(__typename, id, date_plan, routing_plan_day_type, routing_van_id, order_number, warehouse_id, customer_id, van_id, depot_id, capacity_expected, latitude, longitude, ready_time, due_time, warehouse_type, routing_time, status, create_date, create_user, update_date, update_user, expected_from_time, expected_to_time, actual_time, routing_plan_code, group_code, previous_id, next_id, customer, warehouse, van);
    }
  }
  
}

