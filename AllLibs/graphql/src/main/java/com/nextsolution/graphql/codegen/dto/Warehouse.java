package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class Warehouse implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable Integer id;
  private @Nullable String code;
  private @Nullable String address;
  private @Nullable Double lat;
  private @Nullable Double lng;
  private @Nullable Integer area;
  private @Nullable String phone;
  private @Nullable Integer status;
  private @Nullable Integer customer_id;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable String name;
  private @Nullable String description;
  private @Nullable java.util.Date approve_date;
  private @Nullable String approve_user;
  private @Nullable Integer organization_id;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("code", "code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address", "address", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("lat", "lat", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("lng", "lng", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("area", "area", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone", "phone", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("customer_id", "customer_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("name", "name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("description", "description", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("approve_date", "approve_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("approve_user", "approve_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("organization_id", "organization_id", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment Warehouse on WarehouseDto {   id   code   address   lat   lng   area   phone   status   customer_id   create_date   create_user   update_date   update_user   name   description   approve_date   approve_user   organization_id }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("WarehouseDto"));
  public Warehouse(@Nonnull String __typename, @Nullable Integer id, @Nullable String code, @Nullable String address, @Nullable Double lat, @Nullable Double lng, @Nullable Integer area, @Nullable String phone, @Nullable Integer status, @Nullable Integer customer_id, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable String name, @Nullable String description, @Nullable java.util.Date approve_date, @Nullable String approve_user, @Nullable Integer organization_id) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.id = id;
    this.code = code;
    this.address = address;
    this.lat = lat;
    this.lng = lng;
    this.area = area;
    this.phone = phone;
    this.status = status;
    this.customer_id = customer_id;
    this.create_date = create_date;
    this.create_user = create_user;
    this.update_date = update_date;
    this.update_user = update_user;
    this.name = name;
    this.description = description;
    this.approve_date = approve_date;
    this.approve_user = approve_user;
    this.organization_id = organization_id;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "Warehouse{"
        + "__typename=" + __typename + ", "
        + "id=" + id + ", "
        + "code=" + code + ", "
        + "address=" + address + ", "
        + "lat=" + lat + ", "
        + "lng=" + lng + ", "
        + "area=" + area + ", "
        + "phone=" + phone + ", "
        + "status=" + status + ", "
        + "customer_id=" + customer_id + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "name=" + name + ", "
        + "description=" + description + ", "
        + "approve_date=" + approve_date + ", "
        + "approve_user=" + approve_user + ", "
        + "organization_id=" + organization_id + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public Warehouse() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Warehouse) {
      Warehouse that = (Warehouse) o;
      return this.__typename.equals(that.__typename) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.code == null) ? (that.code == null) : this.code.equals(that.code)) && ((this.address == null) ? (that.address == null) : this.address.equals(that.address)) && ((this.lat == null) ? (that.lat == null) : this.lat.equals(that.lat)) && ((this.lng == null) ? (that.lng == null) : this.lng.equals(that.lng)) && ((this.area == null) ? (that.area == null) : this.area.equals(that.area)) && ((this.phone == null) ? (that.phone == null) : this.phone.equals(that.phone)) && ((this.status == null) ? (that.status == null) : this.status.equals(that.status)) && ((this.customer_id == null) ? (that.customer_id == null) : this.customer_id.equals(that.customer_id)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.name == null) ? (that.name == null) : this.name.equals(that.name)) && ((this.description == null) ? (that.description == null) : this.description.equals(that.description)) && ((this.approve_date == null) ? (that.approve_date == null) : this.approve_date.equals(that.approve_date)) && ((this.approve_user == null) ? (that.approve_user == null) : this.approve_user.equals(that.approve_user)) && ((this.organization_id == null) ? (that.organization_id == null) : this.organization_id.equals(that.organization_id));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (code == null) ? 0 : code.hashCode();
      h *= 1000003;
      h ^= (address == null) ? 0 : address.hashCode();
      h *= 1000003;
      h ^= (lat == null) ? 0 : lat.hashCode();
      h *= 1000003;
      h ^= (lng == null) ? 0 : lng.hashCode();
      h *= 1000003;
      h ^= (area == null) ? 0 : area.hashCode();
      h *= 1000003;
      h ^= (phone == null) ? 0 : phone.hashCode();
      h *= 1000003;
      h ^= (status == null) ? 0 : status.hashCode();
      h *= 1000003;
      h ^= (customer_id == null) ? 0 : customer_id.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (name == null) ? 0 : name.hashCode();
      h *= 1000003;
      h ^= (description == null) ? 0 : description.hashCode();
      h *= 1000003;
      h ^= (approve_date == null) ? 0 : approve_date.hashCode();
      h *= 1000003;
      h ^= (approve_user == null) ? 0 : approve_user.hashCode();
      h *= 1000003;
      h ^= (organization_id == null) ? 0 : organization_id.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeInt($responseFields[1], id != null ? id : null);
        writer.writeString($responseFields[2], code != null ? code : null);
        writer.writeString($responseFields[3], address != null ? address : null);
        writer.writeDouble($responseFields[4], lat != null ? lat : null);
        writer.writeDouble($responseFields[5], lng != null ? lng : null);
        writer.writeInt($responseFields[6], area != null ? area : null);
        writer.writeString($responseFields[7], phone != null ? phone : null);
        writer.writeInt($responseFields[8], status != null ? status : null);
        writer.writeInt($responseFields[9], customer_id != null ? customer_id : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[10], create_date != null ? create_date : null);
        writer.writeString($responseFields[11], create_user != null ? create_user : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[12], update_date != null ? update_date : null);
        writer.writeString($responseFields[13], update_user != null ? update_user : null);
        writer.writeString($responseFields[14], name != null ? name : null);
        writer.writeString($responseFields[15], description != null ? description : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[16], approve_date != null ? approve_date : null);
        writer.writeString($responseFields[17], approve_user != null ? approve_user : null);
        writer.writeInt($responseFields[18], organization_id != null ? organization_id : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<Warehouse> {
    @Override
     public Warehouse map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final Integer id = reader.readInt($responseFields[1]);
      final String code = reader.readString($responseFields[2]);
      final String address = reader.readString($responseFields[3]);
      final Double lat = reader.readDouble($responseFields[4]);
      final Double lng = reader.readDouble($responseFields[5]);
      final Integer area = reader.readInt($responseFields[6]);
      final String phone = reader.readString($responseFields[7]);
      final Integer status = reader.readInt($responseFields[8]);
      final Integer customer_id = reader.readInt($responseFields[9]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[10]);
      final String create_user = reader.readString($responseFields[11]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[12]);
      final String update_user = reader.readString($responseFields[13]);
      final String name = reader.readString($responseFields[14]);
      final String description = reader.readString($responseFields[15]);
      final java.util.Date approve_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[16]);
      final String approve_user = reader.readString($responseFields[17]);
      final Integer organization_id = reader.readInt($responseFields[18]);
      return new Warehouse(__typename, id, code, address, lat, lng, area, phone, status, customer_id, create_date, create_user, update_date, update_user, name, description, approve_date, approve_user, organization_id);
    }
  }
  
}

