package com.nextsolution.graphql.api;


import com.nextsolution.graphql.codegen.business.GetCustomerByUsernameQuery;
import com.nextsolution.graphql.codegen.business.UpdateUserMutation;

public class CustomerApolloApi {
    public static void getCustomer(String username, ResponseApollo result, Object... params) {
        GetCustomerByUsernameQuery query = GetCustomerByUsernameQuery.builder().username(username).build();
        AWSClientBase.apolloQuery(query, result, params);
    }
    public static void updateUser(String userName, String fcmToken, ResponseApollo result, Object... params) {
        UpdateUserMutation userMutation = UpdateUserMutation.builder().username(userName).fcm_device_token(fcmToken).build();
        AWSClientBase.apolloMutation(userMutation, result, params);
    }

}
