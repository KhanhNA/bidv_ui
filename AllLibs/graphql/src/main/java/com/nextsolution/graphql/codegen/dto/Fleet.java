package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.types.CustomType;
import java.lang.Integer;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class Fleet implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable String address1;
  private @Nullable String address2;
  private @Nullable String address_registration;
  private @Nullable String business_registration;
  private @Nullable String city;
  private @Nullable String contact_name;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable String email;
  private @Nullable String fax;
  private @Nullable String fleet_code;
  private @Nullable String fleet_name;
  private @Nullable Integer id;
  private @Nullable Integer parent_id;
  private @Nullable String phone1;
  private @Nullable String phone2;
  private @Nullable String postal_code;
  private @Nullable Integer status;
  private @Nullable String tax_code;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable String website;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address1", "address1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address2", "address2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address_registration", "address_registration", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("business_registration", "business_registration", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("city", "city", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("contact_name", "contact_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("email", "email", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("fax", "fax", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("fleet_code", "fleet_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("fleet_name", "fleet_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("parent_id", "parent_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone1", "phone1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone2", "phone2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("postal_code", "postal_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("tax_code", "tax_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("website", "website", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment Fleet on FleetDto {   address1   address2   address_registration   business_registration   city   contact_name   create_date   create_user   email   fax   fleet_code   fleet_name   id   parent_id   phone1   phone2   postal_code   status   tax_code   update_date   update_user   website }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("FleetDto"));
  public Fleet(@Nonnull String __typename, @Nullable String address1, @Nullable String address2, @Nullable String address_registration, @Nullable String business_registration, @Nullable String city, @Nullable String contact_name, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable String email, @Nullable String fax, @Nullable String fleet_code, @Nullable String fleet_name, @Nullable Integer id, @Nullable Integer parent_id, @Nullable String phone1, @Nullable String phone2, @Nullable String postal_code, @Nullable Integer status, @Nullable String tax_code, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable String website) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.address1 = address1;
    this.address2 = address2;
    this.address_registration = address_registration;
    this.business_registration = business_registration;
    this.city = city;
    this.contact_name = contact_name;
    this.create_date = create_date;
    this.create_user = create_user;
    this.email = email;
    this.fax = fax;
    this.fleet_code = fleet_code;
    this.fleet_name = fleet_name;
    this.id = id;
    this.parent_id = parent_id;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.postal_code = postal_code;
    this.status = status;
    this.tax_code = tax_code;
    this.update_date = update_date;
    this.update_user = update_user;
    this.website = website;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "Fleet{"
        + "__typename=" + __typename + ", "
        + "address1=" + address1 + ", "
        + "address2=" + address2 + ", "
        + "address_registration=" + address_registration + ", "
        + "business_registration=" + business_registration + ", "
        + "city=" + city + ", "
        + "contact_name=" + contact_name + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "email=" + email + ", "
        + "fax=" + fax + ", "
        + "fleet_code=" + fleet_code + ", "
        + "fleet_name=" + fleet_name + ", "
        + "id=" + id + ", "
        + "parent_id=" + parent_id + ", "
        + "phone1=" + phone1 + ", "
        + "phone2=" + phone2 + ", "
        + "postal_code=" + postal_code + ", "
        + "status=" + status + ", "
        + "tax_code=" + tax_code + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "website=" + website + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public Fleet() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Fleet) {
      Fleet that = (Fleet) o;
      return this.__typename.equals(that.__typename) && ((this.address1 == null) ? (that.address1 == null) : this.address1.equals(that.address1)) && ((this.address2 == null) ? (that.address2 == null) : this.address2.equals(that.address2)) && ((this.address_registration == null) ? (that.address_registration == null) : this.address_registration.equals(that.address_registration)) && ((this.business_registration == null) ? (that.business_registration == null) : this.business_registration.equals(that.business_registration)) && ((this.city == null) ? (that.city == null) : this.city.equals(that.city)) && ((this.contact_name == null) ? (that.contact_name == null) : this.contact_name.equals(that.contact_name)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.email == null) ? (that.email == null) : this.email.equals(that.email)) && ((this.fax == null) ? (that.fax == null) : this.fax.equals(that.fax)) && ((this.fleet_code == null) ? (that.fleet_code == null) : this.fleet_code.equals(that.fleet_code)) && ((this.fleet_name == null) ? (that.fleet_name == null) : this.fleet_name.equals(that.fleet_name)) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.parent_id == null) ? (that.parent_id == null) : this.parent_id.equals(that.parent_id)) && ((this.phone1 == null) ? (that.phone1 == null) : this.phone1.equals(that.phone1)) && ((this.phone2 == null) ? (that.phone2 == null) : this.phone2.equals(that.phone2)) && ((this.postal_code == null) ? (that.postal_code == null) : this.postal_code.equals(that.postal_code)) && ((this.status == null) ? (that.status == null) : this.status.equals(that.status)) && ((this.tax_code == null) ? (that.tax_code == null) : this.tax_code.equals(that.tax_code)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.website == null) ? (that.website == null) : this.website.equals(that.website));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (address1 == null) ? 0 : address1.hashCode();
      h *= 1000003;
      h ^= (address2 == null) ? 0 : address2.hashCode();
      h *= 1000003;
      h ^= (address_registration == null) ? 0 : address_registration.hashCode();
      h *= 1000003;
      h ^= (business_registration == null) ? 0 : business_registration.hashCode();
      h *= 1000003;
      h ^= (city == null) ? 0 : city.hashCode();
      h *= 1000003;
      h ^= (contact_name == null) ? 0 : contact_name.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (email == null) ? 0 : email.hashCode();
      h *= 1000003;
      h ^= (fax == null) ? 0 : fax.hashCode();
      h *= 1000003;
      h ^= (fleet_code == null) ? 0 : fleet_code.hashCode();
      h *= 1000003;
      h ^= (fleet_name == null) ? 0 : fleet_name.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (parent_id == null) ? 0 : parent_id.hashCode();
      h *= 1000003;
      h ^= (phone1 == null) ? 0 : phone1.hashCode();
      h *= 1000003;
      h ^= (phone2 == null) ? 0 : phone2.hashCode();
      h *= 1000003;
      h ^= (postal_code == null) ? 0 : postal_code.hashCode();
      h *= 1000003;
      h ^= (status == null) ? 0 : status.hashCode();
      h *= 1000003;
      h ^= (tax_code == null) ? 0 : tax_code.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (website == null) ? 0 : website.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeString($responseFields[1], address1 != null ? address1 : null);
        writer.writeString($responseFields[2], address2 != null ? address2 : null);
        writer.writeString($responseFields[3], address_registration != null ? address_registration : null);
        writer.writeString($responseFields[4], business_registration != null ? business_registration : null);
        writer.writeString($responseFields[5], city != null ? city : null);
        writer.writeString($responseFields[6], contact_name != null ? contact_name : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[7], create_date != null ? create_date : null);
        writer.writeString($responseFields[8], create_user != null ? create_user : null);
        writer.writeString($responseFields[9], email != null ? email : null);
        writer.writeString($responseFields[10], fax != null ? fax : null);
        writer.writeString($responseFields[11], fleet_code != null ? fleet_code : null);
        writer.writeString($responseFields[12], fleet_name != null ? fleet_name : null);
        writer.writeInt($responseFields[13], id != null ? id : null);
        writer.writeInt($responseFields[14], parent_id != null ? parent_id : null);
        writer.writeString($responseFields[15], phone1 != null ? phone1 : null);
        writer.writeString($responseFields[16], phone2 != null ? phone2 : null);
        writer.writeString($responseFields[17], postal_code != null ? postal_code : null);
        writer.writeInt($responseFields[18], status != null ? status : null);
        writer.writeString($responseFields[19], tax_code != null ? tax_code : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[20], update_date != null ? update_date : null);
        writer.writeString($responseFields[21], update_user != null ? update_user : null);
        writer.writeString($responseFields[22], website != null ? website : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<Fleet> {
    @Override
     public Fleet map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final String address1 = reader.readString($responseFields[1]);
      final String address2 = reader.readString($responseFields[2]);
      final String address_registration = reader.readString($responseFields[3]);
      final String business_registration = reader.readString($responseFields[4]);
      final String city = reader.readString($responseFields[5]);
      final String contact_name = reader.readString($responseFields[6]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[7]);
      final String create_user = reader.readString($responseFields[8]);
      final String email = reader.readString($responseFields[9]);
      final String fax = reader.readString($responseFields[10]);
      final String fleet_code = reader.readString($responseFields[11]);
      final String fleet_name = reader.readString($responseFields[12]);
      final Integer id = reader.readInt($responseFields[13]);
      final Integer parent_id = reader.readInt($responseFields[14]);
      final String phone1 = reader.readString($responseFields[15]);
      final String phone2 = reader.readString($responseFields[16]);
      final String postal_code = reader.readString($responseFields[17]);
      final Integer status = reader.readInt($responseFields[18]);
      final String tax_code = reader.readString($responseFields[19]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[20]);
      final String update_user = reader.readString($responseFields[21]);
      final String website = reader.readString($responseFields[22]);
      return new Fleet(__typename, address1, address2, address_registration, business_registration, city, contact_name, create_date, create_user, email, fax, fleet_code, fleet_name, id, parent_id, phone1, phone2, postal_code, status, tax_code, update_date, update_user, website);
    }
  }
  
}

