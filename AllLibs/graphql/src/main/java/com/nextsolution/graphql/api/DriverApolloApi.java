package com.nextsolution.graphql.api;


import com.nextsolution.graphql.codegen.business.GetStaffVanQuery;
import com.nextsolution.graphql.codegen.business.UpdateUserMutation;

public class DriverApolloApi {
    public static void getStaffVan(String username, String date, ResponseApollo result, Object... params) {
        GetStaffVanQuery query = GetStaffVanQuery.builder().userName(username).date(date).build();
        AWSClientBase.apolloQuery(query, result, params);
    }
    public static void updateUser(String userName, String fcmToken, ResponseApollo result, Object... params) {
        UpdateUserMutation userMutation = UpdateUserMutation.builder().username(userName).fcm_device_token(fcmToken).build();
        AWSClientBase.apolloMutation(userMutation, result, params);
    }

}
