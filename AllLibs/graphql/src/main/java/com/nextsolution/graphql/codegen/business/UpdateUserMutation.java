package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Mutation;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.User;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class UpdateUserMutation implements Mutation<UpdateUserMutation.Data, UpdateUserMutation.Data, UpdateUserMutation.Variables> {
  public static String OPERATION_DEFINITION = "mutation UpdateUser($username: String!, $first_name: String, $last_name: String, $email: String, $password: String, $avatar_url: String, $banner_url: String, $fcm_device_token: String) {   updateUser(username: $username, first_name: $first_name, last_name: $last_name, email: $email, password: $password, avatar_url: $avatar_url, banner_url: $banner_url, fcm_device_token: $fcm_device_token) {     ...User   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "UpdateUser";
    }
  };
  private UpdateUserMutation.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public UpdateUserMutation.Data wrapData(UpdateUserMutation.Data data) {
    return data;
  }
  
  @Override
   public UpdateUserMutation.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<UpdateUserMutation.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "95e6b548105d5fb340a8145fc3843a75";
  }
  
  public UpdateUserMutation(@Nonnull String username, @Nullable String first_name, @Nullable String last_name, @Nullable String email, @Nullable String password, @Nullable String avatar_url, @Nullable String banner_url, @Nullable String fcm_device_token) {
    Utils.checkNotNull(username, "username == null");
    this.variables = new UpdateUserMutation.Variables(username, first_name, last_name, email, password, avatar_url, banner_url, fcm_device_token);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable User updateUser;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("updateUser", "updateUser", new UnmodifiableMapBuilder<String, Object>(8).put("username", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "username").build()).put("first_name", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "first_name").build()).put("last_name", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "last_name").build()).put("email", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "email").build()).put("password", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "password").build()).put("avatar_url", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "avatar_url").build()).put("banner_url", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "banner_url").build()).put("fcm_device_token", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "fcm_device_token").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable User updateUser) {
      this.updateUser = updateUser;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "updateUser=" + updateUser + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.updateUser == null) ? (that.updateUser == null) : this.updateUser.equals(that.updateUser));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (updateUser == null) ? 0 : updateUser.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], updateUser != null ? updateUser.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private User.Mapper updateUserFieldMapper = new User.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final User updateUser = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<User>() {
                  @Override
                  public User read(ResponseReader reader) {
                    return updateUserFieldMapper.map(reader);
                  }
                });
        return new Data(updateUser);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable String username;
    private @Nullable String first_name;
    private @Nullable String last_name;
    private @Nullable String email;
    private @Nullable String password;
    private @Nullable String avatar_url;
    private @Nullable String banner_url;
    private @Nullable String fcm_device_token;
    Builder() {
      
    }
    
    public Builder username(@Nullable String username) {
      this.username = username;
      return this;
    }
    
    public Builder first_name(@Nullable String first_name) {
      this.first_name = first_name;
      return this;
    }
    
    public Builder last_name(@Nullable String last_name) {
      this.last_name = last_name;
      return this;
    }
    
    public Builder email(@Nullable String email) {
      this.email = email;
      return this;
    }
    
    public Builder password(@Nullable String password) {
      this.password = password;
      return this;
    }
    
    public Builder avatar_url(@Nullable String avatar_url) {
      this.avatar_url = avatar_url;
      return this;
    }
    
    public Builder banner_url(@Nullable String banner_url) {
      this.banner_url = banner_url;
      return this;
    }
    
    public Builder fcm_device_token(@Nullable String fcm_device_token) {
      this.fcm_device_token = fcm_device_token;
      return this;
    }
    
    public UpdateUserMutation build() {
      return new UpdateUserMutation(username, first_name, last_name, email, password, avatar_url, banner_url, fcm_device_token);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull String username;
    private @Nonnull String first_name;
    private @Nonnull String last_name;
    private @Nonnull String email;
    private @Nonnull String password;
    private @Nonnull String avatar_url;
    private @Nonnull String banner_url;
    private @Nonnull String fcm_device_token;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public String username() {
      return username;
    }
    
    public String first_name() {
      return first_name;
    }
    
    public String last_name() {
      return last_name;
    }
    
    public String email() {
      return email;
    }
    
    public String password() {
      return password;
    }
    
    public String avatar_url() {
      return avatar_url;
    }
    
    public String banner_url() {
      return banner_url;
    }
    
    public String fcm_device_token() {
      return fcm_device_token;
    }
    
    public Variables(@Nonnull String username, @Nonnull String first_name, @Nonnull String last_name, @Nonnull String email, @Nonnull String password, @Nonnull String avatar_url, @Nonnull String banner_url, @Nonnull String fcm_device_token) {
      this.username = username;
      this.valueMap.put("username", username);
      this.first_name = first_name;
      this.valueMap.put("first_name", first_name);
      this.last_name = last_name;
      this.valueMap.put("last_name", last_name);
      this.email = email;
      this.valueMap.put("email", email);
      this.password = password;
      this.valueMap.put("password", password);
      this.avatar_url = avatar_url;
      this.valueMap.put("avatar_url", avatar_url);
      this.banner_url = banner_url;
      this.valueMap.put("banner_url", banner_url);
      this.fcm_device_token = fcm_device_token;
      this.valueMap.put("fcm_device_token", fcm_device_token);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeString("username", username);
          writer.writeString("first_name", first_name);
          writer.writeString("last_name", last_name);
          writer.writeString("email", email);
          writer.writeString("password", password);
          writer.writeString("avatar_url", avatar_url);
          writer.writeString("banner_url", banner_url);
          writer.writeString("fcm_device_token", fcm_device_token);
        }
      };
    }
  }
  
}

