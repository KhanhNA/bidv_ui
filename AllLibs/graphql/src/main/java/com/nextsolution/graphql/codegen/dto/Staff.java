package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class Staff implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable Integer SSN;
  private @Nullable String address;
  private @Nullable Integer app_param_value_id;
  private @Nullable String avatar_url;
  private @Nullable Integer billing_rate;
  private @Nullable java.util.Date birth_date;
  private @Nullable String country;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable String description;
  private @Nullable String email;
  private @Nullable String first_name;
  private @Nullable Integer fleet_id;
  private @Nullable String full_name;
  private @Nullable Integer gender;
  private @Nullable java.util.Date hire_date;
  private @Nonnull Integer id;
  private @Nullable String id_no;
  private @Nullable String imei;
  private @Nullable Integer labor_rate;
  private @Nullable String last_name;
  private @Nullable Double latitude;
  private @Nullable java.util.Date leave_date;
  private @Nullable String license_note;
  private @Nullable Double longitude;
  private @Nullable String nation;
  private @Nullable Integer object_type;
  private @Nullable String phone;
  private @Nullable Integer point;
  private @Nullable String province;
  private @Nullable String staff_code;
  private @Nullable String state_province;
  private @Nullable Integer status;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable Integer user_id;
  private @Nullable String user_name;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("SSN", "SSN", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address", "address", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("app_param_value_id", "app_param_value_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("avatar_url", "avatar_url", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("billing_rate", "billing_rate", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("birth_date", "birth_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("country", "country", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("description", "description", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("email", "email", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("first_name", "first_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("fleet_id", "fleet_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("full_name", "full_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("gender", "gender", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("hire_date", "hire_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("id_no", "id_no", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("imei", "imei", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("labor_rate", "labor_rate", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("last_name", "last_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("latitude", "latitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("leave_date", "leave_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("license_note", "license_note", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("longitude", "longitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("nation", "nation", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("object_type", "object_type", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone", "phone", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("point", "point", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("province", "province", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("staff_code", "staff_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("state_province", "state_province", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("user_id", "user_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("user_name", "user_name", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment Staff on StaffDto {   SSN   address   app_param_value_id   avatar_url   billing_rate   birth_date   country   create_date   create_user   description   email   first_name   fleet_id   full_name   gender   hire_date   id   id_no   imei   labor_rate   last_name   latitude   leave_date   license_note   longitude   nation   object_type   phone   point   province   staff_code   state_province   status   update_date   update_user   user_id   user_name }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("StaffDto"));
  public Staff(@Nonnull String __typename, @Nullable Integer SSN, @Nullable String address, @Nullable Integer app_param_value_id, @Nullable String avatar_url, @Nullable Integer billing_rate, @Nullable java.util.Date birth_date, @Nullable String country, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable String description, @Nullable String email, @Nullable String first_name, @Nullable Integer fleet_id, @Nullable String full_name, @Nullable Integer gender, @Nullable java.util.Date hire_date, @Nonnull Integer id, @Nullable String id_no, @Nullable String imei, @Nullable Integer labor_rate, @Nullable String last_name, @Nullable Double latitude, @Nullable java.util.Date leave_date, @Nullable String license_note, @Nullable Double longitude, @Nullable String nation, @Nullable Integer object_type, @Nullable String phone, @Nullable Integer point, @Nullable String province, @Nullable String staff_code, @Nullable String state_province, @Nullable Integer status, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable Integer user_id, @Nullable String user_name) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.SSN = SSN;
    this.address = address;
    this.app_param_value_id = app_param_value_id;
    this.avatar_url = avatar_url;
    this.billing_rate = billing_rate;
    this.birth_date = birth_date;
    this.country = country;
    this.create_date = create_date;
    this.create_user = create_user;
    this.description = description;
    this.email = email;
    this.first_name = first_name;
    this.fleet_id = fleet_id;
    this.full_name = full_name;
    this.gender = gender;
    this.hire_date = hire_date;
    this.id = Utils.checkNotNull(id, "id == null");
    this.id_no = id_no;
    this.imei = imei;
    this.labor_rate = labor_rate;
    this.last_name = last_name;
    this.latitude = latitude;
    this.leave_date = leave_date;
    this.license_note = license_note;
    this.longitude = longitude;
    this.nation = nation;
    this.object_type = object_type;
    this.phone = phone;
    this.point = point;
    this.province = province;
    this.staff_code = staff_code;
    this.state_province = state_province;
    this.status = status;
    this.update_date = update_date;
    this.update_user = update_user;
    this.user_id = user_id;
    this.user_name = user_name;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "Staff{"
        + "__typename=" + __typename + ", "
        + "SSN=" + SSN + ", "
        + "address=" + address + ", "
        + "app_param_value_id=" + app_param_value_id + ", "
        + "avatar_url=" + avatar_url + ", "
        + "billing_rate=" + billing_rate + ", "
        + "birth_date=" + birth_date + ", "
        + "country=" + country + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "description=" + description + ", "
        + "email=" + email + ", "
        + "first_name=" + first_name + ", "
        + "fleet_id=" + fleet_id + ", "
        + "full_name=" + full_name + ", "
        + "gender=" + gender + ", "
        + "hire_date=" + hire_date + ", "
        + "id=" + id + ", "
        + "id_no=" + id_no + ", "
        + "imei=" + imei + ", "
        + "labor_rate=" + labor_rate + ", "
        + "last_name=" + last_name + ", "
        + "latitude=" + latitude + ", "
        + "leave_date=" + leave_date + ", "
        + "license_note=" + license_note + ", "
        + "longitude=" + longitude + ", "
        + "nation=" + nation + ", "
        + "object_type=" + object_type + ", "
        + "phone=" + phone + ", "
        + "point=" + point + ", "
        + "province=" + province + ", "
        + "staff_code=" + staff_code + ", "
        + "state_province=" + state_province + ", "
        + "status=" + status + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "user_id=" + user_id + ", "
        + "user_name=" + user_name + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public Staff() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Staff) {
      Staff that = (Staff) o;
      return this.__typename.equals(that.__typename) && ((this.SSN == null) ? (that.SSN == null) : this.SSN.equals(that.SSN)) && ((this.address == null) ? (that.address == null) : this.address.equals(that.address)) && ((this.app_param_value_id == null) ? (that.app_param_value_id == null) : this.app_param_value_id.equals(that.app_param_value_id)) && ((this.avatar_url == null) ? (that.avatar_url == null) : this.avatar_url.equals(that.avatar_url)) && ((this.billing_rate == null) ? (that.billing_rate == null) : this.billing_rate.equals(that.billing_rate)) && ((this.birth_date == null) ? (that.birth_date == null) : this.birth_date.equals(that.birth_date)) && ((this.country == null) ? (that.country == null) : this.country.equals(that.country)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.description == null) ? (that.description == null) : this.description.equals(that.description)) && ((this.email == null) ? (that.email == null) : this.email.equals(that.email)) && ((this.first_name == null) ? (that.first_name == null) : this.first_name.equals(that.first_name)) && ((this.fleet_id == null) ? (that.fleet_id == null) : this.fleet_id.equals(that.fleet_id)) && ((this.full_name == null) ? (that.full_name == null) : this.full_name.equals(that.full_name)) && ((this.gender == null) ? (that.gender == null) : this.gender.equals(that.gender)) && ((this.hire_date == null) ? (that.hire_date == null) : this.hire_date.equals(that.hire_date)) && this.id.equals(that.id) && ((this.id_no == null) ? (that.id_no == null) : this.id_no.equals(that.id_no)) && ((this.imei == null) ? (that.imei == null) : this.imei.equals(that.imei)) && ((this.labor_rate == null) ? (that.labor_rate == null) : this.labor_rate.equals(that.labor_rate)) && ((this.last_name == null) ? (that.last_name == null) : this.last_name.equals(that.last_name)) && ((this.latitude == null) ? (that.latitude == null) : this.latitude.equals(that.latitude)) && ((this.leave_date == null) ? (that.leave_date == null) : this.leave_date.equals(that.leave_date)) && ((this.license_note == null) ? (that.license_note == null) : this.license_note.equals(that.license_note)) && ((this.longitude == null) ? (that.longitude == null) : this.longitude.equals(that.longitude)) && ((this.nation == null) ? (that.nation == null) : this.nation.equals(that.nation)) && ((this.object_type == null) ? (that.object_type == null) : this.object_type.equals(that.object_type)) && ((this.phone == null) ? (that.phone == null) : this.phone.equals(that.phone)) && ((this.point == null) ? (that.point == null) : this.point.equals(that.point)) && ((this.province == null) ? (that.province == null) : this.province.equals(that.province)) && ((this.staff_code == null) ? (that.staff_code == null) : this.staff_code.equals(that.staff_code)) && ((this.state_province == null) ? (that.state_province == null) : this.state_province.equals(that.state_province)) && ((this.status == null) ? (that.status == null) : this.status.equals(that.status)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.user_id == null) ? (that.user_id == null) : this.user_id.equals(that.user_id)) && ((this.user_name == null) ? (that.user_name == null) : this.user_name.equals(that.user_name));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (SSN == null) ? 0 : SSN.hashCode();
      h *= 1000003;
      h ^= (address == null) ? 0 : address.hashCode();
      h *= 1000003;
      h ^= (app_param_value_id == null) ? 0 : app_param_value_id.hashCode();
      h *= 1000003;
      h ^= (avatar_url == null) ? 0 : avatar_url.hashCode();
      h *= 1000003;
      h ^= (billing_rate == null) ? 0 : billing_rate.hashCode();
      h *= 1000003;
      h ^= (birth_date == null) ? 0 : birth_date.hashCode();
      h *= 1000003;
      h ^= (country == null) ? 0 : country.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (description == null) ? 0 : description.hashCode();
      h *= 1000003;
      h ^= (email == null) ? 0 : email.hashCode();
      h *= 1000003;
      h ^= (first_name == null) ? 0 : first_name.hashCode();
      h *= 1000003;
      h ^= (fleet_id == null) ? 0 : fleet_id.hashCode();
      h *= 1000003;
      h ^= (full_name == null) ? 0 : full_name.hashCode();
      h *= 1000003;
      h ^= (gender == null) ? 0 : gender.hashCode();
      h *= 1000003;
      h ^= (hire_date == null) ? 0 : hire_date.hashCode();
      h *= 1000003;
      h ^= id.hashCode();
      h *= 1000003;
      h ^= (id_no == null) ? 0 : id_no.hashCode();
      h *= 1000003;
      h ^= (imei == null) ? 0 : imei.hashCode();
      h *= 1000003;
      h ^= (labor_rate == null) ? 0 : labor_rate.hashCode();
      h *= 1000003;
      h ^= (last_name == null) ? 0 : last_name.hashCode();
      h *= 1000003;
      h ^= (latitude == null) ? 0 : latitude.hashCode();
      h *= 1000003;
      h ^= (leave_date == null) ? 0 : leave_date.hashCode();
      h *= 1000003;
      h ^= (license_note == null) ? 0 : license_note.hashCode();
      h *= 1000003;
      h ^= (longitude == null) ? 0 : longitude.hashCode();
      h *= 1000003;
      h ^= (nation == null) ? 0 : nation.hashCode();
      h *= 1000003;
      h ^= (object_type == null) ? 0 : object_type.hashCode();
      h *= 1000003;
      h ^= (phone == null) ? 0 : phone.hashCode();
      h *= 1000003;
      h ^= (point == null) ? 0 : point.hashCode();
      h *= 1000003;
      h ^= (province == null) ? 0 : province.hashCode();
      h *= 1000003;
      h ^= (staff_code == null) ? 0 : staff_code.hashCode();
      h *= 1000003;
      h ^= (state_province == null) ? 0 : state_province.hashCode();
      h *= 1000003;
      h ^= (status == null) ? 0 : status.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (user_id == null) ? 0 : user_id.hashCode();
      h *= 1000003;
      h ^= (user_name == null) ? 0 : user_name.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeInt($responseFields[1], SSN != null ? SSN : null);
        writer.writeString($responseFields[2], address != null ? address : null);
        writer.writeInt($responseFields[3], app_param_value_id != null ? app_param_value_id : null);
        writer.writeString($responseFields[4], avatar_url != null ? avatar_url : null);
        writer.writeInt($responseFields[5], billing_rate != null ? billing_rate : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[6], birth_date != null ? birth_date : null);
        writer.writeString($responseFields[7], country != null ? country : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[8], create_date != null ? create_date : null);
        writer.writeString($responseFields[9], create_user != null ? create_user : null);
        writer.writeString($responseFields[10], description != null ? description : null);
        writer.writeString($responseFields[11], email != null ? email : null);
        writer.writeString($responseFields[12], first_name != null ? first_name : null);
        writer.writeInt($responseFields[13], fleet_id != null ? fleet_id : null);
        writer.writeString($responseFields[14], full_name != null ? full_name : null);
        writer.writeInt($responseFields[15], gender != null ? gender : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[16], hire_date != null ? hire_date : null);
        writer.writeInt($responseFields[17], id);
        writer.writeString($responseFields[18], id_no != null ? id_no : null);
        writer.writeString($responseFields[19], imei != null ? imei : null);
        writer.writeInt($responseFields[20], labor_rate != null ? labor_rate : null);
        writer.writeString($responseFields[21], last_name != null ? last_name : null);
        writer.writeDouble($responseFields[22], latitude != null ? latitude : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[23], leave_date != null ? leave_date : null);
        writer.writeString($responseFields[24], license_note != null ? license_note : null);
        writer.writeDouble($responseFields[25], longitude != null ? longitude : null);
        writer.writeString($responseFields[26], nation != null ? nation : null);
        writer.writeInt($responseFields[27], object_type != null ? object_type : null);
        writer.writeString($responseFields[28], phone != null ? phone : null);
        writer.writeInt($responseFields[29], point != null ? point : null);
        writer.writeString($responseFields[30], province != null ? province : null);
        writer.writeString($responseFields[31], staff_code != null ? staff_code : null);
        writer.writeString($responseFields[32], state_province != null ? state_province : null);
        writer.writeInt($responseFields[33], status != null ? status : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[34], update_date != null ? update_date : null);
        writer.writeString($responseFields[35], update_user != null ? update_user : null);
        writer.writeInt($responseFields[36], user_id != null ? user_id : null);
        writer.writeString($responseFields[37], user_name != null ? user_name : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<Staff> {
    @Override
     public Staff map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final Integer SSN = reader.readInt($responseFields[1]);
      final String address = reader.readString($responseFields[2]);
      final Integer app_param_value_id = reader.readInt($responseFields[3]);
      final String avatar_url = reader.readString($responseFields[4]);
      final Integer billing_rate = reader.readInt($responseFields[5]);
      final java.util.Date birth_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[6]);
      final String country = reader.readString($responseFields[7]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[8]);
      final String create_user = reader.readString($responseFields[9]);
      final String description = reader.readString($responseFields[10]);
      final String email = reader.readString($responseFields[11]);
      final String first_name = reader.readString($responseFields[12]);
      final Integer fleet_id = reader.readInt($responseFields[13]);
      final String full_name = reader.readString($responseFields[14]);
      final Integer gender = reader.readInt($responseFields[15]);
      final java.util.Date hire_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[16]);
      final Integer id = reader.readInt($responseFields[17]);
      final String id_no = reader.readString($responseFields[18]);
      final String imei = reader.readString($responseFields[19]);
      final Integer labor_rate = reader.readInt($responseFields[20]);
      final String last_name = reader.readString($responseFields[21]);
      final Double latitude = reader.readDouble($responseFields[22]);
      final java.util.Date leave_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[23]);
      final String license_note = reader.readString($responseFields[24]);
      final Double longitude = reader.readDouble($responseFields[25]);
      final String nation = reader.readString($responseFields[26]);
      final Integer object_type = reader.readInt($responseFields[27]);
      final String phone = reader.readString($responseFields[28]);
      final Integer point = reader.readInt($responseFields[29]);
      final String province = reader.readString($responseFields[30]);
      final String staff_code = reader.readString($responseFields[31]);
      final String state_province = reader.readString($responseFields[32]);
      final Integer status = reader.readInt($responseFields[33]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[34]);
      final String update_user = reader.readString($responseFields[35]);
      final Integer user_id = reader.readInt($responseFields[36]);
      final String user_name = reader.readString($responseFields[37]);
      return new Staff(__typename, SSN, address, app_param_value_id, avatar_url, billing_rate, birth_date, country, create_date, create_user, description, email, first_name, fleet_id, full_name, gender, hire_date, id, id_no, imei, labor_rate, last_name, latitude, leave_date, license_note, longitude, nation, object_type, phone, point, province, staff_code, state_province, status, update_date, update_user, user_id, user_name);
    }
  }
  
}

