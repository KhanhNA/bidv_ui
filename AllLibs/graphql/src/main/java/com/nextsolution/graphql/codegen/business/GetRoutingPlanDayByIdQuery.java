package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.RoutingVanDay;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetRoutingPlanDayByIdQuery implements Query<GetRoutingPlanDayByIdQuery.Data, GetRoutingPlanDayByIdQuery.Data, GetRoutingPlanDayByIdQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetRoutingPlanDayById($id: Int!) {   getRoutingPlanDayById(routingPlanDayId: $id) {     ...RoutingVanDay   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetRoutingPlanDayById";
    }
  };
  private GetRoutingPlanDayByIdQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetRoutingPlanDayByIdQuery.Data wrapData(GetRoutingPlanDayByIdQuery.Data data) {
    return data;
  }
  
  @Override
   public GetRoutingPlanDayByIdQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetRoutingPlanDayByIdQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "4b443f1ef7fbb0569af9caab78532783";
  }
  
  public GetRoutingPlanDayByIdQuery(@Nonnull Integer id) {
    Utils.checkNotNull(id, "id == null");
    this.variables = new GetRoutingPlanDayByIdQuery.Variables(id);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable RoutingVanDay getRoutingPlanDayById;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("getRoutingPlanDayById", "getRoutingPlanDayById", new UnmodifiableMapBuilder<String, Object>(1).put("routingPlanDayId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "id").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable RoutingVanDay getRoutingPlanDayById) {
      this.getRoutingPlanDayById = getRoutingPlanDayById;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getRoutingPlanDayById=" + getRoutingPlanDayById + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getRoutingPlanDayById == null) ? (that.getRoutingPlanDayById == null) : this.getRoutingPlanDayById.equals(that.getRoutingPlanDayById));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getRoutingPlanDayById == null) ? 0 : getRoutingPlanDayById.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], getRoutingPlanDayById != null ? getRoutingPlanDayById.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private RoutingVanDay.Mapper getRoutingPlanDayByIdFieldMapper = new RoutingVanDay.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final RoutingVanDay getRoutingPlanDayById = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<RoutingVanDay>() {
                  @Override
                  public RoutingVanDay read(ResponseReader reader) {
                    return getRoutingPlanDayByIdFieldMapper.map(reader);
                  }
                });
        return new Data(getRoutingPlanDayById);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer id;
    Builder() {
      
    }
    
    public Builder id(@Nullable Integer id) {
      this.id = id;
      return this;
    }
    
    public GetRoutingPlanDayByIdQuery build() {
      return new GetRoutingPlanDayByIdQuery(id);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer id;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer id() {
      return id;
    }
    
    public Variables(@Nonnull Integer id) {
      this.id = id;
      this.valueMap.put("id", id);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("id", id);
        }
      };
    }
  }
  
}

