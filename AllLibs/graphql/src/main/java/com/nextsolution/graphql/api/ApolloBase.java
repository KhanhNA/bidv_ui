package com.nextsolution.graphql.api;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.apollographql.apollo.ApolloCall;

import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Mutation;
import com.apollographql.apollo.api.Query;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import com.nextsolution.graphql.codegen.business.LoginMutation;
import com.nextsolution.graphql.codegen.dto.User;
import com.nextsolution.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ApolloBase {
    static ApolloClient apolloClient;
    public static User loginUser;
    public static void requestApolloLogin(String url, MutableLiveData<Throwable> appException, String username, String password,
                                          ResponseApollo result, final Object... params) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder builder = original.newBuilder().method(original.method(), original.body());
                    return chain.proceed(builder.build());
                })
                .build();

        apolloClient = ApolloClient.builder()
                .serverUrl(url)
                .okHttpClient(okHttpClient)
                .build();



        apolloClient.mutate(LoginMutation.builder()
                .username(username)
                .password(password)
                .build()).enqueue(new ApolloCall.Callback<String>() {
            @Override
            public void onResponse(@NotNull com.apollographql.apollo.api.Response<String> response) {
                if (response.data() != null && response.data().getLogin() != null && !response.hasErrors()) {
                    String token = Objects.requireNonNull(response.data().getLogin()).getJwt();
                    if (StringUtils.isNotNullAndNotEmpty(token)) {
                        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(chain -> {
                                    Request request = chain.request();
                                    Request.Builder newRequest = request.newBuilder().addHeader("Authorization", "Bearer " + token);
                                    return chain.proceed(newRequest.build());
                                });
                        apolloClient = ApolloClient.builder()
                                .serverUrl(url)
                                .okHttpClient(okHttpClient.build())
                                .build();
                        //
                        loginUser = response.data().getLogin();

                        result.onResponse(response, null, params);
                    }else{
                        result.onResponse(null, new Throwable(response.errors().toString()), params);
                    }
                }else {
                    result.onResponse(null, null, null, null);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("login", e.getMessage());
                result.onResponse(null, e);
            }
        });
    }

    public static   <D extends Query.Data, T, V extends Query.Variables> void apolloQuery(Query<D, T, V> query, final ResponseApollo result, final Object... params) {
        apolloClient.query(query).enqueue(new ApolloCall.Callback<T>() {
            @Override
            public void onResponse(@NotNull Response<T> response) {
                if (response.hasErrors()) {

                    result.onResponse(null, new Throwable(response.errors().toString()), params);
                } else {
                    result.onResponse(response, null, params);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("query", e.getMessage());
                e.printStackTrace();
                result.onResponse(null, e);
            }
        });
    }

    public static  <D extends Mutation.Data, T, V extends Query.Variables> void apolloMutation(Mutation<D, T, V> query, ResponseApollo result, Object... params) {
        apolloClient.mutate(query).enqueue(new ApolloCall.Callback<T>() {
            @Override
            public void onResponse(@NotNull Response<T> response) {
                if (response.hasErrors()) {
                    result.onResponse(null, new Throwable(response.errors().toString()), params);
                } else {
                    result.onResponse(response, null, params);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("query", e.getMessage());
                result.onResponse(null, e);
            }
        });
    }
}
