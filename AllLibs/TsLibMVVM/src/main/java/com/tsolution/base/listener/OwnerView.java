package com.tsolution.base.listener;


import android.view.View;

import com.tsolution.base.TsBaseModel;

public interface OwnerView extends BaseListener{
    public void onClicked(View view, TsBaseModel baseModel);
}
