package com.tsolution.base;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ServiceResponse<T> {
    @SerializedName("content")
    private List<T> arrData;

    @SerializedName("totalPages")
    private Integer totalPages;

    public List<T> getArrData() {
        return arrData;
    }

    public void setArrData(List<T> arrData) {
        this.arrData = arrData;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
