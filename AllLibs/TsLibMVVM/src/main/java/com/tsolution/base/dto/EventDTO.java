package com.tsolution.base.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class EventDTO {
    private String message;
    private int code;

    public EventDTO() {
    }

    public EventDTO(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
