package ns.vtc.service;

import ns.utils.DateUtils;
import ns.utils.JPAUtility;

import ns.utils.ObjectUtils;
import ns.vtc.entity.*;
import org.hibernate.internal.SessionImpl;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBService {
    private static DBService instance;

    public static DBService getInstance() {
        if (instance == null) {
            instance = new DBService();
        }
        return instance;
    }

    public SolutionDay getSolution(String groupCode, LocalDate planDate){
        HashMap<String, Object> params = new HashMap<>();
//        params.put("groupCode", groupCode);
        params.put("datePlan", DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
        return JPAUtility.getInstance()
                .jpaFindOne("from SolutionDay where " +
                "                   datePlan = TO_date(:datePlan,'dd/mm/yyyy')", params);
    }

    public List<FleetVehicle> getVehicles(){
        return JPAUtility.getInstance().jpaFindAll("from FleetVehicle where capacity is not null", null);
    }
    public List<RoutingPlanDay> getRoutingPlanDay(String groupCode, LocalDate planDate){

        HashMap<String, Object> params = new HashMap<>();
//        params.put("groupCode", groupCode);
        params.put("datePlan", DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
        return JPAUtility.getInstance()
                .jpaFindAll("from RoutingPlanDay where id >= 855 and " +
                "                    datePlan = TO_DATE(:datePlan,'dd/mm/yyyy')", params);
    }

    public List<LocationData> getLocationData(){
        return JPAUtility.getInstance().jpaFindAll("from LocationData", null);
    }

//    public List<ParkingPointData> getParkingPointData(){
//        return JPAUtility.getInstance().jpaFindAll("from ParkingPointData", null);
//    }





    public void  updateRoutingPlan(Long solId, String insertInfo) throws Exception{

        String sqlUpdate = "INSERT INTO sharevan_routing_plan_day (id, vehicle_id, next_id, previous_id, write_date, write_uid, status) VALUES " + insertInfo +
                " ON CONFLICT (id) DO UPDATE set vehicle_id = EXCLUDED.vehicle_id, " +
                "                         next_id = EXCLUDED.next_id, previous_id = EXCLUDED.previous_id," +
                "                         write_date = current_date, write_uid = 2" ;

        JPAUtility.getInstance().executeJdbcUpdate(sqlUpdate, null);

    }

    public Long insertSolutionDay(String groupCode, LocalDate datePlan, Long softScore, Long hardScore) throws Exception {

        List<Object> params = new ArrayList<>();
        params.add(DateUtils.toDate(datePlan));
        params.add(softScore);
        params.add(hardScore);

        String sql = "INSERT INTO solution_day (date_plan, solve_time, soft_score, hard_score, group_code) VALUES(?,current_date,?,?,'" + groupCode + "')" +
                " ON CONFLICT (id) DO UPDATE set solve_time = current_date, soft_score = EXCLUDED.soft_score, " +
                "                           hard_score = EXCLUDED.hard_score, group_code = '" + groupCode + "'";


        return JPAUtility.getInstance().executeJdbcInsert(sql, params);
    }

}
