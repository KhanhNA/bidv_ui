package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="Bill_Lading")
@Data
public class BillLading {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private long id;
  
  private String billLadingCode;
  private long customerId;
  private long insuranceId;
  private double totalWeight;
  private long totalAmount;
  private long billCycleId;
  private long releaseType;
  private long status;
  private java.sql.Timestamp createDate;
  private String createUser;
  private java.sql.Timestamp updateDate;
  private String updateUser;
  private long vat;
  private String promotionCode;
  private long surcharge;
  private long tolls;
  private long totalPayment;
  private double totalVolume;
  private long totalParcel;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getBillLadingCode() {
    return billLadingCode;
  }

  public void setBillLadingCode(String billLadingCode) {
    this.billLadingCode = billLadingCode;
  }


  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(long customerId) {
    this.customerId = customerId;
  }


  public long getInsuranceId() {
    return insuranceId;
  }

  public void setInsuranceId(long insuranceId) {
    this.insuranceId = insuranceId;
  }


  public double getTotalWeight() {
    return totalWeight;
  }

  public void setTotalWeight(double totalWeight) {
    this.totalWeight = totalWeight;
  }


  public long getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(long totalAmount) {
    this.totalAmount = totalAmount;
  }


  public long getBillCycleId() {
    return billCycleId;
  }

  public void setBillCycleId(long billCycleId) {
    this.billCycleId = billCycleId;
  }


  public long getReleaseType() {
    return releaseType;
  }

  public void setReleaseType(long releaseType) {
    this.releaseType = releaseType;
  }


  public long getStatus() {
    return status;
  }

  public void setStatus(long status) {
    this.status = status;
  }


  public java.sql.Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(java.sql.Timestamp createDate) {
    this.createDate = createDate;
  }


  public String getCreateUser() {
    return createUser;
  }

  public void setCreateUser(String createUser) {
    this.createUser = createUser;
  }


  public java.sql.Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(java.sql.Timestamp updateDate) {
    this.updateDate = updateDate;
  }


  public String getUpdateUser() {
    return updateUser;
  }

  public void setUpdateUser(String updateUser) {
    this.updateUser = updateUser;
  }


  public long getVat() {
    return vat;
  }

  public void setVat(long vat) {
    this.vat = vat;
  }


  public String getPromotionCode() {
    return promotionCode;
  }

  public void setPromotionCode(String promotionCode) {
    this.promotionCode = promotionCode;
  }


  public long getSurcharge() {
    return surcharge;
  }

  public void setSurcharge(long surcharge) {
    this.surcharge = surcharge;
  }


  public long getTolls() {
    return tolls;
  }

  public void setTolls(long tolls) {
    this.tolls = tolls;
  }


  public long getTotalPayment() {
    return totalPayment;
  }

  public void setTotalPayment(long totalPayment) {
    this.totalPayment = totalPayment;
  }


  public double getTotalVolume() {
    return totalVolume;
  }

  public void setTotalVolume(double totalVolume) {
    this.totalVolume = totalVolume;
  }


  public long getTotalParcel() {
    return totalParcel;
  }

  public void setTotalParcel(long totalParcel) {
    this.totalParcel = totalParcel;
  }

}
