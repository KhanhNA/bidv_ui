package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="VAN")
@Data
public class Van {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="licence_Plate")
    private String licencePlate;
    @Column(name="location")
    private String location;

    @Column(name="cost")
    private Long cost;

    @Column(name="name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parking_point_id", insertable = false, updatable = false)
    private ParkingPoint parkingPoint;
//    private String driver;
//    private double costCenter;
//    private long maintenanceTemplateId;
//    private double axles;
//    private double tireFrontSize;
//    private double tireFrontPressure;
//    private double tireRearSize;
//    private double tireRearPressure;
//    private long hasImage;
//    private long hasAttachment;
//    private String note;
//    private String warrantyName1;
//    private java.sql.Timestamp warrantyDate1;
//    private double warrantyMeter1;
//    private String warrantyName2;
//    private java.sql.Timestamp warrantyDate2;
//    private double warrantyMeter2;
//    private long vanTypeId;
//    private long appParamValueId;
//    private double vehicleTonage;
//    private String vanInspection;
      private int capacity;
//    private double availableCapacity;
//    private long statusCar;
//    private long statusAvailable;
//    private long fleetId;
//    private String vehicleRegistration;
//    private String description;
//    private java.sql.Timestamp createDate;
//    private String createUser;
//    private String updateUser;
//    private java.sql.Timestamp updateDate;
//    private double bodyLength;
//    private double bodyWidth;
//    private double height;
//    private double wheelbase;
//    private double grossWeight;
//    private double engineSize;
//    private String fuelType;

    @Override
    public String toString() {
        return "Van{" +
                "id=" + id +
                ", licencePlate='" + licencePlate + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
