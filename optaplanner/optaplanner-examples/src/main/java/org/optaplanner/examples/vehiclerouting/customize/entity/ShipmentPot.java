/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.customize.entity;

import lombok.Getter;
import lombok.Setter;
import ns.utils.ObjectUtils;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.AnchorShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariableGraphType;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.customize.types.RefObject;
import org.optaplanner.examples.vehiclerouting.customize.types.WarehouseType;
import org.optaplanner.examples.vehiclerouting.domain.Standstill;
import org.optaplanner.examples.vehiclerouting.domain.VehicleV2;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.solver.DepotAngleCustomerDifficultyWeightFactory;

import java.util.HashMap;
import java.util.Map;

@PlanningEntity(difficultyWeightFactoryClass = DepotAngleCustomerDifficultyWeightFactory.class)
//@XStreamAlias("VrpCustomer")
//@XStreamInclude({
//        TimeWindowedCustomer.class
//})
@Setter
@Getter
public class ShipmentPot extends AbstractPersistable implements ShipmentStandstill, RefObject {


    protected Location location;
    protected Double demand;
    protected String customerCode;
    protected WarehouseType shipType;
    protected Integer depotId;
    protected Integer warehouseId;
    // Planning variables: changes during planning, between score calculations.
    protected ShipmentStandstill previousStandstill;

    // Shadow variables
    protected ShipmentPot nextShipmentPot;
    protected String prevId, nextId;
    protected VehicleV2 vehicle;



    public ShipmentPot() {
    }

    public ShipmentPot(long id, Location location, Double demand, String customerCode,
                       Integer depotId, Integer warehouseId,
                       WarehouseType warehouseType) {
        super(id);
        this.location = location;
        this.demand = demand;
        this.shipType = warehouseType;//demand < 0 ? ShipType.DELIVERY: ShipType.PICKUP;
        this.customerCode = customerCode;
        this.depotId = depotId;
        this.warehouseId = warehouseId;

    }
    public ShipmentPot(long id, Location location, Double demand) {
        super(id);
        this.location = location;
        this.demand = demand;
        this.shipType = demand < 0 ? WarehouseType.DELIVERY: WarehouseType.PICKUP;
        this.customerCode = Long.toString(id);
    }
    @Override
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getDemand() {
        return demand;
    }

    public void setDemand(Double demand) {
        this.demand = demand;
    }
    @Override
    public String getCode() {
        return "W"+warehouseId;
    }

    @Override
    public String getCodeFromId() {
        return "W" + getId();
    }


    @PlanningVariable(valueRangeProviderRefs = {"vehicleRange", "customerRange"},
            graphType = PlanningVariableGraphType.CHAINED)
    public ShipmentStandstill getPreviousStandstill() {
        return previousStandstill;
    }

    public void setPreviousStandstill(ShipmentStandstill previousStandstill) {
        this.previousStandstill = previousStandstill;

    }

    @Override
    public ShipmentPot getNextShipmentPot() {
        return nextShipmentPot;
    }

    @Override
    public void setNextShipmentPot(ShipmentPot nextShipmentPot) {
        this.nextShipmentPot = nextShipmentPot;

    }

    @Override
    @AnchorShadowVariable(sourceVariableName = "previousStandstill")
    public VehicleV2 getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleV2 vehicle) {
        this.vehicle = vehicle;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    /**
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getDistanceFromPreviousStandstill() {
        if (previousStandstill == null) {
            throw new IllegalStateException("This method must not be called when the previousStandstill ("
                    + previousStandstill + ") is not initialized yet.");
        }
        return getDistanceFrom(previousStandstill);
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getDistanceFrom(ShipmentStandstill standstill) {
        return standstill.getLocation().getDistanceTo(location);
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getDistanceTo(Standstill standstill) {
        return location.getDistanceTo(standstill.getLocation());
    }

//    @Override
//    public String toString() {
//        if (location.getName() == null) {
//            return super.toString();
//        }
//        return location.getName();
//    }





    @Override
    public String toString() {
        return "Customer{" +
                "location=" + location +
                ", demand=" + demand +
                ", customerCode='" + customerCode + '\'' +
                ", shipType=" + shipType +
//                ", previousStandstill=" + previousStandstill +
//                ", nextShipmentPot=" + nextShipmentPot +
                ", vehicle=" + vehicle +
                '}';
    }
    @Override
    public String getInfo(){
        return this.toString();
    }

    @Override
    public Location getBaseLocation() {
        return getLocation();
    }

    @Override
    public Map obj2map() throws Exception{
        HashMap<String, Object> value = new HashMap<>();
        return ObjectUtils.obj2Map(this);
    }
}
