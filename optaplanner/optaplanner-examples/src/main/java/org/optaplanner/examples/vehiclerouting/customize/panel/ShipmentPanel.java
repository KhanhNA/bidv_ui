package org.optaplanner.examples.vehiclerouting.customize.panel;

import ns.utils.DateUtils;
import org.optaplanner.examples.vehiclerouting.customize.entity.TimeWindowedShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.types.*;
import org.optaplanner.examples.vehiclerouting.domain.location.AirLocation;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;

import org.optaplanner.examples.vehiclerouting.swingui.VehicleRoutingWorldPanel;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashMap;

public class ShipmentPanel extends JPanel implements ObjCRUD {
    private final Integer columnSize = 2;//2 input
    public final String Const_id = "id";
    public final String Const_readyTime = "readyTime";
    public final String Const_dueTime = "readyTime";
    private TimeWindowedShipmentPot shipmentPot;
    private VehicleRoutingWorldPanel vehiclePanel;
    private static Integer id = -1;
    private boolean isNew;
    private final HashMap<String, Object> initValue = new HashMap<String, Object>() {{
        put("customerCode", "value1");
        put("demand", "value2");
    }};

    public ShipmentPanel(VehicleRoutingWorldPanel vehiclePanel, double latitude, double longitude) throws Exception {
//        serviceDuration, String customerCode, long depotId, long warehouseId
        this(vehiclePanel, new TimeWindowedShipmentPot(--id, new AirLocation(--id, latitude, longitude), 10d,
                LocalDateTime.now().plusHours(2), LocalDateTime.now().plusHours(5), 1, "1", 1, 1, null, null, WarehouseType.PICKUP), true);
    }

    public ShipmentPanel(VehicleRoutingWorldPanel vehiclePanel, RefObject objFocus, boolean isNew) throws Exception {
        GridBagLayout layout = new GridBagLayout();
        this.vehiclePanel = vehiclePanel;
        this.setLayout(new GridBagLayout());
        this.shipmentPot = (TimeWindowedShipmentPot) objFocus;
        GridBagConstraints c = new GridBagConstraints();
        this.isNew = isNew;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        if (shipmentPot == null) {
            shipmentPot = new TimeWindowedShipmentPot();
        }
        String[] type = { "Depot", "Vehicle", "Shipment"};
        addCell(Keys.type, new JComboBox(type), c)
                .addCell(Keys.id, new FormattedTextField("Id", shipmentPot.getId(), Formater.LONG), c)
//                .addCell("readyTime", new FormattedTextField("Ready time", DateUtils.dt2s(shipmentPot.getReadyTime(), null), new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS)), c)
                .addCell(Keys.readyTime, new FormattedTextField("Ready time", new Date(shipmentPot.getReadyTime() * 1000), Formater.DATE), c)
                .addCell(Keys.dueTime, new FormattedTextField("Due time", new Date(shipmentPot.getDueTime() * 1000), Formater.DATE), c)
                .addCell(Keys.serviceDuration, new FormattedTextField("Service duration", shipmentPot.getServiceDuration(), Formater.LONG), c)
                .addCell(Keys.latitude, new FormattedTextField("Latitude", shipmentPot.getLocation().getLatitude(), Formater.LATITUDE), c)
                .addCell(Keys.longitude, new FormattedTextField("Longitude", shipmentPot.getLocation().getLongitude(), Formater.LONGITUDE), c)
                .addCell(Keys.demand, new FormattedTextField("Demand", shipmentPot.getDemand(), Formater.LONG), c);

    }


    @Override
    public void actionOk(JFrame frame, ActionEvent e) {
//        VehicleRoutingSolutionV2 solutionV2 = (VehicleRoutingSolutionV2)this.vehiclePanel;
        FormattedTextField idField = (FormattedTextField)objMap.get(Keys.id);
        FormattedTextField readyTime = (FormattedTextField)objMap.get(Keys.readyTime);

        JComboBox type = (JComboBox)objMap.get(Keys.type);

        System.out.println("OK1111111111" + idField.getText() + "_" + idField.getValue() +
                readyTime.getText() + "_" + readyTime.getValue() + " type:" + type.getSelectedIndex() + "_" + (String)type.getSelectedItem());


//        shipmentPot.setSaved(false);



        shipmentPot.setId((Long) ((FormattedTextField)(objMap.get(Keys.id))).getValue());
        shipmentPot.setReadyTime(DateUtils.toSeconds((Date) (((FormattedTextField)objMap.get(Keys.readyTime))).getValue()));
        shipmentPot.setDueTime(DateUtils.toSeconds((Date) (((FormattedTextField)objMap.get(Keys.dueTime)).getValue())));
//        shipmentPot.setDueTime((Long)(objMap.get(Keys.dueTime)).getValue());
        shipmentPot.setServiceDuration((Long) (((FormattedTextField)objMap.get(Keys.serviceDuration)).getValue()));
        Location location = shipmentPot.getLocation();
        location.setLatitude((Double) (((FormattedTextField)objMap.get(Keys.latitude)).getValue()));
        location.setLongitude((Double) (((FormattedTextField)objMap.get(Keys.longitude)).getValue()));
        if(isNew) {

            vehiclePanel.getVehicleRoutingPanel().insertShipmentPot(shipmentPot);
            vehiclePanel.getVehicleRoutingPanel().getSolution().setSaved(false);

        }else {
            vehiclePanel.updatePanel(vehiclePanel.getVehicleRoutingPanel().getSolution());
        }
    }
}


