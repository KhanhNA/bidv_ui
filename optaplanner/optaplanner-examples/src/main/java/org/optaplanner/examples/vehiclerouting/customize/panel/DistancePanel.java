package org.optaplanner.examples.vehiclerouting.customize.panel;

import org.optaplanner.examples.vehiclerouting.customize.StaticData;
import org.optaplanner.examples.vehiclerouting.customize.entity.TimeWindowedShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.types.*;

import org.optaplanner.examples.vehiclerouting.domain.location.AirLocation;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedDepot;

import org.optaplanner.examples.vehiclerouting.swingui.VehicleRoutingWorldPanel;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalTime;
import java.util.HashMap;

public class DistancePanel extends JPanel implements ObjCRUD {
    private final Integer columnSize = 2;//2 input
    public final String Const_id = "id";
    public final String Const_readyTime = "readyTime";
    public final String Const_dueTime = "readyTime";
    private TimeWindowedShipmentPot shipmentPot;
    private VehicleRoutingWorldPanel vehiclePanel;
    private static long id = -1;
    private boolean isNew;
    private final HashMap<String, Object> initValue = new HashMap<String, Object>() {{
        put("customerCode", "value1");
        put("demand", "value2");
    }};

    public DistancePanel(VehicleRoutingWorldPanel vehiclePanel, double latitude, double longitude) throws Exception {
//        serviceDuration, String customerCode, long depotId, long warehouseId

        this(vehiclePanel, new TimeWindowedDepot(--id, new AirLocation(--id, latitude, longitude),
                LocalTime.now().plusHours(2), LocalTime.now().plusHours(5)), true);
//        TimeWindowedDepot(--id, new AirLocation(--id, latitude, longitude), 10,
//                LocalDateTime.now().plusHours(2), LocalDateTime.now().plusHours(5), 1, "1", 1, 1, null, null), true);
    }

    public DistancePanel(VehicleRoutingWorldPanel vehiclePanel, RefObject objFocus, boolean isNew) throws Exception {
        System.out.println("helooooooooooooooooooooooooo");
        GridBagLayout layout = new GridBagLayout();
        this.vehiclePanel = vehiclePanel;
        this.setLayout(new GridBagLayout());
        this.shipmentPot = (TimeWindowedShipmentPot) objFocus;
        GridBagConstraints c = new GridBagConstraints();
        this.isNew = isNew;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;

        String[] type = {"Depot", "Vehicle", "Shipment"};
        Double cost = StaticData.getCost(shipmentPot.getPreviousStandstill(), shipmentPot);
        Double time = StaticData.getTime(shipmentPot.getPreviousStandstill(), shipmentPot);
        addCell(Keys.from, new FormattedTextField("From",shipmentPot.getPreviousStandstill().getCode(), null), c)
                .addCell(Keys.to, new FormattedTextField("To", shipmentPot.getCode(), null), c)
                .addCell(Keys.to, new FormattedTextField("Cost", cost, Formater.LONG), c)
                .addCell(Keys.to, new FormattedTextField("Time", time, Formater.LONG), c)
        ;
    }


    @Override
    public void actionOk(JFrame frame, ActionEvent e) {
//        VehicleRoutingSolutionV2 solutionV2 = (VehicleRoutingSolutionV2)this.vehiclePanel;
        FormattedTextField idField = (FormattedTextField) objMap.get(Keys.id);
        FormattedTextField readyTime = (FormattedTextField) objMap.get(Keys.readyTime);

        JComboBox type = (JComboBox) objMap.get(Keys.type);

        System.out.println("OK1111111111" + idField.getText() + "_" + idField.getValue() +
                readyTime.getText() + "_" + readyTime.getValue() + " type:" + type.getSelectedIndex() + "_" + (String) type.getSelectedItem());

        vehiclePanel.updatePanel(vehiclePanel.getVehicleRoutingPanel().getSolution());

    }


}


