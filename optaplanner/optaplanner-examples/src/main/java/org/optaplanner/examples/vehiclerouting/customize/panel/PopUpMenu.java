package org.optaplanner.examples.vehiclerouting.customize.panel;

import lombok.Data;
import lombok.SneakyThrows;
import org.optaplanner.examples.vehiclerouting.customize.types.ObjCRUD;
import org.optaplanner.examples.vehiclerouting.customize.types.RefObject;

import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolutionV2;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.swingui.VehicleRoutingWorldPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@Data
public class PopUpMenu extends JPopupMenu implements ActionListener {
    private final String ADD_DEPOT = "Add Depot";
    private final String ADD_VEHICLE = "Add Vehicle";
    private final String ADD_SHIPMENT = "Add Shipment";
    VehicleRoutingWorldPanel vehiclePanel;
    private double latitude;
    private double longitude;

    public PopUpMenu(VehicleRoutingWorldPanel vehiclePanel) {
        this.vehiclePanel = vehiclePanel;
        this.addItem(ADD_DEPOT)
                .addItem(ADD_VEHICLE)
                .addItem(ADD_SHIPMENT);

    }

    private PopUpMenu addItem(String name) {
        JMenuItem menu = new JMenuItem(name);
        menu.addActionListener(this);

        add(menu);
        return this;
    }

    @SneakyThrows
    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        ObjCRUD objFocus = getFocus(e);

        if(objFocus == null) {
            switch (s) {
                case ADD_DEPOT:
                    objFocus = new DepotPanel(vehiclePanel, latitude, longitude);
                    break;
                case ADD_SHIPMENT:
                    objFocus = new ShipmentPanel(vehiclePanel, latitude, longitude);
                    break;
                case ADD_VEHICLE:

                    break;
            }
        }
        if(objFocus != null){
            showPanel(objFocus);
            return;
        }
    }


    private void showPanel(ObjCRUD objFocus) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                try {
                    objFocus.createAndShowGUI();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private ObjCRUD getFocus(ActionEvent e) throws Exception {
        VehicleRoutingSolutionV2 solution = vehiclePanel.getVehicleRoutingPanel().getSolution();
        List shipmentPotList = solution.getShipmentPotList();

        RefObject objFocus;
        ObjCRUD objCRUD;
        objFocus = getDisplayObj(shipmentPotList, latitude, longitude);
        if (objFocus != null) {
            objCRUD = new ShipmentPanel(vehiclePanel, objFocus, false);
            return objCRUD;
        }

        List depotList = solution.getDepotList();
        objFocus = getDisplayObj(depotList, latitude, longitude);

        if (objFocus != null) {
            objCRUD = new DepotPanel(vehiclePanel, objFocus, false);
            return objCRUD;
        }

        return null;
    }


    private RefObject getDisplayObj(List<RefObject> objs, double latitude, double longitude) {
        Location location;

        for (RefObject sp : objs) {

            location = sp.getBaseLocation();
            if ((location.getLatitude() - 5 < latitude) && (location.getLatitude() + 5 > latitude)
                    && (location.getLongitude() - 5 < longitude) && (location.getLongitude() + 5 > longitude)) {
                return sp;

            }
        }
        return null;
    }
}
