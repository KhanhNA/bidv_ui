package org.optaplanner.examples.vehiclerouting.customize.maps;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Data2Point {
    String text;
    Double value;
}
