package org.optaplanner.examples.vehiclerouting.customize.types;

public final class DistanceInfo<L, R> extends Pair<L, R> {

    public static <L, R> DistanceInfo<L, R> of(L left, R right) {
        return new DistanceInfo(left, right);
    }

    public DistanceInfo(L left, R right) {
        super(left, right);
    }

    public R getCost(){
        return this.right;
    }
    public L getMinute(){
        return this.left;
    }


}
