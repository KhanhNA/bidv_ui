package com.ns.odootest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.ns.App;
import com.ns.odoo.OdooV2;
import com.ns.odoo.core.rpc.helper.OdooSessionDto;
import com.ns.odoo.core.rpc.helper.utils.gson.OdooResponseDto;
import com.ns.odoo.core.rpc.helper.utils.gson.OdooResultDto;
import com.ns.odoo.core.rpc.listenersV2.IOdooLoginCallback;
import com.ns.odoo.core.rpc.listenersV2.IOdooResponse;
import com.ns.odoo.core.support.OUser;
import com.ns.res.VehicleDto;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FirstFragment extends Fragment implements IOdooResponse<OdooSessionDto> {
    OdooV2 mOdoo;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                NavHostFragment.findNavController(FirstFragment.this)
//                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
                loginProcess("thonv");


            }
        });
    }
    private void loginProcess(String database) {
        Log.v("", "LoginProcess");
        final String username = "admin";
        final String password = "admin";
        Log.v("", "Processing Self Hosted Server Login");
//        mLoginProcessStatus.setText(OResource.string(OdooLogin.this, R.string.status_logging_in));
//        mOdoo.authenticate(username, password, database, this);
        App app = (App)getActivity().getApplication();
        mOdoo = app.getOdoo();
        mOdoo.authenticate("admin", "admin", "thonv", this);

    }





    @Override
    public void onResponse(OdooSessionDto response, VolleyError error) {
        System.out.println("sucesss odooo:");

        mOdoo.searchRead( "fleet.vehicle", null, null, 0, 0, "", new IOdooResponse<OdooResultDto<VehicleDto>>() {
            @Override
            public void onResponse(OdooResultDto<VehicleDto> response, VolleyError error) {
                System.out.println("ok");
            }

        });
    }
}
