package com.ns;

import com.android.volley.VolleyError;
import com.ns.odoo.OdooApp;
import com.ns.odoo.OdooV2;

public class App extends OdooApp {
    public final String url = "http://192.168.1.99:8070";

    @Override
    public String getOdooUrl() {
        return url;
    }

    @Override
    public void registerResponse(OdooV2 odoo, VolleyError error) {
        System.out.println("status:" + error + "_" + odoo);
    }


}
