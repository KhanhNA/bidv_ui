package com.ns.odoolib_retrofit.adapter;

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LazilyParsedNumber;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Adapter {
    public static final TypeAdapter<String> STRING = new TypeAdapter<String>() {
        @Override
        public String read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            if (peek == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            /* coerce booleans to strings for backwards compatibility */
            if (peek == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            return in.nextString();
        }

        @Override
        public void write(JsonWriter out, String value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Number> NUMBER = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            JsonToken jsonToken = in.peek();
            switch (jsonToken) {
                case NULL:
                    in.nextNull();
                    return null;
                case BOOLEAN:
                    in.nextBoolean();
                    return null;
                case NUMBER:
                case STRING:
                    return new LazilyParsedNumber(in.nextString());
                default:
                    throw new JsonSyntaxException("Expecting number, got: " + jsonToken);
            }
        }

        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Integer> INTEGER = new TypeAdapter<Integer>() {
        @Override
        public Integer read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            try {
                return new Double(in.nextDouble()).intValue();
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }
        @Override
        public void write(JsonWriter out, Integer value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Long> LONG = new TypeAdapter<Long>() {
        @Override
        public Long read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            try {
                return new Double(in.nextDouble()).longValue();
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }
        @Override
        public void write(JsonWriter out, Long value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Number> FLOAT = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            return (float) in.nextDouble();
        }
        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };

    public static final TypeAdapter<Number> DOUBLE = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            return in.nextDouble();
        }
        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    private static final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final TypeAdapter<OdooDateTime> DATETIME = new TypeAdapter<OdooDateTime>() {
        @Override
        public OdooDateTime read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            try {

                Date utcDate = sf.parse(in.nextString());
                long offset = TimeZone.getDefault().getOffset(utcDate.getTime());
                return new OdooDateTime(utcDate.getTime() + offset);
            } catch (Exception e) {
                throw new IOException(e);
            }
        }
        @Override
        public void write(JsonWriter out, OdooDateTime value) throws IOException {
            if(value == null){
                out.nullValue();
                return;
            }
            long milisec = value.getTime();
            long offset = TimeZone.getDefault().getOffset(milisec);

            out.value(milisec - offset);
        }
    };

    public static final TypeAdapter<OdooDate> DATE = new TypeAdapter<OdooDate>() {
        @Override
        public OdooDate read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            }
            try {

                Date utcDate = sf.parse(in.nextString());

                return new OdooDate(utcDate.getTime());
            } catch (Exception e) {
                throw new IOException(e);
            }
        }
        @Override
        public void write(JsonWriter out, OdooDate value) throws IOException {
            if(value == null) {
                out.nullValue();
            }else {
                out.value(value.getTime());
            }
        }
    };

}
