package com.ns.odoolib_retrofit.service;

import com.google.gson.JsonElement;
import com.ns.odoolib_retrofit.model.OdooResponseDto;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OdooServices {

//    @POST
////    @Headers( {"Content-Type: application/json"} )
////    default Call<Object> getDataJson(
////            @Url String url,
////            @Body RequestBody body){
////        return getDataJsonCokies( url, body);
////    }

    @POST
    Call<OdooResponseDto<JsonElement>> getDataJsonCokies(
//            @Header("Cookie") String cookie,
            @Header("Cookie") String sessionIdAndToken,
            @Url String url,
            @Body RequestBody body);
}
