package com.ns.odoolib_retrofit.wrapper;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.adapter.ArrayAdapterFactory;
import com.ns.odoolib_retrofit.adapter.DeserializeOnly;

import com.ns.odoolib_retrofit.adapter.HaiSer;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.HttpLogger;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseClient<Services> {
    protected Retrofit retrofit = null;

    public static String BASE_URL_OAUTH;

    public static String DATE_FORMAT;
    public static String TOKEN = "";

    public static String language = "vi";

    private Services services;

    public void reset() {
        retrofit = null;
    }

    protected String serverURL;
    protected Gson gson;

    public void changeLanguage(String lang) {
        retrofit = null;
        language = lang;
    }




    public BaseClient(Context context, String baseURL, Class<Services> service) {
        serverURL = baseURL;//stripURL(baseURL);
        gson = new Gson();
        gson = getGsonBuilder().create();
        services = init(service);
    }


//    public void init(Context context, String baseURL, Class<Services> service) {
//        BaseClient client = new BaseClient(context, baseURL, service);
//        return client.init(service);
//    }
    public static Retrofit getAuthClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Request.Builder newRequest = request.newBuilder().addHeader("Authorization", TOKEN)
                                .addHeader("Content-Type", "application/json")
                                .addHeader("Accept-Language", language);
                        return chain.proceed(newRequest.build());
                    }
                });
        //Add the interceptor to the client builder.
        GsonBuilder gsonBuilder = new GsonBuilder();
        //gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        gsonBuilder.setDateFormat(DATE_FORMAT);
        Retrofit ret = new Retrofit.Builder()
                .client(okHttpClientBuilder.build())
                .baseUrl(BASE_URL_OAUTH)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))

                .build();

        return ret;
    }


    public <Services>  Services init(Class<Services> services) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        Cache cache = new Cache(new File(mContext.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
        OkHttpClient.Builder mBuilder = new OkHttpClient.Builder()
//                .cache(cache)
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Request.Builder newRequest = request.newBuilder() //.addHeader("Authorization", TOKEN_DCOM)
                            .addHeader("Cookie", TOKEN)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept-Language", "1");
                    return chain.proceed(newRequest.build());
                });
        ;



        retrofit = new Retrofit.Builder()
//                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(mBuilder.build())
                .build();

        return retrofit.create(services);

    }

    public <T> T createService(final Class<T> service) {
        return retrofit.create(service);
    }






    public GsonBuilder getGsonBuilder(){
        GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(String.class, Adapter.STRING)
                .registerTypeAdapter(Number.class, Adapter.NUMBER)
                .registerTypeAdapter(Integer.class, Adapter.INTEGER)
                .registerTypeAdapter(Long.class, Adapter.LONG)
                .registerTypeAdapter(Float.class, Adapter.FLOAT)
                .registerTypeAdapter(Double.class, Adapter.DOUBLE)
                .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
//                    .registerTypeAdapter(ArrayList.class, new ArrayDeserializer<>())




                .registerTypeAdapterFactory(new ArrayAdapterFactory())

//                    .registerTypeAdapter(type, new MyDeserializer<OdooResponseDto>())
                ;
//
        builder.addSerializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(final FieldAttributes f) {
                return f.getAnnotation(DeserializeOnly.class) != null;
            }

            @Override
            public boolean shouldSkipClass(final Class<?> clazz) {
                return false;
            }
        });
        return builder;
    }


    protected String stripURL(String url) {
        if (url != null) {
            String newUrl;
            if (url.endsWith("/")) {
                newUrl = url.substring(0, url.lastIndexOf("/"));
            } else {
                newUrl = url;
            }
            return newUrl;
        }
        return null;
    }


    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }
}
