/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 * <p>
 * Created on 22/4/15 4:04 PM
 */
package com.ns.odoolib_retrofit.model;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OdooRelType extends ArrayList {


    public void setId(Long id){
        if(id == null){
            return;
        }
        setId(id.toString());
    }
    public void setId(String id){

        if(size() > 0){
            set(0, id);
        }else{
            add(id);
        }
    }
    public Long getId(){
        if(size() > 0){
            String id = get(0).toString();
            return (Double.valueOf(id)).longValue();
        }
        return null;
    }
}

