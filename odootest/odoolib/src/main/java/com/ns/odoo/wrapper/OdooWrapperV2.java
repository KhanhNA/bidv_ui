package com.ns.odoo.wrapper;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.google.gson.Gson;
import com.ns.odoo.OdooV2;
import com.ns.odoo.core.rpc.handler.OdooVersionException;
import com.ns.odoo.core.rpc.helper.OArguments;
import com.ns.odoo.core.rpc.helper.ODomain;
import com.ns.odoo.core.rpc.helper.ORecordValues;
import com.ns.odoo.core.rpc.helper.OdooFields;
import com.ns.odoo.core.rpc.helper.OdooSessionDto;
import com.ns.odoo.core.rpc.helper.OdooVersion;
import com.ns.odoo.core.rpc.helper.utils.OdooLog;
import com.ns.odoo.core.rpc.listenersV2.IModuleInstallListener;
import com.ns.odoo.core.rpc.listenersV2.IOdooConnectionListener;
import com.ns.odoo.core.rpc.listenersV2.IOdooLoginCallback;
import com.ns.odoo.core.rpc.listenersV2.IOdooResponse;
import com.ns.odoo.core.support.OUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OdooWrapperV2<T> {
    public static final String TAG = OdooWrapperV2.class.getName();

    protected String serverURL;
    protected OdooVersion mVersion = new OdooVersion();
    private RequestQueue requestQueue;
    //    private OdooResponseQueueV2 responseQueue;
    protected IOdooConnectionListener mIOdooConnectionListener;
    protected OdooSessionDto odooSession = new OdooSessionDto();
    protected OUser user;
    protected Gson gson;
    private OdooV2 mOdoo;
    private HashMap<String, Object> tempContext = null;

    private Integer new_request_timeout = OdooV2.REQUEST_TIMEOUT_MS;
    private Integer new_request_max_retry = OdooV2.DEFAULT_MAX_RETRIES;

    public OdooWrapperV2(Context context, String baseURL) {
        serverURL = stripURL(baseURL);
        gson = new Gson();
//        responseQueue = new OdooResponseQueueV2();
//        requestQueue = Volley.newRequestQueue(context);
        Network network = new BasicNetwork(new HurlStack());
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap
        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();
    }

    @SuppressWarnings("unchecked")
    protected T connect(boolean synchronizedTask) throws OdooVersionException {
        mOdoo = (OdooV2) this;
        if (serverURL != null) {
            OdooLog.v("Connecting to " + serverURL);

            getVersionInfo(new IOdooResponse<OdooVersion>() {
                @Override
                public void onResponse(OdooVersion response, VolleyError error) {
                    if (mIOdooConnectionListener != null) {
                        mIOdooConnectionListener.onConnect(mOdoo, error);
                    }
                }

            });

        }
        return (T) this;
    }

    public T withRetryPolicy(Integer request_timeout, Integer max_retry) {
        new_request_timeout = request_timeout;
        new_request_max_retry = max_retry;
        return (T) this;
    }
    public void callRoute(final String route, JSONObject params,
                          IOdooResponse odooResponse) {
        String url = serverURL + route;
        newJSONPOSTRequest(url, params, odooResponse);
    }
    private void newJSONPOSTRequest(final String url, JSONObject params,
                                          IOdooResponse odooResponse) {
        OdooLog.d("REQUEST URL : " + url);
        final JSONObject postData = createRequestWrapper(params, odooResponse);
        OdooLog.d("POST DATA: " + postData);

        Type[] types = odooResponse.getClass().getGenericInterfaces();
        Type type;
        if(types != null && types.length > 0){
            type = types[0];
        }else {
            type = odooResponse.getClass().getGenericSuperclass();
        }

        Type arg = ((ParameterizedType) type).getActualTypeArguments()[0];
        OdooRequest request = new OdooRequest(url, postData.toString(), arg, odooResponse);
        request.setRetryPolicy(new DefaultRetryPolicy(new_request_timeout, new_request_max_retry,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);

        new_request_timeout = OdooV2.REQUEST_TIMEOUT_MS;
        new_request_max_retry = OdooV2.DEFAULT_MAX_RETRIES;
    }

//    private void requestController(String fullURL, JSONObject data, IOdooResponse callback) {
//        newJSONPOSTRequest(fullURL, data, callback);
//    }

    private JSONObject createRequestWrapper(JSONObject params, IOdooResponse callback) {
        JSONObject requestData = new JSONObject();
        try {
            int randomId = getRequestID();
            JSONObject newParams = params;
            if (newParams == null) {
                newParams = new JSONObject();
            }

            requestData.put("jsonrpc", "2.0");
            requestData.put("method", "call");
            requestData.put("params", newParams);
            requestData.put("id", randomId);
//            if (callback != null)
//                responseQueue.add(randomId, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
        return requestData;
    }

    private int getRequestID() {
        return Math.abs(new Random().nextInt(9999));
    }

    public void getVersionInfo(final IOdooResponse res) {
        String url = serverURL + "/web/webclient/version_info";
        newJSONPOSTRequest(url, null, new IOdooResponse<OdooVersion>() {
            @Override
            public void onResponse(OdooVersion response, VolleyError error) {
                if (error != null) {
                    res.onResponse(null, error);
                    return;
                } else {
                    if (response == null || response == null) {
                        res.onResponse(null, new VolleyError("versionEmpty"));
                        return;
                    }
//                    mVersion = response.result.getRecords().get(0);
                    res.onResponse(response, null);
                }

            }

        });
    }

    public void getSessionInfo(final IOdooResponse callback) {
        String url = serverURL + "/web/session/get_session_info";
        newJSONPOSTRequest(url, null, new IOdooResponse<OdooSessionDto>() {
            @Override
            public void onResponse(OdooSessionDto response, VolleyError error) {
                if (error != null) {
                    callback.onResponse(null, error);
                    return;
                }
                //thonv:odooSession = response.result.getRecords().get(0); //OdooSession.parseSessionInfo(response);
                if (callback != null)
                    callback.onResponse(response, error);
            }
        });
    }


    /**
     * Authenticate user
     *
     * @param username Username
     * @param password Password
     * @param database Database
     * @param callback Callback Response
     */
    public void authenticate(final String username, final String password, final String database,
                              final IOdooResponse callback) {
        try {
            String url = serverURL + "/web/session/authenticate";
            JSONObject params = new JSONObject();
            params.put("db", database);
            params.put("login", username);
            params.put("password", password);
            params.put("context", new JSONObject());
            newJSONPOSTRequest(url, params, callback);
//            newJSONPOSTRequest(url, params, new IOdooResponse<OdooSessionDto>() {
//                @Override
//                public void onResponse(OdooSessionDto res, VolleyError err) {
//                    if (err != null) {
//                        callback.onLoginResponse(null, null, err);
//                        return;
//                    }
//                    odooSession = res;
//                    generateUserObject(username, password, database, callback);
//
//                }
//
//
//            });
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }

//    private void bindOdooSession(OdooResult response) {
//        if (odooSession == null) odooSession = new OdooSession();
//        if (mVersion.getVersionNumber() > 7 && response.containsKey("company_id"))
//            odooSession.setCompanyId(response.getInt("company_id"));
//        odooSession.setDb(response.getString("db"));
//        odooSession.setSessionId(response.getString("session_id"));
//        odooSession.setUid(response.getInt("uid"));
//        odooSession.setUserContext(response.getMap("user_context"));
//        odooSession.setUsername(response.getString("username"));
//
//        if (mVersion.getVersionNumber() >= 10) {
//            OdooResult currencies = response.getMap("currencies");
//            List<OdooUserCurrency> currencyList = new ArrayList<>();
//            for (String key : currencies.keySet()) {
//                OdooResult map = currencies.getMap(key);
//                OdooUserCurrency currency = new OdooUserCurrency();
//                List<Double> values = map.getArray("digits");
//                currency.id = Integer.parseInt(key);
//                currency.digits = new Integer[]{values.get(0).intValue(), values.get(1).intValue()};
//                currency.position = map.getString("position");
//                currency.symbol = map.getString("symbol");
//                currencyList.add(currency);
//            }
//            odooSession.setCurrencies(currencyList);
//            if (mVersion.isEnterprise()) {
//                if (response.containsKey("expiration_date"))
//                    odooSession.setExpiration_date(response.getString("expiration_date"));
//                if (response.containsKey("expiration_reason"))
//                    odooSession.setExpiration_reason(response.getString("expiration_reason"));
//                if (response.containsKey("warning"))
//                    odooSession.setWarning_level(response.getString("warning"));
//            }
//            odooSession.setIs_admin(response.getBoolean("is_admin"));
//            odooSession.setIs_superuser(response.getBoolean("is_superuser"));
//            odooSession.setServer_version(response.getString("server_version"));
//            odooSession.setWeb_base_url(response.getString("web.base.url"));
//        }
//    }

    public T withContext(HashMap<String, Object> context) {
        if (context != null) {
            tempContext = new HashMap<>();
            tempContext.putAll(context);
        }
        return (T) this;
    }



    public void nameSearch(String model, String query, ODomain domain, int limit, IOdooResponse callback
            ) {
        try {
            HashMap<String, Object> kwargs = new HashMap<>();
            kwargs.put("name", query);
            kwargs.put("args", (domain != null) ? domain.getAsList() : new ArrayList<>());
            kwargs.put("operator", "ilike");
            kwargs.put("limit", limit);
            callMethod(model, "name_search", new OArguments(), kwargs, null, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }



    public void searchCount(String model, ODomain domain, IOdooResponse callback) {
        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("method", "search_count");
            OArguments args = new OArguments();
            args.add(domain.getArray());
            callMethod(model, "search_count", args, new HashMap<String, Object>(), new HashMap<String, Object>(),
                    callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }


    public void searchRead(String model, OdooFields fields, ODomain domain,
                                 int offset, int limit, String sort, final IOdooResponse callback) {
        try {
            String url = serverURL + "/web/dataset/search_read";
            JSONObject params = new JSONObject();
            params.put("model", model);
            if (fields == null) {
                fields = new OdooFields();
            }
            params.put("fields", fields.get().getJSONArray("fields"));
            if (domain == null) {
                domain = new ODomain();
            }
            params.put("domain", domain.getArray());
//            JSONObject context = updateCTX(odooSession.getUserContext());
//            params.put("context", context);
            params.put("offset", offset);
            params.put("limit", limit);
            params.put("sort", (sort == null) ? "" : sort);

            newJSONPOSTRequest(url, params, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }


    public void executeWorkFlow(String model, int id, String signal, IOdooResponse callback) {
        String url = serverURL + "/web/dataset/exec_workflow";
        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("id", id);
            params.put("signal", signal);
            newJSONPOSTRequest(url, params, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }

    private JSONObject updateCTX(JSONObject context) {
        try {
            if (tempContext != null) {
                for (String key : tempContext.keySet()) {
                    context.put(key, tempContext.get(key));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context;
    }

    public void read(String model, int id, OdooFields fields, final IOdooResponse callback) {
        String url = serverURL + "/web/dataset/call_kw/" + model + "/read";
        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("method", "read");
            OArguments args = new OArguments();
            args.add(id);
            if (fields == null) {
                fields = new OdooFields();
            }
            args.add(fields.getArray());
            params.put("args", args.getArray());
            JSONObject kwargs = new JSONObject();
//            kwargs.put("context", odooSession.getUserContext());
            params.put("kwargs", kwargs);
            newJSONPOSTRequest(url, params, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }

    private void getModelFields(String model, IOdooResponse callback) {
        OdooFields fields = new OdooFields();
        fields.addAll(new String[]{"name", "field_description", "ttype", "model_id"});
        ODomain domain = new ODomain();
        domain.add("model_id", "=", model);
        searchRead("ir.model.fields", fields, domain, 0, 0, null, callback);
    }


    public void callMethod(String model, String method, OArguments arguments,
                           HashMap<String, Object> kwargs, HashMap<String, Object> context,
                           IOdooResponse callback) {
        String url = serverURL + "/web/dataset/call_kw";
        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("method", method);
            params.put("args", arguments.getArray());
            params.put("kwargs", (kwargs != null)
                    ? new JSONObject(gson.toJson(kwargs)) : new JSONObject());
//            params.put("context", (context != null) ?
//                    new JSONObject(gson.toJson(updateContext(context)))
//                    : odooSession.getUserContext());
            newJSONPOSTRequest(url, params, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
            callback.onResponse(null, new VolleyError(e));
        }
    }





    public void permRead(String model, List<Integer> ids, IOdooResponse callback) {
        try {
            OArguments args = new OArguments();
            args.add(new JSONArray(ids.toString()));
            callMethod(model, "perm_read", args, null, null, callback);
        } catch (JSONException e) {
            OdooLog.e(e, e.getMessage());
        }
    }



    public void createRecord(String model, ORecordValues values, IOdooResponse callback) {
        try {
            OArguments args = new OArguments();
            args.add(new JSONObject(gson.toJson(values)));
            HashMap<String, Object> map = new HashMap<>();
//            map.put("context", gson.fromJson(odooSession.getUserContext() + "",
//                    HashMap.class));
            callMethod(model, "create", args, map, null, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }



    public void updateRecord(String model, ORecordValues values, List<Integer> ids, IOdooResponse callback) {
        try {
            OArguments args = new OArguments();
            args.add(new JSONArray(ids.toString()));
            args.add(new JSONObject(gson.toJson(values)));
            HashMap<String, Object> map = new HashMap<>();
//            map.put("context", gson.fromJson(odooSession.getUserContext() + "",
//                    HashMap.class));
            callMethod(model, "write", args, map, null, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }



    public void unlinkRecord(String model, List<Integer> ids, IOdooResponse callback) {
        try {
            OArguments args = new OArguments();
            args.add(new JSONArray(ids.toString()));
            HashMap<String, Object> map = new HashMap<>();
//            map.put("context", gson.fromJson(odooSession.getUserContext() + "",
//                    HashMap.class));
            callMethod(model, "unlink", args, map, null, callback);
        } catch (Exception e) {
            OdooLog.e(e, e.getMessage());
        }
    }


    public void installedOnServer(String moduleName, final IModuleInstallListener callback) {
        OdooFields fields = new OdooFields();
        fields.addAll(new String[]{"state", "name"});
        ODomain domain = new ODomain();
        domain.add("name", "=", moduleName);
        searchRead("ir.module.module", fields, domain, 0, 0, null,
                new IOdooResponse<JSONObject>() {

            @Override
            public void onResponse(JSONObject response, VolleyError error) {
                if (error != null) {
                    OdooLog.e(error);
                    callback.installedOnServer(false);
                    return;
                }
                //thonv:if (response.result.getRecords().size() > 0)
                {
//                    OdooRecord rec = response.result.getRecords().get(0);
//                    callback.installedOnServer(rec.getString("state").equals("installed"));
                }
            }

        });
    }

    private void generateUserObject(String username, String password,
                                    String db, final IOdooLoginCallback callback) {
        final OUser[] users = new OUser[1];
        users[0] = new OUser();
        users[0].setUsername(username);
        users[0].setPassword(password);
        users[0].setDatabase(db);
        users[0].setOdooVersion(mVersion);
        users[0].setUserId(odooSession.getUid());
        users[0].setCompanyId(odooSession.getCompany_id());
        users[0].setHost(serverURL);
        OdooFields fields = new OdooFields();
        fields.addAll(new String[]{"name", "partner_id", "tz", "company_id"});
        ODomain domain = new ODomain();
        domain.add("id", "=", users[0].getUserId());


        read("res.users", users[0].getUserId(), fields, new IOdooResponse<ArrayList<OUser>>() {
            @Override
            public void onResponse(ArrayList<OUser> response, VolleyError error) {
                if (error != null) {
                    callback.onLoginResponse(null, null, error);
                    return;
                }
                //thonv:users[0] = response.result.getRecords().get(0);//parseUserObject(users[0], response);

                callback.onLoginResponse(mOdoo, users[0], null);
            }

        });

    }



    private String stripURL(String url) {
        if (url != null) {
            String newUrl;
            if (url.endsWith("/")) {
                newUrl = url.substring(0, url.lastIndexOf("/"));
            } else {
                newUrl = url;
            }
            return newUrl;
        }
        return null;
    }



    private String getDBPrefix(String host) {
        Pattern pattern = Pattern.compile(".runbot[1-9]{1,2}..odoo.com?(.+?)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(host);
        return matcher.replaceAll("").replaceAll("http://", "").replaceAll("https://", "");
    }

    private boolean isRunbotURL(String host) {
        Pattern pattern = Pattern.compile(".runbot[1-9]{1,2}..odoo.com?(.+?)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(host);
        return matcher.find();
    }


}
