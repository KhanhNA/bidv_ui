/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 * <p/>
 * Created on 21/4/15 4:01 PM
 *
 */
package com.ns.odoo;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.ns.odoo.core.rpc.handler.OdooVersionException;
import com.ns.odoo.core.rpc.helper.OdooSessionDto;
import com.ns.odoo.core.rpc.helper.OdooVersion;
import com.ns.odoo.core.rpc.listenersV2.IOdooConnectionListener;
import com.ns.odoo.core.support.OUser;
import com.ns.odoo.wrapper.OdooWrapperV2;


public class OdooV2 extends OdooWrapperV2<OdooV2> {
    public static final String TAG = OdooV2.class.getSimpleName();
    public static Boolean DEBUG = false;
    public static Integer REQUEST_TIMEOUT_MS = DefaultRetryPolicy.DEFAULT_TIMEOUT_MS;
    public static Integer DEFAULT_MAX_RETRIES = DefaultRetryPolicy.DEFAULT_MAX_RETRIES;

    public enum ErrorCode {
        OdooVersionError(700),
        InvalidURL(404),
        OdooServerError(200),
        UnknownError(703),
        AuthenticationFail(401);

        int code;

        ErrorCode(int code) {
            this.code = code;
        }

        public int get() {
            return this.code;
        }

    }

    public OdooV2(Context context, String baseURL) {
        super(context, baseURL);
    }


//    public static OUser quickConnect(Context context, String url, String username,
//                                     String password, String database) throws OdooVersionException {
//        OdooV2 odoo = new OdooV2(context, url).connect(true);
//        return odoo.authenticate(username, password, database);
//    }


//    public static void quickConnect(Context context, String url, String username, String password,
//                                    String database, IOdooLoginCallback callback)
//            throws OdooVersionException {
//        OdooV2 odoo = createInstance(context, url);
//        odoo.authenticate(username, password, database, callback);
//    }

    public static OdooV2 createInstance(Context context, String baseURL) throws OdooVersionException {
        return new OdooV2(context, baseURL).connect(false);
    }


    public static OdooV2 createQuickInstance(Context context, String baseURL)
            throws OdooVersionException {
        return new OdooV2(context, baseURL).connect(true);
    }

    public OdooV2 setOnConnect(IOdooConnectionListener callback) {
        mIOdooConnectionListener = callback;
        return this;
    }

    public OdooVersion getVersion() {
        return mVersion;
    }

    public OUser getUser() {
        return user;
    }

    public OdooSessionDto getSession() {
        return odooSession;
    }

    @Override
    public String toString() {
        return "Odoo{" +
                serverURL + ": " +
                mVersion +
                '}';
    }
}
