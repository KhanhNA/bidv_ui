/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 * <p/>
 * Created on 17/12/14 6:06 PM
 */
package com.ns.odoo;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import com.android.volley.VolleyError;
import com.ns.odoo.core.orm.ModelRegistryUtils;
import com.ns.odoo.core.orm.OModel;
import com.ns.odoo.core.orm.OSQLite;
import com.ns.odoo.core.rpc.handler.OdooVersionException;
import com.ns.odoo.core.rpc.listenersV2.IOdooConnectionListener;
import com.ns.odoo.core.support.OUser;

import java.lang.reflect.Constructor;
import java.util.HashMap;

public abstract class OdooApp extends Application implements IOdooConnectionListener {

    public static final String TAG = OdooApp.class.getSimpleName();
    public static String APPLICATION_NAME;
    private static HashMap<String, OdooV2> mOdooInstances = new HashMap<>();
    private static HashMap<String, OSQLite> mSQLiteObjecs = new HashMap<>();
    private static ModelRegistryUtils modelRegistryUtils = new ModelRegistryUtils();
    private OdooV2 mOdoo;
    @Override
    public void onCreate() {
        super.onCreate();
        OdooApp.APPLICATION_NAME = getPackageManager().getApplicationLabel(getApplicationInfo()).toString();
        OdooApp.modelRegistryUtils.makeReady(getApplicationContext());
        try {
            registerOddo();
        } catch (OdooVersionException e) {
            e.printStackTrace();
        }
    }

    public void registerOddo() throws OdooVersionException {
        OdooV2.createInstance(getApplicationContext(), getOdooUrl()).setOnConnect(this);
    }
    public static OSQLite getSQLite(String userName) {
        return mSQLiteObjecs.containsKey(userName) ? mSQLiteObjecs.get(userName) : null;
    }

    public static void setSQLite(String userName, OSQLite sqLite) {
        mSQLiteObjecs.put(userName, sqLite);
    }

    public OdooV2 getOdoo(OUser user) {
        if (mOdooInstances.containsKey(user.getAndroidName())) {
            return mOdooInstances.get(user.getAndroidName());
        }
        return null;
    }

    public void setOdoo(OdooV2 odoo, OUser user) {
        if (user != null)
            mOdooInstances.put(user.getAndroidName(), odoo);
    }

    /**
     * Checks for network availability
     *
     * @return true, if network available
     */
    public boolean inNetwork() {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw      = connectivityManager.getActiveNetwork() ;
            if(nw == null) {
                return false;
            }
            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
            if(actNw == null) {
                return false;
            }
            if( actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ||
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) ) {
                return true;
            }else{
                return false;
            }
        } else {


            NetworkInfo nInfo = connectivityManager.getActiveNetworkInfo();
            if (nInfo != null && nInfo.isConnectedOrConnecting()) {
                isConnected = true;
            }
        }
        return isConnected;
    }

    /**
     * Checks for installed application
     *
     * @param appPackage
     * @return true, if application installed on device
     */
    public boolean appInstalled(String appPackage) {
        boolean mInstalled = false;
        try {
            PackageManager mPackage = getPackageManager();
            mPackage.getPackageInfo(appPackage, PackageManager.GET_ACTIVITIES);
            mInstalled = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mInstalled;
    }

    public static <T> T getModel(Context context, String modelName, OUser user) {
        Class<? extends OModel> modelCls = OdooApp.modelRegistryUtils.getModel(modelName);
        if (modelCls != null) {
            try {
                Constructor constructor = modelCls.getConstructor(Context.class, OUser.class);
                return (T) constructor.newInstance(context, user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void onConnect(OdooV2 odoo, VolleyError error) {
        mOdoo = odoo;
        registerResponse(odoo, error);
    }



    public ModelRegistryUtils getModelRegistry() {
        return modelRegistryUtils;
    }
    public abstract String getOdooUrl();
    public abstract void registerResponse(OdooV2 odoo, VolleyError error);

    public OdooV2 getOdoo() {
        return mOdoo;
    }
}
