package com.sample;


public class SharevanBillLadingDetail {

  private long id;
  private String nameSeq;
  private long billLadingId;
  private double totalWeight;
  private long warehouseId;
  private String warehouseType;
  private long fromBillLadingDetailId;
  private String description;
  private java.sql.Timestamp expectedFromTime;
  private java.sql.Timestamp expectedToTime;
  private String status;
  private String approvedType;
  private long fromWarehouseId;
  private String statusOrder;
  private long createUid;
  private java.sql.Timestamp createDate;
  private long writeUid;
  private java.sql.Timestamp writeDate;
  private String name;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getNameSeq() {
    return nameSeq;
  }

  public void setNameSeq(String nameSeq) {
    this.nameSeq = nameSeq;
  }


  public long getBillLadingId() {
    return billLadingId;
  }

  public void setBillLadingId(long billLadingId) {
    this.billLadingId = billLadingId;
  }


  public double getTotalWeight() {
    return totalWeight;
  }

  public void setTotalWeight(double totalWeight) {
    this.totalWeight = totalWeight;
  }


  public long getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(long warehouseId) {
    this.warehouseId = warehouseId;
  }


  public String getWarehouseType() {
    return warehouseType;
  }

  public void setWarehouseType(String warehouseType) {
    this.warehouseType = warehouseType;
  }


  public long getFromBillLadingDetailId() {
    return fromBillLadingDetailId;
  }

  public void setFromBillLadingDetailId(long fromBillLadingDetailId) {
    this.fromBillLadingDetailId = fromBillLadingDetailId;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public java.sql.Timestamp getExpectedFromTime() {
    return expectedFromTime;
  }

  public void setExpectedFromTime(java.sql.Timestamp expectedFromTime) {
    this.expectedFromTime = expectedFromTime;
  }


  public java.sql.Timestamp getExpectedToTime() {
    return expectedToTime;
  }

  public void setExpectedToTime(java.sql.Timestamp expectedToTime) {
    this.expectedToTime = expectedToTime;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getApprovedType() {
    return approvedType;
  }

  public void setApprovedType(String approvedType) {
    this.approvedType = approvedType;
  }


  public long getFromWarehouseId() {
    return fromWarehouseId;
  }

  public void setFromWarehouseId(long fromWarehouseId) {
    this.fromWarehouseId = fromWarehouseId;
  }


  public String getStatusOrder() {
    return statusOrder;
  }

  public void setStatusOrder(String statusOrder) {
    this.statusOrder = statusOrder;
  }


  public long getCreateUid() {
    return createUid;
  }

  public void setCreateUid(long createUid) {
    this.createUid = createUid;
  }


  public java.sql.Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(java.sql.Timestamp createDate) {
    this.createDate = createDate;
  }


  public long getWriteUid() {
    return writeUid;
  }

  public void setWriteUid(long writeUid) {
    this.writeUid = writeUid;
  }


  public java.sql.Timestamp getWriteDate() {
    return writeDate;
  }

  public void setWriteDate(java.sql.Timestamp writeDate) {
    this.writeDate = writeDate;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
