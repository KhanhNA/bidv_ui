package oauth2;

import java.io.Serializable;

public class MenuEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -575415150358637616L;

	private Long id;

	private String clientId;

	private String url;

	private String code;

	private String appType;

	private MenuEntity parentMenu;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAppType() {
		return this.appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public MenuEntity getParentMenu() {
		return this.parentMenu;
	}

	public void setParentMenu(MenuEntity parentMenu) {
		this.parentMenu = parentMenu;
	}

}