/*
 * Copyright 2015 - 2016 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.traccar.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.traccar.Context;
import org.traccar.Main;
import org.traccar.api.resource.SessionResource;
import org.traccar.database.StatisticsManager;
import org.traccar.helper.Log;
import org.traccar.model.User;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import oauth2.OAuth2AuthenticationDto;

public class AsyncSocketServlet extends WebSocketServlet {

//    private static final long ASYNC_TIMEOUT = 10 * 60 * 1000;
//
//    @Override
//    public void configure(WebSocketServletFactory factory) {
//        factory.getPolicy().setIdleTimeout(Context.getConfig().getLong("web.timeout", ASYNC_TIMEOUT));
//        factory.setCreator((req, resp) -> {
//            if (req.getSession() != null) {
//                long userId = (Long) req.getSession().getAttribute(SessionResource.USER_ID_KEY);
//                return new AsyncSocket(userId);
//            } else {
//                return null;
//            }
//        });
//    }
    /**
     *
     */
    private static final long serialVersionUID = 407106100299305383L;

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String WWW_AUTHENTICATE = "WWW-Authenticate";
    public static final String BASIC_REALM = "Basic realm=\"api\"";
    public static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final String XML_HTTP_REQUEST = "XMLHttpRequest";
    public static final String SEC_WEBSOCKET_PROTOCOL = "Sec-WebSocket-Protocol";
    private static final long ASYNC_TIMEOUT = 10 * 60 * 1000;

    @javax.ws.rs.core.Context
    private HttpServletRequest request;

    @javax.ws.rs.core.Context
    private ResourceInfo resourceInfo;

    @javax.ws.rs.core.Context
    private SecurityContext securityContext;

    protected long getUserId() {
        UserPrincipal principal = (UserPrincipal) securityContext.getUserPrincipal();
        if (principal != null) {
            return principal.getUserId();
        }
        return 0;
    }

    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.getPolicy().setIdleTimeout(Context.getConfig().getLong("web.timeout", ASYNC_TIMEOUT));
        factory.setCreator((req, resp) -> {
            auth(req);
            return new AsyncSocket(getUserId());
        });
    }

    private void auth(ServletUpgradeRequest req) {

        request = req.getHttpServletRequest();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
            System.out.println(ipAddress);
        }

        if (req.getHttpServletRequest().getMethod().equals("OPTIONS")) {
            return;
        }

        SecurityContext sc = null;

        try {

            String authHeader = request.getParameter("token");
            if (authHeader != null) {
                sc = getUserMe("BEARER " + authHeader.replace(",", " "));
            } else if (req.getSession() != null) {
                Long userId = (Long) req.getSession().getAttribute(SessionResource.USER_ID_KEY);
                if (userId != null) {
                    Context.getPermissionsManager().checkUserEnabled(userId);
                    Main.getInjector().getInstance(StatisticsManager.class).registerRequest(userId);
                    sc = new UserSecurityContext(new UserPrincipal(userId));
                }

            }

        } catch (SecurityException e) {
            Log.exceptionStack(e.getCause());
        }

        if (sc != null) {
            this.securityContext = sc;
        } else {
            Method method = resourceInfo.getResourceMethod();
            if (!method.isAnnotationPresent(PermitAll.class)) {
                Response.ResponseBuilder responseBuilder = Response.status(Response.Status.UNAUTHORIZED);
                if (!XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH))) {
                    responseBuilder.header(WWW_AUTHENTICATE, BASIC_REALM);
                }
                throw new WebApplicationException(responseBuilder.build());
            }
        }

    }

    private SecurityContext getUserMe(String authHeader) {
        StringBuilder strBuf = new StringBuilder();
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        try {
            // Declare the connection to SSO api url
            URL url = new URL("https://mfunctions.com:9999/user/me");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(AUTHORIZATION_HEADER, authHeader);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : " + conn.getResponseCode());
            }
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String output = null;
            while ((output = reader.readLine()) != null) {
                strBuf.append(output);
            }
            ObjectMapper mapper = new ObjectMapper();
            OAuth2AuthenticationDto auth2AuthenticationDto = mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT).readValue(strBuf.toString(),
                    OAuth2AuthenticationDto.class);
            System.out.println(auth2AuthenticationDto.getName());
            User user = Context.getPermissionsManager().login(auth2AuthenticationDto.getName()); //.toLowerCase()
            Main.getInjector().getInstance(StatisticsManager.class).registerRequest(user.getId());
            request.getSession().setAttribute(SessionResource.USER_ID_KEY, user.getId());
            System.out.println(request.getSession());
            return new UserSecurityContext(new UserPrincipal(user.getId()));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }

}
