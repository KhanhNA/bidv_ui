package com.ac.dlp.enums;

import javax.persistence.criteria.CriteriaBuilder;

public enum DistanceType {

    /**
     * type
     * 1.kho -> hub
     * 2.hub -> hub
     * 3.hub -> depot
     * 4.depot -> depot
     * 5.depot -> hub
     * 6.hub -> kho
     */
    WAREHOUSE_HUB (1), HUB_HUB(2), HUB_DEPOT(3),
    DEPOT_DEPOT(4), DEPOT_HUB(5), HUB_WAREHOUSE(6);
    Integer value;
    DistanceType(Integer v){
        this.value = v;
    }
    public Integer getValue(){
        return value;
    }
    public static DistanceType of(Integer v) throws Exception {
        switch (v){
            case 1:
                return WAREHOUSE_HUB;
            case 2:
                return HUB_HUB;
            case 3:
                return HUB_DEPOT;
            case 4:
                return DEPOT_DEPOT;
            case 5:
                return  DEPOT_HUB;
            case 6:
                return HUB_WAREHOUSE;
        }
        throw new Exception("WrongValue");
    }

}
