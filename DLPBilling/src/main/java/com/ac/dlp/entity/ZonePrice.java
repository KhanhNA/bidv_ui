package com.ac.dlp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "sharevan_zone_price")
@Data
@NoArgsConstructor()
public class ZonePrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "price_normal_min")
    private Double priceNormalMin;

    @Column(name = "price_normal_max")
    private Double priceNormalMax;

    @Column(name = "zone_id")
    private Integer zoneId;

    @Column(name = "from_date")
    private Timestamp fromDate;

    @Column(name = "to_date")
    private Timestamp toDate;

    @Column(name = "status")
    private String status;

}
