package com.ac.dlp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Entity
@Table(name = "sharevan_bill_lading_detail")
@Data
@NoArgsConstructor()
public class BillLadingDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name_seq")
    private String nameSeq;
    @Column(name = "bill_Lading_Id")
    private Integer billLadingId;
    @Column(name = "total_Weight")
    private Double total_weight;
    @Column(name = "warehouse_Id")
    private Integer warehouseId;
    @Column(name = "warehouse_Type")
    private String warehouse_type;

    @Column(name = "from_Bill_Lading_Detail_Id")
    private Integer fromBillLadingDetailId;
    @Column(name = "description")
    private String description;

    @Column(name = "expected_From_Time")
    private java.sql.Timestamp expectedFromTime;
    @Column(name = "expected_To_Time")
    private java.sql.Timestamp expectedToTime;

    @Column(name = "status")
    private String status;
    @Column(name = "approved_Type")
    private String approvedType;

    @Column(name = "from_Warehouse_Id")
    private Integer fromWarehouseId;

    @Column(name = "zone_area_id")
    private Integer zone_area_id;

    @Column(name = "status_Order")
    private String statusOrder;

    @Column(name = "name")
    private String name;

    private transient List<AreaDistance> areaDistances;
    private transient List<BillPackage> billPackages;
    private transient List<ServiceType> billService;
    private transient Double extra_price;
    private transient Double distance;
    private Double express_distance;
    private Double price;
    public transient Integer index1;
    public Integer trans_id;
    public void setIndex1(Integer i){
        index1 = i;
    }
    public Integer getIndex1(){
        return index1;
    }

    public void addPrice(Double value){
        price = price == null || price == 0?value:price + value;
    }
}
