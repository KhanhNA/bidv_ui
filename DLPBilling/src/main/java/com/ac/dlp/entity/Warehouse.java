package com.ac.dlp.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Warehouse {
    private Integer id;
    private String name;
    private String areaInfo;
}
