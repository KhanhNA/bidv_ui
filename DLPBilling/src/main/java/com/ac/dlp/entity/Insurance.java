package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sharevan_insurance")
@Data
public class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "status")
    private String status;

    @Column(name = "package_percent")
    private Double packagePercent;

    @Column(name = "package_amount")
    private Double packageAmount;

    @Column(name = "insurance_type")
    private String insuranceType;

    @Column(name = "duration")
    private String duration;

    @Column(name = "package_type")
    private String package_type;
}
