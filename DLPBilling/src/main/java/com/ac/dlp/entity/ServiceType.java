package com.ac.dlp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sharevan_service_type")
@Data
@NoArgsConstructor()
public class ServiceType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "service_code")
    private String serviceCode;

    @Column(name = "max_person")
    private Integer maxPerson;

    @Column(name = "max_weight")
    private Double maxWeight;

    @Column(name = "vendor_service_code")
    private String vendorServiceCode;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;


}
