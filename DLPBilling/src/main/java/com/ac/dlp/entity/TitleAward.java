package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sharevan_title_award")
@Data
public class TitleAward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "percent_commission_value")
    private Double percentCommissionValue;

    @Column(name = "status")
    private String status;

    @Column(name = "title_award_type")
    private String titleAwardType;

    @Column(name = "type")
    private String type;
}
