package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "sharevan_distance_compute")
@Data
@NoArgsConstructor()
public class DistanceCompute {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "distance")
  private Double distance;

  @Column(name = "from_Seq")
  private String fromSeq;

  @Column(name = "to_Seq")
  private String toSeq;

  @Column(name = "status")
  private String status;

  @Column(name = "type")
  private String type;

  @Column(name = "name_seq")
  private String nameSeq;

  @Column(name = "description")
  private String description;

  @Column(name = "min_Price")
  private Double minPrice;

  @Column(name = "max_Price")
  private Double maxPrice;




}
