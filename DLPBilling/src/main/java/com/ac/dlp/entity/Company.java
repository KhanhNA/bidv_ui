package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "res_company")
@Data
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "award_company_id")
    private Integer awardCompanyId;

    @Column(name = "status")
    private String status;

    @Column(name = "company_type")
    private String companyType;

    @Column(name = "company_ranking")
    private Integer companyRanking;

}
