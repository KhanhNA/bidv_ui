export const environment = {
  production: true,
  BASE_URL: 'http://demo.nextsolutions.com.vn:8686',
  BASE_AUTHORIZATION_URL: 'http://demo.nextsolutions.com.vn:9999',
  CLIENT_ID: 'ShareWarehouse',
  CLIENT_SECRET: 'aoeuiDHTNS1234509876snthdIUEOA',
  PAGE_SIZE: 10,
  PAGE_SIZE_OPTIONS: [5, 10, 20, 30, 50, 100],
  DEFAULT_LANGUAGE: 'vi',
  DEFAULT_THEME: 'default',
  AUTHORITIES: [] as string[],
  LANGS: [{id: 1, code: 'vi', name: 'Việt Nam', logo: 'assets/Flags/vi.ico'},
    {id: 2, code: 'en', name: 'English', logo: 'assets/Flags/en.ico'},
    {id: 3, code: 'my', name: 'Myamar', logo: 'assets/Flags/vi.ico'}],
  CURR_LANG: 'vi',
  API_DATE_FORMAT: 'yyyy-MM-dd HH:mm:ss.SSS\'Z\'',
  DIS_DATE_FORMAT: 'dd/MM/yyyy',
  APP_DATE_FORMATS:
    {
      parse: {
        dateInput: {month: 'short', year: 'numeric', day: 'numeric'},
      },
      display: {
        dateInput: 'input',
        monthYearLabel: {year: 'numeric', month: 'numeric'},
        dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
        monthYearA11yLabel: {year: 'numeric', month: 'long'},
      }
    }
};
