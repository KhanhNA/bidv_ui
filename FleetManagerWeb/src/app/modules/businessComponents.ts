import {CustomerComponent} from '../components/category/customer/list-customer/list-customer.component';
import {AddEditCustomerComponent} from '../components/category/customer/a-e-customer/a-e-customer.component';

export const BusinessComponents = [
  CustomerComponent,
  AddEditCustomerComponent
];
