import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Page } from '@next-solutions/next-solutions-base';


@Injectable()
export class AuthenticationService {
  public static requests: HttpRequest<any>[] = [];

  constructor(private http: HttpClient) {
  }

  login(loginPayload) {
    const headers = {
      'Content-type': 'application/x-www-form-urlencoded',
    };
    return this.http.post(environment.BASE_AUTHORIZATION_URL + '/oauth/token', loginPayload, { headers });
  }

  isAuthenticated() {
    if (window.sessionStorage.getItem('token') != null) {
      return true;
    }
    return false;
  }

  userMe() {
    return this.http.get(environment.BASE_AUTHORIZATION_URL + '/user/me');
  }

  findRole() {
    const params = new HttpParams().set('clientId', environment.CLIENT_ID).set('pageNumber', '0').set('pageSize', '8888');
    return this.http.get<Page>(environment.BASE_AUTHORIZATION_URL + '/role/find', { params });
  }
}
