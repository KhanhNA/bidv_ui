import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {CustomerStatusEnum} from '../../../../_models/enums/CustomerStatusEnum';
import {CustomerModel} from '../../../../_models/customer.model';
import {ApiService, BaseAddEditLayout, SelectModel, UtilsService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-a-e-customer',
  templateUrl: './a-e-customer.component.html',
  styleUrls: ['./a-e-customer.component.scss'],
})
export class AddEditCustomerComponent extends BaseAddEditLayout {

  moduleName = 'customer.ae';
  statusValues: SelectModel[] = [];

  constructor(protected activatedRoute: ActivatedRoute, protected formBuilder: FormBuilder, protected location: Location,
              protected translateService: TranslateService, protected apiService: ApiService, protected utilsService: UtilsService) {
    super(activatedRoute, location, translateService, utilsService);
  }

  ngOnInit = async () => {
    super.ngOnInit();

    this.addEditForm = this.formBuilder.group({
      code: [''],
      name: [''],
      address: [''],
      phone: [''],
      status: [''],
    });

    Object.keys(CustomerStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(CustomerStatusEnum, key.replace('_', ''));
      this.translateService.get(value).subscribe(res => {
        this.statusValues.push(new SelectModel(key.replace('_', ''), res));
      });
    });

    if (this.isEdit) {
      const customer = await this.apiService.get('/customers/' + this.id, null).toPromise() as CustomerModel;
      this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, customer));
    }
  };

  onSubmit(): void {
    const objSave = new CustomerModel(this.addEditForm);
    objSave.id = this.id;
    const apiCall = this.isEdit ?
      this.apiService.patch('/customers/' + this.id, objSave) :
      this.apiService.post('/customers', objSave);
    const action = this.isEdit ? '.edit' : '.add';
    this.utilsService.execute(apiCall, this.onSuccessFunc, this.moduleName + action + '.success', this.moduleName + action + '.confirm');
  }

  hasAuthority(): boolean {
    return true;
    // if ((this.isEdit && AuthoritiesUtils.hasAuthority('patch/customers/{id}'))
    //   || (!this.isEdit && AuthoritiesUtils.hasAuthority('post/customers'))) {
    //   return true;
    // }
    // return false;
  }

  isExists(): boolean {
    return this.addEditForm.get('status').value == null ? false : true;
  }

}
