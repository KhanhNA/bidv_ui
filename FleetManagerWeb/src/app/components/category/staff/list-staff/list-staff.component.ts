import {Component, Injector, OnInit} from '@angular/core';
import {
  ApiService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields, FormStateService,
  SelectModel, UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {CustomerStatusEnum} from '../../../../_models/enums/CustomerStatusEnum';
import {AuthoritiesUtils} from '../../../../base/utils/authorities.utils';
import {HttpParams} from '@angular/common/http';
import {StaffModel} from '../../../../_models/staff.model';
import {StaffStatusEnum} from '../../../../_models/enums/StaffStatusEnum';

@Component({
  selector: 'app-list-staff',
  templateUrl: './list-staff.component.html',
  styleUrls: ['./list-staff.component.scss']
})
export class StaffComponent extends BaseSearchLayout {

  moduleName = 'staff';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  statusValues: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder, protected router: Router, protected apiService: ApiService,
              protected utilsService: UtilsService, protected formStateService: FormStateService,
              protected translateService: TranslateService, protected injector: Injector) {
    super(router, apiService, utilsService, formStateService, translateService, injector,
      formBuilder.group({
        staffCode: [''],
        fullName: [''],
        hireDate: [''],
        status: ['_'],
      }));

    this.columns.push(
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
      },
      {
        columnDef: 'staffCode', header: 'staffCode',
        title: (c: StaffModel) => `${c.staffCode}`,
        cell: (c: StaffModel) => `${c.staffCode}`,
        className: 'mat-column-code',
      },
      {
        columnDef: 'fullName', header: 'fullName',
        title: (c: StaffModel) => `${c.fullName}`,
        cell: (c: StaffModel) => `${c.fullName}`,
        className: 'mat-column-name',
      },
      {
        columnDef: 'hireDate', header: 'hireDate',
        title: (c: StaffModel) => `${c.hireDate}`,
        cell: (c: StaffModel) => `${c.hireDate}`,
        className: 'mat-column-address',
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (c: StaffModel) => `${
          this.utilsService.getEnumValueTranslated(CustomerStatusEnum, c.status === StaffStatusEnum._1 ? '1' : '0')}`,
        cell: (c: StaffModel) => `${
          this.utilsService.getEnumValueTranslated(CustomerStatusEnum, c.status === StaffStatusEnum._1 ? '1' : '0')}`,
        className: 'mat-column-status',
      },
    );

    this.buttons.push(
      {
        columnDef: 'resetPass',
        color: 'warn',
        icon: 'loop',
        click: 'resetPass',
        display: (c: StaffModel) => c && true,
      },
 /*     {
        columnDef: 'active',
        color: 'warn',
        icon: 'done_all',
        click: 'active',
        disabled: (c: StaffModel) =>
          UtilsService.getEnumValue(CustomerStatusEnum, c.status === true ? 'ACTIVE' : 'INACTIVE') !== CustomerStatusEnum._0,
        display: (c: StaffModel) => c && AuthoritiesUtils.hasAuthority('post/customers/active'),
      },*/
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'edit',
        header: {
          columnDef: 'add',
          icon: 'add',
          color: 'warn',
          click: 'addOrEdit',
          display: (c: StaffModel) => true, // AuthoritiesUtils.hasAuthority('post/customers'),
        },
        click: 'addOrEdit',
        display: (c: StaffModel) => c && true, //AuthoritiesUtils.hasAuthority('patch/customers/{id}'),
      },
    /*  {
        columnDef: 'deactivate',
        color: 'warn',
        icon: 'clear',
        click: 'deactivate',
        disabled: (c: StaffModel) =>
          UtilsService.getEnumValue(CustomerStatusEnum, c.status === true ? '1' : '0') !== CustomerStatusEnum._1,
        display: (c: StaffModel) => c && AuthoritiesUtils.hasAuthority('post/customers/deactivate'),
      },*/
    );
  }

  ngOnInit = async () => {
    Object.keys(CustomerStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(CustomerStatusEnum, key.replace('_', ''));
      this.translateService.get(value).subscribe(res => {
        this.statusValues.push(new SelectModel(key, res));
      });
    });

    super.ngOnInit();

    this.onSubmit();
  };

  search() {
    const status = this.searchForm.get('status').value.replace('_', '');
    const params = new HttpParams()
      .set('staffCode', this.searchForm.get('staffCode').value)
      .set('fullName', this.searchForm.get('fullName').value)
      .set('hireDate', this.searchForm.get('hireDate').value)
      .set('status', status)
    this._fillData('/staff', params);
  }

  addOrEdit(customer: StaffModel) {
    if (customer) {
      this.router.navigate([this.router.url, 'edit', customer.id]);
    } else {
      this.router.navigate([this.router.url, 'add']);
    }
  }

  active(row: StaffModel, index: number) {
    this.utilsService.execute(this.apiService.post('/customers/active', row),
      this.onSuccessFunc, this.moduleName + '.success.active',
      this.moduleName + '.confirm.active');
  }

  deactivate(row: StaffModel, index: number) {
    this.utilsService.execute(this.apiService.post('/customers/deactivate', row),
      this.onSuccessFunc, this.moduleName + '.success.deactivate',
      this.moduleName + '.confirm.deactivate');
  }

  resetPass(customer: StaffModel) {
    this.utilsService.execute(this.apiService.post('/customers/reset-pass', customer),
      this.onSuccessFunc, this.moduleName + '.confirm.reset-pass',
      this.moduleName + '.confirm.reset-pass');
  }

}
